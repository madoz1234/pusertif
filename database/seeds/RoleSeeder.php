<?php

use Illuminate\Database\Seeder;
use App\Models\Authentication\Role;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [
            ['name' => 'admin', 'display_name' => 'ADMINISTRATOR', 'description' => 'ADMINISTRATOR'],
            ['name' => 'user', 'display_name' => 'User', 'description' => 'User'],
            ['name' => 'spm', 'display_name' => 'SPM', 'description' => 'SPM'],
            ['name' => 'keuangan', 'display_name' => 'KEUANGAN', 'description' => 'KEUANGAN'],

            // ['name' => 'pelaksana', 'display_name' => 'PELAKSANA', 'description' => 'PELAKSANA'],
            // ['name' => 'pelaksana-leader', 'display_name' => 'PELAKSANA LEADER', 'description' => 'PELAKSANA LEADER'],
            
            // YAN
            ['name' => 'yan-kalibrasi', 'display_name' => 'YAN KALIBRASI', 'description' => 'YAN KALIBRASI'],
            ['name' => 'yan-uji', 'display_name' => 'YAN PENGUJIAN', 'description' => 'YAN PENGUJIAN'],
            // MSB
            ['name' => 'msb-kalibrasi', 'display_name' => 'MSB KALIBRASI', 'description' => 'MSB KALIBRASI'],
            ['name' => 'msb-siskit', 'display_name' => 'MSB SISKIT', 'description' => 'MSB SISKIT'],
            ['name' => 'msb-sistgi', 'display_name' => 'MSB SISTGI', 'description' => 'MSB SISTGI'],
            ['name' => 'msb-tegangan-rendah', 'display_name' => 'MSB TEGANGAN RENDAH', 'description' => 'MSB TEGANGAN RENDAH'],
            ['name' => 'msb-tegangan-tinggi', 'display_name' => 'MSB TEGANGAN TINGGI', 'description' => 'MSB TEGANGAN TINGGI'],
            // Admin LAB
            ['name' => 'adminlab-kalibrasi', 'display_name' => 'ADMIN LAB KALIBRASI', 'description' => 'ADMIN LAB KALIBRASI'],
            ['name' => 'adminlab-siskit', 'display_name' => 'ADMIN LAB SISKIT', 'description' => 'ADMIN LAB SISKIT'],
            ['name' => 'adminlab-sistgi', 'display_name' => 'ADMIN LAB SISTGI', 'description' => 'ADMIN LAB SISTGI'],
            ['name' => 'adminlab-tegangan-rendah', 'display_name' => 'ADMIN LAB TEGANGAN RENDAH', 'description' => 'ADMIN LAB TEGANGAN RENDAH'],
            ['name' => 'adminlab-tegangan-tinggi', 'display_name' => 'ADMIN LAB TEGANGAN TINGGI', 'description' => 'ADMIN LAB TEGANGAN TINGGI'],
            // Asman DAL
            ['name' => 'asman-dal-kalibrasi', 'display_name' => 'ASMAN DAL KALIBRASI', 'description' => 'ASMAN DAL KALIBRASI'],
            ['name' => 'asman-dal-siskit', 'display_name' => 'ASMAN DAL SISKIT', 'description' => 'ASMAN DAL SISKIT'],
            ['name' => 'asman-dal-sistgi', 'display_name' => 'ASMAN DAL SISTGI', 'description' => 'ASMAN DAL SISTGI'],
            ['name' => 'asman-dal-tegangan-rendah', 'display_name' => 'ASMAN DAL TEGANGAN RENDAH', 'description' => 'ASMAN DAL TEGANGAN RENDAH'],
            ['name' => 'asman-dal-tegangan-tinggi', 'display_name' => 'ASMAN DAL TEGANGAN TINGGI', 'description' => 'ASMAN DAL TEGANGAN TINGGI'],
            // Asman LOLA
            ['name' => 'asman-lola-kalibrasi', 'display_name' => 'ASMAN LOLA KALIBRASI', 'description' => 'ASMAN LOLA KALIBRASI'],
            ['name' => 'asman-lola-siskit', 'display_name' => 'ASMAN LOLA SISKIT', 'description' => 'ASMAN LOLA SISKIT'],
            ['name' => 'asman-lola-sistgi', 'display_name' => 'ASMAN LOLA SISTGI', 'description' => 'ASMAN LOLA SISTGI'],
            ['name' => 'asman-lola-tegangan-rendah', 'display_name' => 'ASMAN LOLA TEGANGAN RENDAH', 'description' => 'ASMAN LOLA TEGANGAN RENDAH'],
            ['name' => 'asman-lola-tegangan-tinggi', 'display_name' => 'ASMAN LOLA TEGANGAN TINGGI', 'description' => 'ASMAN LOLA TEGANGAN TINGGI'],
            //PELAKSANA
            ['name' => 'pelaksana-kalibrasi', 'display_name' => 'PELAKSANA KALIBRASI', 'description' => 'PELAKSANA KALIBRASI'],
            ['name' => 'pelaksana-siskit', 'display_name' => 'PELAKSANA SISKIT', 'description' => 'PELAKSANA SISKIT'],
            ['name' => 'pelaksana-sistgi', 'display_name' => 'PELAKSANA SISTGI', 'description' => 'PELAKSANA SISTGI'],
            ['name' => 'pelaksana-tegangan-rendah', 'display_name' => 'PELAKSANA TEGANGAN RENDAH', 'description' => 'PELAKSANA TEGANGAN RENDAH'],
            ['name' => 'pelaksana-tegangan-tinggi', 'display_name' => 'PELAKSANA TEGANGAN TINGGI', 'description' => 'PELAKSANA TEGANGAN TINGGI'],
            
            ['name' => 'gm', 'display_name' => 'GM', 'description' => 'GM'],
            ['name' => 'srm-prosmkal', 'display_name' => 'SRM PROSMKAL', 'description' => 'SRM PROSMKAL'],
            ['name' => 'srm-uji', 'display_name' => 'SRM UJI', 'description' => 'SRM UJI'],
        ];

        $this->whopper($data);

    }

    public function whopper($data)
    {
        $this->command->getOutput()->progressStart(count($data));
        foreach ($data as $value) {
        	$exist = Role::where('name', $value['name'])
                           ->first();
            if(!$exist){
	            try {
	                $role_employee = new Role();
				    $role_employee->name = $value['name'];
				    $role_employee->display_name = $value['display_name'];
				    $role_employee->description = $value['description'];
				    $role_employee->save();
	            } catch ( Illuminate\Database\QueryException $e) {
	                //var_dump($e->errorInfo);
	            }
	        }
            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
    }
}
