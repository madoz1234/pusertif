<?php

use Illuminate\Database\Seeder;

class TranslatorSeeder extends Seeder
{
    public function run()
    {
    	$data = [
			['translator' => 'id', 'created_by' => '999'],
			['translator' => 'en', 'created_by' => '999']
        ];

        $this->whopper($data);
    }

    public function whopper($data)
    {
        $this->command->getOutput()->progressStart(count($data));
        foreach ($data as $value) {
            try {
                DB::table('sys_translator')->insert([
					'translator'    => $value['translator'],
					'created_by'    => $value['created_by'],
					'created_at'    => date('Y-m-d H:i:s')
                ]);
            } catch ( Illuminate\Database\QueryException $e) {
                //var_dump($e->errorInfo);
            }

            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
    }
}

