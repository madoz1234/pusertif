<?php

use Illuminate\Database\Seeder;

use App\Models\Authentication\User;
use App\Models\Authentication\Role;

class UserSeeder extends Seeder
{
    public function run()
    {
        $role_admin 		 	   			= Role::where('name', 'admin')->first();
        $role_user  		 	   			= Role::where('name', 'user')->first();
        $role_spm 		   		   			= Role::where('name', 'spm')->first();
        $role_keuangan 		   				= Role::where('name', 'keuangan')->first();

        // $role_pelaksana 		   			= Role::where('name', 'pelaksana')->first();
        // $role_pelaksana_leader 		   		= Role::where('name', 'pelaksana-leader')->first();

        //YAN
        $role_yan_kalibrasi  	   			= Role::where('name', 'yan-kalibrasi')->first();
        $role_yan_uji  		 	   			= Role::where('name', 'yan-uji')->first();
        //MSB
        $role_msb_kalibrasi  	   			= Role::where('name', 'msb-kalibrasi')->first();
        $role_msb_siskit  	 	   			= Role::where('name', 'msb-siskit')->first();
        $role_msb_sistgi  	 	   			= Role::where('name', 'msb-sistgi')->first();
        $role_msb_tegangan_rendah  			= Role::where('name', 'msb-tegangan-rendah')->first();
        $role_msb_tegangan_tinggi  			= Role::where('name', 'msb-tegangan-tinggi')->first();
        // ASMAN DAL
        $role_asman_dal_kalibrasi  	   		= Role::where('name', 'asman-dal-kalibrasi')->first();
        $role_asman_dal_siskit  	   		= Role::where('name', 'asman-dal-siskit')->first();
        $role_asman_dal_sistgi  	   		= Role::where('name', 'asman-dal-sistgi')->first();
        $role_asman_dal_tegangan_rendah  	= Role::where('name', 'asman-dal-tegangan-rendah')->first();
        $role_asman_dal_tegangan_tinggi  	= Role::where('name', 'asman-dal-tegangan-tinggi')->first();
        // ASMAN LOLA
        $role_asman_lola_kalibrasi  	   	= Role::where('name', 'asman-lola-kalibrasi')->first();
        $role_asman_lola_siskit  	   		= Role::where('name', 'asman-lola-siskit')->first();
        $role_asman_lola_sistgi  	   		= Role::where('name', 'asman-lola-sistgi')->first();
        $role_asman_lola_tegangan_rendah  	= Role::where('name', 'asman-lola-tegangan-rendah')->first();
        $role_asman_lola_tegangan_tinggi  	= Role::where('name', 'asman-lola-tegangan-tinggi')->first();
        // Admin LAB
        $role_adminlab_kalibrasi        	= Role::where('name', 'adminlab-kalibrasi')->first();
        $role_adminlab_siskit           	= Role::where('name', 'adminlab-siskit')->first();
        $role_adminlab_sistgi           	= Role::where('name', 'adminlab-sistgi')->first();
        $role_adminlab_tegangan_rendah  	= Role::where('name', 'adminlab-tegangan-rendah')->first();
        $role_adminlab_tegangan_tinggi  	= Role::where('name', 'adminlab-tegangan-tinggi')->first();
        //PELAKSANA
        $role_pelaksana_kalibrasi        	= Role::where('name', 'pelaksana-kalibrasi')->first();
        $role_pelaksana_siskit           	= Role::where('name', 'pelaksana-siskit')->first();
        $role_pelaksana_sistgi           	= Role::where('name', 'pelaksana-sistgi')->first();
        $role_pelaksana_tegangan_rendah  	= Role::where('name', 'pelaksana-tegangan-rendah')->first();
        $role_pelaksana_tegangan_tinggi  	= Role::where('name', 'pelaksana-tegangan-tinggi')->first();

        $role_gm           					= Role::where('name', 'gm')->first();
        $role_srm_prosmkal  				= Role::where('name', 'srm-prosmkal')->first();
        $role_srm_uji 						= Role::where('name', 'srm-uji')->first();

        $data = [
            ['username' => 'admin', 'email' => 'admin@gmail.com', 'nama' => 'Admin', 'role' => $role_admin],
            ['username' => 'SPM', 'email' => 'user.spm@gmail.com', 'nama' => 'SPM', 'role' => $role_spm],
            ['username' => 'n.risa@pln.co.id', 'email' => 'n.risa@pln.co.id', 'nama' => 'Risa', 'role' => $role_keuangan],
            //YAN
            ['username' => 'YAN Kalibrasi', 'email' => 'user.yan-kalibrasi@gmail.com', 'nama' => 'YAN Kalibrasi', 'role' => $role_yan_kalibrasi],
            ['username' => 'YAN Pengujian', 'email' => 'user.yan-uji@gmail.com', 'nama' => 'YAN Pengujian', 'role' => $role_yan_uji],
            //MSB
            //SISTGI
            ['username' => 'afip.nurul@pln.co.id', 'email' => 'afip.nurul@pln.co.id', 'nama' => 'M Afip Nurul Hudah', 'role' => $role_msb_sistgi],
            ['username' => 'ajeng.swasti@pln.co.id', 'email' => 'ajeng.swasti@pln.co.id', 'nama' => 'Ajeng Swasti Fisnaeni', 'role' => $role_asman_dal_sistgi],
            ['username' => 'eko.aptono@pln.co.id', 'email' => 'eko.aptono@pln.co.id', 'nama' => 'Eko Aptono Tri Yuwono', 'role' => $role_asman_lola_sistgi],
            ['username' => 'yuvita@pln.co.id', 'email' => 'yuvita@pln.co.id', 'nama' => 'Yuvita Radiantina', 'role' => $role_adminlab_sistgi],
            ['username' => 'm.said@pln.co.id', 'email' => 'm.said@pln.co.id', 'nama' => 'Muhammad Said Al Manshury', 'role' => $role_pelaksana_sistgi],
            ['username' => 'oki.andrean@pln.co.id', 'email' => 'oki.andrean@pln.co.id', 'nama' => 'Oki Andrean', 'role' => $role_pelaksana_sistgi],
            ['username' => 'armi.purwantara@pln.co.id', 'email' => 'armi.purwantara@pln.co.id', 'nama' => 'Armi Yudha Nugraha Purwantara', 'role' => $role_pelaksana_sistgi],
            ['username' => 'yohsafat.supono@pln.co.id', 'email' => 'yohsafat.supono@pln.co.id', 'nama' => 'Yohsafat Supono', 'role' => $role_pelaksana_sistgi],
            ['username' => 'andi.pradana@pln.co.id', 'email' => 'andi.pradana@pln.co.id', 'nama' => 'Andi Putra Pradana', 'role' => $role_pelaksana_sistgi],
            ['username' => 'agung.andreyansah@pln.co.id', 'email' => 'agung.andreyansah@pln.co.id', 'nama' => 'Agung Satria Andreyansah', 'role' => $role_pelaksana_sistgi],
            ['username' => 'luthfia.aizy@pln.co.id', 'email' => 'luthfia.aizy@pln.co.id', 'nama' => 'Luthfia Rohadhatul Aizy', 'role' => $role_pelaksana_sistgi],
            //KALIBRASI
            ['username' => 'ade.kurniawan@pln.co.id', 'email' => 'ade.kurniawan@pln.co.id', 'nama' => 'Ade Kurniawan', 'role' => $role_msb_kalibrasi],
            ['username' => 'jaya.putra@pln.co.id', 'email' => 'jaya.putra@pln.co.id', 'nama' => 'Jaya Pramana Putra', 'role' => $role_asman_dal_kalibrasi],
            ['username' => 'pugoh.ka@pln.co.id', 'email' => 'pugoh.ka@pln.co.id', 'nama' => 'Pugoh', 'role' => $role_asman_lola_kalibrasi],
            ['username' => 'pugoh.ka@pln.co.id', 'email' => 'pugoh.ka@pln.co.id', 'nama' => 'Pugoh', 'role' => $role_adminlab_kalibrasi],
            ['username' => 'florentinus.oktavian@pln.co.id', 'email' => 'florentinus.oktavian@pln.co.id', 'nama' => 'Florentinus Erick Oktavian', 'role' => $role_pelaksana_kalibrasi],
            ['username' => 'januar.fathoni@pln.co.id', 'email' => 'januar.fathoni@pln.co.id', 'nama' => 'M. Januar Fathoni', 'role' => $role_pelaksana_kalibrasi],
            ['username' => 'dera.ruliana@pln.co.id', 'email' => 'dera.ruliana@pln.co.id', 'nama' => 'Dera Ruliana', 'role' => $role_pelaksana_kalibrasi],
            ['username' => 'cartim@pln.co.id', 'email' => 'cartim@pln.co.id', 'nama' => 'Cartim', 'role' => $role_pelaksana_kalibrasi],
            //TEGANGAN TINGGI
            ['username' => 'aprianto.budi@pln.co.id', 'email' => 'aprianto.budi@pln.co.id', 'nama' => 'Afrianto Budi B.N', 'role' => $role_msb_tegangan_tinggi],
            ['username' => 'ratih.wanda@pln.co.id', 'email' => 'ratih.wanda@pln.co.id', 'nama' => 'Ratih Wandasari', 'role' => $role_asman_dal_tegangan_tinggi],
            ['username' => 'dedit.putro@pln.co.id', 'email' => 'dedit.putro@pln.co.id', 'nama' => 'Dedit Gunarso Putro', 'role' => $role_asman_lola_tegangan_tinggi],
            ['username' => 'niken.palupi2@pln.co.id', 'email' => 'niken.palupi2@pln.co.id', 'nama' => 'Lili Niken', 'role' => $role_adminlab_tegangan_tinggi],
            ['username' => 'henra.sibatuara@pln.co.id', 'email' => 'henra.sibatuara@pln.co.id', 'nama' => 'Henra Sibatuara', 'role' => $role_pelaksana_tegangan_tinggi],
            ['username' => 'puput.wijayanto@pln.co.id', 'email' => 'puput.wijayanto@pln.co.id', 'nama' => 'Puput Tri Wijayanto', 'role' => $role_pelaksana_tegangan_tinggi],
            ['username' => 'supriyatna3@pln.co.id', 'email' => 'supriyatna3@pln.co.id', 'nama' => 'Supriyatna', 'role' => $role_pelaksana_tegangan_tinggi],
            ['username' => 'luqman.hakim@pln.co.id', 'email' => 'luqman.hakim@pln.co.id', 'nama' => 'Luqman Hakim', 'role' => $role_pelaksana_tegangan_tinggi],
            ['username' => 'febi.permana@pln.co.id', 'email' => 'febi.permana@pln.co.id', 'nama' => 'Febi Hadi', 'role' => $role_pelaksana_tegangan_tinggi],
            ['username' => 'yusuf.sunadi@pln.co.id', 'email' => 'yusuf.sunadi@pln.co.id', 'nama' => 'Yusuf Bin Sunadi', 'role' => $role_pelaksana_tegangan_tinggi],
            ['username' => 'ana.wijayanti@pln.co.id', 'email' => 'ana.wijayanti@pln.co.id', 'nama' => 'Ana Tri Wijayanti', 'role' => $role_pelaksana_tegangan_tinggi],
            ['username' => 'setyo.awibowo@pln.co.id', 'email' => 'setyo.awibowo@pln.co.id', 'nama' => 'Setyo Aji', 'role' => $role_pelaksana_tegangan_tinggi],
            ['username' => 'ega.romadona@pln.co.id', 'email' => 'ega.romadona@pln.co.id', 'nama' => 'Ega Yusuf', 'role' => $role_pelaksana_tegangan_tinggi],
            ['username' => 'ade.gumilar@pln.co.id', 'email' => 'ade.gumilar@pln.co.id', 'nama' => 'Ade Gumilar', 'role' => $role_pelaksana_tegangan_tinggi],
            ['username' => 'andika.sanjaya@pln.co.id', 'email' => 'andika.sanjaya@pln.co.id', 'nama' => 'Andika Sanjaya', 'role' => $role_pelaksana_tegangan_tinggi],
            ['username' => 'dyan.ariefianto@pln.co.id', 'email' => 'dyan.ariefianto@pln.co.id', 'nama' => 'Dyan Ariefianto', 'role' => $role_pelaksana_tegangan_tinggi],
            ['username' => 'sukerdi@pln.co.id', 'email' => 'sukerdi@pln.co.id', 'nama' => 'Sukerdi', 'role' => $role_pelaksana_tegangan_tinggi],
            ['username' => 'boby.dermawan@pln.co.id', 'email' => 'boby.dermawan@pln.co.id', 'nama' => 'Boby Dermawan', 'role' => $role_pelaksana_tegangan_tinggi],
            ['username' => 'fajrinao@pln.co.id', 'email' => 'fajrinao@pln.co.id', 'nama' => 'Fajrina', 'role' => $role_pelaksana_tegangan_tinggi],
            // SISKIT
            ['username' => 'agus.warsyun@pln.co.id', 'email' => 'agus.warsyun@pln.co.id', 'nama' => 'Moh. Agoes Warsyun', 'role' => $role_msb_siskit],
            ['username' => 'ariandiky.setyawan@pln.co.id', 'email' => 'ariandiky.setyawan@pln.co.id', 'nama' => 'Ariandiky Eko Setyawan', 'role' => $role_asman_dal_siskit],
            ['username' => 'dikky.anggara@pln.co.id', 'email' => 'dikky.anggara@pln.co.id', 'nama' => 'Dikky Dwi Anggara', 'role' => $role_asman_lola_siskit],
            ['username' => 'nicky.salsabilah@pln.co.id', 'email' => 'nicky.salsabilah@pln.co.id', 'nama' => 'Nicky Serfirah Azka Salsabil', 'role' => $role_adminlab_siskit],
            ['username' => 'cahyo.singgih@pln.co.id', 'email' => 'cahyo.singgih@pln.co.id', 'nama' => 'Cahyo Singgih Kuntoro Aji', 'role' => $role_pelaksana_siskit],
			['username' => 'hadi.nur@pln.co.id', 'email' => 'hadi.nur@pln.co.id', 'nama' => 'Hadi Nurcahyo', 'role' => $role_pelaksana_siskit],
			['username' => 'nur.ahmad@pln.co.id', 'email' => 'nur.ahmad@pln.co.id', 'nama' => 'Nur Achmad Busairi', 'role' => $role_pelaksana_siskit],
			['username' => 'adi.setyono@pln.co.id', 'email' => 'adi.setyono@pln.co.id', 'nama' => 'Adi Setyono', 'role' => $role_pelaksana_siskit],
			['username' => 'radhitya.wahyu@pln.co.id', 'email' => 'radhitya.wahyu@pln.co.id', 'nama' => 'Radhitya Wahyu Jatmiko', 'role' => $role_pelaksana_siskit],
			['username' => 'bagus.panji@pln.co.id', 'email' => 'bagus.panji@pln.co.id', 'nama' => 'Bagus Panji Asmoro', 'role' => $role_pelaksana_siskit],
			['username' => 'sugianto94@pln.co.id', 'email' => 'sugianto94@pln.co.id', 'nama' => 'Sugianto', 'role' => $role_pelaksana_siskit],
			['username' => 'dede.mubarok@pln.co.id', 'email' => 'dede.mubarok@pln.co.id', 'nama' => 'Dede Mubarok', 'role' => $role_pelaksana_siskit],
			['username' => 'haryadi@pln.co.id', 'email' => 'haryadi@pln.co.id', 'nama' => 'Haryadi', 'role' => $role_pelaksana_siskit],
			['username' => 'gariki.putri@pln.co.id', 'email' => 'gariki.putri@pln.co.id', 'nama' => 'Gariki Putri', 'role' => $role_pelaksana_siskit],
			['username' => 'maulia.eka@pln.co.id', 'email' => 'maulia.eka@pln.co.id', 'nama' => 'Maulia Eka Septiana', 'role' => $role_pelaksana_siskit],
			['username' => 'suparta3@pln.co.id', 'email' => 'suparta3@pln.co.id', 'nama' => 'Suparta', 'role' => $role_pelaksana_siskit],
			['username' => 'hendra.budiono@pln.co.id', 'email' => 'hendra.budiono@pln.co.id', 'nama' => 'Hendra Budiono Putra Parap', 'role' => $role_pelaksana_siskit],
			['username' => 'sumedi1@pln.co.id', 'email' => 'sumedi1@pln.co.id', 'nama' => 'Sumedi', 'role' => $role_pelaksana_siskit],
			['username' => 'nina.andiastari@pln.co.id', 'email' => 'nina.andiastari@pln.co.id', 'nama' => 'Nina Andiastari', 'role' => $role_pelaksana_siskit],
			['username' => 'agus.salim5@pln.co.id', 'email' => 'agus.salim5@pln.co.id', 'nama' => 'Agus Salim', 'role' => $role_pelaksana_siskit],
			['username' => 'sandy.ataroka@pln.co.id', 'email' => 'sandy.ataroka@pln.co.id', 'nama' => 'Sandy Ataroka', 'role' => $role_pelaksana_siskit],
			['username' => 'reihan.alhabsi@pln.co.id', 'email' => 'reihan.alhabsi@pln.co.id', 'nama' => 'Reihan Alhabsi', 'role' => $role_pelaksana_siskit],
			['username' => 'satriyo.hadi@pln.co.id', 'email' => 'satriyo.hadi@pln.co.id', 'nama' => 'Satriyo Hadi Mishandoko', 'role' => $role_pelaksana_siskit],
			['username' => 'winda.fitriah@pln.co.id', 'email' => 'winda.fitriah@pln.co.id', 'nama' => 'Winda Fitriah Astono', 'role' => $role_pelaksana_siskit],
			['username' => 'indra.kusdwiatmaja@pln.co.id', 'email' => 'indra.kusdwiatmaja@pln.co.id', 'nama' => 'Indra Kusdwiatmaja', 'role' => $role_pelaksana_siskit],
			['username' => 'zulfikar.kuswayan@pln.co.id', 'email' => 'zulfikar.kuswayan@pln.co.id', 'nama' => 'Zulfikar Jaya Kuswayan', 'role' => $role_pelaksana_siskit],
			['username' => 'prasetyo.anugroho@pln.co.id', 'email' => 'prasetyo.anugroho@pln.co.id', 'nama' => 'Prasetyo Adi Nugroho', 'role' => $role_pelaksana_siskit],
			['username' => 'wida.betasani@pln.co.id', 'email' => 'wida.betasani@pln.co.id', 'nama' => 'Wida Betasani', 'role' => $role_pelaksana_siskit],
			['username' => 'yus.taufika@pln.co.id', 'email' => 'yus.taufika@pln.co.id', 'nama' => 'Yus Satria Taufika', 'role' => $role_pelaksana_siskit],
			['username' => 'winda.kurniawati@pln.co.id', 'email' => 'winda.kurniawati@pln.co.id', 'nama' => 'Winda Kurniawati', 'role' => $role_pelaksana_siskit],
			['username' => 'anwar.rusmana@pln.co.id', 'email' => 'anwar.rusmana@pln.co.id', 'nama' => 'Anwar Rusmana', 'role' => $role_pelaksana_siskit],
			['username' => 'fajar.rajendra@pln.co.id', 'email' => 'fajar.rajendra@pln.co.id', 'nama' => 'Fajar Cahyadi Rajendra', 'role' => $role_pelaksana_siskit],
			['username' => 'wisnu.prasetya@pln.co.id', 'email' => 'wisnu.prasetya@pln.co.id', 'nama' => 'Wisnu Yoga Prasetya', 'role' => $role_pelaksana_siskit],
			['username' => 'budiyanto4@pln.co.id', 'email' => 'budiyanto4@pln.co.id', 'nama' => 'Budiyanto', 'role' => $role_pelaksana_siskit],
			['username' => 'riki.atmanu@pln.co.id', 'email' => 'riki.atmanu@pln.co.id', 'nama' => 'Riki Atmanu', 'role' => $role_pelaksana_siskit],
			['username' => 'agung.prayogo@pln.co.id', 'email' => 'agung.prayogo@pln.co.id', 'nama' => 'Agung Tri Prayogo', 'role' => $role_pelaksana_siskit],
			// TEGANGAN RENDAH
            ['username' => 'rendy.yuliandhika@pln.co.id', 'email' => 'rendy.yuliandhika@pln.co.id', 'nama' => 'Rendy Yuliandhika', 'role' => $role_msb_tegangan_rendah],
            ['username' => 'kridia@pln.co.id', 'email' => 'kridia@pln.co.id', 'nama' => 'Kridia Agus Burhani', 'role' => $role_asman_dal_tegangan_rendah],
            ['username' => 'bobby.joeadams@pln.co.id', 'email' => 'bobby.joeadams@pln.co.id', 'nama' => 'Bobby Joe Adams', 'role' => $role_asman_lola_tegangan_rendah],
            ['username' => 'soraya.belladina@pln.co.id', 'email' => 'soraya.belladina@pln.co.id', 'nama' => 'Soraya Bella Dina', 'role' => $role_adminlab_tegangan_rendah],
            ['username' => 'ifan.randi@pln.co.id', 'email' => 'ifan.randi@pln.co.id', 'nama' => 'Ifan Randi Pratama', 'role' => $role_pelaksana_tegangan_rendah],
			['username' => 'nurcahyo.paryoso@pln.co.id', 'email' => 'nurcahyo.paryoso@pln.co.id', 'nama' => 'Nurcahyo Paryoso', 'role' => $role_pelaksana_tegangan_rendah],
			['username' => 'ongky.ganesia@pln.co.id', 'email' => 'ongky.ganesia@pln.co.id', 'nama' => 'Ongky Ganesia', 'role' => $role_pelaksana_tegangan_rendah],
			['username' => 'bondhan.tejobaskoro@pln.co.id', 'email' => 'bondhan.tejobaskoro@pln.co.id', 'nama' => 'Bondhan Aji Tejobaskoro', 'role' => $role_pelaksana_tegangan_rendah],
			['username' => 'kagum.bina@pln.co.id', 'email' => 'kagum.bina@pln.co.id', 'nama' => 'Kagum Bina Ahzab', 'role' => $role_pelaksana_tegangan_rendah],
			['username' => 'faritzi.alqornie@pln.co.id', 'email' => 'faritzi.alqornie@pln.co.id', 'nama' => 'Faritzi Wais Alqornie', 'role' => $role_pelaksana_tegangan_rendah],
			['username' => 'novianto.putra@pln.co.id', 'email' => 'novianto.putra@pln.co.id', 'nama' => 'Novianto Qaedi Putra', 'role' => $role_pelaksana_tegangan_rendah],
			['username' => 'theo.rachmadi@pln.co.id', 'email' => 'theo.rachmadi@pln.co.id', 'nama' => 'Theo Rachmadi', 'role' => $role_pelaksana_tegangan_rendah],
			['username' => 'swm.tajanwar@gmail.com', 'email' => 'swm.tajanwar@gmail.com', 'nama' => 'Tajanwar Pradana Swm', 'role' => $role_pelaksana_tegangan_rendah],
			['username' => 'luth.bhaskara@pln.co.id', 'email' => 'luth.bhaskara@pln.co.id', 'nama' => 'Luth Bhaskara', 'role' => $role_pelaksana_tegangan_rendah],
			['username' => 'yusrizal.kahfidya@pln.co.id', 'email' => 'yusrizal.kahfidya@pln.co.id', 'nama' => 'Yusrizal Kahfidya', 'role' => $role_pelaksana_tegangan_rendah],
			//GM
			['username' => 'heru.sriwidodosari@pln.co.id', 'email' => 'heru.sriwidodosari@pln.co.id', 'nama' => 'Heru S', 'role' => $role_gm],
			//SRM PROSMKAL
			['username' => 'edy.junaidi@pln.co.id', 'email' => 'edy.junaidi@pln.co.id', 'nama' => 'Edy Junaidi', 'role' => $role_srm_prosmkal],
			//SRM UJI
			['username' => 'haryo.lukito@pln.co.id', 'email' => 'haryo.lukito@pln.co.id', 'nama' => 'Haryo Lukito', 'role' => $role_srm_uji],
        ];

        $this->whopper($data);

    }

    public function whopper($data)
    {
        $this->command->getOutput()->progressStart(count($data));
        foreach ($data as $value) {
            $exist = User::where('email', $value['email'])
                           ->first();
            if(!$exist){
                try {
                    $user             = new User();
                    $user->username   = $value['username'];
                    $user->password   = bcrypt('password');
                    $user->email      = $value['email'];
                    $user->last_login = date('Y-m-d H:i:s');
                    $user->nama       = $value['nama'];
                    $user->save();
                    $user->roles()->attach($value['role']);
                } catch ( Illuminate\Database\QueryException $e) {
                    //var_dump($e->errorInfo);
                }
            }else{
                $exist->username   = $value['username'];
                $exist->password   = bcrypt('password');
                $exist->email      = $value['email'];
                $exist->last_login = date('Y-m-d H:i:s');
                $exist->nama       = $value['nama'];
                $exist->save();
                $exist->roles()->attach($value['role']);
            }
            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
    }
}

