<?php

use Illuminate\Database\Seeder;

class PerusahaanSeeder extends Seeder
{
    public function run()
    {
        $data = [
            ['nama' => 'PT. PRAGMA INFORMATIKA','kode' => '0001','no_tlp' => '(021) 7900034','alamat' => 'Bandung','status' => '1','kategori' => '1','email' => 'pragma@gmail.com'],
            ['nama' => 'PT. MAJU JAYA','kode' => '0002','no_tlp' => '(021) 7900034','alamat' => 'Jakarta Barat','status' => '1','kategori' => '1','email' => 'majujaya@gmail.com'],
            ['nama' => 'PT. BERKAH JAYA','kode' => '0003','no_tlp' => '(021) 7900034','alamat' => 'Surabaya','status' => '1','kategori' => '1','email' => 'berkahjaya@gmail.com'],
            ['nama' => 'PT. BERKAH MAKMUR','kode' => '0004','no_tlp' => '(021) 7900034','alamat' => 'Malang','status' => '1','kategori' => '1','email' => 'berkahmakmur@gmail.com'],
            ['nama' => 'PT. ANGKARA MURKA','kode' => '0005','no_tlp' => '(021) 7900034','alamat' => 'Bogor','status' => '1','kategori' => '1','email' => 'angkaramurka@gmail.com'],
            ['nama' => 'PT. INDAH JAYA','kode' => '0006','no_tlp' => '(021) 7900034','alamat' => 'Tanggerang','status' => '1','kategori' => '1','email' => 'indahjaya@gmail.com']
        ];

        $this->whopper($data);
    }

    public function whopper($data)
    {
        $this->command->getOutput()->progressStart(count($data));
        foreach ($data as $value) {
            try {
                DB::table('ref_perusahaan')->insert([
                    'nama'          => $value['nama'],
                    'kode'          => $value['kode'],
                    'no_tlp'        => $value['no_tlp'],
                    'alamat'        => $value['alamat'],
                    'status'        => $value['status'],
                    'kategori'      => $value['kategori'],
                    'email'         => $value['email'],
                    'created_by'    => '1',
                    'created_at'    => date('Y-m-d H:i:s')
                ]);
            } catch ( Illuminate\Database\QueryException $e) {
                //var_dump($e->errorInfo);
            }

            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
    }
}
