<?php

use Illuminate\Database\Seeder;

class PrioritasSeeder extends Seeder
{
    public function run()
    {
    	$data = [
			['layanan_id' => '1','waktu' => '3'],
			['layanan_id' => '2','waktu' => '4'],
			['layanan_id' => '3','waktu' => '4'],
			['layanan_id' => '4','waktu' => '5'],
			['layanan_id' => '5','waktu' => '5']
        ];

        $this->whopper($data);
    }

    public function whopper($data)
    {
        $this->command->getOutput()->progressStart(count($data));
        foreach ($data as $value) {
            try {
                DB::table('ref_prioritas')->insert([
					'layanan_id'          => $value['layanan_id'],
					'waktu'          	  => $value['waktu'],
					'created_by'   	 	  => '1',
					'created_at'   		  => date('Y-m-d H:i:s')
                ]);
            } catch ( Illuminate\Database\QueryException $e) {
                //var_dump($e->errorInfo);
            }

            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
    }
}

