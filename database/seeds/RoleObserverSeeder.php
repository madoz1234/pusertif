<?php

use Illuminate\Database\Seeder;
use App\Models\Authentication\Role;


class RoleObserverSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [
            ['name' => 'observer-prosmkal', 'display_name' => 'OBSERVER PROSMKAL', 'description' => 'OBSERVER PROSMKAL'],
            ['name' => 'observer-uji', 'display_name' => 'OBSERVER UJI', 'description' => 'OBSERVER UJI'],
        ];

        $this->whopper($data);

    }

    public function whopper($data)
    {
        $this->command->getOutput()->progressStart(count($data));
        foreach ($data as $value) {
        	$exist = Role::where('name', $value['name'])
                           ->first();
            if(!$exist){
	            try {
	                $role_employee = new Role();
				    $role_employee->name = $value['name'];
				    $role_employee->display_name = $value['display_name'];
				    $role_employee->description = $value['description'];
				    $role_employee->save();
	            } catch ( Illuminate\Database\QueryException $e) {
	                //var_dump($e->errorInfo);
	            }
	        }
            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
    }
}
