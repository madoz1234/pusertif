<?php

use Illuminate\Database\Seeder;

class LingkupSeeder extends Seeder
{
    public function run()
    {
    	$data = [
			['jenis_pelayanan_id' => '1', 'nama' => 'Listrik'],
			['jenis_pelayanan_id' => '1', 'nama' => 'Non-Listrik'],
			['jenis_pelayanan_id' => '2', 'nama' => 'Sipil'],
			['jenis_pelayanan_id' => '2', 'nama' => 'Kimia'],
			['jenis_pelayanan_id' => '2', 'nama' => 'Material'],
			['jenis_pelayanan_id' => '2', 'nama' => 'Efisiensi Pembangkit'],
			['jenis_pelayanan_id' => '2', 'nama' => 'Energi Primer dan Terbarukan'],
			['jenis_pelayanan_id' => '2', 'nama' => 'Continous Based Maintenance (CBM)'],
			['jenis_pelayanan_id' => '3', 'nama' => 'Proteksi'],
			['jenis_pelayanan_id' => '3', 'nama' => 'SCADA'],
			['jenis_pelayanan_id' => '3', 'nama' => 'Instalasi dan Assesment'],
			['jenis_pelayanan_id' => '3', 'nama' => 'Power System'],
			['jenis_pelayanan_id' => '4', 'nama' => 'Uji Jenis'],
			['jenis_pelayanan_id' => '4', 'nama' => 'Uji Verifikasi'],
			['jenis_pelayanan_id' => '4', 'nama' => 'Uji Karakteristik'],
			['jenis_pelayanan_id' => '4', 'nama' => 'Uji Serah Terima'],
			['jenis_pelayanan_id' => '4', 'nama' => 'Uji Pengawasan'],
			['jenis_pelayanan_id' => '5', 'nama' => 'Uji Jenis'],
			['jenis_pelayanan_id' => '5', 'nama' => 'Uji Verifikasi'],
			['jenis_pelayanan_id' => '5', 'nama' => 'Uji Karakteristik'],
			['jenis_pelayanan_id' => '5', 'nama' => 'Uji Serah Terima'],
			['jenis_pelayanan_id' => '5', 'nama' => 'Uji Pengawasan'],
			['jenis_pelayanan_id' => '5', 'nama' => 'Uji Rutin'],
        ];

        $this->whopper($data);
    }

    public function whopper($data)
    {
        $this->command->getOutput()->progressStart(count($data));
        foreach ($data as $value) {
            try {
                DB::table('ref_lingkup')->insert([
					'jenis_pelayanan_id'    => $value['jenis_pelayanan_id'],
					'nama'          		=> $value['nama'],
					'created_by'    		=> '1',
					'created_at'    		=> date('Y-m-d H:i:s')
                ]);
            } catch ( Illuminate\Database\QueryException $e) {
                //var_dump($e->errorInfo);
            }

            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
    }
}

