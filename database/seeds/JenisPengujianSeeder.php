<?php

use Illuminate\Database\Seeder;

class JenisPengujianSeeder extends Seeder
{
    public function run()
    {
    	$data = [
			['lingkup_id' => '1', 'nama' => 'KWH III Phase', 'deskripsi' => '-'],
			['lingkup_id' => '1', 'nama' => 'Amperemeter', 'deskripsi' => '-'],
			['lingkup_id' => '2', 'nama' => 'Flow Meter', 'deskripsi' => '-'],
			['lingkup_id' => '3', 'nama' => 'Tower', 'deskripsi' => '-'],
			['lingkup_id' => '4', 'nama' => 'Minyak Trafo', 'deskripsi' => '-'],
			['lingkup_id' => '5', 'nama' => 'Pipa Boiler', 'deskripsi' => '-'],
			['lingkup_id' => '6', 'nama' => 'Heat Rate', 'deskripsi' => '-'],
			['lingkup_id' => '7', 'nama' => 'Batubara', 'deskripsi' => '-'],
			['lingkup_id' => '8', 'nama' => 'PLTD', 'deskripsi' => '-'],
			['lingkup_id' => '9', 'nama' => 'OCR', 'deskripsi' => '-'],
			['lingkup_id' => '10', 'nama' => 'RTU', 'deskripsi' => '-'],
			['lingkup_id' => '11', 'nama' => 'Assesment', 'deskripsi' => '-'],
			['lingkup_id' => '12', 'nama' => 'Pemodelan', 'deskripsi' => '-'],
			// ['lingkup_id' => '13', 'nama' => 'Kabel 1', 'deskripsi' => '-'],
			// ['lingkup_id' => '14', 'nama' => 'Kabel 2', 'deskripsi' => '-'],
			// ['lingkup_id' => '15', 'nama' => 'Kabel 3', 'deskripsi' => '-'],
			// ['lingkup_id' => '16', 'nama' => 'Kabel 4', 'deskripsi' => '-'],
			// ['lingkup_id' => '17', 'nama' => 'Kabel 5', 'deskripsi' => '-'],
			// ['lingkup_id' => '18', 'nama' => 'Kabel 1', 'deskripsi' => '-'],
			// ['lingkup_id' => '19', 'nama' => 'Kabel 2', 'deskripsi' => '-'],
			// ['lingkup_id' => '20', 'nama' => 'Kabel 3', 'deskripsi' => '-'],
			// ['lingkup_id' => '21', 'nama' => 'Kabel 4', 'deskripsi' => '-'],
			// ['lingkup_id' => '22', 'nama' => 'Kabel 5', 'deskripsi' => '-'],
			['lingkup_id' => '3', 'nama' => 'Site Assesment', 'deskripsi' => '-'],
			['lingkup_id' => '3', 'nama' => 'Pengujian Laboratorium', 'deskripsi' => '-'],
			['lingkup_id' => '4', 'nama' => 'Minyak Trafo Baru', 'deskripsi' => '-'],
			['lingkup_id' => '4', 'nama' => 'Minyak Trafo Pakai', 'deskripsi' => '-'],
			['lingkup_id' => '4', 'nama' => 'Minyak Trafo Kabel', 'deskripsi' => '-'],
			['lingkup_id' => '4', 'nama' => 'Assesment Siklus Kimia dan Uap Air', 'deskripsi' => '-'],
			['lingkup_id' => '5', 'nama' => 'Site Assesment', 'deskripsi' => '-'],
			['lingkup_id' => '5', 'nama' => 'Pengujian Laboratorium', 'deskripsi' => '-'],
			['lingkup_id' => '6', 'nama' => 'Spesific Fuel Consumption', 'deskripsi' => '-'],
			['lingkup_id' => '7', 'nama' => 'Pengujian Energi Primer', 'deskripsi' => '-'],
			['lingkup_id' => '7', 'nama' => 'Pengujian Energi Terbarukan', 'deskripsi' => '-'],
			['lingkup_id' => '8', 'nama' => 'Aset Wellnes', 'deskripsi' => '-'],
			['lingkup_id' => '8', 'nama' => 'Balancing', 'deskripsi' => '-'],
			['lingkup_id' => '8', 'nama' => 'Laser Alignment', 'deskripsi' => '-'],
			['lingkup_id' => '9', 'nama' => 'RTDS Baru', 'deskripsi' => '-'],
			['lingkup_id' => '9', 'nama' => 'RTDS Perpanjangan', 'deskripsi' => '-'],
			['lingkup_id' => '9', 'nama' => 'Fungsional', 'deskripsi' => '-'],
			['lingkup_id' => '9', 'nama' => 'Lapangan', 'deskripsi' => '-'],
			['lingkup_id' => '10', 'nama' => 'Baru', 'deskripsi' => '-'],
			['lingkup_id' => '10', 'nama' => 'Perpanjangan', 'deskripsi' => '-'],
			['lingkup_id' => '10', 'nama' => 'Lapangan', 'deskripsi' => '-'],
			['lingkup_id' => '11', 'nama' => 'Lapangan', 'deskripsi' => '-'],
			['lingkup_id' => '12', 'nama' => 'Defense Scheme', 'deskripsi' => '-'],
			['lingkup_id' => '12', 'nama' => 'Koordinasi Setting Proteksi Transmisi/Distribusi', 'deskripsi' => '-'],
			['lingkup_id' => '12', 'nama' => 'Stability', 'deskripsi' => '-'],
			['lingkup_id' => '13', 'nama' => 'Uji Lengkap', 'deskripsi' => '-'],
			['lingkup_id' => '14', 'nama' => 'Perpanjangan', 'deskripsi' => '-'],
			['lingkup_id' => '14', 'nama' => 'Audit Khusus', 'deskripsi' => '-'],
			['lingkup_id' => '14', 'nama' => 'Pengawasan', 'deskripsi' => '-'],
			['lingkup_id' => '15', 'nama' => 'Uji Karakteristik', 'deskripsi' => '-'],
			['lingkup_id' => '15', 'nama' => 'Tertentu', 'deskripsi' => '-'],
			['lingkup_id' => '16', 'nama' => 'Uji Serah Terima', 'deskripsi' => '-'],
			['lingkup_id' => '16', 'nama' => 'Rutin', 'deskripsi' => '-'],
			['lingkup_id' => '18', 'nama' => 'Uji Jenis', 'deskripsi' => '-'],
			['lingkup_id' => '19', 'nama' => 'Perpanjangan', 'deskripsi' => '-'],
			['lingkup_id' => '19', 'nama' => 'Audit Khusus', 'deskripsi' => '-'],
			['lingkup_id' => '19', 'nama' => 'Pengawasan', 'deskripsi' => '-'],
			['lingkup_id' => '21', 'nama' => 'Uji Serah Terima', 'deskripsi' => '-'],
			['lingkup_id' => '23', 'nama' => 'PDKB', 'deskripsi' => '-']
        ];

        $this->whopper($data);
    }

    public function whopper($data)
    {
        $this->command->getOutput()->progressStart(count($data));
        foreach ($data as $value) {
            try {
                DB::table('ref_jenis_pengujian')->insert([
					'lingkup_id'    		=> $value['lingkup_id'],
					'nama'          		=> $value['nama'],
					'deskripsi'          	=> $value['deskripsi'],
					'created_by'    		=> '1',
					'created_at'    		=> date('Y-m-d H:i:s')
                ]);
            } catch ( Illuminate\Database\QueryException $e) {
                //var_dump($e->errorInfo);
            }

            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
    }
}

