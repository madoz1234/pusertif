<?php

use Illuminate\Database\Seeder;

class PelayananSeeder extends Seeder
{
    public function run()
    {
    	$data = [
			['nama' => 'Kalibrasi', 'kode' => '16', 'seri' => '1'],
			['nama' => 'Pengujian Sistem Pembangkitan (SISKIT)', 'kode' => '12', 'seri' => '1'],
			['nama' => 'Pengujian Sistem Transmisi Gardu Induk (SISTGI)', 'kode' => '13', 'seri' => '1'],
			['nama' => 'Pengujian Produk Tegangan Rendah', 'kode' => '14', 'seri' => '1'],
			['nama' => 'Pengujian Produk Tegangan Tinggi', 'kode' => '15', 'seri' => '1']
        ];

        $this->whopper($data);
    }

    public function whopper($data)
    {
        $this->command->getOutput()->progressStart(count($data));
        foreach ($data as $value) {
            try {
                DB::table('ref_jenis_pelayanan')->insert([
					'nama'          => $value['nama'],
					'kode'          => $value['kode'],
					'seri'          => $value['seri'],
					'created_by'    => '1',
					'created_at'    => date('Y-m-d H:i:s')
                ]);
            } catch ( Illuminate\Database\QueryException $e) {
                //var_dump($e->errorInfo);
            }

            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
    }
}

