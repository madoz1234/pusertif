<?php

use Illuminate\Database\Seeder;
use App\Models\Authentication\Role;
use App\Models\Authentication\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin 		 	   			= Role::where('name', 'admin')->first();
        $role_user  		 	   			= Role::where('name', 'user')->first();
        $role_spm  		   		   			= Role::where('name', 'spm')->first();
        $role_keuangan  		   		   	= Role::where('name', 'keuangan')->first();
        // $role_pelaksana  		   		   	= Role::where('name', 'pelaksana')->first();
        // $role_pelaksana_leader  		   	= Role::where('name', 'pelaksana-leader')->first();
        // YAN
        $role_yan_kalibrasi  	   			= Role::where('name', 'yan-kalibrasi')->first();
        $role_yan_uji  		 	   			= Role::where('name', 'yan-uji')->first();
        //MSB
        $role_msb_kalibrasi  	   			= Role::where('name', 'msb-kalibrasi')->first();
        $role_msb_siskit  	 	   			= Role::where('name', 'msb-siskit')->first();
        $role_msb_sistgi  	 	   			= Role::where('name', 'msb-sistgi')->first();
        $role_msb_tegangan_rendah  			= Role::where('name', 'msb-tegangan-rendah')->first();
        $role_msb_tegangan_tinggi  			= Role::where('name', 'msb-tegangan-tinggi')->first();
        //ASMAN DAL
        $role_asman_dal_kalibrasi  	   		= Role::where('name', 'asman-dal-kalibrasi')->first();
        $role_asman_dal_siskit  	   		= Role::where('name', 'asman-dal-siskit')->first();
        $role_asman_dal_sistgi  	   		= Role::where('name', 'asman-dal-sistgi')->first();
        $role_asman_dal_tegangan_rendah  	= Role::where('name', 'asman-dal-tegangan-rendah')->first();
        $role_asman_dal_tegangan_tinggi  	= Role::where('name', 'asman-dal-tegangan-tinggi')->first();
        //ASMAN LOLA
        $role_asman_lola_kalibrasi  	   	= Role::where('name', 'asman-lola-kalibrasi')->first();
        $role_asman_lola_siskit  	   		= Role::where('name', 'asman-lola-siskit')->first();
        $role_asman_lola_sistgi  	   		= Role::where('name', 'asman-lola-sistgi')->first();
        $role_asman_lola_tegangan_rendah  	= Role::where('name', 'asman-lola-tegangan-rendah')->first();
        $role_asman_lola_tegangan_tinggi  	= Role::where('name', 'asman-lola-tegangan-tinggi')->first();
        //ADMIN LAB
        $role_adminlab_kalibrasi            = Role::where('name', 'adminlab-kalibrasi')->first();
        $role_adminlab_siskit               = Role::where('name', 'adminlab-siskit')->first();
        $role_adminlab_sistgi               = Role::where('name', 'adminlab-sistgi')->first();
        $role_adminlab_tegangan_rendah      = Role::where('name', 'adminlab-tegangan-rendah')->first();
        $role_adminlab_tegangan_tinggi      = Role::where('name', 'adminlab-tegangan-tinggi')->first();

        //PELAKSANA
       	$role_pelaksana_kalibrasi           = Role::where('name', 'pelaksana-kalibrasi')->first();
       	$role_pelaksana_siskit           	= Role::where('name', 'pelaksana-siskit')->first();
       	$role_pelaksana_sistgi           	= Role::where('name', 'pelaksana-sistgi')->first();
       	$role_pelaksana_tegangan_rendah     = Role::where('name', 'pelaksana-tegangan-rendah')->first();
       	$role_pelaksana_tegangan_tinggi     = Role::where('name', 'pelaksana-tegangan-tinggi')->first();

       	$role_gm     						= Role::where('name', 'gm')->first();
       	$role_srm_prosmkal     				= Role::where('name', 'srm-prosmkal')->first();
       	$role_srm_uji     					= Role::where('name', 'srm-uji')->first();
    	
    	// create permissions
        // DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // DB::table('sys_permission_role')->truncate();
        // DB::table('sys_permissions')->truncate();
        // DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    	
    	$permissions = [
    		// ------------- DASHBOARD ---------------
    		[
				'name'         => 'dashboard',
				'display_name' => 'Dashboard',
				'action'       => ['view'],
    		],

            // ------------- PENERIMAAN SURAT ---------------
            [
                'name'         => 'penerimaan-surat',
                'display_name' => 'Penerimaan Surat',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],

            // ------------- Surat Penawaran ---------------
            [
                'name'         => 'surat-penawaran', 
                'display_name' => 'Surat Penawaran',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],

            // ------------- Kaji Ulang ---------------
            [
                'name'         => 'kaji-ulang',
                'display_name' => 'Kaji Ulang',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],

             // ------------- KONFIRMASI PEMBAYARAN ---------------
            [
                'name'         => 'konfirmasi-pembayaran',
                'display_name' => 'Konfirmasi Pembayaran',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],

             // ------------- RELISASI BIAYA ---------------
            [
                'name'         => 'relisasi-biaya',
                'display_name' => 'Relisasi Biaya',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],

            // ------------- Virtual Account ---------------
            [
                'name'         => 'virtual-account',
                'display_name' => 'Virtual Account (VA)',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],

            // ------------- Nota Buku ---------------
            [
                'name'         => 'nota-dinas',
                'display_name' => 'Nota Buku',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
            // ------------- Pengembalian Dana ---------------
            [
                'name'         => 'pengembalian-dana',
                'display_name' => 'Pengembalian Dana',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
            // ------------- Penerimaan Barang Uji ---------------
            [
                'name'         => 'penerimaan-barang-uji',
                'display_name' => 'Penerimaan Barang Uji',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
            // ------------- Penyerahan Barang Uji ---------------
            [
                'name'         => 'pengembalian-barang-uji',
                'display_name' => 'Pengembalian Barang Uji',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],

            // ------------- Reschedule ---------------
            [
                'name'         => 'reschedule',
                'display_name' => 'Reschedule',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],

            // ------------- Pengujian Serah Terima ---------------
            [
                'name'         => 'pengujian-serah-terima',
                'display_name' => 'Pengujian Serah Terima',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],

            // ------------- Pengujian ---------------
            [
                'name'         => 'pengujian',
                'display_name' => 'Pengujian',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
            [
                'name'         => 'pengujian-siskit',
                'display_name' => 'SISKIT',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
            [
                'name'         => 'pengujian-sistgi',
                'display_name' => 'Sistgi',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
            [
                'name'         => 'pengujian-tt',
                'display_name' => 'Tegangan Tinggi',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
            [
                'name'         => 'pengujian-tr',
                'display_name' => 'Tegangan Rendah',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
            [
                'name'         => 'kalibrasi',
                'display_name' => 'kalibrasi',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],

            // ------------- Laporan Pengujian ---------------
            [
                'name'         => 'laporan-pengujian',
                'display_name' => 'Laporan Pengujian',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],

            // ------------- Pengiriman Laporan ---------------
            [
                'name'         => 'pengiriman-laporan',
                'display_name' => 'Pengiriman Laporan',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],

            // ------------- Close Order ---------------
            [
                'name'         => 'close-order',
                'display_name' => 'Close Order',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
            [
                'name'         => 'cancel-order',
                'display_name' => 'Cancel Order',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],

            // ------------- Historikal Data ---------------
            [
                'name'         => 'historikal-data',
                'display_name' => 'Historikal Data',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],

            // ------------- Aktivasi User ---------------
            [
                'name'         => 'aktivasi-user',
                'display_name' => 'Aktivasi User',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],

    		// ------------- MASTER ---------------
    		[
				'name'         => 'master-jenis-pelayanan',
				'display_name' => 'Pelayanan',
				'action'       => ['view', 'add', 'edit', 'delete'],
    		],
            [
                'name'         => 'master-lingkup',
                'display_name' => 'Lingkup',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
            [
                'name'         => 'master-jenis-pengujian',
                'display_name' => 'Jenis Pengujian',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
            [
                'name'         => 'master-mata-uji',
                'display_name' => 'Mata Uji',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
            [
                'name'         => 'master-jenis-alat',
                'display_name' => 'Jenis Alat',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
            [
                'name'         => 'master-perusahaan',
                'display_name' => 'Perusahaan',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
            [
                'name'         => 'master-contact',
                'display_name' => 'Contact',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
            [
                'name'         => 'master-survei',
                'display_name' => 'Survei',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
            [
                'name'         => 'master-satuan',
                'display_name' => 'Satuan',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
            [
                'name'         => 'master-hari-kerja',
                'display_name' => 'Hari Kerja',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
    		[
				'name'         => 'master-prioritas',
				'display_name' => 'Prioritas',
				'action'       => ['view', 'add', 'edit', 'delete'],
    		],
    		// ------------- Konfigurasi ---------------
    		[
				'name'         => 'konfigurasi-users',
				'display_name' => 'Manajemen Pengguna',
				'action'       => ['view', 'add', 'edit', 'delete'],
    		],
    		[
				'name'         => 'konfigurasi-roles',
				'display_name' => 'Hak Akses',
				'action'       => ['view', 'add', 'edit', 'delete'],
    		],
    		[
				'name'         => 'pengaduan',
				'display_name' => 'Pengaduan',
				'action'       => ['view', 'add', 'edit', 'delete'],
    		],
    		[
				'name'         => 'permintaan-spm',
				'display_name' => 'Permintaan SPM',
				'action'       => ['view', 'add', 'edit', 'delete'],
    		],
            [
                'name'         => 'realisasi-biaya',
                'display_name' => 'Realisasi Biaya',
                'action'       => ['view', 'add', 'edit', 'delete'],
            ],
    	];

    	foreach ($permissions as $row) {
    		foreach ($row['action'] as $key => $val) {
                $exist = Permission::where('name', $row['name'].'-'.$val)->first();
                if(!$exist){
        			$temp = [
    					'name'         => $row['name'].'-'.$val,
    					'display_name' => $row['display_name'].' '.ucfirst($val)
        			];	
        			$perms = Permission::create($temp);

        			$role_admin 		 	   			->attachPermission($perms);
    		        $role_user  		 	   			->attachPermission($perms);
    		        $role_spm  		   		   			->attachPermission($perms);
    		        $role_keuangan  					->attachPermission($perms);

    		        // $role_pelaksana  					->attachPermission($perms);
    		        // $role_pelaksana_leader  			->attachPermission($perms);

    		        $role_yan_kalibrasi  	   			->attachPermission($perms);
    		        $role_yan_uji  		 	   			->attachPermission($perms);

    		        $role_msb_kalibrasi  	   			->attachPermission($perms);
    		        $role_msb_siskit  	 	   			->attachPermission($perms);
    		        $role_msb_sistgi  	 	   			->attachPermission($perms);
    		        $role_msb_tegangan_rendah  			->attachPermission($perms);
    		        $role_msb_tegangan_tinggi  			->attachPermission($perms);

    		        $role_asman_dal_kalibrasi  	   		->attachPermission($perms);
    		        $role_asman_dal_siskit  	   		->attachPermission($perms);
    		        $role_asman_dal_sistgi  	   		->attachPermission($perms);
    		        $role_asman_dal_tegangan_rendah  	->attachPermission($perms);
    		        $role_asman_dal_tegangan_tinggi  	->attachPermission($perms);

    		        $role_asman_lola_kalibrasi  	   	->attachPermission($perms);
    		        $role_asman_lola_siskit  	   		->attachPermission($perms);
    		        $role_asman_lola_sistgi  	   		->attachPermission($perms);
    		        $role_asman_lola_tegangan_rendah  	->attachPermission($perms);
    		        $role_asman_lola_tegangan_tinggi  	->attachPermission($perms);

                    $role_adminlab_kalibrasi        	->attachPermission($perms);
                    $role_adminlab_sistgi           	->attachPermission($perms);
                    $role_adminlab_siskit           	->attachPermission($perms);
                    $role_adminlab_tegangan_tinggi  	->attachPermission($perms);
                    $role_adminlab_tegangan_rendah  	->attachPermission($perms);

                    $role_pelaksana_kalibrasi        	->attachPermission($perms);
                    $role_pelaksana_sistgi           	->attachPermission($perms);
                    $role_pelaksana_siskit           	->attachPermission($perms);
                    $role_pelaksana_tegangan_tinggi  	->attachPermission($perms);
                    $role_pelaksana_tegangan_rendah  	->attachPermission($perms);

                    $role_gm           					->attachPermission($perms);
                    $role_srm_prosmkal  				->attachPermission($perms);
                    $role_srm_uji  						->attachPermission($perms);
                }
    		}
    	}
    }
}
