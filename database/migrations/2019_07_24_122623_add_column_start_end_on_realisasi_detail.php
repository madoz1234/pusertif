<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStartEndOnRealisasiDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('trans_realisasi_biaya_detail', function (Blueprint $table) {
            $table->date('end')->nullable()->after('periode');
            $table->date('start')->nullable()->after('periode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('trans_realisasi_biaya_detail', function (Blueprint $table) {
            $table->dropColumn(['start','end']);
        });
    }
}
