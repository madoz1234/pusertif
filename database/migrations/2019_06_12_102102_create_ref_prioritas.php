<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefPrioritas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_prioritas',function(Blueprint $table){
            $table->increments('id');
            $table->integer('layanan_id')->unsigned();
            $table->integer('waktu')->default(0)->comment('Bulan');

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('layanan_id')->references('id')->on('ref_jenis_pelayanan');
        });

        Schema::create('log_ref_prioritas',function(Blueprint $table){
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('layanan_id')->unsigned();
            $table->integer('waktu')->default(0)->comment('Bulan');

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps(); 
        });

        Schema::table('trans_pp', function (Blueprint $table) {
            $table->text('catatan')->nullable()->after('tipe');
        });

        Schema::table('log_trans_pp', function (Blueprint $table) {
            $table->text('catatan')->nullable()->after('tipe');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_trans_pp', function (Blueprint $table) {
            $table->dropColumn('catatan');
        });

        Schema::table('trans_pp', function (Blueprint $table) {
            $table->dropColumn('catatan');
        });

       	Schema::dropIfExists('ref_prioritas');
        Schema::dropIfExists('log_ref_prioritas');
    }
}
