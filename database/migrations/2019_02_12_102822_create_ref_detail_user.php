<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefDetailUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_detail_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('jenis_pelayanan_id')->unsigned();
            $table->string('nama_lengkap', 150);
            $table->string('nip', 150);
            $table->string('jabatan', 150);
            $table->text('alamat');
            $table->string('no_tlp', 150);
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('user_id')->references('id')->on('sys_users');
            $table->foreign('jenis_pelayanan_id')->references('id')->on('ref_jenis_pelayanan');
        });

        Schema::create('log_ref_detail_user', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ref_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('jenis_pelayanan_id')->unsigned();
            $table->string('nama_lengkap', 150);
            $table->string('nip', 150);
            $table->string('jabatan', 150);
            $table->text('alamat');
            $table->string('no_tlp', 150);
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_detail_user');
        Schema::dropIfExists('log_ref_detail_user');
    }
}
