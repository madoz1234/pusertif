<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefJenisPengujian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('ref_lingkup', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jenis_pelayanan_id')->unsigned();
            $table->string('nama', 300);
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('jenis_pelayanan_id')->references('id')->on('ref_jenis_pelayanan');
        });

        Schema::create('log_ref_lingkup', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ref_id')->unsigned();
            $table->integer('jenis_pelayanan_id')->unsigned();
            $table->string('nama', 300);
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
        
        Schema::create('ref_jenis_pengujian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jenis_pelayanan_id')->unsigned();
            $table->integer('lingkup_id')->unsigned();
            $table->string('nama', 300);
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('jenis_pelayanan_id')->references('id')->on('ref_jenis_pelayanan');
            $table->foreign('lingkup_id')->references('id')->on('ref_lingkup');
        });

        Schema::create('log_ref_jenis_pengujian', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ref_id')->unsigned();
            $table->integer('jenis_pelayanan_id')->unsigned();
            $table->integer('lingkup_id')->unsigned();
            $table->string('nama', 300);
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_ref_jenis_pengujian');
        Schema::dropIfExists('log_ref_lingkup');
        Schema::dropIfExists('ref_jenis_pengujian');
        Schema::dropIfExists('ref_lingkup');
    }
}
