<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransPengembalianDanaBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_pengembalian_dana_barang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('close_id')->unsigned();
            $table->string('no_nota', 200)->nullable();
            $table->string('tgl_nota', 200)->nullable();
            $table->decimal('dana_kembali', 30,2)->default(0);
            $table->integer('status')->default(0)->comment('0:Belum,1:Selesai');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('close_id')->references('id')->on('trans_close_order');
      	});

       	Schema::create('trans_pengembalian_dana_barang_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dana_id')->unsigned();
            $table->integer('close_detail_id')->unsigned();
            $table->integer('group_id')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('dana_id')->references('id')->on('trans_pengembalian_dana_barang');
            $table->foreign('close_detail_id')->references('id')->on('trans_close_order_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_pengembalian_dana_barang_detail');
        Schema::dropIfExists('trans_pengembalian_dana_barang');
    }
}
