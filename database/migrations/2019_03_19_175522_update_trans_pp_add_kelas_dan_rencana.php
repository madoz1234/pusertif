<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransPpAddKelasDanRencana extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pp', function (Blueprint $table) {
            $table->integer('kelas')->default(0)->comment('1:Reguler,2:Express');
            $table->string('rencana', 100)->nullable();
        });

        Schema::table('log_trans_pp', function (Blueprint $table) {
            $table->integer('kelas')->default(0)->comment('1:Reguler,2:Express');
            $table->string('rencana', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_trans_pp', function (Blueprint $table) {
            $table->dropColumn('kelas');
            $table->dropColumn('rencana');
        });

        Schema::table('trans_pp', function (Blueprint $table) {
            $table->dropColumn('kelas');
            $table->dropColumn('rencana');
        });
    }
}
