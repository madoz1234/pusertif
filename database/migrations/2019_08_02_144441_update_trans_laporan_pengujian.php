<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransLaporanPengujian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_laporan_pengujian', function (Blueprint $table) {
            $table->string('nota_dinas', 200)->nullable()->after('status');
            $table->string('tgl_nota_dinas', 200)->nullable()->after('nota_dinas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_laporan_pengujian', function (Blueprint $table) {
             $table->dropColumn('nota_dinas');
             $table->dropColumn('tgl_nota_dinas');
        });
    }
}
