<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransKajiUlang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_kaji_ulang', function (Blueprint $table) {
            $table->string('bukti', 255)->nullable()->after('keputusan');
           	$table->string('filename', 255)->nullable()->after('bukti');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_kaji_ulang', function (Blueprint $table) {
            $table->dropColumn('bukti');
            $table->dropColumn('filename');
        });
    }
}
