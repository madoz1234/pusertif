<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransAduan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('trans_aduan', function (Blueprint $table) {
           	$table->increments('id');
			$table->integer('pilihan')->default(0)->comment('1:Aduan,2:Saran');
			$table->string('nama', 300);
			$table->string('email', 100);
			$table->string('no_hp', 100);
			$table->text('pesan');
			$table->string('lampiran', 255);
           	$table->string('filename', 255);
			$table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_aduan', function (Blueprint $table) {
           	$table->increments('id');
           	$table->integer('ref_id')->unsigned();
			$table->integer('pilihan')->default(0)->comment('1:Aduan,2:Saran');
			$table->string('nama', 300);
			$table->string('email', 100);
			$table->string('no_hp', 100);
			$table->text('pesan');
			$table->string('lampiran', 255);
           	$table->string('filename', 255);
			$table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_aduan');
        Schema::dropIfExists('trans_aduan');
    }
}
