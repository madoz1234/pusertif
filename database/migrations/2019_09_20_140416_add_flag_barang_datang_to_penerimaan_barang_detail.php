<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlagBarangDatangToPenerimaanBarangDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_penerimaan_barang_detail', function (Blueprint $table) {
         	$table->tinyInteger('flag')->default(0)->comment('0: Sudah Datang, 1:Belum Datang')->after('tentative_end');
        });

        Schema::table('log_trans_penerimaan_barang_detail', function (Blueprint $table) {
             $table->tinyInteger('flag')->default(0)->comment('0: Sudah Datang, 1:Belum Datang')->after('tentative_end');
        });

        Schema::table('trans_pengujian_detail', function (Blueprint $table) {
         	$table->tinyInteger('flag')->default(0)->comment('0: Sudah Datang, 1:Belum Datang');
        });

        Schema::table('log_trans_pengujian_detail', function (Blueprint $table) {
             $table->tinyInteger('flag')->default(0)->comment('0: Sudah Datang, 1:Belum Datang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_penerimaan_barang_detail', function (Blueprint $table) {
            $table->dropColumn('flag');
        });
        
        Schema::table('trans_penerimaan_barang_detail', function (Blueprint $table) {
         	$table->dropColumn('flag');
        });

        Schema::table('log_trans_pengujian_detail', function (Blueprint $table) {
            $table->dropColumn('flag');
        });
        
        Schema::table('trans_pengujian_detail', function (Blueprint $table) {
         	$table->dropColumn('flag');
        });
    }
}
