<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransPp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_pp', function (Blueprint $table) {
           	$table->increments('id');
			$table->string('no_order')->unique();
			$table->date('tgl_order');
			$table->integer('user_id')->unsigned();
			$table->integer('layanan_id')->unsigned();
			$table->integer('lingkup_id')->unsigned();
			$table->integer('status')->default(0)->comment('1:Surat Penawaran,2:Kaji Ulang,3:Konfirmasi Pembayaran,4:In Progress,5:Batal,6:Selesai,7:Gagal Diproses');
			$table->string('bukti', 255);
           	$table->string('filename', 255);
			$table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('user_id')->references('id')->on('sys_users');
            $table->foreign('layanan_id')->references('id')->on('ref_jenis_pelayanan');
            $table->foreign('lingkup_id')->references('id')->on('ref_lingkup');
        });

        Schema::create('trans_pp_detail', function (Blueprint $table) {
        	$table->increments('id');
        	$table->integer('pp_id')->unsigned();
        	$table->integer('jenis_id')->unsigned();
        	$table->string('merk');
        	$table->string('tipe', 200)->nullable();
        	$table->integer('jumlah')->default(0)->nullable();
        	$table->string('satuan', 150)->nullable();
        	$table->text('spesifikasi')->nullable();
        	
        	$table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('pp_id')->references('id')->on('trans_pp')->onDelete('cascade');
            $table->foreign('jenis_id')->references('id')->on('ref_jenis_pengujian');
        });

        Schema::create('log_trans_pp', function (Blueprint $table) {
           	$table->increments('id');
           	$table->integer('ref_id')->unsigned();
			$table->string('no_order')->unique();
			$table->date('tgl_order');
			$table->integer('user_id')->unsigned();
			$table->integer('layanan_id')->unsigned();
			$table->integer('lingkup_id')->unsigned();
			$table->integer('status')->default(0)->comment('1:Surat Penawaran,2:Kaji Ulang,3:Konfirmasi Pembayaran,4:In Progress,5:Batal,6:Selesai,7:Gagal Diproses');
			$table->string('bukti', 255);
           	$table->string('filename', 255);
			$table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {	
    	Schema::dropIfExists('log_trans_pp');
        Schema::dropIfExists('trans_pp_detail');
        Schema::dropIfExists('trans_pp');
    }
}
