<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransSuratPenawaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_surat_penawaran', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('va_id')->unsigned();
            $table->string('no_surat', 200);
            $table->string('tgl_surat', 200)->nullable();
            $table->string('jadwal_start', 200)->nullable();
            $table->string('jadwal_end', 200)->nullable();
            $table->integer('adendum_status')->default(0)->comment('1:Adendum 1,2:Adendum 2, Dst');
            $table->text('adendum')->nullable();
            $table->integer('surat_status')->default(0)->comment('0:Belum Dikirim,2:Terkirim');

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('va_id')->references('id')->on('trans_va');
        });

        Schema::create('trans_surat_penawaran_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('surat_id')->unsigned();
            $table->text('komponen')->nullable();
            $table->integer('nominal');
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('surat_id')->references('id')->on('trans_surat_penawaran');
        });

        Schema::table('trans_va', function (Blueprint $table) {
            $table->integer('status_va')->default(0)->comment('0:Belum Dikirim,1:Sudah Dikirim')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_surat_penawaran_detail');
        Schema::dropIfExists('trans_surat_penawaran');
        Schema::table('trans_va', function (Blueprint $table) {
           $table->dropColumn('status_va');
        });
    }
}
