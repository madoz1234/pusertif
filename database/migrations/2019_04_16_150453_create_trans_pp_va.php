<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransPpVa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_pp_va', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('detail_id')->unsigned();
            $table->integer('pp_id')->unsigned();
            $table->integer('jenis_id')->unsigned();
            $table->integer('perusahaan_id')->unsigned();
            $table->string('no_va',200)->unique();
            $table->date('tgl_aktif')->nullable();
            $table->date('tgl_kadaluarsa')->nullable();
            $table->string('wbs_io',200)->unique();
            $table->integer('status')->default(0)->comment('1:Aktif,2:Menunggu Aktif,3:Gagal Terbayaran,4:Terbayar');
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('detail_id')->references('id')->on('trans_pp_detail');
            $table->foreign('pp_id')->references('id')->on('trans_pp');
            $table->foreign('jenis_id')->references('id')->on('ref_jenis_pengujian');
            $table->foreign('perusahaan_id')->references('id')->on('ref_perusahaan');

        });

        Schema::create('log_trans_pp_va', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('detail_id')->unsigned();
            $table->integer('pp_id')->unsigned();
            $table->integer('jenis_id')->unsigned();
            $table->integer('perusahaan_id')->unsigned();
            $table->string('no_va',200)->unique();
            $table->date('tgl_aktif')->nullable();
            $table->date('tgl_kadaluarsa')->nullable();
            $table->string('wbs_io',200)->unique();
            $table->integer('status')->default(0)->comment('1:Aktif,2:Menunggu Aktif,3:Gagal Terbayaran,4:Terbayar');
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_pp_va');
        Schema::dropIfExists('trans_pp_va');
    }
}
