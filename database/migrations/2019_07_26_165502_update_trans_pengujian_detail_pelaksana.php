<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransPengujianDetailPelaksana extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pengujian_detail_pelaksana', function (Blueprint $table) {
            $table->integer('pelaksana_laporan_id')->unsigned()->default(0)->after('pelaksana_id');
            $table->integer('status')->default(0)->comment('0:Belum,1:Selesai')->after('pelaksana_laporan_id');

            $table->foreign('pelaksana_laporan_id')->references('id')->on('sys_users');
        });

        Schema::create('trans_pengujian_detail_laporan', function (Blueprint $table) {
    		$table->increments('id');
    		$table->integer('detail_pelaksana_id')->unsigned();
    		$table->string('bukti', 255);
           	$table->string('filename', 255);
    		$table->integer('created_by')->unsigned()->nullable();
    		$table->integer('updated_by')->unsigned()->nullable();
    		$table->nullableTimestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_pengujian_detail_pelaksana', function (Blueprint $table) {
            $table->dropForeign(['pelaksana_laporan_id']);
            $table->dropColumn('pelaksana_laporan_id');
            $table->dropColumn('status');
        });

        Schema::dropIfExists('trans_pengujian_detail_laporan');
    }
}
