<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UbahTransPenerimaanBarangDanDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_penerimaan_barang', function (Blueprint $table) {
            $table->tinyInteger('status_close')->default(0)->comment('0: belum, 1:sudah');
        });

        Schema::table('trans_penerimaan_barang_detail', function (Blueprint $table) {
            $table->tinyInteger('status_close')->default(0)->comment('0: belum, 1:sudah');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_penerimaan_barang', function (Blueprint $table) {
            $table->dropColumn('status_close');
        });

        Schema::table('trans_penerimaan_barang_detail', function (Blueprint $table) {
            $table->dropColumn('status_close');
        });
    }
}
