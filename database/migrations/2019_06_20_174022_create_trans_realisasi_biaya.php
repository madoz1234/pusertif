<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransRealisasiBiaya extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_realisasi_biaya', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('konfirmasi_id')->unsigned();
            $table->integer('status')->default(0)->comment('0:Belum,1:Sudah');

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('konfirmasi_id')->references('id')->on('trans_konfirmasi_pembayaran');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_realisasi_biaya');
    }
}
