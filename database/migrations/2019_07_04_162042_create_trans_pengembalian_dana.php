<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransPengembalianDana extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_pengembalian_dana', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('konfirmasi_id')->unsigned();
            $table->string('nota_batal',200)->nullable();
            $table->string('nota_kembali',200)->nullable();
            $table->decimal('jumlah_dana', 30,2)->default(0);
            $table->string('tgl_kembali',200)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('konfirmasi_id')->references('id')->on('trans_konfirmasi_pembayaran');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_pengembalian_dana');
    }
}
