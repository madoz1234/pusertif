<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransLaporanPengujian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_laporan_pengujian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pengujian_id')->unsigned();
            $table->string('no_laporan', 200)->nullable();
            $table->string('tgl_laporan', 200)->nullable();
            $table->integer('status')->default(0)->comment('0:Belum,1:Selesai');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('pengujian_id')->references('id')->on('trans_pengujian');
        });

       Schema::create('trans_laporan_pengujian_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('laporan_pengujian_id')->unsigned();
            $table->integer('pengujian_detail_id')->unsigned();
            $table->integer('group_id')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('laporan_pengujian_id')->references('id')->on('trans_laporan_pengujian');
            $table->foreign('pengujian_detail_id')->references('id')->on('trans_pengujian_detail');
        });

       Schema::table('trans_pengujian_detail', function (Blueprint $table) {
           $table->integer('status_laporan')->default(0)->comment('0:Belum,1:Sudah')->after('fpp_fpk');
       });

       Schema::dropIfExists('trans_pengujian_detail_laporan');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_laporan_pengujian_detail');
        Schema::dropIfExists('trans_laporan_pengujian');
        Schema::table('trans_pengujian_detail', function (Blueprint $table) {
            $table->dropColumn('status_laporan');
        });

        Schema::create('trans_pengujian_detail_laporan', function (Blueprint $table) {
    		$table->increments('id');
    		$table->integer('detail_pelaksana_id')->unsigned();
    		$table->string('bukti', 255);
           	$table->string('filename', 255);
    		$table->integer('created_by')->unsigned()->nullable();
    		$table->integer('updated_by')->unsigned()->nullable();
    		$table->nullableTimestamps();
    	});
    }
}
