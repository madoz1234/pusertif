<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRefPerusahaanAddNoTlp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_perusahaan', function (Blueprint $table) {
            $table->string('no_tlp', 150)->nullable()->after('kode');
            $table->integer('status')->default(0)->comment('0:Tidak Valid,1:Valid')->after('alamat');
            $table->integer('kategori')->default(0)->comment('0:PLN,1:Non PLN,2:A-PLN')->after('status');
            $table->string('email', 150)->nullable()->after('kategori');
        });

        Schema::table('log_ref_perusahaan', function (Blueprint $table) {
            $table->string('no_tlp', 150)->nullable()->after('kode');
            $table->integer('status')->default(0)->comment('0:Tidak Valid,1:Valid')->after('alamat');
            $table->integer('kategori')->default(0)->comment('0:PLN,1:Non PLN,2:A-PLN')->after('status');
            $table->string('email', 150)->nullable()->after('kategori');
        });

        Schema::table('ref_pelanggan', function (Blueprint $table) {
            $table->string('nip', 150)->nullable()->after('pesan');
        });

        Schema::table('log_ref_pelanggan', function (Blueprint $table) {
            $table->string('nip', 150)->nullable()->after('pesan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_ref_perusahaan', function (Blueprint $table) {
            $table->dropColumn('no_tlp');
            $table->dropColumn('status');
            $table->dropColumn('kategori');
            $table->dropColumn('email');
        });

        Schema::table('ref_perusahaan', function (Blueprint $table) {
            $table->dropColumn('no_tlp');
            $table->dropColumn('status');
            $table->dropColumn('kategori');
            $table->dropColumn('email');
        });

        Schema::table('log_ref_pelanggan', function (Blueprint $table) {
            $table->dropColumn('nip');
        });

        Schema::table('ref_pelanggan', function (Blueprint $table) {
            $table->dropColumn('nip');
        });
    }
}
