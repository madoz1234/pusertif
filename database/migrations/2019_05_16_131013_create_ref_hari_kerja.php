    <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefHariKerja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_hari_kerja',function(Blueprint $table){
            $table->increments('id');
            $table->string('nama',200)->nullable();
            $table->string('description',200)->nullable();
            $table->string('hk',200)->nullable();

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_hari_kerja',function(Blueprint $table){
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('nama',200)->nullable();
            $table->string('description',200)->nullable();
            $table->string('hk',200)->nullable();

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_hari_kerja');
        Schema::dropIfExists('log_ref_hari_kerja');
    }
}
