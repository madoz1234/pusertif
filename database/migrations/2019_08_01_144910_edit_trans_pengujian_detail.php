<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTransPengujianDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pengujian_detail', function (Blueprint $table) {
            $table->string('fpp_fpk', 255)->nullable()->after('status');
        });

        Schema::table('trans_pengujian', function (Blueprint $table) {
            $table->dropColumn('fpp_fpk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_pengujian_detail', function (Blueprint $table) {
            $table->dropColumn('fpp_fpk');
        });

        Schema::table('trans_pengujian', function (Blueprint $table) {
            $table->string('fpp_fpk', 200)->nullable()->after('status_pengujian');
        });
    }
}
