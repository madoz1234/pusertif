<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReschedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('trans_reschedule', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pengujian_id')->unsigned();
            $table->integer('status')->default(0)->comment('0:Belum,1:Selesai');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('pengujian_id')->references('id')->on('trans_pengujian');
        });

       Schema::create('trans_reschedule_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reschedule_id')->unsigned();
            $table->integer('pengujian_detail_id')->unsigned();
            $table->string('tentative_start_old', 200)->nullable();
        	$table->string('tentative_end_old', 200)->nullable();
            $table->string('tentative_start_new', 200)->nullable();
        	$table->string('tentative_end_new', 200)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('reschedule_id')->references('id')->on('trans_reschedule');
            $table->foreign('pengujian_detail_id')->references('id')->on('trans_pengujian_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_reschedule_detail');
        Schema::dropIfExists('trans_reschedule');
    }
}
