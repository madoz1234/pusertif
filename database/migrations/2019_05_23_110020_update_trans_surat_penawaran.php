<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransSuratPenawaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_surat_penawaran', function (Blueprint $table) {
            $table->string('nomor');
            $table->string('wbs_io', 200)->nullable()->change();
        });

        Schema::table('trans_surat_penawaran_detail', function (Blueprint $table) {
            $table->decimal('nominal', 30,2)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_surat_penawaran', function (Blueprint $table) {
            $table->dropColumn('nomor');
            $table->string('wbs_io', 200)->nullable(false)->change();
        });

        Schema::table('trans_surat_penawaran_detail', function (Blueprint $table) {
            $table->integer('nominal')->change();
        });
    }
}
