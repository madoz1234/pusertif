<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDisplayOnHariKerja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('ref_hari_kerja', function (Blueprint $table) {
            $table->string('display_menu')->nullable()->after('menu');
        });

        Schema::table('log_ref_hari_kerja', function (Blueprint $table) {
            $table->string('display_menu')->nullable()->after('menu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('ref_hari_kerja', function (Blueprint $table) {
            $table->dropColumn(['display_menu']);
        });

        Schema::table('log_ref_hari_kerja', function (Blueprint $table) {
            $table->dropColumn(['display_menu']);
        });
    }
}
