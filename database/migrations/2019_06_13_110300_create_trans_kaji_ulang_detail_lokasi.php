<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransKajiUlangDetailLokasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_kaji_ulang_detail', function (Blueprint $table) {
            $table->dropColumn('jenis_lokasi');
            $table->dropColumn('ket_lokasi');
        });

        Schema::create('trans_kaji_ulang_detail_lokasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('detail_id')->unsigned();
            $table->integer('jenis_lokasi')->default(0)->comment('1:in-house,2:on-site');
            $table->text('ket_lokasi')->nullable();

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('detail_id')->references('id')->on('trans_kaji_ulang_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('trans_kaji_ulang_detail_lokasi');
    	
        Schema::table('trans_kaji_ulang_detail', function (Blueprint $table) {
            $table->integer('jenis_lokasi')->default(0)->comment('1:in-house,2:on-site');
            $table->text('ket_lokasi')->nullable();
        });
    }
}
