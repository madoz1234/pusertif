<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UbahTransPengujianDetailPelaksana extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pengujian_detail_pelaksana', function (Blueprint $table) {
            $table->integer('status_data')->default(0)->comment('0:Belum,1:Pelaksana,2:Asmen,3:Msb,4:Yan,5:PemintaJasa')->after('status');
            $table->integer('status_file')->default(0)->comment('0:Belum,1:Sudah')->after('status_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_pengujian_detail_pelaksana', function (Blueprint $table) {
           $table->dropColumn('status_data');
           $table->dropColumn('status_file');
        });
    }
}
