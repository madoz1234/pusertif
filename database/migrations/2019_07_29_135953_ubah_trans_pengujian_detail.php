<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UbahTransPengujianDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pengujian_detail', function (Blueprint $table) {
            $table->integer('status')->default(0)->comment('0:Belum,1:Pelaksana,2:Asmen,3:Msb,4:Yan,5:PemintaJasa')->after('pelaksana_id');
        });

        Schema::create('act_pengujian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned();
            $table->integer('pengirim_id')->unsigned();
            $table->integer('penerima_id')->unsigned();
            $table->integer('tipe')->default(0)->comment('1:Dialihkan,2:Diteruskan');
            $table->integer('jenis')->default(0)->comment('1:Verifikasi,2:Pengujian');
            $table->text('keterangan')->nullable();
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('pengirim_id')->references('id')->on('sys_users');
            $table->foreign('penerima_id')->references('id')->on('sys_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_pengujian_detail', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        
        Schema::dropIfExists('act_pengujian');
    }
}
