<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateActDisposisi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('act_disposisi', function (Blueprint $table) {
            $table->integer('status_kaji_ulang')->default(0)->comment('1:Didiskusikan')->after('keterangan');
            $table->integer('status_surat_penawaran')->default(0)->comment('0:Adendum 0, 1:Adendum 1, Dst')->after('status_kaji_ulang');
        });

        Schema::table('log_act_disposisi', function (Blueprint $table) {
            $table->integer('status_kaji_ulang')->default(0)->comment('1:Didiskusikan')->after('keterangan');
            $table->integer('status_surat_penawaran')->default(0)->comment('0:Adendum 0, 1:Adendum 1, Dst')->after('status_kaji_ulang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {	
    	Schema::table('log_act_disposisi', function (Blueprint $table) {
           	$table->dropColumn('status_kaji_ulang');
            $table->dropColumn('status_surat_penawaran');
        });

        Schema::table('act_disposisi', function (Blueprint $table) {
           	$table->dropColumn('status_kaji_ulang');
            $table->dropColumn('status_surat_penawaran');
        });
    }
}
