<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributOnRefPelayanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_jenis_pelayanan', function (Blueprint $table) {
            $table->longText('deskripsi')->nullable();
        });

        Schema::table('log_ref_jenis_pelayanan', function (Blueprint $table) {
            $table->longText('deskripsi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_jenis_pelayanan', function (Blueprint $table) {
            $table->dropColumn('deskripsi')->nullable();
        });

        Schema::table('log_ref_jenis_pelayanan', function (Blueprint $table) {
            $table->dropColumn('deskripsi')->nullable();
        });
    }
}
