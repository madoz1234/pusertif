<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransRealisasiBiayaDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_realisasi_biaya_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('realisasi_id')->unsigned();
            $table->string('periode',200)->nullable();
            $table->decimal('jumlah', 30,2)->default(0);
            $table->text('file_upload')->nullable();
            $table->text('path')->nullable();
            $table->string('tgl_upload',200)->nullable();

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('realisasi_id')->references('id')->on('trans_realisasi_biaya');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_realisasi_biaya_detail');
    }
}
