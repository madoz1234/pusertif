<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributeOnSysUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sys_users', function (Blueprint $table) {
            $table->integer('no_hp')->nullable();
            $table->string('peminta_jasa')->nullable();
            $table->string('pelanggan')->nullable();
            $table->string('no_tlp')->nullable();
            $table->string('nama')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sys_users', function (Blueprint $table) {
            $table->dropColumn(['no_hp','peminta_jasa','pelanggan','no_tlp','nama']);
        });
    }
}
