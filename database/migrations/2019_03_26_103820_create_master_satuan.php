<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterSatuan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_satuan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('satuan', 150);
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_satuan', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ref_id')->unsigned();
            $table->string('satuan', 150);
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::table('trans_pp_detail', function (Blueprint $table) {
            $table->dropColumn('satuan');
            $table->integer('satuan_id')->unsigned()->nullable()->after('jumlah');

            $table->foreign('satuan_id')->references('id')->on('ref_satuan');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('trans_pp_detail', function (Blueprint $table) {
            $table->dropForeign(['satuan_id']);
            $table->dropColumn('satuan_id');
            $table->string('satuan', 150)->nullable()->after('jumlah');
        });

        Schema::dropIfExists('log_ref_satuan');
        Schema::dropIfExists('ref_satuan');
    }
}
