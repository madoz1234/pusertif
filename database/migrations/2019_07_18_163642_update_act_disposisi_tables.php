<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateActDisposisiTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('act_disposisi', function (Blueprint $table) {
            $table->integer('status_pengujian')->default(0)->comment('1: Barang Uji, 2: Pengujian')->after('status_surat_penawaran');
        });
        Schema::table('log_act_disposisi', function (Blueprint $table) {
            $table->integer('status_pengujian')->default(0)->comment('1: Barang Uji, 2: Pengujian')->after('status_surat_penawaran');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_act_disposisi', function (Blueprint $table) {
            $table->dropColumn('status_pengujian');
        });
        Schema::table('act_disposisi', function (Blueprint $table) {
            $table->dropColumn('status_pengujian');
        });
    }
}
