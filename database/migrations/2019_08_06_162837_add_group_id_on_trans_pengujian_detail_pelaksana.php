<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGroupIdOnTransPengujianDetailPelaksana extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pengujian_detail_pelaksana', function (Blueprint $table) {
            $table->integer('group_id')->default(0)->after('status_file');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_pengujian_detail_pelaksana', function (Blueprint $table) {
             $table->dropColumn('group_id');
        });
    }
}
