<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUniqueWbsIo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_konfirmasi_pembayaran', function (Blueprint $table) {
         	$table->dropUnique(['wbs_io']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_konfirmasi_pembayaran', function (Blueprint $table) {
         	// $table->string('wbs_io', 200)->unique()->change();
        });
    }
}
