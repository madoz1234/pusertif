<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransPenerimaanBarangUji extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_penerimaan_barang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('konfirmasi_id')->unsigned();
            $table->string('no_kpj',200)->nullable();
            $table->string('no_agenda',200)->nullable();
            $table->integer('status')->default(0)->comment('0:Baru,1:On Progress,2:Selesai');
            $table->integer('pengirim_id')->unsigned();
            $table->integer('penerima_id')->unsigned();

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('konfirmasi_id')->references('id')->on('trans_konfirmasi_pembayaran');
            $table->foreign('pengirim_id')->references('id')->on('sys_users');
            $table->foreign('penerima_id')->references('id')->on('sys_users');
        });


        Schema::create('trans_penerimaan_barang_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penerimaan_id')->unsigned();
            $table->integer('detail_pp_id')->unsigned();
            $table->string('no_seri',200)->nullable();
            $table->string('nama_barang',200)->nullable();
            $table->text('catatan')->nullable();
            $table->integer('status')->default(0)->comment('0:Belum Dibuat Penerimaan Barang,1:Belum Diuji,2:Sedang Diuji,3:Selesai');

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('penerimaan_id')->references('id')->on('trans_penerimaan_barang');
            $table->foreign('detail_pp_id')->references('id')->on('trans_pp_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_penerimaan_barang_detail');
        Schema::dropIfExists('trans_penerimaan_barang');
    }
}
