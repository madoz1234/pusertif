<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRefDetailUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_detail_user', function (Blueprint $table) {
            $table->dropForeign(['jenis_pelayanan_id']);
            $table->dropColumn('jenis_pelayanan_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_detail_user', function (Blueprint $table) {
            $table->integer('jenis_pelayanan_id')->unsigned()->default(1)->after('user_id');
            
            $table->foreign('jenis_pelayanan_id')->references('id')->on('ref_jenis_pelayanan');
        });
    }
}
