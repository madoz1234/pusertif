<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPenyesuaianAllModulUtkSerahTerima extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pp', function (Blueprint $table) {
             $table->tinyInteger('tipe_surat')->default(0)->comment('0: Normal, 1:Serah Terima')->after('status');
        });

        Schema::table('log_trans_pp', function (Blueprint $table) {
             $table->tinyInteger('tipe_surat')->default(0)->comment('0: Normal, 1:Serah Terima')->after('status');
        });

        Schema::table('trans_kaji_ulang', function (Blueprint $table) {
             $table->tinyInteger('tipe')->default(0)->comment('0: Normal, 1:Serah Terima')->after('status')->after('keterangan');
        });

        Schema::table('log_trans_kaji_ulang', function (Blueprint $table) {
             $table->tinyInteger('tipe')->default(0)->comment('0: Normal, 1:Serah Terima')->after('status')->after('keterangan');
        });

        Schema::table('trans_surat_penawaran', function (Blueprint $table) {
             $table->tinyInteger('tipe')->default(0)->comment('0: Normal, 1:Serah Terima')->after('status')->after('pph');
        });

        Schema::table('log_trans_surat_penawaran', function (Blueprint $table) {
             $table->tinyInteger('tipe')->default(0)->comment('0: Normal, 1:Serah Terima')->after('status')->after('pph');
        });

        Schema::table('trans_va', function (Blueprint $table) {
             $table->tinyInteger('tipe')->default(0)->comment('0: Normal, 1:Serah Terima')->after('status')->after('wbs_io');
        });

        Schema::table('log_trans_va', function (Blueprint $table) {
             $table->tinyInteger('tipe')->default(0)->comment('0: Normal, 1:Serah Terima')->after('status')->after('wbs_io');
        });

        Schema::table('trans_konfirmasi_pembayaran', function (Blueprint $table) {
             $table->tinyInteger('tipe')->default(0)->comment('0: Normal, 1:Serah Terima')->after('status')->after('wbs_io');
        });

        Schema::table('log_trans_konfirmasi_pembayaran', function (Blueprint $table) {
             $table->tinyInteger('tipe')->default(0)->comment('0: Normal, 1:Serah Terima')->after('status')->after('wbs_io');
        });

        Schema::table('trans_uji_serah_terima', function (Blueprint $table) {
             $table->integer('pp_id')->unsigned()->nullable()->after('id');
             $table->foreign('pp_id')->references('id')->on('trans_pp');
        });

        Schema::table('log_trans_uji_serah_terima', function (Blueprint $table) {
             $table->integer('pp_id')->unsigned()->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_konfirmasi_pembayaran', function (Blueprint $table) {
             $table->dropColumn('tipe');
        });
        Schema::table('log_trans_va', function (Blueprint $table) {
             $table->dropColumn('tipe');
        });
        Schema::table('log_trans_surat_penawaran', function (Blueprint $table) {
             $table->dropColumn('tipe');
        });
        Schema::table('log_trans_kaji_ulang', function (Blueprint $table) {
             $table->dropColumn('tipe');
        });
        Schema::table('log_trans_pp', function (Blueprint $table) {
             $table->dropColumn('tipe_surat');
        });


        Schema::table('trans_pp', function (Blueprint $table) {
             $table->dropColumn('tipe_surat');
        });
        Schema::table('trans_kaji_ulang', function (Blueprint $table) {
             $table->dropColumn('tipe');
        });
        Schema::table('trans_surat_penawaran', function (Blueprint $table) {
             $table->dropColumn('tipe');
        });
        Schema::table('trans_va', function (Blueprint $table) {
             $table->dropColumn('tipe');
        });
        Schema::table('trans_konfirmasi_pembayaran', function (Blueprint $table) {
             $table->dropColumn('tipe');
        });

        Schema::table('log_trans_uji_serah_terima', function (Blueprint $table) {
            $table->dropColumn('pp_id');
        });

        Schema::table('trans_uji_serah_terima', function (Blueprint $table) {
            $table->dropForeign(['pp_id']);
            $table->dropColumn('pp_id');
        });
    }
}
