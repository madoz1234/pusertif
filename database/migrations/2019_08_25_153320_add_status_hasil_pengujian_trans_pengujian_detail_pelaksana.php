<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusHasilPengujianTransPengujianDetailPelaksana extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pengujian_detail_pelaksana', function (Blueprint $table) {
            $table->tinyInteger('status_hasil_pengujian')->default(0)->comment('0: belum, 1:diterima, 2:ditolak')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_pengujian_detail_pelaksana', function (Blueprint $table) {
            $table->dropColumn('status_hasil_pengujian');
        });
    }
}
