<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UbahTransKajiUlangDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_kaji_ulang_detail', function (Blueprint $table) {
            $table->decimal('jumlah', 30,2)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_kaji_ulang_detail', function (Blueprint $table) {
            $table->integer('jumlah')->default(0)->nullable()->change();
        });
    }
}
