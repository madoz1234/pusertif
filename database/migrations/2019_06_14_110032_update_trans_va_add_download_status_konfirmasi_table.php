<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransVaAddDownloadStatusKonfirmasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_va', function (Blueprint $table) {
            $table->integer('download_by')->unsigned()->nullable()->after('updated_by');
            $table->timestamp('download_date')->nullable()->after('updated_at');

            $table->integer('status_konfirmasi')->default(0)->comment('0:Belum,1:Kurang,2:Lebih,3:Lunas')->after('status');
        });

        Schema::table('log_trans_va', function (Blueprint $table) {
            $table->integer('download_by')->unsigned()->nullable()->after('updated_by');
            $table->timestamp('download_date')->nullable()->after('updated_at');

            $table->integer('status_konfirmasi')->default(0)->comment('0:Belum,1:Kurang,2:Lebih,3:Lunas')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_va', function (Blueprint $table) {
            $table->dropColumn('status_konfirmasi');
            $table->dropColumn('download_date');
            $table->dropColumn('download_by');
        });

        Schema::table('log_trans_va', function (Blueprint $table) {
            $table->dropColumn('status_konfirmasi');
            $table->dropColumn('download_date');
            $table->dropColumn('download_by');
        });
    }
}
