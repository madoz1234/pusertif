<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefSurveiTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_survei', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->text('deskripsi')->nullable();
            $table->integer('status')->default(0)->comment('0:Aktif,1:Tidak Aktif');
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_survei', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ref_id')->unsigned();
            $table->string('nama');
            $table->text('deskripsi')->nullable();
            $table->integer('status')->default(0)->comment('0:Aktif,1:Tidak Aktif');
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_survei_detail', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('survei_id')->unsigned();
            $table->string('pertanyaan');
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('survei_id')->references('id')->on('ref_survei');
        });

        Schema::create('log_ref_survei_detail', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ref_id')->unsigned();
            $table->integer('survei_id')->unsigned();
            $table->string('pertanyaan');
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_ref_survei_detail');
        Schema::dropIfExists('ref_survei_detail');
        Schema::dropIfExists('log_ref_survei');
        Schema::dropIfExists('ref_survei');
    }
}
