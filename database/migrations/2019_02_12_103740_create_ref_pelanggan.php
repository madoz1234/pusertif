<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefPelanggan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_pelanggan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('perusahaan_id')->unsigned();
            $table->integer('tipe_customer')->default(0)->comment('0:PLN,1:Non PLN,1:A-PLN');
            $table->string('nama_lengkap', 150);
            $table->string('email', 150);
            $table->string('no_tlp', 150);
            $table->text('alamat');
            $table->integer('status')->default(0)->nullable()->comment('0:Aktif,1:Tidak Aktif');
            $table->string('no_hp', 100)->nullable();
            $table->text('pesan')->nullable();
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('user_id')->references('id')->on('sys_users');
            $table->foreign('perusahaan_id')->references('id')->on('ref_perusahaan');
        });

        Schema::create('log_ref_pelanggan', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ref_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('perusahaan_id')->unsigned();
            $table->integer('tipe_customer')->default(0)->comment('0:PLN,1:Non PLN,1:A-PLN');
            $table->string('nama_lengkap', 150);
            $table->string('email', 150);
            $table->string('no_tlp', 150);
            $table->text('alamat');
            $table->integer('status')->default(0)->nullable()->comment('0:Aktif,1:Tidak Aktif');
            $table->string('no_hp', 100)->nullable();
            $table->text('pesan')->nullable();
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_pelanggan');
        Schema::dropIfExists('log_ref_pelanggan');
    }
}
