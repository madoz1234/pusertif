<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransKonfirmasiPembayaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_va', function (Blueprint $table) {
            $table->integer('tipe_customer')->default(0)->comment('0:PLN,1:Non PLN,1:A-PLN')->after('no_va');
            $table->dropColumn('status_konfirmasi');
        });

        Schema::table('log_trans_va', function (Blueprint $table) {
            $table->integer('tipe_customer')->default(0)->comment('0:PLN,1:Non PLN,1:A-PLN')->after('no_va');
            $table->dropColumn('status_konfirmasi');
        });

        Schema::create('trans_konfirmasi_pembayaran', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('va_id')->unsigned();
            $table->integer('status')->default(0)->comment('0:Belum,1:Kurang,2:Lebih,3:Lunas');
            $table->string('status_dokumen',200)->nullable();
            $table->text('catatan_dokumen')->nullable();
            $table->string('wbs_io',200)->unique()->nullable();
            $table->text('catatan')->nullable();

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('va_id')->references('id')->on('trans_va');
        });

        Schema::create('log_trans_konfirmasi_pembayaran', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('va_id')->unsigned();
            $table->integer('status')->default(0)->comment('0:Belum,1:Kurang,2:Lebih,3:Lunas');
            $table->string('status_dokumen',200)->nullable();
            $table->text('catatan_dokumen')->nullable();
            $table->string('wbs_io',200)->nullable();
            $table->text('catatan')->nullable();

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_konfirmasi_pembayaran');
        Schema::dropIfExists('trans_konfirmasi_pembayaran');

        Schema::table('trans_va', function (Blueprint $table) {
            $table->integer('status_konfirmasi')->default(0)->comment('0:Belum,1:Kurang,2:Lebih,3:Lunas')->after('status');
            $table->dropColumn('tipe_customer');
        });

        Schema::table('log_trans_va', function (Blueprint $table) {
            $table->integer('status_konfirmasi')->default(0)->comment('0:Belum,1:Kurang,2:Lebih,3:Lunas')->after('status');
            $table->dropColumn('tipe_customer');
        });
    }
}
