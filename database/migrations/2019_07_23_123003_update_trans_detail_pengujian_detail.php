<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransDetailPengujianDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pengujian_detail', function (Blueprint $table) {
            $table->integer('pelaksana_id')->unsigned()->after('verifikasi');
            $table->text('keterangan')->nullable()->after('pelaksana_id');
            $table->foreign('pelaksana_id')->references('id')->on('sys_users');
        });

        Schema::dropIfExists('trans_pengujian_detail_pelaksana');

        Schema::table('trans_penerimaan_barang', function (Blueprint $table) {
 			$table->integer('status_pengujian')->default(0)->comment('0:Belum,1:Selesai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('trans_penerimaan_barang', function (Blueprint $table) {
    		$table->dropColumn('status_pengujian');
        });

        Schema::table('trans_pengujian_detail', function (Blueprint $table) {
           	$table->dropForeign(['pelaksana_id']);
            $table->dropColumn('pelaksana_id');
            $table->dropColumn('keterangan');
        });

        Schema::create('trans_pengujian_detail_pelaksana', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('detail_pengujian_id')->unsigned();
            $table->integer('pelaksana_id')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('detail_pengujian_id')->references('id')->on('trans_pengujian');
            $table->foreign('pelaksana_id')->references('id')->on('sys_users');
        });
    }
}
