<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKurangBayarToTransKonfirmasiPembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_konfirmasi_pembayaran', function (Blueprint $table) {
            $table->decimal('dana_kurang', 30,2)->default(0)->after('catatan');
        });

        Schema::table('log_trans_konfirmasi_pembayaran', function (Blueprint $table) {
            $table->decimal('dana_kurang', 30,2)->default(0)->after('catatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_konfirmasi_pembayaran', function (Blueprint $table) {
            $table->dropColumn('dana_kurang');
        });

        Schema::table('log_trans_konfirmasi_pembayaran', function (Blueprint $table) {
            $table->dropColumn('dana_kurang');
        });
    }
}
