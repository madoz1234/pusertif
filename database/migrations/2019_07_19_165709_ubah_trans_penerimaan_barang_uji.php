<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UbahTransPenerimaanBarangUji extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('trans_penerimaan_barang', function (Blueprint $table) {
            $table->string('tanggal_terima',200)->nullable()->after('status');
            $table->dropColumn('no_kpj');
            $table->dropColumn('no_agenda');
        });

        Schema::table('trans_penerimaan_barang_detail', function (Blueprint $table) {
            $table->integer('max')->default(0)->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    	Schema::table('trans_penerimaan_barang', function (Blueprint $table) {
            $table->dropColumn('tanggal_terima');
            $table->string('no_kpj',200)->nullable()->after('konfirmasi_id');
            $table->string('no_agenda',200)->nullable()->after('no_kpj');
        });

        Schema::table('trans_penerimaan_barang_detail', function (Blueprint $table) {
            $table->dropColumn('max');
        });
    }
}
