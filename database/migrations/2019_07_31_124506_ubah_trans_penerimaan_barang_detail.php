<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UbahTransPenerimaanBarangDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_penerimaan_barang_detail', function (Blueprint $table) {
            $table->integer('group_id')->default(0);
        });

        Schema::table('trans_pengujian_detail', function (Blueprint $table) {
            $table->integer('group_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_penerimaan_barang_detail', function (Blueprint $table) {
            $table->dropColumn('group_id');
        });

        Schema::table('trans_pengujian_detail', function (Blueprint $table) {
            $table->dropColumn('group_id');
        });
    }
}
