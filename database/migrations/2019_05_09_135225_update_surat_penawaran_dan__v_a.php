<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSuratPenawaranDanVA extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('trans_va', function (Blueprint $table) {
            $table->dropForeign(['kaji_id']);
            $table->dropColumn('kaji_id');
            $table->dropColumn('status_va');

            $table->integer('surat_id')->unsigned()->after('id');
            $table->foreign('surat_id')->references('id')->on('trans_surat_penawaran');
        });

        Schema::table('trans_surat_penawaran', function (Blueprint $table) {
            $table->dropForeign(['va_id']);
            $table->dropColumn('va_id');
            $table->string('wbs_io', 200);
            $table->integer('kaji_id')->unsigned()->after('id');
            $table->foreign('kaji_id')->references('id')->on('trans_kaji_ulang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_surat_penawaran', function (Blueprint $table) {
            $table->dropForeign(['kaji_id']);
            $table->dropColumn('kaji_id');
            $table->dropColumn('wbs_io');
            $table->integer('va_id')->unsigned()->after('id');
            $table->foreign('va_id')->references('id')->on('trans_va');
        });

        Schema::table('trans_va', function (Blueprint $table) {
            $table->dropForeign(['surat_id']);
            $table->dropColumn('surat_id');
            $table->integer('status_va')->default(0)->comment('0:Belum Dikirim,1:Sudah Dikirim')->after('status');
            $table->integer('kaji_id')->unsigned()->after('id');
            $table->foreign('kaji_id')->references('id')->on('trans_kaji_ulang');
        });
    }
}
