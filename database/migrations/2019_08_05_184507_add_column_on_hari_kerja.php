<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOnHariKerja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('ref_hari_kerja', function (Blueprint $table) {
            $table->string('menu')->nullable()->after('nama');
            $table->integer('jenis_pelayanan_id')->unsigned()->nullable()->after('nama');

            $table->foreign('jenis_pelayanan_id')->references('id')->on('ref_jenis_pelayanan');
        });

        Schema::table('log_ref_hari_kerja', function (Blueprint $table) {
            $table->string('menu')->nullable()->after('nama');
            $table->integer('jenis_pelayanan_id')->unsigned()->nullable()->after('nama');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('ref_hari_kerja', function (Blueprint $table) {
            $table->dropForeign('ref_hari_kerja_jenis_pelayanan_id_foreign');
            $table->dropColumn(['menu','jenis_pelayanan_id']);
        });

        Schema::table('log_ref_hari_kerja', function (Blueprint $table) {
            $table->dropColumn(['menu','jenis_pelayanan_id']);
        });
    }
}
