<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransVa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('trans_surat_penawaran', function (Blueprint $table) {
            $table->decimal('total', 30,2)->default(0)->after('surat_status');
        });

        Schema::table('trans_va', function (Blueprint $table) {
            $table->decimal('nominal', 30,2)->default(0)->after('tgl_kadaluarsa');
            $table->string('wbs_io', 200)->nullable()->change();
        });

        Schema::create('log_trans_va', function (Blueprint $table) {
           	$table->increments('id');
           	$table->integer('ref_id')->unsigned();
           	$table->integer('surat_id')->unsigned();
			$table->string('no_va', 200);
            $table->string('wbs_io', 200)->nullable();
            $table->string('tgl_aktif', 200)->nullable();
            $table->string('tgl_kadaluarsa', 200)->nullable();
            $table->decimal('nominal', 30,2)->default(0);
            $table->integer('status')->default(0)->comment('1:Aktif,2:Tidak Aktif');
			$table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {	
    	Schema::table('trans_surat_penawaran', function (Blueprint $table) {
    		$table->dropColumn('total');
        });

    	Schema::dropIfExists('log_trans_va');
    	
        Schema::table('trans_va', function (Blueprint $table) {
           $table->dropColumn('nominal');
           $table->string('wbs_io', 200)->nullable()->change();
        });
    }
}
