<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDataTransPp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pp', function (Blueprint $table) {
            $table->integer('nomor')->default(0);
        });

        Schema::table('log_trans_pp', function (Blueprint $table) {
            $table->integer('nomor')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_pp', function (Blueprint $table) {
           $table->dropColumn('nomor');
        });

        Schema::table('trans_pp', function (Blueprint $table) {
           $table->dropColumn('nomor');
        });
    }
}
