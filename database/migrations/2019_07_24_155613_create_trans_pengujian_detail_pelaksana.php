<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransPengujianDetailPelaksana extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('trans_pengujian_detail_pelaksana', function (Blueprint $table) {
    		$table->increments('id');
    		$table->integer('pengujian_detail_id')->unsigned();
    		$table->integer('pelaksana_id')->unsigned();
    		$table->integer('created_by')->unsigned()->nullable();
    		$table->integer('updated_by')->unsigned()->nullable();
    		$table->nullableTimestamps();

    		$table->foreign('pelaksana_id')->references('id')->on('sys_users');
    	});

    	Schema::create('trans_pengujian_detail_member', function (Blueprint $table) {
    		$table->increments('id');
    		$table->integer('detail_pelaksana_id')->unsigned();
    		$table->integer('pelaksana_id')->unsigned();
    		$table->integer('created_by')->unsigned()->nullable();
    		$table->integer('updated_by')->unsigned()->nullable();
    		$table->nullableTimestamps();

    		$table->foreign('pelaksana_id')->references('id')->on('sys_users');
    	});

    	Schema::table('trans_pengujian', function (Blueprint $table) {
            $table->integer('status_pengujian')->default(0)->comment('1:Diterima,2:Ditolak')->after('status_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_pengujian_detail_member');
        Schema::dropIfExists('trans_pengujian_detail_pelaksana');

        Schema::table('trans_pengujian', function (Blueprint $table) {
            $table->dropColumn('status_pengujian');
        });
    }
}
