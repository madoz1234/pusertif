<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransPengujian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_pengujian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penerimaan_id')->unsigned();
            $table->integer('tipe')->default(0)->comment('1:Kalibrasi,2:SISKIT,3:SISTGI,4:TT,5:TR');
            $table->integer('status')->default(0)->comment('1:Verifikasi,2:Pengujian');
            $table->integer('status_dispo')->default(0)->comment('1:Yan,2:Msb,3:Asmen,4:Pelaksana,5:Peminta Jasa');
            $table->integer('status_data')->default(0)->comment('1:Belum,2:Selesai');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('penerimaan_id')->references('id')->on('trans_penerimaan_barang');
        });

        Schema::create('trans_pengujian_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pengujian_id')->unsigned();
            $table->integer('detail_penerimaan_id')->unsigned();
            $table->integer('verifikasi')->default(0)->comment('1:Diterima,2:Ditolak');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('pengujian_id')->references('id')->on('trans_pengujian');
            $table->foreign('detail_penerimaan_id')->references('id')->on('trans_penerimaan_barang_detail');
        });

        Schema::create('trans_pengujian_detail_pelaksana', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('detail_pengujian_id')->unsigned();
            $table->integer('pelaksana_id')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('detail_pengujian_id')->references('id')->on('trans_pengujian');
            $table->foreign('pelaksana_id')->references('id')->on('sys_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_pengujian_detail_pelaksana');
        Schema::dropIfExists('trans_pengujian_detail');
        Schema::dropIfExists('trans_pengujian');
    }
}
