<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRefLayanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_jenis_pelayanan', function (Blueprint $table) {
            $table->tinyInteger('kode')->default(0)->after('nama');
            $table->tinyInteger('seri')->default(0)->after('kode');
        });

        Schema::table('log_ref_jenis_pelayanan', function (Blueprint $table) {
            $table->tinyInteger('kode')->default(0)->after('nama');
            $table->tinyInteger('seri')->default(0)->after('kode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_jenis_pelayanan', function (Blueprint $table) {
            $table->dropColumn('kode');
            $table->dropColumn('seri');
        });

        Schema::table('log_ref_jenis_pelayanan', function (Blueprint $table) {
            $table->dropColumn('kode');
            $table->dropColumn('seri');
        });
    }
}
