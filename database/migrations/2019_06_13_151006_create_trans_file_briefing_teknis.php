<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransFileBriefingTeknis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_files_briefing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kaji_id')->unsigned();
            $table->string('label');
            $table->string('path');
            $table->string('attachable_type')->nullable();
            $table->timestamps();

            $table->foreign('kaji_id')->references('id')->on('trans_kaji_ulang')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_files_briefing');
    }
}
