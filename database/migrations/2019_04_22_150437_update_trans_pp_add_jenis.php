<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransPpAddJenis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pp', function (Blueprint $table) {
            $table->integer('jenis')->default(0)->comment('0:Kalibrasi,1:Pengujian');
        });

        Schema::table('log_trans_pp', function (Blueprint $table) {
            $table->integer('jenis')->default(0)->comment('0:Kalibrasi,1:Pengujian');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_pp', function (Blueprint $table) {
           $table->dropColumn('jenis');
        });

        Schema::table('trans_pp', function (Blueprint $table) {
           $table->dropColumn('jenis');
        });
    }
}
