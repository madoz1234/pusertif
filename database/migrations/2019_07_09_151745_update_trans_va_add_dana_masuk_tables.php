<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransVaAddDanaMasukTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_konfirmasi_pembayaran', function (Blueprint $table) {
            $table->decimal('dana_masuk', 30,2)->default(0)->after('status');
        });

        Schema::table('log_trans_konfirmasi_pembayaran', function (Blueprint $table) {
            $table->decimal('dana_masuk', 30,2)->default(0)->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_konfirmasi_pembayaran', function (Blueprint $table) {
            $table->dropColumn('dana_masuk');
        });

        Schema::table('log_trans_konfirmasi_pembayaran', function (Blueprint $table) {
            $table->dropColumn('dana_masuk');
        });
    }
}
