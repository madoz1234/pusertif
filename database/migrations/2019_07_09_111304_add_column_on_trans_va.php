<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOnTransVa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('trans_va', function (Blueprint $table) {
            $table->integer('parent_id')->unsigned()->nullable()->after('download_date');
            $table->foreign('parent_id')->references('id')->on('trans_va');
        });

        Schema::table('log_trans_va', function (Blueprint $table) {
            $table->integer('parent_id')->unsigned()->nullable()->after('download_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('trans_va', function (Blueprint $table) {
            $table->dropForeign('trans_va_parent_id_foreign');
            $table->dropColumn('parent_id');
        });

        Schema::table('log_trans_va', function (Blueprint $table) {
            $table->dropColumn('parent_id');
        });
    }
}
