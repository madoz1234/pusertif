<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActKajiUlang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('act_kaji_ulang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pp_id')->unsigned();
            $table->integer('pengirim_id')->unsigned();
            $table->integer('penerima_id')->unsigned();
            $table->integer('tipe')->default(0)->comment('1:Dialihkan,2:Diteruskan');
            $table->text('keterangan')->nullable();
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('pp_id')->references('id')->on('trans_pp');
            $table->foreign('pengirim_id')->references('id')->on('sys_users');
            $table->foreign('penerima_id')->references('id')->on('sys_users');
        });

        Schema::create('log_act_kaji_ulang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('act_id')->unsigned();
            $table->integer('pp_id')->unsigned();
            $table->integer('pengirim_id')->unsigned();
            $table->integer('penerima_id')->unsigned();
            $table->integer('tipe')->default(0)->comment('1:Dialihkan,2:Diteruskan');
            $table->text('keterangan')->nullable();
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('act_kaji_ulang');
        Schema::dropIfExists('log_act_kaji_ulang');
    }
}
