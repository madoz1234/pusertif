<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransVaAddBuktiBayar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_va', function (Blueprint $table) {
            $table->integer('status')->default(0)->comment('1:Aktif,2:Tidak Aktif,3:Terbayar')->change();
            $table->string('bukti_url', 100)->nullable()->after('status');
            $table->string('bukti_filename', 255)->nullable()->after('bukti_url');
            $table->string('tgl_bayar', 200)->nullable()->after('bukti_filename');
        });

        Schema::table('log_trans_va', function (Blueprint $table) {
            $table->integer('status')->default(0)->comment('1:Aktif,2:Tidak Aktif,3:Terbayar')->change();
            $table->string('bukti_url', 100)->nullable()->after('status');
            $table->string('bukti_filename', 255)->nullable()->after('bukti_url');
            $table->string('tgl_bayar', 200)->nullable()->after('bukti_filename');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_va', function (Blueprint $table) {
            $table->dropColumn('tgl_bayar');
            $table->dropColumn('bukti_filename');
            $table->dropColumn('bukti_url');
            $table->integer('status')->default(0)->comment('1:Aktif,2:Tidak Aktif')->change();
        });

        Schema::table('log_trans_va', function (Blueprint $table) {
            $table->dropColumn('tgl_bayar');
            $table->dropColumn('bukti_filename');
            $table->dropColumn('bukti_url');
            $table->integer('status')->default(0)->comment('1:Aktif,2:Tidak Aktif')->change();
        });
    }
}
