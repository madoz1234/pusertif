<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransCloseOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_close_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penerimaan_id')->unsigned();
            $table->string('no_nota', 200)->nullable();
            $table->string('tgl_nota', 200)->nullable();
            $table->integer('status')->default(0)->comment('0:Belum,1:Selesai');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('penerimaan_id')->references('id')->on('trans_penerimaan_barang');
        });

       Schema::create('trans_close_order_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('close_id')->unsigned();
            $table->integer('penerimaan_detail_id')->unsigned();
            $table->integer('group_id')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('close_id')->references('id')->on('trans_close_order');
            $table->foreign('penerimaan_detail_id')->references('id')->on('trans_penerimaan_barang_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_close_order_detail');
        Schema::dropIfExists('trans_close_order');
    }
}
