<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefPerusahaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_perusahaan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 150);
            $table->string('kode', 5)->nullable();
            $table->text('alamat')->nullable();
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_perusahaan', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ref_id')->unsigned();
            $table->string('nama', 150);
            $table->string('kode', 5)->nullable();
            $table->text('alamat')->nullable();
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_perusahaan');
        Schema::dropIfExists('log_ref_perusahaan');
    }
}
