<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPengujianDetailIdInRescheduleDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_reschedule_detail', function (Blueprint $table) {
            $table->integer('pengujian_detail_id')->unsigned()->after('reschedule_id');

            $table->foreign('pengujian_detail_id')->references('id')->on('trans_pengujian_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_reschedule_detail', function (Blueprint $table) {
            $table->dropForeign(['pengujian_detail_id']);
            $table->dropColumn('pengujian_detail_id');
        });
    }
}
