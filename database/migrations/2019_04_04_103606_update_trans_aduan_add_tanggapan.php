<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransAduanAddTanggapan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_aduan', function (Blueprint $table) {
        	$table->integer('status')->default(0)->comment('0:Belum Ditanggapi,1:Ditanggapi')->after('filename');
            $table->integer('user_id')->unsigned()->nullable()->after('status');
            $table->string('tgl')->nullable()->after('user_id');
            $table->text('keterangan')->nullable()->after('tgl');
            $table->string('tanggapan', 255)->nullable()->after('keterangan');
           	$table->string('path', 255)->nullable()->after('tanggapan');

            $table->foreign('user_id')->references('id')->on('sys_users');
        });

        Schema::table('log_trans_aduan', function (Blueprint $table) {
        	$table->integer('status')->default(0)->comment('0:Belum Ditanggapi,1:Ditanggapi')->after('filename');
           	$table->integer('user_id')->unsigned()->nullable()->after('status');
           	$table->string('tgl')->nullable()->after('user_id');
           	$table->text('keterangan')->nullable()->after('tgl');
           	$table->string('tanggapan', 255)->nullable()->after('keterangan');
           	$table->string('path', 255)->nullable()->after('tanggapan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_aduan', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('user_id');
            $table->dropColumn('tgl');
            $table->dropColumn('keterangan');
            $table->dropColumn('tanggapan');
            $table->dropColumn('path');
        });

        Schema::table('trans_aduan', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn('status');
            $table->dropColumn('user_id');
            $table->dropColumn('tgl');
            $table->dropColumn('keterangan');
            $table->dropColumn('tanggapan');
            $table->dropColumn('path');
        });
    }
}
