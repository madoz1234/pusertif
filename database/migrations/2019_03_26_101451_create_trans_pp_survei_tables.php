<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransPpSurveiTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_pp_survei', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pp_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('nama');
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('pp_id')->references('id')->on('trans_pp');
            $table->foreign('user_id')->references('id')->on('sys_users');
        });

        Schema::create('trans_pp_survei_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pp_survei_id')->unsigned();
            $table->string('pertanyaan');
            $table->integer('jawaban');
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('pp_survei_id')->references('id')->on('trans_pp_survei')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_pp_survei_detail');
        Schema::dropIfExists('trans_pp_survei');
        
    }
}
