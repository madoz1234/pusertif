<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefContact extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_contact', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_tlp', 150);
            $table->string('wa', 150);
            $table->integer('status')->default(0)->nullable()->comment('0:Aktif,1:Tidak Aktif');
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_contact', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ref_id')->unsigned();
            $table->string('no_tlp', 150);
            $table->string('wa', 150);
            $table->integer('status')->default(0)->nullable()->comment('0:Aktif,1:Tidak Aktif');
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_ref_contact');
        Schema::dropIfExists('ref_contact');
    }
}
