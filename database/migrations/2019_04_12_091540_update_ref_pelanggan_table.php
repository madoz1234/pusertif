<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRefPelangganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_pelanggan', function (Blueprint $table) {
            $table->text('keterangan')->nullable()->after('nip');
        });

        Schema::table('log_ref_pelanggan', function (Blueprint $table) {
            $table->text('keterangan')->nullable()->after('nip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_ref_pelanggan', function (Blueprint $table) {
            $table->dropColumn('keterangan');
        });
        
        Schema::table('ref_pelanggan', function (Blueprint $table) {
            $table->dropColumn('keterangan');
        });
    }
}
