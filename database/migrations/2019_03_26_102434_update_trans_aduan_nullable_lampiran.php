<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransAduanNullableLampiran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_aduan', function (Blueprint $table) {
            $table->string('lampiran', 255)->nullable()->change();
           	$table->string('filename', 255)->nullable()->change();
        });

        Schema::table('log_trans_aduan', function (Blueprint $table) {
            $table->string('lampiran', 255)->nullable()->change();
           	$table->string('filename', 255)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       	Schema::table('trans_aduan', function (Blueprint $table) {
            $table->string('lampiran', 255)->nullable(false)->change();
           	$table->string('filename', 255)->nullable(false)->change();
        });

        Schema::table('log_trans_aduan', function (Blueprint $table) {
            $table->string('lampiran', 255)->nullable(false)->change();
           	$table->string('filename', 255)->nullable(false)->change();
        });
    }
}
