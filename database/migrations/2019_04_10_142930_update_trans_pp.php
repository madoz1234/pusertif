<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransPp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pp', function (Blueprint $table) {
            $table->string('no_surat', 200)->nullable()->after('filename');
            $table->string('tgl_surat', 200)->nullable()->after('no_surat');
            $table->integer('tipe')->default(0)->comment('1:Online,2:AMS,3:SPM')->after('tgl_surat');
        });

        Schema::table('log_trans_pp', function (Blueprint $table) {
            $table->string('no_surat', 200)->nullable()->after('filename');
            $table->string('tgl_surat', 200)->nullable()->after('no_surat');
            $table->integer('tipe')->default(0)->comment('1:Online,2:AMS,3:SPM')->after('tgl_surat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_pp', function (Blueprint $table) {
            $table->dropColumn('no_surat');
            $table->dropColumn('tgl_surat');
            $table->dropColumn('tipe');
        });

        Schema::table('trans_pp', function (Blueprint $table) {
            $table->dropColumn('no_surat');
            $table->dropColumn('tgl_surat');
            $table->dropColumn('tipe');
        });
    }
}
