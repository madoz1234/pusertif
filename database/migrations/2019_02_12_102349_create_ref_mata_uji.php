<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefMataUji extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_mata_uji', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jenis_pengujian_id')->unsigned();
            $table->string('nama_mata_uji', 150);
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('jenis_pengujian_id')->references('id')->on('ref_jenis_pengujian');
        });

        Schema::create('log_ref_mata_uji', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ref_id')->unsigned();
            $table->integer('jenis_pengujian_id')->unsigned();
            $table->string('nama_mata_uji', 150);
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_mata_uji');
        Schema::dropIfExists('log_ref_mata_uji');
    }
}
