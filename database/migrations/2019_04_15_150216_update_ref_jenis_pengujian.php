<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRefJenisPengujian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

    	Schema::table('log_ref_jenis_pengujian', function (Blueprint $table) {
            $table->dropColumn('jenis_pelayanan_id');
        });
        
        Schema::table('ref_jenis_pengujian', function (Blueprint $table) {
            $table->dropForeign(['jenis_pelayanan_id']);
            $table->dropColumn('jenis_pelayanan_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_jenis_pengujian', function (Blueprint $table) {
            $table->integer('jenis_pelayanan_id')->unsigned()->nullable();
            $table->foreign('jenis_pelayanan_id')->references('id')->on('ref_jenis_pelayanan');
        });

        Schema::table('log_ref_jenis_pengujian', function (Blueprint $table) {
            $table->integer('jenis_pelayanan_id')->unsigned()->nullable();
        });
    }
}
