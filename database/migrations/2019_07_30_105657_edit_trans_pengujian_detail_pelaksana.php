<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTransPengujianDetailPelaksana extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pengujian_detail_pelaksana', function (Blueprint $table) {
            $table->integer('status_barang')->default(0)->comment('0:Dikembalikan,1:Tidak Dikembalikan')->after('status_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_pengujian_detail_pelaksana', function (Blueprint $table) {
            $table->dropColumn('status_barang');
        });
    }
}
