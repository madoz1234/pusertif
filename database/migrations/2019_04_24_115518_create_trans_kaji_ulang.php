<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransKajiUlang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_kaji_ulang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pp_id')->unsigned();
            $table->integer('keputusan')->default(0)->comment('1:Diterima,2:Ditolak,3:Didiskusikan');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('pp_id')->references('id')->on('trans_pp');
        });

        Schema::create('trans_kaji_ulang_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kaji_ulang_id')->unsigned();
            $table->integer('jenis_id')->unsigned();
            $table->integer('jenis_lokasi')->default(0)->comment('1:in-house,2:on-site');
            $table->text('ket_lokasi')->nullable();
            $table->text('spesifikasi')->nullable();
        	$table->integer('tarif')->default(0)->nullable();
        	$table->integer('jumlah')->default(0)->nullable();
            $table->string('tentative_start', 200)->nullable();
        	$table->string('tentative_end', 200)->nullable();
        	$table->string('reschedule_start', 200)->nullable();
        	$table->string('reschedule_end', 200)->nullable();
        	$table->string('realisasi_start', 200)->nullable();
        	$table->string('realisasi_end', 200)->nullable();
        	$table->string('laporan_start', 200)->nullable();
        	$table->string('laporan_end', 200)->nullable();


            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('kaji_ulang_id')->references('id')->on('trans_kaji_ulang');
        });

        Schema::create('trans_kaji_ulang_detail_mata_uji', function (Blueprint $table) {
        	$table->increments('id');
        	$table->integer('detail_id')->unsigned();
        	$table->text('mata_uji')->nullable();

        	$table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
            
            $table->foreign('detail_id')->references('id')->on('trans_kaji_ulang_detail')->onDelete('cascade');
        });

        Schema::dropIfExists('log_ref_mata_uji');
        Schema::dropIfExists('ref_mata_uji');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('ref_mata_uji', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jenis_pengujian_id')->unsigned();
            $table->string('nama_mata_uji', 150);
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('jenis_pengujian_id')->references('id')->on('ref_jenis_pengujian');
        });

        Schema::create('log_ref_mata_uji', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ref_id')->unsigned();
            $table->integer('jenis_pengujian_id')->unsigned();
            $table->string('nama_mata_uji', 150);
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::dropIfExists('trans_kaji_ulang_detail_mata_uji');
        Schema::dropIfExists('trans_kaji_ulang_detail');
        Schema::dropIfExists('trans_kaji_ulang');
    }
}
