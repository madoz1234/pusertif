<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTransSuratPenawaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_surat_penawaran', function (Blueprint $table) {
            $table->string('no_skki', 200)->nullable();
            $table->string('tgl_skki', 200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_surat_penawaran', function (Blueprint $table) {
            $table->dropColumn('no_skki');
            $table->dropColumn('tgl_skki');
        });
    }
}
