<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateActSuratPenawaranAddTipeDisposisi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('act_surat_penawaran', function (Blueprint $table) {
            $table->integer('tipe')->default(0)->comment('1:Dialihkan,2:Diteruskan')->after('penerima_id');
        });

        Schema::table('log_act_surat_penawaran', function (Blueprint $table) {
            $table->integer('tipe')->default(0)->comment('1:Dialihkan,2:Diteruskan')->after('penerima_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_act_surat_penawaran', function (Blueprint $table) {
            $table->dropColumn('tipe');
        });
        
        Schema::table('act_surat_penawaran', function (Blueprint $table) {
            $table->dropColumn('tipe');
        });
    }
}
