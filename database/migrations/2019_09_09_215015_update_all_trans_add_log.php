<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAllTransAddLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_trans_uji_serah_terima', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('no_order');
            $table->date('tgl_order');
            $table->integer('user_id')->unsigned();
            $table->integer('layanan_id')->unsigned();
            $table->integer('lingkup_id')->unsigned();
            $table->integer('status')->default(0)->comment('0:In Progress,1:Selesai,3:Gagal Diproses');
            $table->string('bukti', 255)->nullable();
            $table->string('filename', 255)->nullable();
            $table->string('no_kontrak', 200)->nullable();
            $table->string('no_wbs', 200)->nullable();
            $table->string('tgl_kontrak', 200)->nullable();
            $table->text('catatan')->nullable();
            $table->integer('kelas')->default(0)->comment('1:Reguler,2:Express');
            $table->string('rencana', 100)->nullable();
            $table->integer('nomor')->default(0);

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_uji_serah_terima_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('uji_id')->unsigned();
            $table->integer('jenis_id')->unsigned();
            $table->string('merk')->nullable();
            $table->string('tipe', 200)->nullable();
            $table->integer('jumlah')->default(0)->nullable();
            $table->integer('satuan_id')->unsigned();
            $table->text('spesifikasi')->nullable();
            $table->date('tgl_mulai')->nullable();
            $table->date('tgl_selesai')->nullable();
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_surat_penawaran', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('kaji_id')->unsigned();
            $table->string('no_surat', 200);
            $table->string('tgl_surat', 200)->nullable();
            $table->string('jadwal_start', 200)->nullable();
            $table->string('jadwal_end', 200)->nullable();
            $table->integer('adendum_status')->default(0)->comment('1:Adendum 1,2:Adendum 2, Dst');
            $table->text('adendum')->nullable();
            $table->integer('surat_status')->default(0)->comment('0:Belum Dikirim,2:Terkirim');
            $table->decimal('total', 30,2)->default(0);
            $table->decimal('pph', 30,2)->default(0);
            $table->string('wbs_io', 200)->nullable();
            $table->string('nomor');
            $table->string('no_skki', 200)->nullable();
            $table->string('tgl_skki', 200)->nullable();

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });


        Schema::create('log_trans_surat_penawaran_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('surat_id')->unsigned();
            $table->text('komponen')->nullable();
            $table->decimal('nominal', 30,2)->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_reschedule', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('pengujian_id')->unsigned();
            $table->integer('status')->default(0)->comment('0:Belum,1:Selesai');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_reschedule_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('reschedule_id')->unsigned();
            $table->integer('pengujian_detail_id')->unsigned();
            $table->string('tentative_start_old', 200)->nullable();
        	$table->string('tentative_end_old', 200)->nullable();
            $table->string('tentative_start_new', 200)->nullable();
        	$table->string('tentative_end_new', 200)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_realisasi_biaya', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('konfirmasi_id')->unsigned();
            $table->integer('status')->default(0)->comment('0:Belum,1:Sudah');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_realisasi_biaya_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('realisasi_id')->unsigned();
            $table->string('periode',200)->nullable();
            $table->date('start')->nullable();
            $table->date('end')->nullable();
            $table->decimal('jumlah', 30,2)->default(0);
            $table->text('file_upload')->nullable();
            $table->text('path')->nullable();
            $table->string('tgl_upload',200)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_pp_survei', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('pp_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('nama');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_pp_survei_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('pp_survei_id')->unsigned();
            $table->string('pertanyaan');
            $table->integer('jawaban');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_pengujian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('penerimaan_id')->unsigned();
            $table->integer('tipe')->default(0)->comment('1:Kalibrasi,2:SISKIT,3:SISTGI,4:TT,5:TR');
            $table->integer('status')->default(0)->comment('1:Verifikasi,2:Pengujian');
            $table->integer('status_dispo')->default(0)->comment('1:Yan,2:Msb,3:Asmen,4:Pelaksana,5:Peminta Jasa');
            $table->integer('status_data')->default(0)->comment('1:Belum,2:Selesai');
            $table->integer('status_pengujian')->default(0)->comment('1:Diterima,2:Ditolak');
            $table->string('no_laporan',200)->nullable();
            $table->integer('status_data_pengujian')->default(0)->comment('	0:Belum,1:Sudah');
            $table->string('tgl_laporan',200)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_pengujian_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('pengujian_id')->unsigned();
            $table->integer('detail_penerimaan_id')->unsigned();
            $table->integer('verifikasi')->default(0)->comment('1:Diterima,2:Ditolak');
            $table->integer('pelaksana_id')->unsigned();
            $table->integer('status')->default(0)->comment('0:Belum,1:Pelaksana,2:Asmen,3:Msb,4:Yan,5:PemintaJasa');
            $table->string('fpp_fpk',255)->nullable();
            $table->integer('status_laporan')->default(0)->comment('1:Diterima,2:Ditolak');
            $table->string('tgl_selesai',200)->nullable();
            $table->text('keterangan')->nullable();
            $table->integer('group_id')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_pengujian_detail_member', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('detail_pelaksana_id')->unsigned();
    		$table->integer('pelaksana_id')->unsigned();
    		$table->integer('created_by')->unsigned()->nullable();
    		$table->integer('updated_by')->unsigned()->nullable();
    		$table->nullableTimestamps();
        });

        Schema::create('log_trans_pengujian_detail_pelaksana', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('pengujian_detail_id')->unsigned();
    		$table->integer('pelaksana_id')->unsigned();
    		$table->integer('pelaksana_laporan_id')->unsigned()->default(0);
            $table->integer('status')->default(0)->comment('0:Belum,1:Selesai');
            $table->tinyInteger('status_hasil_pengujian')->default(0)->comment('0: belum, 1:diterima, 2:ditolak ');
    		$table->integer('status_data')->default(0)->comment('0:Belum,1:Pelaksana,2:Asmen,3:Msb,4:Yan,5:PemintaJasa');
            $table->integer('status_barang')->default(0)->comment('0:Dikembalikan,1:Tidak Dikembalikan');
            $table->integer('status_file')->default(0)->comment('0:Belum,1:Sudah');
            $table->integer('group_id')->default(0);
            $table->integer('status_pengembalian')->default(0)->comment('0:Belum,1:Sudah');
            $table->string('tgl_selesai',200)->nullable();
    		$table->integer('created_by')->unsigned()->nullable();
    		$table->integer('updated_by')->unsigned()->nullable();
    		$table->nullableTimestamps();
        });

        Schema::create('log_trans_pengembalian_dana', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('konfirmasi_id')->unsigned();
            $table->string('nota_batal',200)->nullable();
            $table->string('nota_kembali',200)->nullable();
            $table->decimal('jumlah_dana', 30,2)->default(0);
            $table->string('tgl_kembali',200)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_pengembalian_dana_barang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('close_id')->unsigned();
            $table->string('no_nota', 200)->nullable();
            $table->string('tgl_nota', 200)->nullable();
            $table->decimal('dana_kembali', 30,2)->default(0);
            $table->integer('status')->default(0)->comment('0:Belum,1:Selesai');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_pengembalian_dana_barang_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('dana_id')->unsigned();
            $table->integer('close_detail_id')->unsigned();
            $table->integer('group_id')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_pengembalian_barang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('penerimaan_id')->unsigned();
            $table->string('no_nota_dinas', 200)->nullable();
            $table->string('tgl_nota_dinas', 200)->nullable();
            $table->integer('status')->default(0)->comment('0:Belum,1:Selesai');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_pengembalian_barang_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('pengembalian_id')->unsigned();
            $table->integer('penerimaan_detail_id')->unsigned();
            $table->integer('group_id')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_penerimaan_barang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('konfirmasi_id')->unsigned();
            $table->integer('status')->default(0)->comment('0:Baru,1:On Progress,2:Selesai');
            $table->string('tanggal_terima',200)->nullable();
            $table->string('pengirim',200)->nullable();
            $table->integer('pengirim_id')->unsigned();
            $table->integer('status_pengujian')->default(0)->comment('0:Belum,1:Selesai');
            $table->integer('status_close')->default(0)->comment('0:Belum,1:Sudah');
            $table->integer('status_pengembalian')->default(0)->comment('0:Belum,1:Sudah');
            $table->integer('penerima_id')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_penerimaan_barang_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('penerimaan_id')->unsigned();
            $table->integer('detail_pp_id')->unsigned();
            $table->string('no_seri',200)->nullable();
            $table->string('nama_barang',200)->nullable();
            $table->integer('urutan')->default(0);
            $table->text('catatan')->nullable();
            $table->integer('status')->default(0)->comment('0:Belum Dibuat Penerimaan Barang,1:Belum Diuji,2:Sedang Diuji,3:Selesai');
            $table->integer('max')->default(0);
            $table->string('tentative_start', 200)->nullable();
        	$table->string('tentative_end', 200)->nullable();
        	$table->integer('group_id')->default(0);
        	$table->tinyInteger('status_close')->default(0)->comment('0: belum, 1:sudah');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_laporan_pengujian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('pengujian_id')->unsigned();
            $table->string('no_laporan', 200)->nullable();
            $table->string('tgl_laporan', 200)->nullable();
            $table->integer('status')->default(0)->comment('0:Belum,1:Selesai');
            $table->string('nota_dinas', 200)->nullable();
            $table->string('tgl_nota_dinas', 200)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_laporan_pengujian_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('laporan_pengujian_id')->unsigned();
            $table->integer('pengujian_detail_id')->unsigned();
            $table->integer('group_id')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_kaji_ulang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('pp_id')->unsigned();
            $table->integer('keputusan')->default(0)->comment('1:Diterima,2:Ditolak,3:Didiskusikan');
            $table->string('bukti', 255)->nullable();
           	$table->string('filename', 255)->nullable();
           	$table->text('keterangan')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_kaji_ulang_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('kaji_ulang_id')->unsigned();
            $table->integer('jenis_id')->unsigned();
            $table->text('spesifikasi')->nullable();
            $table->text('tarif')->nullable();
            $table->decimal('jumlah', 30,2)->default(0);
            $table->string('tentative_start', 200)->nullable();
        	$table->string('tentative_end', 200)->nullable();
        	$table->string('reschedule_start', 200)->nullable();
        	$table->string('reschedule_end', 200)->nullable();
        	$table->string('realisasi_start', 200)->nullable();
        	$table->string('realisasi_end', 200)->nullable();
        	$table->string('laporan_start', 200)->nullable();
        	$table->string('laporan_end', 200)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_kaji_ulang_detail_lokasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('detail_id')->unsigned();
            $table->integer('jenis_lokasi')->default(0)->comment('1:in-house,2:on-site');
            $table->text('ket_lokasi')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_kaji_ulang_detail_mata_uji', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('detail_id')->unsigned();
        	$table->text('mata_uji')->nullable();
        	$table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_files_briefing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('kaji_id')->unsigned();
            $table->string('label');
            $table->string('path');
            $table->string('attachable_type')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_close_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('penerimaan_id')->unsigned();
            $table->string('no_nota', 200)->nullable();
            $table->string('tgl_nota', 200)->nullable();
            $table->integer('status')->default(0)->comment('0:Belum,1:Selesai');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_close_order_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('close_id')->unsigned();
            $table->integer('penerimaan_detail_id')->unsigned();
            $table->integer('group_id')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_pp_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('pp_id')->unsigned();
        	$table->integer('jenis_id')->unsigned();
        	$table->string('merk');
        	$table->string('tipe', 200)->nullable();
        	$table->integer('jumlah')->default(0)->nullable();
        	$table->integer('satuan_id')->unsigned()->nullable();
        	$table->text('spesifikasi')->nullable();
        	$table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {	

    	Schema::dropIfExists('log_trans_pp_detail');
    	
    	Schema::dropIfExists('log_trans_close_order_detail');
    	Schema::dropIfExists('log_trans_close_order');
    	
    	Schema::dropIfExists('log_trans_files_briefing');

    	Schema::dropIfExists('log_trans_kaji_ulang_detail_mata_uji');
        Schema::dropIfExists('log_trans_kaji_ulang_detail_lokasi');
        Schema::dropIfExists('log_trans_kaji_ulang_detail');
        Schema::dropIfExists('log_trans_kaji_ulang');

    	Schema::dropIfExists('log_trans_laporan_pengujian_detail');
        Schema::dropIfExists('log_trans_laporan_pengujian');

    	Schema::dropIfExists('log_trans_penerimaan_barang_detail');
        Schema::dropIfExists('log_trans_penerimaan_barang');

    	Schema::dropIfExists('log_trans_pengembalian_barang_detail');
        Schema::dropIfExists('log_trans_pengembalian_barang');

    	Schema::dropIfExists('log_trans_pengembalian_dana_barang_detail');
        Schema::dropIfExists('log_trans_pengembalian_dana_barang');
        Schema::dropIfExists('log_trans_pengembalian_dana');

    	Schema::dropIfExists('log_trans_pengujian_detail_pelaksana');
        Schema::dropIfExists('log_trans_pengujian_detail_member');
        Schema::dropIfExists('log_trans_pengujian_detail');
        Schema::dropIfExists('log_trans_pengujian');

    	Schema::dropIfExists('log_trans_pp_survei_detail');
        Schema::dropIfExists('log_trans_pp_survei');

        Schema::dropIfExists('log_trans_realisasi_biaya_detail');
        Schema::dropIfExists('log_trans_realisasi_biaya');

        Schema::dropIfExists('log_trans_reschedule_detail');
        Schema::dropIfExists('log_trans_reschedule');

        Schema::dropIfExists('log_trans_surat_penawaran_detail');
        Schema::dropIfExists('log_trans_surat_penawaran');

        Schema::dropIfExists('log_trans_uji_serah_terima_detail');
        Schema::dropIfExists('log_trans_uji_serah_terima');
    }
}
