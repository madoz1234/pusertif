<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransPenerimaanBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_penerimaan_barang', function (Blueprint $table) {
        	$table->dropForeign(['pengirim_id']);
            $table->dropColumn('pengirim_id');
            $table->string('pengirim',200)->nullable()->after('status');
        });

        Schema::table('trans_penerimaan_barang_detail', function (Blueprint $table) {
            $table->integer('jumlah_barang')->default(0)->after('nama_barang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_penerimaan_barang', function (Blueprint $table) {
        	$table->dropColumn('pengirim');
           	$table->integer('pengirim_id')->unsigned()->default(1);
           	$table->foreign('pengirim_id')->references('id')->on('sys_users');
        });

        Schema::table('trans_penerimaan_barang_detail', function (Blueprint $table) {
            $table->dropColumn('jumlah_barang');
        });
    }
}
