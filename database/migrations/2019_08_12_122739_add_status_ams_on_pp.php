<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusAmsOnPp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('trans_pp', function (Blueprint $table) {
            $table->tinyInteger('status_ams')->default(0)->comment('0: baru, 1:udah update');
        });
        Schema::table('log_trans_pp', function (Blueprint $table) {
            $table->tinyInteger('status_ams')->default(0)->comment('0: baru, 1:udah update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('trans_pp', function (Blueprint $table) {
            $table->dropColumn('status_ams');
        });
        Schema::table('log_trans_pp', function (Blueprint $table) {
            $table->dropColumn('status_ams');
        });
    }
}
