<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTransPengujian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pengujian', function (Blueprint $table) {
            $table->string('no_laporan', 200)->nullable()->after('fpp_fpk');
            $table->string('tgl_laporan', 200)->nullable()->after('no_laporan');
            $table->integer('status_data_pengujian')->default(0)->comment('0:Belum,1:Sudah')->after('no_laporan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_pengujian', function (Blueprint $table) {
            $table->dropColumn('no_laporan');
            $table->dropColumn('tgl_laporan');
            $table->dropColumn('status_data_pengujian');
        });
    }
}
