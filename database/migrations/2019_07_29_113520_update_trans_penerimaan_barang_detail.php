<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransPenerimaanBarangDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_penerimaan_barang_detail', function (Blueprint $table) {
             $table->string('tentative_start', 200)->nullable()->after('max');
        	 $table->string('tentative_end', 200)->nullable()->after('tentative_start');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_penerimaan_barang_detail', function (Blueprint $table) {
            $table->dropColumn('tentative_start');
            $table->dropColumn('tentative_end');
        });
    }
}
