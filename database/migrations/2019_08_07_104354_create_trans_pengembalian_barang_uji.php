<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransPengembalianBarangUji extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_pengembalian_barang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penerimaan_id')->unsigned();
            $table->string('no_nota_dinas', 200)->nullable();
            $table->string('tgl_nota_dinas', 200)->nullable();
            $table->integer('status')->default(0)->comment('0:Belum,1:Selesai');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('penerimaan_id')->references('id')->on('trans_penerimaan_barang');
        });

       Schema::create('trans_pengembalian_barang_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pengembalian_id')->unsigned();
            $table->integer('penerimaan_detail_id')->unsigned();
            $table->integer('group_id')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('penerimaan_detail_id')->references('id')->on('trans_penerimaan_barang_detail');
            $table->foreign('pengembalian_id')->references('id')->on('trans_pengembalian_barang');
        });

       Schema::table('trans_pengujian_detail_pelaksana', function (Blueprint $table) {
           $table->integer('status_pengembalian')->default(0)->comment('0:Belum,1:Sudah')->after('group_id');
       });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_pengembalian_barang_detail');
        Schema::dropIfExists('trans_pengembalian_barang');
        Schema::table('trans_pengujian_detail_pelaksana', function (Blueprint $table) {
            $table->dropColumn('status_pengembalian');
        });
    }
}
