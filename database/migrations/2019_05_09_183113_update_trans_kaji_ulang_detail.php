<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransKajiUlangDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_kaji_ulang_detail', function (Blueprint $table) {
            $table->text('tarif')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_kaji_ulang_detail', function (Blueprint $table) {
            $table->integer('tarif')->default(0)->nullable()->change();
        });
    }
}
