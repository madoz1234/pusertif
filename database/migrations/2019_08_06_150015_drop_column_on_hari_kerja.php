<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnOnHariKerja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('ref_hari_kerja', function (Blueprint $table) {
            $table->dropColumn('nama');
        });

        Schema::table('log_ref_hari_kerja', function (Blueprint $table) {
            $table->dropColumn('nama');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('ref_hari_kerja', function (Blueprint $table) {
            $table->string('nama')->nullable()->after('id');
        });

        Schema::table('log_ref_hari_kerja', function (Blueprint $table) {
            $table->string('nama')->nullable()->after('id');
        });
    }
}
