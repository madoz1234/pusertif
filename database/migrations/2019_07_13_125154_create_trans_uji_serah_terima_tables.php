<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransUjiSerahTerimaTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_uji_serah_terima', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_order')->unique();
            $table->date('tgl_order');
            $table->integer('user_id')->unsigned();
            $table->integer('layanan_id')->unsigned();
            $table->integer('lingkup_id')->unsigned();
            $table->integer('status')->default(0)->comment('0:In Progress,1:Selesai,3:Gagal Diproses');
            $table->string('bukti', 255)->nullable();
            $table->string('filename', 255)->nullable();
            $table->string('no_kontrak', 200)->nullable();
            $table->string('no_wbs', 200)->nullable();
            $table->string('tgl_kontrak', 200)->nullable();
            $table->text('catatan')->nullable();
            $table->integer('kelas')->default(0)->comment('1:Reguler,2:Express');
            $table->string('rencana', 100)->nullable();
            $table->integer('nomor')->default(0);

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('user_id')->references('id')->on('sys_users');
            $table->foreign('layanan_id')->references('id')->on('ref_jenis_pelayanan');
            $table->foreign('lingkup_id')->references('id')->on('ref_lingkup');
        });

        Schema::create('trans_uji_serah_terima_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uji_id')->unsigned();
            $table->integer('jenis_id')->unsigned();
            $table->string('merk')->nullable();
            $table->string('tipe', 200)->nullable();
            $table->integer('jumlah')->default(0)->nullable();
            $table->integer('satuan_id')->unsigned();
            $table->text('spesifikasi')->nullable();
            $table->date('tgl_mulai')->nullable();
            $table->date('tgl_selesai')->nullable();
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('uji_id')->references('id')->on('trans_uji_serah_terima')->onDelete('cascade');
            $table->foreign('jenis_id')->references('id')->on('ref_jenis_pengujian');
            $table->foreign('satuan_id')->references('id')->on('ref_satuan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_uji_serah_terima_detail');
        Schema::dropIfExists('trans_uji_serah_terima');
    }
}
