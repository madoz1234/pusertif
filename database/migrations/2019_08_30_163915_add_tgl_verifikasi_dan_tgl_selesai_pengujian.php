<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTglVerifikasiDanTglSelesaiPengujian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pengujian_detail', function (Blueprint $table) {
            $table->string('tgl_selesai', 200)->nullable()->after('status_laporan');
        });

        Schema::table('trans_pengujian_detail_pelaksana', function (Blueprint $table) {
            $table->string('tgl_selesai', 200)->nullable()->after('status_pengembalian');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_pengujian_detail', function (Blueprint $table) {
            $table->dropColumn('tgl_selesai');
        });

        Schema::table('trans_pengujian_detail_pelaksana', function (Blueprint $table) {
            $table->dropColumn('tgl_selesai');
        });
    }
}
