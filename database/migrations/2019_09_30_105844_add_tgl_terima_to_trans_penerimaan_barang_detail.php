<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTglTerimaToTransPenerimaanBarangDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_penerimaan_barang_detail', function (Blueprint $table) {
            $table->string('tanggal_terima',200)->nullable()->default(\Carbon\Carbon::now())->after('status');
        });

        Schema::table('log_trans_penerimaan_barang_detail', function (Blueprint $table) {
            $table->string('tanggal_terima',200)->nullable()->default(\Carbon\Carbon::now())->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {	
    	Schema::table('log_trans_penerimaan_barang_detail', function (Blueprint $table) {
            $table->dropColumn('tanggal_terima');
        });

        Schema::table('trans_penerimaan_barang_detail', function (Blueprint $table) {
            $table->dropColumn('tanggal_terima');
        });
    }
}
