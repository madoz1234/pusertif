<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
    // return redirect('home');
// });
Route::get('/', 'HomeController@index')->name('home');
// Route::get('/tracking', 'HomeController@tracking')->name('tracking');
Route::get('/cari-perusahaan', 'HomeController@perusahaan');
Route::get('/aktivasi/{id}/', 'HomeController@aktivasi');
Route::post('/registrasi', 'HomeController@registrasi');

Route::group(['prefix' => 'ajax/option', 'namespace' => 'Ajax'], function(){
    Route::post('jadwal-kaji', 'ChainOptionController@getJadwalKaji');
    Route::post('lingkup', 'ChainOptionController@getLingkup');
    Route::post('lingkupOrder', 'ChainOptionController@getLingkupOrder');
    Route::post('prioritas', 'ChainOptionController@getPrioritas');
    Route::post('jenis', 'ChainOptionController@getJenisPengujian');
    Route::post('jenisOrder', 'ChainOptionController@getJenisPengujianOrder');
    Route::post('pelanggan', 'ChainOptionController@getPelanggan');
    Route::post('pelanggan-spm', 'ChainOptionController@getPelangganSpm');
    Route::post('disposisi', 'ChainOptionController@getDisposisi');
    Route::post('disposisi-msb', 'ChainOptionController@getDisposisiMsb');
    Route::post('perbaikan-msb', 'ChainOptionController@getPerbaikanMsb');
    Route::post('kaji_ulang', 'ChainOptionController@getVa');
    Route::post('cek-va', 'ChainOptionController@getcekVa');
    Route::post('cek-va2', 'ChainOptionController@getcekVa2');
    Route::post('pp_detail', 'ChainOptionController@getDetailPP');
    Route::get('chart-kaji', 'ChainOptionController@getChartKaji');
    Route::get('chart-pengujian', 'ChainOptionController@getChartPengujian');
    Route::get('chart-laporan', 'ChainOptionController@getChartLaporan');
});

Route::group(['prefix' => 'option', 'namespace' => 'Ajax'], function(){
    Route::get('showNextPengujian/{id}', 'ChainOptionController@nextGetPengujian');
});

Route::group(['namespace' => 'FrontEnd'], function(){
    Route::resource('tracking', 'TrackingController');
    Route::resource('hubungi', 'HubungiController');
    Route::post('tracking/search', 'TrackingController@search');
});


Route::post('/langs', 'Ajax\LanguageController@show');

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('download/{id}/{type}', 'DownloadController@multiple');
Route::get('download-briefing/{id}/{type}/{jenis?}', 'DownloadController@multipleBriefing');
Route::get('download-laporan-pengujian/{id}', 'DownloadController@laporanPengujian');
Route::get('download-va/{id}', 'DownloadController@va');
Route::get('download-tanggapan/{id}', 'DownloadController@tanggapan');
// Route::group(['middleware' => ['auth']], function () {
Route::group(['middleware' => 'auth'], function () {
    Route::get('/backend', 'BackendController@index');
    Route::resource('backend/profile', 'Konfigurasi\ProfileController');

    Route::group(['prefix' => 'konfigurasi', 'namespace' => 'Konfigurasi'], function(){
        Route::post('users/grid', 'UsersController@grid');
        Route::resource('users', 'UsersController');
       
        Route::put('roles/{id}/grant', 'RolesController@grant');
        Route::post('roles/grid', 'RolesController@grid');
        Route::resource('roles', 'RolesController');
    });
    
    Route::group(['prefix' => 'permintaan', 'namespace' => 'Permintaan'], function(){
        Route::get('spm/get-detail-pengujian', 'PendaftaranPengujianSPMController@getDetail');
        Route::get('spm/upload/{lingkup}', 'PendaftaranPengujianSPMController@upload');
        Route::get('spm/export/{lingkup}', 'PendaftaranPengujianSPMController@exportTemplate');
        Route::post('spm/request/data-upload', 'PendaftaranPengujianSPMController@cekUpload');
        Route::get('spm/lihat-layanan', 'PendaftaranPengujianSPMController@lihatLayanan');
        Route::get('spm/upload','PendaftaranPengujianSPMController@upload');
        Route::post('spm/upload-excel','PendaftaranPengujianSPMController@storeUpload');
        Route::get('spm/buat', 'PendaftaranPengujianSPMController@buat');
        Route::get('spm/ubah', 'PendaftaranPengujianSPMController@ubah');
        Route::get('spm/lihat-jadwal/{data}', 'PendaftaranPengujianSPMController@lihat');
        Route::get('spm/{id}/detil', 'PendaftaranPengujianSPMController@detil');
        Route::get('spm/download/{id}', 'PendaftaranPengujianSPMController@download');
        Route::post('spm/request/item-uji', 'PendaftaranPengujianSPMController@cekRequestItemUji');
        Route::post('spm/grid', 'PendaftaranPengujianSPMController@grid');
        Route::post('spm/histori', 'PendaftaranPengujianSPMController@histori');
        Route::post('spm/search-layanan', 'PendaftaranPengujianSPMController@searchLayanan');
        // Route::post('spm/histori', 'PendaftaranPengujianSPMController@gridHistori');
        Route::get('spm/{id}/konfirmasi', 'PendaftaranPengujianSPMController@konfirmasi');
        Route::get('spm/{id}/detail', 'PendaftaranPengujianSPMController@detail');
        Route::get('spm/{id}/preview', 'PendaftaranPengujianSPMController@preview');
        Route::get('spm/{id}/preview-multiple/{type}', 'PendaftaranPengujianSPMController@previewMultiple');
        Route::resource('spm', 'PendaftaranPengujianSPMController');
    });

    Route::group(['prefix' => 'penerimaan', 'namespace' => 'Penerimaan'], function(){
        Route::get('surat/import-ams', 'PenerimaanSuratController@importAms');
        Route::post('surat/save-ams', 'PenerimaanSuratController@saveAms');
        Route::post('surat/grid', 'PenerimaanSuratController@grid');
        Route::post('surat/ams', 'PenerimaanSuratController@ams');
        Route::post('surat/spm', 'PenerimaanSuratController@spm'); 
        Route::post('surat/histori', 'PenerimaanSuratController@histori');
        // Route::post('surat/dialihkan', 'PenerimaanSuratController@dialihkan');
        Route::get('surat/download/{id}', 'PenerimaanSuratController@download');
        Route::get('surat/buat', 'PenerimaanSuratController@buat');
        Route::get('surat/{id}/detil', 'PenerimaanSuratController@detil');
        Route::get('surat/{id}/lihat', 'PenerimaanSuratController@lihat');
        Route::get('surat/{id}/lihat-multiple/{type}', 'PenerimaanSuratController@lihatMultiple');
        Route::get('surat/{id}/perbaikan-message', 'PenerimaanSuratController@perbaikanMessage');
        Route::get('surat/{id}/ubah', 'PenerimaanSuratController@ubah');
        Route::get('surat/{id}/ubah-ams', 'PenerimaanSuratController@ubahAms');
        Route::post('surat/{id}/saveUbah', 'PenerimaanSuratController@saveUbah');
        Route::get('surat/{id}/tolakOnline', 'PenerimaanSuratController@tolakOnline');
        Route::get('surat/{id}/detil-terkirim', 'PenerimaanSuratController@detilTerkirim');
        Route::get('surat/{id}/detil-tolak', 'PenerimaanSuratController@detilTolak');
        Route::get('surat/detail', 'PenerimaanSuratController@detail');
        Route::get('surat/dispo', 'PenerimaanSuratController@dispo');
        Route::post('surat/request/item-uji', 'PenerimaanSuratController@cekRequestItemUji');
        Route::resource('surat', 'PenerimaanSuratController');
    });

    Route::group(['prefix' => 'aktivasiuser', 'namespace' => 'AktivasiUser'], function(){
        Route::post('aktivasi-user/grid', 'AktivasiUserController@grid');
        Route::post('aktivasi-user/grid2', 'AktivasiUserController@grid2');
        Route::put('aktivasi-user/aktivasi-ulang/{id}', 'AktivasiUserController@aktivasiUlang');
        Route::get('aktivasi-user/{id}/ulang', 'AktivasiUserController@ulang');
        Route::get('aktivasi-user/cari/{id}', 'AktivasiUserController@cari');
        Route::get('aktivasi-user/perusahaan/{id}', 'AktivasiUserController@perusahaan');
        Route::get('aktivasi-user/{id}/lihat', 'AktivasiUserController@lihat');
        Route::get('aktivasi-user/{id}/download', 'AktivasiUserController@download');
        Route::resource('aktivasi-user', 'AktivasiUserController');
    });

    Route::group(['namespace' => 'LaporanPengujian'], function(){
        Route::post('laporan-pengujian/grid', 'LaporanPengujianController@grid');
        Route::get('laporan-pengujian/download/{id}', 'LaporanPengujianController@download');
        Route::get('laporan-pengujian/{id}/detil', 'LaporanPengujianController@detil');
        Route::get('laporan-pengujian/{id}/preview', 'LaporanPengujianController@preview');
        Route::get('laporan-pengujian/{id}/preview-multiple/{type}', 'LaporanPengujianController@previewMultiple');
        Route::resource('laporan-pengujian/', 'LaporanPengujianController');
    });

    Route::group(['namespace' => 'PengirimanLaporan'], function(){
        Route::post('pengiriman-laporan/grid', 'PengirimanLaporanController@grid');
        Route::post('pengiriman-laporan/historis', 'PengirimanLaporanController@historis');
        Route::get('pengiriman-laporan/simpan/{data}', 'PengirimanLaporanController@pengirimanLaporan');
        Route::get('pengiriman-laporan/download/{id}', 'PengirimanLaporanController@download');
        Route::get('pengiriman-laporan/{id}/detil', 'PengirimanLaporanController@detil');
        Route::put('pengiriman-laporan/{id}/kirimLaporan', 'PengirimanLaporanController@saveKirimLaporan');
        Route::get('pengiriman-laporan/{id}/preview', 'PengirimanLaporanController@preview');
        Route::get('pengiriman-laporan/{id}/preview-multiple/{type}', 'PengirimanLaporanController@previewMultiple');
        Route::resource('pengiriman-laporan/', 'PengirimanLaporanController');
    });

    Route::group(['namespace' => 'HistoryData'], function(){
        Route::get('history-data/grid', 'HistoryDataController@grid');
        Route::get('history-data/print/{id}/{tipe}', 'HistoryDataController@print');
        // Route::get('/download/{menu}/{id}', 'HistoryDataController@download');
        Route::resource('history-data/', 'HistoryDataController');
    });

    Route::group(['prefix' => 'backend', 'namespace' => 'Backend'], function(){
        Route::resource('dashboard', 'DashboardController');
    });

    Route::group(['namespace' => 'CloseOrder'], function(){
        Route::get('close-order/{id}/closeOrderKalibrasi', 'CloseOrderController@closeOrderKalibrasi');
        Route::put('close-order/{id}/saveCloseKalibrasi', 'CloseOrderController@saveCloseKalibrasi');
        Route::get('close-order/{id}/close', 'CloseOrderController@closeOrder');
        Route::get('close-order/{data}/closeUji/{max}', 'CloseOrderController@closeOrderUji');
        Route::get('close-order/{id}/pembatalan', 'CloseOrderController@pembatalan');
        Route::get('close-order/{id}/detail', 'CloseOrderController@detail');
        Route::get('close-order/{id}/edit', 'CloseOrderController@edit');
        Route::post('close-order/grid', 'CloseOrderController@grid');
        Route::post('close-order/historis', 'CloseOrderController@historis');
        Route::get('close-order/download/{id}', 'CloseOrderController@download');
        Route::get('close-order/simpan/{data}', 'CloseOrderController@pengirimanLaporan');
        Route::put('close-order/{id}/kirimLaporan', 'CloseOrderController@saveKirimLaporan');
        Route::get('close-order/simpanClose/{data}/{tipe}', 'CloseOrderController@simpanClose');
        Route::get('close-order/{id}/preview', 'CloseOrderController@preview');
        Route::get('close-order/{id}/preview-multiple/{type}', 'CloseOrderController@previewMultiple');
        Route::resource('close-order/', 'CloseOrderController');
    });

    Route::group(['namespace' => 'Virtual'], function(){
    	Route::get('virtual-account/updateVa', 'VirtualAccountController@updateVa');
        Route::post('virtual-account/grid', 'VirtualAccountController@grid');
        Route::post('virtual-account/menunggu', 'VirtualAccountController@menunggu');
        Route::post('virtual-account/submit', 'VirtualAccountController@submit');
        Route::post('virtual-account/nonaktif', 'VirtualAccountController@nonaktif');
        Route::post('virtual-account/download', 'VirtualAccountController@download');
        Route::get('virtual-account/download/{data}', 'VirtualAccountController@downloadExcelNewFormat');
        Route::get('virtual-account/downloadUlang/{data}', 'VirtualAccountController@downloadExcelNewFormatUlang');
        Route::get('virtual-account/group/{data}', 'VirtualAccountController@detailGroup');
        Route::get('virtual-account/{id}/detail', 'VirtualAccountController@detail');
        Route::get('virtual-account/{id}/detil', 'VirtualAccountController@detil');
        Route::get('virtual-account/{id}/aktif','VirtualAccountController@aktif');
        Route::get('virtual-account/{id}/aktivasi','VirtualAccountController@aktivasi');
        Route::put('virtual-account/{id}/saveAktivasi','VirtualAccountController@saveAktivasi');
        Route::get('virtual-account/{id}/preview', 'VirtualAccountController@preview');
        Route::get('virtual-account/{id}/preview-multiple/{type}', 'VirtualAccountController@previewMultiple');
        Route::resource('virtual-account', 'VirtualAccountController');
    });

    Route::group(['namespace'=>'NotaDinas'],function(){
        Route::post('nota-dinas/grid','NotaDinasController@grid');
        Route::get('nota-dinas/download/{id}', 'NotaDinasController@download');
        Route::post('nota-dinas/histori','NotaDinasController@histori');
        Route::get('nota-dinas/{id}/detail', 'NotaDinasController@detail');
        Route::get('nota-dinas/{id}/detil', 'NotaDinasController@detil');
        Route::get('nota-dinas/{id}/preview', 'NotaDinasController@preview');
        Route::get('nota-dinas/{id}/preview-multiple/{type}', 'NotaDinasController@previewMultiple');
        Route::resource('nota-dinas','NotaDinasController');
    });

    Route::group(['namespace'=>'PengembalianDana'],function(){
        Route::get('pengembalian-dana/{id}/pengembalian','PengembalianDanaController@pengembalian');
        Route::get('pengembalian-dana/{id}/buat-pengembalian','PengembalianDanaController@buatPengembalian');
        Route::get('pengembalian-dana/{id}/detail','PengembalianDanaController@detailPengembalian');
        Route::put('pengembalian-dana/{id}/savePengembalian','PengembalianDanaController@savePengembalian');
        Route::put('pengembalian-dana/{id}/savePengembalianDanaBarang','PengembalianDanaController@savePengembalianDanaBarang');
        Route::get('pengembalian-dana/{id}/detil','PengembalianDanaController@detil');
        Route::get('pengembalian-dana/{id}/detail-data','PengembalianDanaController@detail');
        Route::post('pengembalian-dana/grid','PengembalianDanaController@grid');
        Route::post('pengembalian-dana/histori','PengembalianDanaController@histori');
        Route::post('pengembalian-dana/kembali','PengembalianDanaController@kembali');
        Route::get('pengembalian-dana/download/{id}', 'PengembalianDanaController@download');
        Route::post('pengembalian-dana/historiKembali','PengembalianDanaController@historiKembali');
        Route::get('pengembalian-dana/{id}/preview', 'PengembalianDanaController@preview');
        Route::get('pengembalian-dana/{id}/preview-multiple/{type}', 'PengembalianDanaController@previewMultiple');
        Route::resource('pengembalian-dana','PengembalianDanaController');
    });

    Route::group(['namespace'=>'PenerimaanBarangUji'],function(){
        Route::post('penerimaan-barang-uji/grid', 'PenerimaanBarangUjiController@grid');
        Route::get('penerimaan-barang-uji/download/{id}', 'PenerimaanBarangUjiController@download');
        Route::post('penerimaan-barang-uji/progress', 'PenerimaanBarangUjiController@progress');
        Route::post('penerimaan-barang-uji/historis', 'PenerimaanBarangUjiController@historis');
        Route::get('penerimaan-barang-uji/{id}/detail', 'PenerimaanBarangUjiController@detail');
        Route::get('penerimaan-barang-uji/{id}/detil', 'PenerimaanBarangUjiController@detil');
        Route::get('penerimaan-barang-uji/{id}/buat', 'PenerimaanBarangUjiController@buat');
        Route::get('penerimaan-barang-uji/{id}/ubah', 'PenerimaanBarangUjiController@ubah');
        Route::put('penerimaan-barang-uji/{id}/saveUbah', 'PenerimaanBarangUjiController@saveUbah');
        Route::get('penerimaan-barang-uji/{id}/cetakSurat', 'PenerimaanBarangUjiController@printSuratPdf');
        Route::get('penerimaan-barang-uji/{id}/cetakKartu', 'PenerimaanBarangUjiController@printKartuPdf');
        Route::get('penerimaan-barang-uji/{id}/preview', 'PenerimaanBarangUjiController@preview');
        Route::get('penerimaan-barang-uji/{id}/preview-multiple/{type}', 'PenerimaanBarangUjiController@previewMultiple');
        Route::resource('penerimaan-barang-uji','PenerimaanBarangUjiController');
    });

    Route::group(['namespace'=>'PengembalianBarangUji'],function(){
        Route::post('pengembalian-barang-uji/grid', 'PengembalianBarangUjiController@grid');
        Route::get('pengembalian-barang-uji/download/{id}', 'PengembalianBarangUjiController@download');
        Route::post('pengembalian-barang-uji/historis', 'PengembalianBarangUjiController@historis');
        Route::get('pengembalian-barang-uji/{id}/buat-pengembalian', 'PengembalianBarangUjiController@buatPengembalian');
        Route::get('pengembalian-barang-uji/simpan/{data}', 'PengembalianBarangUjiController@buatPengembalianBarangUji');
        Route::put('pengembalian-barang-uji/{id}/simpanPengembalian', 'PengembalianBarangUjiController@simpanPengembalian');
        Route::get('pengembalian-barang-uji/{id}/cetakSurat', 'PengembalianBarangUjiController@printSuratPdf');
        Route::get('pengembalian-barang-uji/{id}/detil', 'PengembalianBarangUjiController@detil');
        Route::get('pengembalian-barang-uji/{id}/preview', 'PengembalianBarangUjiController@preview');
        Route::get('pengembalian-barang-uji/{id}/preview-multiple/{type}', 'PengembalianBarangUjiController@previewMultiple');
        Route::resource('pengembalian-barang-uji','PengembalianBarangUjiController');
    });

    Route::group(['namespace' => 'Surat'], function(){
        Route::post('surat-penawaran/filter', 'SuratPenawaranController@indexFilter');
        Route::post('surat-penawaran/saveKomponen', 'SuratPenawaranController@saveKomponen');
        Route::post('surat-penawaran/saveAdendum', 'SuratPenawaranController@saveAdendum');
        Route::put('surat-penawaran/{id}/updateKomponen', 'SuratPenawaranController@updateKomponen');
        Route::get('surat-penawaran/download/{id}', 'SuratPenawaranController@download');
        Route::get('surat-penawaran/{id}/detail/{jenis}', 'SuratPenawaranController@detail');
        Route::get('surat-penawaran/{id}/buat/{data}/{tipe}', 'SuratPenawaranController@buat');
        Route::get('surat-penawaran/{id}/adendum/{data}/{tipe}', 'SuratPenawaranController@adendum');
        Route::get('surat-penawaran/{id}/adendum/{tipe}', 'SuratPenawaranController@buatAdendum');
        Route::get('surat-penawaran/{id}/buat/{tipe}', 'SuratPenawaranController@buatData');
        Route::post('surat-penawaran/kirim', 'SuratPenawaranController@kirim');
        Route::get('surat-penawaran/{id}/ubah', 'SuratPenawaranController@ubah');
        Route::get('surat-penawaran/{id}/detil', 'SuratPenawaranController@detil');
        Route::get('surat-penawaran/{id}/kirim/{data}', 'SuratPenawaranController@upload');
        Route::post('surat-penawaran/{id}/virtual', 'SuratPenawaranController@virtual');
        Route::post('surat-penawaran/{id}/virtual2', 'SuratPenawaranController@virtual2');
        // Route::get('surat-penawaran/{id}/virtual', 'SuratPenawaranController@virtual2');
        Route::get('surat-penawaran/{nomor}/upload', 'SuratPenawaranController@upload');
        Route::put('surat-penawaran/saveUpload', 'SuratPenawaranController@saveUpload');
        Route::get('surat-penawaran/konfirmasi-pembayaran/{id}', 'SuratPenawaranController@konfirmasiPembayaran');
        Route::get('surat-penawaran/{id}/upload-penolakan', 'SuratPenawaranController@uploadPenolakan');
        Route::put('surat-penawaran/{id}/saveDataPenolakan', 'SuratPenawaranController@savePenolakan');
        Route::get('surat-penawaran/{id}/preview', 'SuratPenawaranController@preview');
        Route::get('surat-penawaran/{id}/preview-multiple/{type}', 'SuratPenawaranController@previewMultiple');
        Route::get('surat-penawaran/{id}/preview-surat', 'SuratPenawaranController@previewSurat');
        Route::get('surat-penawaran/download-surat/{id}', 'SuratPenawaranController@downloadSurat');
        Route::get('surat-penawaran/{id}/preview-penolakan', 'SuratPenawaranController@previewPenolakan');
        Route::get('surat-penawaran/download-penolakan/{id}', 'SuratPenawaranController@downloadPenolakan');
        Route::resource('surat-penawaran', 'SuratPenawaranController');
    });

    Route::group(['namespace' => 'Pengaduan'], function(){
        Route::post('pengaduan/grid', 'PengaduanController@grid');
        Route::get('pengaduan/download/{id}', 'PengaduanController@download');
        Route::get('pengaduan/tanggapan/{id}', 'PengaduanController@tanggapan');
        Route::resource('pengaduan', 'PengaduanController');
    });

    Route::group(['prefix' => 'master', 'namespace' => 'Master'], function(){
        Route::post('jenis-layanan/grid', 'PelayananController@grid');
        Route::resource('jenis-layanan', 'PelayananController');

        Route::post('lingkup/grid', 'LingkupController@grid');
        Route::resource('lingkup', 'LingkupController');

        Route::post('jenis-pengujian/grid', 'JenisPengujianController@grid');
        Route::resource('jenis-pengujian', 'JenisPengujianController');

        Route::post('mata-uji/grid', 'MataUjiController@grid');
        Route::resource('mata-uji', 'MataUjiController');

        Route::post('jenis-alat/grid', 'JenisAlatController@grid');
        Route::resource('jenis-alat', 'JenisAlatController');

        Route::post('perusahaan/grid', 'PerusahaanController@grid');
        Route::resource('perusahaan', 'PerusahaanController');

        Route::post('contact/grid', 'ContactController@grid');
        Route::resource('contact', 'ContactController');

        Route::post('survei/grid', 'SurveiController@grid');
        Route::get('survei/{id}/detail', 'SurveiController@detail');
        Route::resource('survei', 'SurveiController');

        Route::post('satuan/grid', 'SatuanController@grid');
        Route::resource('satuan', 'SatuanController');

        Route::post('hari-kerja/grid','HariKerjaController@grid');
        Route::resource('hari-kerja','HariKerjaController');

        Route::post('prioritas/grid','PrioritasController@grid');
        Route::resource('prioritas','PrioritasController');
    });

    Route::group(['prefix' => 'kaji-ulang', 'namespace' => 'KajiUlang'], function(){
        Route::post('proses-kaji-ulang/grid', 'KajiUlangController@grid');
        Route::post('proses-kaji-ulang/diskusi', 'KajiUlangController@diskusi');
        Route::post('proses-kaji-ulang/histori', 'KajiUlangController@histori');
        Route::get('proses-kaji-ulang/{id}/detail', 'KajiUlangController@detail');
        Route::get('proses-kaji-ulang/{id}/ubah', 'KajiUlangController@Ubah');
        Route::put('proses-kaji-ulang/{id}/saveUbah', 'KajiUlangController@saveUbah');
        Route::get('proses-kaji-ulang/download/{id}', 'KajiUlangController@download');
        Route::get('proses-kaji-ulang/{id}/dispo', 'KajiUlangController@dispo');
        Route::get('proses-kaji-ulang/{id}/perbaikan', 'KajiUlangController@perbaikan');
        Route::get('proses-kaji-ulang/{id}/perbaikan-message', 'KajiUlangController@perbaikanMessage');
        Route::get('proses-kaji-ulang/{id}/briefingJadwal', 'KajiUlangController@briefingJadwal');
        Route::get('proses-kaji-ulang/{id}/briefingNotulen', 'KajiUlangController@briefingNotulen');
        Route::put('proses-kaji-ulang/{id}/saveDispo', 'KajiUlangController@saveDispo');
        Route::put('proses-kaji-ulang/{id}/savePerbaikan', 'KajiUlangController@savePerbaikan');
        Route::put('proses-kaji-ulang/{id}/saveBriefingJadwal', 'KajiUlangController@saveBriefingJadwal');
        Route::put('proses-kaji-ulang/{id}/saveBriefingNotulen', 'KajiUlangController@saveBriefingNotulen');
        Route::get('proses-kaji-ulang/lihatJadwal', 'KajiUlangController@loadJadwal');
        Route::post('proses-kaji-ulang/search-jadwal', 'KajiUlangController@loadDataJadwal');
        Route::get('proses-kaji-ulang/{id}/preview', 'KajiUlangController@preview');
        Route::get('proses-kaji-ulang/{id}/preview-multiple/{type}', 'KajiUlangController@previewMultiple');
        Route::resource('proses-kaji-ulang', 'KajiUlangController');
    });

    Route::group(['namespace' => 'Reschedule'], function(){
        Route::post('reschedules/grid', 'RescheduleController@grid');
        Route::post('reschedules/histori', 'RescheduleController@historis');
        Route::get('reschedules/{id}/detil', 'RescheduleController@detil');
        Route::get('reschedules/download/{id}', 'RescheduleController@detil');
        Route::resource('reschedules', 'RescheduleController');
    });

    Route::group(['namespace' => 'PengujianSerahTerima'], function(){
        Route::get('pengujian-serah-terima/get-detail-pengujian', 'PengujianSerahTerimaController@getDetail');
        Route::get('pengujian-serah-terima/upload/{lingkup}', 'PengujianSerahTerimaController@upload');
        Route::get('pengujian-serah-terima/export/{lingkup}', 'PengujianSerahTerimaController@exportTemplate');
        Route::post('pengujian-serah-terima/request/data-upload', 'PengujianSerahTerimaController@cekUpload');
        Route::get('pengujian-serah-terima/lihat-layanan', 'PengujianSerahTerimaController@lihatLayanan');
        Route::get('pengujian-serah-terima/upload','PengujianSerahTerimaController@upload');
        Route::post('pengujian-serah-terima/upload-excel','PengujianSerahTerimaController@storeUpload');
        Route::get('pengujian-serah-terima/buat', 'PengujianSerahTerimaController@buat');
        Route::get('pengujian-serah-terima/ubah', 'PengujianSerahTerimaController@ubah');
        Route::get('pengujian-serah-terima/lihat-jadwal/{data}', 'PengujianSerahTerimaController@lihat');
        Route::get('pengujian-serah-terima/{id}/detil', 'PengujianSerahTerimaController@detil');
        Route::get('pengujian-serah-terima/download/{id}', 'PengujianSerahTerimaController@download');
        Route::get('pengujian-serah-terima/print-qr-code/{id}', 'PengujianSerahTerimaController@printQrCode');
        Route::post('pengujian-serah-terima/request/item-uji', 'PengujianSerahTerimaController@cekRequestItemUji');
        Route::post('pengujian-serah-terima/grid', 'PengujianSerahTerimaController@grid');
        Route::post('pengujian-serah-terima/histori', 'PengujianSerahTerimaController@histori');
        Route::post('pengujian-serah-terima/search-layanan', 'PengujianSerahTerimaController@searchLayanan');
        Route::get('pengujian-serah-terima/{id}/konfirmasi', 'PengujianSerahTerimaController@konfirmasi');
        Route::get('pengujian-serah-terima/{id}/detail', 'PengujianSerahTerimaController@detail');
        Route::get('pengujian-serah-terima/{id}/cetakQrCode', 'PengujianSerahTerimaController@printBarcodePdf');
        Route::get('pengujian-serah-terima/{id}/preview', 'PengujianSerahTerimaController@preview');
        Route::get('pengujian-serah-terima/{id}/preview-multiple/{type}', 'PengujianSerahTerimaController@previewMultiple');
        Route::resource('pengujian-serah-terima', 'PengujianSerahTerimaController');
    });
    Route::group(['prefix' => 'konfirmasi', 'namespace' => 'KonfirmasiPembayaran'], function(){
        Route::post('konfirmasi-pembayaran/belum', 'KonfirmasiPembayaranController@belum');
        Route::post('konfirmasi-pembayaran/kurang', 'KonfirmasiPembayaranController@kurang');
        Route::post('konfirmasi-pembayaran/lebih', 'KonfirmasiPembayaranController@lebih');
        Route::post('konfirmasi-pembayaran/lunas', 'KonfirmasiPembayaranController@lunas');
        Route::post('konfirmasi-pembayaran/terbit', 'KonfirmasiPembayaranController@terbit');
        Route::post('konfirmasi-pembayaran/histori','KonfirmasiPembayaranController@histori');
        Route::get('konfirmasi-pembayaran/buat/{data}', 'KonfirmasiPembayaranController@buat');
        Route::get('konfirmasi-pembayaran/{id}/detail', 'KonfirmasiPembayaranController@detail');
        Route::get('konfirmasi-pembayaran/{id}/detil', 'KonfirmasiPembayaranController@detil');
        Route::get('konfirmasi-pembayaran/{id}/aktif','KonfirmasiPembayaranController@aktif');
        Route::get('konfirmasi-pembayaran/download/{id}', 'KonfirmasiPembayaranController@download');
        Route::get('konfirmasi-pembayaran/ubahKurang/{data}', 'KonfirmasiPembayaranController@ubahKurang');
        Route::put('konfirmasi-pembayaran/{id}/saveKurang', 'KonfirmasiPembayaranController@saveKurang');
        Route::get('konfirmasi-pembayaran/{id}/lihat', 'KonfirmasiPembayaranController@lihat');
        Route::get('konfirmasi-pembayaran/{id}/preview', 'KonfirmasiPembayaranController@preview');
        Route::get('konfirmasi-pembayaran/{id}/preview-multiple/{type}', 'KonfirmasiPembayaranController@previewMultiple');
        Route::resource('konfirmasi-pembayaran', 'KonfirmasiPembayaranController');

    });

    Route::group(['prefix' => 'realisasi', 'namespace' => 'RealisasiBiaya'], function(){
        Route::get('realisasi-biaya/export', 'RealisasiBiayaController@exportTemplate');
        Route::put('realisasi-biaya/{id}/saveUpload', 'RealisasiBiayaController@saveUpload');
        Route::get('realisasi-biaya/{id}/upload', 'RealisasiBiayaController@upload');
        Route::get('realisasi-biaya/upload-template', 'RealisasiBiayaController@uploadTemplate');
        Route::post('realisasi-biaya/upload-template', 'RealisasiBiayaController@uploadTemplateSave');
        Route::put('realisasi-biaya/{id}/saveUploadUlang', 'RealisasiBiayaController@saveUploadUlang');
        Route::get('realisasi-biaya/{id}/uploadUlang', 'RealisasiBiayaController@uploadUlang');
        Route::post('realisasi-biaya/grid','RealisasiBiayaController@grid');
        Route::post('realisasi-biaya/histori','RealisasiBiayaController@histori');
        Route::get('realisasi-biaya/{id}/detail', 'RealisasiBiayaController@detail');
        Route::get('realisasi-biaya/{id}/detil', 'RealisasiBiayaController@detil');
        Route::get('realisasi-biaya/download/{id}', 'RealisasiBiayaController@download');
        Route::get('realisasi-biaya/{id}/preview', 'RealisasiBiayaController@preview');
        Route::get('realisasi-biaya/{id}/preview-multiple/{type}', 'RealisasiBiayaController@previewMultiple');
        Route::resource('realisasi-biaya','RealisasiBiayaController');
    });

    Route::group(['prefix' => 'pengujian', 'namespace' => 'Pengujian'], function(){
        Route::post('layanan-kalibrasi/grid', 'KalibrasiController@grid');
        Route::get('layanan-kalibrasi/{id}/dispo/{tipe}', 'KalibrasiController@dispo');
        Route::put('layanan-kalibrasi/{id}/saveDispo', 'KalibrasiController@saveDispo');
        Route::put('layanan-kalibrasi/{id}/saveDispoPengujian', 'KalibrasiController@saveDispoPengujian');
        Route::put('layanan-kalibrasi/{id}/saveDispoUpload', 'KalibrasiController@saveDispoUpload');
        Route::get('layanan-kalibrasi/{id}/buat', 'KalibrasiController@buat');
        Route::get('layanan-kalibrasi/{id}/edit', 'KalibrasiController@edit');
        Route::get('layanan-kalibrasi/{id}/dispoPelaksana', 'KalibrasiController@dispoPelaksana');
        Route::get('layanan-kalibrasi/{id}/dispoAsmen', 'KalibrasiController@dispoAsmen');
        Route::get('layanan-kalibrasi/{id}/dispoMsb', 'KalibrasiController@dispoMsb');
        Route::get('layanan-kalibrasi/{id}/dispoYan', 'KalibrasiController@dispoYan');
        Route::get('layanan-kalibrasi/{id}/pengujianPelaksana', 'KalibrasiController@pengujianPelaksana');
        Route::get('layanan-kalibrasi/{id}/savedispoPelaksana', 'KalibrasiController@savedispoPelaksana');
        Route::put('layanan-kalibrasi/{id}/saveHasilPengujian', 'KalibrasiController@saveHasilPengujian');
        Route::get('layanan-kalibrasi/{id}/verifikasi', 'KalibrasiController@verifikasi');
        Route::get('layanan-kalibrasi/{id}/verifikasi-item_uji/{tipe}', 'KalibrasiController@verifikasiItemUji');
        Route::get('layanan-kalibrasi/{id}/pengujian-item_uji/{tipe}', 'KalibrasiController@pengujianItemUji');
        Route::get('layanan-kalibrasi/{id}/verifikasi-pengujian', 'KalibrasiController@verifikasiPengujian');
        Route::get('layanan-kalibrasi/{id}/verifikasi-laporan-pengujian/{tipe}/{jenis}', 'KalibrasiController@verifikasiLaporanPengujian');
        Route::get('layanan-kalibrasi/{id}/upload-final', 'KalibrasiController@uploadLaporanFinal');
        Route::get('layanan-kalibrasi/{id}/verifikasi-laporan', 'KalibrasiController@verifikasiPengujian');
        Route::get('layanan-kalibrasi/{id}/edit-pelaksana', 'KalibrasiController@editPelaksana');
        Route::get('layanan-kalibrasi/{id}/edit-laporan-pelaksana', 'KalibrasiController@editLaporanPelaksana');
        Route::get('layanan-kalibrasi/{id}/edit-laporan-msb', 'KalibrasiController@editLaporanMsb');
        Route::get('layanan-kalibrasi/{id}/edit-laporan-final', 'KalibrasiController@editLaporanFinal');
        Route::put('layanan-kalibrasi/{id}/savePelaksana', 'KalibrasiController@savePelaksana');
        Route::put('layanan-kalibrasi/{id}/saveLaporanPelaksana', 'KalibrasiController@saveLaporanPelaksana');
        Route::put('layanan-kalibrasi/{id}/savePenyelesaian', 'KalibrasiController@savePenyelesaian');
        Route::put('layanan-kalibrasi/{id}/simpanData', 'KalibrasiController@simpanData');
        Route::put('layanan-kalibrasi/{id}/uploadLaporan', 'KalibrasiController@uploadLaporan');
        Route::get('layanan-kalibrasi/download/{id}', 'KalibrasiController@download');
        Route::get('layanan-kalibrasi/{id}/detail', 'KalibrasiController@detil');
        Route::get('layanan-kalibrasi/simpan/{data}', 'KalibrasiController@buatLaporan');
        Route::get('layanan-kalibrasi/simpanLaporan/{data}', 'KalibrasiController@buatLaporan');
        Route::get('layanan-kalibrasi/simpanLaporanData/{data}/{tipe}', 'KalibrasiController@simpanLaporanData');
        Route::get('layanan-kalibrasi/simpanDispo/{data}/{tipe}', 'KalibrasiController@simpanDispo');
        Route::get('layanan-kalibrasi/simpanPengujianItemUji/{data}/{tipe}', 'KalibrasiController@simpanPengujianItemUji');
        Route::post('layanan-kalibrasi/grid2', 'KalibrasiController@grid2');
        Route::post('layanan-kalibrasi/grid3', 'KalibrasiController@grid3');
        Route::post('layanan-kalibrasi/grid4', 'KalibrasiController@grid4');
        Route::post('layanan-kalibrasi/grid5', 'KalibrasiController@grid5');
        Route::get('layanan-kalibrasi/{id}/cetakBarcode', 'KalibrasiController@printBarcodePdf');
        Route::get('layanan-kalibrasi/{id}/preview', 'KalibrasiController@preview');
        Route::get('layanan-kalibrasi/{id}/preview-multiple/{type}', 'KalibrasiController@previewMultiple');
        Route::resource('layanan-kalibrasi', 'KalibrasiController');

        Route::post('layanan-tr/grid', 'TrController@grid');
        Route::get('layanan-tr/{id}/dispo/{tipe}', 'TrController@dispo');
        Route::put('layanan-tr/{id}/saveDispo', 'TrController@saveDispo');
        Route::put('layanan-tr/{id}/saveDispoPengujian', 'TrController@saveDispoPengujian');
        Route::put('layanan-tr/{id}/saveDispoUpload', 'TrController@saveDispoUpload');
        Route::get('layanan-tr/{id}/buat', 'TrController@buat');
        Route::get('layanan-tr/{id}/edit', 'TrController@edit');
        Route::get('layanan-tr/{id}/dispoPelaksana', 'TrController@dispoPelaksana');
        Route::get('layanan-tr/{id}/dispoAsmen', 'TrController@dispoAsmen');
        Route::get('layanan-tr/{id}/dispoMsb', 'TrController@dispoMsb');
        Route::get('layanan-tr/{id}/dispoYan', 'TrController@dispoYan');
        Route::get('layanan-tr/{id}/pengujianPelaksana', 'TrController@pengujianPelaksana');
        Route::get('layanan-tr/{id}/savedispoPelaksana', 'TrController@savedispoPelaksana');
        Route::put('layanan-tr/{id}/saveHasilPengujian', 'TrController@saveHasilPengujian');
        Route::get('layanan-tr/{id}/verifikasi', 'TrController@verifikasi');
        Route::get('layanan-tr/{id}/verifikasi-item_uji/{tipe}', 'TrController@verifikasiItemUji');
        Route::get('layanan-tr/{id}/pengujian-item_uji/{tipe}', 'TrController@pengujianItemUji');
        Route::get('layanan-tr/{id}/verifikasi-pengujian', 'TrController@verifikasiPengujian');
        Route::get('layanan-tr/{id}/verifikasi-laporan-pengujian/{tipe}/{jenis}', 'TrController@verifikasiLaporanPengujian');
        Route::get('layanan-tr/{id}/upload-final', 'TrController@uploadLaporanFinal');
        Route::get('layanan-tr/{id}/verifikasi-laporan', 'TrController@verifikasiPengujian');
        Route::get('layanan-tr/{id}/edit-pelaksana', 'TrController@editPelaksana');
        Route::get('layanan-tr/{id}/edit-laporan-pelaksana', 'TrController@editLaporanPelaksana');
        Route::get('layanan-tr/{id}/edit-laporan-msb', 'TrController@editLaporanMsb');
        Route::get('layanan-tr/{id}/edit-laporan-final', 'TrController@editLaporanFinal');
        Route::put('layanan-tr/{id}/savePelaksana', 'TrController@savePelaksana');
        Route::put('layanan-tr/{id}/saveLaporanPelaksana', 'TrController@saveLaporanPelaksana');
        Route::put('layanan-tr/{id}/savePenyelesaian', 'TrController@savePenyelesaian');
        Route::put('layanan-tr/{id}/simpanData', 'TrController@simpanData');
        Route::put('layanan-tr/{id}/uploadLaporan', 'TrController@uploadLaporan');
        Route::get('layanan-tr/download/{id}', 'TrController@download');
        Route::get('layanan-tr/{id}/detail', 'TrController@detil');
        Route::get('layanan-tr/simpan/{data}', 'TrController@buatLaporan');
        Route::get('layanan-tr/simpanLaporan/{data}', 'TrController@buatLaporan');
        Route::get('layanan-tr/simpanLaporanData/{data}/{tipe}', 'TrController@simpanLaporanData');
        Route::get('layanan-tr/simpanDispo/{data}/{tipe}', 'TrController@simpanDispo');
        Route::get('layanan-tr/simpanPengujianItemUji/{data}/{tipe}', 'TrController@simpanPengujianItemUji');
        Route::post('layanan-tr/grid2', 'TrController@grid2');
        Route::post('layanan-tr/grid3', 'TrController@grid3');
        Route::post('layanan-tr/grid4', 'TrController@grid4');
        Route::post('layanan-tr/grid5', 'TrController@grid5');
        Route::get('layanan-tr/{id}/cetakBarcode', 'TrController@printBarcodePdf');
        Route::get('layanan-tr/{id}/preview', 'TrController@preview');
        Route::get('layanan-tr/{id}/preview-multiple/{type}', 'TrController@previewMultiple');
        Route::resource('layanan-tr', 'TrController');

        Route::post('layanan-tt/grid', 'TtController@grid');
        Route::get('layanan-tt/{id}/dispo/{tipe}', 'TtController@dispo');
        Route::put('layanan-tt/{id}/saveDispo', 'TtController@saveDispo');
        Route::put('layanan-tt/{id}/saveDispoPengujian', 'TtController@saveDispoPengujian');
        Route::put('layanan-tt/{id}/saveDispoUpload', 'TtController@saveDispoUpload');
        Route::get('layanan-tt/{id}/buat', 'TtController@buat');
        Route::get('layanan-tt/{id}/edit', 'TtController@edit');
        Route::get('layanan-tt/{id}/dispoPelaksana', 'TtController@dispoPelaksana');
        Route::get('layanan-tt/{id}/dispoAsmen', 'TtController@dispoAsmen');
        Route::get('layanan-tt/{id}/dispoMsb', 'TtController@dispoMsb');
        Route::get('layanan-tt/{id}/dispoYan', 'TtController@dispoYan');
        Route::get('layanan-tt/{id}/pengujianPelaksana', 'TtController@pengujianPelaksana');
        Route::get('layanan-tt/{id}/savedispoPelaksana', 'TtController@savedispoPelaksana');
        Route::put('layanan-tt/{id}/saveHasilPengujian', 'TtController@saveHasilPengujian');
        Route::get('layanan-tt/{id}/verifikasi', 'TtController@verifikasi');
        Route::get('layanan-tt/{id}/verifikasi-item_uji/{tipe}', 'TtController@verifikasiItemUji');
        Route::get('layanan-tt/{id}/pengujian-item_uji/{tipe}', 'TtController@pengujianItemUji');
        Route::get('layanan-tt/{id}/verifikasi-pengujian', 'TtController@verifikasiPengujian');
        Route::get('layanan-tt/{id}/verifikasi-laporan-pengujian/{tipe}/{jenis}', 'TtController@verifikasiLaporanPengujian');
        Route::get('layanan-tt/{id}/upload-final', 'TtController@uploadLaporanFinal');
        Route::get('layanan-tt/{id}/verifikasi-laporan', 'TtController@verifikasiPengujian');
        Route::get('layanan-tt/{id}/edit-pelaksana', 'TtController@editPelaksana');
        Route::get('layanan-tt/{id}/edit-laporan-pelaksana', 'TtController@editLaporanPelaksana');
        Route::get('layanan-tt/{id}/edit-laporan-msb', 'TtController@editLaporanMsb');
        Route::get('layanan-tt/{id}/edit-laporan-final', 'TtController@editLaporanFinal');
        Route::put('layanan-tt/{id}/savePelaksana', 'TtController@savePelaksana');
        Route::put('layanan-tt/{id}/saveLaporanPelaksana', 'TtController@saveLaporanPelaksana');
        Route::put('layanan-tt/{id}/savePenyelesaian', 'TtController@savePenyelesaian');
        Route::put('layanan-tt/{id}/simpanData', 'TtController@simpanData');
        Route::put('layanan-tt/{id}/uploadLaporan', 'TtController@uploadLaporan');
        Route::get('layanan-tt/download/{id}', 'TtController@download');
        Route::get('layanan-tt/{id}/detail', 'TtController@detil');
        Route::get('layanan-tt/simpan/{data}', 'TtController@buatLaporan');
        Route::get('layanan-tt/simpanLaporan/{data}', 'TtController@buatLaporan');
        Route::get('layanan-tt/simpanLaporanData/{data}/{tipe}', 'TtController@simpanLaporanData');
        Route::get('layanan-tt/simpanDispo/{data}/{tipe}', 'TtController@simpanDispo');
        Route::get('layanan-tt/simpanPengujianItemUji/{data}/{tipe}', 'TtController@simpanPengujianItemUji');
        Route::post('layanan-tt/grid2', 'TtController@grid2');
        Route::post('layanan-tt/grid3', 'TtController@grid3');
        Route::post('layanan-tt/grid4', 'TtController@grid4');
        Route::post('layanan-tt/grid5', 'TtController@grid5');
        Route::get('layanan-tt/{id}/cetakBarcode', 'TtController@printBarcodePdf');
        Route::get('layanan-tt/{id}/preview', 'TtController@preview');
        Route::get('layanan-tt/{id}/preview-multiple/{type}', 'TtController@previewMultiple');
        Route::resource('layanan-tt', 'TtController');

        Route::post('layanan-siskit/grid', 'SiskitController@grid');
        Route::get('layanan-siskit/{id}/dispo/{tipe}', 'SiskitController@dispo');
        Route::put('layanan-siskit/{id}/saveDispo', 'SiskitController@saveDispo');
        Route::put('layanan-siskit/{id}/saveDispoPengujian', 'SiskitController@saveDispoPengujian');
        Route::put('layanan-siskit/{id}/saveDispoUpload', 'SiskitController@saveDispoUpload');
        Route::get('layanan-siskit/{id}/buat', 'SiskitController@buat');
        Route::get('layanan-siskit/{id}/edit', 'SiskitController@edit');
        Route::get('layanan-siskit/{id}/dispoPelaksana', 'SiskitController@dispoPelaksana');
        Route::get('layanan-siskit/{id}/dispoAsmen', 'SiskitController@dispoAsmen');
        Route::get('layanan-siskit/{id}/dispoMsb', 'SiskitController@dispoMsb');
        Route::get('layanan-siskit/{id}/dispoYan', 'SiskitController@dispoYan');
        Route::get('layanan-siskit/{id}/pengujianPelaksana', 'SiskitController@pengujianPelaksana');
        Route::get('layanan-siskit/{id}/savedispoPelaksana', 'SiskitController@savedispoPelaksana');
        Route::put('layanan-siskit/{id}/saveHasilPengujian', 'SiskitController@saveHasilPengujian');
        Route::get('layanan-siskit/{id}/verifikasi', 'SiskitController@verifikasi');
        Route::get('layanan-siskit/{id}/verifikasi-item_uji/{tipe}', 'SiskitController@verifikasiItemUji');
        Route::get('layanan-siskit/{id}/pengujian-item_uji/{tipe}', 'SiskitController@pengujianItemUji');
        Route::get('layanan-siskit/{id}/verifikasi-pengujian', 'SiskitController@verifikasiPengujian');
        Route::get('layanan-siskit/{id}/verifikasi-laporan-pengujian/{tipe}/{jenis}', 'SiskitController@verifikasiLaporanPengujian');
        Route::get('layanan-siskit/{id}/upload-final', 'SiskitController@uploadLaporanFinal');
        Route::get('layanan-siskit/{id}/verifikasi-laporan', 'SiskitController@verifikasiPengujian');
        Route::get('layanan-siskit/{id}/edit-pelaksana', 'SiskitController@editPelaksana');
        Route::get('layanan-siskit/{id}/edit-laporan-pelaksana', 'SiskitController@editLaporanPelaksana');
        Route::get('layanan-siskit/{id}/edit-laporan-msb', 'SiskitController@editLaporanMsb');
        Route::get('layanan-siskit/{id}/edit-laporan-final', 'SiskitController@editLaporanFinal');
        Route::put('layanan-siskit/{id}/savePelaksana', 'SiskitController@savePelaksana');
        Route::put('layanan-siskit/{id}/saveLaporanPelaksana', 'SiskitController@saveLaporanPelaksana');
        Route::put('layanan-siskit/{id}/savePenyelesaian', 'SiskitController@savePenyelesaian');
        Route::put('layanan-siskit/{id}/simpanData', 'SiskitController@simpanData');
        Route::put('layanan-siskit/{id}/uploadLaporan', 'SiskitController@uploadLaporan');
        Route::get('layanan-siskit/download/{id}', 'SiskitController@download');
        Route::get('layanan-siskit/{id}/detail', 'SiskitController@detil');
        Route::get('layanan-siskit/simpan/{data}', 'SiskitController@buatLaporan');
        Route::get('layanan-siskit/simpanLaporan/{data}', 'SiskitController@buatLaporan');
        Route::get('layanan-siskit/simpanLaporanData/{data}/{tipe}', 'SiskitController@simpanLaporanData');
        Route::get('layanan-siskit/simpanDispo/{data}/{tipe}', 'SiskitController@simpanDispo');
        Route::get('layanan-siskit/simpanPengujianItemUji/{data}/{tipe}', 'SiskitController@simpanPengujianItemUji');
        Route::post('layanan-siskit/grid2', 'SiskitController@grid2');
        Route::post('layanan-siskit/grid3', 'SiskitController@grid3');
        Route::post('layanan-siskit/grid4', 'SiskitController@grid4');
        Route::post('layanan-siskit/grid5', 'SiskitController@grid5');
        Route::get('layanan-siskit/{id}/cetakBarcode', 'SiskitController@printBarcodePdf');
        Route::get('layanan-siskit/{id}/preview', 'SiskitController@preview');
        Route::get('layanan-siskit/{id}/preview-multiple/{type}', 'SiskitController@previewMultiple');
        Route::resource('layanan-siskit', 'SiskitController');

        Route::post('layanan-sistgi/grid', 'SistgiController@grid');
        Route::get('layanan-sistgi/{id}/dispo/{tipe}', 'SistgiController@dispo');
        Route::put('layanan-sistgi/{id}/saveDispo', 'SistgiController@saveDispo');
        Route::put('layanan-sistgi/{id}/saveDispoPengujian', 'SistgiController@saveDispoPengujian');
        Route::put('layanan-sistgi/{id}/saveDispoUpload', 'SistgiController@saveDispoUpload');
        Route::get('layanan-sistgi/{id}/buat', 'SistgiController@buat');
        Route::get('layanan-sistgi/{id}/edit', 'SistgiController@edit');
        Route::get('layanan-sistgi/{id}/dispoPelaksana', 'SistgiController@dispoPelaksana');
        Route::get('layanan-sistgi/{id}/dispoAsmen', 'SistgiController@dispoAsmen');
        Route::get('layanan-sistgi/{id}/dispoMsb', 'SistgiController@dispoMsb');
        Route::get('layanan-sistgi/{id}/dispoYan', 'SistgiController@dispoYan');
        Route::get('layanan-sistgi/{id}/pengujianPelaksana', 'SistgiController@pengujianPelaksana');
        Route::get('layanan-sistgi/{id}/savedispoPelaksana', 'SistgiController@savedispoPelaksana');
        Route::put('layanan-sistgi/{id}/saveHasilPengujian', 'SistgiController@saveHasilPengujian');
        Route::get('layanan-sistgi/{id}/verifikasi', 'SistgiController@verifikasi');
        Route::get('layanan-sistgi/{id}/verifikasi-item_uji/{tipe}', 'SistgiController@verifikasiItemUji');
        Route::get('layanan-sistgi/{id}/pengujian-item_uji/{tipe}', 'SistgiController@pengujianItemUji');
        Route::get('layanan-sistgi/{id}/verifikasi-pengujian', 'SistgiController@verifikasiPengujian');
        Route::get('layanan-sistgi/{id}/verifikasi-laporan-pengujian/{tipe}/{jenis}', 'SistgiController@verifikasiLaporanPengujian');
        Route::get('layanan-sistgi/{id}/upload-final', 'SistgiController@uploadLaporanFinal');
        Route::get('layanan-sistgi/{id}/verifikasi-laporan', 'SistgiController@verifikasiPengujian');
        Route::get('layanan-sistgi/{id}/edit-pelaksana', 'SistgiController@editPelaksana');
        Route::get('layanan-sistgi/{id}/edit-laporan-pelaksana', 'SistgiController@editLaporanPelaksana');
        Route::get('layanan-sistgi/{id}/edit-laporan-msb', 'SistgiController@editLaporanMsb');
        Route::get('layanan-sistgi/{id}/edit-laporan-final', 'SistgiController@editLaporanFinal');
        Route::put('layanan-sistgi/{id}/savePelaksana', 'SistgiController@savePelaksana');
        Route::put('layanan-sistgi/{id}/saveLaporanPelaksana', 'SistgiController@saveLaporanPelaksana');
        Route::put('layanan-sistgi/{id}/savePenyelesaian', 'SistgiController@savePenyelesaian');
        Route::put('layanan-sistgi/{id}/simpanData', 'SistgiController@simpanData');
        Route::put('layanan-sistgi/{id}/uploadLaporan', 'SistgiController@uploadLaporan');
        Route::get('layanan-sistgi/download/{id}', 'SistgiController@download');
        Route::get('layanan-sistgi/{id}/detail', 'SistgiController@detil');
        Route::get('layanan-sistgi/simpan/{data}', 'SistgiController@buatLaporan');
        Route::get('layanan-sistgi/simpanLaporan/{data}', 'SistgiController@buatLaporan');
        Route::get('layanan-sistgi/simpanLaporanData/{data}/{tipe}', 'SistgiController@simpanLaporanData');
        Route::get('layanan-sistgi/simpanDispo/{data}/{tipe}', 'SistgiController@simpanDispo');
        Route::get('layanan-sistgi/simpanPengujianItemUji/{data}/{tipe}', 'SistgiController@simpanPengujianItemUji');
        Route::post('layanan-sistgi/grid2', 'SistgiController@grid2');
        Route::post('layanan-sistgi/grid3', 'SistgiController@grid3');
        Route::post('layanan-sistgi/grid4', 'SistgiController@grid4');
        Route::post('layanan-sistgi/grid5', 'SistgiController@grid5');
        Route::get('layanan-sistgi/{id}/cetakBarcode', 'SistgiController@printBarcodePdf');
        Route::get('layanan-sistgi/{id}/preview', 'SistgiController@preview');
        Route::get('layanan-sistgi/{id}/preview-multiple/{type}', 'SistgiController@previewMultiple');
        Route::resource('layanan-sistgi', 'SistgiController');
    });
});

Route::group(['middleware' => ['auth', 'role:user']], function () {
    Route::resource('/frontend/profile', 'FrontEnd\ProfileController');
    Route::get('/frontend/ganti-password','Auth\GantiPasswordController@getgantipassword');
    Route::post('/frontend/password-reset','Auth\GantiPasswordController@postgantipassword');

    Route::group(['prefix' => 'frontend', 'namespace' => 'FrontEnd'], function(){
        Route::get('pendaftaran-pengujian/get-detail-pengujian', 'PendaftaranPengujianController@getDetail');
        Route::get('pendaftaran-pengujian/buat', 'PendaftaranPengujianController@buat');
        Route::get('pendaftaran-pengujian/upload/{lingkup}', 'PendaftaranPengujianController@upload');
        Route::get('pendaftaran-pengujian/export/{lingkup}', 'PendaftaranPengujianController@exportTemplate');
        Route::post('pendaftaran-pengujian/request/data-upload', 'PendaftaranPengujianController@cekUpload');
        Route::get('pendaftaran-pengujian/ubah', 'PendaftaranPengujianController@ubah');
        Route::get('pendaftaran-pengujian/lihat-jadwal/{data}', 'PendaftaranPengujianController@lihat');
        Route::get('pendaftaran-pengujian/lihat-layanan', 'PendaftaranPengujianController@lihatLayanan');
        Route::get('pendaftaran-pengujian/{id}/detil', 'PendaftaranPengujianController@detil');
        Route::get('pendaftaran-pengujian/download/{id}', 'PendaftaranPengujianController@download');
        Route::post('pendaftaran-pengujian/request/item-uji', 'PendaftaranPengujianController@cekRequestItemUji');
        Route::post('pendaftaran-pengujian/grid', 'PendaftaranPengujianController@grid');
        Route::post('pendaftaran-pengujian/search-layanan', 'PendaftaranPengujianController@searchLayanan');
        Route::get('pendaftaran-pengujian/{id}/konfirmasi', 'PendaftaranPengujianController@konfirmasi');
        Route::get('pendaftaran-pengujian/{id}/detail', 'PendaftaranPengujianController@detail');
        Route::put('pendaftaran-pengujian/{id}/saveKonfirmasi', 'PendaftaranPengujianController@saveKonfirmasi');
        Route::get('pendaftaran-pengujian/{id}/downloadPenolakan', 'PendaftaranPengujianController@downloadPenolakan');
        Route::resource('pendaftaran-pengujian', 'PendaftaranPengujianController');
    });

    Route::group(['prefix' => 'frontend', 'namespace' => 'FrontEnd'], function(){
        Route::post('account-setting/grid', 'AccountSettingController@grid');
        Route::get('account-setting/{id}/ubahData', 'AccountSettingController@ubah');
        Route::resource('account-setting', 'AccountSettingController');
    });

    Route::group(['prefix' => 'frontend', 'namespace' => 'FrontEnd'], function(){
        Route::get('daftar-order/{id}', 'DaftarOrderController@detail');
        Route::post('daftar-order/grid', 'DaftarOrderController@grid');
        Route::get('daftar-order/{id}/download', 'DaftarOrderController@download');
        Route::get('daftar-order/{id}/detail', 'DaftarOrderController@detail');
        Route::resource('daftar-order', 'DaftarOrderController');
    });

    Route::group(['prefix' => 'frontend', 'namespace' => 'FrontEnd'], function(){
        Route::post('survei/grid', 'SurveiController@grid');
        Route::resource('survei', 'SurveiController');
    });
});
// });


