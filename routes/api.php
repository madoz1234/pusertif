<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('password/email', 'Auth\ForgotPasswordController@getResetToken');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::group(['middleware' => 'auth:api', 'namespace' => 'API'], function(){
    Route::post('update-user', 'Dashboard\DashboardController@updateUser');

	Route::get('jadwal-pengujian', 'JadwalController@jadwal');
    Route::post('tracking', 'TrackingController@tracking');
    Route::post('riwayat', 'TrackingController@riwayat');

    Route::group(['namespace'=>'Dashboard','prefix'=>'dashboard'],function(){
        Route::get('graph','DashboardController@graph');
        Route::get('buble','DashboardController@getBuble');
    });

	Route::group(['namespace'=>'Penerimaan','prefix'=>'penerimaan'],function(){
		Route::get('surat/index','PenerimaanSuratController@index');
		Route::get('surat/online','PenerimaanSuratController@online');

		Route::get('surat/ams','PenerimaanSuratController@ams');

		Route::get('surat/spm','PenerimaanSuratController@spm');

		Route::get('surat/histori','PenerimaanSuratController@histori'); 

        Route::get('surat/download/{id}','PenerimaanSuratController@download'); 
	});

	Route::group(['namespace'=>'KajiUlang','prefix'=>'kaji-ulang'],function(){
		Route::get('proses-kaji-ulang/index','KajiUlangController@index');
		
        Route::get('proses-kaji-ulang/grid','KajiUlangController@grid');

		Route::get('proses-kaji-ulang/diskusi','KajiUlangController@diskusi');

		Route::get('proses-kaji-ulang/histori','KajiUlangController@histori');

        Route::get('proses-kaji-ulang/download/{id}','KajiUlangController@download');
	});

	Route::group(['namespace'=>'Surat'],function(){
		Route::get('surat-penawaran/index','SuratPenawaranController@index');

		Route::get('surat-penawaran/done','SuratPenawaranController@done');

		Route::get('surat-penawaran/kirim','SuratPenawaranController@kirim');

		Route::get('surat-penawaran/histori','SuratPenawaranController@histori');

		Route::get('surat-penawaran/tolak','SuratPenawaranController@tolak');

        Route::get('surat-penawaran/download/{id}','SuratPenawaranController@download');
	});

	Route::group(['namespace'=>'Virtual'],function(){
		Route::get('virtual-account/index','VirtualAccountController@index');

		Route::get('virtual-account/grid','VirtualAccountController@grid');
		
        Route::get('virtual-account/menunggu','VirtualAccountController@menunggu');
		
        Route::get('virtual-account/submit','VirtualAccountController@submit');
		
        Route::get('virtual-account/nonaktif','VirtualAccountController@nonaktif');
		
        Route::get('virtual-account/download','VirtualAccountController@download');
        
        Route::get('virtual-account/download/{id}','VirtualAccountController@downloadVA');
	});

	Route::group(['prefix' => 'konfirmasi', 'namespace' => 'KonfirmasiPembayaran'], function(){
		Route::get('konfirmasi-pembayaran/index', 'KonfirmasiPembayaranController@index');
		
        Route::get('konfirmasi-pembayaran/belum', 'KonfirmasiPembayaranController@belum');
        
        Route::get('konfirmasi-pembayaran/kurang', 'KonfirmasiPembayaranController@kurang');
        
        Route::get('konfirmasi-pembayaran/lebih', 'KonfirmasiPembayaranController@lebih');
        
        Route::get('konfirmasi-pembayaran/lunas', 'KonfirmasiPembayaranController@lunas');
        
        Route::get('konfirmasi-pembayaran/terbit', 'KonfirmasiPembayaranController@terbit');
        
        Route::get('konfirmasi-pembayaran/histori','KonfirmasiPembayaranController@histori');
        
        Route::get('konfirmasi-pembayaran/download/{id}','KonfirmasiPembayaranController@download');
	});

	Route::group(['prefix' => 'realisasi', 'namespace' => 'RealisasiBiaya'], function(){
		Route::get('realisasi-biaya/grid','RealisasiBiayaController@grid');

		Route::get('realisasi-biaya/histori','RealisasiBiayaController@histori');
        
        Route::get('realisasi-biaya/download/{id}','RealisasiBiayaController@download');
	});

	Route::group(['namespace'=>'NotaDinas'],function(){
    	Route::get('nota-dinas/grid','NotaDinasController@grid');

        Route::get('nota-dinas/histori','NotaDinasController@histori');
        
        Route::get('nota-dinas/download/{id}','NotaDinasController@download');
    });

	Route::group(['namespace'=>'PengembalianDana'],function(){
        Route::get('pengembalian-dana/grid','PengembalianDanaController@grid');
        
        Route::get('pengembalian-dana/histori','PengembalianDanaController@histori');
        
        Route::get('pengembalian-dana/dana-uji', 'PengembalianDanaController@dana');
        
        Route::get('pengembalian-dana/histori-dana-uji', 'PengembalianDanaController@danaHistori');
        
        Route::get('pengembalian-dana/download/{id}', 'PengembalianDanaController@download');
    });

    Route::group(['namespace'=>'PengembalianBarangUji'],function(){
        Route::get('pengembalian-barang-uji/grid','PengembalianBarangUjiController@grid');
        
        Route::get('pengembalian-barang-uji/histori','PengembalianBarangUjiController@histori');
        
        Route::get('pengembalian-barang-uji/download/{id}','PengembalianBarangUjiController@download');
        Route::get('pengembalian-barang-uji/{id}}/cetak-surat','PengembalianBarangUjiController@printSuratPdf');
    });

    Route::group(['namespace'=>'PenerimaanBarangUji'],function(){
    	Route::get('penerimaan-barang-uji/grid', 'PenerimaanBarangUjiController@grid');
        
        Route::get('penerimaan-barang-uji/progress', 'PenerimaanBarangUjiController@progress');
        
        Route::get('penerimaan-barang-uji/historis', 'PenerimaanBarangUjiController@historis');
        
        Route::get('penerimaan-barang-uji/{id}/cetakSurat', 'PenerimaanBarangUjiController@printSuratPdf');
        Route::get('penerimaan-barang-uji/{id}/cetakKartu', 'PenerimaanBarangUjiController@printKartuPdf');
        Route::get('penerimaan-barang-uji/download/{id}', 'PenerimaanBarangUjiController@download');
    });
    Route::group(['namespace' => 'CloseOrder'], function(){
    	Route::get('close-order/grid', 'CloseOrderController@grid');
        
        Route::get('close-order/historis', 'CloseOrderController@historis');
        
        Route::get('close-order/download/{id}', 'CloseOrderController@download');
    });

    Route::group(['namespace' => 'HistoryData'], function(){
    	Route::get('histori-data/index', 'HistoryDataController@grid');
        Route::get('histori-data/print/{id}/{type}', 'HistoryDataController@print');
    });

    Route::group(['namespace' => 'Reschedule'], function(){
    	Route::get('reschedule/grid', 'RescheduleController@grid');
    	
        Route::get('reschedule/historis', 'RescheduleController@histori');
        
        Route::get('reschedule/download/{id}', 'RescheduleController@download');
    });

    Route::group(['namespace' => 'OrderUji'], function(){
        Route::get('order-uji/grid', 'OrderUjiController@grid');
        
        Route::get('order-uji/historis', 'OrderUjiController@histori');
        
        Route::get('order-uji/download/{id}', 'OrderUjiController@download');
        Route::get('order-uji/barcode/{id}', 'OrderUjiController@printBarcodePdf');
    });

    Route::group(['namespace' => 'LaporanPengujian'], function(){
        Route::get('laporan-pengujian/grid', 'LaporanPengujianController@grid');

        Route::get('laporan-pengujian/download/{id}', 'LaporanPengujianController@download');
    });

    Route::group(['namespace' => 'PengirimanLaporan'], function(){
        Route::get('pengiriman-laporan/grid', 'PengirimanLaporanController@grid');
        
        Route::get('pengiriman-laporan/histori', 'PengirimanLaporanController@histori');
        
        Route::get('pengiriman-laporan/download/{id}', 'PengirimanLaporanController@download');
    });

    Route::group(['namespace' => 'Download'], function(){
        Route::get('download-va/{id}', 'DownloadController@va');
        Route::get('download/{id}/{type}', 'DownloadController@multiple');
    });

    Route::group(['prefix' => 'pengujian', 'namespace' => 'Pengujian'], function(){
    	Route::get('verifikasi-baru', 'PengujianController@verifikasi');
        Route::get('verifikasi-histori', 'PengujianController@verifikasiHistori');
        Route::get('pengujian-baru', 'PengujianController@pengujian');
        Route::get('pengujian-histori', 'PengujianController@pengujianHistori');
        Route::get('qr-code', 'PengujianController@qrCode');
        Route::get('download-barcode/{id}', 'PengujianController@downloadBarcode');
        Route::get('download/{id}', 'PengujianController@download');

        Route::get('kalibrasi/verifikasi', 'KalibrasiController@verifikasi');
    	Route::get('kalibrasi/verifikasi-histori', 'KalibrasiController@verifikasiHistori');
    	Route::get('kalibrasi/pengujian', 'KalibrasiController@pengujian');
    	Route::get('kalibrasi/pengujian-histori', 'KalibrasiController@pengujianHistori');
    	Route::get('kalibrasi/qr-code', 'KalibrasiController@qrCode');

    	Route::get('tr/verifikasi', 'TrController@verifikasi');
    	Route::get('tr/verifikasi-histori', 'TrController@verifikasiHistori');
    	Route::get('tr/pengujian', 'TrController@pengujian');
    	Route::get('tr/pengujian-histori', 'TrController@pengujianHistori');
    	Route::get('tr/qr-code', 'TrController@qrCode');

    	Route::get('tt/verifikasi', 'TtController@verifikasi');
    	Route::get('tt/verifikasi-histori', 'TtController@verifikasiHistori');
    	Route::get('tt/pengujian', 'TtController@pengujian');
    	Route::get('tt/pengujian-histori', 'TtController@pengujianHistori');
    	Route::get('tt/qr-code', 'TtController@qrCode');

    	Route::get('siskit/verifikasi', 'SiskitController@verifikasi');
    	Route::get('siskit/verifikasi-histori', 'SiskitController@verifikasiHistori');
    	Route::get('siskit/pengujian', 'SiskitController@pengujian');
    	Route::get('siskit/pengujian-histori', 'SiskitController@pengujianHistori');
    	Route::get('siskit/qr-code', 'SiskitController@qrCode');

    	Route::get('sistgi/verifikasi', 'SistgiController@verifikasi');
    	Route::get('sistgi/verifikasi-histori', 'SistgiController@verifikasiHistori');
    	Route::get('sistgi/pengujian', 'SistgiController@pengujian');
    	Route::get('sistgi/pengujian-histori', 'SistgiController@pengujianHistori');
    	Route::get('sistgi/qr-code', 'SistgiController@qrCode');
    });

	Route::group(['prefix' => 'profile'], function(){
		Route::post('/change-photo','ProfileController@changePhoto');
		Route::post('/change-password', 'ProfileController@changePassword');
		Route::resource('/', 'ProfileController');
	});

    Route::group(['prefix' => 'master', 'namespace' => 'Master'], function(){
        Route::get('pelayanan','PelayananController@index');
        Route::get('perusahaan','PerusahaanController@index');
        Route::get('user/histori-data','UserController@historidata');
    });


	Route::get('/user', function (Request $request) {
		$user = $request->user();
		$with = [
			'hak_akses' => $user->roles,
			'pelanggan' => isset($user->pelanggans) ? $user->pelanggans : '-',
			'detail' => isset($user->detail) ? $user->detail : '-',
		];
	    return $user;
	});
});