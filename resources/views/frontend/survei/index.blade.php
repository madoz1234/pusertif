@extends('layouts.list-user')

@section('js-filters')
    d.no_order = $("input[name='filter[no_order]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			title: ['empty'],
		};
	</script>
@endsection

@section('filters')
	<div class="field">
		<input name="filter[no_order]" placeholder="{{trans('translator.No Order')}}" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="{{trans('translator.Cari Data')}}">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="{{trans('translator.Bersihkan Pencarian')}}">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
@endsection
@section('scripts')
<script type="text/javascript">
</script>
@section('init-modal')
	<script>
		initModal = function(){
			$('.menu .item').tab();
		    $('.next').tab();

		    $(document).ready(function(){
		    	 $('.menu .item').tab();
		        $('.next').tab();
		    })
		}
	</script>
@endsection
