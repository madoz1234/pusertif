@extends('layouts.form-user')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/wp.css') }}">
@append

@section('content-body')
@if($record)
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}"  method="POST">
		{!! csrf_field() !!}
	    <input type="hidden" name="pp_id" value="{{ $pp_id }}">
	    <input type="hidden" name="survei_id" value="{{ $record->id }}">
	    <input type="hidden" name="nama" value="{{ $record->nama }}">

		<div class="ui very basic thin segment">
			<table class="ui compact celled table" style="border-radius: 0; margin: 5px 0">
				<thead>
					<tr class="middle aligned">
						<th rowspan="2" width="5%" class="center aligned">#</th>
						<th rowspan="2" width="70%" class="center aligned">{{trans('translator.Pertanyaan')}}</th>
						<th rowspan="2" width="25%" class="center aligned">{{trans('translator.Rating')}}</th>
					</tr>
				</thead>
				<tbody class="detail container">
					@foreach($record->detail as $key => $row)
						<tr>
							<td class="center aligned">{{$key+1}}</td>
							<td class="field"><div class="ui transparent fluid input">
								<label>{{ $row->pertanyaan }}</label>
								<input type="hidden" name="detail[{{ $row->id }}][pertanyaan]" value="{{ $row->pertanyaan }}">
							</div></td>
							<td class="center aligned">
								<div class="ui large star rating" data-max-rating="5" data-id="{{ $row->id }}"></div>
								<input type="hidden" name="detail[{{ $row->id }}][jawaban]" id="jawaban-{{ $row->id }}">
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</form>

	<div class="ui divider"></div>
	<div class="actions">
		<div class="ui two column grid">
	  		<div class="left aligned column">
				<div class="ui gray deny labeled icon button" onclick="window.history.back()">
	      		<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">		
				<button type="button" class="ui positive right labeled icon save as page button">
					Simpan
					<i class="save icon"></i>
				</button>
			</div>
		</div>	
	</div>
@else
	<div class="ui icon message">
		<i class="inbox icon"></i>
		<div class="content">
			<div class="header">
				Oops, Maaf!
			</div>
			<p>Data survei belum tersedia.</p>
		</div>
	</div>
@endif
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		$('.ui.rating').rating('setting', 'onRate', function(value) {
			var id = $(this).data("id");
			$('#jawaban-'+id).val(value);
		});
	});

</script>
@append