@extends('layouts.list-user')

@section('css')
<style type="text/css">
	.upload.button{
		border-radius: inherit;
	}
</style>
@append

@section('js')
<script type="text/javascript">
	$(document).ready(function($) {
		$('.profile.image').dimmer({
			on: 'hover'
		});

		$('.tabable .item').tab();

		$('.upload.photo.button').click(function(){ $('#photo').trigger('click'); });
	});

	$('.upload.photo.button').click(function(){ $('#photo').trigger('click'); });
	
	$(document).on('change', '#photo' , function(){
		console.log('start upload'); 
	});
</script>
@append

@section('content-body')
<div class="ui stackable grid">	
	<div class="four wide column">	
		<div class="ui fluid card">
			<div class="ui medium image image-container">
				<img class="ui image-preview image" 
					 src="{{ isset($user->photo) ? asset('storage/'.$user->photo) : asset('img/user-default.jpg') }}" 
					 style="border-top: none; border-right: none; border-left: none;">
			</div>
			<div class="content">
				<a class="header" href="#">{{ $user->username }}</a>
				<div class="meta">
					<a>{{ $user->roles()->first()->display_name }}</a>
				</div>
			</div>
			{{-- <div class="extra content">
				<div class="ui list">
					<div class="item">
						<i class="file text blue icon"></i>
						<div class="content">
							<span class="header">{{ $ongoing->count() + $done->count() }} Working Permit</span>
							<div class="description">{{ $done->count() }} dari {{ $ongoing->count() + $done->count() }} WP Telah Selesai </div>
						</div>
					</div>
				</div>
			</div> --}}
		</div>
	</div>
	<div class="twelve wide column">	
		<div class="ui pointing secondary menu tabable">
			<a class="item active" data-tab="first">{{trans('translator.Data Akun & Pribadi')}}</a>
			<a class="item" data-tab="second">{{trans('translator.Aktifitas')}}</a>
		</div>
		<div class="ui tab active" data-tab="first" >
			<form id="dataForm" class="ui form" action="{{ url($pageUrl) }}" method="POST">
				{!! csrf_field() !!}
				{{-- <input type="hidden" name="pelanggan_id" value="{{ $user->pelanggan_id }}"> --}}
				<h3 class="ui center aligned dividing header">{{trans('translator.Data Pribadi')}}</h3>
				<div class="ui columns doubling stackable grid">
					<div class="nine wide column">
						<div class="field">
							<label>{{trans('translator.Nama Lengkap')}}</label>
							<input type="text" name="pelanggan[nama_lengkap]" placeholder="{{trans('translator.Nama Lengkap')}}" value="{{ $pelanggan->nama_lengkap or '' }}">
						</div>
						<div class="fields">
							<div class="six wide field">
								<label>{{trans('translator.Nomor Telepon')}}</label>
								<input type="text" name="pelanggan[no_hp]" placeholder="{{trans('translator.Nomor Telepon')}}" value="{{ $pelanggan->no_hp or '' }}">
							</div>
							<div class="ten wide field">
								<label>{{trans('translator.Alamat Email')}}</label>
								<input type="text" name="pelanggan[email]" placeholder="{{trans('translator.Alamat Email')}}" value="{{ $pelanggan->email }}">
							</div>
						</div>
						<div class="field">
							<label>{{trans('translator.Kategori')}}</label>
							<div class="ui radio checkbox" style="margin-right: 30px;">
								<input type="radio" name="tipe_customer" value="0" @if($pelanggan->tipe_customer == 0) checked="" @endif tabindex="0" class="hidden">
								<label>PLN</label>
							</div>
							<div class="ui radio checkbox" style="margin-right: 30px;">
								<input type="radio" name="tipe_customer" value="1" @if($pelanggan->tipe_customer == 1) checked="" @endif tabindex="0" class="hidden">
								<label>NON PLN</label>
							</div>
							<div class="ui radio checkbox">
								<input type="radio" name="tipe_customer" value="2" @if($pelanggan->tipe_customer == 2) checked="" @endif tabindex="0" class="hidden">
								<label>AP-PLN</label>
							</div>
						</div>
					</div>
					<div class="seven wide column">
						<div class="field">
							<label>{{trans('translator.Nama Perusahaan')}}</label>
							<input type="text" name="pelanggan[perusahaan_id]" placeholder="{{trans('translator.Nama Perusahaan')}}" value="{{ $pelanggan->perusahaan->nama }}" readonly="">
						</div>
						<div class="field">
							<label>{{trans('translator.No. Telpon Perusahaan')}}</label>
							<input type="text" name="pelanggan[no_tlp]" placeholder="{{trans('translator.No. Telpon Perusahaan')}}" value="{{ $pelanggan->no_tlp }}" readonly="">
						</div>
						<div class="field">
							<label>{{trans('translator.Alamat Perusahaan')}}</label>
							<input type="text" name="pelanggan[alamat]" placeholder="{{trans('translator.Alamat Perusahaan')}}" value="{{ $pelanggan->alamat }}" readonly="">
						</div>
					</div>
				</div>
				<h3 class="ui center aligned dividing header">{{trans('translator.Data Akun & Ubah Password')}}</h3>
				<div class="ui stackable grid">
					<div class="four wide column">
						<div class="field">
							<label>{{trans('translator.Foto Profil')}}</label>
							<div class="ui medium profile image image-container">
								<div class="ui dimmer">
									<div class="content">
										<div class="ui inverted blue upload photo button">{{trans('translator.Upload Gambar')}}</div>
										<input type="file" id="photo" name="user[photo]" class="attachment" style="display: none!important;" />
									</div>
								</div>
								<img class="ui image-preview image" 
									 src="{{ isset($user->photo) ? asset('storage/'.$user->photo) : asset('img/user-default.jpg') }}" 
									 style="border-top: none; border-right: none; border-left: none;">
							</div>
						</div>
					</div>
					<div class="twelve wide column">
						<div class="two fields">
							<div class="field">
								<label>{{trans('translator.Username')}}</label>
								<input type="text" name="user[username]" placeholder="{{trans('translator.Username')}}" value="{{ $user->username }}">
							</div>
							<div class="field">
								<label>{{trans('translator.Hak Akses')}}</label>
								<input type="text" value="{{ $user->roles->first()->display_name }}" readonly>
							</div>
						</div>
						<div class="ui info message">
							<div class="header">{{trans('translator.Informasi Ubah Password')}}</div>
							<p>{{trans('translator.Isi field dibawah untuk mengubah password, kosongkan jika tidak ingin merubah password.')}}</p>
						</div>
						<div class="three fields">
							<div class="field">
								<label>{{trans('translator.Password Saat Ini')}}</label>
								<input type="password" name="old_password" placeholder="{{trans('translator.Password Saat Ini')}}">
							</div>
							<div class="field">
								<label>{{trans('translator.Password Baru')}}</label>
								<input type="password" name="password" placeholder="{{trans('translator.Password Baru')}}">
							</div>
							<div class="field">
								<label>{{trans('translator.Konfirmasi Password')}}</label>
								<input type="password" name="confirm_password" placeholder="{{trans('translator.Konfirmasi Password')}}">
							</div>
						</div>
					</div>
				</div>
				<div class="ui transparent divider"></div>
			  	<button type="button" class="ui right floated blue labeled icon save as page button"><i class="save icon"></i>{{trans('translator.Simpan')}}</button>
				<div class="ui clearing transparent divider"></div>
			</form>
			{{-- <h1 class="ui header" style="line-height: 400px">
				U N D E R &nbsp; &nbsp; C O N S T R U C T I O N
			</h1> --}}
		</div>
		<div class="ui tab center aligned segment" data-tab="second" style="height:400px;">
			<h1 class="ui header" style="line-height: 400px">
				U N D E R &nbsp; &nbsp; C O N S T R U C T I O N
			</h1>
		</div>
	</div>
</div>
@endsection