@extends('layouts.list-user')

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			nama: ['empty'],
		};
	</script>
@endsection

@section('filters')
	<div class="field">
		<input name="filter[nama]" placeholder="{{trans('translator.Nama')}}" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="{{trans('translator.Cari Data')}}">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="{{trans('translator.Bersihkan Pencarian')}}">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
	{{-- <button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		{{trans('translator.Tambah Data')}}
	</button> --}}
@endsection

@section('init-modal')
	<script>
		initModal = function(){
			$('.menu .item').tab();
		    $('.next').tab();

		    $(document).ready(function(){
		    	 $('.menu .item').tab();
		        $('.next').tab();
		    })
		}
	</script>
@endsection
