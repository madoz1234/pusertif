<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">{{trans('translator.Terdaftar Sejak')}}<i><h5>{{ \Carbon::parse($record->created_at)->format('d M Y') }} </h5></i></div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{$record->id}}">
		{!! csrf_field() !!}
	  	<div class="field">
			<label>{{trans('translator.Nama Lengkap')}}</label>
			<input name="nama" placeholder="{{trans('translator.Nama Lengkap')}}" type="text" value="{{ $record->nama_lengkap }}">
		</div>     
      	<div class="field">
			<label>{{trans('translator.Email')}}</label>
			<input name="email" placeholder="{{trans('translator.Email')}}" type="text" value="{{ $record->email }}">
		</div>
      	<div class="field">
    		<label>{{trans('translator.No HP')}}</label>
        	<input name="no_hp" placeholder="{{trans('translator.No HP')}}" type="text" value="{{ $record->no_hp }}">
      	</div>
      	<div class="field">
      		<label>Status</label>
      		<br>
      		<div class="ui checked checkbox">
      			<input type="checkbox" name="status" {{ $record->status == 0 ? 'checked' : '' }}>
      			<label>{{trans('translator.Aktif')}}</label>
      		</div>
      	</div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	
</script>