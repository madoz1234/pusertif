@extends('layouts.list-user')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append
@section('modals')
<div class="ui small modal pln" id="PLN">
	<div class="header">
		Pembayaran
	</div>
	<div class="content">
		<div class="ui form" id="PLN">
			<div class="two fields">
				<div class="field">
					<label for="apdChoice">Nomor Order / Virtual Account</label>
					<div class="ui input">
						<input type="text" name="no-order" id="no-order" value="003-2019-0001" disabled="">
					</div>
				</div>
				<div class="field">
					<label for="apdChoice">Persetujuan</label>
					<div class="ui action choises input">
						<input type="text" readonly>
						<input type="file" name="lampiran" style="display: none!important;">
						<div class="ui icon button">
							<i class="cloud upload alternate icon"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="two fields">
				<div class="field">
					<label for="apdChoice">No SKAI/SKAO</label>
					<div class="ui input">
						<input type="text" name="no-order" id="no-order" placeholder="No SKAI/SKAO">
					</div>
				</div>
				<div class="field">
					<label for="apdChoice">No PRK</label>
					<div class="ui input">
						<input type="text" name="no-order" id="no-order" placeholder="No PRK">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="actions">
		<div class="ui default left floated deny button">
			Batal
		</div>
		<div class="ui positive right labeled icon button">
			Simpan
			<i class="checkmark icon"></i>
		</div>
	</div>
</div>

<div class="ui small modal non-pln" id="non-PLN">
	<div class="header">
		Pembayaran
	</div>
	<div class="content">
		<div class="ui form" id="non-PLN">
			<div class="field">
				<label for="apdChoice">Nomor Order / Virtual Account</label>
				<div class="ui input">
					<input type="text" name="no-order" id="no-order" value="003-2019-0001" disabled="">
				</div>
			</div>
			<div class="two fields">
				<div class="field">
					<label for="apdChoice">Bukti Pembayaran</label>
					<div class="ui action choises input">
						<input type="text" readonly>
						<input type="file" name="lampiran" style="display: none!important;">
						<div class="ui icon button">
							<i class="cloud upload alternate icon"></i>
						</div>
					</div>
				</div>
				<div class="field">
					<label for="apdChoice">Persetujuan</label>
					<div class="ui action choises input">
						<input type="text" readonly>
						<input type="file" name="lampiran" style="display: none!important;">
						<div class="ui icon button">
							<i class="cloud upload alternate icon"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="actions">
		<div class="ui default left floated deny button">
			Batal
		</div>
		<div class="ui positive right labeled icon button">
			Simpan
			<i class="checkmark icon"></i>
		</div>
	</div>
</div>
@endsection
@section('content-header')
<h2 class="ui header">
	<div class="content">
		Pembayaran
	</div>
</h2>
@show
@section('filters')
	<div class="field">
		<input type="text" name="filter[display_name]" placeholder="No Order">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection
@section('subcontent')
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="ui bottom demo active tab segment" data-tab="first">
	<table id="example" class="ui celled compact red table" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th>No</th>
				<th>Nomor Order / Virtual Account</th>
				<th>Jenis Alat</th>
				<th>Merk</th>
				<th>Tipe Alat</th>
				<th>Jumlah Sampel</th>
				<th>No Surat Permintaan</th>
				<th>Spesifikasi Teknis</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td>003-2019-0001</td>
				<td>Dispenser</td>
				<td>Miyako</td>
				<td>Rumah Tangga</td>
				<td class="right aligned">2</td>
				<td>003-2019-0001</td>
				<td>Rusak</td>
				<td>
					<button type="button" class="ui mini basic green tambah pln icon button" data-tooltip="Upload Bukti" data-position="left center" style="margin-bottom: 3px;"><i class="cloud upload outline icon"></i></button>
					<br>
					<button type="button" class="ui mini basic green tambah non icon button" data-tooltip="Upload Bukti Non PLN" data-position="left center"><i class="cloud upload outline icon"></i></button>
				</td>
			</tr>
			<tr>
				<td>2</td>
				<td>003-2019-0001</td>
				<td>Setrika</td>
				<td>Philips</td>
				<td>Rumah Tangga</td>
				<td class="right aligned">4</td>
				<td>003-2019-0001</td>
				<td>Rusak</td>
				<td>
					<button type="button" class="ui mini basic green tambah pln icon button" data-tooltip="Upload Bukti" data-position="left center" style="margin-bottom: 3px;"><i class="cloud upload outline icon"></i></button>
					<br>
					<button type="button" class="ui mini basic green tambah non icon button" data-tooltip="Upload Bukti Non PLN" data-position="left center"><i class="cloud upload outline icon"></i></button>
				</td>
			</tr>
			<tr>
				<td>3</td>
				<td>003-2019-0001</td>
				<td>Kompor Gas</td>
				<td>Quantum</td>
				<td>Rumah Tangga</td>
				<td class="right aligned">3</td>
				<td>003-2019-0001</td>
				<td>Rusak</td>
				<td>
					<button type="button" class="ui mini basic green tambah pln icon button" data-tooltip="Upload Bukti" data-position="left center" style="margin-bottom: 3px;"><i class="cloud upload outline icon"></i></button>
					<br>
					<button type="button" class="ui mini basic green tambah non icon button" data-tooltip="Upload Bukti Non PLN" data-position="left center"><i class="cloud upload outline icon"></i></button>
				</td>
			</tr>
		</tbody>
	</table>
</div>
@endsection

@section('js-filters')
	d.display_name = $("input[name='filter[display_name]']").val();
	d.name = $("input[name='filter[name]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			username: 'empty',
			email: 'empty',
			roles: 'empty',
		};
	</script>
@endsection

@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	$(document).ready(function() {
		$('.demo.tabular.menu .item').tab();
		$('#example').DataTable( {
			"paging":   10,
			"ordering": false,
			"lengthChange": false,
			"filter": false,
			"info":     true,
		});
	});
</script>
@append