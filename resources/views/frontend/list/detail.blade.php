@extends('layouts.form-user')
@section('styles')
<style type="text/css">
.responsive.table{
	width: 100%;
	overflow-x: auto;
}
.jus{
	text-align: justify;
	text-justify: inter-word;
}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		tipe: ['empty'],
	};
</script>
@endsection

@section('content-body')
<form class="ui form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
	<div class="ui very basic thin segment">
		<table class="ui compact table" style="border-radius: 0; margin: 0">
			<tr>
				<td width="150px;"><b>{{trans('translator.No Pesanan')}}</b></td>
				<td width="10px;">:</td>
				<td>
					{{$record->no_order}}
				</td>
				<td width="150px;"><b>{{trans('translator.Nama Pemesan')}}</b></td>
				<td width="5px;">:</td>
				<td>
					{{$record->user->pelanggans->nama_lengkap}}
				</td>
			</tr>
			<tr>
				<td width="150px;"><b>{{trans('translator.Tgl Pesanan')}}</b></td>
				<td width="5px;">:</td>
				<td>
					{{$record->tgl_order}}
				</td>
				<td width="150px;"><b>Email</b></td>
				<td width="5px;">:</td>
				<td>
					{{$record->user->email}}
				</td>
			</tr>
			<tr>
				<td width="150px;"><b>{{trans('translator.No Hp Pemesan')}}</b></td>
				<td width="5px;">:</td>
				<td>
					{{$record->user->pelanggans->no_hp}}
				</td>
				<td width="150px;"><b>{{trans('translator.Nama Perusahaan')}}</b></td>
				<td width="5px;">:</td>
				<td>
					[ {{$record->user->pelanggans->perusahaan->kode}} ] - [ {{$record->user->pelanggans->perusahaan->nama}} ]
				</td>
			</tr>
			<tr>
				<td width="150px;"><b>{{trans('translator.Alamat Perusahaan')}}</b></td>
				<td width="5px;">:</td>
				<td colspan="4" style="text-align: justify;text-justify: inter-word;">
					{{$record->user->pelanggans->alamat}}
				</td>
			</tr>
		</table>
		</div>
		<div class="ui very basic thin segment">
			<table id="detail" class="ui compact celled table" style="border-radius: 0; margin: 5px 0;">
				<thead>
					<tr class="middle aligned">
						<th width="30px" class="center aligned">No</th>
						<th width="300px" class="center aligned">{{trans('translator.Jenis Pengujian')}}</th>
						<th width="300px"class="center aligned">{{trans('translator.Merk')}}</th>
						<th width="150px" class="center aligned">{{trans('translator.Tipe')}}</th>
						<th width="100px" class="center aligned">{{trans('translator.Jumlah')}}</th>
						<th width="100px" class="center aligned">{{trans('translator.Satuan')}}</th>
						<th width="200px" class="center aligned">{{trans('translator.Spesifikasi')}}</th>
						<th width="200px" class="center aligned">{{trans('translator.Nilai')}}</th>
					</tr>
				</thead>
				<tbody class="detail fluid container">
					@foreach($record->detail as $key => $data)
						<tr class="list-row-{{$data->id}}" id="data-row" data-id="{{$data->id}}">
							<td class="center aligned numbor">{{$key+1}}</td>
							<td class="pjg">
								<span>{{ $data->jenis->nama }}</span>
							</td>
							<td>
								<span class="sedit-label lrow-{{$data->id}}">{{$data->merk}}</span>
							</td>
							<td>
								<span class="sedit-label lrow-{{$data->id}}">{{$data->tipe}}</span>
							</td>
							<td>
								<span class="sedit-label lrow-{{$data->id}}">{{$data->jumlah}}</span>
							</td>
							<td>
								<span class="sedit-label lrow-{{$data->id}}">{{$data->satuan->satuan}}</span>
							</td>
							<td>
								<span class="sedit-label lrow-{{$data->id}}">{{$data->spesifikasi}}</span>
							</td>
							<td style="text-align: right;">
								<span class="sedit-label lrow-{{$data->id}}">Rp.2000000</span>
							</td>
						</tr>			
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<th colspan="7" class="right aligned" width="40px">Total</th>
						<th class="right aligned" width="40px">Rp.2000000</th>
					</tr>		
				</tfoot>
			</table>
		</div>
	</form>
	@endsection
	@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			$('.ui.pilihan').css({
				'width': '400px'
			})
		});
	</script>
	@append