<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">{{trans('translator.Item Uji')}}</div>
<div class="ui warning list-uji hidden message" style="margin-left: 10px;margin-right: 10px; width: 27%;margin-bottom: -10px;text-align: center;">
	{{trans('translator.Nama Merk Sudah ada')}}
</div>
<div class="content">
	<form class="ui form" id="formPengujian" action="{{ url($pageUrl.'request/item-uji') }}" method="POST">
		{!! csrf_field() !!}
		<table class="table" style="border-radius: 0; margin: 0">
			<tr>
				<td style="width: 22%;"><b>{{trans('translator.Jenis Pengujian')}}</b></td>
				<td width="10">:</td>
				<td style="width: 100%;">
					<div class="field">
						<select name="jenis_id" id="jenis_id" class="watcher ui search pilihan dropdown">
							<option value="">{{trans('translator.pilih salah satu')}}</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td style="width: 22%;"><b>{{trans('translator.Merk')}}</b></td>
				<td width="10">:</td>
				<td style="width: 100%;">
					<div class="field">
						<input type="text" name="merk" id="merk" placeholder="{{trans('translator.Merk')}}">
					</div>
				</td>
			</tr>
			<tr>
				<td style="width: 22%;"><b>{{trans('translator.Tipe')}}</b></td>
				<td width="10">:</td>
				<td style="width: 100%;">
					<div class="field">
						<input type="text" name="tipe" id="tipe" placeholder="{{trans('translator.Tipe')}}">
					</div>
				</td>
			</tr>
			<tr>
				<td style="width: 22%;"><b>{{trans('translator.Jumlah')}}</b></td>
				<td width="10">:</td>
				<td style="width: 100%;">
					<div class="field">
						<input type="text" name="jumlah" id="jumlah" class="number" placeholder="{{trans('translator.Jumlah')}}">
					</div>
				</td>
			</tr>
			<tr>
				<td style="width: 22%;"><b>{{trans('translator.Satuan')}}</b></td>
				<td width="10">:</td>
				<td style="width: 100%;">
					<div class="field">
						<select name="satuan_id" id="satuan_id" class="watcher ui fluid search pilihan dropdown">
							{!!
								\App\Models\Master\Satuan::options('satuan', 'id', [
									'filters' => [
									],
									'selected' => old('satuan_id')
								])
								!!}
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td style="width: 22%;"><b>{{trans('translator.Spesifikasi')}}</b></td>
				<td width="10">:</td>
				<td style="width: 100%;">
					<div class="field">
						<textarea name="spesifikasi" class="areas input" id="spesifikasi" placeholder="{{trans('translator.Spesifikasi')}}" rows="2"></textarea>
					</div>
				</td>
			</tr>
		</table>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button" style="background-color: #363636;">
		{{trans('translator.Batal')}}
	</div>
	<div class="ui positive right labeled icon save button add-uji" style="background-color: #00AEEF;">
		{{trans('translator.Simpan')}}
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$('.ui.pilihan').dropdown();
	$("#merk").on("keypress keyup blur",function (e) {    
		$('.ui.warning.list-uji').addClass('hidden');
		$('.add-uji').removeClass('disabled');
	});
	$(".number").on("keypress keyup blur",function (e) {    
		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	});
	$.ajax({
		url: '{{ url('ajax/option/jenis') }}',
		type: 'POST',
		data: {
			_token: "{{ csrf_token() }}",
			jenis_pelayanan_id: $('select[name=jenis_pelayanan_id]').val(),
			lingkup_id: $('select[name=lingkup_id]').val(),
		},
	})
	.done(function(response) {
		$('select[name=jenis_id]').html(response)
	})
	.fail(function() {
		console.log("error");
	});
</script>