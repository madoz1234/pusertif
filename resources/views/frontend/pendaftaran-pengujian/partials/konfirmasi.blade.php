@extends('layouts.form-user')
@section('styles')
<style type="text/css">
.responsive.table{
	width: 100%;
	overflow-x: auto;
}
.jus{
	text-align: justify;
	text-justify: inter-word;
}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		tipe: ['empty'],
	};
</script>
@endsection

@section('content-body')
<form class="ui form" id="dataForm" action="{{ url($pageUrl.$record->id).'/saveKonfirmasi' }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
	<div class="ui very basic thin segment">
		<div class="ui very basic thin segment">
			<table class="ui compact celled table" style="border-radius: 0; margin: 0;margin-bottom: -6px;">
				<tr style="background-color:#ffffff;">
					<th colspan="4">{{trans('translator.No Pesanan')}} : {{$record->no_order}}</th>
				</tr>
			</table>
			<input type="hidden" name="kadaluarsa" value="{{ $record->kaji_ulang->surat->va->tgl_kadaluarsa }}">
			<table id="detail" class="ui compact celled table" style="border-radius: 0; margin: 5px 0;">
				<tr class="top aligned">
					<td width="50%">
						<div>
							<div>
								<div class="item"><i>{{trans('translator.Jumlah Pembayaran')}}</i></div>
								<div class="item"><i><h2>Rp. {{ rtrim(rtrim(number_format($record->kaji_ulang->surat->va->nominal,2,',','.'), '0'), ',')}}</h2></i></div>
								<div class="item"><i>{{trans('translator.Lakukan pembayaran sesuai jumlah diatas, hingga tiga digit terakhir')}}</i></div>
								<div class="item"><i><h3>{{trans('translator.No Virtual Account')}} : {{$record->kaji_ulang->surat->va->no_va}}</h3></i></div> <b>(Bank Mandiri)</b>
							</div>
						</div>
					</td>
					<td width="50%">
						<div>
							<div class="item"><i>{{trans('translator.Batas Waktu Pembayaran')}}</i></div>
							<div class="item"><i><a class="ui huge red label" id="demo"></a></i></div>
							<div class="item"><i>{{trans('translator.Sampai Hari')}} : <h3 style="color:red;">{{ DateToStringWday(Carbon::parse($record->kaji_ulang->surat->va->tgl_kadaluarsa)) }} 23:59:59</h3></i></div>
							<div class="item"><i>{{trans('translator.Upload Bukti Pembayaran')}}</div>
							<div class="item"><i>
								<div class="field cek">
									<div class="ui action choises input" style="width: 250px;">
										<input type="text"  readonly>
										<input type="file" name="bukti" style="display: none!important;" accept="image/*"> 
										<div class="ui icon button" data-tooltip="Upload" data-position="top center">
											<i class="cloud upload alternate icon"></i>
										</div>
									</div>
									<button type="button" class="ui green small icon button" data-tooltip="{{trans('translator.Simpan')}}" data-position="top center" style="padding-top: 11px;padding-bottom: 11px;padding-right: 11px;padding-left: 11px;"><i class="save icon"></i></button>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="ui very basic thin segment">
			<table class="ui compact celled table" style="border-radius: 0; margin: 0;margin-bottom: -6px;">
				<tr style="background-color:#ffffff;">
					<th colspan="4">{{trans('translator.Cara Pembayaran')}}</th>
				</tr>
			</table>
			<table id="detail" class="ui compact celled table" style="border-radius: 0; margin: 5px 0;">
				<tr class="top aligned">
					<td>
						<div>
							<div width="50px">
								<div class="item"><i>{{trans('translator.Cara Pembayaran')}}</i></div>
								<div class="item"><i>1. {{trans('translator.Transfer dapat melalui ATM, SMS / M-Banking, dan E-Banking')}}.</i></div>
								<div class="item"><i>2. {{trans('translator.Masukkan Nomor Virtual Account')}} {{$record->kaji_ulang->surat->va->no_va}}</i></div>
								<div class="item"><i>3. {{trans('translator.Masukkan jumlah bayar tepat hingga 3 digit terakhir, yaitu')}} Rp. <b>{{ rtrim(rtrim(number_format($record->kaji_ulang->surat->va->nominal,2,',','.'), '0'), ',')}}</b></i></div>
								<div class="item"><i>4. {{trans('translator.Simpan dan upload bukti transfer yang Anda dapatkan')}}.</i></div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="ui very basic thin segment">
			<table class="ui compact celled table" style="border-radius: 0; margin: 0;margin-bottom: -2px;">
				<tr style="background-color:#ffffff;">
					<th colspan="4">{{trans('translator.Detail Pesanan')}}</th>
				</tr>
			</table>
			<table class="ui compact table" style="border-radius: 0; margin: 0;margin-bottom: -6px;">
				<tr>
					<td width="150px;"><b>{{trans('translator.Layanan')}}</b></td>
					<td width="10px;">:</td>
					<td>
						{{$record->pelayanan->nama}}
					</td>
				</tr>
				<tr>
					<td width="150px;"><b>{{trans('translator.Lingkup')}}</b></td>
					<td width="5px;">:</td>
					<td>
						{{$record->lingkup->nama}}
					</td>
				</tr>
			</table>
			<div class="ui top attached" style="margin-top: 6px;">
				<table id="example" class="ui celled compact red table display" id="table-rab" width="100%" cellspacing="0">
					<thead>
						<tr class="center aligned">
							<th>No</th>
							<th>Uraian</th>
							<th>Nominal (Rp)</th>
						</tr>
					</thead>
					<tbody class="komponens">
						@php
						$total = 0;
						@endphp
						@foreach($record->kaji_ulang->surat->detail as $kiy => $cik)
						<tr class="data_komponens" data-id="1">
							<td class="center aligned numbor numboor-1" style="width: 50px;">{{ $kiy+1 }}</td>
							<td width="300px">
								<div class="ui transparent fluid input field">
									{{$cik->komponen}}
								</div>
							</td>
							<td width="200px">
								<div class="ui transparent" style="text-align: right;">
									{{ rtrim(rtrim(number_format($cik->nominal,2,',','.'), '0'), ',')}}
									@php
									$total += $cik->nominal;
									@endphp
								</div>
							</td>
						</tr>
						@endforeach
						@php
						$ppn = (($total/100)*10);
						$total_ppn = ($total + $ppn);
						@endphp
					</tbody>
					<tfoot>
						<tr>
							<td style="border-top: 1px solid rgba(34,36,38,.1);"></td>
							<td scope="row" style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);border-left: none;">Total Sebelum PPn</td>
							<td style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);" class="total">{{ rtrim(rtrim(number_format($total,2,',','.'), '0'), ',')}}</td>
						</tr>
						<tr>
							<td style="border-top:none;"></td>
							<td scope="row" style="text-align: right;border-top:none;border-left: none;">PPn</td>
							<td style="text-align: right;border-top:none;" class="total">{{ rtrim(rtrim(number_format($ppn,2,',','.'), '0'), ',')}}</td>
						</tr>
						<tr>
							<td style="border-top:none;"></td>
							<td scope="row" style="text-align: right;border-top:none;border-left: none;">Total Sesudah PPn</td>
							<td style="text-align: right;border-top:none;" class="total">{{ rtrim(rtrim(number_format($total_ppn,2,',','.'), '0'), ',')}}</td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</form>
	<div class="actions">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui gray deny labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					{{trans('translator.Kembali')}}
				</div>
			</div>
		</div>	
	</div>
	@endsection
	@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			$('.ui.pilihan').css({
				'width': '400px'
			})
			var kadaluarsa = $("input[name=kadaluarsa]").val();
			var d = new Date(kadaluarsa).setHours(23,59,59,0);
			// var waktu = d.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
			var countDownDate = new Date(d).getTime();
			// Update the count down every 1 second
			var x = setInterval(function() {
			// Get todays date and time
			var now = new Date().getTime();
			// Find the distance between now and the count down date
			var distance = countDownDate - now;
			// Time calculations for days, hours, minutes and seconds
			var days = Math.floor(distance / (1000 * 60 * 60 * 24));
			var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((distance % (1000 * 60)) / 1000);
			// Output the result in an element with id="demo"
			document.getElementById("demo").innerHTML = days + " {{trans('translator.Hari')}}&nbsp;&nbsp;" + hours + " {{trans('translator.Jam')}}&nbsp;&nbsp;"
			+ minutes + " {{trans('translator.Menit')}}&nbsp;&nbsp;" + seconds + " {{trans('translator.Detik')}}&nbsp;&nbsp;";
			// If the count down is over, write some text 
			if (distance < 0) {
			clearInterval(x);
			document.getElementById("demo").innerHTML = "EXPIRED";
			$('.cek').addClass('disabled');
			}
			}, 1000);


			$(document).on('click', '.save', function(e){
				var formDom = "dataForm";
				if($(this).data("form") !== undefined){
					formDom = $(this).data('form');
				}
				swal({
					title: 'Apakah Anda Yakin?',
					text: "Data yang sudah dikirim, tidak dapat dikembalikan!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Simpan',
					cancelButtonText: 'Batal',
					reverseButtons: true
				}).then((result) => {
					if (result) {
						saveFormKonfirmasi(formDom);
					}
				})
			});

			function saveFormKonfirmasi(form="dataForm")
			{
				if($("#"+form).form('is valid')){
					$('#formModal').find('.loading.dimmer').addClass('active');
					$('#cover').show();
					$("#"+form).ajaxSubmit({
						success: function(resp){
							console.log('nice');
							$("#formModal").modal('hide');
							swal(
								'Berhasil!',
								'Terimakasih sudah melakukan pembayaran. <h5>Anda bisa mengupload ulang bukti pembayaran jika ada kesalahan data selama waktu konfirmasi masih ada.</h5>',
								'success'
								).then((result) => {
									if(postSave(resp)){
										return true;
									}else{
										$('#cover').hide();
										window.location = "{{ url($pageUrl) }}";
										return true;
									}
									$('#formModal').find('.loading.dimmer').removeClass('active');
								})
							},
							error: function(resp){
								if (resp.responseJSON.status !== "undefined" && resp.responseJSON.status === 'false'){
									swal(
										'Oops, Maaf!',
										resp.responseJSON.message,
										'error'
										)
								}

								$('#cover').hide();
								var response = resp.responseJSON;
								$.each(response.errors, function(index, val) {
									clearFormError(index,val);
									showFormError(index,val);
								});
								time = 5;
								interval = setInterval(function(){
									time--;
									if(time == 0){
										clearInterval(interval);
										$('.pointing.prompt.label.transition.visible').remove();
										$('.error').each(function (index, val) {
											$(val).removeClass('error');
										});
									}
								},1000)
								$('#formModal').find('.loading.dimmer').removeClass('active');
							}
						});
				}else{
					$("#"+form).ajaxSubmit({
						success: function(resp){
							$("#formModal").modal('hide');
							swal(
								'Berhasil!',
								'Terimakasih sudah melakukan pembayaran.',
								'success'
								).then((result) => {
									if(postSave(resp)){
										return true;
									}else{
										$('#cover').hide();
										window.location = "{{ url($pageUrl) }}";
										return true;
									}
								})
							},
							error: function(resp){
								$.each(resp.responseJSON, function(index, val) {
									clearFormError(index,val);
									showFormError(index,val);
								});
								time = 5;
								interval = setInterval(function(){
									time--;
									if(time == 0){
										clearInterval(interval);
										$('.pointing.prompt.label.transition.visible').remove();
										$('.message').remove();
										$('.error').each(function (index, val) {
											$(val).removeClass('error');
										});
									}
								},1000)
							}
						});
				}
			}
		});
	</script>
	@append