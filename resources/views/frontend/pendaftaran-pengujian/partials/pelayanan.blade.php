<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Daftar Layanan</div>
<div class="content">
	<form class="ui filter form">
        <div class="inline fields">
        	<label><i class="filter circular inverted teal icon"></i> FILTER :</label>
        	<div class="field">
				<select name="filter[pelayanan]" class="watcher ui search dropdown pilihan filter-pelayanan">
					{!!
						\App\Models\Master\JenisPelayanan::options('nama', 'id', [], '(Pilih Salah Satu)')
					!!}
				</select>
			</div>
			<div class="field">
				<select name="filter[lingkup]" class="watcher ui search dropdown pilihan">
					{!!
						\App\Models\Master\Lingkup::options('nama', 'id', [], '(Pilih Salah Satu)')
					!!}
				</select>
			</div>
			<div class="field">
				<input name="filter[jenis_pengujian]" placeholder="Jenis Pengujian" type="text">
			</div>
			<button type="button" class="ui teal icon filter-pelayanan button" data-content="{{trans('translator.Cari Data')}}">
				<i class="search icon"></i>
			</button>
			<button type="reset" class="ui icon reset-pelayanan button" data-content="{{trans('translator.Bersihkan Pencarian')}}">
				<i class="refresh icon"></i>
			</button>
        </div>
    </form>
    <div class="listtable">
		@include('frontend.pendaftaran-pengujian.partials.pelayanan-table')
    </div>
</div>
<div class="actions">
	<div class="right aligned column">
		<div class="ui gray deny labeled icon button kembali">
			<i class="chevron left icon"></i>
			Kembali
		</div>
	</div>
</div>
<script type="text/javascript">
	$('.ui.search.dropdown').dropdown();

	$(document).on('click', '.kembali', function(e){
		$('#formModal').find('.loading.dimmer').removeClass('active');
	});

	$(".filter-pelayanan").on('change', function(){
		$.ajax({
			url: '{{ url('ajax/option/lingkup') }}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				jenis_pelayanan_id: $("select[name='filter[pelayanan]']").val()
			},
		})
		.done(function(response) {
			$("select[name='filter[lingkup]']").html(response)
		});
	});
</script>