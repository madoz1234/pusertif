@php
	$dataTrans = 'id';
	if(Auth::check())
	{
		if(auth()->user()->id == true){
			if(\App\Models\Translators::where('created_by',auth()->user()->id)->first()){
				$dataTrans = \App\Models\Translators::where('created_by',auth()->user()->id)->first()->translator;
			}else {
				$dataTrans = 'id';
			}
		}else {
			$dataTrans = 'id';
		}
	}
@endphp
@extends('layouts.form-user')

@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
		.fn{
			font-size: 12px;
		}
		.dataTables_info{
			font-size: 12px;
		}
		.paginate_button{
			font-size: 12px;
		}
	</style>
@append

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			judul: ['empty'],
		};
	</script>
@endsection

@section('content-body')
<div class="ui top demo tabular menu">
	<div class="active item fn" data-tab="first">{{trans('translator.Detil Order')}}</div>
	<div class="item fn" data-tab="second">{{trans('translator.Jenis & Jadwal')}}</div>
	<div class="item fn" data-tab="third">{{trans('translator.Surat Penawaran')}}</div>
	<div class="item fn" data-tab="four">{{trans('translator.Riwayat Aktivitas')}}</div>
</div>

<div class="ui bottom demo active tab" data-tab="first">
	<div class="two fields">
		<table class="ui compact table" style="border-radius: 0; margin: 0">
			<tr>
				<td width="250px"><label class="fn">{{trans('translator.Tgl Order')}}</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">{{ DateToStringYear($record->tgl_order) }}</td>
			</tr>
			<tr>
				<td width="250px" class="fn"><label>{{trans('translator.No Order')}}</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">{{ $record->no_order }}</td>
			</tr>
			<tr>
				<td width="250px" class="fn"><label>{{trans('translator.Layanan')}}</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">{{ $record->pelayanan->nama }}</td>
			</tr>
			<tr>
				<td><label class="fn">{{trans('translator.Lingkup')}}</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $record->lingkup->nama or '' }}</td>
			</tr>
			<tr>
				<td><label class="fn">{{trans('translator.Rencana Pelaksanaan')}}</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ BulanToString($record->rencana) }}</td>
			</tr>
			<tr>
				<td><label class="fn">{{trans('translator.Kelas')}}</label></td>
				<td class="fn">:</td>
				<td class="fn">@if($record->kelas == '1')
					{{trans('translator.Reguler')}}
					@else
					{{trans('translator.Prioritas')}}
					@endif
				</td>
			</tr>
			<tr>
				<td><label class="fn">{{trans('translator.No Surat Permintaan')}}</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $record->no_surat or '-' }}</td>
			</tr>
			<tr>
				<td><label class="fn">{{trans('translator.Tgl Surat Permintaan')}}</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ DateToStringYear($record->tgl_surat) }}</td>
			</tr>
			<tr>
				<td><label class="fn">{{trans('translator.File Surat Permintaan')}}</label></td>
				<td class="fn">:</td>
				<td class="fn">
					<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<a href="{{ url($pageUrl).'/download/'.$record->id }}">
							<i class="download icon"></i></a>
						</div>
					</td>
			</tr>
			<tr>
				<td><label class="fn">{{trans('translator.Channel')}}</label></td>
				<td class="fn">:</td>
				<td class="fn">@if($record->tipe == 1)
					Online
					@elseif($record->tipe == 2)
					AMS
					@else
					Manual
				@endif</td>
			</tr>
			<tr>
				<td><label class="fn">{{trans('translator.Peminta Jasa')}}</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $record->user->pelanggans->perusahaan->nama or '' }}</td>
			</tr>
			<tr>
				<td><label class="fn">{{trans('translator.Kategori')}}</label></td>
				<td class="fn">:</td>
				<td class="fn">
					@if($record->user->pelanggans->perusahaan->kategori == 0)
					PLN
					@elseif($record->user->pelanggans->perusahaan->kategori == 1)
					NON PLN
					@else
					A-PLN
					@endif
				</td>
			</tr>
			<tr>
				<td><label class="fn">{{trans('translator.Dokumen Pendukung')}}</label></td>
				<td class="fn">:</td>
				<td class="fn">
					@if(isset($record))
					@if(isset($record->files))
					@if($record->files->count() > 0)
					<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<a href="{{ url('download', $record->id).'/pendaftaran-pengujian' }}">
							<i class="download icon"></i></a>
						</div>
						@endif
						@endif
						@endif
					</td>
				</tr>
				<tr>
					<td><label class="fn">{{trans('translator.Catatan')}}</label></td>
					<td class="fn">:</td>
					<td class="fn">{{ $record->catatan or '-' }}
					</td>
				</tr>
				<tr>
					<td><label class="fn">{{trans('translator.Keputusan')}} </label></td>
					<td class="fn">:</td>
					<td class="left aligned">
						@if($record->kaji_ulang)
							@if($record->kaji_ulang->keputusan == 1)
								<a class="ui tag label fn" style="background-color:#00abffcc;">DITERIMA</a>
							@elseif($record->kaji_ulang->keputusan == 2)
								<a class="ui tag label fn" style="background-color:#dd0000cc;">DITOLAK</a>
							@else 
								<a class="ui tag label fn" style="background-color:#dddd00cc;">DIDISKUSIKAN</a>
							@endif
						@else
							-
						@endif
					</td>
				</tr>
		</table>
	</div>
	<div class="actions">
		<div class="ui two column grid">
			<div class="left aligned column">
				<br>
				<div class="ui gray deny labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					{{trans('translator.Kembali')}}
				</div>
			</div>
		</div>	
	</div>
</div>

<div class="ui bottom demo tab" data-tab="second">
	<table id="example1" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
		<thead>
			<tr class="middle aligned">
				<th width="10" class="center aligned fn">{{trans('translator.No')}}</th>
				<th width="150" class="center aligned fn">{{trans('translator.Jenis Pengujian')}}</th>
				<th width="200" class="center aligned fn">{{trans('translator.Lokasi')}}</th>
				<th width="200" class="center aligned fn">{{trans('translator.Mata Uji')}}</th>
				<th width="200" class="center aligned fn">{{trans('translator.Spesifikasi')}}</th>
				<th width="100" class="center aligned fn">{{trans('translator.Merk')}}</th>
				<th width="100" class="center aligned fn">{{trans('translator.Tipe')}}</th>
				<th width="50" class="center aligned fn">{{trans('translator.Jumlah Benda Uji')}}</th>
				<th width="150" class="center aligned" style="font-size: 12px;">{{trans('translator.Jadwal Pengujian Tentative')}}
					<br><div class="ui label">{{trans('translator.Rencana')}} : {{ BulanToString($record->rencana) }}</div>
				</th>
			</tr>
		</thead>
		<tbody class="detail fluid container detail">
			@if($kaji_ulang)
				@foreach($kaji_ulang->detail as $key => $data)
				<tr class="detail fn">
					<td class="center aligned">{{ $key+1 }}</td>
					<td class="left aligned">{{ $data->detail_pendaftaran->jenis->nama }}</td>
					<td>
						<div class="ui bulleted list">
							@foreach($data->lokasi as $kiy => $val)
								<div class="item">
									<label class="isi">
										@if($val->jenis_lokasi == 1)
											In House :
										@else 
											On Site :
										@endif
									</label>
									{{ $val->ket_lokasi }}
								</div>
							@endforeach
						</div>
					</td>
					<td width="200">
						@if($data->mata_uji)
							<div class="ui ordered list">
								@foreach($data->mata_uji as $key => $cek)
									<div class="item">{{ $cek->mata_uji }}</div>
								@endforeach
							</div>
						@else
							-
						@endif
					</td>
					<td style="text-align: justify;text-justify: inter-word;">
						{{$data->spesifikasi or '-'}}
					</td>
					<td class="left aligned" style="text-align: justify;text-justify: inter-word;">
						{{$data->detail_pendaftaran->merk or '-'}}
					</td>
					<td class="left aligned" style="text-align: justify;text-justify: inter-word;">
						{{$data->detail_pendaftaran->tipe or '-'}}
					</td>
					<td class="right aligned">
						{{ rtrim(rtrim(number_format($data->jumlah,2,',','.'), '0'), ',')}}
						<label>{{$data->detail_pendaftaran->satuan->satuan or '-'}}</label>
					</td>
					<td class="center aligned">
						{{ DateToStringYear($data->tentative_start) }} - <br><label style="margin-left: -6px;">{{ DateToStringYear($data->tentative_end) }}</label>
					</td>
				</tr>
				@endforeach
			@else
				<tr class="detail">
					<td class="center aligned fn" colspan="8">{{trans('translator.Tidak ditemukan data yang sesuai')}}</td>
				</tr>
			@endif
		</tbody>
	</table>
	<div class="actions">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				{{trans('translator.Kembali')}}
			</div>
		</div>
	</div>
</div>

<div class="ui bottom demo tab" data-tab="third">
	@if($surat)
		<form class="ui form" id="dataForm" action="{{ url($pageUrl.'saveKomponen') }}" method="POST">
			{!! csrf_field() !!}
				<div class="ui yellow segment">
					<div class="field " style="width: 100%">
						<div class="ui form">
							<div class="four fields">
								<div class="field">
									<label>{{trans('translator.No Surat Penawaran')}}</label>
									{{ $surat->no_surat or '-' }}
								</div>
								<div class="field">
									<label>{{trans('translator.Tgl Surat Penawaran')}}</label>
									{{DateToStringYear(date("Y-m-d"))}}
								</div>
								<div class="field">
									<label>{{trans('translator.Status')}}</label>
									<div class="field">
										<p style="text-align: justify;text-justify: inter-word;">
											@if($surat->adendum_status <= 0)
												Normal (Adendum 0)
											@else
												Adendum {{ $surat->adendum_status }}
											@endif</p>
									</div>
								</div>
								<div class="field">
									<label>{{trans('translator.Kelas')}}</label>
									<div class="field">
										@if($data->kelas == 1)
											{{trans('translator.Reguler')}}
										@else 
											{{trans('translator.Prioritas')}}
										@endif
									</div>
								</div>
								<div class="field" @if($record->user->pelanggans->perusahaan->kategori !== 0) style="display: none;" @endif>
									<label>No SKKI/SKKO/PRK</label>
									{{ $surat->no_skki }}
								</div>
								<div class="field" @if($record->user->pelanggans->perusahaan->kategori !== 0) style="display: none;" @endif>
									<label>Tgl SKKI/SKKO/PRK</label>
									{{ DateToStringYear($surat->tgl_skki) }}
								</div>
								<div class="field">
									<label>{{trans('translator.Total Sesudah PPn')}}</label>
									<div class="field">
										Rp. {{ rtrim(rtrim(number_format($surat->total,2,',','.'), '0'), ',')}}
									</div>
								</div>
							</div>
						</div>
						<div class="ui top attached">
							<a href="" class="ui red ribbon label">{{trans('translator.Komponen RAB')}}</a>
							<table id="example2" class="ui celled compact red table display" id="table-rab" width="100%" cellspacing="0">
								<thead>
									<tr class="center aligned">
										<th>{{trans('translator.No')}}</th>
										<th>{{trans('translator.Uraian')}}</th>
										<th>{{trans('translator.Nominal')}} (Rp)</th>
									</tr>
								</thead>
								<tbody class="komponens">
									@php
										$total = 0;
									@endphp
									@foreach($surat->detail as $kiy => $cik)
									<tr class="data_komponens" data-id="1">
										<td class="center aligned numbor numboor-1" style="width: 50px;">{{ $kiy+1 }}</td>
										<td width="300px">
											<div class="ui transparent fluid input field">
												{{$cik->komponen}}
											</div>
										</td>
										<td width="200px">
											<div class="ui transparent" style="text-align: right;">
												{{ rtrim(rtrim(number_format($cik->nominal,2,',','.'), '0'), ',')}}
												@php
													$total += $cik->nominal;
												@endphp
											</div>
										</td>
									</tr>
									@endforeach
									@php
										$ppn = (($total/100)*10);
										$total_ppn = ($total + $ppn);
									@endphp
								</tbody>
								<tfoot>
									<tr>
										<td style="border-top: 2px solid rgba(0, 0, 0, 1);"></td>
										<td scope="row" style="text-align: right;border-top: 2px solid rgba(0, 0, 0, 1);border-left: none;">{{trans('translator.Total Sebelum PPn')}}</td>
										<td style="text-align: right;border-top: 2px solid rgba(0, 0, 0, 1);" class="total">{{ rtrim(rtrim(number_format($total,2,',','.'), '0'), ',')}}</td>
									</tr>
									<tr>
										<td style="border-top: 1px solid rgba(34,36,38,.1);"></td>
										<td scope="row" style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);border-left: none;">{{trans('translator.PPn')}}</td>
										<td style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);" class="total">{{ rtrim(rtrim(number_format($ppn,2,',','.'), '0'), ',')}}</td>
									</tr>
									<tr>
										<td style="border-top: 1px solid rgba(34,36,38,.1);"></td>
										<td scope="row" style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);border-left: none;">{{trans('translator.Total Sesudah PPn')}}</td>
										<td style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);" class="total">{{ rtrim(rtrim(number_format($total_ppn,2,',','.'), '0'), ',')}}</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
		</form>
	@else 
	<div class="ui icon message">
		<i class="notched circle loading icon"></i>
		<div class="content">
			<div class="header">
				@if($record->kaji_ulang)
					@if($record->kaji_ulang->surat)
					@else
						{{trans('translator.Data Masih dalam tahap kaji ulang')}}. 
					@endif
				@else 
					{{trans('translator.Data Masih dalam tahap penerimaan surat')}}.
				@endif
			</div>
			@if($record->kaji_ulang)
				@if($record->kaji_ulang->surat)
				@else
					<p>{{trans('translator.Data akan tampil setelah selesai tahap kaji ulang')}}.</p>
				@endif
			@else 
				<p>{{trans('translator.Data akan tampil setelah selesai tahap penerimaan surat')}}.</p>
			@endif
		</div>
	</div>
	@endif
	<div class="actions">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				{{trans('translator.Kembali')}}
			</div>
		</div>
	</div>
</div>
<div class="ui bottom demo tab" data-tab="four">
	@if($record->act_dispo()->count() > 0)
		@php 
			$prev = $record->logsyan()->first(); 
		@endphp
{{-- 		@foreach($record->act_dispo->sortByDesc('created_at') as $key => $cek)
			<div class="ui feed">
				<div class="event">
					<div class="label">
						<img src="{{ url(asset('img/avatar04.png')) }}">
					</div>
					<div class="content">
						<div class="date">
							{{$cek->created_at->diffForHumans()}}
						</div>
						<div class="summary">
							@php
								$Date1 	= $cek->created_at;
								$prev 	= $cek->prev();
							@endphp

							@if($cek->tipe == 0)
								<a>{{ $cek->pengirim->roles->first()->display_name }}</a> mengubah data Layanan, Lingkup dan Jenis Pengujian
							@elseif($cek->tipe == 1)
								<a>{{ $cek->pengirim->roles->first()->display_name }}</a>  Mengalihkan surat <a>{{ $record->no_order }}</a> kepada <a>{{ $cek->penerima->roles->first()->display_name }}</a> @if($cek->keterangan)<i>dengan catatan : {{ $cek->keterangan }}</i>@endif
							@elseif($cek->tipe == 2)
								<a>{{ $cek->pengirim->roles->first()->display_name }}</a>  Mendisposisikan surat <a>{{ $record->no_order }}</a> kepada <a>{{ $cek->penerima->roles->first()->display_name }}</a> @if($cek->keterangan)<i>dengan catatan : {{ $cek->keterangan }}</i>@endif
								, dalam waktu &nbsp;&nbsp; {{ timestamp($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name) }}
							@elseif($cek->tipe == 3)
								@if($cek->status_kaji_ulang == 0)
									<a>{{ $cek->pengirim->roles->first()->display_name }}</a> menyelesaikan <a>Kaji Ulang</a> dengan status <a>Diterima</a>, dalam waktu &nbsp;&nbsp; {{ timestamp($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name) }}
								@elseif($cek->status_kaji_ulang == 1)
									<a>{{ $cek->pengirim->roles->first()->display_name }}</a> menyelesaikan <a>Kaji Ulang</a> dengan status <a>Didiskusikan</a>, dalam waktu &nbsp;&nbsp; {{ timestamp($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name) }}
								@else
									<a>{{ $cek->pengirim->roles->first()->display_name }}</a> menyelesaikan <a>Kaji Ulang</a> dengan status <a>Diterima</a>, dalam waktu &nbsp;&nbsp; {{ timestamp($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name) }}
								@endif
							@elseif($cek->tipe == 4)
								<a>{{ $cek->pengirim->roles->first()->display_name }}</a> menyelesaikan <a>Surat Penawaran,</a> No Order <a>{{ $record->no_order }}</a>@if($cek->status_surat_penawaran > 0) dengan <a> Adendum {{$cek->status_surat_penawaran}}</a> @endif
							@elseif($cek->tipe == 5)
								<a>{{ $cek->pengirim->roles->first()->display_name }}</a> menyelesaikan <a> Virtual Account,</a> No Order <a>{{ $record->no_order }}</a>@if($cek->status_surat_penawaran > 0) dengan <a> Adendum {{$cek->status_surat_penawaran}}</a> @endif dan mengirim email ke <a>{{ $record->user->pelanggans->perusahaan->nama or '' }}</a>
							@else
							@endif
						</div>
					</div>
				</div>
			</div>
		@endforeach --}}
		@if($record->kaji_ulang)
			@if($record->kaji_ulang->surat)
				@if($record->kaji_ulang->surat->va)
					@if($record->kaji_ulang->surat->va->konfirmasi)
						@if($record->kaji_ulang->surat->va->konfirmasi->penerimaan)
							@if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->closeorder)
								<div class="ui feed">
									<div class="event">
										<div class="label">
											<img src="{{ url(asset('img/avatar04.png')) }}">
										</div>
										<div class="content">
											<div class="date">
												{{$record->kaji_ulang->surat->va->konfirmasi->penerimaan->closeorder->created_at->diffForHumans()}}
											</div>
											<div class="summary">
												<a>Close Order</a>
											</div>
										</div>
									</div>
								</div>
							@else
							@endif
						@else
						@endif
					@else
					@endif
				@else
				@endif
			@else
			@endif
		@else
		@endif
		@if($record->kaji_ulang)
			@if($record->kaji_ulang->surat)
				@if($record->kaji_ulang->surat->va)
					@if($record->kaji_ulang->surat->va->konfirmasi)
						@if($record->kaji_ulang->surat->va->konfirmasi->penerimaan)
							@if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian)
								@if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->laporan_pengujian)
									<div class="ui feed">
										<div class="event">
											<div class="label">
												<img src="{{ url(asset('img/avatar04.png')) }}">
											</div>
											<div class="content">
												<div class="date">
													{{$record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->laporan_pengujian->first()->created_at->diffForHumans()}}
												</div>
												<div class="summary">
													<a>Pengiriman Laporan</a>
												</div>
											</div>
										</div>
									</div>
								@else
								@endif
							@else
							@endif
						@else
						@endif
					@else
					@endif
				@else
				@endif
			@else
			@endif
		@else
		@endif
		@if($dispo = $record->act_dispo->where('tipe',6)->where('jenis',5)->sortByDesc('created_at')->first())
		<div class="ui feed">
			<div class="event">
				<div class="label">
					<img src="{{ url(asset('img/avatar04.png')) }}">
				</div>
				<div class="content">
					<div class="date">
						{{$dispo->created_at->diffForHumans()}}
					</div>
					<div class="summary">
						<a>Pengujian</a>
					</div>
				</div>
			</div>
		</div>
		@endif
		@if($dispo = $record->act_dispo->where('tipe',5)->sortByDesc('created_at')->first())
		<div class="ui feed">
			<div class="event">
				<div class="label">
					<img src="{{ url(asset('img/avatar04.png')) }}">
				</div>
				<div class="content">
					<div class="date">
						{{$dispo->created_at->diffForHumans()}}
					</div>
					<div class="summary">
						<a>Konfirmasi Pembayaran</a>
					</div>
				</div>
			</div>
		</div>
		@endif
		@if($dispo = $record->act_dispo->where('tipe',4)->sortByDesc('created_at')->first())
		<div class="ui feed">
			<div class="event">
				<div class="label">
					<img src="{{ url(asset('img/avatar04.png')) }}">
				</div>
				<div class="content">
					<div class="date">
						{{$dispo->created_at->diffForHumans()}}
					</div>
					<div class="summary">
						<a>Virtual Account</a>
					</div>
				</div>
			</div>
		</div>
		@endif
		@if($dispo = $record->act_dispo->where('tipe',3)->where('status_kaji_ulang',1)->sortByDesc('created_at')->first())
		<div class="ui feed">
			<div class="event">
				<div class="label">
					<img src="{{ url(asset('img/avatar04.png')) }}">
				</div>
				<div class="content">
					<div class="date">
						{{$dispo->created_at->diffForHumans()}}
					</div>
					<div class="summary">
						<a>Surat Penawaran</a>
					</div>
				</div>
			</div>
		</div>
		@endif
		@if($dispo = $record->act_dispo->where('tipe',2)->where('jenis',1)->sortByDesc('created_at')->first())
		<div class="ui feed">
			<div class="event">
				<div class="label">
					<img src="{{ url(asset('img/avatar04.png')) }}">
				</div>
				<div class="content">
					<div class="date">
						{{$dispo->created_at->diffForHumans()}}
					</div>
					<div class="summary">
						<a>Kaji Ulang</a>
					</div>
				</div>
			</div>
		</div>
		@endif
		<div class="ui feed">
			<div class="event">
				<div class="label">
					<img src="{{ url(asset('img/avatar04.png')) }}">
				</div>
				<div class="content">
					<div class="date">
						{{$record->created_at->diffForHumans()}}
					</div>
					<div class="summary">
						<a>{{trans('translator.Penerimaan Surat')}}</a>
					</div>
				</div>
			</div>
		</div>
	@else
		<div class="ui feed">
			<div class="event">
				<div class="label">
					<img src="{{ url(asset('img/avatar04.png')) }}">
				</div>
				<div class="content">
					<div class="date">
						{{$record->created_at->diffForHumans()}}
					</div>
					<div class="summary">
						<a>{{trans('translator.Penerimaan Surat')}}</a>
					</div>
				</div>
			</div>
		</div>
	@endif
	<div class="actions">
		<div class="ui two column grid">
			<div class="left aligned column">
				<br>
				<div class="ui gray deny labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					{{trans('translator.Kembali')}}
				</div>
			</div>
		</div>	
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$('#example1').DataTable( {
			"paging":   10,
			"ordering": false,
			"lengthChange": false,
			"filter": false,
			"info":     true,
			language: {
				url: "{{ asset('plugins/datatables/Indonesian.json') }}"
			},
		});

		$('#example2').DataTable( {
			"paging":   10,
			"ordering": false,
			"lengthChange": false,
			"filter": false,
			"info":     true,
			language: {
				url: "{{ asset('plugins/datatables/Indonesian.json') }}"
			},
		});
	})
</script>
@append