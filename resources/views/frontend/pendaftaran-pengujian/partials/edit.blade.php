<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Item Uji</div>
<div class="content">
	<form class="ui data form" id="formPengujian" action="{{ url($pageUrl.'request/item-uji') }}" method="POST">
		{!! csrf_field() !!}
		<table class="ui compact table" style="border-radius: 0; margin: 0">
			<tr>
				<td width="150"><b>Jenis Pengujian</b></td>
				<td width="10">:</td>
				<td>
					<div class="field">
						<select name="jenis_id" id="jenis_id" class="watcher ui search pilihan dropdown">
							<option value="">(Pilih Salah Satu)</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td width="150"><b>Merk</b></td>
				<td width="10">:</td>
				<td>
					<div class="field">
						<input type="text" name="merk" id="merk" placeholder="Merk">
					</div>
				</td>
			</tr>
			<tr>
				<td width="150"><b>Tipe</b></td>
				<td width="10">:</td>
				<td>
					<div class="field">
						<input type="text" name="tipe" id="tipe" placeholder="Tipe">
					</div>
				</td>
			</tr>
			<tr>
				<td width="150"><b>Jumlah</b></td>
				<td width="10">:</td>
				<td>
					<div class="field">
						<input type="text" name="jumlah" id="jumlah" class="number" placeholder="Jumlah">
					</div>
				</td>
			</tr>
			<tr>
				<td width="150"><b>Satuan</b></td>
				<td width="10">:</td>
				<td>
					<div class="field">
						<input type="text" name="satuan" id="satuan" placeholder="Satuan">
					</div>
				</td>
			</tr>
			<tr>
				<td width="150"><b>Spesifikasi</b></td>
				<td width="10">:</td>
				<td>
					<div class="field">
						<textarea name="spesifikasi" class="areas input" id="spesifikasi" placeholder="Spesifikasi" rows="2"></textarea>
					</div>
				</td>
			</tr>
		</table>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button" style="background-color: #363636;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button" style="background-color: #00AEEF;">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$('.ui.pilihan').dropdown();
	$(".number").on("keypress keyup blur",function (e) {    
		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	});
	$.ajax({
		url: '{{ url('ajax/option/jenis') }}',
		type: 'POST',
		data: {
			_token: "{{ csrf_token() }}",
			jenis_pelayanan_id: $('select[name=jenis_pelayanan_id]').val(),
			lingkup_id: $('select[name=lingkup_id]').val(),
		},
	})
	.done(function(response) {
		$('select[name=jenis_id]').html(response)
	})
	.fail(function() {
		console.log("error");
	});
</script>