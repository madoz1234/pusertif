<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">{{trans('translator.Upload Item Uji')}}</div>
<div class="content">
	<form class="ui form" id="formUpload" action="{{ url($pageUrl.'request/data-upload') }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="lingkup_id" value="{{ $lingkup_id }}">
		<table class="table" style="border-radius: 0; margin: 0">
			<tr>
				<td style="width: 22%;"><b>{{trans('translator.Pilih File')}}</b></td>
				<td width="10">:</td>
				<td style="width: 100%;">
					<div class="field">
						<div class="ui action choises input">
							<input type="text" class="isi_upload_data upload_data" readonly placeholder="Format .xls">
							<input type="file" id="upload_data" name="upload_data" style="display: none!important;" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel">
							<div class="ui icon upload_data button">
								<i class="cloud upload alternate icon"></i>
							</div>
						</div>
						<div class="required inline field">
							<label>maks 350 {{trans('translator.item uji')}}</label>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button" style="background-color: #363636;">
		{{trans('translator.Batal')}}
	</div>
	<div class="ui positive right labeled icon save data button" style="background-color: #00AEEF;">
		{{trans('translator.Simpan')}}
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	
</script>