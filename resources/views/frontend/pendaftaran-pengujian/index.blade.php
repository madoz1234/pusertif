@extends('layouts.list-user')

@section('js-filters')
    d.no_order = $("input[name='filter[no_order]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			title: ['empty'],
		};
	</script>
@endsection

@section('filters')
	<div class="field">
		<input name="filter[no_order]" placeholder="{{trans('translator.No Order')}}" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="{{trans('translator.Cari Data')}}">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="{{trans('translator.Bersihkan Pencarian')}}">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
	<button type="button" class="ui blue add-page button" style="background-color: #363636;">
		<i class="plus icon"></i>
		{{trans('translator.Buat Order Baru')}}
	</button>
@endsection
@section('init-modal')
	<script>
		onShow = function(){
        	var nowDate = new Date();
        	var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), 0, 0, 0, 0);
        	var tanggal = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);

            $('#tgl').calendar({
        		ampm: false,
        		type: 'date',
        		endCalendar: tanggal,
        		formatter: {
        			date: function (date, settings) {
        				if (!date) return '';
        				let momentDate = moment(date)
        				return momentDate.format('YYYY-MM-DD')
        			}
        		}
        	});
            return false;
        };
	</script>
@endsection
