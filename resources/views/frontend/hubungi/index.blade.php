@extends('layouts.front-secondary-dynamic')

@section('scripts')
@include('frontend.tracking.script.script')
@endsection

@section('content')
<div class="ui middle aligned stackable grid container">
	<div class="row">
		<div class="sixteen wide column">
			<div class="ui inverted dimmer loading">
			    <div class="ui text loader">Loading</div>
			</div>
			<div class="content">
				<div class="card" style="width: 100%;">
					<div class="content">
						<h4 class="ui green color header">{{trans('translator.Silahkan isi form berikut untuk menyampaikan aspirasi Anda')}}</h4>
						<div class="ui divider"></div>
						<div class="content">
							<div class="ui main container">
								<form class="ui form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
									{!! csrf_field() !!}
									<div class="two fields">
										<div class="field">
											<label>{{trans('translator.Perihal')}}</label>
											<div class="one field">
												<div class="field">
													<select id="pilihan" class="ui watcher search dropdown" name="pilihan">
														<option value="">{{trans('translator.pilih salah satu')}}</option>
														<option value="1">{{trans('translator.Aduan')}}</option>
														<option value="2">{{trans('translator.Saran')}}</option>
													</select>
												</div>
											</div>
										</div>
										<div class="field">
											<label>{{trans('translator.No HP')}}</label>
											<div class="one field">
												<div class="field">
													<input type="text" id="no_hp" name="no_hp" placeholder="{{trans('translator.No HP')}}">
												</div>
											</div>
										</div>
									</div>
									<div class="two fields">
										<div class="field">
											<label>{{trans('translator.Nama')}}</label>
											<div class="one field">
												<div class="field">
													<input type="text" id="nama" name="nama" placeholder="{{trans('translator.Nama')}}">
												</div>
											</div>
										</div>
										<div class="field">
											<label>{{trans('translator.Lampiran')}}</label>
											<div class="one field">
												<div class="field">
													<div class="ui file action choises input">
														<input id="lampiran" type="text" readonly placeholder="Format .doc, .xlsx, .pdf, .jpg, .jpeg, .png">
														<input type="file" name="lampiran" style="display: none!important;" accept=".doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf,image/x-eps,image/jpeg,image/gif,image, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
														<div class="ui icon button">
															<i class="cloud upload alternate icon"></i>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="two fields">
										<div class="field">
											<label>Email</label>
											<div class="one field">
												<div class="field">
													<input type="text" id="email" name="email" placeholder="Email">
												</div>
											</div>
										</div>
									</div>
									<div class="field">
										<label>{{trans('translator.Pesan')}}</label>
										<div class="one field">
											<div class="field">
												<textarea rows="5" id="pesan" name="pesan" placeholder="{{trans('translator.Pesan')}}"></textarea>
											</div>
										</div>
									</div>
									<div class="actions">
										<div class="ui two column grid">
											<div class="left aligned column">
												<div class="ui gray deny labeled icon reset button">
													<i class="undo left icon"></i>
													Reset
												</div>
											</div>
											<div class="right aligned column">		
												<button type="button" class="ui positive right labeled icon simpan button">
													Submit
													<i class="save icon"></i>
												</button>
											</div>
										</div>	
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
		$(document).ready(function($) {
			document.getElementById("pilihan").tabIndex = "1";
			document.getElementById("nama").tabIndex = "2";
			document.getElementById("email").tabIndex = "3";
			document.getElementById("no_hp").tabIndex = "4"; 
			document.getElementById("lampiran").tabIndex = "5"; 
			document.getElementById("pesan").tabIndex = "6"; 
			$('.ui.dropdown').dropdown();
			$(document).on('click', '.reset', function(e){
				window.location.reload();
				$('#dataForm').trigger("reset");
			});
			$(document).on('click', '.ui.file.input input:text, .ui.button', function(e) {
				$(e.target).parent().find('input:file').click();
			});

			$(document).on('change', '.ui.file.input input:file', function(e) {
				var file = $(e.target);
				var name = '';

				for (var i=0; i<e.target.files.length; i++) {
					name += e.target.files[i].name + ', ';
				}
				name = name.replace(/,\s*$/, '');

			$('input:text', file.parent()).val(name);
			});

			$(document).on('click', '.simpan', function(e){
				var formDom = "dataForm";
				if($(this).data("form") !== undefined){
					formDom = $(this).data('form');
				}
				swal({
					title: 'Apakah Anda Yakin?',
					// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Simpan',
					cancelButtonText: 'Batal',
					reverseButtons: true
					}).then((result) => {
						if (result) {
							saveForm(formDom);
						}
					})
			});

			function saveForm(form="dataForm")
			{
				if($("#"+form).form('is valid')){
					$('.loading.dimmer').addClass('active');
					$('#cover').show();
					$("#"+form).ajaxSubmit({
						success: function(resp){
							$("#formModal").modal('hide');
							swal(
								'Tersimpan!',
								'Data berhasil disimpan.',
								'success'
								).then((result) => {
									$('.loading.dimmer').removeClass('active');
									window.location.reload();
									$('#dataForm').trigger("reset");
								})
							},
							error: function(resp){
								$('.loading.dimmer').removeClass('active');
								$.each(resp.responseJSON.errors, function(index, val) {
									clearFormError(index,val);
									showFormError(index,val);
								});
								time = 7;
								interval = setInterval(function(){
									time--;
									if(time == 0){
										$('.red.error-label').remove();
										$('.pointing.prompt.label.transition.visible').remove();
										$('.error').each(function (index, val) {
											$(val).removeClass('error');
										});
										clearInterval(interval);
									}
								},1000);
							}
						});
				}
			}

			showFormError = function(key, value)
			{
				if(key.includes("."))
				{
					res = key.split('.');
					key = res[0] + '[' + res[1] + ']';
					if(res[1] == 0)
					{
						key = res[0] + '\\[\\]';
						var exist = $('#dataForm' + ' [name="' + res[0] + '[' + res[1] + ']' + '"]');
						if(exist.length > 0)
						{
							key = res[0] + '[' + res[1] + ']';
						}
					}
					if(res[2])
					{
						key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']';
						if(res[2] == 0)
						{
							key = res[0] + '['+ res[1] +']' + '\\[\\]';
						}
					}
					if(res[3])
					{
						key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']';
						if(res[3] == 0)
						{
							key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '\\[\\]';
						}
					}
					if(res[4])
					{
						key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']';
						if(res[4] == 0)
						{
							key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '\\[\\]';
						}
					}
					if(res[5])
					{
						key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']' + '[' + res[5] + ']';
						if(res[5] == 0)
						{
							key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '['+ res[4] +']' + '\\[\\]';
						}
					}
				}
				var elm = $('#dataForm' + ' [name^="' + key + '"]').closest('.field');
				var tabs = $('#dataForm' + ' [name^="' + key + '"]').parents('.tab.segment');
				if(tabs.length > 0)
				{
					selectedTabs(tabs);
				}
				var message = `<div class="ui basic red pointing prompt label transition visible">`+ value +`</div>`;
				var showerror = $('#dataForm' + ' [name^="' + key + '"]').closest('.field');
				var multipleCheckbox = $(showerror).parents('.multiple-checkbox');
				if($('#dataForm' + ' [name^="' + key + '"]').closest('.field').find('input').length > 0 && $('#dataForm' + ' [name^="' + key + '"]').closest('.field').find('input').hasClass('hidden'))
				{
					$(elm).addClass('error');
					if($('#dataForm' + ' [name^="' + key + '"]').closest('.field').find('div').length > 0)
					{
						$('#dataForm' + ' [name^="' + key + '"]').closest('.field').find('div').append('<div class="ui left pointing red basic label">' + value + '</div>');
					}else{
						$(showerror).append('<div class="ui basic red pointing prompt label transition visible">' + value + '</div>');
					}
				}else{
					if(multipleCheckbox.length > 0)
					{
						multipleCheckboxLabel = multipleCheckbox.find('label:first-child');
						$(multipleCheckboxLabel).append('<span class="red error-label" style="color:#9f3a38 !important;">' + value + '</span>');
					}else{
						$(elm).addClass('error');
						$(showerror).append('<div class="ui basic red pointing prompt label transition visible">' + value + '</div>');
					}
				}
			}

			clearFormError = function(key, value)
			{
				if(key.includes("."))
				{
					res = key.split('.');
					key = res[0] + '[' + res[1] + ']';
					if(res[1] == 0)
					{
						key = res[0] + '\\[\\]';
						var exist = $('#dataForm' + ' [name="' + res[0] + '[' + res[1] + ']' + '"]');
						if(exist.length > 0)
						{
							key = res[0] + '[' + res[1] + ']';
						}
					}
					if(res[2])
					{
						key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']';
						if(res[2] == 0)
						{
							key = res[0] + '['+ res[1] +']' + '\\[\\]';
						}
					}
					if(res[3])
					{
						key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']';
						if(res[3] == 0)
						{
							key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '\\[\\]';
						}
					}
					if(res[4])
					{
						key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']';
						if(res[4] == 0)
						{
							key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '\\[\\]';
						}
					}
					if(res[5])
					{
						key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']' + '[' + res[5] + ']';
						if(res[5] == 0)
						{
							key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '['+ res[4] +']' + '\\[\\]';
						}
					}
				}
				if($('#dataForm' + ' [name^="' + key + '"]').closest('.field').find('div').length > 0){
					var elm = $('#dataForm' + ' [name^="' + key + '"]').closest('.field');
					$(elm).removeClass('error');

					var showerror = $('#dataForm' + ' [name^="' + key + '"]').closest('.field').find('.ui.left.pointing.red.basic.label').remove();
				}else{
					var elm = $('#dataForm' + ' [name^="' + key + '"]').closest('.field');
					$(elm).removeClass('error');

					var showerror = $('#dataForm' + ' [name^="' + key + '"]').closest('.field').find('.ui.basic.red.pointing.prompt.label.transition.visible').remove();
				}
			}
		});
</script>
@append
