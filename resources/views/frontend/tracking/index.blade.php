@extends('layouts.front-secondary-dynamic')

@section('scripts')
@include('frontend.tracking.script.script')
@endsection

@section('content')
<div class="ui middle aligned stackable grid container">
	<div class="row">
		<div class="sixteen wide column">
			<div class="ui inverted dimmer loading">
			    <div class="ui text loader">Loading</div>
			</div>
			<div class="content">
				<div class="card" style="width: 100%;">
					<div class="content">
						<h4 class="ui green color header">Input ID Tracking & Surat Permintaan</h4>
						<form class="ui form">
							<div class="inline fields">
								<div class="field">
										<input type="text" name="filter[display_name]" placeholder="No Order" style="width: 250px">
								</div>
								<div class="field">
										<input type="text" name="filter[display_name_surat]" placeholder="No Surat Permintaan" style="width: 250px">
								</div>
								<button type="button" class="ui teal icon filter button" data-content="Cari Data"><i class="search icon"></i> Submit
								</button>
							</div>
						</form>
						<div class="ui divider"></div>
						<div class="appendTables">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

