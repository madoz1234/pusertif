@if($records->count() > 0)
	@foreach($records as $row)
		<div class="ui horizontal segments">
			<table id="example" class="ui celled compact table" width="100%" cellspacing="0">
				<tr>
					<td style="width:350px">{{trans('translator.No Permintaan')}}</td>
					<td>{{ $row->no_order }}</td>
				</tr>
				<tr>
					<td style="width:350px">{{trans('translator.Layanan')}}</td>
					<td>{{ $row->pelayanan->nama }}</td>
				</tr>
				<tr>
					<td style="width:350px">{{trans('translator.Perusahaan')}}</td>
					<td>{{ $row->user->pelanggans->perusahaan->nama }}</td>
				</tr>
				<tr>
					<td style="width:350px">{{trans('translator.PIC')}}</td>
					<td>{{ $row->user->pelanggans->nama_lengkap }}</td>
				</tr>
				<tr>
					<td style="width:350px">Status</td>
					@php
						if($row->status == 1){
							$string = status($row->status);
							// $tgl = \Carbon::parse($row->kaji_ulang->surat->created_at)->format('Y-m-d');
						}elseif($row->status == 2){
							if($row->kaji_ulang){
								if($row->kaji_ulang->surat){
									if($row->kaji_ulang->surat->va){
										if($row->kaji_ulang->surat->va->status == 0){
											$string = "Menunggu Virtual Account";
										}elseif($row->kaji_ulang->surat->va->status == 1){
											if($row->user->pelanggans->perusahaan->kategori !== 0){
												$string = "Konfirmasi Pembayaran";
											}else{
												$string = "Menunggu Konfirmasi Pembayaran";
											}
										}elseif($row->kaji_ulang->surat->va->status == 2){
											if($row->kaji_ulang->surat->va->bukti_url){
												$string = "Virtual Account Kadaluarsa";
											}else{
												$string = "Virtual Account Nonaktif";
											}
										}else{
											if($row->user->pelanggans->perusahaan->kategori !== 0){
												if($row->kaji_ulang->surat->va->konfirmasi){
													if($row->kaji_ulang->surat->va->konfirmasi->status == 1){
														$string = "Kurang Bayar";
													}elseif($row->kaji_ulang->surat->va->konfirmasi->status == 2){
														$string = "Pengembalian Dana";
													}elseif($row->kaji_ulang->surat->va->konfirmasi->status == 3){
														if($row->kaji_ulang->surat->va->konfirmasi->penerimaan){
															if($row->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian){
																$string = "Pengujian";
															}else{
																$string = "Menunggu Pengujian";
															}
														}else{
															$string = "Penyerahan Barang Uji";
														}
													}else{
														$string = "Menunggu Konfirmasi Pembayaran";
													}
												}else{
													$string = "Menunggu Konfirmasi Pembayaran";
												}
											}else{
												if($row->kaji_ulang->surat->va->konfirmasi){
													if($row->kaji_ulang->surat->va->konfirmasi->penerimaan){
														if($row->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian){
															$string = "Pengujian";
														}else{
															$string = "Menunggu Pengujian";
														}
													}else{
														$string = "Penyerahan Barang Uji";
													}
												}else{
													$string = "Pembayaran Berhasil";
												}
											}
										}
									}else{
										$string = "Menunggu Surat Penawaran Terkirim";
									}
								}else{
									if($row->kaji_ulang->keputusan == 3){
										$string = "Didiskusikan";
									}elseif($row->kaji_ulang->keputusan == 2){
										if($row->kaji_ulang->bukti){
											$string = "Ditolak";
										}else{
											$string = "Menunggu Surat Penawaran";
										}
									}else{
										$string = "Menunggu Surat Penawaran";
									}
								}
							}else{
								return '<label class="ui olive tag label">Menunggu Kaji Ulang</label>';
							}
						}elseif($row->status == 3){
							$string ='-';
						}elseif($row->status == 4){
							$string = status($row->status);
						}elseif($row->status == 5){
							$string = status($row->status);
						}elseif($row->status == 6){
							$string = status($row->status);
						}elseif($row->status == 7){
							$string = status($row->status);
						}elseif($row->status == 8){
							$string = status($row->status);
						}elseif($row->status == 9){
							$string = status($row->status);
						}elseif($row->status == 10){
							if($row->kaji_ulang->bukti){
								$string = "Ditolak";
							}else{
								$string = "Menunggu Surat Penawaran";
							}
							$string = status($row->status);
						}else{
							$string ='-';
						}
						// if($row->kaji_ulang){
						// 	if($row->kaji_ulang->surat){
						// 		$string = 'Surat Penawaran';
						// 		$tgl = \Carbon::parse($row->kaji_ulang->surat->created_at)->format('Y-m-d');
						// 	}else{
						// 		$string = 'Kaji Ulang';
						// 		$tgl = \Carbon::parse($row->kaji_ulang->created_at)->format('Y-m-d');
						// 	}
						// }else{
						// 	if($row->act_dispo->count() > 0){
						// 		$string = 'Surat Penawaran Terkirim';
						// 		$tgl =  \Carbon::parse($row->act_dispo->last()->created_at)->format('Y-m-d');
						// 	}else{
						// 		$string = 'Penerimaan Surat';
						// 		$tgl = \Carbon::parse($row->created_at)->format('Y-m-d');
						// 	}
						// }
					@endphp
					<td>{{ $string }}</td>
				</tr>
			</table>
		</div>
	@endforeach
@else
	<div class="ui segments" style="text-align:center">
		<center>~ {{ trans('translator.Data Tidak Ditemukan') }}~</center>
	</div>
@endif