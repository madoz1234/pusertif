<script type="text/javascript">
	$(document).on('click', '.filter', function(){
		var hasil = $('input[name="filter[display_name]"]').val();
		var hasil2 = $('input[name="filter[display_name_surat]"]').val();


		if(hasil !== '' || hasil2!==''){
			$('.loading').addClass('active');

			$.ajax({
				url: '{{  url($pageUrl) }}/search',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: hasil,
					surat: hasil2,
				},
			})
			.done(function(response) {
				$('.loading').removeClass('active');

				$('.appendTables').html(response);

				// console.log("success");
			})
			.fail(function(response) {
				$('.loading').removeClass('active');

				console.log("error");
			});

			var html = `
				    <div class="ui horizontal segments">
						<table id="example" class="ui celled compact table" width="100%" cellspacing="0">
							<tr>
								<td style="width:350px">Layanan</td>
								<td>Pengujian KWh Meter</td>
							</tr>
							<tr>
								<td style="width:350px">Status</td>
								<td>Kaji Ulang</td>
							</tr>
							<tr>
								<td style="width:350px">Tgl</td>
								<td>20 Februari 2019</td>
							</tr>
						</table>
					</div>
				    <div class="ui horizontal segments">
						<table id="example" class="ui celled compact table" width="100%" cellspacing="0">
							<tr>
								<td style="width:350px">Layanan</td>
								<td>Pengujian Relay Arah</td>
							</tr>
							<tr>
								<td style="width:350px">Status</td>
								<td>Surat Penawaran</td>
							</tr>
							<tr>
								<td style="width:350px">Tgl</td>
								<td>14 Februari 2019</td>
							</tr>
						</table>
					</div>
				    <div class="ui horizontal segments">
						<table id="example" class="ui celled compact table" width="100%" cellspacing="0">
							<tr>
								<td style="width:350px">Layanan</td>
								<td>Kalibrasi Clamp Meter</td>
							</tr>
							<tr>
								<td style="width:350px">Status</td>
								<td>Kalibrasi</td>
							</tr>
							<tr>
								<td style="width:350px">Tgl</td>
								<td>31 Janurai 2019</td>
							</tr>
						</table>
					</div>
			`;
		}else{
			var html = `
				<div class="ui segments" style="text-align:center">
						<center>~ {{ trans('translator.Data Tidak Ditemukan') }} ~</center>
				</div>
			`;
			$('.appendTables').html(html);
		}
		
	});
</script>