@extends('layouts.front')

@section('scripts')
<script>

	var openModal = function (string){
		alert(string)
		$('#modal-uji').modal('show')
	}

	$('#tracking').slideUp()
	$('#modal-tracking').modal({
		observeChanges: true, 
		onShow: function(){
			$('#tracking').slideUp()
		},
		onHide: function(){
			$('#tracking').slideUp()
		}
	})

	function openTrackingModal(){
		$('#modal-tracking').modal('show')
		setTimeout(function(){
			$('#modal-tracking .ui.dimmer').fadeOut(100)
		}, 750)
	}

	function openTrackingResult(){
		$('#modal-tracking .ui.dimmer').fadeIn(100)
		setTimeout(function(){
			$('#tracking').slideDown()
			$('#modal-tracking').modal('refresh')
			$('#modal-tracking .ui.dimmer').fadeOut(100)
		}, 1000)
	}

</script>
@append

@section('content')
@include('frontend.guest.home')
@include('frontend.guest.about')
@include('frontend.guest.work-order')
{{-- @include('frontend.guest.features') --}}
{{-- @include('frontend.guest.scope') --}}
{{-- <div class="ui center aligned vertical stripe segment" id="banner"></div> --}}
{{-- @include('frontend.guest.certifications') --}}
@endsection

@section('modals')
<div class="ui basic feature modal" id="modal-uji">
	<div class="content">
		<div class="ui centered cards">
			<div class="card" style="width: 100%;">
				<div class="image">
					<img src="{{asset('img/front/pengujian.png')}}">
				</div>
				<div class="content">
					<h3 class="ui green color header">Lingkup Pengujian</h3>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Sipil</div>
								<div class="description">Tanah</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Mekanik</div>
								<div class="description">Vibraasi, Tube</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Kimia</div>
								<div class="description">Minyak Insulasi, Batubara</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Fisika</div>
								<div class="description">Batubara</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Kelistrikan</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>KWh Meter</li>
										<li>Indeks Pengaman</li>
										<li>MCB</li>
										<li>Kabel Listrik Berinsulasi PVC</li>
										<li>Kabel Daya</li>
										<li>Kabel Pilih Udara</li>
										<li>Isolator</li>
										<li>Stick Isolasi</li>
										<li>PHB TR</li>
										<li>PHB TM</li>
										<li>Lengkapan Kabel</li>
										<li>Trafo Distribusi</li>
										<li>Trafo Arus (CT)</li>
										<li>Transformer Tegangan (PT)</li>
										<li>Relay Jarak</li>
										<li>Reklay Differensial Kawat Pilot</li>
										<li>Relay Arus Lebih</li>
										<li>Relay Tegangan Lebih/Kurang</li>
										<li>Relay Arah</li>
										<li>Relay Cek Sinkron</li>
										<li>Relay Gangguan Tanah Terbatas (REF)</li>
										<li>Relay Penutup Balik</li>
										<li>Relay Diferensial Trafo Tenaga</li>
										<li>Relay Tunda Waktu</li>
										<li>Relay Penguat Medan Hilang</li>
										<li>Relay Thermal Overload</li>
										<li>Transformator Tenaga</li>
										<li>Penangkap Petir/Arrester</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<h4 class="ui green ribbon label">Dukungan Akreditasi</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Sertifikasi Akreditasi KAN</div>
								<div class="description">No. LP-005-IDN (SNI ISO/IEC 17025:2017)</div>
							</div>
						</div>
					</div>
				</div>
				<div class="extra content">
					<a>
						<i class="user icon"></i>
						22 Friends
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ui basic feature modal" id="modal-kal">
	<div class="content">
		<div class="ui centered cards">
			<div class="card" style="width: 100%;">
				<div class="image">
					<img src="{{asset('img/front/kalibrasi.png')}}">
				</div>
				<div class="content">
					<h3 class="ui orange header">Lingkup Kalibrasi</h3>
					<h4 class="orange text">Kalibrasi Peralatan Listrik</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Arus</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Clamp Meter</li>
										<li>Multimeter</li>
										<li>Power Meter</li>
										<li>AC/DC Ampere Meter</li>
										<li>AC/DC V-A Meter</li>
										<li>Relay Tester</li>
										<li>Power Analyzer</li>
										<li>Power Quality</li>
										<li>Tera Table</li>
										<li>CT TTR</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Tegangan</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Clamp Meter</li>
										<li>Multimeter</li>
										<li>Power Meter</li>
										<li>AC/DC Ampere Meter</li>
										<li>AC/DC V-A Meter</li>
										<li>Relay Tester</li>
										<li>AC/DC High Voltage Tester</li>
										<li>Power Analyzer</li>
										<li>Power Quality</li>
										<li>Tera Table</li>
										<li>PT TTR</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Watt</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Single Phase Watt Meter</li>
										<li>Three Phase Watt Meter</li>
										<li>Power Meter</li>
										<li>Power Quality</li>
										<li>Power Analyzer</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Energy Meter</div>
								<div class="description">KWh Meter, KVArh Meter</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Frequency</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Frequency Meter</li>
										<li>Power Meter</li>
										<li>Relay Tester</li>
										<li>Clamp Meter</li>
										<li>Multimeter</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Ohm</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Portable Wheatstone Bridge</li>
										<li>Resistor Standard</li>
										<li>Portable Double Bridge</li>
										<li>Precision Double Bridge</li>
										<li>Earth Tester</li>
										<li>Decade Resistor</li>
										<li>Insulation Tester</li>
										<li>Micro Ohm Meter</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Degree</div>
								<div class="description">Phase Meter</div>
							</div>
						</div>
					</div>
					<h4 class="orange text">Kalibrasi Non Listrik</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Temperature</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Glass Thermometer</li>
										<li>Temp. Calibrator</li>
										<li>Temp. Detector</li>
										<li>Temp. Recorded</li>
										<li>Temp.Control</li>
										<li>Thermacouple</li>
										<li>RTD</li>
										<li>Handy Calibrator</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Pressure</div>
								<div class="description">Vacuum Gauge, Pressure Gauge</div>
							</div>
						</div>
					</div>
					<h4 class="ui orange ribbon label">Dukungan Akreditasi</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Sertifikasi Akreditasi KAN</div>
								<div class="description">No. LK-007-IDN (SNI ISO/IEC 17025.2017)</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ui basic feature modal" id="modal-sert-operasi">
	<div class="content">
		<div class="ui centered cards">
			<div class="card" style="width: 100%;">
				<div class="image">
					<img src="{{asset('img/front/pengujian.png')}}">
				</div>
				<div class="content">
					<h3 class="ui red color header">Lingkup Sertifikasi Laik Operasi dan Komisioning</h3>
					<h4 class="red text">Sertifikasi Laik Operasi Instalasi Tenaga Listrik</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Pembangkit Tenaga Listrik</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>PLTU</li>
										<li>PLTGU</li>
										<li>PLTG</li>
										<li>PLTD/PLTMG</li>
										<li>PLTP</li>
										<li>PLTA</li>
										<li>PLTMH</li>
										<li>PLTS</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Transmisi Tenaga Listrik</div>
								<div class="description">Transmisi, Gardu Induk</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Distribusi Tenaga Listrik</div>
								<div class="description">Distribusi Tenaga Menengah, Distribusi Tenaga Rendah</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Instalasi Pemanfaatan Tenaga Listrik</div>
								<div class="description">Pemanfaatan/Pelanggan TT, Pemanfaatan/Pelanggan TM</div>
							</div>
						</div>
					</div>
					<h4 class="red text">Supervisi Komisioning Instalasi Tenaga Listrik</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Pembangkit Tenaga Listrik</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>PLTU</li>
										<li>PLTGU</li>
										<li>PLTG</li>
										<li>PLTD/PLTMG</li>
										<li>PLTP</li>
										<li>PLTA</li>
										<li>PLTMH</li>
										<li>PLTS</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Transmisi Tenaga Listrik</div>
								<div class="description">Transmisi, Gardu Induk</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Supervisi PT dan NDC Pembangkit</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>PLTU</li>
										<li>PLTGU</li>
										<li>PLTG</li>
										<li>PLTD/PLTMG</li>
										<li>PLTP</li>
										<li>PLTA</li>
										<li>PLTMH</li>
										<li>PLTS</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Instalasi FAT Peralatan Listrik</div>
								<div class="description">Pembangkit, Transmisi dan Distribusi Tenaga Listrik</div>
							</div>
						</div>
					</div>
					<h4 class="ui red ribbon label">Dukungan Akreditasi</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Sertifikasi Akreditasi KAN</div>
								<div class="description">No. LI-026-IDN (SNI ISO.IEC 17020:2012)</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Sertifikasi Akreditasi DJK</div>
								<div class="description">No. 5Stf/20/DJL.4/2016</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">SK Penunjukan DJK</div>
								<div class="description">No. 219/20/DJL.4/2016</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ui basic feature modal" id="modal-sert-produk">
	<div class="content">
		<div class="ui centered cards">
			<div class="card" style="width: 100%;">
				<div class="image">
					<img src="{{asset('img/front/kalibrasi.png')}}">
				</div>
				<div class="content">
					<h3 class="ui blue color header">Lingkup Sertifikasi Produk</h3>
					<h4 class="blue text">Sertifikasi Produk SNI</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Elektrik, Magnetik, Pengukuran Elektrik dan Magnetik</div>
								<div class="description">KWh Meter</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Kabel Elektrik</div>
								<div class="description">Kabel Listrik Berinsulasi PVC, Kabel Daya</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Aksesoris Listrik</div>
								<div class="description">Tusuk Kontak dan Kotak Kontak, Sakelar, Miniature Circuit Breaker (MCB), Pemutus Sirkuit Arus Sisa Tanpa Proteksi Arus Lebih Terpadu (RCCB)</div>
							</div>
						</div>
					</div>
					<h4 class="blue text">Sistem Pengawasan Mutu (SPM) untuk peralatan listrik yang digunakan oleh PLN</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Material Distribusi Utama</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Material Transmisi Utama</div>
							</div>
						</div>
					</div>
					<h4 class="ui blue ribbon label">Dukungan Akreditasi</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Sertifikasi Akreditasi KAN</div>
								<div class="description">No. LLSPr-005-IDN (SNI ISO.IEC 17065:2012)</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ui basic feature modal" id="modal-sert-manajemen">
	<div class="content">
		<div class="ui centered cards">
			<div class="card" style="width: 100%;">
				<div class="image">
					<img src="{{asset('img/front/pengujian.png')}}">
				</div>
				<div class="content">
					<h3 class="ui green color header">Lingkup Sertifikasi Sistem Manajemen</h3>
					<h4 class="green text">Sertifikasi Sistem Manajemen Mutu (Berbasis ISO 9001:2015)</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Peralatan Listrik dan Peralatan Optik</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Penyediaan Kelistrikan</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Teknologi Informasi</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Jasa Engineering</div>
							</div>
						</div>
					</div>
					<h4 class="green text">Sertifikasi Sistem Manajemen Lingkungan (Berbasis ISO 14001:2015)</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Peralatan Listrik dan Peralatan Optik</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Penyediaan Kelistrikan</div>
							</div>
						</div>
					</div>
					<h4 class="green text">Sertifikasi Sistem Manajemen K3 (Berbasis ISO 45001:2018)</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Peralatan Listrik dan Peralatan Optik</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Penyediaan Kelistrikan</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Teknologi Informasi</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Jasa Engineering</div>
							</div>
						</div>
					</div>
					<h4 class="green text">Audit Sistem Manajemen K3 (PP No. 50 tahun 2012)</h4>
					<h4 class="ui green ribbon label">Dukungan Akreditasi</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Sertifikasi Akreditasi KAN</div>
								<div class="description">No. LSSM-011-IDN dan LSSML-009-IDN (SNI ISO.IEC 17021-1:2015)</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">SK Dirjen Inwasnaker & K3</div>
								<div class="description">No. 278 tahun 2016</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ui basic feature modal" id="modal-tracking">
	<div class="content">
		<div class="ui centered cards">
			<div class="card" style="width: 100%;">
				<div class="content">
					<h3 class="ui green color header">Cari Data Tracking</h3>

					<div class="ui active inverted dimmer">
						<div class="ui text loader">Loading</div>
					</div>
					
					<form action="" class="ui form">
						<div class="field">
							<div class="ui action input">
								<input type="text" name="filter[display_name]" placeholder="No Order">
								<button type="button" class="ui teal icon filter button" data-content="Cari Data" onclick="openTrackingResult()">
									<i class="search icon"></i> Cari Data Order
								</button>
							</div>
						</div>
					</form>

					<table id="tracking" class="ui celled compact red table" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th class="ui center aligned">#</th>
								<th class="ui center aligned ten wide column">Layanan</th>
								{{-- <th class="ui center aligned">Jenis Alat</th>
								<th class="ui center aligned">Tipe Alat</th>
								<th class="ui center aligned">Merk</th> --}}
								<th class="ui center aligned" colspan="2">Status</th>
							</tr>
						</thead>
						<tbody>
							<tr class="center aligned">
								<td>1</td>
								<td class="left aligned">Pengujian KWh Meter</td>
								{{-- <td>Taping Connector</td>
								<td>Cable T-001</td>
								<td>Dzumba</td> --}}
								<td><a class="ui teal fluid label">Kaji Ulang</a></td>
								<td>01 Januari 2019</td>
							</tr>
							<tr class="center aligned">
								<td>2</td>
								<td class="left aligned">Pengujian Relay Arah</td>
								{{-- <td>PHB (Box MCB)</td>
								<td>MCB M-001</td>
								<td>MCB</td> --}}
								<td><a class="ui teal fluid label">Surat Penawaran</a></td>
								<td>02 Januari 2019</td>
							</tr>
							<tr class="center aligned">
								<td>3</td>
								<td class="left aligned">Kalibrasi Clamp Meter</td>
								{{-- <td>Elektrode Pembumian (Arde)</td>
								<td>Arde Arde-001</td>
								<td>Arde</td> --}}
								<td><a class="ui blue fluid label">Kalibrasi</a></td>
								<td>03 Januari 2019</td>
							</tr>
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection