<div class="ui vertical stripe segment" id="about">
	<div class="ui top aligned stackable grid container">
		<div class="row">
			<div class="eight wide right aligned column">
				<h3 class="ui header">{{trans('translator.Tentang Kami')}}</h3>
				<p style="text-align: justify;text-justify: inter-word;text-indent: 50px;">{{trans('translator.Informasi Tentang Kami')}}</p>
			</div>
			<div class="eight wide right floated left aligned column">
				<h3 class="ui header">{{trans('translator.Visi')}}</h3>
				<p style="text-align: justify;text-justify: inter-word;">{{trans('translator.Informasi Visi')}}</p>
				
				<div class="ui divider"></div>

				<h3 class="ui header">{{trans('translator.Misi')}}</h3>
				<ol>
					<li style="text-align: justify;text-justify: inter-word;">{{trans('translator.Informasi Misi Satu')}}</li>
					<li style="text-align: justify;text-justify: inter-word;">{{trans('translator.Informasi Misi Dua')}}</li>
					<li style="text-align: justify;text-justify: inter-word;">{{trans('translator.Informasi Misi Tiga')}}</li>
				</ol>
			</div>
		</div>
	</div>
</div>