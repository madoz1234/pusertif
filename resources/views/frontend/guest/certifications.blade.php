<div class="ui vertical stripe segment" id="e-slo">
	<div class="ui middle aligned stackable grid container">
		<div class="row">
			<div class="eight wide column">
				<div class="ui red segment">
					<h3 class="ui red color header">Lingkup Sertifikasi Laik Operasi dan Komisioning</h3>
					<h4 class="red text">Sertifikasi Laik Operasi Instalasi Tenaga Listrik</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Pembangkit Tenaga Listrik</div>
								<div class="description">PLTU, PLTGU, PLTG, PLTD/PLTMG, PLTP, PLTA, PLTMH, PLTS</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Transmisi Tenaga Listrik</div>
								<div class="description">Transmisi, Gardu Induk</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Distribusi Tenaga Listrik</div>
								<div class="description">Distribusi Tenaga Menengah, Distribusi Tenaga Rendah</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Instalasi Pemanfaatan Tenaga Listrik</div>
								<div class="description">Pemanfaatan/Pelanggan TT, Pemanfaatan/Pelanggan TM</div>
							</div>
						</div>
					</div>
					<h4 class="red text">Supervisi Komisioning Instalasi Tenaga Listrik</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Pembangkit Tenaga Listrik</div>
								<div class="description">PLTU, PLTGU, PLTG, PLTD/PLTMG, PLTP, PLTA, PLTMH, PLTS</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Transmisi Tenaga Listrik</div>
								<div class="description">Transmisi, Gardu Induk</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Supervisi PT dan NDC Pembangkit</div>
								<div class="description">PLTU, PLTGU, PLTG, PLTD/PLTMG, PLTP, PLTA, PLTMH, PLTS</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Instalasi FAT Peralatan Listrik</div>
								<div class="description">Pembangkit, Transmisi dan Distribusi Tenaga Listrik</div>
							</div>
						</div>
					</div>
					<h4 class="ui red ribbon label">Dukungan Akreditasi</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Sertifikasi Akreditasi KAN</div>
								<div class="description">No. LI-026-IDN (SNI ISO.IEC 17020:2012)</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Sertifikasi Akreditasi DJK</div>
								<div class="description">No. 5Stf/20/DJL.4/2016</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">SK Penunjukan DJK</div>
								<div class="description">No. 219/20/DJL.4/2016</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="eight wide column">
				<img src="{{asset('img/front/pengujian.png')}}" class="ui fluid image">
			</div>
		</div>
	</div>
</div>

<div class="ui vertical stripe segment" id="e-sertifikasi">
	<div class="ui middle aligned stackable grid container">
		<div class="row">
			<div class="eight wide column">
				<img src="{{asset('img/front/kalibrasi.png')}}" class="ui fluid image">
			</div>
			<div class="eight wide column">
				<div class="ui blue segment">
					<h3 class="ui blue color header">Lingkup Sertifikasi Produk</h3>
					<h4 class="blue text">Sertifikasi Produk SNI</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Elektrik, Magnetik, Pengukuran Elektrik dan Magnetik</div>
								<div class="description">KWh Meter</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Kabel Elektrik</div>
								<div class="description">Kabel Listrik Berinsulasi PVC, Kabel Daya</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Aksesoris Listrik</div>
								<div class="description">Tusuk Kontak dan Kotak Kontak, Sakelar, Miniature Circuit Breaker (MCB), Pemutus Sirkuit Arus Sisa Tanpa Proteksi Arus Lebih Terpadu (RCCB)</div>
							</div>
						</div>
					</div>
					<h4 class="blue text">Sistem Pengawasan Mutu (SPM) untuk peralatan listrik yang digunakan oleh PLN</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Material Distribusi Utama</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Material Transmisi Utama</div>
							</div>
						</div>
					</div>
					<h4 class="ui blue ribbon label">Dukungan Akreditasi</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Sertifikasi Akreditasi KAN</div>
								<div class="description">No. LLSPr-005-IDN (SNI ISO.IEC 17065:2012)</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ui vertical stripe segment" id="e-manajemen">
	<div class="ui middle aligned stackable grid container">
		<div class="row">
			<div class="eight wide column">
				<div class="ui green segment">
					<h3 class="ui green color header">Lingkup Sertifikasi Sistem Manajemen</h3>
					<h4 class="green text">Sertifikasi Sistem Manajemen Mutu (Berbasis ISO 9001:2015)</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Peralatan Listrik dan Peralatan Optik</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Penyediaan Kelistrikan</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Teknologi Informasi</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Jasa Engineering</div>
							</div>
						</div>
					</div>
					<h4 class="green text">Sertifikasi Sistem Manajemen Lingkungan (Berbasis ISO 14001:2015)</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Peralatan Listrik dan Peralatan Optik</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Penyediaan Kelistrikan</div>
							</div>
						</div>
					</div>
					<h4 class="green text">Sertifikasi Sistem Manajemen K3 (Berbasis ISO 45001:2018)</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Peralatan Listrik dan Peralatan Optik</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Penyediaan Kelistrikan</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Teknologi Informasi</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Jasa Engineering</div>
							</div>
						</div>
					</div>
					<h4 class="green text">Audit Sistem Manajemen K3 (PP No. 50 tahun 2012)</h4>
					<h4 class="ui green ribbon label">Dukungan Akreditasi</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Sertifikasi Akreditasi KAN</div>
								<div class="description">No. LSSM-011-IDN dan LSSML-009-IDN (SNI ISO.IEC 17021-1:2015)</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">SK Dirjen Inwasnaker & K3</div>
								<div class="description">No. 278 tahun 2016</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="eight wide column">
				<img src="{{asset('img/front/pic3.png')}}" class="ui fluid image">
			</div>
		</div>
	</div>
</div>