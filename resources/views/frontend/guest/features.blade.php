<div class="ui vertical stripe segment" id="e-slo">
	<div class="ui middle aligned stackable equal height grid container">
		<div class="five column row">

			<div class="center aligned feature column" onclick="$('#modal-uji').modal('show')">
				<div class="ui orange inverted center aligned segment">
					<i class="massive flask icon"></i>
				</div>
				<h3 class="ui no-margin header">Pengujian</h3>
				{{-- Deserunt inventore sapiente atque possimus. --}}
			</div>

			<div class="center aligned feature column" onclick="$('#modal-kal').modal('show')">
				<div class="ui orange inverted center aligned segment">
					<i class="massive clock icon"></i>
				</div>
				<h3 class="ui no-margin header">Kalibrasi</h3>
				{{-- Deserunt inventore sapiente atque possimus. --}}
			</div>

			<div class="center aligned feature column" onclick="$('#modal-sert-operasi').modal('show')">
				<div class="ui orange inverted center aligned segment">
					<i class="massive certificate icon"></i>
				</div>
				<h4 class="ui no-margin header">Sertifikasi<br>Laik Operasi dan Komisioning</h4>
				{{-- Deserunt inventore sapiente atque possimus. --}}
			</div>

			<div class="center aligned feature column" onclick="$('#modal-sert-produk').modal('show')">
				<div class="ui orange inverted center aligned segment">
					<i class="massive certificate icon"></i>
				</div>
				<h4 class="ui no-margin header">Sertifikasi<br>Laik Produk</h4>
				{{-- Deserunt inventore sapiente atque possimus. --}}
			</div>

			<div class="center aligned feature column" onclick="$('#modal-sert-manajemen').modal('show')">
				<div class="ui orange inverted center aligned segment">
					<i class="massive certificate icon"></i>
				</div>
				<h5 class="ui no-margin header">Sertifikasi<br>Laik Sistem Manajemen</h5>
				{{-- Deserunt inventore sapiente atque possimus. --}}
			</div>

		</div>
	</div>
</div>