<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tanggapan</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{$record->id}}">
		{!! csrf_field() !!}     
	      <div class="field">
	    	<label>Keterangan</label>
	        	<textarea name="keterangan" rows="2" placeholder="Keterangan"></textarea>
	      </div>
	      <div class="field">
	      	<div class="field">
	      		<label>Lampiran</label>
	      		<div class="field">
	      			<div class="ui action choises input">
	      				<input type="text" readonly placeholder="Format .doc, .xlsx, .pdf, .jpg, .jpeg, .png">
	      				<input type="file" name="data" style="display: none!important;" accept=".doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf,image/x-eps,image/jpeg,image/gif,image, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
	      				<div class="ui icon button">
	      					<i class="cloud upload alternate icon"></i>
	      				</div>
	      			</div>
	      		</div>
	      	</div>
	      </div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>