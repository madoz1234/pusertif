@extends('layouts.list-spm')

@section('filterdata')
    d.no_order = $("input[name='filter[no_order]']").val();
    d.tanggal_order = $("input[name='filter[tanggal_order]']").val();
    d.no_surat = $("input[name='filter[no_surat]']").val();
    d.jenis_pelayanan_id = $("select[name='filter[jenis_pelayanan_id]']").val();
    d.perusahaan_id = $("select[name='filter[perusahaan_id]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			title: ['empty'],
		};
	</script>
@endsection

@section('filters')
	<div class="field">
		<input name="filter[no_order]" placeholder="No Order" type="text">
	</div>
    <div class="field">
        <div class="ui left icon date input">
            <i class="calendar icon"></i>
            <input type="text" name="filter[tanggal_order]" placeholder="Tanggal Order">
        </div>
    </div>
    <div class="field">
      <input type="text" name="filter[no_surat]" placeholder="No Surat Permintaan">
    </div>
    <div class="field">
      <select name="filter[jenis_pelayanan_id]" class="watcher ui fluid search dropdown">
        {!! \App\Models\Master\JenisPelayanan::options('nama', 'id', [], 'Pilih Layanan') !!}
      </select>
    </div>
    <div class="field">
      <select name="filter[perusahaan_id]" class="watcher ui fluid search dropdown">
        {!!
          \App\Models\Master\Perusahaan::optionsa('nama', 'id', ['filters' => [function($q){
            return $q->where('status', 1);
          }],
          'selected' => old('id')
        ], 'Nama Perusahaan')
        !!}
      </select>
    </div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
	@php
		$user = auth()->user();
	@endphp
	@if($user->hasRole('user') || $user->hasRole('keuangan'))
	@else 
		<button type="button" class="ui blue add-page button" style="background-color: #363636;">
			<i class="plus icon"></i>
			Buat Order Baru
		</button>
	@endif
@endsection

@section('tables')
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="ui top demo tabular menu">
  <div class="active item" data-tab="first">On Progress</div>
  <div class="item" data-tab="second">Historis</div>
</div>
<div class="ui bottom demo active tab" data-tab="first">
	@if(isset($structs['listStruct']))
	<table id="listTable" class="ui celled compact red table display" width="100%" cellspacing="0">
		<thead>
			<tr>
				@foreach ($structs['listStruct'] as $struct)
				<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@yield('tableBody')
		</tbody>
	</table>
	@endif
</div>
<div class="ui bottom demo tab" data-tab="second">
	@if(isset($structs['listStruct2']))
	<table id="listTable2" class="ui celled compact red table display" width="100%" cellspacing="0">
		<thead>
			<tr>
				@foreach ($structs['listStruct2'] as $struct)
				<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@yield('tableBody')
		</tbody>
	</table>
	@endif
</div>
@endsection

@section('init-modal')
	<script>
		onShow = function(){
        	var nowDate = new Date();
        	var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), 0, 0, 0, 0);
        	var tanggal = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);

            $('#tgl').calendar({
        		ampm: false,
        		type: 'date',
        		endCalendar: tanggal,
        		formatter: {
        			date: function (date, settings) {
        				if (!date) return '';
        				let momentDate = moment(date)
        				return momentDate.format('YYYY-MM-DD')
        			}
        		}
        	});
            return false;
        };
	</script>
@endsection

@section('scripts')
<script>
    $('.date').calendar({
        type: 'date',
        formatter: {
          date: function (date, settings) {
            if (!date) return '';
            let momentDate = moment(date)
            return momentDate.format('DD/MM/YYYY')
          }
        }
    })
    var filterdata = function(d){
        d._token = "{{ csrf_token() }}";
        @yield('filterdata')
    }
    var dt1="";
    var dt2="";
    $(document).ready(function() {
        /*Start Of Online*/
        dt1 = $('#listTable').DataTable(
        {
        	dom: 'rt<"bottom"ip><"clear">',
	        destroy: true,
	        responsive: true,
	        autoWidth: false,
	        processing: true,
	        serverSide: true,
	        lengthChange: false,
	        pageLength: 10,
	        info:     true,
	        filter: false,
	        sorting: [],
        	language: {
        		url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        	},
        	ajax:  {
        		url: "{{ url($pageUrl.'grid') }}",
        		type: 'POST',
        		data: filterdata
        	},
        	columns: {!! json_encode($structs['listStruct']) !!},
        	drawCallback: function() {
        		var api = this.api();
            	api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i, x, y) {
                	cell.innerHTML = parseInt(cell.innerHTML)+i+1;
                	// cell.innerHTML = i+1;
            	});

        		$('[data-content]').popup({
        			hoverable: true,
        			position : 'top center',
        			delay: {
        				show: 300,
        				hide: 800
        			}
        		});
        	}
        }
        );

        // $('#btn-filter').on('click', function(e) {
        //     dt.draw();
        //     e.preventDefault();
        // });

        // $('.text-filter').on('keydown', function(e) {
        //     if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
        //         dt.draw();
        //         e.preventDefault();
        //     }
        // })
        /*End Of Online*/

        /*Start Of Histori*/
        dt2 = $('#listTable2').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				lengthChange: false,
				pageLength: 10,
				filter: false,
				sorting: [],
                language: {
					url: "{{ asset('plugins/datatables/Indonesian.json') }}"
				},
                ajax:  {
                    url: "{{ url($pageUrl.'histori') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct2']) !!},
                drawCallback: function() {
					var api = this.api();
					api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
						start = cell.innerHTML;
						cell.innerHTML = (parseInt(start) + (i+1));
					});

				$('[data-content]').popup({
					hoverable: true,
					position : 'top center',
					delay: {
						show: 300,
						hide: 800
					}
				});
				}
            }
        );

        dt2.on('draw.dt', function () {
            dt2.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
        }).draw();

        $('.filter.button').on('click', function(e) {
            dt2.draw();
            dt1.draw();
            // dt2.ajax.reload();
            e.preventDefault();
        });

        $('.reset.button').on('click', function(e) {
            $('.dropdown .delete').trigger('click');
            setTimeout(function(){
                dt2.draw();
                dt1.draw();
            }, 100);
            $('.dropdown').dropdown('clear');
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt2.draw();
                dt1.draw();
                e.preventDefault();
            }
        })
        /*End Of Histori*/
    });
</script>
@endsection
