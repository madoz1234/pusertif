@extends('layouts.form-user')
@section('styles')
<style type="text/css">
.responsive.table{
	width: 100%;
	overflow-x: auto;
}
.jus{
	text-align: justify;
	text-justify: inter-word;
}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		tipe: ['empty'],
	};
</script>
@endsection

@section('content-body')
<div class="ui warning pendaftaran hidden message">
	Layanan dan Lingkup harus diisi untuk menambahkan Item Uji.
</div>
<form class="ui form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
	<div class="ui very basic thin segment">
		<table class="tabel" style="margin: 0;border: none;box-sizing: inherit;">
			<tr>
				<td width="150"><b>Layanan</b></td>
				<td width="10">:</td>
				<td class="field">
					<select name="jenis_pelayanan_id" class="watcher ui fluid search pilihan dropdown">
						{!!
							\App\Models\Master\JenisPelayanan::options('nama', 'id', [
								'filters' => [
								],
								'selected' => ($record->layanan_id ? $record->layanan_id : null)
							])
							!!}
						</select>
					</td>
				</tr>
				<tr>
					<td width="150"><b>Lingkup</b></td>
					<td width="10">:</td>
					<input type="hidden" name="data_lingkup" value="{{$record->lingkup_id}}">
					<td class="field">
						<select name="lingkup_id" class="watcher ui fluid search pilihan dropdown">
							<option value="">Pilih Salah Satu</option>
						</select>
					</td>
				</tr>
				<tr>
					<td width="150"><b>Kelas</b></td>
					<td width="10">:</td>
					<td class="field">
						<select name="kelas" class="watcher ui fluid search pilihan dropdown">
							<option value="">Pilih Salah Satu</option>
							<option value="1" @if($record->kelas == '1') selected @endif>Reguler</option>
							<option value="2" @if($record->kelas == '2') selected @endif>Prioritas</option>
						</select>
					</td>
					<td>
						<button type="button" style="background-color: #da251c;" class="ui blue mini icon append lihat button @if($record->kelas == '1') disabled @endif" data-tooltip="Lihat Jadwal" data-position="left center"><i class="eye icon"></i></button>
					</td>
					<td width="150" style="padding-left: 23px;"><b>Rencana Pelaksanaan</b></td>
					<td width="10">:</td>
					<td class="field">
						<div class="field">
							<div class="ui calendar labeled input periode">
								<div class="ui input left icon">
									<i class="calendar icon date"></i>
									<input name="rencana" type="text" placeholder="Rencana Pelaksanaan" value="{{$record->rencana}}">
								</div>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td width="250"><b>Surat Resmi Manajemen Perusahaan</b></td>
					<td width="10">:</td>
					<td class="lefted">
						<div class="field">
							<div class="ui action choises input" style="width: 85%;">
								<input type="text"  readonly value="{{$record->filename}}">
								<input type="file" name="bukti" style="display: none!important;">
								<div class="ui icon button" data-tooltip="Upload" data-position="top center">
									<i class="cloud upload alternate icon"></i>
								</div>
							</div>
							<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
								<a href="{{ url($pageUrl.$record->id,'download') }}">
									<i class="download icon"></i></a>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td width="150"><b>Item Uji</b></td>
					<td width="10">:</td>
					<td class="field">
						@if($record->detail->count()>0)
							<button type="button" class="ui blue buat button" style="background-color: #363636;">
								<i class="plus icon"></i>
								Tambah Data
							</button>
						@else
							<button type="button" class="ui blue buat button" style="background-color: #363636;">
								<i class="plus icon"></i>
								Buat Baru
							</button>
						@endif
					</td>
				</tr>
			</table>
		</div>
		<div class="ui very basic thin segment">
			<table id="detail" class="ui compact celled table" style="border-radius: 0; margin: 5px 0;">
				<thead>
					<tr class="middle aligned">
						<th width="30px" class="center aligned">No</th>
						<th width="300px" class="center aligned">Jenis Pengujian</th>
						<th width="300px"class="center aligned">Merk</th>
						<th width="150px" class="center aligned">Tipe</th>
						<th width="100px" class="center aligned">Jumlah</th>
						<th width="100px" class="center aligned">Satuan</th>
						<th width="200px" class="center aligned">Spesifikasi</th>
						<th width="80px" class="center aligned">Aksi</th>
					</tr>
				</thead>
				<tbody class="detail fluid container">
					@foreach($record->detail as $key => $data)
						<tr class="list-row-{{$data->id}}" id="data-row" data-id="{{$data->id}}">
							<td class="center aligned numbor">{{$data->id}}</td>
							<input type="hidden" name="exists[]" value="{{$data->id}}">
							<td class="pjg">
								<div class="ui fluid input">
									<span>{{ $data->jenis->nama }}</span>
									<input type="hidden" name="detail[{{$data->id}}][jenis_id]" value="{{$data->jenis_id}}">
								</div>
							</td>
							<td>
								<div class="ui fluid input">
									<span class="sedit-label lrow-{{$data->id}}">{{$data->merk}}</span>
									<input type="text" class="sedit-input hidden inrow-{{$data->id}}" name="detail[{{$data->id}}][merk]" value="{{$data->merk}}">
								</div>
							</td>
							<td>
								<div class="ui fluid input">
									<span class="sedit-label lrow-{{$data->id}}">{{$data->tipe}}</span>
									<input type="text" class="sedit-input hidden inrow-{{$data->id}}" name="detail[{{$data->id}}][tipe]" value="{{$data->tipe}}">
								</div>
							</td>
							<td>
								<div class="ui fluid input">
									<span class="sedit-label lrow-{{$data->id}}">{{$data->jumlah}}</span>
									<input type="text" class="sedit-input hidden inrow-{{$data->id}} number" name="detail[{{$data->id}}][jumlah]" value="{{$data->jumlah}}">
								</div>
							</td>
							<td>
								<div class="ui fluid input">
									<span>{{$data->satuan->satuan}}</span>
									<input type="hidden" name="detail[{{$data->id}}][satuan_id]" value="{{$data->satuan->id}}">
								</div>
							</td>
							<td>
								<div class="ui fluid input">
									<span class="sedit-label lrow-{{$data->id}}">{{$data->spesifikasi}}</span>
									<input type="text" class="sedit-input hidden inrow-{{$data->id}}" name="detail[{{$data->id}}][spesifikasi]" value="{{$data->spesifikasi}}">
								</div>
							</td>
							<td colspan="2" class="center aligned deletor kcl">
								<button class="ui green mini icon btn-save button hidden" data-id="{{$data->id}}" data-tooltip="Simpan Item Uji" data-position="left center"><i class="save icon"></i></button>
								<button class="ui orange mini icon btn-edit button" data-id="{{$data->id}}" data-tooltip="Ubah Item Uji" data-position="left center"><i class="pencil icon"></i></button>
								<button class="ui red mini icon hapus_data button" data-id="{{$data->id}}" data-tooltip="Hapus Item Uji" data-position="left center"><i class="remove icon"></i></button>
							</td>
						</tr>			
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="ui bottom attached segment">
			<div class="actions">
				<div class="ui two column grid">
					<div class="left aligned column">
						<div class="ui gray deny labeled icon button" onclick="window.history.back()">
							<i class="chevron left icon"></i>
							Kembali
						</div>
					</div>
					<div class="right aligned column">		
						<button type="button" class="ui positive right labeled icon save as page button">
							Simpan
							<i class="save icon"></i>
						</button>
					</div>
				</div>	
			</div>
		</div>
	</form>
	@endsection
	@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			$(document).on('click', '.lihat', function(e){
				event.preventDefault();
				// /* Act on the event */
				loadModalJadwal({
					'url' : '{{ url($pageUrl) }}/lihat-jadwal',
					'modal' : '.{{ $modalSize }}.modal',
					'formId' : '#dataForm',
					'onShow' : function(){ 
						onShow();
					},
				})
			});

			$('select[name=kelas]').on('change', function(e){
				if(this.value == 2){
					$('.lihat').removeClass('disabled');
				}else{
					$('.lihat').addClass('disabled');
				}
			})

			$('.periode').calendar({
				type: 'month',
				// maxDate: today,
				formatter: {
					date: function (date, settings) {
						if (!date) return '';
						var day = date.getDate();
						var month = date.getMonth() + 1;
						var year = date.getFullYear();
						var month = month < 10 ? '0' + month : '' + month;
						return year+'-'+month;
					}
				}
			});

			$('.ui.pilihan').css({
				'width': '400px'
			})

			$.ajax({
				url: '{{ url('ajax/option/lingkup') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					jenis_pelayanan_id: $('select[name=jenis_pelayanan_id]').val(),
					lingkup_id: $('input[name=data_lingkup]').val()
				},
			})
			.done(function(response) {
				$('select[name=lingkup_id]').html(response)
			});

			$(document).on('click', '.ui.tambah.pengujian', function(event) {
				$('#Pengujian').modal({
					onShow: function(){
						$.ajax({
							url: '{{ url('ajax/option/jenis') }}',
							type: 'POST',
							data: {
								_token: "{{ csrf_token() }}",
								jenis_pelayanan_id: $('select[name=jenis_pelayanan_id]').val(),
								lingkup_id: $('select[name=lingkup_id]').val(),
							},
						})
						.done(function(response) {
							$('select[name=jenis_id]').html(response)
						})
						.fail(function() {
							console.log("error");
						});
					},
				}).modal('show');
			});
		});

		$.ajax({
			url: '{{ url('ajax/option/lingkup') }}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				jenis_pelayanan_id: $('select[name=jenis_pelayanan_id]').val()
			},
		})
		.done(function(response) {
			$('select[name=lingkup_id]').html(response)
		});

		$('select[name=jenis_pelayanan_id]').on('change', function(){
			$.ajax({
				url: '{{ url('ajax/option/lingkup') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					jenis_pelayanan_id: this.value
				},
			})
			.done(function(response) {
				$('select[name=lingkup_id]').html(response)
			})
			.fail(function() {
				console.log("error");
			});
		})

		$(document).on('click', '.hapus_data', function (e){
			var row = $(this).closest('tr');
			row.remove();
		});

		$(document).on('click', '.btn-edit', function (e){
			var id = $(this).data('id');

			$('.lrow-'+id).each(function() {
			  $( this ).addClass('hidden');
			});

			$('.inrow-'+id).each(function() {
			  $( this ).removeClass('hidden');
			});

			$('.btn-edit').each(function() {
			  if($( this ).data('id') == id){
				$(this).addClass('hidden');
			  }
			});

			$('.btn-save').each(function() {
			  if($( this ).data('id') == id){
				$(this).removeClass('hidden');
			  }
			});
			$('.save.as.page').addClass('disabled');
		});

		$(document).on('click', '.btn-save', function (e){
			var id = $(this).data('id');

			$('.inrow-'+id).each(function() {
			  $( this ).addClass('hidden');
			  $(this).parent().find('.lrow-'+id).html($(this).val());
			});

			$('.lrow-'+id).each(function() {
			  $( this ).removeClass('hidden');
			});

			$('.btn-edit').each(function() {
			  if($( this ).data('id') == id){
				$(this).removeClass('hidden');
			  }
			});

			$('.btn-save').each(function() {
			  if($( this ).data('id') == id){
				$(this).addClass('hidden');
			  }
			});

			$('.save.as.page').removeClass('disabled');
		});
	</script>
	@append