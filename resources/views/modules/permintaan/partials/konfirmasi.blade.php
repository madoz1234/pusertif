@extends('layouts.form-user')
@section('styles')
<style type="text/css">
.responsive.table{
	width: 100%;
	overflow-x: auto;
}
.jus{
	text-align: justify;
	text-justify: inter-word;
}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		tipe: ['empty'],
	};
</script>
@endsection

@section('content-body')
<form class="ui form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
	<div class="ui very basic thin segment">
		<div class="ui very basic thin segment">
			<table class="ui compact celled table" style="border-radius: 0; margin: 0;margin-bottom: -6px;">
				<tr style="background-color:#ffffff;">
					<th colspan="4">{{trans('translator.No Pesanan')}} : {{$record->no_order}}</th>
				</tr>
			</table>
			<table id="detail" class="ui compact celled table" style="border-radius: 0; margin: 5px 0;">
				<tr class="top aligned">
					<td width="50%">
						<div>
							<div>
								<div class="item"><i>{{trans('translator.Jumlah Pembayaran')}}</i></div>
								<div class="item"><i><h3>Rp. 200.500.500</h3></i></div>
								<div class="item"><i>{{trans('translator.Lakukan pembayaran sesuai jumlah diatas, hingga tiga digit terakhir')}}</i></div>
							</div>
						</div>
					</td>
					<td width="50%">
						<div>
							<div class="item"><i>{{trans('translator.Batas Waktu Pembayaran')}}</i></div>
							<div class="item"><i><a class="ui huge red label" id="demo"></a></i></div>
							<div class="item"><i>{{trans('translator.Sampai Hari')}} : <h3 style="color:red;">Jumat 5 April 2019 23:59:59</h3></i></div>
							<div class="item"><i>{{trans('translator.Upload Bukti Pembayaran')}}</div>
							<div class="item"><i>
								<div class="field cek">
									<div class="ui action choises input" style="width: 250px;">
										<input type="text"  readonly>
										<input type="file" name="bukti" style="display: none!important;">
										<div class="ui icon button" data-tooltip="Upload" data-position="top center">
											<i class="cloud upload alternate icon"></i>
										</div>
									</div>
									<button type="button" class="ui green small icon button" data-tooltip="{{trans('translator.Simpan')}}" data-position="top center" style="padding-top: 11px;padding-bottom: 11px;padding-right: 11px;padding-left: 11px;"><i class="save icon"></i></button>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="ui very basic thin segment">
			<table class="ui compact celled table" style="border-radius: 0; margin: 0;margin-bottom: -6px;">
				<tr style="background-color:#ffffff;">
					<th colspan="4">{{trans('translator.Cara Pembayaran')}}</th>
				</tr>
			</table>
			<table id="detail" class="ui compact celled table" style="border-radius: 0; margin: 5px 0;">
				<tr class="top aligned">
					<td>
						<div>
							<div width="50px">
								<div class="item"><i>{{trans('translator.Cara Pembayaran')}}</i></div>
								<div class="item"><i>1. {{trans('translator.Transfer dapat melalui ATM, SMS / M-Banking, dan E-Banking')}}.</i></div>
								<div class="item"><i>2. {{trans('translator.Masukkan Nomor Rekening PLN Pusertif. Bank Mandiri')}} 123 0090 111 909</i></div>
								<div class="item"><i>3. {{trans('translator.Masukkan jumlah bayar tepat hingga 3 digit, yaitu')}} Rp 200.500.500</i></div>
								<div class="item"><i>4. {{trans('translator.Simpan dan upload bukti transfer yang Anda dapatkan')}}.</i></div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="ui very basic thin segment">
			<table class="ui compact celled table" style="border-radius: 0; margin: 0;margin-bottom: -2px;">
				<tr style="background-color:#ffffff;">
					<th colspan="4">{{trans('translator.Detail Pesanan')}}</th>
				</tr>
			</table>
			<table class="ui compact table" style="border-radius: 0; margin: 0;margin-bottom: -6px;">
				<tr>
					<td width="150px;"><b>{{trans('translator.Layanan')}}</b></td>
					<td width="10px;">:</td>
					<td>
						{{$record->pelayanan->nama}}
					</td>
				</tr>
				<tr>
					<td width="150px;"><b>{{trans('translator.Lingkup')}}</b></td>
					<td width="5px;">:</td>
					<td>
						{{$record->lingkup->nama}}
					</td>
				</tr>
			</table>
			<table id="detail" class="ui compact celled table" style="border-radius: 0; margin: 5px 0;">
				<thead>
					<tr class="middle aligned">
						<th width="30px" class="center aligned">No</th>
						<th width="300px" class="center aligned">{{trans('translator.Jenis Pengujian')}}</th>
						<th width="300px"class="center aligned">{{trans('translator.Merk')}}</th>
						<th width="150px" class="center aligned">{{trans('translator.Tipe')}}</th>
						<th width="100px" class="center aligned">{{trans('translator.Jumlah')}}</th>
						<th width="100px" class="center aligned">{{trans('translator.Satuan')}}</th>
						<th width="200px" class="center aligned">{{trans('translator.Spesifikasi')}}</th>
						<th width="200px" class="center aligned">{{trans('translator.Nilai')}}</th>
					</tr>
				</thead>
				<tbody class="detail fluid container">
					@foreach($record->detail as $key => $data)
						<tr class="list-row-{{$data->id}}" id="data-row" data-id="{{$data->id}}">
							<td class="center aligned numbor">{{$key+1}}</td>
							<td class="pjg">
								<span>{{ $data->jenis->nama }}</span>
							</td>
							<td>
								<span class="sedit-label lrow-{{$data->id}}">{{$data->merk}}</span>
							</td>
							<td>
								<span class="sedit-label lrow-{{$data->id}}">{{$data->tipe}}</span>
							</td>
							<td>
								<span class="sedit-label lrow-{{$data->id}}">{{$data->jumlah}}</span>
							</td>
							<td>
								<span class="sedit-label lrow-{{$data->id}}">{{$data->satuan->satuan}}</span>
							</td>
							<td>
								<span class="sedit-label lrow-{{$data->id}}">{{$data->spesifikasi}}</span>
							</td>
							<td style="text-align: right;">
								<span class="sedit-label lrow-{{$data->id}}">Rp.2000000</span>
							</td>
						</tr>			
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<th colspan="7" class="right aligned" width="40px">Total</th>
						<th class="right aligned" width="40px">Rp.2000000</th>
					</tr>		
				</tfoot>
			</table>
		</div>
	</form>
	@endsection
	@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			$('.ui.pilihan').css({
				'width': '400px'
			})

			var countDownDate = new Date("Apr 5, 2019 23:59:59").getTime();
			// Update the count down every 1 second
			var x = setInterval(function() {
			// Get todays date and time
			var now = new Date().getTime();
			// Find the distance between now and the count down date
			var distance = countDownDate - now;
			// Time calculations for days, hours, minutes and seconds
			var days = Math.floor(distance / (1000 * 60 * 60 * 24));
			var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((distance % (1000 * 60)) / 1000);
			// Output the result in an element with id="demo"
			document.getElementById("demo").innerHTML = days + " {{trans('translator.Hari')}}&nbsp;&nbsp;" + hours + " {{trans('translator.Jam')}}&nbsp;&nbsp;"
			+ minutes + " {{trans('translator.Menit')}}&nbsp;&nbsp;" + seconds + " {{trans('translator.Detik')}}&nbsp;&nbsp;";
			// If the count down is over, write some text 
			if (distance < 0) {
			clearInterval(x);
			document.getElementById("demo").innerHTML = "EXPIRED";
			$('.cek').addClass('disabled');
			}
			}, 1000);
		});
	</script>
	@append