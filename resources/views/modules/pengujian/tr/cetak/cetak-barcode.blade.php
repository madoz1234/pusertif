<html>
<head>
<style>
        @page {
            margin: 60px 25px;
            counter-reset: page;
        }

       /* Define now the real margins of every page in the PDF */
       body {
           margin-top: 3cm;
           margin-left: 0cm;
           margin-right: 0cm;
           margin-bottom: 2cm;
           font-style: normal;
       }

       .ui.table.bordered {
         border: solid black 1px;
         border-collapse: collapse;
         width: 100%;
         font-family : "Arial", "Verdana", sans-serif;
       }

       .ui.table.bordered td {
         border: solid black .5px;
         border-collapse: collapse;
         /*padding:10px;*/
         padding:5px;
       }

       .ui.table.bordered td.center.aligned {
         text-align : center;
       }

       .page-break {
            page-break-after: always;
       }

       .pad{
       	padding-top: 5px;
       	padding-bottom: 5px;
       }

       /* Define the header rules */
        header {
            position: fixed;
            top: -40px;
            left: 0px;
            right: 0px;
            height: 10px;

            /* Extra personal styles */
            text-align: center;
            /*line-height: 35px;*/

            font-size: 10px;
        }
		
       main {
         position: sticky;
         font-size : 12px;
         padding-top : -1cm;
         margin-top : 2px;
         border : none;
         width: 100%;
       }

       main p {
         margin-left: 5px;
         margin-right: 5px;
       }

       /* Define the footer rules */
       footer {
           position: fixed;
           bottom: -60px;
           left: 0px;
           right: 0px;
           height: 50px;

           /* Extra personal styles */
           text-align: center;
           line-height: 35px;
       }
/*       .page-num:after { 
       	counter-increment: pages; 
       	content: counter(page) " of " counter(pages); 
       }*/
       table.page_content {width: 100%; border: none; padding: 1mm; font-size: 10px;font-family : "Arial", "Verdana", sans-serif;}
       table.page_header {width: 100%; border: none; padding: 2mm; font-size: 10px;font-family : "Arial", "Verdana", sans-serif; text-align: left; margin-left: 50px;margin-top: 50px;}
       table.page_content_header {width: 100%; font-size: 10px;border-collapse: collapse;font-family : "Arial", "Verdana", sans-serif;text-align: center; padding-bottom: 1px;
       	padding-top: 1px;}
       /*.footer .page-number:after {
         content: counter(page);
       }*/
</style>
</head>
<body>
    <!-- Define header and footer blocks before your content -->
	{{-- <script type="text/php">
	    if (isset($pdf)) {
	        $x = 532;
	        $y = 85;
	        $text = "Halaman {PAGE_NUM} dari {PAGE_COUNT}";
	        $font = null;
	        $size = 7;
	        $color = array(0,0,0);
	        $word_space = 0.0;  //  default
	        $char_space = 0.0;  //  default
	        $angle = 0.0;   //  default
	        $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
	    }
	</script> --}}
	 <header>
    	<table class="ui table bordered">
    		<tr>
    			<td class="center aligned" style="width: 79px;"><img src="{{ asset('img/icon.png') }}" width="75px" height="100px" style="display: block;"></td>
    			<td style="width: 161px;"><b style="font-size: 16px;">PT PLN (PERSERO)</b>
						<br><b style="font-size: 16px;">PUSAT SERTIFIKASI</b></td>
				<td class="center aligned" style="width: 99px;border-left: none;" colspan="3"><img src="{{ asset('img/lmk.png') }}" width="250px" height="90px" style="display: block;"></td>
    		</tr>
      	</table>
  	</header>
  	<br>
	<main>
		@php
			$barcode = $records->no_laporan.'-'.$records->tgl_laporan;
		@endphp
		<table style="padding-left: 270px;">
			<tbody>
				<tr>
					<td>No Laporan</td>
					<td>:</td>
					<td>{{ $records->no_laporan }}</td>
				</tr>
				<tr>
					<td>Tanggal Laporan</td>
					<td>:</td>
					<td>{{ $records->tgl_laporan }}</td>
				</tr>
			</tbody>
		</table>
		<table style="padding-left: 270px;">
			<tbody>
				<tr>
					<td colspan="3" style="border:3px solid black;"><img style="display: block;" src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(250)->generate($barcode)) }} ">
					<label style="font-size: 10px;">PT. PLN PERSERO</label>
					</td>
				</tr>
			</tbody>
		</table>
	</main>
</body>
</html>