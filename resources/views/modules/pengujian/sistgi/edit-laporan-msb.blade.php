@extends('layouts.form')

@section('styles')
<style type="text/css">
	.responsive.table{
		width: 100%;
		overflow-x: auto;
	}
	.jus{
		text-align: justify;
		text-justify: inter-word;
	}
	.fn{
		font-size: 12px;
	}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		judul: ['empty'],
	};
</script>
@endsection

@section('content-body')
<form class="ui data form" id="data" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
	{!! csrf_field() !!}
	<div style="overflow-x:auto;">
		<table id="listTable" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
			<thead>
				<tr class="middle aligned">
					<th width="10" class="center aligned fn">#</th>
					<th width="200" class="center aligned fn">Jenis Pengujian</th>
					<th width="250" class="center aligned fn">Nama Barang</th>
					<th width="150" class="center aligned fn">Merk</th>
					<th width="150" class="center aligned fn">No Seri</th>
					<th width="150" class="center aligned fn">Tgl Penerimaan Barang Uji</th>
					<th width="150" class="center aligned fn">Hasil Verifikasi</th>
					<th width="150" class="center aligned fn">Tgl Verifikasi Barang Uji</th>
					<th width="250" class="center aligned fn">Catatan</th>
					<th width="250" class="center aligned fn">No FPP/FPK</th>
					<th width="200" class="center aligned fn">Tgl Selesai Pengujian</th>
					<th width="200" class="center aligned fn">Hasil Pengujian</th>
					<th width="300" class="center aligned fn">Pembuat Laporan</th>
					<th width="200" class="center aligned fn">Barang Uji Kembali</th>
					<th width="300" class="center aligned fn">Laporan Pengujian </br><button type="button" class="ui mini button checked-all"><i class="checkmark icon"></i> Checklist All</button></th>
				</tr>
			</thead>
			<tbody>
				@php 
					$i=1;
					$angka =0;
					$jumlah=0;
				@endphp
				@foreach($record->detail_penerimaan_barang->where('flag', 0)->groupBy('group_id') as $key => $row)
					@foreach($row as $kuy => $data)
						@if($kuy == 0)
							<tr>
								<td rowspan="{{$row->count()}}">{{$i}}</td>
								<td rowspan="{{$row->count()}}">{{$data->detail_pp->jenis->nama}}</td>
								<td>{{ $data->nama_barang }} [{{$data->urutan}}/{{$data->max}}]</td>
								<td rowspan="{{$row->count()}}">{{ $data->detail_pp->merk }}</td>
								<td>{{$data->no_seri}}</td>
								<td class="center aligned" rowspan="{{$row->count()}}">{{ DateToStringYear($data->tanggal_terima) }}</td>
								<td class="center aligned" rowspan="{{$row->count()}}">
									@if($data->detail_pengujian->verifikasi == 1)
										<a class="ui tag label fn" style="background-color:#00abffcc;">DITERIMA</a>
									@else
										<a class="ui tag label fn" style="background-color:#dd0000cc;">DITOLAK</a>
									@endif
								</td>
								<td class="center aligned" rowspan="{{$row->count()}}">
									@if($data->detail_pengujian)
										@if($data->detail_pengujian->tgl_selesai)
											{{ DateToStringYear(Carbon\Carbon::parse($data->detail_pengujian->tgl_selesai)->format('Y-m-d')) }}
										@else 
											-
										@endif
									@else
										-
									@endif
								</td>
								<td style="text-align:justify;text-justify:auto;">{!! readMoreText($data->catatan, 50) !!}</td>
								<td rowspan="{{$row->count()}}">
									{{ $data->detail_pengujian->fpp_fpk }}
								</td>
								<td class="center aligned" rowspan="{{$row->count()}}">
									@if($data->detail_pengujian->detailpelaksana)
										@if($data->detail_pengujian->detailpelaksana->tgl_selesai)
											{{ DateToStringYear(Carbon\Carbon::parse($data->detail_pengujian->detailpelaksana->tgl_selesai)->format('Y-m-d')) }}
										@else 
											-
										@endif
									@else
										-
									@endif
								</td>
								<td class="center aligned" rowspan="{{$row->count()}}">
									@if($data->detail_pengujian->detailpelaksana)
										@if($data->detail_pengujian->detailpelaksana->status_hasil_pengujian == 1)
											<a class="ui tag label fn" style="background-color:#00FF00;">BERHASIL</a>
										@elseif($data->detail_pengujian->detailpelaksana->status_hasil_pengujian == 2)
											<a class="ui tag label fn" style="background-color:#dd0000cc;">GAGAL</a>
										@else
											<div class="ui label">
												<i class="hourglass end icon"></i>Menunggu Hasil Pengujian
											</div>
										@endif
									@else 
										-
									@endif
								</td>
								<td rowspan="{{$row->count()}}">
									@if($data->detail_pengujian->detailpelaksana->status == 0)
										<div class="ui label">
											<i class="hourglass end icon"></i>Menunggu Verifikasi Hasil Pengujian
										</div>
									@else
										@if($data->detail_pengujian->detailpelaksana->pelaksana_laporan_id !== 1)
											{{ $data->detail_pengujian->detailpelaksana->pelaksanalaporan->nama }}
										@else
											<div class="ui label">
												<i class="hourglass end icon"></i>Menunggu Verifikasi Asman
											</div>
										@endif
									@endif
								</td>
								<td rowspan="{{$row->count()}}">
									@if($data->detail_pengujian->detailpelaksana->status_barang == 0)
										<div class="ui label">
											<i class="close icon"></i> Barang tidak dikembalikan
										</div>
									@else
										<div class="ui label">
											<i class="check icon"></i> Barang dikembalikan
										</div>
									@endif
								</td>
								<td rowspan="{{$row->count()}}" class="center aligned">
									@if($data->detail_pengujian->detailpelaksana->status == 0)
										<div class="ui label">
											<i class="hourglass end icon"></i>Menunggu Verifikasi Hasil Pengujian
										</div>
									@else
										@if($data->detail_pengujian->detailpelaksana->status_file == 1)
											@if($data->detail_pengujian->detailpelaksana->status_data == 2)
												@php
												$jumlah++;
												@endphp
												<div class="ui fitted checkbox">
													<input name="checklist[]" class="check" type="checkbox" data-id="{{ $data->detail_pengujian->detailpelaksana->id }}">
													<label></label>
												</div>
											@elseif($data->detail_pengujian->detailpelaksana->status_data == 3)
												<div class="ui green label">
													<i class="check icon"></i> Sudah Disetujui
												</div>
											@else 
												<div class="ui label">
													<i class="hourglass end icon"></i>Menunggu Laporan Asmen
												</div>
											@endif
										@else
											<div class="ui label">
												<i class="hourglass end icon"></i>Menunggu Laporan Pengujian
											</div>
										@endif
									@endif
								</td>
							</tr>
						@else
							<tr>
								<td style="border-left: 1px solid #e8e9e9">{{ $data->nama_barang }} [{{$data->urutan}}/{{$data->max}}]</td>
								<td>{{$data->no_seri}}</td>
								<td style="text-align:justify;text-justify:auto;">{!! readMoreText($data->catatan, 50) !!}</td>
							</tr>
						@endif
					@endforeach
					@php
						$i++;
					@endphp
				@endforeach
			</tbody>
		</table>
	</div>
</form>
<div class="actions menu top" style="margin-top: 12px;">
	<div class="ui two column grid">
		<div class="left aligned column">
			<div class="ui gray deny labeled icon button next" data-tab="first" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		@if($jumlah > 0)
			<div class="right aligned column">
				<div class="ui green labeled icon button kirim-data">
					Simpan
					<i class="save icon"></i>
				</div>
			</div>
		@endif
	</div>		
</div>	
@endsection
@section('scripts')
<script type="text/javascript">
	$('.tgl').calendar({
		ampm: false,
		type: 'date',
			// maxDate: tanggal,
			formatter: {
				date: function (date, settings) {
					if (!date) return '';
					let momentDate = moment(date)
					return momentDate.format('YYYY-MM-DD')
				}
			}
		});

	$(".number").on("keypress keyup blur",function (e) {    
		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	});

	$(document).on('click', '.verifikasi', function(event) {
		event.preventDefault();
		var id = $(this).data('id');
		var tipe = $(this).data('tipe');
		var url = "{{ url($pageUrl) }}/"+id+"/verifikasi-laporan-pengujian/"+tipe+"/2";
		swal({
			title: 'Apakah Anda yakin?',
			text: "Data yang sudah disetujui, tidak dapat diubah!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya',
			cancelButtonText: 'Batal',
			reverseButtons: true
		}).then((result) => {
			if (result) {
				$.ajax({
					url: url,
					type: 'GET',
					success: function(resp){
						swal(
							'Berhasil!',
							'Data berhasil disetujui.',
							'success'
							).then(function(e){
								window.location.reload();
							});
						},
						error : function(resp){
							swal(
								'Gagal!',
								'Data gagal disetujui.',
								'error'
								).then(function(e){
									window.location.reload();
								});
							}
						});

			}
		})
	});


	$(document).on('click', '.checked-all', function(e){
		var checked		= true;

		$('.check').each(function(e){
			checked = !$(this).prop('checked') ? false : checked;
		});
		$('.check').prop('checked', !checked);
	});

	$(document).on('click', '.ambil', function(e){
		var formDom = "dataForm";
		if($(this).data("form") !== undefined){
			formDom = $(this).data('form');
		}
		swal({
			title: 'Apakah Anda Yakin?',
				text: "Data yang sudah dikirim, tidak dapat diubah!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Simpan',
				cancelButtonText: 'Batal',
				reverseButtons: true
			}).then((result) => {
				if (result) {
					simpanData(formDom);
				}
			})
	});

	function simpanData(form="dataForm")
	{
		if($("#"+form).form('is valid')){
			$('#formModal').find('.loading.dimmer').addClass('active');
			$('#cover').show();
			$("#"+form).ajaxSubmit({
				success: function(resp){
					console.log('nice');
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							if(postSave(resp)){
								return true;
							}else{
								$('#cover').hide();
								window.location = "{{ url($pageUrl) }}";
								return true;
							}
							$('#formModal').find('.loading.dimmer').removeClass('active');
						})
					},
					error: function(resp){
						if (resp.responseJSON.status !== "undefined" && resp.responseJSON.status === 'false'){
							swal(
								'Oops, Maaf!',
								resp.responseJSON.message,
								'error'
								)
						}

						$('#cover').hide();
						var response = resp.responseJSON;
						$.each(response.errors, function(index, val) {
							clearFormError(index,val);
							showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
						},1000)
						$('#formModal').find('.loading.dimmer').removeClass('active');
					}
				});
		}else{
			$("#"+form).ajaxSubmit({
				success: function(resp){
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							if(postSave(resp)){
								return true;
							}else{
								$('#cover').hide();
								window.location = "{{ url($pageUrl) }}";
								return true;
							}
						})
					},
					error: function(resp){
						$.each(resp.responseJSON, function(index, val) {
							clearFormError(index,val);
							showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.message').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
						},1000)
					}
				});
		}
	}

	$(document).on('click', '.select-all', function(e){
		e.preventDefault();
		var awal = null;		
		$('.dropdown-pelaksana').each(function(e){
			if(awal==null){
				awal = $(this).dropdown('get value');
			}
			if(awal!=null && awal!=''){
				$(this).dropdown('set selected',awal);
			}
		});
	});

	$(document).on('click', '.kirim-data', function(e){
			var arr = [];
			$('.check').each(function(){
				if($(this).is(':checked')){
					arr.push($(this).data('id'));
				}
			});

			if (arr == '') {
				swal(
					'Oops !',
					'Checklist data terlebih dahulu.',
					'error'
				).then(function(e){
				});
			}else{
				swal({
					title: 'Apakah Anda yakin?',
					text: "Data yang sudah dichecklist, tidak dapat diubah!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya',
					cancelButtonText: 'Batal',
					reverseButtons: true
				}).then((result) => {
					if (result) {
						event.preventDefault();
						var url = "{{ url($pageUrl) }}/simpanLaporanData/"+arr+"/2";
						loadModal({
							'url' : url,
							'formId' : '#dataForm',
							'modal' : 'tiny',
							'onShow' : function(){ 
								onShow();
							},
						})
					}
				})
			}
		});
</script>
@append