@extends('layouts.form')

@section('styles')
<style type="text/css">
	.responsive.table{
		width: 100%;
		overflow-x: auto;
	}
	.jus{
		text-align: justify;
		text-justify: inter-word;
	}
	.fn{
		font-size: 12px;
	}
	.ui.status.dropdown .menu .item:nth-child(1) {
		background-color: #00abffcc;
		color: #333;
	}
	.ui.status.dropdown .menu .item:nth-child(2) {
		background-color: #dd0000cc;
		color: #333;
	}
/*	.ui.status.dropdown .menu .item:nth-child(3) {
		background-color: #dddd00cc;
		color: #333;
	}*/
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		judul: ['empty'],
	};
</script>
@endsection

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
{!! csrf_field() !!}
	<div style="overflow-x:auto;">
		<table id="listTable" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
			<thead>
				<tr class="middle aligned">
					<th width="10" class="center aligned fn">#</th>
					<th width="200" class="center aligned fn">Jenis Pengujian</th>
					<th width="200" class="center aligned fn">Nama Barang</th>
					<th width="150" class="center aligned fn">Merk</th>
					<th width="150" class="center aligned fn">No Seri</th>
					<th width="250" class="center aligned fn">Catatan</th>
					<th width="300" class="center aligned fn">Pelaksana Verifikasi</th>
					<th width="300" class="center aligned fn">Hasil Verifikasi</th>
				</tr>
			</thead>
			<tbody class="detail fluid container detail">
				@php 
					$i=1;
					$angka =0;
				@endphp
				@foreach($record->detailpengujian->where('flag', 0)->groupBy('group_id') as $key => $riw)
					@foreach($riw as $kuy => $row)
						@if($kuy == 0)
							<tr class="middle aligned">
								<td class="center aligned" rowspan="{{$riw->count()}}">{{$i}}</td>
								<td rowspan="{{$riw->count()}}">{{$row->detail_penerimaan_barang->detail_pp->jenis->nama}}</td>
								<td>{{$row->detail_penerimaan_barang->nama_barang}} [{{$row->detail_penerimaan_barang->urutan}}/{{$row->detail_penerimaan_barang->max}}]</td>
								<td rowspan="{{$riw->count()}}">{{$row->detail_penerimaan_barang->detail_pp->merk}}</td>
								<td>{{$row->detail_penerimaan_barang->no_seri}}</td>
								<td style="text-align:justify;text-justify:auto;">{!! readMoreText($row->detail_penerimaan_barang->catatan, 50) !!}</td>
								<td rowspan="{{$riw->count()}}">
									{{$row->pelaksana->nama}}
								</td>
								<td rowspan="{{$riw->count()}}">
									@if($row->status == 0)
										@if(auth()->user()->id == $row->pelaksana_id)
										<input type="hidden" name="detail[{{$row->id}}][detail_id]" value="{{$row->id}}">
										<input type="hidden" name="detail[{{$row->id}}][group_id]" value="{{ $row->group_id }}">
										<select name="detail[{{$row->id}}][verifikasi]" class="watcher ui fluid search transparent status dropdown">
											<option value="">Pilih Salah Satu</option>
											<option value="1" selected>DITERIMA</option>
											<option value="2">DITOLAK</option>
										</select>
										@else 
											-
										@endif
									@elseif($row->status == 3)
										<div class="ui green label">
											<i class="check icon"></i> Sudah Disetujui
										</div>
									@elseif($row->status == 1)
										<div class="ui label">
											<i class="hourglass end icon"></i>Menunggu Verifikasi Pelaksana
										</div>
									@else 
										<div class="ui green label">
											<i class="check icon"></i> Sudah Disetujui
										</div>
									@endif
								</td>
							</tr>
						@else 
							<tr class="middle aligned">
								<td style="border-left: 1px solid #e8e9e9">{{$row->detail_penerimaan_barang->nama_barang}} [{{$row->detail_penerimaan_barang->urutan}}/{{$row->detail_penerimaan_barang->max}}]</td>
								<td>{{$row->detail_penerimaan_barang->no_seri}}</td>
								<td style="text-align:justify;text-justify:auto;">{!! readMoreText($row->detail_penerimaan_barang->catatan, 50) !!}</td>
							</tr>
						@endif
					@endforeach
					@php
						$i++;
					@endphp
				@endforeach
			</tbody>
		</table>
	</div>
</form>
<div class="actions">
	<div class="ui two column grid">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		<div class="right aligned column">
			<br>
			<button id="simpan" type="button" class="ui large positive right labeled icon simpan button">
				Simpan
				<i class="save icon"></i>
			</button>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.tgl').calendar({
		ampm: false,
		type: 'date',
			// maxDate: tanggal,
			formatter: {
				date: function (date, settings) {
					if (!date) return '';
					let momentDate = moment(date)
					return momentDate.format('YYYY-MM-DD')
				}
			}
		});

	$(".number").on("keypress keyup blur",function (e) {    
		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	});

$(document).ready(function() {
		$('.ui.status.dropdown').css({
			'background-color': '#00abffcc!important',
			'color': '#333'
		});

        $('.ui.status.dropdown').dropdown({
        	onChange: function(value, text, $selectedItem) {
        		switch(text){
        			case 'DITERIMA':
        				$(this).css({
        					'background-color': '#00abffcc!important',
        					'color': '#333'
        				})
        				break;
        			case 'DITOLAK':
        				$(this).css({
        					'background-color': '#dd0000cc!important',
        					'color': '#333'
        				})
        				break;
        		}
		    }
        })

        $(document).on('click', '.simpan.button', function(e){
            swal({
                title: 'Apakah Anda Yakin?',
				text: "Data yang sudah dikirim, tidak dapat diubah!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Simpan',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result) {
                    saveForm('dataForm');
                }
            })
        })
});
</script>
@append