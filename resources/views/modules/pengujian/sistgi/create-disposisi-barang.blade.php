@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
		/*.disposisi.dropdown .menu .item:nth-child(1) {
			background-color: #00dd00cc;
			color: #fff;
		}
		.disposisi.dropdown .menu .item:nth-child(2) {
			background-color: #dd0000cc;
			color: #fff;
		}*/
	</style>
@append
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Disposisi Verifikasi Barang Uji</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id)."/saveDispo" }}" method="POST">
      {!! csrf_field() !!}
      <input type="hidden" name="_method" value="PUT">
      <input type="hidden" name="id" value="{{ $record->id }}">
      <input type="hidden" name="pengujian_id" value="{{ $record->pengujian->id or '0' }}">
	  <input type="hidden" name="pengirim" value="{{ auth()->user()->id }}">
	  <input type="hidden" name="layanan" value="{{ $record->konfirmasi->va->surat->kaji_ulang->pp->layanan_id }}">
	  @php 
	  	if($pengujian){
	  		$status 	= $pengujian->status;
	  		$status_data = $pengujian->status_data;
	  	}else{
	  		$status =1;
	  		$status_data =1;
	  	}
	  	$layanan = $record->konfirmasi->va->surat->kaji_ulang->pp->layanan_id;
	  	if(auth()->user()->roles->first()->name == 'msb-kalibrasi' || auth()->user()->roles->first()->name == 'msb-siskit' || auth()->user()->roles->first()->name == 'msb-sistgi' || auth()->user()->roles->first()->name == 'msb-tegangan-rendah' || auth()->user()->roles->first()->name == 'msb-tegangan-tinggi'){
	  		$tipe =1;
	  		$status_dispo = 2;
	  	}elseif(auth()->user()->roles->first()->name == 'asman-dal-sistgi' || auth()->user()->roles->first()->name == 'asman-lola-sistgi'){
	  		$tipe =2;
	  		$status_dispo = 3;
	  	}elseif(auth()->user()->roles->first()->name == 'yan-kalibrasi' || auth()->user()->roles->first()->name == 'yan-uji'){
	  		$tipe =3;
	  		$status_dispo = 1;
	  	}elseif(auth()->user()->roles->first()->name == 'pelaksana-sistgi'){
	  		$tipe =4;
	  		$status_dispo = 4;
	  	}elseif(auth()->user()->roles->first()->name == 'user'){
	  		$tipe =5;
	  		$status_dispo = 5;
	  	}else{
	  		$tipe =5;
	  		$status_dispo = 5;
	  	}
	  @endphp
		<div class="ui form">
			<input type="hidden" name="status_dispo" value="{{ $status_dispo }}">
			<table class="ui compact table" style="border-radius: 0; margin: 0">
				<tr>
					<td width="100px"><label>No WBS/IO</label></td>
					<td width="5px">:</td>
					<td>{{ $record->konfirmasi->wbs_io }}</td>
				</tr>
				<tr>
					<td><label>Dari </label></td>
					<td>:</td>
					<td>{{ auth()->user()->nama }}</td>
				</tr>
				<tr>
					<td><label>Kepada </label></td>
					<td>:</td>
					<td>
						<div class="field">
							<select name="penerima_id[]" class="watcher ui fluid search dropdown cari perusahaan">
								{!!
									\App\Models\Authentication\User::optionsa('nama', 'id', ['filters' => [function($q) use($layanan, $tipe, $status, $status_data){
										return $q->dispo($layanan, $tipe, $status, $status_data)->get();
									}],
									'selected' => old('perusahaan_id')
								])
								!!}
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td><label>Keterangan </label></td>
					<td>:</td>
					<td>
						<div class="field">
							<textarea name="keterangan" class="areas input" id="keterangan" placeholder="Keterangan" rows="2"></textarea>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="ui divider"></div>
		<div class="actions">
			<div class="ui black deny button" style="background-color: #363636;margin-left: -1px;">
				Batal
			</div>
			<div class="ui right floated save as page button" style="background-color: #00AEEF;">
				Submit
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
</script>