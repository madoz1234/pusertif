@extends('layouts.form')

@section('styles')
<style type="text/css">
	.responsive.table{
		width: 100%;
		overflow-x: auto;
	}
	.jus{
		text-align: justify;
		text-justify: inter-word;
	}
	.fn{
		font-size: 12px;
	}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		judul: ['empty'],
	};
</script>
@endsection

@section('content-body')
@php 
	$nilai = 0;
	foreach ($record->detailpengujian->where('flag',0) as $key => $value) {
		if($value->detailpelaksana->status_hasil_pengujian == 0){
			$nilai = $nilai+1;
		}
	}
@endphp
@if($nilai > 0)
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id)."/saveHasilPengujian" }}" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
	{!! csrf_field() !!}
@else 
	<form class="ui data form" id="form" method="POST" enctype="multipart/form-data">
@endif
	<div style="overflow-x:auto;">
		<table id="listTable" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
			<thead>
				<tr class="middle aligned">
					<th width="10" class="center aligned fn">#</th>
					<th width="200" class="center aligned fn">Jenis Pengujian</th>
					<th width="200" class="center aligned fn">Nama Barang</th>
					<th width="150" class="center aligned fn">Merk</th>
					<th width="150" class="center aligned fn">No Seri</th>
					<th width="150" class="center aligned fn">Tgl Penerimaan Barang Uji</th>
					<th width="250" class="center aligned fn">Catatan</th>
					<th width="250" class="center aligned fn">No FPP/FPK</th>
					<th width="300" class="center aligned fn">Pelaksana Verifikasi</th>
					<th width="150" class="center aligned fn">Tgl Verifikasi Barang Uji</th>
					<th width="300" class="center aligned fn">Hasil Verifikasi</th>
					<th width="200" class="center aligned fn">Hasil Pengujian </br><button type="button" class="ui mini button select-all"><i class="checkmark icon"></i>Samakan hasil untuk semua data</button></th>
					<th width="300" class="center aligned fn">Detil Pelaksana Member</th>
					<th width="200" class="center aligned fn">Barang Uji Kembali</th>
					<th width="200" class="center aligned fn">Laporan Pengujian </br><button type="button" class="ui mini button checked-all"><i class="checkmark icon"></i> Checklist All</button></th>
				</tr>
			</thead>
			<tbody class="detail fluid container detail">
				@php
					$angka=0;
					$i=1;
				@endphp
				@foreach($record->detailpengujian->where('flag',0) as $key => $row)
					<tr class="middle aligned">
						<td class="center aligned">{{$i}}</td>
						<td>{{$row->detail_penerimaan_barang->detail_pp->jenis->nama}}</td>
						<td>{{$row->detail_penerimaan_barang->nama_barang}} [{{$row->detail_penerimaan_barang->urutan}}/{{$row->detail_penerimaan_barang->max}}]</td>
						<td>{{$row->detail_penerimaan_barang->detail_pp->merk}}</td>
						<td>{{$row->detail_penerimaan_barang->no_seri}}</td>
						<td class="center aligned">{{ DateToStringYear($row->detail_penerimaan_barang->tanggal_terima) }}</td>
						<td style="text-align:justify;text-justify:auto;">{!! readMoreText($row->detail_penerimaan_barang->catatan, 50) !!}</td>
						<td class="center aligned">{{$row->fpp_fpk}}</td>
						<td>
							{{$row->pelaksana->nama}}
						</td>
						<td class="center aligned">{{ DateToStringYear(Carbon\Carbon::parse($row->detail_penerimaan_barang->detail_pengujian->created_at)->format('Y-m-d')) }}</td>
						<td class="center aligned">
							@if($row->verifikasi == 1)
								<a class="ui tag label fn" style="background-color:#00abffcc;">DITERIMA</a>
							@else
								<a class="ui tag label fn" style="background-color:#dd0000cc;">DITOLAK</a>
							@endif
						</td>
						<td class="center aligned field">
							@if(auth()->user()->id == $row->detailpelaksana->pelaksana_id)
								@if($row->detailpelaksana->status == 0)
									<input type="hidden" name="detail[{{$row->id}}][detail_id]" value="{{$row->detailpelaksana->id}}">
									<select name="detail[{{$row->id}}][status_hasil]" class="watcher ui fluid search transparent dropdown dropdown-hasil">
											<option value="">Pilih Status Hasil Pengujian</option>
											<option value="1">Berhasil</option>
											<option value="2">Gagal</option>
									</select>
								@else
									@if($row->detailpelaksana->status_hasil_pengujian == 1)
										<a class="ui tag label fn" style="background-color:#3df405;">BERHASIL</a>
									@else 
										<a class="ui tag label fn" style="background-color:#dd0000cc;">GAGAL</a>
									@endif
								@endif
							@else
								@if($row->detailpelaksana->status == 0)
									<div class="ui label">
										<i class="hourglass end icon"></i>Menunggu Verifikasi Hasil Pengujian
									</div>
								@else
									@if($row->detailpelaksana->status_hasil_pengujian == 1)
										<a class="ui tag label fn" style="background-color:#3df405;">BERHASIL</a>
									@else 
										<a class="ui tag label fn" style="background-color:#dd0000cc;">GAGAL</a>
									@endif
								@endif
							@endif
						</td>
						<td class="left aligned field" width="500px">
							<div class="ui ordered list list-more1" data-display="3">
							@foreach($row->detailpelaksana->detailmember as $member)
								<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian">{{ $member->pelaksana->nama }}</a></div>
							@endforeach
							</div>
						</td>
						<td class="center aligned">
							@if(auth()->user()->id == $row->detailpelaksana->pelaksana_id)
								@if($row->detailpelaksana->status_hasil_pengujian == 0)
									@if($row->detailpelaksana->status_barang == 0)
										<div class="ui small basic icon buttons">
											<button class="ui mini circular orange check icon button verifikasi_barang" data-id="{{ $row->detailpelaksana->id }}" data-tooltip="Klik bila barang kembali" data-position="top center">
												<i class="check icon"></i>
											</button>
										</div>
									@else
										<div class="ui label">
											<i class="check icon"></i> Barang dikembalikan
										</div>
									@endif
								@else
									@if($row->detailpelaksana->status_barang == 1)
										<div class="ui label">
											<i class="check icon"></i> Barang dikembalikan
										</div>
									@else 
										<div class="ui label">
											<i class="close icon"></i> Barang tidak dikembalikan
										</div>
									@endif
								@endif
							@else 
								@if($row->detailpelaksana->status_barang == 1)
									<div class="ui label">
										<i class="check icon"></i> Barang dikembalikan
									</div>
								@else 
									<div class="ui label">
										<i class="close icon"></i> Barang tidak dikembalikan
									</div>
								@endif
							@endif
						</td>
						<td class="center aligned">
							@if(auth()->user()->id == $row->detailpelaksana->pelaksana_laporan_id)
								@if($row->detailpelaksana->status_file == 0)
									@php
										$angka++;
									@endphp
									<div class="ui fitted checkbox">
										<input name="checklist[]" class="check" type="checkbox" data-id="{{ $row->detailpelaksana->id }}">
										<label></label>
									</div>
								@else
									<div class="ui green label">
										<i class="check icon"></i> Selesai
									</div>
								@endif
							@else
								-
							@endif
						</td>
					</tr>
					@php
						$i++;
					@endphp
				@endforeach
				<input type="hidden" name="angka" value="{{ $angka }}">
			</tbody>
		</table>
	</div>
</form>
<div class="actions menu top" style="margin-top: 12px;">
	<div class="ui two column grid">
		<div class="left aligned column">
			<div class="ui gray deny labeled icon button next" data-tab="first" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		@if($nilai > 0)
			<div class="right aligned column">
				<button id="simpan" type="button" class="ui large positive right labeled icon simpan button">
					Simpan
					<i class="save icon"></i>
				</button>
			</div>
		@else 
			@if($angka > 0)
				<div class="right aligned column">
					<div class="ui green labeled icon button kirim-data">
						Simpan
						<i class="save icon"></i>
					</div>
				</div>
			@endif
		@endif
	</div>		
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.tgl').calendar({
		ampm: false,
		type: 'date',
			// maxDate: tanggal,
			formatter: {
				date: function (date, settings) {
					if (!date) return '';
					let momentDate = moment(date)
					return momentDate.format('YYYY-MM-DD')
				}
			}
		});

	$(".number").on("keypress keyup blur",function (e) {    
		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	});

	$(document).on('click', '.verifikasi_barang', function(event) {
		event.preventDefault();
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/"+id+"/pengujian-item_uji/9";
		swal({
			title: 'Apakah Anda yakin?',
			text: "Barang yang sudah dinyatakan dikembalikan, tidak dapat diubah!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya',
			cancelButtonText: 'Batal',
			reverseButtons: true
		}).then((result) => {
			if (result) {
				$.ajax({
					url: url,
					type: 'GET',
					success: function(resp){
						window.location.reload();
					},
					error : function(resp){
						window.location.reload();
					}
				});

			}
		})
	});

	$(document).on('click', '.checked-all', function(e){
		var checked		= true;

		$('.check').each(function(e){
			checked = !$(this).prop('checked') ? false : checked;
		});
		$('.check').prop('checked', !checked);
	});

	$(document).on('click', '.select-all', function(e){
		e.preventDefault();
		var awal = null;		
		$('.dropdown-hasil').each(function(e){
			if(awal==null){
				awal = $(this).dropdown('get value');
			}
			if(awal!=null && awal!=''){
				$(this).dropdown('set selected',awal);
			}
		});
	});

	$(document).on('click', '.simpan.button', function(e){
        swal({
            title: 'Apakah Anda Yakin?',
			text: "Data yang sudah dikirim, tidak dapat diubah!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Simpan',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result) {
                saveForm('dataForm');
            }
        })
    });

    $(document).on('click', '.kirim-data', function(e){
			var arr = [];
			$('.check').each(function(){
				if($(this).is(':checked')){
					arr.push($(this).data('id'));
				}
			});
			var angka = $("input[name=angka]").val();
			if (arr == '') {
				swal(
					'Oops !',
					'Checklist data terlebih dahulu.',
					'error'
				).then(function(e){
				});
			}else{
				swal({
					title: 'Apakah Anda yakin?',
					text: "Data yang sudah dichecklist, tidak dapat diubah!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya',
					cancelButtonText: 'Batal',
					reverseButtons: true
				}).then((result) => {
					if (result) {
						event.preventDefault();
						var url = "{{ url($pageUrl) }}/simpanPengujianItemUji/"+arr+"/1";
						$.ajax({
							url: url,
							type: 'GET',
							success: function(resp){
								swal(
									'Tersimpan!',
									'Data berhasil disimpan.',
									'success'
								).then((result) => {
									$('#cover').hide();
									if(angka == arr.length){
										window.location = "{{ url($pageUrl) }}";
									}else{
										window.location.reload();
									}
									return true;
								});
							},
							error : function(resp){
								swal(
									'Gagal!',
									'Data gagal disimpan.',
									'error'
								).then((result) => {
									$('#cover').hide();
									if(angka == arr.length){
										window.location = "{{ url($pageUrl) }}";
									}else{
										window.location.reload();
									}
									return true;
								});
							}
						});
					}
				})
			}
	});
</script>
@append