@extends('layouts.form')

@section('styles')
<style type="text/css">
	.responsive.table{
		width: 100%;
		overflow-x: auto;
	}
	.jus{
		text-align: justify;
		text-justify: inter-word;
	}
	.fn{
		font-size: 12px;
	}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		judul: ['empty'],
	};
</script>
@endsection

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
{!! csrf_field() !!}
	<div style="overflow-x:auto;">
		<table id="listTable" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
			<thead>
				<tr class="middle aligned">
					<th width="10" class="center aligned fn">#</th>
					<th width="200" class="center aligned fn">Jenis Pengujian</th>
					<th width="200" class="center aligned fn">Nama Barang</th>
					<th width="150" class="center aligned fn">Merk</th>
					<th width="150" class="center aligned fn">No Seri</th>
					<th width="250" class="center aligned fn">Catatan</th>
					<th width="300" class="center aligned fn">
						Pelaksana<br>
						<button type="button" class="ui mini button select-all"><i class="checkmark icon"></i> Samakan Pelaksana untuk semua data</button>
					</th>
				</tr>
			</thead>
			<tbody class="detail fluid container detail">
				@foreach($record->detail_penerimaan_barang as $key => $row)
					<input type="hidden" name="pengujian_id" value="{{ $record->pengujian->id }}">
					<input type="hidden" name="detail_penerimaan_id" value="{{ $row->id }}">
					<input type="hidden" name="detail[{{$row->id}}][group_id]" value="{{ $row->group_id }}">
					<tr class="middle aligned">
						<td class="center aligned">{{$key+1}}</td>
						<td>{{$row->detail_pp->jenis->nama}}</td>
						<td>{{$row->nama_barang}} [{{$row->urutan}}/{{$row->max}}]</td>
						<td>{{$row->detail_pp->merk}}</td>
						<td>{{$row->no_seri}}</td>
						<td>{{$row->catatan}}</td>
						<td class="field">
							<select name="detail[{{$row->id}}][pelaksana_id]" class="watcher ui fluid search transparent dropdown dropdown-pelaksana">
								{!! \App\Models\Authentication\User::options('nama', 'id', ['filters' => [function($q){
									return $q->whereHas('roles', function($u){
										$u->where('name', 'pelaksana-kalibrasi');
									})->get();
								}], 'selected'], 'Pilih Salah Satu') !!}
							</select>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</form>
<div class="actions">
	<div class="ui two column grid">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		<div class="right aligned column">
			<br>
			<button id="simpan" type="button" class="ui large positive right labeled icon save as page button">
				Simpan
				<i class="save icon"></i>
			</button>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.tgl').calendar({
		ampm: false,
		type: 'date',
			// maxDate: tanggal,
			formatter: {
				date: function (date, settings) {
					if (!date) return '';
					let momentDate = moment(date)
					return momentDate.format('YYYY-MM-DD')
				}
			}
		});

	$(".number").on("keypress keyup blur",function (e) {    
		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	});

	$(document).on('click', '.select-all', function(e){
		e.preventDefault();
		var awal = null;		
		$('.dropdown-pelaksana').each(function(e){
			if(awal==null){
				awal = $(this).dropdown('get value');
			}
			if(awal!=null && awal!=''){
				$(this).dropdown('set selected',awal);
			}
		});
	});

</script>
@append