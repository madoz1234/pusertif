@extends('layouts.form')

@section('styles')
<style type="text/css">
	.responsive.table{
		width: 100%;
		overflow-x: auto;
	}
	.jus{
		text-align: justify;
		text-justify: inter-word;
	}
	.fn{
		font-size: 12px;
	}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		judul: ['empty'],
	};
</script>
@endsection

@section('content-body')
<form class="ui data form" id="data" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
{!! csrf_field() !!}
	<div style="overflow-x:auto;">
		<table id="listTable" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
			<thead>
				<tr class="middle aligned">
					<th width="10" class="center aligned fn">#</th>
					<th width="200" class="center aligned fn">Jenis Pengujian</th>
					<th width="250" class="center aligned fn">Nama Barang</th>
					<th width="150" class="center aligned fn">Merk</th>
					<th width="150" class="center aligned fn">No Seri</th>
					<th width="150" class="center aligned fn">Tgl Penerimaan Barang Uji</th>
					<th width="150" class="center aligned fn">Hasil Verifikasi</th>
					<th width="150" class="center aligned fn">Tgl Verifikasi Barang Uji</th>
					<th width="250" class="center aligned fn">Catatan</th>
					<th width="250" class="center aligned fn">No FPP/FPK</th>
					<th width="100" class="center aligned fn">Hasil Pengujian</th>
					<th width="200" class="center aligned fn">Tgl Selesai Pengujian</th>
					<th width="300" class="center aligned fn">Pembuat Laporan </br><button type="button" class="ui mini button select-all"><i class="checkmark icon"></i>Samakan pembuat laporan untuk semua data</button></th>
					<th width="200" class="center aligned fn">Barang Uji Kembali</th>
					<th width="300" class="center aligned fn">Laporan Pengujian </br><button type="button" class="ui mini button checked-all"><i class="checkmark icon"></i> Checklist All</button></th>
				</tr>
			</thead>
			<tbody class="detail fluid container detail">
				@php 
					$jumlah=0;
					$i=1;
				@endphp
				@foreach($record->detail_penerimaan_barang->where('flag',0) as $key => $row)
					@php 
						$member = array();
						foreach($row->detail_pengujian->detailpelaksana->detailmember as $key => $value){
							$member[] = $value->pelaksana_id;
						}	
						$id_pelaksana = array($row->detail_pengujian->detailpelaksana->pelaksana_id);
						$result = array_merge($id_pelaksana, $member);
						$result = array_unique($result);
					@endphp
					<input type="hidden" name="pengujian_id" value="{{ $record->pengujian->id }}">
					<input type="hidden" name="detail_penerimaan_id" value="{{ $row->id }}">
					<tr class="middle aligned">
						<td class="center aligned">{{$i}}</td>
						<td>{{$row->detail_pp->jenis->nama}}</td>
						<td>{{$row->nama_barang}} [{{$row->urutan}}/{{$row->max}}]</td>
						<td>{{$row->detail_pp->merk}}</td>
						<td>{{$row->no_seri}}</td>
						<td class="center aligned">{{ DateToStringYear($row->tanggal_terima) }}</td>
						<td class="center aligned">
							@if($row->detail_pengujian->verifikasi == 1)
								<a class="ui tag label fn" style="background-color:#00abffcc;">DITERIMA</a>
							@else
								<a class="ui tag label fn" style="background-color:#dd0000cc;">DITOLAK</a>
							@endif
						</td>
						<td class="center aligned">
							@if($row->detail_pengujian)
								@if($row->detail_pengujian->tgl_selesai)
									{{ DateToStringYear(Carbon\Carbon::parse($row->detail_pengujian->tgl_selesai)->format('Y-m-d')) }}
								@else 
									-
								@endif
							@else
								-
							@endif
						</td>
						<td style="text-align:justify;text-justify:auto;">{!! readMoreText($row->catatan, 50) !!}</td>
						<td class="center aligned">{{ $row->detail_pengujian->fpp_fpk }}</td>
						<td class="center aligned">
							@if($row->detail_pengujian->detailpelaksana)
								@if($row->detail_pengujian->detailpelaksana->status_hasil_pengujian == 1)
									<a class="ui tag label fn" style="background-color:#00FF00;">BERHASIL</a>
								@elseif($row->detail_pengujian->detailpelaksana->status_hasil_pengujian == 2)
									<a class="ui tag label fn" style="background-color:#dd0000cc;">GAGAL</a>
								@else
									<div class="ui label">
										<i class="hourglass end icon"></i>Menunggu Hasil Pengujian
									</div>
								@endif
							@else 
								-
							@endif
						</td>
						<td class="center aligned">
							@if($row->detail_pengujian->detailpelaksana)
								@if($row->detail_pengujian->detailpelaksana->tgl_selesai)
									{{ DateToStringYear(Carbon\Carbon::parse($row->detail_pengujian->detailpelaksana->tgl_selesai)->format('Y-m-d')) }}
								@else 
									-
								@endif
							@else
								-
							@endif
						</td>
						<td class="center aligned field">
							@if($row->detail_pengujian->detailpelaksana->status == 0)
								<div class="ui label">
									<i class="hourglass end icon"></i>Menunggu Verifikasi Hasil Pengujian
								</div>
							@else
								@if($row->detail_pengujian->detailpelaksana->pelaksana_laporan_id !== 1)
									{{ $row->detail_pengujian->detailpelaksana->pelaksanalaporan->nama }}
								@else
									<div class="ui label">
										<i class="hourglass end icon"></i>Menunggu Verifikasi Asman
									</div>
								@endif
							@endif
						</td>
						<td class="center aligned">
							@if($row->detail_pengujian->detailpelaksana->status_barang == 0)
								<div class="ui label">
									<i class="close icon"></i> Barang tidak dikembalikan
								</div>
							@else
								<div class="ui label">
									<i class="check icon"></i> Barang dikembalikan
								</div>
							@endif
						</td>
						<td class="center aligned">
							@if($row->detail_pengujian->detailpelaksana->status == 0)
								<div class="ui label">
									<i class="hourglass end icon"></i>Menunggu Laporan Hasil Pengujian
								</div>
							@else
								@if($row->detail_pengujian->detailpelaksana->status_file == 1)
									@if($row->detail_pengujian->detailpelaksana->status_data == 2)
										@php
											$jumlah++;
										@endphp
										<div class="ui fitted checkbox">
											<input name="checklist[]" class="check" type="checkbox" data-id="{{ $row->detail_pengujian->detailpelaksana->id }}">
											<label></label>
										</div>
									@elseif($row->detail_pengujian->detailpelaksana->status_data == 3)
										<div class="ui green label">
											<i class="check icon"></i> Sudah Disetujui
										</div>
									@else 
										<div class="ui label">
											<i class="hourglass end icon"></i>Menunggu Laporan Asmen
										</div>
									@endif
								@else
									<div class="ui label">
										<i class="hourglass end icon"></i>Menunggu Laporan Pengujian
									</div>
								@endif
							@endif
						</td>
					</tr>
					@php
						$i++;
					@endphp
				@endforeach
			</tbody>
		</table>
	</div>
</form>
<div class="actions menu top" style="margin-top: 12px;">
	<div class="ui two column grid">
		<div class="left aligned column">
			<div class="ui gray deny labeled icon button next" data-tab="first" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		@if($jumlah > 0)
			<div class="right aligned column">
				<div class="ui green labeled icon button kirim-data">
					Simpan
					<i class="save icon"></i>
				</div>
			</div>
		@endif
	</div>		
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.tgl').calendar({
		ampm: false,
		type: 'date',
			// maxDate: tanggal,
			formatter: {
				date: function (date, settings) {
					if (!date) return '';
					let momentDate = moment(date)
					return momentDate.format('YYYY-MM-DD')
				}
			}
		});

	$(".number").on("keypress keyup blur",function (e) {    
		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	});
	$(document).on('click', '.checked-all', function(e){
		var checked		= true;

		$('.check').each(function(e){
			checked = !$(this).prop('checked') ? false : checked;
		});
		$('.check').prop('checked', !checked);
	});

	$(document).on('click', '.kirim-data', function(e){
			var arr = [];
			$('.check').each(function(){
				if($(this).is(':checked')){
					arr.push($(this).data('id'));
				}
			});

			if (arr == '') {
				swal(
					'Oops !',
					'Checklist data terlebih dahulu.',
					'error'
				).then(function(e){
				});
			}else{
				swal({
					title: 'Apakah Anda yakin?',
					text: "Data yang sudah dichecklist, tidak dapat diubah!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya',
					cancelButtonText: 'Batal',
					reverseButtons: true
				}).then((result) => {
					if (result) {
						event.preventDefault();
						var url = "{{ url($pageUrl) }}/simpanLaporanData/"+arr+"/2";
						loadModal({
							'url' : url,
							'formId' : '#dataForm',
							'modal' : 'tiny',
							'onShow' : function(){ 
								onShow();
							},
						})
					}
				})
			}
		});
</script>
@append