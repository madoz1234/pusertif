@extends('layouts.form')

@section('styles')
<style type="text/css">
	.responsive.table{
		width: 100%;
		overflow-x: auto;
	}
	.jus{
		text-align: justify;
		text-justify: inter-word;
	}
	.fn{
		font-size: 12px;
	}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		judul: ['empty'],
	};
</script>
@endsection

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id)."/saveDispoPelaksana" }}" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
{!! csrf_field() !!}
	<div style="overflow-x:auto;">
		<table id="listTable" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
			<thead>
				<tr class="middle aligned">
					<th width="10" class="center aligned fn">#</th>
					<th width="200" class="center aligned fn">Jenis Pengujian</th>
					<th width="200" class="center aligned fn">Nama Barang</th>
					<th width="150" class="center aligned fn">Merk</th>
					<th width="150" class="center aligned fn">No Seri</th>
					<th width="250" class="center aligned fn">Catatan</th>
					<th width="300" class="center aligned fn">Pelaksana Verifikasi</th>
					<th width="300" class="center aligned fn">Hasil Verifikasi</th>
				</tr>
			</thead>
			<tbody class="detail fluid container detail">
				@foreach($record->detailpengujian->where('flag',0) as $key => $row)
				<input type="hidden" name="detail[{{$row->id}}][detail_id]" value="{{$row->id}}">
					<tr class="middle aligned">
						<td class="center aligned">{{$key+1}}</td>
						<td>{{$row->detail_penerimaan_barang->detail_pp->jenis->nama}}</td>
						<td>{{$row->detail_penerimaan_barang->nama_barang}} [{{$row->detail_penerimaan_barang->urutan}}/{{$row->detail_penerimaan_barang->max}}]</td>
						<td>{{$row->detail_penerimaan_barang->detail_pp->merk}}</td>
						<td>{{$row->detail_penerimaan_barang->no_seri}}</td>
						<td style="text-align:justify;text-justify:auto;">{!! readMoreText($row->detail_penerimaan_barang->catatan, 50) !!}</td>
						<td>
							{{$row->pelaksana->nama}}
						</td>
						<td class="center aligned">
							@if($row->verifikasi == 1)
								<a class="ui tag label fn" style="background-color:#00abffcc;">DITERIMA</a>
							@else
								<a class="ui tag label fn" style="background-color:#dd0000cc;">DITOLAK</a>
							@endif
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</form>
<div class="actions">
	<div class="ui two column grid">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
	</div>	
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.tgl').calendar({
		ampm: false,
		type: 'date',
			// maxDate: tanggal,
			formatter: {
				date: function (date, settings) {
					if (!date) return '';
					let momentDate = moment(date)
					return momentDate.format('YYYY-MM-DD')
				}
			}
		});

	$(".number").on("keypress keyup blur",function (e) {    
		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	});

	$(document).on('click', '.verifikasi', function(event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = "{{ url($pageUrl) }}/"+id+"/verifikasi-item_uji/1";
            swal({
            	title: 'Apakah Anda yakin?',
            	text: "Data yang sudah disetujui, tidak dapat diubah!",
            	type: 'warning',
            	showCancelButton: true,
            	confirmButtonColor: '#3085d6',
            	cancelButtonColor: '#d33',
            	confirmButtonText: 'Ya',
            	cancelButtonText: 'Batal',
            	reverseButtons: true
            }).then((result) => {
            	if (result) {
            		$.ajax({
            			url: url,
            			type: 'GET',
            			success: function(resp){
        					window.location.reload();
        				},
        				error : function(resp){
        							window.location.reload();
        				}
        			});

            	}
            })
        });

</script>
@append