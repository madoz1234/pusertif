@extends('layouts.form')

@section('styles')
<style type="text/css">
	.responsive.table{
		width: 100%;
		overflow-x: auto;
	}
	.jus{
		text-align: justify;
		text-justify: inter-word;
	}
	.fn{
		font-size: 12px;
	}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		judul: ['empty'],
	};
</script>
@endsection

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id)."/savePelaksana" }}" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
	{!! csrf_field() !!}
	<div style="overflow-x:auto;">
		<table id="listTable" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
			<thead>
				<tr class="middle aligned">
					<th width="10" class="center aligned fn">#</th>
					<th width="200" class="center aligned fn">Jenis Pengujian</th>
					<th width="250" class="center aligned fn">Nama Barang</th>
					<th width="150" class="center aligned fn">Merk</th>
					<th width="150" class="center aligned fn">No Seri</th>
					<th width="150" class="center aligned fn">Tgl Penerimaan Barang Uji</th>
					<th width="150" class="center aligned fn">Hasil Verifikasi</th>
					<th width="150" class="center aligned fn">Tgl Verifikasi Barang Uji</th>
					<th width="250" class="center aligned fn">Catatan</th>
					<th width="250" class="center aligned fn">No FPP/FPK</th>
					<th width="300" class="center aligned fn">Pelaksana (Leader) </br><button type="button" class="ui mini button select-all"><i class="checkmark icon"></i>Samakan pelaksana untuk semua data</button></th>
					<th width="300" class="center aligned fn">Pelaksana (Member)</th>
				</tr>
			</thead>
			<tbody class="detail fluid container detail">
				@php
					$i=1;
				@endphp
				@foreach($record->detail_penerimaan_barang->where('flag',0) as $key => $row)
					<input type="hidden" name="pengujian_id" value="{{ $record->pengujian->id }}">
					<input type="hidden" name="detail_penerimaan_id" value="{{ $row->id }}">
					<tr class="middle aligned">
						<td class="center aligned">{{$i}}</td>
						<td>{{$row->detail_pp->jenis->nama}}</td>
						<td>{{$row->nama_barang}} [{{$row->urutan}}/{{$row->max}}]</td>
						<td>{{$row->detail_pp->merk}}</td>
						<td>{{$row->no_seri}}</td>
						<td class="center aligned">{{ DateToStringYear($row->tanggal_terima) }}</td>
						<td class="center aligned">
							@if($row->detail_pengujian->verifikasi == 1)
								<a class="ui tag label fn" style="background-color:#00abffcc;">DITERIMA</a>
							@else
								<a class="ui tag label fn" style="background-color:#dd0000cc;">DITOLAK</a>
							@endif
						</td>
						<td class="center aligned">{{ DateToStringYear(Carbon\Carbon::parse($row->detail_pengujian->created_at)->format('Y-m-d')) }}</td>
						<td style="text-align:justify;text-justify:auto;">{!! readMoreText($row->catatan, 50) !!}</td>
						<td class="field">
							<input type="text" name="detail[{{ $row->id }}][fpp_fpk]" placeholder="No FPP/FPK">
						</td>
						<td class="field">
							<input type="hidden" name="detail[{{ $row->id }}][detail_pengujian_id]" value="{{ $row->detail_pengujian->id }}">
							<select name="detail[{{$row->id}}][pelaksana_leader]" class="watcher ui fluid search transparent dropdown dropdown-pelaksana">
								{!! \App\Models\Authentication\User::options('nama', 'id', ['filters' => [function($q){
									return $q->whereHas('roles', function($u){
										$u->where('name', 'pelaksana-kalibrasi');
									})->get();
								}], 'selected'], 'Pilih Salah Satu') !!}
							</select>
						</td>
						<td class="field">
							<select name="detail[{{$row->id}}][pelaksana_id][]" class="watcher ui fluid search transparent dropdown" multiple="">
								{!! \App\Models\Authentication\User::options('nama', 'id', ['filters' => [function($q){
									return $q->whereHas('roles', function($u){
										$u->where('name', 'pelaksana-kalibrasi');
									})->get();
								}], 'selected'], 'Pilih Salah Satu') !!}
							</select>
						</td>
					</tr>
					@php
						$i++;
					@endphp
				@endforeach
			</tbody>
		</table>
	</div>
</form>
<div class="actions menu top" style="margin-top: 12px;">
	<div class="ui two column grid">
		<div class="left aligned column">
			<div class="ui gray deny labeled icon button next" data-tab="first" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		<div class="right aligned column">
			<div class="ui green labeled icon button save as page">
				Simpan
				<i class="save icon"></i>
			</div>
		</div>
	</div>		
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.tgl').calendar({
		ampm: false,
		type: 'date',
			// maxDate: tanggal,
			formatter: {
				date: function (date, settings) {
					if (!date) return '';
					let momentDate = moment(date)
					return momentDate.format('YYYY-MM-DD')
				}
			}
		});

	$(".number").on("keypress keyup blur",function (e) {    
		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	});

	$(document).on('click', '.select-all', function(e){
		e.preventDefault();
		var awal = null;		
		$('.dropdown-pelaksana').each(function(e){
			if(awal==null){
				awal = $(this).dropdown('get value');
			}
			if(awal!=null && awal!=''){
				$(this).dropdown('set selected',awal);
			}
		});
	});
</script>
@append