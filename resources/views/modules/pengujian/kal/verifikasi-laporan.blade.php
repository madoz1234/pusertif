@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
	</style>
@append
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Upload Laporan Pengujian</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$id)."/saveUpload" }}" method="POST">
      {!! csrf_field() !!}
      <input type="hidden" name="_method" value="PUT">
      <input type="hidden" name="id" value="{{ $id }}">
		<div class="ui form">
			<table class="ui compact table" style="border-radius: 0; margin: 0">
				<tr>
					<td><label>Upload Laporan Pengujian </label></td>
					<td>:</td>
					<td>
						<div class="field">
							<div class="ui action choises input">
								<input type="text" class="isi_hasil" readonly placeholder="Format .doc, .xls, .pdf, .jpg, .jpeg, .png">
								<input type="file" name="bukti" style="display: none!important;" accept=".doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf,image/x-eps,image/jpeg,image/gif,image,.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
								<div class="ui icon hasil button">
									<i class="cloud upload alternate icon"></i>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="ui divider"></div>
		<div class="actions">
			<div class="ui black deny button" style="background-color: #363636;margin-left: -1px;">
				Batal
			</div>
			<div class="ui right floated simpan button" style="background-color: #00AEEF;">
				Submit
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('change', '[name^=hasil]', function (e) {
			var count = e.target.files.length;
			if (count > 0) {
				$('.isi_hasil').val(count+` Files Selected`);
			}
		});
	});


	$(document).on('click', '.simpan', function(e){
		var formDom = "dataForm";
		if($(this).data("form") !== undefined){
			formDom = $(this).data('form');
		}
		swal({
			title: 'Apakah Anda Yakin?',
				text: "Data yang sudah diupload, tidak dapat diubah!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Simpan',
				cancelButtonText: 'Batal',
				reverseButtons: true
			}).then((result) => {
				if (result) {
					saveForm(formDom);
				}
			})
	});

	function saveForm(form="dataForm")
	{
		if($("#"+form).form('is valid')){
			$('#formModal').find('.loading.dimmer').addClass('active');
			$('#cover').show();
			$("#"+form).ajaxSubmit({
				success: function(resp){
					console.log('nice');
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							if(postSave(resp)){
								return true;
							}else{
								$('#cover').hide();
								window.location.reload();
								return true;
							}
							$('#formModal').find('.loading.dimmer').removeClass('active');
						})
					},
					error: function(resp){
						if (resp.responseJSON.status !== "undefined" && resp.responseJSON.status === 'false'){
							swal(
								'Oops, Maaf!',
								resp.responseJSON.message,
								'error'
								)
						}

						$('#cover').hide();
						var response = resp.responseJSON;
						$.each(response.errors, function(index, val) {
							clearFormError(index,val);
							showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
						},1000)
						$('#formModal').find('.loading.dimmer').removeClass('active');
					}
				});
		}else{
			$("#"+form).ajaxSubmit({
				success: function(resp){
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							if(postSave(resp)){
								return true;
							}else{
								$('#cover').hide();
								window.location.reload();
								return true;
							}
						})
					},
					error: function(resp){
						$.each(resp.responseJSON, function(index, val) {
							clearFormError(index,val);
							showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.message').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
						},1000)
					}
				});
		}
	}
</script>