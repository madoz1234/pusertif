@extends('layouts.form')

@section('styles')
<style type="text/css">
	.responsive.table{
		width: 100%;
		overflow-x: auto;
	}
	.jus{
		text-align: justify;
		text-justify: inter-word;
	}
	.fn{
		font-size: 12px;
	}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		judul: ['empty'],
	};
</script>
@endsection

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id)."/savePelaksana" }}" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
	{!! csrf_field() !!}
	<div style="overflow-x:auto;">
		<table id="listTable" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
			<thead>
				<tr class="middle aligned">
					<th width="10" class="center aligned fn">#</th>
					<th width="200" class="center aligned fn">Jenis Pengujian</th>
					<th width="250" class="center aligned fn">Nama Barang</th>
					<th width="150" class="center aligned fn">Merk</th>
					<th width="150" class="center aligned fn">No Seri</th>
					<th width="150" class="center aligned fn">Tgl Penerimaan Barang Uji</th>
					<th width="150" class="center aligned fn">Hasil Verifikasi</th>
					<th width="150" class="center aligned fn">Tgl Verifikasi Barang Uji</th>
					<th width="250" class="center aligned fn">Catatan</th>
					<th width="250" class="center aligned fn">No FPP/FPK</th>
					<th width="300" class="center aligned fn">Pelaksana (Leader) </br><button type="button" class="ui mini button select-all"><i class="checkmark icon"></i>Samakan pelaksana untuk semua data</button></th>
					<th width="300" class="center aligned fn">Pelaksana (Member)</th>
				</tr>
			</thead>
			<tbody>
				@php 
					$i=1;
				@endphp
				@foreach($record->detail_penerimaan_barang->where('flag', 0)->groupBy('group_id') as $key => $row)
					@foreach($row as $kuy => $data)
						@if($kuy == 0)
							<tr>
								<td rowspan="{{$row->count()}}">{{$i}}</td>
								<td rowspan="{{$row->count()}}">{{$data->detail_pp->jenis->nama}}</td>
								<td>{{ $data->nama_barang }} [{{$data->urutan}}/{{$data->max}}]</td>
								<td rowspan="{{$row->count()}}">{{ $data->detail_pp->merk }}</td>
								<td>{{$data->no_seri}}</td>
								<td class="center aligned" rowspan="{{$row->count()}}">
									@if($data->updated_at)
										{{ $data->updated_at }}
									@else 
										{{ DateToStringYear($data->tanggal_terima) }}
									@endif
								</td>
								<td class="center aligned" rowspan="{{$row->count()}}">
									@if($data->detail_pengujian->verifikasi == 1)
										<a class="ui tag label fn" style="background-color:#00abffcc;">DITERIMA</a>
									@else
										<a class="ui tag label fn" style="background-color:#dd0000cc;">DITOLAK</a>
									@endif
								</td>
								<td class="center aligned" rowspan="{{$row->count()}}">{{ DateToStringYear(Carbon\Carbon::parse($data->detail_pengujian->created_at)->format('Y-m-d')) }}</td>
								<td style="text-align:justify;text-justify:auto;">{!! readMoreText($data->catatan, 50) !!}</td>
								<td rowspan="{{$row->count()}}" class="field">
									<input type="text" name="detail[{{ $row->first()->id }}][fpp_fpk]" placeholder="No FPP/FPK">
								</td>
								<td rowspan="{{$row->count()}}" class="field">
									<input type="hidden" name="detail[{{ $row->first()->id }}][detail_pengujian_id]" value="{{ $row->first()->detail_pengujian->id }}">
									<select name="detail[{{$row->first()->id}}][pelaksana_leader]" class="watcher ui fluid search transparent dropdown dropdown-pelaksana">
										{!! \App\Models\Authentication\User::options('nama', 'id', ['filters' => [function($q){
											return $q->whereHas('roles', function($u){
												$u->where('name', 'pelaksana-tegangan-tinggi');
											})->get();
										}], 'selected'], 'Pilih Salah Satu') !!}
									</select>
								</td>
								<td rowspan="{{$row->count()}}" class="field">
									<select name="detail[{{$row->first()->id}}][pelaksana_id][]" class="watcher ui fluid search transparent dropdown" multiple="">
										{!! \App\Models\Authentication\User::options('nama', 'id', ['filters' => [function($q){
											return $q->whereHas('roles', function($u){
												$u->where('name', 'pelaksana-tegangan-tinggi');
											})->get();
										}], 'selected'], 'Pilih Salah Satu') !!}
									</select>
								</td>
							</tr>
						@else
							<tr>
								<td style="border-left: 1px solid #e8e9e9">{{ $data->nama_barang }} [{{$data->urutan}}/{{$data->max}}]</td>
								<td>{{$data->no_seri}}</td>
								<td style="text-align:justify;text-justify:auto;">{!! readMoreText($data->catatan, 50) !!}</td>
							</tr>
						@endif
					@endforeach
					@php
						$i++;
					@endphp
				@endforeach
			</tbody>
		</table>
	</div>
</form>
<div class="actions menu top" style="margin-top: 12px;">
	<div class="ui two column grid">
		<div class="left aligned column">
			<div class="ui gray deny labeled icon button next" data-tab="first" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		<div class="right aligned column">
			<div class="ui green labeled icon button save as page">
				Simpan
				<i class="save icon"></i>
			</div>
		</div>
	</div>		
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.tgl').calendar({
		ampm: false,
		type: 'date',
			// maxDate: tanggal,
			formatter: {
				date: function (date, settings) {
					if (!date) return '';
					let momentDate = moment(date)
					return momentDate.format('YYYY-MM-DD')
				}
			}
		});

	$(".number").on("keypress keyup blur",function (e) {    
		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	});

	$(document).on('click', '.select-all', function(e){
		e.preventDefault();
		var awal = null;		
		$('.dropdown-pelaksana').each(function(e){
			if(awal==null){
				awal = $(this).dropdown('get value');
			}
			if(awal!=null && awal!=''){
				$(this).dropdown('set selected',awal);
			}
		});
	});
</script>
@append