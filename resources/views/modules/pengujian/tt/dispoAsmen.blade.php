@extends('layouts.form')

@section('styles')
<style type="text/css">
	.responsive.table{
		width: 100%;
		overflow-x: auto;
	}
	.jus{
		text-align: justify;
		text-justify: inter-word;
	}
	.fn{
		font-size: 12px;
	}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		judul: ['empty'],
	};
</script>
@endsection

@section('content-body')
<form class="ui data form" id="data" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
{!! csrf_field() !!}
	<div style="overflow-x:auto;">
		<table id="listTable" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
			<thead>
				<tr class="middle aligned">
					<th width="10" class="center aligned fn">#</th>
					<th width="200" class="center aligned fn">Jenis Pengujian</th>
					<th width="200" class="center aligned fn">Nama Barang</th>
					<th width="150" class="center aligned fn">Merk</th>
					<th width="150" class="center aligned fn">No Seri</th>
					<th width="250" class="center aligned fn">Catatan</th>
					<th width="300" class="center aligned fn">Pelaksana Verifikasi</th>
					<th width="300" class="center aligned fn">Hasil Verifikasi</th>
					<th width="300" class="center aligned fn">Approval </br><button type="button" class="ui mini button select-all"><i class="checkmark icon"></i> Checklist All</button></th>
				</tr>
			</thead>
			<tbody class="detail fluid container detail">
				@php 
					$i=1;
					$angka =0;
				@endphp
				@foreach($record->detailpengujian->where('flag', 0)->groupBy('group_id') as $key => $riw)
					@foreach($riw as $kuy => $row)
						@if($kuy == 0)
							<input type="hidden" name="detail[{{$row->id}}][detail_id]" value="{{$row->id}}">
							<tr class="middle aligned">
								<td class="center aligned" rowspan="{{$riw->count()}}">{{$i}}</td>
								<td rowspan="{{$riw->count()}}">{{$row->detail_penerimaan_barang->detail_pp->jenis->nama}}</td>
								<td>{{$row->detail_penerimaan_barang->nama_barang}} [{{$row->detail_penerimaan_barang->urutan}}/{{$row->detail_penerimaan_barang->urutan}}]</td>
								<td rowspan="{{$riw->count()}}" class="center aligned">{{$row->detail_penerimaan_barang->detail_pp->merk}}</td>
								<td>{{$row->detail_penerimaan_barang->no_seri}}</td>
								<td style="text-align:justify;text-justify:auto;">{!! readMoreText($row->detail_penerimaan_barang->catatan, 50) !!}</td>
								<td rowspan="{{$riw->count()}}" class="center aligned">
									{{$row->pelaksana->nama}}
								</td>
								<td class="center aligned" rowspan="{{$riw->count()}}">
									@if($row->verifikasi == 1)
										<a class="ui tag label fn" style="background-color:#00abffcc;">DITERIMA</a>
									@elseif($row->verifikasi == 2)
										<a class="ui tag label fn" style="background-color:#dd0000cc;">DITOLAK</a>
									@else
										<div class="ui label">
											<i class="hourglass end icon"></i>Menunggu Verifikasi Pelaksana
										</div>
									@endif
								</td>
								<td class="center aligned" rowspan="{{$riw->count()}}">
									@if($row->status == 2)
										@if($row->verifikasi == 2)
											@if($row->detailreshcedule->tentative_start_new)
												<div class="ui label">
													<i class="hourglass end icon"></i>Menunggu Verifikasi Disetujui Pelaksana
												</div>
											@else 
												<div class="ui label">
													<i class="hourglass end icon"></i>Menunggu Reschedule
												</div>
											@endif
										@else 
											@php
												$angka++;
											@endphp
											<div class="ui fitted checkbox">
												<input name="checklist[]" class="check" type="checkbox" data-id="{{ $row->id }}">
												<label></label>
											</div>
										@endif
									@elseif($row->status == 3)
										<div class="ui green label">
											<i class="check icon"></i> Sudah Disetujui
										</div>
									@elseif($row->status == 1)
										<div class="ui label">
											<i class="hourglass end icon"></i>Menunggu Verifikasi Pelaksana
										</div>
									@elseif($row->status == 0)
										<div class="ui label">
											<i class="hourglass end icon"></i>Menunggu Verifikasi Pelaksana
										</div>
									@else 
										<div class="ui green label">
											<i class="check icon"></i> Sudah Disetujui
										</div>
									@endif
								</td>
							</tr>
						@else 
							<tr class="middle aligned">
								<td style="border-left: 1px solid #e8e9e9">{{$row->detail_penerimaan_barang->nama_barang}} [{{$row->detail_penerimaan_barang->urutan}}/{{$row->detail_penerimaan_barang->urutan}}]</td>
								<td>{{$row->detail_penerimaan_barang->no_seri}}</td>
								<td style="text-align:justify;text-justify:auto;">{!! readMoreText($row->detail_penerimaan_barang->catatan, 50) !!}</td>
								{{-- <td class="center aligned">
									@if($row->verifikasi == 1)
										<a class="ui tag label fn" style="background-color:#00abffcc;">DITERIMA</a>
									@elseif($row->verifikasi == 2)
										<a class="ui tag label fn" style="background-color:#dd0000cc;">DITOLAK</a>
									@else
										<div class="ui label">
											<i class="hourglass end icon"></i>Menunggu Verifikasi Pelaksana
										</div>
									@endif
								</td>
								<td class="center aligned">
									@if($row->status == 2)
										@if($row->verifikasi == 2)
											@if($row->detailreshcedule->tentative_start_new)
												@php
												$angka++;
												@endphp
												<div class="ui fitted checkbox">
													<input name="checklist[]" class="check" type="checkbox" data-id="{{ $row->id }}">
													<label></label>
												</div>
											@else 
												<div class="ui label">
													<i class="hourglass end icon"></i>Menunggu Reschedule
												</div>
											@endif
										@else 
											@php
												$angka++;
											@endphp
											<div class="ui fitted checkbox">
												<input name="checklist[]" class="check" type="checkbox" data-id="{{ $row->id }}">
												<label></label>
											</div>
										@endif
									@elseif($row->status == 3)
										<div class="ui green label">
											<i class="check icon"></i> Sudah Disetujui
										</div>
									@elseif($row->status == 1)
										<div class="ui label">
											<i class="hourglass end icon"></i>Menunggu Verifikasi Pelaksana
										</div>
									@elseif($row->status == 0)
										<div class="ui label">
											<i class="hourglass end icon"></i>Menunggu Verifikasi Pelaksana
										</div>
									@else 
										<div class="ui green label">
											<i class="check icon"></i> Sudah Disetujui
										</div>
									@endif
								</td> --}}
							</tr>
						@endif
					@endforeach
					@php
						$i++;
					@endphp
				@endforeach
				<input type="hidden" name="angka" value="{{ $angka }}">
			</tbody>
		</table>
	</div>
</form>
<div class="actions">
	<div class="ui two column grid">
		<div class="left aligned column">
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		@if($angka > 0)
			<div class="right aligned column">
				<div class="ui green labeled icon button kirim-data">
					Simpan
					<i class="save icon"></i>
				</div>
			</div>
		@endif
	</div>	
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.tgl').calendar({
		ampm: false,
		type: 'date',
			// maxDate: tanggal,
			formatter: {
				date: function (date, settings) {
					if (!date) return '';
					let momentDate = moment(date)
					return momentDate.format('YYYY-MM-DD')
				}
			}
		});

	$(".number").on("keypress keyup blur",function (e) {    
		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	});

	$(document).on('click', '.select-all', function(e){
		var checked		= true;

		$('.check').each(function(e){
			checked = !$(this).prop('checked') ? false : checked;
		});
		$('.check').prop('checked', !checked);
	});

	$(document).on('click', '.kirim-data', function(e){
			var arr = [];
			$('.check').each(function(){
				if($(this).is(':checked')){
					arr.push($(this).data('id'));
				}
			});
			var angka = $("input[name=angka]").val();
			if (arr == '') {
				swal(
					'Oops !',
					'Checklist data terlebih dahulu.',
					'error'
				).then(function(e){
				});
			}else{
				swal({
					title: 'Apakah Anda yakin?',
					text: "Data yang sudah dichecklist, tidak dapat diubah!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya',
					cancelButtonText: 'Batal',
					reverseButtons: true
				}).then((result) => {
					if (result) {
						event.preventDefault();
						var url = "{{ url($pageUrl) }}/simpanDispo/"+arr+"/2";
						$.ajax({
							url: url,
							type: 'GET',
							success: function(resp){
								if(angka == arr.length){
									window.location = "{{ url($pageUrl) }}";
								}else{
									window.location.reload();
								}
							},
							error : function(resp){
								if(angka == arr.length){
									window.location = "{{ url($pageUrl) }}";
								}else{
									window.location.reload();
								}
							}
						});
					}
				})
			}
		});
</script>
@append