@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
	</style>
@append
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Upload Laporan Final</div>
<div class="content">
	@php 
		$id = 1;
	@endphp
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$id)."/uploadLaporan" }}" method="POST" enctype="multipart/form-data">
      <input type="hidden" name="_method" value="PUT">
      <input type="hidden" name="id" value="{{ $id }}">
      <input type="hidden" name="data" value="{{ $data }}">
      {!! csrf_field() !!}
		<div class="ui form">
			<table class="ui compact table" style="border-radius: 0; margin: 0">
				<tr>
					<td style="width:150px;"><label>Upload Laporan Final</label></td>
					<td>:</td>
					<td>
						<div class="field">
							<div class="ui action choises input">
								<input type="text" class="isi_hasil" readonly placeholder="Format .doc, .xls, .pdf, .jpg, .jpeg, .png">
								<input type="file" name="bukti[]" style="display: none!important;" accept=".doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf,image/x-eps,image/jpeg,image/gif,image, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" multiple="">
								<div class="ui icon hasil button">
									<i class="cloud upload alternate icon"></i>
								</div>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td><label>No Laporan</label></td>
					<td>:</td>
					<td>
						<div class="field">
							<input name="no_laporan" type="text" placeholder="No Laporan" class="ui input transparent">
						</div>
					</td>
				</tr>
				<tr>
					<td><label>Tgl Laporan</label></td>
					<td>:</td>
					<td>
						<div class="field">
							<div class="ui calendar labeled input tgl">
								<div class="ui input left icon">
									<i class="calendar icon date"></i>
									<input name="tgl_laporan" type="text" placeholder="Tgl Laporan">
								</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="ui divider"></div>
		<div class="actions">
			<div class="ui black deny button" style="background-color: #363636;margin-left: -1px;">
				Batal
			</div>
			<div class="ui right floated simpan button" style="background-color: #00AEEF;">
				Submit
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('change', '[name^=bukti]', function (e) {
			var count = e.target.files.length;
			if (count > 0) {
				$('.isi_hasil').val(count+` Files Selected`);
			}
		});
	});

	$('.tgl').calendar({
		ampm: false,
		type: 'date',
	// maxDate: tanggal,
		formatter: {
			date: function (date, settings) {
				if (!date) return '';
				let momentDate = moment(date)
				return momentDate.format('YYYY-MM-DD')
			}
		}
	});


	$(document).on('click', '.simpan', function(e){
		var formDom = "dataForm";
		if($(this).data("form") !== undefined){
			formDom = $(this).data('form');
		}
		swal({
			title: 'Apakah Anda Yakin?',
				text: "Data yang sudah diupload, tidak dapat diubah!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Simpan',
				cancelButtonText: 'Batal',
				reverseButtons: true
			}).then((result) => {
				if (result) {
					saveForm(formDom);
				}
			})
	});

	function saveForm(form="dataForm")
	{
		if($("#"+form).form('is valid')){
			$('#formModal').find('.loading.dimmer').addClass('active');
			$('#cover').show();
			$("#"+form).ajaxSubmit({
				success: function(resp){
					console.log('nice');
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							if(postSave(resp)){
								return true;
							}else{
								$('#cover').hide();
								window.location = "{{ url($pageUrl) }}";
								return true;
							}
							$('#formModal').find('.loading.dimmer').removeClass('active');
						})
					},
					error: function(resp){
						if (resp.responseJSON.status !== "undefined" && resp.responseJSON.status === 'false'){
							swal(
								'Oops, Maaf!',
								resp.responseJSON.message,
								'error'
								)
						}

						$('#cover').hide();
						var response = resp.responseJSON;
						$.each(response.errors, function(index, val) {
							clearFormError(index,val);
							showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
						},1000)
						$('#formModal').find('.loading.dimmer').removeClass('active');
					}
				});
		}else{
			$("#"+form).ajaxSubmit({
				success: function(resp){
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							if(postSave(resp)){
								return true;
							}else{
								$('#cover').hide();
								window.location = "{{ url($pageUrl) }}";
								return true;
							}
						})
					},
					error: function(resp){
						$.each(resp.responseJSON, function(index, val) {
							clearFormError(index,val);
							showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.message').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
						},1000)
					}
				});
		}
	}
</script>