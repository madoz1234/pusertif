@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
		/*.disposisi.dropdown .menu .item:nth-child(1) {
			background-color: #00dd00cc;
			color: #fff;
		}
		.disposisi.dropdown .menu .item:nth-child(2) {
			background-color: #dd0000cc;
			color: #fff;
		}*/
	</style>
@append
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Penyelesaian Pengujian</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$data->id)."/savePenyelesaian" }}" method="POST">
      {!! csrf_field() !!}
      <input type="hidden" name="_method" value="PUT">
      <input type="hidden" name="id" value="{{ $data->id }}">
		<div class="ui form">
			<table class="ui compact table" style="border-radius: 0; margin: 0">
				<tr>
					<td><label>Hasil </label></td>
					<td>:</td>
					<td>
						<div class="field">
							<select name="hasil" id="hasil" class="watcher ui fluid search dropdown">
								<option value="">Pilih Salah Satu</option>
								<option value="1">Diterima</option>
								<option value="2">Ditolak</option>
							</select>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="ui divider"></div>
		<div class="actions">
			<div class="ui black deny button" style="background-color: #363636;margin-left: -1px;">
				Batal
			</div>
			<div class="ui right floated simpan button" style="background-color: #00AEEF;">
				Submit
			</div>
		</div>
	</form>
</div>
@section('init-modal')
    <script>
    	onShow = function(){
    		$('.ui.watcher.dropdown').css({
    			'width': '250px'
    		});
    		$('.watcher').dropdown({
	    	});
    	};
    </script>
@endsection

<script type="text/javascript">
	$(document).on('click', '.simpan', function(e){
		var formDom = "dataForm";
		if($(this).data("form") !== undefined){
			formDom = $(this).data('form');
		}
		swal({
			title: 'Apakah Anda Yakin?',
				text: "Data yang sudah diupload, tidak dapat diubah!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Simpan',
				cancelButtonText: 'Batal',
				reverseButtons: true
			}).then((result) => {
				if (result) {
					saveForm(formDom);
				}
			})
	});

	function saveForm(form="dataForm")
	{
		if($("#"+form).form('is valid')){
			$('#formModal').find('.loading.dimmer').addClass('active');
			$('#cover').show();
			$("#"+form).ajaxSubmit({
				success: function(resp){
					console.log('nice');
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							if(postSave(resp)){
								return true;
							}else{
								$('#cover').hide();
								window.location.reload();
								return true;
							}
							$('#formModal').find('.loading.dimmer').removeClass('active');
						})
					},
					error: function(resp){
						if (resp.responseJSON.status !== "undefined" && resp.responseJSON.status === 'false'){
							swal(
								'Oops, Maaf!',
								resp.responseJSON.message,
								'error'
								)
						}

						$('#cover').hide();
						var response = resp.responseJSON;
						$.each(response.errors, function(index, val) {
							clearFormError(index,val);
							showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
						},1000)
						$('#formModal').find('.loading.dimmer').removeClass('active');
					}
				});
		}else{
			$("#"+form).ajaxSubmit({
				success: function(resp){
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							if(postSave(resp)){
								return true;
							}else{
								$('#cover').hide();
								window.location.reload();
								return true;
							}
						})
					},
					error: function(resp){
						$.each(resp.responseJSON, function(index, val) {
							clearFormError(index,val);
							showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.message').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
						},1000)
					}
				});
		}
	}
</script>
