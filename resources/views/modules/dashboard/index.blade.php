@extends('layouts.list')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/fullcalendar/scheduler.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
<script src="{{ asset('plugins/chartjs/Chart.min.js') }}"></script>
<script src="{{ asset('plugins/fullcalendar/moment.min.js') }}"></script>
{{-- <script src="{{ asset('plugins/fullcalendar/jquery.min.js') }}"></script> --}}
<script src="{{ asset('plugins/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="{{ asset('plugins/fullcalendar/scheduler.min.js') }}"></script>
<script src="{{ asset('plugins/fullcalendar/locale/id.js') }}"></script>

@append

@section('styles')
<style type="text/css">
  /*perusahaan*/
  .summary-panel#perusahaan{
    background: #04b8e0!important;
    position: relative;
    cursor: default;
  }
  .summary-panel#perusahaan:before {
    font-family: "Icons", sans-serif;
    font-size: 4rem;
    content: "\f1ad";
    position: absolute;
    bottom: 2.5rem;
    right: 1rem;
    color: rgba(255,255,255,.25);
    transition: all .5s ease-in-out;
  }
  .summary-panel#perusahaan:hover:before{
    font-size: 6rem;
    bottom: 2.75rem;
  }
  /*pelanggan*/
  .summary-panel#pelanggan{
    background: #05c610!important;
    position: relative;
    cursor: default;
  }
  .summary-panel#pelanggan:before {
    font-family: "Icons", sans-serif;
    font-size: 5rem;
    content: "\f15b";
    position: absolute;
    bottom: 2.5rem;
    right: 1rem;
    color: rgba(255,255,255,.25);
    transition: all .5s ease-in-out;
  }
  .summary-panel#pelanggan:hover:before{
    font-size: 6rem;
    bottom: 2.75rem;
  }
  /*layanan*/
  .summary-panel#layanan{
    background: #f2711c!important;
    position: relative;
    cursor: default;
  }
  .summary-panel#layanan:before {
    font-family: "Icons", sans-serif;
    font-size: 4rem;
    content: "\f0c0";
    position: absolute;
    bottom: 2.5rem;
    right: 1rem;
    color: rgba(255,255,255,.25);
    transition: all .5s ease-in-out;
  }
  .summary-panel#layanan:hover:before{
    font-size: 6rem;
    bottom: 2.75rem;
  }

  /*pengujian*/
  .summary-panel#pengujian{
    background: #eda200!important;
    position: relative;
    cursor: default;
  }
  .summary-panel#pengujian:before {
    font-family: "Icons", sans-serif;
    font-size: 4rem;
    content: "\f0e4";
    position: absolute;
    bottom: 2.5rem;
    right: 1rem;
    color: rgba(255,255,255,.25);
    transition: all .5s ease-in-out;
  }
  .summary-panel#pengujian:hover:before{
    font-size: 6rem;
    bottom: 2.75rem;
  }

  .summary-panel#perusahaan>.statistic>.value,
  .summary-panel#pelanggan>.statistic>.value,
  .summary-panel#pengujian>.statistic>.value,
  .summary-panel#layanan>.statistic>.value {
    color: rgba(255,255,255,.9)!important
  }
  .summary-panel>.statistic>.label {
    color: rgba(255,255,255,.8)!important;
    font-weight: 300;
    font-size: 1.5rem;
    padding: 20px;
  }
</style>
@append

@section('scripts')
<script>
  $( document ).ready(function() {
  	$('.search.dropdown').css({
          'width': '400px'
    });
    $(document).on('click', '.filter.button', function(event) {
		event.preventDefault();
		$('#listCalendar').fullCalendar('destroy');
		loadJadwal();
	});

	$(document).on('click', '.reset.button', function(event) {
		event.preventDefault();
		$('#listCalendar').fullCalendar('destroy');
		$("select[name=perusahaan_id]").val('')
		loadJadwal();
	});

    loadJadwal();

    var kpj;
    var fpk;
    var bendauji;

    $.ajax({
      url: '{{ url('ajax/option/chart-kaji') }}',
      type: 'GET',
    })
    .done(function(response) {
      kpj = displayChart($('#kpj-chart'),response.selesai,response.belum);
    });

    $.ajax({
      url: '{{ url('ajax/option/chart-pengujian') }}',
      type: 'GET',
    })
    .done(function(response) {
      fpk = displayChart($('#fpk-chart'),response.selesai,response.belum);
    });

    $.ajax({
      url: '{{ url('ajax/option/chart-laporan') }}',
      type: 'GET',
    })
    .done(function(response) {
      bendauji = displayChart($('#bendauji-chart'),response.selesai,response.belum);
    });


  });

  function displayChart(element,selesai,belum){
    var data = {
      labels: [
          "Selesai",
          "Belum",
      ],
      datasets: [
          {
              data: [selesai, belum],
              backgroundColor: [
                  "orange",
                  "lightgray",
              ],
          }]
    };

    new Chart(element, {
        type: 'doughnut',
        data: data,
        options: {
          rotation: 1 * Math.PI,
          circumference: 1 * Math.PI,
          legend: {
              display: false
          },
        },
        plugins: [{
          afterDraw: function(chart) {
            var width = chart.chart.width,
                height = chart.chart.height,
                ctx = chart.chart.ctx;

            ctx.restore();
            var fontSize = (height / 100).toFixed(2);
            ctx.font = fontSize + "em Times New Roman";
            ctx.textBaseline = "middle";
            ctx.textAlign = "center";
            ctx.fillStyle = 'black';
            var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
            var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 1.5);

            var text = selesai+"/"+(selesai+belum);
                // textX = Math.round((width - ctx.measureText(text).width) / 2),
                // textY = (height - ((height/2)/2))-10;

            ctx.fillText(text, centerX, centerY);
            ctx.fillText('selesai', centerX, centerY+20);
            ctx.save();
          }
        }]
    });
  }

  function displayJadwal(resource,jadwal){
  	$('.loading.dimmer').removeClass('active');
    $('#listCalendar').fullCalendar({
      locale: 'id',
      contentHeight: 400,
      plugins: [ 'resourceTimeline' ],
      header: {
        left: 'today prev,next',
        center: 'title',
      },
      defaultView: 'timelineMonth',
      resourceGroupField: 'building',
      resourceColumns: [
        {
          labelText: 'Peminta Jasa',
          field: 'title'
        },
      ],
      resources: resource,
      events: jadwal,
      schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
      viewRender: function(view, element) {
        //note: this is a hack, i don't know why the view title keep showing "undefined" text in it.
        //probably bugs in jquery fulllistCalendar
        $('.fc-center')[0].children[0].innerText = view.title.replace(new RegExp("undefined", 'g'), ""); ;
      },
    });
  }


  function loadJadwal(){
    $.ajax({
      url: '{{ url('ajax/option/jadwal-kaji') }}',
      type: 'POST',
      data: {
        _token: "{{ csrf_token() }}",
        perusahaan_id: $('select[name=perusahaan_id]').val(),
      },
    })
    .done(function(response) {
    	console.log(response.jenis, response.jadwal)
      displayJadwal(response.jenis,response.jadwal)
    });
  }
</script>
@endsection

@section('filters')
@section('toolbars')  
@show
@endsection

@section('content')

<div class="ui grid">
  <div class="sixteen wide column">
    <div class="ui three cards">
      <div class="ui raised card">
        <div class="content">
          <div class="header" align="center">KAJI ULANG</div>
          <canvas id="kpj-chart" width="200" height="70"></canvas>
        </div>
      </div>
      <div class="ui raised card">
        <div class="content">
          <div class="header" align="center">PENGUJIAN</div>
          <canvas id="fpk-chart" width="200" height="70"></canvas>
        </div>
      </div>

      <div class="ui raised card">
        <div class="content">
          <div class="header" align="center">LAPORAN</div>
          <canvas id="bendauji-chart" width="200" height="70"></canvas>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="ui grid">
  <div class="four wide column">
    <div class="ui left aligned segment summary-panel" id="perusahaan">
      <div class="ui teal statistic">
        <div class="label">
          NO ORDER
        </div>
      </div>
    </div>
  </div>

  <div class="four wide column">
    <div class="ui left aligned segment summary-panel" id="pelanggan">
      <div class="ui yellow statistic">
        <div class="label">
          NO WBS/IO
        </div>
      </div>
    </div>
  </div>

  <div class="four wide column">
    <div class="ui left aligned segment summary-panel" id="layanan">
      <div class="ui orange statistic">
        <div class="label">
          NO FPP/FPK
        </div>
      </div>
    </div>
  </div>

  <div class="four wide column">
    <div class="ui left aligned segment summary-panel" id="pengujian">
      <div class="ui orange statistic">
        <div class="label">
          NO LAPORAN
        </div>
      </div>
    </div>
  </div>
</div>

<div class="ui grid">

  <div class="ui segment">
  	<form class="ui filter form">
  		<div class="header" align="center" style="font-size: 18px;font-weight: bold;">Jadwal Pengujian Tentative<br> <label style="font-size: 12px;">( Sudah Melewati Kaji Ulang)</label style="font-size: 12px;"></div>
        <div class="inline fields">
        	<label><i class="filter circular inverted teal icon"></i> FILTER :</label>
			<div class="field">
				<select name="perusahaan_id" class="watcher ui fluid search dropdown">
					{!!
						\App\Models\Master\Perusahaan::optionsa('nama', 'id', ['filters' => [function($q){
							return $q->where('status', 1);
						}],
						'selected' => old('id')
					], 'Peminta Jasa')
					!!}
				</select>
			</div>
			<button type="button" class="ui teal icon filter button" data-content="{{trans('translator.Cari Data')}}">
				<i class="search icon"></i>
			</button>
			<button type="reset" class="ui icon reset button" data-content="{{trans('translator.Bersihkan Pencarian')}}">
				<i class="refresh icon"></i>
			</button>
        </div>
    </form>
    <div class="four wide column">
      <div id='listCalendar'></div>
    </div>
  </div>
</div>
@endsection