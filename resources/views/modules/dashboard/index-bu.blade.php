@extends('layouts.list')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/fullcalendar/scheduler.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
<script src="{{ asset('plugins/chartjs/Chart.min.js') }}"></script>
<script src="{{ asset('plugins/fullcalendar/moment.min.js') }}"></script>
{{-- <script src="{{ asset('plugins/fullcalendar/jquery.min.js') }}"></script> --}}
<script src="{{ asset('plugins/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="{{ asset('plugins/fullcalendar/scheduler.min.js') }}"></script>
@append

@section('styles')
<style type="text/css">
/*perusahaan*/
.summary-panel#perusahaan{
    background: #04b8e0!important;
    position: relative;
    cursor: default;
}
.summary-panel#perusahaan:before {
    font-family: "Icons", sans-serif;
    font-size: 4rem;
    content: "\f1ad";
    position: absolute;
    bottom: 2.5rem;
    right: 1rem;
    color: rgba(255,255,255,.25);
    transition: all .5s ease-in-out;
}
.summary-panel#perusahaan:hover:before{
    font-size: 6rem;
    bottom: 2.75rem;
}
/*pelanggan*/
.summary-panel#pelanggan{
    background: #05c610!important;
    position: relative;
    cursor: default;
}
.summary-panel#pelanggan:before {
    font-family: "Icons", sans-serif;
    font-size: 5rem;
    content: "\f15b";
    position: absolute;
    bottom: 2.5rem;
    right: 1rem;
    color: rgba(255,255,255,.25);
    transition: all .5s ease-in-out;
}
.summary-panel#pelanggan:hover:before{
    font-size: 6rem;
    bottom: 2.75rem;
}
/*layanan*/
.summary-panel#layanan{
    background: #f2711c!important;
    position: relative;
    cursor: default;
}
.summary-panel#layanan:before {
    font-family: "Icons", sans-serif;
    font-size: 4rem;
    content: "\f0c0";
    position: absolute;
    bottom: 2.5rem;
    right: 1rem;
    color: rgba(255,255,255,.25);
    transition: all .5s ease-in-out;
}
.summary-panel#layanan:hover:before{
    font-size: 6rem;
    bottom: 2.75rem;
}

/*pengujian*/
.summary-panel#pengujian{
    background: #eda200!important;
    position: relative;
    cursor: default;
}
.summary-panel#pengujian:before {
    font-family: "Icons", sans-serif;
    font-size: 4rem;
    content: "\f0e4";
    position: absolute;
    bottom: 2.5rem;
    right: 1rem;
    color: rgba(255,255,255,.25);
    transition: all .5s ease-in-out;
}
.summary-panel#pengujian:hover:before{
    font-size: 6rem;
    bottom: 2.75rem;
}

.summary-panel#perusahaan>.statistic>.value,
.summary-panel#pelanggan>.statistic>.value,
.summary-panel#pengujian>.statistic>.value,
.summary-panel#layanan>.statistic>.value {
    color: rgba(255,255,255,.9)!important
}
.summary-panel>.statistic>.label {
    color: rgba(255,255,255,.8)!important;
    font-weight: 300;
    font-size: 1.5rem;
    padding: 20px;
}
</style>
@append

@section('scripts')
<script>
    $(document).ready(function($){
//Calender
$('.ui.calendar').calendar({
    type: 'month'
});

//Kpj
 Chart.defaults.doughnutLabels = Chart.helpers.clone(Chart.defaults.doughnut);

 var helpers = Chart.helpers;
 var defaults = Chart.defaults;

 Chart.controllers.doughnutLabels = Chart.controllers.doughnut.extend({
   updateElement: function(arc, index, reset) {
     var _this = this;
     var chart = _this.chart,
       chartArea = chart.chartArea,
       opts = chart.options,
       animationOpts = opts.animation,
       arcOpts = opts.elements.arc,
       centerX = (chartArea.left + chartArea.right) / 2,
       centerY = (chartArea.top + chartArea.bottom) / 2,
       startAngle = opts.rotation, // non reset case handled later
       endAngle = opts.rotation, // non reset case handled later
       dataset = _this.getDataset(),
       circumference = reset && animationOpts.animateRotate ? 0 : arc.hidden ? 0 : _this.calculateCircumference(dataset.data[index]) * (opts.circumference / (2.0 * Math.PI)),
       innerRadius = reset && animationOpts.animateScale ? 0 : _this.innerRadius,
       outerRadius = reset && animationOpts.animateScale ? 0 : _this.outerRadius,
       custom = arc.custom || {},
       valueAtIndexOrDefault = helpers.getValueAtIndexOrDefault;

     helpers.extend(arc, {
       // Utility
       _datasetIndex: _this.index,
       _index: index,

       // Desired view properties
       _model: {
         x: centerX + chart.offsetX,
         y: centerY + chart.offsetY,
         startAngle: startAngle,
         endAngle: endAngle,
         circumference: circumference,
         outerRadius: outerRadius,
         innerRadius: innerRadius,
         label: valueAtIndexOrDefault(dataset.label, index, chart.data.labels[index])
       },

       draw: function() {
         var ctx = this._chart.ctx,
           vm = this._view,
           sA = vm.startAngle,
           eA = vm.endAngle,
           opts = this._chart.config.options;

         var labelPos = this.tooltipPosition();
         var segmentLabel = vm.circumference / opts.circumference * 100;

         ctx.beginPath();

         ctx.arc(vm.x, vm.y, vm.outerRadius, sA, eA);
         ctx.arc(vm.x, vm.y, vm.innerRadius, eA, sA, true);

         ctx.closePath();
         ctx.strokeStyle = vm.borderColor;
         ctx.lineWidth = vm.borderWidth;

         ctx.fillStyle = vm.backgroundColor;

         ctx.fill();
         ctx.lineJoin = 'bevel';

         if (vm.borderWidth) {
           ctx.stroke();
         }

         if (vm.circumference > 0.0015) { // Trying to hide label when it doesn't fit in segment
           ctx.beginPath();
           ctx.font = helpers.fontString('30', opts.defaultFontStyle, opts.defaultFontFamily);
           ctx.fillStyle = "#190707";
           ctx.textBaseline = "top";
           ctx.textAlign = "center";

           // Round percentage in a way that it always adds up to 100%
           /* ctx.fillText(segmentLabel.toFixed(2) + "%", labelPos.x, labelPos.y) */;

         }
         //display in the center the total sum of all segments
         var total = dataset.data.reduce((sum, val) => sum + val, 0);
         //ctx.fillText('Total = ' + total, vm.x, vm.y - 20, 200);
         ctx.fillText('2/6', vm.x, vm.y - 70, 200);
         ctx.fillText('Selesai', vm.x, vm.y - 40, 200);
         ctx.fillText('0', vm.x - 200, vm.y);
         ctx.fillText('100', vm.x + 200, vm.y);
       }
     });

     var model = arc._model;
     model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : valueAtIndexOrDefault(dataset.backgroundColor, index, arcOpts.backgroundColor);
     model.hoverBackgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : valueAtIndexOrDefault(dataset.hoverBackgroundColor, index, arcOpts.hoverBackgroundColor);
     model.borderWidth = custom.borderWidth ? custom.borderWidth : valueAtIndexOrDefault(dataset.borderWidth, index, arcOpts.borderWidth);
     model.borderColor = custom.borderColor ? custom.borderColor : valueAtIndexOrDefault(dataset.borderColor, index, arcOpts.borderColor);

     // Set correct angles if not resetting
     if (!reset || !animationOpts.animateRotate) {
       if (index === 0) {
         model.startAngle = opts.rotation;
       } else {
         model.startAngle = _this.getMeta().data[index - 1]._model.endAngle;
       }

       model.endAngle = model.startAngle + model.circumference;
     }

     arc.pivot();
   }
 });

 var config = {
   type: 'doughnutLabels',
   data: {
     datasets: [{
       data: [
         2,
         4,
       ],
       backgroundColor: [
         "#ef5f10",
         "#d4d4d6"
       ]
     }],
     labels: [
       "Selesai",
       "Belum"
     ]
   },
   options: {
     circumference: Math.PI,
     rotation: 1.0 * Math.PI,
     responsive: true,
     legend: {
       display: false,
     },
     /* title: {
       display: true,
       text: 'KPJ'
     }, */
     animation: {
       animateScale: true,
       animateRotate: true
     },
     tooltips: {
       callbacks: {
         label: function(tooltipItem, data) {

           var dataset = data.datasets[tooltipItem.datasetIndex];
           var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
             return previousValue + currentValue;
           });
           var currentValue = dataset.data[tooltipItem.index];
           //var precentage = Math.floor(((currentValue / total) * 100) + 0.5);
           /* return window.upDownChart.data.labels[tooltipItem.index] + " " + currentValue + "hr" */;
           return currentValue + " " + window.upDownChart.data.labels[tooltipItem.index];

         }
       }
     }
   }
 };

 var ctx = document.getElementById("kpj-chart").getContext("2d");
 window.upDownChart = new Chart(ctx, config);


//Fpk
 Chart.defaults.doughnutLabels = Chart.helpers.clone(Chart.defaults.doughnut);

 var helpers = Chart.helpers;
 var defaults = Chart.defaults;

 Chart.controllers.doughnutLabels = Chart.controllers.doughnut.extend({
   updateElement: function(arc, index, reset) {
     var _this = this;
     var chart = _this.chart,
       chartArea = chart.chartArea,
       opts = chart.options,
       animationOpts = opts.animation,
       arcOpts = opts.elements.arc,
       centerX = (chartArea.left + chartArea.right) / 2,
       centerY = (chartArea.top + chartArea.bottom) / 2,
       startAngle = opts.rotation, // non reset case handled later
       endAngle = opts.rotation, // non reset case handled later
       dataset = _this.getDataset(),
       circumference = reset && animationOpts.animateRotate ? 0 : arc.hidden ? 0 : _this.calculateCircumference(dataset.data[index]) * (opts.circumference / (2.0 * Math.PI)),
       innerRadius = reset && animationOpts.animateScale ? 0 : _this.innerRadius,
       outerRadius = reset && animationOpts.animateScale ? 0 : _this.outerRadius,
       custom = arc.custom || {},
       valueAtIndexOrDefault = helpers.getValueAtIndexOrDefault;

     helpers.extend(arc, {
       // Utility
       _datasetIndex: _this.index,
       _index: index,

       // Desired view properties
       _model: {
         x: centerX + chart.offsetX,
         y: centerY + chart.offsetY,
         startAngle: startAngle,
         endAngle: endAngle,
         circumference: circumference,
         outerRadius: outerRadius,
         innerRadius: innerRadius,
         label: valueAtIndexOrDefault(dataset.label, index, chart.data.labels[index])
       },

       draw: function() {
         var ctx = this._chart.ctx,
           vm = this._view,
           sA = vm.startAngle,
           eA = vm.endAngle,
           opts = this._chart.config.options;

         var labelPos = this.tooltipPosition();
         var segmentLabel = vm.circumference / opts.circumference * 100;

         ctx.beginPath();

         ctx.arc(vm.x, vm.y, vm.outerRadius, sA, eA);
         ctx.arc(vm.x, vm.y, vm.innerRadius, eA, sA, true);

         ctx.closePath();
         ctx.strokeStyle = vm.borderColor;
         ctx.lineWidth = vm.borderWidth;

         ctx.fillStyle = vm.backgroundColor;

         ctx.fill();
         ctx.lineJoin = 'bevel';

         if (vm.borderWidth) {
           ctx.stroke();
         }

         if (vm.circumference > 0.0015) { // Trying to hide label when it doesn't fit in segment
           ctx.beginPath();
           ctx.font = helpers.fontString('30', opts.defaultFontStyle, opts.defaultFontFamily);
           ctx.fillStyle = "#190707";
           ctx.textBaseline = "top";
           ctx.textAlign = "center";

           // Round percentage in a way that it always adds up to 100%
           /* ctx.fillText(segmentLabel.toFixed(2) + "%", labelPos.x, labelPos.y) */;

         }
         //display in the center the total sum of all segments
         var total = dataset.data.reduce((sum, val) => sum + val, 0);
         //ctx.fillText('Total = ' + total, vm.x, vm.y - 20, 200);
         ctx.fillText('6/11', vm.x, vm.y - 70, 200);
         ctx.fillText('Selesai', vm.x, vm.y - 40, 200);
         ctx.fillText('0', vm.x - 200, vm.y);
         ctx.fillText('100', vm.x + 200, vm.y);
       }
     });

     var model = arc._model;
     model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : valueAtIndexOrDefault(dataset.backgroundColor, index, arcOpts.backgroundColor);
     model.hoverBackgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : valueAtIndexOrDefault(dataset.hoverBackgroundColor, index, arcOpts.hoverBackgroundColor);
     model.borderWidth = custom.borderWidth ? custom.borderWidth : valueAtIndexOrDefault(dataset.borderWidth, index, arcOpts.borderWidth);
     model.borderColor = custom.borderColor ? custom.borderColor : valueAtIndexOrDefault(dataset.borderColor, index, arcOpts.borderColor);

     // Set correct angles if not resetting
     if (!reset || !animationOpts.animateRotate) {
       if (index === 0) {
         model.startAngle = opts.rotation;
       } else {
         model.startAngle = _this.getMeta().data[index - 1]._model.endAngle;
       }

       model.endAngle = model.startAngle + model.circumference;
     }

     arc.pivot();
   }
 });

 var config = {
   type: 'doughnutLabels',
   data: {
     datasets: [{
       data: [
         6,
         11,
       ],
       backgroundColor: [
         "#d9e80b",
         "#d4d4d6"
       ]
     }],
     labels: [
       "Selesai",
       "Belum"
     ]
   },
   options: {
     circumference: Math.PI,
     rotation: 1.0 * Math.PI,
     responsive: true,
     legend: {
       display: false,
     },
     /* title: {
       display: true,
       text: 'KPJ'
     }, */
     animation: {
       animateScale: true,
       animateRotate: true
     },
     tooltips: {
       callbacks: {
         label: function(tooltipItem, data) {

           var dataset = data.datasets[tooltipItem.datasetIndex];
           var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
             return previousValue + currentValue;
           });
           var currentValue = dataset.data[tooltipItem.index];
           //var precentage = Math.floor(((currentValue / total) * 100) + 0.5);
           /* return window.upDownChart.data.labels[tooltipItem.index] + " " + currentValue + "hr" */;
           return currentValue + " " + window.upDownChart.data.labels[tooltipItem.index];

         }
       }
     }
   }
 };

 var ctx = document.getElementById("fpk-chart").getContext("2d");
 window.upDownChart = new Chart(ctx, config);

//Bendauji
 Chart.defaults.doughnutLabels = Chart.helpers.clone(Chart.defaults.doughnut);

 var helpers = Chart.helpers;
 var defaults = Chart.defaults;

 Chart.controllers.doughnutLabels = Chart.controllers.doughnut.extend({
   updateElement: function(arc, index, reset) {
     var _this = this;
     var chart = _this.chart,
       chartArea = chart.chartArea,
       opts = chart.options,
       animationOpts = opts.animation,
       arcOpts = opts.elements.arc,
       centerX = (chartArea.left + chartArea.right) / 2,
       centerY = (chartArea.top + chartArea.bottom) / 2,
       startAngle = opts.rotation, // non reset case handled later
       endAngle = opts.rotation, // non reset case handled later
       dataset = _this.getDataset(),
       circumference = reset && animationOpts.animateRotate ? 0 : arc.hidden ? 0 : _this.calculateCircumference(dataset.data[index]) * (opts.circumference / (2.0 * Math.PI)),
       innerRadius = reset && animationOpts.animateScale ? 0 : _this.innerRadius,
       outerRadius = reset && animationOpts.animateScale ? 0 : _this.outerRadius,
       custom = arc.custom || {},
       valueAtIndexOrDefault = helpers.getValueAtIndexOrDefault;

     helpers.extend(arc, {
       // Utility
       _datasetIndex: _this.index,
       _index: index,

       // Desired view properties
       _model: {
         x: centerX + chart.offsetX,
         y: centerY + chart.offsetY,
         startAngle: startAngle,
         endAngle: endAngle,
         circumference: circumference,
         outerRadius: outerRadius,
         innerRadius: innerRadius,
         label: valueAtIndexOrDefault(dataset.label, index, chart.data.labels[index])
       },

       draw: function() {
         var ctx = this._chart.ctx,
           vm = this._view,
           sA = vm.startAngle,
           eA = vm.endAngle,
           opts = this._chart.config.options;

         var labelPos = this.tooltipPosition();
         var segmentLabel = vm.circumference / opts.circumference * 100;

         ctx.beginPath();

         ctx.arc(vm.x, vm.y, vm.outerRadius, sA, eA);
         ctx.arc(vm.x, vm.y, vm.innerRadius, eA, sA, true);

         ctx.closePath();
         ctx.strokeStyle = vm.borderColor;
         ctx.lineWidth = vm.borderWidth;

         ctx.fillStyle = vm.backgroundColor;

         ctx.fill();
         ctx.lineJoin = 'bevel';

         if (vm.borderWidth) {
           ctx.stroke();
         }

         if (vm.circumference > 0.0015) { // Trying to hide label when it doesn't fit in segment
           ctx.beginPath();
           ctx.font = helpers.fontString('30', opts.defaultFontStyle, opts.defaultFontFamily);
           ctx.fillStyle = "#190707";
           ctx.textBaseline = "top";
           ctx.textAlign = "center";

           // Round percentage in a way that it always adds up to 100%
           /* ctx.fillText(segmentLabel.toFixed(2) + "%", labelPos.x, labelPos.y) */;

         }
         //display in the center the total sum of all segments
         var total = dataset.data.reduce((sum, val) => sum + val, 0);
         //ctx.fillText('Total = ' + total, vm.x, vm.y - 20, 200);
         ctx.fillText('10/29', vm.x, vm.y - 70, 200);
         ctx.fillText('Selesai', vm.x, vm.y - 40, 200);
         ctx.fillText('0', vm.x - 200, vm.y);
         ctx.fillText('100', vm.x + 200, vm.y);
       }
     });

     var model = arc._model;
     model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : valueAtIndexOrDefault(dataset.backgroundColor, index, arcOpts.backgroundColor);
     model.hoverBackgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : valueAtIndexOrDefault(dataset.hoverBackgroundColor, index, arcOpts.hoverBackgroundColor);
     model.borderWidth = custom.borderWidth ? custom.borderWidth : valueAtIndexOrDefault(dataset.borderWidth, index, arcOpts.borderWidth);
     model.borderColor = custom.borderColor ? custom.borderColor : valueAtIndexOrDefault(dataset.borderColor, index, arcOpts.borderColor);

     // Set correct angles if not resetting
     if (!reset || !animationOpts.animateRotate) {
       if (index === 0) {
         model.startAngle = opts.rotation;
       } else {
         model.startAngle = _this.getMeta().data[index - 1]._model.endAngle;
       }

       model.endAngle = model.startAngle + model.circumference;
     }

     arc.pivot();
   }
 });

 var config = {
   type: 'doughnutLabels',
   data: {
     datasets: [{
       data: [
         10,
         29,
       ],
       backgroundColor: [
         "#d84c06",
         "#d4d4d6"
       ]
     }],
     labels: [
       "Selesai",
       "Belum"
     ]
   },
   options: {
     circumference: Math.PI,
     rotation: 1.0 * Math.PI,
     responsive: true,
     legend: {
       display: false,
     },
     /* title: {
       display: true,
       text: 'KPJ'
     }, */
     animation: {
       animateScale: true,
       animateRotate: true
     },
     tooltips: {
       callbacks: {
         label: function(tooltipItem, data) {

           var dataset = data.datasets[tooltipItem.datasetIndex];
           var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
             return previousValue + currentValue;
           });
           var currentValue = dataset.data[tooltipItem.index];
           //var precentage = Math.floor(((currentValue / total) * 100) + 0.5);
           /* return window.upDownChart.data.labels[tooltipItem.index] + " " + currentValue + "hr" */;
           return currentValue + " " + window.upDownChart.data.labels[tooltipItem.index];

         }
       }
     }
   }
 };

 var ctx = document.getElementById("bendauji-chart").getContext("2d");
 window.upDownChart = new Chart(ctx, config);
});

$( document ).ready(function() {
    loadJadwal();
  });

function displayJadwal(resource,jadwal){
    $('#calendar').fullCalendar({
      contentHeight: 400,
        header: {
          left: 'today prev,next',
          center: 'title',
        },
        defaultView: 'timelineMonth',
        resourceColumns: [
          {
            labelText: 'Jenis Pengujian',
            field: 'title'
          },
        ],
        resources: resource,
        events: jadwal
      });
  }

function loadJadwal(){
    $.ajax({
      url: '{{ url('ajax/option/jadwal-kaji') }}',
      type: 'POST',
      data: {
        _token: "{{ csrf_token() }}",
      },
    })
    .done(function(response) {
      displayJadwal(response.jenis,response.jadwal)
    });
  }
</script>
@endsection

@section('filters')
@section('toolbars')  
@show
@endsection

@section('content')

<div class="ui grid">
    <div class="sixteen wide column">
        <div class="ui three cards">
            <div class="ui raised card">
                <div class="content">
                    <div class="header" align="center">KAJI ULANG</div>
                        <canvas id="kpj-chart" width="200" height="70"></canvas>
                </div>
            </div>
            <div class="ui raised card">
                <div class="content">
                    <div class="header" align="center">PENGUJIAN</div>
                        <canvas id="fpk-chart" width="200" height="70"></canvas>
                </div>
            </div>

            <div class="ui raised card">
                <div class="content">
                    <div class="header" align="center">LAPORAN</div>
                        <canvas id="bendauji-chart" width="200" height="70"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ui grid">
    <div class="four wide column">
        <div class="ui left aligned segment summary-panel" id="perusahaan">
            <div class="ui teal statistic">
                <div class="label">
                    NO ORDER
                </div>
            </div>
        </div>
    </div>

    <div class="four wide column">
        <div class="ui left aligned segment summary-panel" id="pelanggan">
            <div class="ui yellow statistic">
                <div class="label">
                    NO WBS/IO
                </div>
            </div>
        </div>
    </div>

    <div class="four wide column">
        <div class="ui left aligned segment summary-panel" id="layanan">
            <div class="ui orange statistic">
                <div class="label">
                    NO FPP/FPK
                </div>
            </div>
        </div>
    </div>

    <div class="four wide column">
        <div class="ui left aligned segment summary-panel" id="pengujian">
            <div class="ui orange statistic">
                <div class="label">
                    NO LAPORAN
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ui grid">
    <div class="ui segment">
        <div class="four wide column">
            <div id='calendar'></div>
        </div>
    </div>
</div>

@endsection