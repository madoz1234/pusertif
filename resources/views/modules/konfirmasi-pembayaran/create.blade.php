@extends('layouts.form')

@section('styles')
<style type="text/css">
	.responsive.table{
		width: 100%;
		overflow-x: auto;
	}
	.jus{
		text-align: justify;
		text-justify: inter-word;
	}
	.fn{
		font-size: 12px;
	}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		judul: ['empty'],
	};
</script>
@endsection

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
{!! csrf_field() !!}
	<table id="listTable" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
		<thead>
			<tr class="middle aligned">
				<th width="10" class="center aligned fn">#</th>
				<th width="200" class="center aligned fn">Peminta Jasa</th>
				<th width="200" class="center aligned fn">Layanan / Lingkup</th>
				<th width="200" class="center aligned fn">Jenis Pengujian</th>
				<th width="200" class="center aligned fn">No WBS/IO</th>
				<th width="50" class="center aligned fn">No VA</th>
				<th width="50" class="center aligned fn">Nominal VA (RP)</th>
				<th width="100" class="center aligned fn">Bukti Pembayaran</th>
				<th width="100" class="center aligned fn">Tgl Pembayaran</th>
			</tr>
		</thead>
		<tbody class="detail fluid container detail">
			@foreach($data as $key => $row)
				<tr class="middle aligned">
					<td class="center aligned">{{$key+1}}</td>
					<td class="center aligned">
						@php
							$peminta_jasa = $row->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$row->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$row->surat->kaji_ulang->pp->user->pelanggans->no_hp;
						@endphp
						{!!$peminta_jasa!!}
					</td>
					<td class="center aligned">
						{{$row->surat->kaji_ulang->pp->pelayanan->nama.' / '.$row->surat->kaji_ulang->pp->lingkup->nama}}
					</td>
					<td>
						@php
						$jenis_pengujian = '';
		            	if($row->surat->kaji_ulang->pp->detail){
			            	$jenis_pengujian .= '<div class="ui bulleted list">';
			            	foreach ($row->surat->kaji_ulang->pp->detail as $key => $value) {
			            		$kaji_ulang = $row->surat->kaji_ulang;
			            		if($kaji_ulang){
			            			foreach ($kaji_ulang->detail as $kuy => $val) {
			            				if($val->jenis_id == $value->id){
						            		$jenis_pengujian .= '<div class="item"><a class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
			            				}
			            			}
			            		}else{
			            			$jenis_pengujian .= '<div class="item"><a class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
			            		}
			            	}
			            	$jenis_pengujian .='</div>';
		            	}
						@endphp
						{!! $jenis_pengujian !!}
					</td>
					<td>{{ $row->wbs_io ? $row->wbs_io : '-' }}</td>
					<td>{{ $row->no_va ? $row->no_va : '-' }}</td>
					<td class="right aligned">{{ number_format($row->nominal) }}</td>
					<td class="field">
						<input type="hidden" name="entry[{{$row->id}}][id_va]" value="{{$row->id}}">
						<div class="ui action choises input">
							<input type="text" id="label-attachment" readonly placeholder="Pilih File">
							<input type="file" id="attachment" name="entry[{{$row->id}}][bukti]" style="display: none!important;">
							<div class="ui icon button">
								<i class="cloud upload alternate icon"></i>
							</div>
						</div>
					</td>
					<td class="field">
						<div class="field">
							<div class="ui calendar labeled input tgl">
								<div class="ui input left icon">
									<i class="calendar icon date"></i>
									<input name="entry[{{$row->id}}][tgl_bayar]" type="text" placeholder="Tgl Pembayaran">
								</div>
							</div>
						</div>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</form>
<div class="actions">
	<div class="ui two column grid">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		<div class="right aligned column">
			<br>
			<button id="simpan" type="button" class="ui large positive right labeled icon save as page button">
				Simpan
				<i class="save icon"></i>
			</button>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.tgl').calendar({
		ampm: false,
		type: 'date',
			// maxDate: tanggal,
			formatter: {
				date: function (date, settings) {
					if (!date) return '';
					let momentDate = moment(date)
					return momentDate.format('YYYY-MM-DD')
				}
			}
		});

	$(".number").on("keypress keyup blur",function (e) {    
		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	});

</script>
@append