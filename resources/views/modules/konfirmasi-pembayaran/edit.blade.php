<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Konfirmasi
	<h5>No Order : {{ $record->surat->kaji_ulang->pp->no_order }}<br>
	No VA : {{ $record->no_va }} <br> Nominal VA : {{ rtrim(rtrim(number_format($record->nominal,2,',','.'), '0'), ',')}}</h5>
</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{$record->id}}">
		{!! csrf_field() !!}
			<div class="field">
				<label>Status</label>
				<select name="status" class="watcher ui fluid search dropdown">
					<option value="">Status</option>
				    <option value="0" @if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0) disabled @else selected @endif>Belum Bayar</option>
				    <option value="1" @if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0) disabled @else @endif>Kurang Bayar</option>
				    <option value="2" @if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0) disabled @else @endif>Lebih Bayar</option>
				    <option value="3" @if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0) selected @else @endif>Lunas</option>
				</select>
			</div>
			<div class="field">
	    		<label>Dana Masuk (Rp)</label>
	    		@if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0)
					<input type="text" class="dana" name="dana_masuk" placeholder="Dana Masuk" data-inputmask="'alias' : 'numeric'" value="{{ rtrim(rtrim(number_format($record->nominal,2,',','.'), '0'), ',') }}" readonly="">
				@else 
					<input type="text" class="dana" name="dana_masuk" placeholder="Dana Masuk" disabled="true" data-inputmask="'alias' : 'numeric'">
				@endif
	      	</div>
			<div class="appendField"></div>
		  	<div class="field">
				<label>Catatan</label>
				<textarea name="catatan" class="catatan" rows="3" placeholder="Catatan" @if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0) @else disabled="true" @endif></textarea>
			</div>
	      	<div class="field">
	    		<label>No WBS/IO</label>
				<input type="text" class="wbs" name="wbs_io" placeholder="No WBS/IO"  @if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0) @else disabled="true" @endif>
	      	</div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$('select[name=status]').on('change', function(){
		var status = this.value;
		var html = '';

		if(status == 1){
			$('.catatan').prop( "disabled", false );
        	$('.wbs').prop( "disabled", false );
        	$('.dana').prop( "disabled", false );
        	html = `<div class="field">
							<label>Status Kelengkapan Dokumen</label>
							<input type="text" name="status_kelengkapan" class="status_kelengkapan" placeholder="Status Kelengkapan Dokumen">
						</div>
						<div class="field">
							<label>Catatan Kelengkapan Dokumen</label>
							<textarea name="catatan_kelengkapan" rows="3" class="catatan_kelengkapan" placeholder="Catatan Kelengkapan Dokumen"></textarea>
						</div>`;
	    	$('.appendField').append(html);
		}else if (status == 2 || status == 3){
        	$('.catatan').prop( "disabled", false );
        	$('.wbs').prop( "disabled", false );
        	$('.dana').prop( "disabled", false );
	    	$('.appendField').empty();
        }else{
        	$('.catatan').prop( "disabled", true );
        	$('.wbs').prop( "disabled", true );
        	$('.dana').prop( "disabled", true );
	    	$('.appendField').empty();
        }
  	})

	$(document).on('keyup', '.status_kelengkapan', function (e){
		var catatanKelengkapan = $('.catatan_kelengkapan').val();

		if ($(this).val() != '' && catatanKelengkapan != '') {
        	$('.wbs').prop( "disabled", false );
		}else{
        	$('.wbs').prop( "disabled", true );
		}
	});

	$(document).on('keyup', '.catatan_kelengkapan', function (e){
		var statusKelengkapan = $('.status_kelengkapan').val();

		if ($(this).val() != '' && statusKelengkapan != '') {
        	$('.wbs').prop( "disabled", false );
		}else{
        	$('.wbs').prop( "disabled", true );
		}
	});

</script>
@include('scripts.inputmask')