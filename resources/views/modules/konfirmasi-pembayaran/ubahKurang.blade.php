<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Pelunasan
	<h5>No Order : {{ $record->surat->kaji_ulang->pp->no_order }}<br>
	No VA : {{ $record->no_va }} <br> Nominal VA : {{ rtrim(rtrim(number_format($record->nominal,2,',','.'), '0'), ',')}}<br>
	Dana yang Sudah Masuk : {{ rtrim(rtrim(number_format($record->konfirmasi->dana_masuk,2,',','.'), '0'), ',')}}<br>
	</h5>
</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id)."/saveKurang" }}" method="POST">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{$record->konfirmasi->id}}">
		{!! csrf_field() !!}
			<div class="field">
				<label>Status</label>
				<select name="status" class="watcher ui fluid search dropdown">
					<option value="">Status</option>
				    <option value="3">Lunas</option>
				</select>
			</div>
			<div class="field">
	    		<label>Dana Masuk (Rp)</label>
	    		<div class="ui labeled input">
	    			<div class="ui label">
	    				Rp.
	    			</div>
	    			<input type="text" style="text-align: right;" disabled="disabled" value="{{rtrim(rtrim(number_format($record->konfirmasi->dana_masuk,2,',','.'), '0'), ',')}}">
	    		</div>
	      	</div>
			<div class="field">
	    		<label>Dana Kurang (Rp)</label>
				<input type="text" class="dana" name="dana_kurang" placeholder="Dana Kurang" data-inputmask="'alias' : 'numeric'">
	      	</div>
			<div class="appendField"></div>
		  	<div class="field">
				<label>Catatan</label>
				<textarea name="catatan" class="catatan" rows="3" placeholder="Catatan"></textarea>
			</div>
	      	<div class="field">
	    		<label>No WBS/IO</label>
				<input type="text" class="wbs" name="wbs_io" placeholder="No WBS/IO"  value="{{$record->konfirmasi->wbs_io}}" readonly="">
	      	</div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	// $('select[name=status]').on('change', function(){
	// 	var status = this.value;
	// 	var html = '';

	// 	if(status == 1){
	// 		$('.catatan').prop( "disabled", false );
 //        	$('.wbs').prop( "disabled", true );
 //        	$('.dana').prop( "disabled", false );
 //        	html = `<div class="field">
	// 						<label>Status Kelengkapan Dokumen</label>
	// 						<input type="text" name="status_kelengkapan" class="status_kelengkapan" placeholder="Status Kelengkapan Dokumen">
	// 					</div>
	// 					<div class="field">
	// 						<label>Catatan Kelengkapan Dokumen</label>
	// 						<textarea name="catatan_kelengkapan" rows="3" class="catatan_kelengkapan" placeholder="Catatan Kelengkapan Dokumen"></textarea>
	// 					</div>`;
	//     	$('.appendField').append(html);
	// 	}else if (status == 2 || status == 3){
 //        	$('.catatan').prop( "disabled", false );
 //        	$('.wbs').prop( "disabled", false );
 //        	$('.dana').prop( "disabled", false );
	//     	$('.appendField').empty();
 //        }else{
 //        	$('.catatan').prop( "disabled", true );
 //        	$('.wbs').prop( "disabled", true );
 //        	$('.dana').prop( "disabled", true );
	//     	$('.appendField').empty();
 //        }
 //  	})

	// $(document).on('keyup', '.status_kelengkapan', function (e){
	// 	var catatanKelengkapan = $('.catatan_kelengkapan').val();

	// 	if ($(this).val() != '' && catatanKelengkapan != '') {
 //        	$('.wbs').prop( "disabled", false );
	// 	}else{
 //        	$('.wbs').prop( "disabled", true );
	// 	}
	// });

	// $(document).on('keyup', '.catatan_kelengkapan', function (e){
	// 	var statusKelengkapan = $('.status_kelengkapan').val();

	// 	if ($(this).val() != '' && statusKelengkapan != '') {
 //        	$('.wbs').prop( "disabled", false );
	// 	}else{
 //        	$('.wbs').prop( "disabled", true );
	// 	}
	// });

</script>
@include('scripts.inputmask')