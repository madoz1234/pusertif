<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail Pengujian</div>
<div class="content">
	<div class="ui top demo tabular menu">
  		<div class="active item" data-tab="awal">Detail</div>
  		<div class="item" data-tab="akhir">Lokasi</div>
	</div>
	<div class="ui bottom demo active tab" data-tab="awal">
		<table class="ui compact table" style="border-radius: 0; margin: 0">
			<tr>
				<td width="100"><b>Jenis Pengujian</b></td>
				<td width="10">:</td>
				<td width="200">
					{{ $record->detail_pendaftaran->jenis->nama or '-' }}
				</td>
			</tr>
			<tr>
				<td width="200"><b>Jadwal Pengujian Tentative</b></td>
				<td width="10">:</td>
				<td width="200">
					@if($record)
						{{ DateToStringYear($record->tentative_start) }} - {{ DateToStringYear($record->tentative_end) }}
					@else
						-
					@endif
				</td>
			</tr>
		</table>
		<table class="ui compact celled table" style="border-radius: 0; margin: 5px 0;">
			<thead>
				<tr class="middle aligned">
					<th class="center aligned" width="50">No</th>
					<th class="center aligned">Nama Mata Uji</th>
				</tr>
			</thead>
			<tbody class="detail fluid container">
				@if($record)
				@if($record->mata_uji)
				@foreach($record->mata_uji as $key => $data)
				<tr>
					<td class="center aligned">{{ $key+1 }}</td>
					<td class="center aligned">{{ $data->mata_uji }}</td>
				</tr>
				@endforeach
				@else
				<tr>
					<td class="center aligned" colspan="2">Tidak ada data</td>
				</tr>
				@endif
				@else
				<tr>
					<td class="center aligned" colspan="2">Tidak ada data</td>
				</tr>
				@endif
			</tbody>
		</table>
	</div>
	<div class="ui bottom demo tab" data-tab="akhir">
		<table class="ui compact celled table" style="border-radius: 0; margin: 5px 0;">
			<thead>
				<tr class="middle aligned">
					<th class="center aligned" width="50">No</th>
					<th class="center aligned">Jenis Lokasi</th>
					<th class="center aligned">Lokasi</th>
				</tr>
			</thead>
			<tbody class="detail fluid container">
				@if($record)
					@if($record->lokasi)
					@foreach($record->lokasi as $key => $data)
						<tr>
							<td class="center aligned">{{ $key+1 }}</td>
							<td class="center aligned">
								@if($data->jenis_lokasi == 1)
									In House
								@else 
									On Site
								@endif
							</td>
							<td class="center aligned">{{ $data->ket_lokasi }}</td>
						</tr>
					@endforeach
					@else
						<tr>
							<td class="center aligned" colspan="3">Tidak ada data</td>
						</tr>
					@endif
				@else
					<tr>
						<td class="center aligned" colspan="3">Tidak ada data</td>
					</tr>
				@endif
			</tbody>
		</table>
	</div>
</div>
<div class="actions">
	<div class="ui black deny button" style="background-color: #363636;">
		Kembali
	</div>
</div>

<script type="text/javascript">
</script>