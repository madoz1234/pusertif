<html>
<head>
<style>
        @page {
            margin: 60px 25px;
            counter-reset: page;
        }

       /* Define now the real margins of every page in the PDF */
       body {
           margin-top: 3cm;
           margin-left: 0cm;
           margin-right: 0cm;
           margin-bottom: 2cm;
           font-style: normal;
       }

       .ui.table.bordered {
         border: solid black 1px;
         border-collapse: collapse;
         width: 100%;
         font-family : "Arial", "Verdana", sans-serif;
       }

       .ui.table.bordered td {
         border: solid black .5px;
         border-collapse: collapse;
         /*padding:10px;*/
         padding:5px;
       }

       .ui.table.bordered td.center.aligned {
         text-align : center;
       }

       .page-break {
            page-break-after: always;
       }

       .pad{
       	padding-top: 5px;
       	padding-bottom: 5px;
       }

       /* Define the header rules */
        header {
            position: fixed;
            top: -40px;
            left: 0px;
            right: 0px;
            height: 10px;

            /* Extra personal styles */
            text-align: center;
            /*line-height: 35px;*/

            font-size: 10px;
        }
		
       main {
         position: sticky;
         font-size : 12px;
         padding-top : -1cm;
         margin-top : 2px;
         border : none;
         width: 100%;
       }

       main p {
         margin-left: 5px;
         margin-right: 5px;
       }

       /* Define the footer rules */
       footer {
           position: fixed;
           bottom: -60px;
           left: 0px;
           right: 0px;
           height: 50px;

           /* Extra personal styles */
           text-align: center;
           line-height: 35px;
       }
       table.page_content {width: 100%; border: none; padding: 1mm; font-size: 10px;font-family : "Arial", "Verdana", sans-serif;}
       table.page_header {width: 100%; border: none; padding: 2mm; font-size: 10px;font-family : "Arial", "Verdana", sans-serif; text-align: left; margin-left: 50px;margin-top: 50px;}
       table.page_content_header {width: 100%; font-size: 10px;border-collapse: collapse;font-family : "Arial", "Verdana", sans-serif;text-align: center; padding-bottom: 1px;
       	padding-top: 1px;}
</style>
</head>
<body>
    <header>
    	<table class="ui table bordered">
    		<tr>
    			<td class="center aligned" style="width: 79px;border-right: none;"><img src="{{ asset('img/icon.png') }}" width="75px" height="100px" style="display: block;"></td>
    			<td style="width: 161px;border-left: none;border-right: none;"><b style="font-size: 16px;">PT PLN (PERSERO)</b>
						<br><b style="font-size: 16px;">PUSAT SERTIFIKASI</b></td>
				<td class="center aligned" style="width: 99px;border-left: none;" colspan="3"><img src="{{ asset('img/lmk.png') }}" width="250px" height="90px" style="display: block;"></td>
    		</tr>
      	</table>
  	</header>
  	<br>
	<main>
      	<table class="ui table" style="border:none;margin-top: -20px;">
    		<tr>
    			<td colspan="2">Jalan Duren Tiga, Jakarta 112760</td>
    		</tr>
      	</table>
      	<table class="ui table" style="border:none;margin-top: -20px;">
    		<tr>
    			<td>Alamat Surat</td>
    			<td>:</td>
    			<td>Jakarta 12760</td>
    		</tr>
    		<tr>
    			<td>Telepon</td>
    			<td>:</td>
    			<td>7900034</td>
    		</tr>
    		<tr>
    			<td>Faks</td>
    			<td>:</td>
    			<td>7994749, 7982034, 7943450</td>
    		</tr>
      	</table>
		<div class="ui segment" style="padding-left: 500px;margin-top: 20px;">
			<table class="ui transparent fluid table" style="border-radius: 0; margin: 0">
				<tbody>
					<tr>
						<td style="width: 300px;" colspan="3"><b>BUKTI PENGEMBALIAN BARANG</b></td>
					</tr>
				</tbody>
			</table>
			<table class="ui transparent fluid table" style="border-radius: 0; margin: 0">
				<tbody>
					<tr>
						<td>WBS/IO</td>
						<td>:</td>
						<td>{{ $records->penerimaan->konfirmasi->wbs_io }}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="ui segment" style="padding-left: 100px;margin-top: -40px;">
			<table class="ui transparent fluid table" style="border-radius: 0; margin: 0">
				<tbody>
					<tr>
						<td>Dari</td>
						<td>:</td>
						<td>{{ $records->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama }}</td>
					</tr>
					<tr>
						<td>Nota Dinas</td>
						<td>:</td>
						<td>{{ $records->no_nota_dinas }}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<br>
		<table class="page_content_header" border="1">
			<tr>
				<th style="background:#ebebe0; width: 10px;">No</th>
				<th style="background:#ebebe0; width: 200px;">Nama Barang</th>
				<th style="background:#ebebe0; width: 50px;">Satuan</th>
				<th style="background:#ebebe0; width: 200px;">Catatan</th>
			</tr>
			@foreach($records->detail_pengembalian as $key => $data)
				<tr>
					<td style="text-align: center;">{{ $key+1 }}</td>
					<td style="text-align: center;">
						<table class="ui compact celled table" style="border-radius: 0; margin: 0; border:none;">
							<tbody>
								<tr>
									<td style="border-left: none;">Nama Barang</td>
									<td style="border-left: none;">:</td>
									<td style="border-left: none;">{{ $data->penerimaandetail->nama_barang }} [{{$data->penerimaandetail->urutan}}/{{$data->penerimaandetail->max}}]</td>
								</tr>
								<tr>
									<td style="border-left: none;">Merek</td>
									<td style="border-left: none;">:</td>
									<td style="border-left: none;">{{ $data->penerimaandetail->detail_pp->merk }} - {{ $data->penerimaandetail->detail_pp->tipe }}</td>
								</tr>
								<tr>
									<td style="border-left: none;">No. Seri</td>
									<td style="border-left: none;">:</td>
									<td style="border-left: none;">{{ $data->penerimaandetail->no_seri }}</td>
								</tr>
							</tbody>
						</table>
					</td>
					<td style="text-align: center;">{{ $data->penerimaandetail->detail_pp->satuan->satuan }}</td>
					<td style="text-align: left;text-align:justify;text-justify:auto;text-align-last: justify;"><p class="pad">{{ $data->penerimaandetail->catatan or '-' }}</p></td>
				</tr>
			@endforeach
		</table>
		<br>
		<table class="page_content_header" style="padding-left: -50px;">
            <div class="ui segment" style="padding-left: 500px;margin-top: 20px;">
            	<table class="ui transparent fluid table" style="border-radius: 0; margin: 0">
            		<tbody>
            			<tr>
            				<td style="width: 300px;">Jakarta, {{ DateToStringYear($records->tgl_nota_dinas)}}</td>
            			</tr>
            		</tbody>
            	</table>
            </div>
        </table>
	    </br>
		<table class="page_content_header" style="padding-left: -50px;">
            <tr>
                <td style="text-align: center;">
                    Penerima<br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    ...........
                </td>
                <td style="text-align: center;">
                    Pembuat<br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    {{ $records->creatorName() }}
                </td>
            </tr>
        </table>
	</main>
</body>
</html>