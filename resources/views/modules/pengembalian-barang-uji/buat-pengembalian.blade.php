@extends('layouts.form')

@section('styles')
<style type="text/css">
	.responsive.table{
		width: 100%;
		overflow-x: auto;
	}
	.jus{
		text-align: justify;
		text-justify: inter-word;
	}
	.fn{
		font-size: 12px;
	}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		judul: ['empty'],
	};
</script>
@endsection

@section('content-body')
<form class="ui data form" method="POST" enctype="multipart/form-data">
	<table id="listTable" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
		<thead>
			<tr class="middle aligned">
				<th width="10" class="center aligned fn">#</th>
				<th width="200" class="center aligned fn">Jenis Pengujian</th>
				<th width="250" class="center aligned fn">Nama Barang</th>
				<th width="150" class="center aligned fn">Merk</th>
				<th width="150" class="center aligned fn">No Seri</th>
				<th width="150" class="center aligned fn">Tgl Penerimaan Barang Uji</th>
				<th width="150" class="center aligned fn">Hasil Verifikasi</th>
				<th width="150" class="center aligned fn">Tgl Verifikasi Barang Uji</th>
				<th width="250" class="center aligned fn">Catatan</th>
				<th width="250" class="center aligned fn">No FPP/FPK</th>
				<th width="200" class="center aligned fn">Barang Uji Kembali</th>
				<th width="200" class="center aligned fn">Cheklist </br><button type="button" class="ui mini button select-all"><i class="checkmark icon"></i> Select All</button></th>
			</tr>
		</thead>
		<tbody class="detail fluid container detail">
			@php
				$angka=0;
				$i=1;
			@endphp
			@foreach($record->detail_penerimaan_barang->where('flag',0) as $key => $row)
				@if($row->detail_pengujian->detailpelaksana->status_barang ==1)
					<input type="hidden" name="pengujian_id" value="{{ $record->pengujian->id }}">
					<input type="hidden" name="detail_penerimaan_id" value="{{ $row->id }}">
					<tr class="middle aligned">
						<td class="center aligned">{{$i}}</td>
						<td>{{$row->detail_pp->jenis->nama}}</td>
						<td>{{$row->nama_barang}} [{{$row->urutan}}/{{$row->max}}]</td>
						<td>{{$row->detail_pp->merk}}</td>
						<td>{{$row->no_seri}}</td>
						<td class="center aligned">{{ DateToStringYear($row->penerimaan->tanggal_terima) }}</td>
						<td class="center aligned">
							@if($row->detail_pengujian->verifikasi == 1)
								<a class="ui green label">Diterima</a>
							@else
								<a class="ui red label">Ditolak</a>
							@endif
						</td>
						<td class="center aligned">{{ DateToStringYear(Carbon\Carbon::parse($row->detail_pengujian->created_at)->format('Y-m-d')) }}</td>
						<td>{{ $row->catatan }}</td>
						<td>
							{{ $row->detail_pengujian->fpp_fpk }}
						</td>
						<td class="center aligned">
							@if($row->detail_pengujian->detailpelaksana->status_barang == 0)
								<div class="ui label">
									<i class="check icon"></i> Barang tidak dikembalikan
								</div>
							@else
								<div class="ui label">
									<i class="check icon"></i> Barang dikembalikan
								</div>
							@endif
						</td>
						<td class="center aligned">
							<div class="ui fitted checkbox">
								@if($row->detail_pengujian->detailpelaksana->status_barang == 0)
									-
								@else
									@if($row->detail_pengujian->detailpelaksana->status_pengembalian == 0)
										@php
											$angka++; 
										@endphp
										<input name="checklist[]" class="check" type="checkbox" data-id="{{ $row->id }}">
										<label></label>
									@else
										<div class="ui green label">
											<i class="check icon"></i> Barang sudah dikembalikan
										</div>
									@endif
								@endif
							</div>
						</td>
					</tr>
				@php
					$i++;
				@endphp
				@endif
			@endforeach
		</tbody>
	</table>
</form>
<div class="actions menu top" style="margin-top: 12px;">
	<div class="ui two column grid">
		<div class="left aligned column">
			<div class="ui gray deny labeled icon button next" data-tab="first" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		@if($angka > 0)
			<div class="right aligned column">
				<div class="ui green labeled icon button kirim-data">
					Buat Pengembalian Barang Uji
					<i class="save icon"></i>
				</div>
			</div>
		@endif
	</div>		
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.tgl').calendar({
		ampm: false,
		type: 'date',
			// maxDate: tanggal,
			formatter: {
				date: function (date, settings) {
					if (!date) return '';
					let momentDate = moment(date)
					return momentDate.format('YYYY-MM-DD')
				}
			}
		});

	$(document).on('click', '.select-all', function(e){
		var checked		= true;

		$('.check').each(function(e){
			checked = !$(this).prop('checked') ? false : checked;
		});
		$('.check').prop('checked', !checked);
	});

	$(".number").on("keypress keyup blur",function (e) {    
		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	});

	$(document).on('click', '.kirim-data', function(e){
			var arr = [];
			$('.check').each(function(){
				if($(this).is(':checked')){
					arr.push($(this).data('id'));
				}
			});

			if (arr == '') {
				swal(
					'Oops !',
					'Checklist data terlebih dahulu.',
					'error'
				).then(function(e){
				});
			}else{
				swal({
					title: 'Apakah anda yakin?',
					text: "Data yang sudah dichecklist, tidak dapat dibubah kembali!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya',
					cancelButtonText: 'Batal',
					reverseButtons: true
				}).then((result) => {
					if (result) {
						event.preventDefault();
						var url = "{{ url($pageUrl) }}/simpan/"+arr;
						loadModal({
							'url' : url,
							'formId' : '#dataForm',
							'modal' : 'tiny',
							'onShow' : function(){ 
								onShow();
							},
						})
					}
				})
			}
		});
</script>
@append