@extends('layouts.form')
@section('styles')
<style type="text/css">
</style>
@append

@section('js-filters')
@endsection
@section('content-body')
<div class="ui segment">
	<div id="monotiron"></div>
</div>
<div class="ui segment">
	<div class="ui styled accordion" style="width: 100%;">
		<div class="title">
			<i class="dropdown icon"></i>
			Daftar Pengisi Survei
		</div>
		<div class="content">
			<table id="example" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
				<thead>
					<tr class="middle aligned">
						<th width="10" class="center aligned fn">No.</th>
						<th width="150" class="center aligned fn">Nama</th>
						<th width="250" class="center aligned fn">Perusahaan</th>
					</tr>
				</thead>
				<tbody class="detail fluid container detail">
					@foreach($record->transsurvei as $key => $data)
					<tr class="detail fn">
						<td class="center aligned">{{ $key+1 }}</td>
						<td class="left aligned">{{ $data->pp->user->pelanggans->nama_lengkap }}</td>
						<td class="left aligned">{{ $data->pp->user->pelanggans->perusahaan->nama }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="actions">
	<div class="ui two column grid">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
Highcharts.chart('monotiron', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Survei'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [
        		@foreach($grafik['data'] as $label => $point)
					{
						name: '{{ $label }} Bintang',
						y: {{ $point }}
					},
				@endforeach
        ]
    }]
});
</script>
@append