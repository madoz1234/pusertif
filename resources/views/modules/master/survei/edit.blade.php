@extends('layouts.form')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/wp.css') }}">
@append

@section('content-body')
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}"  method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">
		<div class="ui very basic thin segment">
			<table class="ui compact table" style="border-radius: 0; margin: 0">
				<tr>
					<td width="220"><b>Nama Survei</b></td>
					<td width="10">:</td>
					<td class="field">
						<div class="ui fluid transparent input">
							<input type="text" name="nama" placeholder="Masukkan nama Survei" value="{{$record->nama}}" readonly="">
						</div>
					</td>
					<td rowspan="4" class="center aligned" width="200" style="border-left: 1px solid rgba(34,36,38,.1);">
						<b>Status</b><br><br>
						<div class="ui toggle checkbox checked" style="margin-top:5px;">
							<input name="status" tabindex="0" class="hidden" type="checkbox" {{ $record->status == 1 ? 'checked' : '' }}>
							<label>Aktif</label>
						</div>
					</td>
				</tr>
				<tr>
					<td><b>Deskripsi</b></td>
					<td>:</td>
					<td>
						<div class="ui fluid transparent input">
							<input type="text" name="deskripsi" placeholder="Deskripsi tentang Survei" value="{{$record->deskripsi}}" readonly="">
						</div>
					</td>
				</tr>
			</table>
			<table class="ui compact celled table" style="border-radius: 0; margin: 5px 0">
				<thead>
					<tr class="middle aligned">
						<th rowspan="2" width="75%" class="center aligned">Pertanyaan</th>
						<th rowspan="2" width="15%" class="center aligned">Rating</th>
						<th rowspan="2" width="10%" class="center aligned"><button type="button" class="ui green mini icon append button"><i class="plus icon"></i></button></th>
					</tr>
				</thead>
				<tbody class="detail container">
					@foreach($record->detail as $details)
						<tr>
							<td class="field"><div class="ui transparent fluid input">
								<input type="hidden" name="exists[]" value="{{$details->id}}">
								<input type="hidden" name="detail[{{ $details->id }}][id]" value="{{$details->id}}">
								<textarea class="inline" name="detail[{{ $details->id }}][pertanyaan]" placeholder="Pertanyaan" style="height: 20px">{{ $details->pertanyaan }}</textarea>
							</div></td>
							<td class="center aligned">
								<div class="ui large star rating" data-max-rating="5" data-rating="5"></div>
							</td>
							<td class="center aligned">
								<button class="ui red mini icon remove button"><i class="remove icon"></i></button>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</form>

	<div class="ui divider"></div>
	<div class="actions">
		<div class="ui two column grid">
	  		<div class="left aligned column">
				<div class="ui gray deny labeled icon button" onclick="window.history.back()">
	      		<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">		
				<button type="button" class="ui positive right labeled icon save as page button">
					Simpan
					<i class="save icon"></i>
				</button>
			</div>
		</div>	
	</div>
@endsection

@section('scripts')
<script type="text/javascript">
	var num = {{ $record->detail->last()->id + 1 }};

	$(document).ready(function($) {
		$('.ui.rating')
		  .rating('disable')
		;

		$('.ui.checkbox')
		  .checkbox()
		;

		$('textarea.inline').trigger('keydown');
	});

	$(document).on('keydown', 'textarea.inline', function(e){
		var el = this;
		setTimeout(function(){
			el.style.cssText = 'height:20px; padding:0';
			// for box-sizing other than "content-box" use:
			// el.style.cssText = '-moz-box-sizing:content-box';
			el.style.cssText = 'height:' + el.scrollHeight + 'px';
		},0);
	});

	$(document).on('click', '.append.button', function(e){
		var c = num++;
		var html = `
			<tr>
				<td class="field"><div class="ui transparent fluid input">
					<textarea class="inline" name="detail[`+c+`][pertanyaan]" placeholder="Pertanyaan" style="height: 20px"></textarea>
				</div></td>
				<td class="center aligned">
					<div class="ui large star rating" data-max-rating="5" data-rating="5"></div>
				</td>
				<td class="center aligned">
					<button class="ui red mini icon remove button"><i class="remove icon"></i></button>
				</td>
			</tr>`;

		$('.detail.container').append(html);

		$('.ui.rating')
		  .rating('disable')
		;

		$('.dropdown').dropdown();
	});

	$(document).on('click', '.remove.button', function (e){
		var row = $(this).closest('tr');
		row.remove();
	});

</script>
@append