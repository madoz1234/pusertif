<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data Hari Kerja</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{$record->id}}">
		{!! csrf_field() !!}     
	      <div class="field">
			<label>Jenis Layanan</label>
			<select name="jenis_pelayanan_id" class="watcher ui fluid search dropdown disabled">
				{!!
                    \App\Models\Master\JenisPelayanan::options('nama', 'id', [
                        'filters' => [
                        ],
                        'selected' => $record->jenis_pelayanan_id
                    ])
                !!}
			</select>
		</div>
		<div class="field">
			<label>Menu</label>
			<div class="ui selection dropdown disabled">
				<input type="hidden" name="menu" value="{{ $record->menu }}" class="input-menu">
				<i class="dropdown icon"></i>
				<div class="default text">Pilih Menu</div>
				<div class="menu">
					@foreach($items = $mainMenu->roots() as $item)
						@if(!$item->hasChildren())
							<div class="item" data-value="{{ $item->url() }}">{!! $item->title !!}</div>
						@else
							<div class="header">
						      {!! $item->title !!}
						    </div>
						    <div class="divider"></div>
							@foreach ($item->children() as $child)
								<div class="item" style="margin-left: 20px" data-value="{{ $child->url() }}">{!! $child->title !!}</div>
							@endforeach
						@endif
					@endforeach
				</div>
			</div>
		</div>
		<input type="hidden" name="display_menu" value="{{ $record->display_menu }}" class="display-menu">
	      <div class="field">
	    	<label>Hari Kerja</label>
	        	<input name="hk" placeholder="Hari Kerja" type="text" value="{{ $record->hk }}">
	      </div>
	      <div class="field">
	    	<label>Description</label>
   		    	<textarea name="description" rows="2" placeholder="Description">{{ $record->description or ''}}</textarea>
	      </div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$('.ui.circular.label').addClass('hidden');
	$(document).on('change', '.input-menu', function(e){
		var menu = $(this).val();
		var display = '';
		@foreach($items = $mainMenu->roots() as $item)
			@if(!$item->hasChildren())
				var item = "{{ $item->url() }}";
				if(item==menu){
					display = "{!! $item->title !!}";
				}
			@else
				@foreach ($item->children() as $child)
					var item = "{{ $child->url() }}";
					if(item==menu){
						display = "{!! $item->title." ".$child->title !!}";
					}
				@endforeach
			@endif
		@endforeach

		$('.display-menu').val(display);
	});
</script>