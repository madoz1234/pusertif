@extends('layouts.list')

@section('js-filters')
    d.no_tlp = $("input[name='filter[no_tlp]']").val();
    d.wa = $("input[name='filter[wa]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			no_tlp: ['empty'],
			wa: ['empty'],
		};
	</script>
@endsection

@section('filters')
	<div class="field">
		<input name="filter[no_tlp]" placeholder="No Tlp" type="text">
	</div>
	<div class="field">
		<input name="filter[wa]" placeholder="WhatsApp" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
@if(auth()->user()->hasRole(['admin']))
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
			Tambah Data
	</button>
@endif
@endsection