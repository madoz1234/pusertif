<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data Contact</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{$record->id}}">
		{!! csrf_field() !!}     
	      <div class="field">
	    	<label>No Telpon</label>
	        	<input name="no_tlp" placeholder="No Telpon" type="text" value="{{ $record->no_tlp }}" maxlength="15">
	      </div>
	      <div class="field">
	    	<label>WhatsApp</label>
	        	<input name="wa" placeholder="WhatsApp" type="text" value="{{ $record->wa }}" maxlength="15">
	      </div>
	      <div class="field">
      		<label>Status</label>
      		<br>
      		<div class="ui checked checkbox">
      			<input type="checkbox" name="status" {{ $record->status == 1 ? 'checked' : '' }}  @if($jum > 2) disabled @endif>
      			<label>{{trans('translator.Aktif')}}</label>
      		</div>
      	  </div>
      	  @if($jum > 2)
      	  	<label style="color: red;">*Hanya boleh 3 No Telpon dan WhatsApp yang aktif.</label>
      	  @endif
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>