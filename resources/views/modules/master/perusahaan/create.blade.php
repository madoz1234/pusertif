<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Data Perusahaan</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
	      <div class="field">
	    	<label>Kode Perusahaan</label>
	        	<input name="kode" placeholder="Kode Perusahaan" type="text" value="{{ $no_perusahaan }}" readonly="">
	      </div>
	      <div class="field">
	    	<label>Nama Perusahaan</label>
	        	<input name="nama" placeholder="Nama Perusahaan" type="text" id="upper">
	      </div>
	      <div class="field">
	    	<label>No. Telpon</label>
	        	<input name="no_tlp" placeholder="No. Telpon Perusahaan" type="text" maxlength="15">
	      </div>
	      <div class="field">
	      	<label>Alamat</label>
	      	<textarea name="alamat" rows="2" placeholder="Alamat"></textarea>
	      </div>
	      <div class="field">
	      	<label>Status</label>
	      	<select name="status" class="watcher ui fluid search dropdown">
	      		<option value="">Pilih Status</option>
	      		<option value="0">Tidak Aktif</option>
	      		<option value="1">Aktif</option>
	      	</select>
	      </div>
	      <div class="field">
	      	<label>Kategori</label>
	      	<select name="kategori" class="watcher ui fluid search dropdown">
	      		<option value="">Pilih Kategori</option>
	      		<option value="0">PLN</option>
	      		<option value="1">NON PLN</option>
	      		<option value="2">A-PLN</option>
	      	</select>
	      </div>
	      <div class="field">
	    	<label>Email</label>
	        	<input name="email" placeholder="Email" type="text" maxlength="40">
	      </div>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button" type="submit">
		Simpan
		<i class="checkmark icon"></i>
	</div>
	<span id="error_message" class="text-danger"></span>  
    <span id="success_message" class="text-success"></span>
</div>
</form>
<script>
	$(document).on('keyup', '#upper', function(e){
    	var string = $(this).val();
		$(this).val(string.toUpperCase());
    });

    $(document).on('keydown', '#upper', function(e){
    	var string = $(this).val();
		$(this).val(string.toUpperCase());
    });

    $(document).on('change', '#upper', function(e){
    	var string = $(this).val();
		$(this).val(string.toUpperCase());
    });

	$.ajax().done(function(response){
    //check if response has errors object
    if(response.errors){

    // do what you want with errors, 

    }

});
// $(document).ready(function(){
// 	$('#submit').click(function(){  
//            var name = $('#name').val();  
//            var message = $('#message').val();  
//            if(name == '' || message == '')  
//            {  
//                 $('#error_message').html("All Fields are required");  
//            }  
//            else  
//            {  
//                 $('#error_message').html('');  
//                 $.ajax({  
//                      url:'{{ url($pageUrl) }}/master/perusahaan',  
//                      method:"POST",  
//                      data:{name:name, message:message},  
//                      success:function(data){  
//                           $("form").trigger("reset");  
//                           $('#success_message').fadeIn().html(data);  
//                           setTimeout(function(){  
//                                $('#success_message').fadeOut("Slow");  
//                           }, 2000);  
//                      }  
//                 });  
//            }  
//       });  
// });
</script>
