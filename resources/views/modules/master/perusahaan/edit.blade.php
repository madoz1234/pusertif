<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data Perusahaan</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{$record->id}}">
		{!! csrf_field() !!}     
	      <div class="field">
	    	<label>Kode Perusahaan</label>
	        	<input name="kode" placeholder="Kode Perusahaan" type="text" value="{{ $record->kode }}" readonly="">
	      </div>
	      <div class="field">
	    	<label>Nama Perusahaan</label>
	        	<input name="nama" placeholder="Nama Perusahaan" type="text" value="{{ $record->nama }}" id="upper">
	      </div>
	       <div class="field">
	    	<label>No. Telpon</label>
	        	<input name="no_tlp" placeholder="No. Telpon Perusahaan" type="text" value="{{ $record->no_tlp or '' }}" maxlength="15">
	      </div>
	      <div class="field">
	      	<label>Alamat</label>
	      	<textarea name="alamat" rows="2" placeholder="Alamat">{{ $record->alamat or '' }}</textarea>
	      </div>
	      <div class="field">
	      	<label>Status</label>
	      	<select name="status" class="watcher ui fluid search dropdown">
	      		<option value="">Pilih Status</option>
	      		<option value="0" @if($record->status == 0) selected @endif>Tidak Aktif</option>
	      		<option value="1" @if($record->status == 1) selected @endif>Aktif</option>
	      	</select>
	      </div>
	      <div class="field">
	      	<label>Kategori</label>
	      	<select name="kategori" class="watcher ui fluid search dropdown">
	      		<option value="">Pilih Kategori</option>
	      		<option value="0" @if($record->kategori == 0) selected @endif>PLN</option>
	      		<option value="1" @if($record->kategori == 1) selected @endif>NON PLN</option>
	      		<option value="2" @if($record->kategori == 2) selected @endif>A-PLN</option>
	      	</select>
	      </div>
	      <div class="field">
	    	<label>Email</label>
	        	<input name="email" placeholder="Email" type="text" value="{{ $record->email or '' }}" maxlength="40">
	      </div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$(document).on('keyup', '#upper', function(e){
    	var string = $(this).val();
		$(this).val(string.toUpperCase());
    });

    $(document).on('keydown', '#upper', function(e){
    	var string = $(this).val();
		$(this).val(string.toUpperCase());
    });

    $(document).on('change', '#upper', function(e){
    	var string = $(this).val();
		$(this).val(string.toUpperCase());
    });
</script>