<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Data</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		  <div class="field">
				<label>Jenis Layanan</label>
				<select name="jenis_pelayanan_id" class="watcher ui fluid search dropdown">
					{!! \App\Models\Master\JenisPelayanan::options('nama', 'id', [], 'Pilih Jenis Pelayanan') !!}
				</select>
			</div>
	      <div class="field">
	    	<label>Nama</label>
	        	<input name="nama" placeholder="Nama" type="text">
	      </div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>