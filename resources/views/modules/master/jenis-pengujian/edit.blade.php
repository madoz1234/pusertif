<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{$record->id}}">
		{!! csrf_field() !!}
		  	<div class="field">
				<label>Jenis Layanan</label>
				<select name="jenis_pelayanan_id" class="watcher ui fluid search dropdown disabled">
					{!!
                        \App\Models\Master\JenisPelayanan::options('nama', 'id', [
                            'filters' => [
                            ],
                            'selected' => ($record->lingkup->pelayanan ? $record->lingkup->pelayanan->id : null)
                        ])
                    !!}
				</select>
			</div>
			<div class="field">
				<label>Lingkup</label>
				<select name="lingkup_id" class="watcher ui fluid search dropdown disabled">
					{!!
                        \App\Models\Master\Lingkup::options('nama', 'id', [
                            'filters' => [
                            ],
                            'selected' => ($record->lingkup_id ? $record->lingkup->id : null)
                        ])
                    !!}
				</select>
			</div>
	      	<div class="field">
	    		<label>Nama</label>
	        	<input name="nama" placeholder="Nama" type="text" value="{{ $record->nama }}">
	      	</div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$.ajax({
		url: '{{ url('ajax/option/lingkup') }}',
		type: 'POST',
		data: {
			_token: "{{ csrf_token() }}",
			jenis_pelayanan_id: $('select[name=jenis_pelayanan_id]').val()
		},
	})
	.done(function(response) {
		$('select[name=lingkup_id]').html(response)
	});

	$('select[name=jenis_pelayanan_id]').on('change', function(){
        $.ajax({
            url: '{{ url('ajax/option/lingkup') }}',
            type: 'POST',
            data: {
            	_token: "{{ csrf_token() }}",
                jenis_pelayanan_id: this.value
            },
        })
        .done(function(response) {
            $('select[name=lingkup_id]').html(response)
        })
        .fail(function() {
            console.log("error");
        });
  	})
</script>