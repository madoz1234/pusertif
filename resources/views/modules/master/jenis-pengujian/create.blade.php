<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Data</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
			<div class="field">
				<label>Jenis Layanan</label>
				<select name="jenis_pelayanan_id" class="watcher ui fluid search dropdown">
					{!!
                        \App\Models\Master\JenisPelayanan::options('nama', 'id', [
                            'filters' => [
                            ],
                            'selected' => old('jenis_pelayanan_id')
                        ])
                    !!}
				</select>
			</div>
		  	<div class="field">
				<label>Lingkup</label>
				<select name="lingkup_id" class="watcher ui fluid search dropdown">
				</select>
			</div>
	      	<div class="field">
	    		<label>Jenis Pengujian</label>
	        	<input name="nama" placeholder="Jenis Pengujian" type="text">
	      	</div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$('select[name=jenis_pelayanan_id]').on('change', function(){
        $.ajax({
            url: '{{ url('ajax/option/lingkup') }}',
            type: 'POST',
            data: {
            	_token: "{{ csrf_token() }}",
                jenis_pelayanan_id: this.value
            },
        })
        .done(function(response) {
        	console.log(response);
            $('select[name=lingkup_id]').html(response)
        })
        .fail(function() {
            console.log("error");
        });
  	})
</script>