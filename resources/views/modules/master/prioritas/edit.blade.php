<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{$record->id}}">
		{!! csrf_field() !!}     
	      <div class="field">
				<label>Jenis Layanan</label>
				<select name="jenis_pelayanan_id" class="watcher ui fluid search dropdown">
					{!! \App\Models\Master\JenisPelayanan::options('nama', 'id', ['selected' => ($record->layanan_id ? $record->layanan_id : null)], 'Pilih Jenis Pelayanan') !!}
				</select>
			</div>
	      <div class="field">
	    	<label>Waktu</label>
	        	<input name="waktu" placeholder="Waktu" type="text" class="number" value="{{ $record->waktu }}">
	        	<div class="required inline field">
					<label>bulan</label>
				</div>
	      </div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>