@extends('layouts.list')

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			waktu: ['empty'],
			jenis_pelayanan_id: ['empty'],
		};
	</script>
@endsection

@section('filters')
	<div class="field">
		<input name="filter[nama]" placeholder="Nama Jenis Pengujian" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
@if(auth()->user()->hasRole(['admin']))
		<button type="button" class="ui blue add button">
			<i class="plus icon"></i>
			Tambah Data
		</button>
	@endif
@endsection
@section('init-modal')
<script>
	onShow = function(){
		$(".number").on("keypress keyup blur",function (e) {    
            $(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
        });
	};
</script>
@endsection