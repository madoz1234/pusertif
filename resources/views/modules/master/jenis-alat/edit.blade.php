<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data Jenis Alat</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{$record->id}}">
		{!! csrf_field() !!}     
	      <div class="field">
	    	<label>Nama Jenis Alat</label>
	        	<input name="nama_jenis_alat" placeholder="Nama Jenis Alat" type="text" value="{{ $record->nama_jenis_alat }}">
	      </div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>