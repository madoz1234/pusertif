<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data {{ $title or '' }}</div>
<div class="content">
  <form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">

      {!! csrf_field() !!}
      <input type="hidden" name="_method" value="PUT">
      <input type="hidden" name="id" value="{{ $record->id }}">
      <input type="hidden" name="id_detail" value="{{ $record->detail->id or '' }}">
        <div class="fields">
            <div class="field eight wide column">
                <label>Username</label>
                <div class="ui left icon input">
                    <i class="user outline icon"></i>
                    <input type="text" placeholder="Username" name="username" value="{{ $record->username or '' }}" maxlength="50">
                </div>
            </div>
            <div class="field eight wide column">
                <label>NIP</label>
                <div class="ui left icon input">
                    <i class="hashtag icon"></i>
                    <input type="text" placeholder="NIP" name="nip" value="{{ $record->detail->nip or '' }}" maxlength="15">
                </div>
            </div>
        </div>
        <div class="fields">
            <div class="field eight wide column">
                <label>Nama Lengkap</label>
                <div class="ui left icon input">
                    <i class="address card icon"></i>
                    <input type="text" placeholder="Nama Lengkap" name="nama_lengkap" value="{{$record->detail->nama_lengkap or ''}}" maxlength="50">
                </div>
            </div>
            <div class="field eight wide column">
                <label>E-Mail</label>
                <div class="ui left icon input">
                    <i class="mail icon"></i>
                    <input type="email" placeholder="E-Mail" name="email" value="{{$record->email or ''}}" maxlength="50">
                </div>
            </div>
        </div>
        <div class="fields">
            <div class="field eight wide column">
                <label>Nomor HP</label>
                <div class="ui left icon input">
                    <i class="phone icon"></i>
                    <input type="text" placeholder="Nomor HP" class="angka" name="no_tlp" value="{{ $record->detail->no_tlp or '' }}" maxlength="12">
                </div>
            </div>
            
            <div class="field eight wide column">
                <label>Hak Akses</label>
                <select name="roles[]" class="ui fluid search dropdown">
                       {!! App\Models\Authentication\Role::options('display_name', 'id', ['selected' => $record->roles->pluck('id')->toArray(), 'filters' => [function($q){
									return $q->whereNotIn('id', [1,2,7,8]);
							}]],  'Pilih Hak Akses') !!}
                </select>
            </div>
        </div>
        <div class="field">
            <label>Alamat</label>
            <div class="ui left icon input">
                <i class="address icon"></i>
                <textarea name="alamat" rows="2" placeholder="Alamat">{{$record->detail->alamat or ''}}</textarea>
            </div>
        </div>
        <div class="fields">
            <div class="field eight wide column">
                <label>Password</label>
                <div class="ui left icon input">
                    <i class="lock icon"></i>
                    <input type="password" name="password" placeholder="Password">
                </div>
            </div>
            <div class="field eight wide column">
                <label>Confirm Password</label>
                <div class="ui left icon input">
                    <i class="unlock alternate icon"></i>
                    <input type="password" name="confirm_password" placeholder="Confirm Password">
                </div>
            </div>
        </div>
</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function($) {
    $(".angka").on("keypress keyup blur",function (e) {    
        $(this).val($(this).val().replace(/[^0-9]/g, ''));
    });
    $(document).on('click','.next',function(){
        var data = $(this).data('tab');
        var dataPrev = $(this).data('prev');
        $('.menu').find("[data-tab='" + dataPrev + "']").removeClass('active');
        $('.menu').find("[data-tab='" + data + "']").addClass('active');
    });
    $('.menu .item').tab();
    $('.next').tab();
    $('.ui.radio.checkbox').checkbox();
    $('.message .close')
    .on('click', function() {
        $(this)
        .closest('.message')
        .transition('fade')
        ;
    })
    ;

    $('.ui.checkbox').checkbox()
    $('.ui.search').search({
        minCharacters : 2,
        fullTextSearch: true,
        apiSettings: {
            url: "{{ url('cari-perusahaan') }}?q={query}",
        },
        fields: {
            results : 'data',
            title   : 'nama',
            id      : 'id'
        },
        onSelect: function(result, response){
            console.log(result);
            if(result){
                $('[name=perusahaan_id]').val(result.id);
            }else{
                $('[name=perusahaan_id]').val('');
            }
        }
    });
});

$(document).on('keyup', '.nama.usaha', function(e){
        var code = e.which; // recommended to use e.which, it's normalized across browsers
        if(code!=13){
            $('[name=perusahaan_id]').val('');
        }
    });
</script>