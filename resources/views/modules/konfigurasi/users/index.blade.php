@extends('layouts.list')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
	<div class="field">
		<input type="text" name="filter[username]" placeholder="Username Pengguna">
	</div>
	<div class="field">
		<input type="email" name="filter[email]" placeholder="Email Pengguna">
	</div>
	<div class="field">
		<select name="filter[hak_akses]" class="ui search dropdown">
	        {!! App\Models\Authentication\Role::options('display_name', 'id', ['filters' => [function($q){
							return $q->whereNotIn('id', [1,2,7,8]);
					}]], 'Pilih Hak Akses') !!}
	    </select>
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('js-filters')
	d.username = $("input[name='filter[username]']").val();
	d.email = $("input[name='filter[email]']").val();
	d.role = $("select[name='filter[hak_akses]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			// username: 'empty',
			// email: 'empty',
			// roles: 'empty',
		};
	</script>
@endsection

@section('init-modal')
	<script>
		$(document).ready(function() {
			
		});
	</script>
@endsection