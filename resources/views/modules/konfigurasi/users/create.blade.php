<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Data {{ $title or '' }}</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{{-- <div class="ui error message">
		</div> --}}
		{!! csrf_field() !!}
       {{--  <div class="field">
            <label>Jenis Layanan</label>
            <select name="jenis_pelayanan_id" class="watcher ui fluid search dropdown">
                {!! \App\Models\Master\JenisPelayanan::options('nama', 'id', [], 'Pilih Jenis Pelayanan') !!}
            </select>
        </div> --}}
        <div class="fields">
            <div class="field eight wide column">
            	<label>Username</label>
                <div class="ui left icon input">
                    <i class="user outline icon"></i>
                    <input type="text" placeholder="Username" name="username" value="{{ old('username') }}" maxlength="50">
                </div>
            </div>
            <div class="field eight wide column">
                <label>NIP</label>
                <div class="ui left icon input">
                    <i class="hashtag icon"></i>
                    <input type="text" placeholder="NIP" name="nip" value="{{ old('nip') }}" maxlength="15">
                </div>
            </div>
        </div>
        <div class="fields">
            <div class="field eight wide column">
                <label>Nama Lengkap</label>
                <div class="ui left icon input">
                    <i class="address card icon"></i>
                    <input type="text" placeholder="Nama Lengkap" name="nama_lengkap" value="{{ old('nama') }}" maxlength="50">
                </div>
            </div>
            <div class="field eight wide column">
            	<label>E-Mail</label>
                <div class="ui left icon input">
                    <i class="mail icon"></i>
                    <input type="email" placeholder="E-Mail" name="email" value="{{ old('email') }}" maxlength="50">
                </div>
            </div>
        </div>
        <div class="fields">
            <div class="field eight wide column">
                <label>Nomor HP</label>
                <div class="ui left icon input">
                    <i class="phone icon"></i>
                    <input type="text" class="angka" placeholder="Nomor HP" name="no_tlp" value="{{ old('no_tlp') }}" maxlength="12">
                </div>
            </div>
            
            <div class="field eight wide column">
                <label>Hak Akses</label>
                <select name="roles[]" class="ui fluid search dropdown">
                    {!! App\Models\Authentication\Role::options('display_name', 'id', ['filters' => [function($q){
									return $q->whereNotIn('id', [1,2,7,8]);
							}]], 'Pilih Hak Akses') !!}
                </select>
            </div>
        </div>
       
        <div class="field">
            <label>Alamat</label>
            <div class="ui left icon input">
                <i class="address icon"></i>
                <textarea name="alamat" rows="2" placeholder="Alamat"></textarea>
            </div>
        </div>
        <div class="fields">
            <div class="field eight wide column">
            	<label>Password</label>
                <div class="ui left icon input">
                    <i class="lock icon"></i>
                    <input type="password" name="password" placeholder="Password" maxlength="20">
                </div>
            </div>
            <div class="field eight wide column">
            	<label>Confirm Password</label>
                <div class="ui left icon input">
                    <i class="unlock alternate icon"></i>
                    <input type="password" name="confirm_password" placeholder="Confirm Password" maxlength="20">
                </div>
            </div>
        </div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function($) {
        $(".angka").on("keypress keyup blur",function (e) {    
            $(this).val($(this).val().replace(/[^0-9]/g, ''));
        });
        $(document).on('click','.next',function(){
            var data = $(this).data('tab');
            var dataPrev = $(this).data('prev');
            $('.menu').find("[data-tab='" + dataPrev + "']").removeClass('active');
            $('.menu').find("[data-tab='" + data + "']").addClass('active');
        });
        $('.menu .item').tab();
        $('.next').tab();
        $('.ui.radio.checkbox').checkbox();
        $('.message .close')
          .on('click', function() {
            $(this)
              .closest('.message')
              .transition('fade')
            ;
          })
        ;

        $('.ui.checkbox').checkbox()
        $('.ui.search').search({
            minCharacters : 2,
            fullTextSearch: true,
            apiSettings: {
                url: "{{ url('cari-perusahaan') }}?q={query}",
            },
            fields: {
                results : 'data',
                title   : 'nama',
                id      : 'id'
            },
            onSelect: function(result, response){
                console.log(result);
                if(result){
                    $('[name=perusahaan_id]').val(result.id);
                }else{
                    $('[name=perusahaan_id]').val('');
                }
            }
        });
    });

    $(document).on('keyup', '.nama.usaha', function(e){
        var code = e.which; // recommended to use e.which, it's normalized across browsers
        if(code!=13){
            $('[name=perusahaan_id]').val('');
        }
    });
</script>