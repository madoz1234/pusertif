@extends('layouts.list')

@section('css')
<style type="text/css">
	.upload.button{
		border-radius: inherit;
	}
</style>
@append

@section('js')
<script type="text/javascript">
	$(document).ready(function($) {
		$('.profile.image').dimmer({
			on: 'hover'
		});

		$('.tabable .item').tab();

		$('.upload.photo.button').click(function(){ $('#photo').trigger('click'); });
	});

	$('.upload.photo.button').click(function(){ $('#photo').trigger('click'); });
	
	$(document).on('change', '#photo' , function(){
		console.log('start upload'); 
	});
</script>
@append

@section('content-body')
<div class="ui stackable grid">	
	<div class="four wide column">	
		<div class="ui fluid card">
			<div class="ui medium image image-container">
				<img class="ui image-preview image" 
					 src="{{ isset($user->photo) ? asset('storage/'.$user->photo) : asset('img/user-default.jpg') }}" 
					 style="border-top: none; border-right: none; border-left: none;">
			</div>
			<div class="content">
				<a class="header" href="#">{{ $user->username }}</a>
				<div class="meta">
					<a>{{ $user->roles()->first()->display_name }}</a>
				</div>
			</div>
			{{-- <div class="extra content">
				<div class="ui list">
					<div class="item">
						<i class="file text blue icon"></i>
						<div class="content">
							<span class="header">{{ $ongoing->count() + $done->count() }} Working Permit</span>
							<div class="description">{{ $done->count() }} dari {{ $ongoing->count() + $done->count() }} WP Telah Selesai </div>
						</div>
					</div>
				</div>
			</div> --}}
		</div>
	</div>
	<div class="twelve wide column">	
		<div class="ui pointing secondary menu tabable">
			<a class="item active" data-tab="first">Data Akun & Pribadi</a>
			{{-- <a class="item" data-tab="second">Aktifitas</a> --}}
		</div>
		<div class="ui tab active" data-tab="first" >
			<form id="dataForm" class="ui form" action="{{ url($pageUrl) }}" method="POST">
				{!! csrf_field() !!}
				<input type="hidden" name="id" value="{{ $user->id }}">
				<h3 class="ui center aligned dividing header">Data Pribadi</h3>
				<div class="ui columns doubling stackable grid">
					<div class="nine wide column">
						<div class="field">
							<label>Nama Lengkap</label>
							<input type="text" name="nama_lengkap" placeholder="Nama Lengkap" value="{{ $user->nama or '' }}">
						</div>
						<div class="fields">
							<div class="ten wide field">
								<label>NIP</label>
								<input type="text" name="nip" placeholder="Nomor Induk Pegawai" value="{{ $user->detail->nip or '' }}">
							</div>
							<div class="six wide field">
								<label>Jabatan</label>
								<input type="text" name="jabatan" placeholder="Jabatan" value="{{ $user->detail->jabatan or '' }}">
							</div>
						</div>
						<div class="fields">
							<div class="six wide field">
								<label>Nomor Telepon</label>
								<input type="text" name="no_tlp" placeholder="Nomor Telepon Pegawai" value="{{ $user->detail->no_tlp or '' }}">
							</div>
							<div class="ten wide field">
								<label>Alamat Email</label>
								<input type="text" name="email" placeholder="Email Pegawai" value="{{ $user->email or '' }}">
							</div>
						</div>
					</div>
					<div class="seven wide column">
						<div class="field">
							<label>Jenis Layanan</label>
            				<select name="jenis_pelayanan_id" class="watcher ui fluid search dropdown">
								{!! \App\Models\Master\JenisPelayanan::options('nama', 'id', ['selected' => ($user->detail) ? $user->detail->jenis_pelayanan_id : ''], 'Pilih Jenis Pelayanan') !!}
							</select>
						</div>
						<div class="field">
							<label>Alamat</label>
							<textarea name="alamat" rows="4" placeholder="Alamat">{{$user->detail->alamat or ''}}</textarea>
						</div>
					</div>
				</div>
				<h3 class="ui center aligned dividing header">Data Akun & Ubah Password</h3>
				<div class="ui stackable grid">
					<div class="four wide column">
						<div class="field">
							<label>Foto Profil</label>
							<div class="ui medium profile image image-container">
								<div class="ui dimmer">
									<div class="content">
										<div class="ui inverted blue upload photo button">Upload Gambar</div>
										<input type="file" id="photo" name="photo" class="attachment" style="display: none!important;" />
									</div>
								</div>
								<img class="ui image-preview image" 
									 src="{{ isset($user->photo) ? asset('storage/'.$user->photo) : asset('img/user-default.jpg') }}" 
									 style="border-top: none; border-right: none; border-left: none;">
							</div>
						</div>
					</div>
					<div class="twelve wide column">
						<div class="two fields">
							<div class="field">
								<label>Username</label>
								<input type="text" name="username" value="{{ $user->username }}">
							</div>
							<div class="field">
								<label>Hak Akses</label>
								<input type="text" value="{{ $user->roles->first()->display_name }}" readonly>
							</div>
						</div>
						<div class="ui info message">
							<div class="header">Informasi Ubah Password</div>
							<p>Isi field dibawah untuk mengubah password, kosongkan jika tidak ingin merubah password.</p>
						</div>
						<div class="three fields">
							<div class="field">
								<label>Password Saat Ini</label>
								<input type="password" name="old_password" placeholder="Isi Password Dengan Benar">
							</div>
							<div class="field">
								<label>Password Baru</label>
								<input type="password" name="password" placeholder="Isi Password Dengan Benar">
							</div>
							<div class="field">
								<label>Konfirmasi Password</label>
								<input type="password" name="confirm_password" placeholder="Konfirmasi Password sesuai dengan Password Baru">
							</div>
						</div>
					</div>
				</div>
				<div class="ui transparent divider"></div>
			  	<button type="button" class="ui right floated blue labeled icon save as page button"><i class="save icon"></i>Simpan</button>
				<div class="ui clearing transparent divider"></div>
			</form>
		</div>
		{{-- <div class="ui tab center aligned segment" data-tab="second" style="height:400px;">
			<h1 class="ui header" style="line-height: 400px">
				U N D E R &nbsp; &nbsp; C O N S T R U C T I O N
			</h1>
		</div> --}}
	</div>
</div>
@endsection