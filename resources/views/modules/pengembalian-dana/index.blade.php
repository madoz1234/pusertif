@extends('layouts.list-pengembalian-dana')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
	<div class="field">
        <input name="filter[no_order]" placeholder="No Order" type="text">
    </div>
    <div class="field">
        <div class="ui left icon date input">
            <i class="calendar icon"></i>
            <input type="text" name="filter[tanggal_order]" placeholder="Tanggal Order">
        </div>
    </div>
{{--     <div class="field">
      <input type="text" name="filter[no_surat]" placeholder="No Surat Permintaan">
    </div> --}}
    <div class="field">
      <select name="filter[jenis_pelayanan_id]" class="watcher ui fluid search dropdown">
        {!! \App\Models\Master\JenisPelayanan::options('nama', 'id', [], 'Pilih Layanan') !!}
      </select>
    </div>
    <div class="field">
      <select name="filter[perusahaan_id]" class="watcher ui fluid search dropdown">
        {!!
          \App\Models\Master\Perusahaan::optionsa('nama', 'id', ['filters' => [function($q){
            return $q->where('status', 1);
          }],
          'selected' => old('id')
        ], 'Nama Perusahaan')
        !!}
      </select>
    </div>
    <div class="field">
        <input type="text" name="filter[no_va]" placeholder="No VA">
    </div>
    <div class="field">
        <input type="text" name="filter[no_wbs]" placeholder="No WBS/IO">
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data">
        <i class="search icon"></i>
    </button>
    <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button>
@endsection

@section('filterdata')
    d.no_order = $("input[name='filter[no_order]']").val();
    d.tanggal_order = $("input[name='filter[tanggal_order]']").val();
    d.no_surat = $("input[name='filter[no_surat]']").val();
    d.jenis_pelayanan_id = $("select[name='filter[jenis_pelayanan_id]']").val();
    d.perusahaan_id = $("select[name='filter[perusahaan_id]']").val();
    d.no_va = $("input[name='filter[no_va]']").val();
    d.no_wbs = $("input[name='filter[no_wbs]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			username: 'empty',
			email: 'empty',
			roles: 'empty',
		};
	</script>
@endsection

@section('tables')
    <div class="ui inverted loading dimmer">
        <div class="ui text loader">Loading</div>
    </div>
    <div class="ui top demo tabular menu">
      <div class="active item" data-tab="first">Lebih Bayar @if($satu > 0)<span style="background-color:red;"class="ui circular label">{{ $satu }}</span>@endif</div>
      <div class="item" data-tab="second">Historis</div>
      <div class="item" data-tab="third">Pengembalian Dana Uji @if($dua > 0)<span style="background-color:red;"class="ui circular label">{{ $dua }}</span>@endif</div>
      <div class="item" data-tab="four">Historis</div>
    </div>
    <div class="ui bottom demo active tab" data-tab="first">
        @if(isset($structs['listStruct']))
            <table id="listTable" class="ui celled compact red table display" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        @foreach ($structs['listStruct'] as $struct)
                        <th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @yield('tableBody')
                </tbody>
            </table>
        @endif
    </div>
    <div class="ui bottom demo tab" data-tab="second">
        @if(isset($structs['listStruct2']))
        <table id="listTable2" class="ui celled compact red table display" width="100%" cellspacing="0">
            <thead>
                <tr>
                    @foreach ($structs['listStruct2'] as $struct)
                    <th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @yield('tableBody')
            </tbody>
        </table>
        @endif
    </div>
    <div class="ui bottom demo tab" data-tab="third">
        @if(isset($structs['listStruct3']))
            <table id="listTable3" class="ui celled compact red table display" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        @foreach ($structs['listStruct3'] as $struct)
                        <th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @yield('tableBody')
                </tbody>
            </table>
        @endif
    </div>
    <div class="ui bottom demo tab" data-tab="four">
        @if(isset($structs['listStruct4']))
        <table id="listTable4" class="ui celled compact red table display" width="100%" cellspacing="0">
            <thead>
                <tr>
                    @foreach ($structs['listStruct4'] as $struct)
                    <th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @yield('tableBody')
            </tbody>
        </table>
        @endif
    </div>
@endsection

{{-- formodal --}}
@section('init-modal')
<script type="text/javascript">
    initModal = function() {
        $('.date_calendar').calendar({
            ampm: false,
            type: 'date',
        // maxDate: tanggal,
        formatter: {
        date: function (date, settings) {
            if (!date) return '';
            let momentDate = moment(date)
            return momentDate.format('YYYY-MM-DD')
                }
            }
          });
          
        $(".disa").prop("disabled", true);
    };
</script>
@endsection

@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
    $('.date').calendar({
        type: 'date',
        formatter: {
          date: function (date, settings) {
            if (!date) return '';
            let momentDate = moment(date)
            return momentDate.format('DD/MM/YYYY')
          }
        }
    })
        $(document).on('click', '.pengembalian', function (e){
            var id = $(this).data("id");
            var url = "{{ url($pageUrl) }}/"+id+"/pengembalian"
            loadModal({
                'url' : url,
                'modal' : 'tiny modal',
                'formId' : '#dataForm',
                'onShow' : function(){ 
                    onShow();
                },
            })
        });

        $(document).on('click', '.buat-pengembalian', function (e){
            var id = $(this).data("id");
            var url = "{{ url($pageUrl) }}/"+id+"/buat-pengembalian"
            loadModal({
                'url' : url,
                'modal' : 'tiny modal',
                'formId' : '#dataForm',
                'onShow' : function(){ 
                    onShow();
                },
            })
        });

        $(document).on('click', '.detailPengembalian', function (e){
            var id = $(this).data("id");
            var url = "{{ url($pageUrl) }}/"+id+"/detail"
            loadModal({
                'url' : url,
                'modal' : 'tiny modal',
                'formId' : '#dataForm',
                'onShow' : function(){ 
                    onShow();
                },
            })
        });
        $(document).on("keypress keyup blur",".number",function (e) {    
            console.log("tes");
            $(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
        });

</script>
@append