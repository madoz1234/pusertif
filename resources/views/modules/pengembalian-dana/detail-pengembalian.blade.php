@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
		.invis{
			border-radius: 0;
			margin: 0;
			border: 0px solid transparent;	
		}
	</style>
@append
{{-- @section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append --}}

{{-- @section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append --}}
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header" style="border-radius: 0; margin: 0; border: 0px solid transparent;">Lihat Pengembalian Dana</div>
<div class="content">
	<form class="ui data form" id="dataForm" role="form" action="#" method="POST">
		<div class="ui form">
			<table class="ui compact table" style="border-radius: 0; margin: 0; border: 0px solid transparent;">
				<tr>
					<td style="border-radius: 0; margin: 0; border: 0px solid transparent;"><b>Nominal VA (Rp)</b></td>
					<td width="10" style="border-radius: 0; margin: 0; border: 0px solid transparent;">:</td>
					<td style="border-radius: 0; margin: 0; border: 0px solid transparent;" class="field">
						<div class="ui right labeled input">
							<label class="ui label">Rp.</label>
							<input type="text" min="0" class="number @if(auth()->user()->hasRole('keuangan')) disa @else @endif" maxlength="16" placeholder="Nominal VA (Rp)" value="{{ $record->va->surat->total or ''}}" data-inputmask="'alias' : 'numeric'" readonly="">
							<div class="ui basic label">.00</div>
						</div>
					</td>
				</tr>
				<tr>
					<td style="border-radius: 0; margin: 0; border: 0px solid transparent;"><b>Dana Masuk (Rp)</b></td>
					<td width="10" style="border-radius: 0; margin: 0; border: 0px solid transparent;">:</td>
					<td style="border-radius: 0; margin: 0; border: 0px solid transparent;" class="field">
						<div class="ui right labeled input">
							<label class="ui label">Rp.</label>
							<input type="text" min="0" class="number @if(auth()->user()->hasRole('keuangan')) disa @else @endif" maxlength="16" placeholder="Dana Masuk (Rp)" value="{{ $record->dana_masuk or '' }}" data-inputmask="'alias' : 'numeric'" readonly="">
							<div class="ui basic label">.00</div>
						</div>
					</td>
				</tr>
				<tr>
					<td style="border-radius: 0; margin: 0; border: 0px solid transparent;"><b>Tanggal Dana Masuk</b></td>
					<td width="10" style="border-radius: 0; margin: 0; border: 0px solid transparent;">:</td>
					<td style="border-radius: 0; margin: 0; border: 0px solid transparent;;" class="field">
						<div class="ui calendar date_calendar">
							<div class="ui input left icon">
						    	<i class="calendar icon"></i>
								<input type="text" class="@if(auth()->user()->hasRole('keuangan')) disa @else @endif" placeholder="Tanggal Dana Masuk" value="{{ $record->va->tgl_bayar }}" disabled="">	
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td style="border-radius: 0; margin: 0; border: 0px solid transparent;"><b>Nota Dinas Pembatalan</b></td>
					<td width="10" style="border-radius: 0; margin: 0; border: 0px solid transparent;">:</td>
					<td style="border-radius: 0; margin: 0; border: 0px solid transparent;" class="field">
						<input type="text" min="0" class="number @if(auth()->user()->hasRole('keuangan')) disa @else @endif" maxlength="16" name="nota_batal" value="{{ $record->pengembalian->nota_batal or '' }}" placeholder="Nota Dinas Pembatalan" readonly="">
					</td>
				</tr>
				<tr>
					<td style="border-radius: 0; margin: 0; border: 0px solid transparent;"><b>Nota Dinas Pengembalian Dana</b></td>
					<td width="10" style="border-radius: 0; margin: 0; border: 0px solid transparent;">:</td>
					<td style="border-radius: 0; margin: 0; border: 0px solid transparent;" class="field">
						<input type="text" min="0" class="number @if(auth()->user()->hasRole('keuangan')) disa @else @endif" maxlength="16" name="nota_kembali" value="{{ $record->pengembalian->nota_kembali or '' }}" placeholder="Nota Dinas Pengembalian Dana" readonly="">
					</td>
				</tr>
				<tr>
					<td style="border-radius: 0; margin: 0; border: 0px solid transparent;"><b>Jumlah Pengembalian Dana (Rp)</b></td>
					<td width="10" style="border-radius: 0; margin: 0; border: 0px solid transparent;">:</td>
					<td style="border-radius: 0; margin: 0; border: 0px solid transparent;" class="field">
						<div class="ui right labeled input">
							<label class="ui label">Rp.</label>
							<input type="text" min="0" class="number @if(auth()->user()->hasRole('keuangan')) disa @else @endif" maxlength="16" name="jumlah_dana" value="{{ $record->pengembalian->jumlah_dana or '' }}" placeholder="Jumlah Pengembalian Dana" data-inputmask="'alias' : 'numeric'" readonly="">
							<div class="ui basic label">.00</div>
						</div>
					</td>
				</tr>
				<tr>
					<td style="border-radius: 0; margin: 0; border: 0px solid transparent;"><b>Tanggal Pengembalian Dana</b></td>
					<td width="10" style="border-radius: 0; margin: 0; border: 0px solid transparent;">:</td>
					<td style="border-radius: 0; margin: 0; border: 0px solid transparent;;" class="field">
						<div class="ui calendar date_calendar">
							<div class="ui input left icon">
						    	<i class="calendar icon"></i>
								<input type="text" name="tgl_kembali" placeholder="Tanggal Pengembalian Dana" value="{{$record->pengembalian->tgl_kembali or ''}}" disabled="">
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="ui divider" style="border-radius: 0; border: 1px solid transparent;"></div>
		<div class="actions">
			<div class="ui black deny button" style="background-color: #363636;margin-left: -1px;">
				Batal
			</div>
			{{-- <div class="ui right floated save as page button" style="background-color: #00AEEF;">
				Submit
			</div> --}}
		</div>
	</form>
</div>

@include('scripts.inputmask')
