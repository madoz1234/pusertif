@extends('layouts.form')

@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
	</style>
@append

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			judul: ['empty'],
		};
	</script>
@endsection

@section('content-body')
	<form class="ui form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}
		<div class="ui very basic thin segment">
			<table class="ui compact table" style="border-radius: 0; margin: 0">
				<tr>
					<td width="150"><b>Jenis Pengujian</b></td>
					<td width="10">:</td>
					<td class="field">
						<select name="hari" class="watcher ui fluid search transparent dropdown">
							<option value="">Pilih Jenis Pengujian</option>
							<option value="Uji serah terima (MDU)">Uji serah terima (MDU)</option>
							<option value="Uji Rutin">Uji Rutin</option>
							<option value="Uji jenis">Uji jenis</option>
						</select>
					</td>
					<td width="150"><b>Lokasi Uji</b></td>
					<td width="10">:</td>
					<td class="field">
						<div class="ui transparent input">
							<input type="text" name="lokasi" placeholder="Lokasi">
						</div>
					</td>
				</tr>
				<tr>
					<td><b>Catatan</b></td>
					<td>:</td>
					<td colspan="4">
						<div class="ui transparent fluid input catatan">
							<textarea class="inline catatan" id="catatan" name="catatan" style="height: 30px" placeholder="Catatan"></textarea>
						</div>
					</td>
				</tr>
			</table>
			<div style="overflow-x: scroll;">
				<table class="ui compact celled table" style="border-radius: 0; margin: 5px 0;">
					<thead>
						<tr class="middle aligned">
							<th rowspan="2" width="30" class="center aligned">No</th>
							<th rowspan="2" class="center aligned" style="width: 200px">Spesifikasi</th>
							<th rowspan="2" class="center aligned" style="width: 200px">Mata Uji</th>
							<th rowspan="2" class="center aligned">Usulan Tarif</th>
							<th rowspan="2" class="center aligned">Jumlah Benda Uji</th>
							<th colspan="2" class="center aligned">Jadwal Tentative</th>
							<th colspan="2" class="center aligned">Jadwal Reschedule</th>
							<th colspan="2" class="center aligned">Realisasi</th>
							<th colspan="2" class="center aligned">Tanggal Laporan</th>
							<th rowspan="2" class="center aligned">Lampiran</th>
							<th rowspan="2" width="50" class="center aligned"><button type="button" class="ui green mini icon append tambah_detail button" data-tooltip="Tambah Data" data-position="left center"><i class="plus icon"></i></button></th>
						</tr>
						<tr class="middle aligned">
							<th style="">Start</th>
							<th>End</th>
							<th>Start</th>
							<th>End</th>
							<th>Start</th>
							<th>End</th>
							<th>Start</th>
							<th>End</th>
						</tr>
					</thead>
					<tbody class="detail fluid container">				
						<tr>
							<td class="center aligned">1</td>
							<td>
								<div class="ui transparent fluid input field spesifikasi">
									<input type="text" name="detail[0][spesifikasi]" placeholder="Spesifikasi">
								</div>
							</td>
							<td>
								<select name="detail[0][mata_uji]" class="watcher ui fluid search transparent dropdown multiple" multiple="">
									<option value="">Pilih Mata Uji</option>
									<option value="Mata Uji 1">Mata Uji 1</option>
									<option value="Mata Uji 2">Mata Uji 2</option>
									<option value="Mata Uji 3">Mata Uji 3</option>
								</select>
							</td>
							<td>
								<div class="ui transparent fluid input field">
									<input class="number" type="text" name="detail[0][tarif]" placeholder="Usulan Tarif">
								</div>
							</td>
							<td>
								<div class="ui transparent fluid input field">
									<input class="number" type="text" name="detail[0][jumlah]" placeholder="Jumlah Benda Uji">
								</div>
							</td>
							<td>
								<div class="ui tentative_start field">
									<div class="ui transparent input left icon">
										<i class="calendar icon"></i>
										<input name="detail[0][tentative_start]" type="text" placeholder="Tentative Start">
									</div>
								</div>
							</td>
							<td>
								<div class="ui tentative_end field">
									<div class="ui transparent input left icon">
										<i class="calendar icon"></i>
										<input name="detail[0][tentative_end]" type="text" placeholder="Tentative End">
									</div>
								</div>
							</td>
							<td>
								<div class="ui reschedule_start field">
									<div class="ui transparent input left icon">
										<i class="calendar icon"></i>
										<input name="detail[0][reschedule_start]" type="text" placeholder="Reschedule Start">
									</div>
								</div>
							</td>
							<td>
								<div class="ui reschedule_end field">
									<div class="ui transparent input left icon">
										<i class="calendar icon"></i>
										<input name="detail[0][reschedule_end]" type="text" placeholder="Reschedule End">
									</div>
								</div>
							</td>
							<td>
								<div class="ui realisasi_start field">
									<div class="ui transparent input left icon">
										<i class="calendar icon"></i>
										<input name="detail[0][realisasi_start]" type="text" placeholder="Realisasi Start">
									</div>
								</div>
							</td>
							<td>
								<div class="ui realisasi_end field">
									<div class="ui transparent input left icon">
										<i class="calendar icon"></i>
										<input name="detail[0][realisasi_end]" type="text" placeholder="Realisasi End">
									</div>
								</div>
							</td>
							<td>
								<div class="ui tgl_laporan_start field">
									<div class="ui transparent input left icon">
										<i class="calendar icon"></i>
										<input name="detail[0][tgl_laporan_start]" type="text" placeholder="Tanggal Laporan Start">
									</div>
								</div>
							</td>
							<td>
								<div class="ui tgl_laporan_end field">
									<div class="ui transparent input left icon">
										<i class="calendar icon"></i>
										<input name="detail[0][tgl_laporan_end]" type="text" placeholder="Tanggal Laporan End">
									</div>
								</div>
							</td>
							<td class="lefted">
								<div class="field">
									<div class="ui action choises input">
										<input type="text" readonly>
										<input type="file" name="detail[0][lampiran]" style="display: none!important;">
										<div class="ui icon button">
											<i class="cloud upload alternate icon"></i>
										</div>
									</div>
								</div>
							</td>
							<td class="center aligned">
								<button class="ui red mini icon remove hapus_data button" data-tooltip="Hapus Data" data-position="left center"><i class="remove icon"></i></button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="ui bottom attached segment">
			<div class="actions">
				<div class="ui two column grid">
					<div class="left aligned column">
						<div class="ui gray deny labeled icon button" onclick="window.history.back()">
							<i class="chevron left icon"></i>
							Kembali
						</div>
					</div>
					<div class="right aligned column">		
						<button type="button" class="ui positive right labeled icon save as page button">
							Simpan
							<i class="save icon"></i>
						</button>
					</div>
				</div>	
			</div>
		</div>
	</form>
@endsection
@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	//Calender
	$(document).ready(function() {
        var nowDate = new Date();
        var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), 0, 0, 0, 0);
        var tanggal = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);

	    var startCalendar = null;

    	$('.tentative_start').calendar({
    		type: 'date',
    		formatter: {
    			date: function (date, settings) {
    				if (!date) return '';
    				let momentDate = moment(date)
    				return momentDate.format('YYYY-MM-DD')
    			}
    		},
    		onChange: function () {
    			startCalendar = $(this)
    			var element = $(this).parents('tr').find('td div.tentative_end');
    			element.calendar({
    				type: 'date',
    				startCalendar: startCalendar,
    				formatter: {
    					date: function (date, settings) {
    						if (!date) return '';
    						let momentDate = moment(date)
    						return momentDate.format('YYYY-MM-DD')
    					}
    				}
    			})
    			$(this).parents('tr').find('td div.tentative_end').find('div').find('input').focus();
    		}
    	});

    	$('.reschedule_start').calendar({
    		type: 'date',
    		formatter: {
    			date: function (date, settings) {
    				if (!date) return '';
    				let momentDate = moment(date)
    				return momentDate.format('YYYY-MM-DD')
    			}
    		},
    		onChange: function () {
    			startCalendar = $(this)
    			var element = $(this).parents('tr').find('td div.reschedule_end');
    			element.calendar({
    				type: 'date',
    				startCalendar: startCalendar,
    				formatter: {
    					date: function (date, settings) {
    						if (!date) return '';
    						let momentDate = moment(date)
    						return momentDate.format('YYYY-MM-DD')
    					}
    				}
    			})
    			$(this).parents('tr').find('td div.reschedule_end').find('div').find('input').focus();
    		}
    	});

    	$('.realisasi_start').calendar({
    		type: 'date',
    		formatter: {
    			date: function (date, settings) {
    				if (!date) return '';
    				let momentDate = moment(date)
    				return momentDate.format('YYYY-MM-DD')
    			}
    		},
    		onChange: function () {
    			startCalendar = $(this)
    			var element = $(this).parents('tr').find('td div.realisasi_end');
    			element.calendar({
    				type: 'date',
    				startCalendar: startCalendar,
    				formatter: {
    					date: function (date, settings) {
    						if (!date) return '';
    						let momentDate = moment(date)
    						return momentDate.format('YYYY-MM-DD')
    					}
    				}
    			})
    			$(this).parents('tr').find('td div.realisasi_end').find('div').find('input').focus();
    		}
    	});

    	$('.tgl_laporan_start').calendar({
    		type: 'date',
    		formatter: {
    			date: function (date, settings) {
    				if (!date) return '';
    				let momentDate = moment(date)
    				return momentDate.format('YYYY-MM-DD')
    			}
    		},
    		onChange: function () {
    			startCalendar = $(this)
    			var element = $(this).parents('tr').find('td div.tgl_laporan_end');
    			element.calendar({
    				type: 'date',
    				startCalendar: startCalendar,
    				formatter: {
    					date: function (date, settings) {
    						if (!date) return '';
    						let momentDate = moment(date)
    						return momentDate.format('YYYY-MM-DD')
    					}
    				}
    			})
    			$(this).parents('tr').find('td div.tgl_laporan_end').find('div').find('input').focus();
    		}
    	});

	    $(".number").on("keypress keyup blur",function (e) {    
            $(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
        });

        $('.ui.dropdown').css({
        	'width': '200px'
        })

        $('.spesifikasi').css({
        	'width': '200px'
        })

	    var no = 1;
	    $(document).on('click', '.tambah_detail', function(e){
	    	var e = no++;
	    	var f = e+=1;
	    	var html = `
					<tr>
						<td class="center aligned">`+f+`</td>
						<td>
							<div class="ui transparent fluid input field">
								<input type="text" name="detail[`+e+`][spesifikasi]" placeholder="Spesifikasi">
							</div>
						</td>
						<td>
							<div>
								<select name="detail[`+e+`][mata_uji]" class="watcher ui fluid search transparent dropdown multiple" multiple="">
									<option value="">Pilih Mata Uji</option>
									<option value="Mata Uji 1">Mata Uji 1</option>
									<option value="Mata Uji 2">Mata Uji 2</option>
									<option value="Mata Uji 3">Mata Uji 3</option>
								</select>
							</div>
						</td>
						<td>
							<div class="ui transparent fluid input field">
								<input class="number" type="text" name="detail[`+e+`][tarif]" placeholder="Usulan Tarif">
							</div>
						</td>
						<td>
							<div class="ui transparent fluid input field">
								<input class="number" type="text" name="detail[`+e+`][jumlah]" placeholder="Jumlah Benda Uji">
							</div>
						</td>
						<td>
							<div class="ui tentative_start field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[`+e+`][tentative_start]" type="text" placeholder="Tentative Start">
								</div>
							</div>
						</td>
						<td>
							<div class="ui tentative_end field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[`+e+`][tentative_end]" type="text" placeholder="Tentative End">
								</div>
							</div>
						</td>
						<td>
							<div class="ui reschedule_start field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[`+e+`][reschedule_start]" type="text" placeholder="Reschedule Start">
								</div>
							</div>
						</td>
						<td>
							<div class="ui reschedule_end field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[`+e+`][reschedule_end]" type="text" placeholder="Reschedule End">
								</div>
							</div>
						</td>
						<td>
							<div class="ui realisasi_start field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[`+e+`][realisasi_start]" type="text" placeholder="Realisasi Start">
								</div>
							</div>
						</td>
						<td>
							<div class="ui realisasi_end field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[`+e+`][realisasi_end]" type="text" placeholder="Realisasi End">
								</div>
							</div>
						</td>
						<td>
							<div class="ui tgl_laporan_start field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[`+e+`][tgl_laporan_start]" type="text" placeholder="Tanggal Laporan Start">
								</div>
							</div>
						</td>
						<td>
							<div class="ui tgl_laporan_end field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[`+e+`][tgl_laporan_end]" type="text" placeholder="Tanggal Laporan End">
								</div>
							</div>
						</td>
						<td class="lefted">
							<div class="field">
								<div class="ui action choises input">
									<input type="text" readonly>
									<input type="file" name="detail[`+e+`][lampiran]" style="display: none!important;">
									<div class="ui icon button">
										<i class="cloud upload alternate icon"></i>
									</div>
								</div>
							</div>
						</td>
						<td class="center aligned">
							<button class="ui red mini icon remove hapus_data button" data-tooltip="Hapus Data" data-position="left center"><i class="remove icon"></i></button>
						</td>
					</tr>`;
	    	$('.detail.fluid.container').append(html);
	    	$('.timepickertime').calendar({
	    		ampm: false,
	    		type: 'time'
	    	});

	    	$('.watcher').dropdown({
	    	});

	    	var nowDate = new Date();
	    	var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), 0, 0, 0, 0);
	    	var tanggal = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);

	    	$(".number").on("keypress keyup blur",function (e) {    
	    		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	    	});

	    	var startCalendar = null;

	    	$('.tentative_start').calendar({
	    		type: 'date',
	    		formatter: {
	    			date: function (date, settings) {
	    				if (!date) return '';
	    				let momentDate = moment(date)
	    				return momentDate.format('YYYY-MM-DD')
	    			}
	    		},
	    		onChange: function () {
	    			startCalendar = $(this)
	    			var element = $(this).parents('tr').find('td div.tentative_end');
	    			element.calendar({
	    				type: 'date',
	    				startCalendar: startCalendar,
	    				formatter: {
	    					date: function (date, settings) {
	    						if (!date) return '';
	    						let momentDate = moment(date)
	    						return momentDate.format('YYYY-MM-DD')
	    					}
	    				}
	    			})
	    			$(this).parents('tr').find('td div.tentative_end').find('div').find('input').focus();
	    		}
	    	});

	    	$('.reschedule_start').calendar({
	    		type: 'date',
	    		formatter: {
	    			date: function (date, settings) {
	    				if (!date) return '';
	    				let momentDate = moment(date)
	    				return momentDate.format('YYYY-MM-DD')
	    			}
	    		},
	    		onChange: function () {
	    			startCalendar = $(this)
	    			var element = $(this).parents('tr').find('td div.reschedule_end');
	    			element.calendar({
	    				type: 'date',
	    				startCalendar: startCalendar,
	    				formatter: {
	    					date: function (date, settings) {
	    						if (!date) return '';
	    						let momentDate = moment(date)
	    						return momentDate.format('YYYY-MM-DD')
	    					}
	    				}
	    			})
	    			$(this).parents('tr').find('td div.reschedule_end').find('div').find('input').focus();
	    		}
	    	});

	    	$('.realisasi_start').calendar({
	    		type: 'date',
	    		formatter: {
	    			date: function (date, settings) {
	    				if (!date) return '';
	    				let momentDate = moment(date)
	    				return momentDate.format('YYYY-MM-DD')
	    			}
	    		},
	    		onChange: function () {
	    			startCalendar = $(this)
	    			var element = $(this).parents('tr').find('td div.realisasi_end');
	    			element.calendar({
	    				type: 'date',
	    				startCalendar: startCalendar,
	    				formatter: {
	    					date: function (date, settings) {
	    						if (!date) return '';
	    						let momentDate = moment(date)
	    						return momentDate.format('YYYY-MM-DD')
	    					}
	    				}
	    			})
	    			$(this).parents('tr').find('td div.realisasi_end').find('div').find('input').focus();
	    		}
	    	});

	    	$('.tgl_laporan_start').calendar({
	    		type: 'date',
	    		formatter: {
	    			date: function (date, settings) {
	    				if (!date) return '';
	    				let momentDate = moment(date)
	    				return momentDate.format('YYYY-MM-DD')
	    			}
	    		},
	    		onChange: function () {
	    			startCalendar = $(this)
	    			var element = $(this).parents('tr').find('td div.tgl_laporan_end');
	    			element.calendar({
	    				type: 'date',
	    				startCalendar: startCalendar,
	    				formatter: {
	    					date: function (date, settings) {
	    						if (!date) return '';
	    						let momentDate = moment(date)
	    						return momentDate.format('YYYY-MM-DD')
	    					}
	    				}
	    			})
	    			$(this).parents('tr').find('td div.tgl_laporan_end').find('div').find('input').focus();
	    		}
	    	});
	    });

		$(document).on('click', '.hapus_data', function (e){
			var row = $(this).closest('tr');
			row.remove();
		});
	});
</script>
@append
