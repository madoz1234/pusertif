<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Data {{ $title or '' }}</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="" method="POST">
		<input type="hidden" name="_method" value="PUT">
		<div class="ui grid">
			<div class="six wide column">
				<div class="ui card">
					<div class="center aligned content">
						<i class="huge circular file icon"></i>
						<input type="file" id="hidden-new-file" style="display: none">
					</div>
					<div class="ui bottom attached blue icon button" for="hidden-new-file">
						<i class="upload icon"></i>
						Upload
					</div>
				</div>
			</div>
			<div class="ten wide column">

				<div class="field">		
					<div class="ui input left icon">
						<i class="file icon"></i>
						<input type="text" placeholder="No. Laporan" name="no_laporan">
					</div>
				</div>
				<div class="field">		
					<div class="ui input left icon">
						<i class="file icon"></i>
						<input type="text" placeholder="No. Ekspedisi" name="no_ekspedisi">
					</div>
				</div>
				<div class="field">		
					<div class="ui input left icon">
						<i class="file icon"></i>
						<input type="text" placeholder="No. Resi" name="no_resi">
					</div>
				</div>
				<div class="field">		
					<div class="ui input left icon">
						<i class="file icon"></i>
						<input type="text" placeholder="No. Nota Buku" name="no_nota">
					</div>
				</div>

				<div class="field">		
					<div class="ui calendar">
						<div class="ui input left icon">
							<i class="calendar icon"></i>
							<input type="text" placeholder="Tanggal Selesai Pengiriman" name="tgl_selesai">
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript" charset="utf-8" async defer>
	$('.ui.calendar').calendar({
		type: 'date'
	});

	$(document).on('click', '#btn-tolak', function(e){
		var id = $(this).data("id");
		swal({
			title: 'Apakah anda yakin ?',
			text: "Data yang sudah ditolak, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Setuju',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
					url: "{{url($pageUrl)}}/on-change-pop-tolak/"+id,
					type: 'GET',
					dataType: 'json',
				})
				.done(function(response) {
					dt.draw();
				})
				.fail(function() {
					dt.draw();
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			}
		})

	});
</script>