@extends('layouts.form')

@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
		.fn{
			font-size: 12px;
		}
	</style>
@append

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			judul: ['empty'],
		};
	</script>
@endsection

@section('content-body')
<div class="ui top demo tabular menu">
	<div class="active item fn" data-tab="first">Detil Order</div>
	<div class="item fn" data-tab="second">Jenis & Jadwal</div>
	<div class="item fn" data-tab="third">Surat Penawaran</div>
	<div class="item fn" data-tab="four">Pengujian</div>
	<div class="item fn" data-tab="five">Riwayat Aktivitas</div>
</div>

<div class="ui bottom demo active tab" data-tab="first">
	<div class="two fields">
		<table class="ui compact table" style="border-radius: 0; margin: 0">
			<tr>
				<td width="250px"><label class="fn">Tgl Order</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">{{ DateToStringYear($record->tgl_order) }}</td>
			</tr>
			<tr>
				<td width="250px" class="fn"><label>No Order</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">{{ $record->no_order }}</td>
			</tr>
			<tr>
				<td width="250px" class="fn"><label>Layanan</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">{{ $record->pelayanan->nama }}</td>
			</tr>
			<tr>
				<td><label class="fn">Lingkup</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $record->lingkup->nama or '' }}</td>
			</tr>
			<tr>
				<td><label class="fn">Rencana Pelaksanaan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ BulanToString($record->rencana) }}</td>
			</tr>
			<tr>
				<td><label class="fn">Kelas</label></td>
				<td class="fn">:</td>
				<td class="fn">@if($record->kelas == '1')
					Reguler
					@else
					Prioritas
					@endif
				</td>
			</tr>
			<tr>
				<td><label class="fn">No Surat Permintaan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $record->no_surat or '-' }}</td>
			</tr>
			<tr>
				<td><label class="fn">Tgl Surat Permintaan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ DateToStringYear($record->tgl_surat) }}</td>
			</tr>
			<tr>
				<td><label class="fn">File Surat Permintaan</label></td>
				<td class="fn">:</td>
				<td class="fn">
					<div class="ui button preview" data-id="{{ $record->id }}" data-tooltip="Lihat" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<i class="file image outline icon" style="margin-left: 0px;margin-right: 0px;"></i>
					</div>
					<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<a href="{{ url($pageUrl).'/download/'.$record->id }}">
							<i class="download icon"></i></a>
						</div>
					</td>
			</tr>
			<tr>
				<td><label class="fn">Channel</label></td>
				<td class="fn">:</td>
				<td class="fn">@if($record->tipe == 1)
					Online
					@elseif($record->tipe == 2)
					AMS
					@else
					Manual
				@endif</td>
			</tr>
			<tr>
				<td><label class="fn">Peminta Jasa</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $record->user->pelanggans->perusahaan->nama or '' }}</td>
			</tr>
			<tr>
				<td><label class="fn">Kategori</label></td>
				<td class="fn">:</td>
				<td class="fn">
					@if($record->user->pelanggans->perusahaan->kategori == 0)
					PLN
					@elseif($record->user->pelanggans->perusahaan->kategori == 1)
					NON PLN
					@else
					A-PLN
					@endif
				</td>
			</tr>
			<tr>
				<td><label class="fn">Dokumen Pendukung</label></td>
				<td class="fn">:</td>
				<td class="fn">
					<div class="ui button preview_multiple" data-id="{{ $record->id }}" data-tooltip="Lihat" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<i class="file image outline icon" style="margin-left: 0px;margin-right: 0px;"></i>
					</div>
					@if(isset($record))
					@if(isset($record->files))
					@if($record->files->count() > 0)
					<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<a href="{{ url('download', $record->id).'/pendaftaran-pengujian' }}">
							<i class="download icon"></i></a>
						</div>
						@endif
						@endif
						@endif
					</td>
				</tr>
				<tr>
					<td><label class="fn">Catatan</label></td>
					<td class="fn">:</td>
					<td class="fn">{{ $record->catatan or '-' }}
					</td>
				</tr>
				<tr>
					<td><label class="fn">Keputusan </label></td>
					<td class="fn">:</td>
					<td class="left aligned">
						<a class="ui tag label fn" style="background-color:#00abffcc;">DITERIMA</a>
					</td>
				</tr>
				<tr>
					<td><label class="fn">Keterangan </label></td>
					<td class="fn">:</td>
					<td class="left aligned fn">
						@if($record->kaji_ulang)
						{{$record->kaji_ulang->keterangan}}
						@else
						-
						@endif
					</td>
				</tr>
		</table>
	</div>
	<div class="actions">
		<div class="ui two column grid">
			<div class="left aligned column">
				<br>
				<div class="ui gray deny labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
		</div>	
	</div>
</div>

<div class="ui bottom demo tab" data-tab="second">
	<table id="example" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
		<thead>
			<tr class="middle aligned">
				<th width="10" class="center aligned fn">No.</th>
				<th width="150" class="center aligned fn">Jenis Pengujian</th>
				<th width="150" class="center aligned fn">Merk</th>
				<th width="150" class="center aligned fn">Tipe</th>
				<th width="200" class="center aligned fn">Lokasi</th>
				<th width="200" class="center aligned fn">Mata Uji</th>
				<th width="250" class="center aligned fn">Spesifikasi</th>
				<th width="50" class="center aligned fn">Usulan Terkait Tarif</th>
				<th width="50" class="center aligned fn">Jumlah Benda Uji</th>
				<th width="150" class="center aligned" style="font-size: 12px;">Jadwal Pengujian Tentative
					<br><div class="ui label">Rencana : {{ BulanToString($record->rencana) }}</div>
				</th>
			</tr>
		</thead>
		<tbody class="detail fluid container detail">
			@if($kaji_ulang)
				@foreach($kaji_ulang->detail as $key => $data)
				<tr class="detail fn">
					<td class="center aligned">{{ $key+1 }}</td>
					<td class="left aligned">{{ $data->detail_pendaftaran->jenis->nama }}</td>
					<td class="left aligned">{{ $data->detail_pendaftaran->merk }}</td>
					<td class="left aligned">{{ $data->detail_pendaftaran->tipe }}</td>
					<td>
						<div class="ui bulleted list">
							@foreach($data->lokasi as $kiy => $val)
								<div class="item">
									<label class="isi">
										@if($val->jenis_lokasi == 1)
											In House :
										@else 
											On Site :
										@endif
									</label>
									{{ $val->ket_lokasi }}
								</div>
							@endforeach
						</div>
					</td>
					<td width="200">
						@if($data->mata_uji)
							<div class="ui ordered list">
								@foreach($data->mata_uji as $key => $cek)
									<div class="item">{{ $cek->mata_uji }}</div>
								@endforeach
							</div>
						@else
							-
						@endif
					</td>
					<td style="text-align: justify;text-justify: inter-word;">
						{{$data->spesifikasi or '-'}}
					</td>
					<td class="left aligned" style="text-align: justify;text-justify: inter-word;">
						{{$data->tarif or '-'}}
					</td>
					<td class="right aligned">
						{{ rtrim(rtrim(number_format($data->jumlah,2,',','.'), '0'), ',')}}
						<label>{{$data->detail_pendaftaran->satuan->satuan or '-'}}</label>
					</td>
					<td class="center aligned">
						{{ DateToStringYear($data->tentative_start) }} - <br><label style="margin-left: -6px;">{{ DateToStringYear($data->tentative_end) }}</label>
					</td>
				</tr>
				@endforeach
			@else
				<tr class="detail">
					<td class="center aligned fn" colspan="8">Tidak ada Data</td>
				</tr>
			@endif
		</tbody>
	</table>
	<div class="actions">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
	</div>
</div>

<div class="ui bottom demo tab" data-tab="third">
	<form class="ui form" id="dataForm" action="{{ url($pageUrl.'saveKomponen') }}" method="POST">
		{!! csrf_field() !!}
			<div class="ui yellow segment">
				<div class="field " style="width: 100%">
					<div class="ui form">
						<div class="four fields">
							<div class="field">
								<label>No Surat Penawaran</label>
								<input type="hidden" name="no_surat" value="{{ $surat->no_surat }}">
								{{ $surat->no_surat }}
							</div>
							<div class="field">
								<label>Tgl Surat Penawaran</label>
								<input type="hidden" name="tgl_surat" value="{{ date("Y-m-d") }}">
								{{DateToStringYear(date("Y-m-d"))}}
							</div>
							<div class="field">
								<label>Status</label>
								<div class="field">
									<p style="text-align: justify;text-justify: inter-word;">
										@if($surat->adendum_status <= 0)
											Normal (Adendum 0)
										@else
											Adendum {{ $surat->adendum_status }}
										@endif</p>
								</div>
							</div>
							<div class="field">
								<label>Kelas</label>
								<div class="field">
									@if($data->detail_pendaftaran->pp->kelas == 1)
										Reguler
									@else 
										Prioritas
									@endif
								</div>
							</div>
							<div class="field">
								<label>Total Sesudah PPn</label>
								<div class="field">
									Rp. {{ rtrim(rtrim(number_format($surat->total,2,',','.'), '0'), ',')}}
								</div>
							</div>
						</div>
					</div>
					<div class="ui top attached">
						<a href="" class="ui red ribbon label">Komponen RAB</a>
						<table id="example" class="ui celled compact red table display" id="table-rab" width="100%" cellspacing="0">
							<thead>
								<tr class="center aligned">
									<th>No</th>
									<th>Uraian</th>
									<th>Nominal (Rp)</th>
								</tr>
							</thead>
							<tbody class="komponens">
								@php
									$total = 0;
								@endphp
								@foreach($surat->detail as $kiy => $cik)
								<tr class="data_komponens" data-id="1">
									<td class="center aligned numbor numboor-1" style="width: 50px;">{{ $kiy+1 }}</td>
									<td width="300px">
										<div class="ui transparent fluid input field">
											{{$cik->komponen}}
										</div>
									</td>
									<td width="200px">
										<div class="ui transparent" style="text-align: right;">
											{{ rtrim(rtrim(number_format($cik->nominal,2,',','.'), '0'), ',')}}
											@php
												$total += $cik->nominal;
											@endphp
										</div>
									</td>
								</tr>
								@endforeach
								@php
									$ppn = (($total/100)*10);
									$total_ppn = ($total + $ppn);
								@endphp
							</tbody>
							<tfoot>
								<tr>
									<td style="border-top: 2px solid rgba(34,36,38,.1);"></td>
									<td scope="row" style="text-align: right;border-top: 2px solid rgba(34,36,38,.1);border-left: none;">Total Sebelum PPn</td>
									<td style="text-align: right;border-top: 2px solid rgba(34,36,38,.1);" class="total">{{ rtrim(rtrim(number_format($total,2,',','.'), '0'), ',')}}</td>
								</tr>
								<tr>
									<td style="border-top: 1px solid rgba(34,36,38,.1);"></td>
									<td scope="row" style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);border-left: none;">PPn</td>
									<td style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);" class="total">{{ rtrim(rtrim(number_format($ppn,2,',','.'), '0'), ',')}}</td>
								</tr>
								<tr>
									<td style="border-top: 1px solid rgba(34,36,38,.1);"></td>
									<td scope="row" style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);border-left: none;">Total Sesudah PPn</td>
									<td style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);" class="total">{{ rtrim(rtrim(number_format($total_ppn,2,',','.'), '0'), ',')}}</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
	</form>
	<div class="actions">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
	</div>
</div>
<div class="ui bottom demo tab" data-tab="four">
	<table id="example2" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
		<thead>
			<tr class="middle aligned">
				<th width="10" class="center aligned fn">#</th>
				<th width="200" class="center aligned fn">Jenis Pengujian</th>
				<th width="250" class="center aligned fn">Nama Barang</th>
				<th width="150" class="center aligned fn">Merk</th>
				<th width="150" class="center aligned fn">No Seri</th>
				<th width="150" class="center aligned fn">Tgl Penerimaan Barang Uji</th>
				<th width="300" class="center aligned fn">Pelaksana Verifikasi</th>
				<th width="150" class="center aligned fn">Hasil Verifikasi</th>
				<th width="150" class="center aligned fn">Tgl Verifikasi Barang Uji</th>
				<th width="250" class="center aligned fn">Catatan</th>
				<th width="250" class="center aligned fn">No FPP/FPK</th>
				<th width="300" class="center aligned fn">Pelaksana Pengujian</th>
				<th width="200" class="center aligned fn">Tgl Selesai Pengujian</th>
				<th width="200" class="center aligned fn">Hasil Pengujian</th>
				<th width="300" class="center aligned fn">Pembuat Laporan</th>
				<th width="200" class="center aligned fn">Barang Uji Kembali</th>
			</tr>
		</thead>
		<tbody class="detail fluid container detail fn">
			@php
				$i=1;
			@endphp
			@if($pengujian->penerimaan->count() > 0)
				@foreach($pengujian->penerimaan->detail_penerimaan_barang->where('flag', 0)->groupBy('group_id') as $key => $row)
						@foreach($row as $kuy => $data)
							@if($kuy == 0)
								<tr>
									<td rowspan="{{$row->count()}}">{{$i}}</td>
									<td rowspan="{{$row->count()}}">{{$data->detail_pp->jenis->nama}}</td>
									<td>{{ $data->nama_barang }} [{{$data->urutan}}/{{$data->max}}]</td>
									<td rowspan="{{$row->count()}}">{{ $data->detail_pp->merk }}</td>
									<td>{{$data->no_seri}}</td>
									<td class="center aligned" rowspan="{{$row->count()}}">{{ DateToStringYear($data->penerimaan->tanggal_terima) }}</td>
									<td rowspan="{{$row->count()}}" class="center aligned">
										{{$data->detail_pengujian->pelaksana->nama}}
									</td>
									<td class="center aligned" rowspan="{{$row->count()}}">
										@if($data->detail_pengujian->verifikasi == 1)
											<a class="ui tag label fn" style="background-color:#00abffcc;">DITERIMA</a>
										@elseif($data->detail_pengujian->verifikasi == 1)
											<a class="ui tag label fn" style="background-color:#dd0000cc;">DITOLAK</a>
										@else 
											-
										@endif
									</td>
									<td class="center aligned" rowspan="{{$row->count()}}">
										@if($data->detail_pengujian)
											@if($data->detail_pengujian->tgl_selesai)
												{{ DateToStringYear(Carbon\Carbon::parse($data->detail_pengujian->tgl_selesai)->format('Y-m-d')) }}
											@else 
												-
											@endif
										@else
											-
										@endif
									</td>
									<td style="text-align:justify;text-justify:auto;">{!! readMoreText($data->catatan, 50) !!}</td>
									<td rowspan="{{$row->count()}}">
										{{ $data->detail_pengujian->fpp_fpk or '-' }}
									</td>
									<td rowspan="{{$row->count()}}" class="center aligned">
										@if($data->detail_pengujian->detailpelaksana)
											{{$data->detail_pengujian->detailpelaksana->pelaksana->nama}}
										@else
											-
										@endif
									</td>
									<td class="center aligned" rowspan="{{$row->count()}}">
										@if($data->detail_pengujian->detailpelaksana)
											@if($data->detail_pengujian->detailpelaksana->tgl_selesai)
												{{ DateToStringYear(Carbon\Carbon::parse($data->detail_pengujian->detailpelaksana->tgl_selesai)->format('Y-m-d')) }}
											@else 
												-
											@endif
										@else
											-
										@endif
									</td>
									<td class="center aligned" rowspan="{{$row->count()}}">
										@if($data->detail_pengujian->detailpelaksana)
											@if($data->detail_pengujian->detailpelaksana->status_hasil_pengujian == 1)
												<a class="ui tag label fn" style="background-color:#00FF00;">BERHASIL</a>
											@elseif($data->detail_pengujian->detailpelaksana->status_hasil_pengujian == 2)
												<a class="ui tag label fn" style="background-color:#dd0000cc;">GAGAL</a>
											@else
												<div class="ui label">
													<i class="hourglass end icon"></i>Menunggu Hasil Pengujian
												</div>
											@endif
										@else 
											-
										@endif
									</td>
									<td rowspan="{{$row->count()}}">
										@if($data->detail_pengujian->detailpelaksana)
											@if($data->detail_pengujian->detailpelaksana->status == 0)
												<div class="ui label">
													<i class="hourglass end icon"></i>Menunggu Verifikasi Hasil Pengujian
												</div>
											@else
												@if($data->detail_pengujian->detailpelaksana->pelaksana_laporan_id !== 1)
													{{ $data->detail_pengujian->detailpelaksana->pelaksanalaporan->nama }}
												@else
													<div class="ui label">
														<i class="hourglass end icon"></i>Menunggu Verifikasi Asman
													</div>
												@endif
											@endif
										@else 
											-
										@endif 
									</td>
									<td rowspan="{{$row->count()}}">
										@if($data->detail_pengujian->detailpelaksana)
											@if($data->detail_pengujian->detailpelaksana->status_barang == 0)
												<div class="ui label">
													<i class="close icon"></i> Barang tidak dikembalikan
												</div>
											@else
												<div class="ui label">
													<i class="check icon"></i> Barang dikembalikan
												</div>
											@endif
										@else 
										-
										@endif
									</td>
								</tr>
							@else
								<tr>
									<td style="border-left: 1px solid #e8e9e9">{{ $data->nama_barang }} [{{$data->urutan}}/{{$data->max}}]</td>
									<td>{{$data->no_seri}}</td>
									<td style="text-align:justify;text-justify:auto;">{!! readMoreText($data->catatan, 50) !!}</td>
								</tr>
							@endif
						@endforeach
						@php
							$i++;
						@endphp
				@endforeach
			@else 	
				<tr class="middle aligned">
					<td class="center aligned" colspan="11">Tidak Ada Data</td>
				</tr>
			@endif
		</tbody>
	</table>
	<div class="actions">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
	</div>
</div>
<div class="ui bottom demo tab" data-tab="five">
	{!! riwayatPengujian($record) !!}
	{!! riwayatAktivitas($record) !!}
	<div class="actions">
		<div class="ui two column grid">
			<div class="left aligned column">
				<br>
				<div class="ui gray deny labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
		</div>	
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
	$(document).on('click', '.preview', function(e){
			var id = $(this).data('id');
			var url = "{{ url($pageUrl) }}/"+id+"/preview";
			console.log(url)
			loadModal({
                'url' : url,
                'modal' : 'longer modal',
                'formId' : '#dataForm',
                'onShow' : function(){ 
                    onShow();
                },
            })
		});

		$(document).on('click', '.preview_multiple', function(e){
			var id = $(this).data('id');
			var url = "{{ url($pageUrl) }}/"+id+"/preview-multiple/pendaftaran-pengujian";
			loadModal({
                'url' : url,
                'modal' : 'longer modal',
                'formId' : '#dataForm',
                'onShow' : function(){ 
                    onShow();
                },
            })
		});	
});
</script>
@append