@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
		/*.disposisi.dropdown .menu .item:nth-child(1) {
			background-color: #00dd00cc;
			color: #fff;
		}
		.disposisi.dropdown .menu .item:nth-child(2) {
			background-color: #dd0000cc;
			color: #fff;
		}*/
	</style>
@append
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Upload File</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$id)."/saveUpload" }}" method="POST">
      {!! csrf_field() !!}
      <input type="hidden" name="_method" value="PUT">
      <input type="hidden" name="id" value="{{ $id }}">
		<div class="ui form">
			<table class="ui compact table" style="border-radius: 0; margin: 0">
				<tr>
					<td style="width: 40%;"><b>Periode</b></td>
					<td width="10">:</td>
					<td style="width: 100%;">
						<div class="field">
							<div class="ui calendar labeled input periode">
								<div class="ui input left icon">
									<i class="calendar icon date"></i>
									<input name="periode" type="text" placeholder="Periode">
								</div>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td style="width: 40%;"><b>File Realisasi Biaya</b></td>
					<td width="10">:</td>
					<td style="width: 100%;">
						<div class="field">
							<div class="ui action choises input">
								<input type="text" class="isi_upload" readonly placeholder="Format .doc, .xls, .pdf, .jpg, .jpeg, .png">
								<input type="file" id="data_upload" name="data_upload" style="display: none!important;">
								<div class="ui icon data_upload button">
									<i class="cloud upload alternate icon"></i>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="ui divider"></div>
		<div class="actions">
			<div class="ui black deny button" style="background-color: #363636;margin-left: -1px;">
				Batal
			</div>
			<div class="ui right floated save as page button" style="background-color: #00AEEF;">
				Submit
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		switch($('.disposisi.dropdown').dropdown('get text')){
				case 'Dialihkan':
					$('.disposisi.dropdown').css({
						'background-color': '#ffc000!important',
						'color': '#000'
					})
					break;
				case 'Diteruskan':
					$('.disposisi.dropdown').css({
						'background-color': '#00b0f0!important',
						'color': '#000'
					})
					break;
			}

		$(document).on('change', '.disposisi.dropdown', function (e){
			if($(this).find(":selected").val() == 1){
				$('#penerima').dropdown('clear');
				$(this).css({
					'background-color': '#ffc000!important',
					'color': '#000'
				})
			}else if($(this).find(":selected").val() == 2){
				$('#penerima').dropdown('clear');
				$(this).css({
					'background-color': '#00b0f0!important',
					'color': '#000'
				})
			}else{
				$('#penerima').dropdown('clear');
			}

			$.ajax({
				url: '{{ url('ajax/option/disposisi-msb') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					dispo_id: $(this).find(":selected").val(),
					pengirim: $('input[name=pengirim_id]').val(),
					layanan: $('input[name=layanan_id]').val()
				},
			})
			.done(function(response) {
				$('select[name^=penerima_id]').html(response)
			})
			.fail(function() {
				console.log("error");
			});
		});

		$(document).on('change', '[name=data_upload]', function (e) {
			var count = e.target.files.length;
			if (count > 0) {
				$('.isi_upload').val(count+` Files Selected`);
			}
		});
	});
</script>