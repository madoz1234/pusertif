@extends('layouts.list-realisasi')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
	<div class="field">
		<input name="filter[no_order]" placeholder="No Order" type="text">
	</div>
    <div class="field">
        <div class="ui left icon date input">
            <i class="calendar icon"></i>
            <input type="text" name="filter[tanggal_order]" placeholder="Tanggal Order">
        </div>
    </div>
    <div class="field">
      <select name="filter[jenis_pelayanan_id]" class="watcher ui fluid search dropdown">
        {!! \App\Models\Master\JenisPelayanan::options('nama', 'id', [], 'Pilih Layanan') !!}
      </select>
    </div>
    <div class="field">
      <select name="filter[perusahaan_id]" class="watcher ui fluid search dropdown">
        {!!
          \App\Models\Master\Perusahaan::optionsa('nama', 'id', ['filters' => [function($q){
            return $q->where('status', 1);
          }],
          'selected' => old('id')
        ], 'Nama Perusahaan')
        !!}
      </select>
    </div>
    <div class="field">
        <input type="text" name="filter[wbs_io]" placeholder="No WBS/IO">
    </div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('filterdata')
	d.no_order = $("input[name='filter[no_order]']").val();
    d.tanggal_order = $("input[name='filter[tanggal_order]']").val();
    d.no_surat = $("input[name='filter[no_surat]']").val();
    d.jenis_pelayanan_id = $("select[name='filter[jenis_pelayanan_id]']").val();
    d.perusahaan_id = $("select[name='filter[perusahaan_id]']").val();
	d.wbs_io = $("input[name='filter[wbs_io]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			username: 'empty',
			email: 'empty',
			roles: 'empty',
		};
	</script>
@endsection

@section('tables')
	<div class="ui inverted loading dimmer">
		<div class="ui text loader">Loading</div>
	</div>
	<div class="ui top demo tabular menu">
	  <div class="active item" data-tab="first">On Progress @if($satu > 0)<span style="background-color:red;"class="ui circular label">{{ $satu }}</span>@endif</div>
	  <div class="item" data-tab="second">Historis</div>
	</div>
	<div class="ui bottom demo active tab" data-tab="first">
		@if(isset($structs['listStruct']))
			<table id="listTable" class="ui celled compact red table display" width="100%" cellspacing="0">
				<thead>
					<tr>
						@foreach ($structs['listStruct'] as $struct)
						<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
						@endforeach
					</tr>
				</thead>
				<tbody>
					@yield('tableBody')
				</tbody>
			</table>
		@endif
	</div>
	<div class="ui bottom demo tab" data-tab="second">
		@if(isset($structs['listStruct2']))
		<table id="listTable2" class="ui celled compact red table display" width="100%" cellspacing="0">
			<thead>
				<tr>
					@foreach ($structs['listStruct2'] as $struct)
					<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
					@endforeach
				</tr>
			</thead>
			<tbody>
				@yield('tableBody')
			</tbody>
		</table>
		@endif
	</div>
@endsection
@section('init-modal')
	<script>
		onShow = function(){
        	var nowDate = new Date();
        	var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), 0, 0, 0, 0);
        	var tanggal = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);
        	$('.periode').calendar({
        		type: 'month',
        		maxDate: nowDate,
        		formatter: {
        			date: function (date, settings) {
        				if (!date) return '';
        				var day = date.getDate();
        				var month = date.getMonth() + 1;
        				var year = date.getFullYear();
        				var month = month < 10 ? '0' + month : '' + month;
        				return year+'-'+month;
        			}
        		}
        	});
			$(document).on('click', '.upload-realisasi', function (e){
				$("#dataForm").form('validate form');
				if($("#dataForm").form('is valid')){
					$('#formModal').find('.loading.dimmer').addClass('active');
					$("#dataForm").ajaxSubmit({
						success: function(resp){
							// console.log(resp);
							$("#formModal").modal('hide');
							swal(
								'Terupload!',
								'Data berhasil diupload dan terdapat '+resp.fail+' data yang tidak dapat diupload',
								'success'
								).then((result) => {
									$('#formModal').find('.loading.dimmer').removeClass('active');
									if(document.getElementById('listTable') !=null){
										window.location.reload();
									}else{
										window.location.reload();
									}
									return true;
								})
						},
						error: function(resp){
								$('#formModal').find('.loading.dimmer').removeClass('active');
								$.each(resp.responseJSON.errors, function(index, val) {
									clearError(index,val);
									showError(index,val);
								});
								time = 7;
								interval = setInterval(function(){
									time--;
									if(time == 0){
										$('.red.error-label').remove();
										$('.pointing.prompt.label.transition.visible').remove();
										$('.error').each(function (index, val) {
											$(val).removeClass('error');
										});
										clearInterval(interval);
									}
								},1000);
						}
					});
				}
			});
            return false;
        };
	</script>
@endsection
@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	$('.date').calendar({
        type: 'date',
        formatter: {
          date: function (date, settings) {
            if (!date) return '';
            let momentDate = moment(date)
            return momentDate.format('DD/MM/YYYY')
          }
        }
    })
		$(document).on('click', '.upload-file', function (e){
			var id = $(this).data("id");
			var url = "{{ url($pageUrl) }}/"+id+"/upload"
			loadModal({
				'url' : url,
				'modal' : 'tiny modal',
				'formId' : '#dataForm',
				'onShow' : function(){ 
					onShow();
				},
			})
		});

		$(document).on('click', '.upload-template', function (e){
			var url = "{{ url($pageUrl) }}/upload-template";
			loadModal({
				'url' : url,
				'modal' : 'tiny modal',
				'formId' : '#dataForm',
				'onShow' : function(){ 
					onShow();
				},
			})
		});

		$(document).on('click', '.template', function(e){
			var url = "{{ url($pageUrl) }}/export";
			window.location = url;
		});

		$(document).on('click', '.upload-ulang', function (e){
			var id = $(this).data("id");
			var url = "{{ url($pageUrl) }}/"+id+"/uploadUlang"
			loadModal({
				'url' : url,
				'modal' : 'tiny modal',
				'formId' : '#dataForm',
				'onShow' : function(){ 
					onShow();
				},
			})
		});
</script>
@append

