@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
	</style>
@append
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Upload File</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl)."/upload-template" }}" method="POST">
      {!! csrf_field() !!}
		<div class="ui form">
			<table class="ui compact table" style="border-radius: 0; margin: 0">
				<tr>
					<td style="width: 40%;"><b>Periode</b></td>
					<td width="10">:</td>
					<td style="width: 100%;">
						<div class="field">
							<div class="ui calendar labeled input periode">
								<div class="ui input left icon">
									<i class="calendar icon date"></i>
									<input name="periode" type="text" placeholder="Periode">
								</div>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td style="width: 40%;"><b>File Realisasi Biaya</b></td>
					<td width="10">:</td>
					<td style="width: 100%;">
						<div class="field">
							<div class="ui action choises input">
								<input type="text" class="isi_upload" readonly placeholder="Format .xls, .xlsx">
								<input type="file" id="data_upload" name="data_upload" style="display: none!important;" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
								<div class="ui icon data_upload button">
									<i class="cloud upload alternate icon"></i>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="ui divider"></div>
		<div class="actions">
			<div class="ui black deny button" style="background-color: #363636;margin-left: -1px;">
				Batal
			</div>
			<div class="ui right floated upload-realisasi button" style="background-color: #00AEEF;">
				Submit
			</div>
		</div>
	</form>
</div>