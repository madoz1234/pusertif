<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="" method="POST">
		<div class="ui error message">
		</div>
		{!! csrf_field() !!}
	<table class="ui compact table" style="border-radius: 0; margin: 0">
		<tr>
			<td width="140px"><label>No WBS/IO </label></td>
			<td width="5px">:</td>
			<td>2018-001-0001</td>
		</tr>
	</table>
	<br>
	  <div class="field">
      	<div class="field">
    		<label>Lampiran</label>
    		<div class="field">
    			<div class="ui action choises input">
    				<input type="text" readonly>
    				<input type="file" name="detail[0][lampiran]" style="display: none!important;">
    				<div class="ui icon button">
    					<i class="cloud upload alternate icon"></i>
    				</div>
    			</div>
    		</div>
      	</div>
	  </div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>