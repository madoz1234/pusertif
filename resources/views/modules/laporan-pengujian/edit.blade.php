<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data {{ $title or '' }}</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="" method="POST">
		<input type="hidden" name="_method" value="PUT">
		<div class="ui form">
			<div class="field">		
				<div class="fields">
					<div class="sixteen wide field">
						<input type="text" placeholder="Nomor Laporan" name="nomor">
					</div>
				</div>
			</div>

			<div class="field">		
				<div class="fields">
					<div class="six wide field">
						<div class="two fields">
							<div class="field">		
								<div class="ui calendar">
									<div class="ui input left icon">
										<i class="calendar icon"></i>
										<input type="text" placeholder="Tanggal Mulai" name="mulai">
									</div>
								</div>
							</div>
							<p style="text-align: center; margin: 10px; ">s.d.</p>
							<div class="field">		
								<div class="ui calendar">
									<div class="ui input left icon">
										<i class="calendar icon"></i>
										<input type="text" placeholder="Tanggal Sampai" name="selesai">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="ten wide field">
						<div class="field">
							<select class="ui search dropdown">
								<option value="">-- Pilih Disposisi --</option>
								<option value="1">Afif</option>
								<option value="2">Ei Muhdas</option>
								<option value="3">Yudi Heriyadi</option>
								<option value="4">Ariandiky Eko S</option>
								<option value="5">Florentinus Erick</option>
								<option value="6">Dedit</option>
								<option value="7">Agus Warsyun</option>
							</select>
						</div> 
					</div>
				</div>
			</div>
		</div>
		{{-- <div class="field">		
			<div class="fields">
				<div class="six wide field">
					<div class="two fields">
						<div class="field">		
							<div class="ui mom_statrt" id="from">
								<div class="ui input left icon">
									<i class="calendar icon"></i>
									<input type="text" placeholder="Tanggal Mulai" name="mulai">
								</div>
							</div>
						</div>
						<p style="text-align: center; margin: 10px; ">s.d.</p>
						<div class="field">		
							<div class="ui mom_end" id="to">
								<div class="ui input left icon">
									<i class="calendar icon"></i>
									<input type="text" placeholder="Tanggal Sampai" name="selesai">
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="ten wide field">
					<div class="field">
						<select class="ui search dropdown">
							<option value="">Select Country</option>
							<option value="AF">Afghanistan</option>
						</select>
					</div> 
				</div>
			</div>
		</div> --}}
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript" charset="utf-8" async defer>
		//Calender
		$('.ui.calendar').calendar({
		  type: 'date'
		});

		//ditolak
		$(document).on('click', '#btn-tolak', function(e){
			var id = $(this).data("id");
			// $(".loading").addClass('active');
			swal({
				title: 'Apakah anda yakin ?',
				text: "Data yang sudah ditolak, tidak dapat dikembalikan!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Setuju',
				cancelButtonText: 'Batal'
			}).then((result) => {
				if (result) {
					$.ajax({
		                url: "{{url($pageUrl)}}/on-change-pop-tolak/"+id,
		                type: 'GET',
		                dataType: 'json',
		            })
		            .done(function(response) {
		                dt.draw();
		                // dt2.draw();
						// $(".loading").removeClass('active');
		                // window.location.reload();
		            })
		            .fail(function() {
		            	dt.draw();
		    //             dt2.draw();
						// $(".loading").removeClass('active');
		                console.log("error");
		            })
		            .always(function() {
		                console.log("complete");
		            });
				}
			})
			
		});
	</script>