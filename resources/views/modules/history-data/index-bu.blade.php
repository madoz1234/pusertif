@extends('layouts.list')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('scripts')
<script>
    $('.menu .item').tab()
    $('.month').calendar({
        type: 'month',
        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                var month = (date.getMonth() + 1) + '';
                if (month.length < 2) {
                    month = '0' + month;
                }
                var year = date.getFullYear();
                return month + '/' + year;
            }
        }
    })
</script>
@append

@section('filters')
<div class="field">
  <select name="jenis" class="ui dropdown selection search">
      <option value="">--pilih--</option>
      <option value="0">Semua</option>
      <option value="1">Jenis Layanan</option>
      <option value="2">Jenis Uji</option>
      <option value="3">Status Progress</option>
  </select>
</div>
<div class="field">
    <div class="ui left icon month input">
        <i class="calendar icon"></i>
        <input type="text" name="filter[bulan]" placeholder="Bulan">
    </div>
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
  <i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
  <i class="refresh icon"></i>
</button>
@endsection

@section('toolbars')
<span class="ui blue left labeled icon button"><i class="download icon"></i> Download Laporan</span>
@endsection

@section('js-filters')
d.no_laporan = $("input[name='filter[no_laporan]']").val();
d.no_ekspedisi = $("input[name='filter[no_ekspedisi]']").val();
d.no_resi = $("input[name='filter[no_resi]']").val();
d.no_nota = $("input[name='filter[no_nota]']").val();
d.tgl_selesai = $("input[name='filter[tgl_selesai]']").val();
@endsection

@section('rules')
<script type="text/javascript">
  formRules = {
     username: 'empty'
 };
</script>
@endsection

@section('tables')
<table id="listTable" class="ui celled compact red table display" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="one wide center aligned">No</th>
            <th class="two wide center aligned">Tanggal</th>
            <th class="center aligned">Jenis Layanan</th>
            <th class="center aligned">Jenis Uji</th>
            <th class="one wide center aligned">Status</th>
            <th class="one wide center aligned">Jumlah Laporan</th>
            <th class="one wide center aligned">Efektifitas Biaya</th>
            <th class="one wide center aligned">Time Achievement</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="center aligned">1</td>
            <td class="center aligned">31/01/2019</td>
            <td class="center aligned">Pengujian Alat</td>
            <td class="center aligned">Uji A</td>
            <td class="center aligned">
                <span class="ui green fluid label">Selesai</span>
            </td>
            <td class="center aligned">
                <span class="ui green fluid label">12/12</span>
            </td>
            <td class="center aligned">
                <span class="ui orange fluid label">50%</span>
            </td>
            <td class="center aligned">
                <span class="ui green fluid label">90%</span>
            </td>
        </tr>
        <tr>
            <td class="center aligned">2</td>
            <td class="center aligned">31/01/2019</td>
            <td class="center aligned">Kalibrasi</td>
            <td class="center aligned">Uji B</td>
            <td class="center aligned">
                <span class="ui orange fluid label">Progress</span>
            </td>
            <td class="center aligned">
                <span class="ui orange fluid label">6/12</span>
            </td>
            <td class="center aligned">
                <span class="ui green fluid label">80%</span>
            </td>
            <td class="center aligned">
                <span class="ui red fluid label">20%</span>
            </td>
        </tr>
        <tr>
            <td class="center aligned">3</td>
            <td class="center aligned">31/01/2019</td>
            <td class="center aligned">Kalibrasi</td>
            <td class="center aligned">Uji C</td>
            <td class="center aligned">
                <span class="ui red fluid label">Pending</span>
            </td>
            <td class="center aligned">
                <span class="ui red fluid label">0/12</span>
            </td>
            <td class="center aligned">
                <span class="ui red fluid label">0%</span>
            </td>
            <td class="center aligned">
                <span class="ui red fluid label">10%</span>
            </td>
        </tr>
    </tbody>
</table>
@endsection

@section('init-modal')
<script>
        //Calender
        $('.ui.calendar').calendar({
          type: 'date'
      });

        onShow = function(){
            $('.checkbox').checkbox();
            $('.ui.dropdown').dropdown({
                onChange: function(value) {
                    var target = $(this).dropdown();
                    if (value!="") {
                        target
                        .find('.dropdown.icon')
                        .removeClass('dropdown')
                        .addClass('delete')
                        .on('click', function() {
                            target.dropdown('clear');
                            $(this).removeClass('delete').addClass('dropdown');
                            return false;
                        });
                    }
                }
            });
            // force onChange  event to fire on initialization
            $('.ui.dropdown')
            .closest('.ui.selection')
            .find('.item.active').addClass('qwerty').end()
            .dropdown('clear')
            .find('.qwerty').removeClass('qwerty')
            .trigger('click');

            $('[name=display_name]').on('change, keyup', function(event) {
               var display_name = $(this).val();
               $('[name=name]').val(slugify(display_name));
           });

            return false;
        };

        // $(document).on('click', '.moda.button', function(e){
        //     var id = $(this).data('id');
        //     var url = "{{ url($pageUrl) }}/"+id+"/edit";

        //     loadModal(url);
        // });

        $('.ui.calendar').calendar({
          type: 'date'
      });
</script>
@endsection

