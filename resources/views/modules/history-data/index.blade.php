@extends('layouts.list-history')
@section('styles')
<style type="text/css">
	.DTFC_LeftBodyLiner{
		overflow-y: none !important;
		overflow-x: hidden;
	}
	.DTFC_RightBodyLiner{
		overflow-y: none !important;
		overflow-x: hidden;
	}
</style>
@append
@section('scripts')
<script>
</script>
@append

@section('filters')
<div class="field">
  <select name="jenis_pelayanan_id" class="watcher ui fluid search dropdown">
        {!! \App\Models\Master\JenisPelayanan::options('nama', 'id', [], 'Pilih Layanan') !!}
  </select>
</div>
<button type="button" class="ui teal icon filter button" id="btn-filter" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" id="btn-reset" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('toolbars')
{{-- <span class="ui blue left labeled icon button"><i class="download icon"></i> Download Laporan</span> --}}
@endsection

@section('tables')
<div id="history">
{{-- 	<div class="ui inverted loading dimmer">
		<div class="ui text loader">Loading</div>
	</div> --}}
	<div class="ui bottom demo active tab" data-tab="first">
	<div style="overflow-x:auto;">
		<table class="ui celled compact red table display" width="100%" cellspacing="0">
			<thead>
				<tr class="middle aligned">
					<th width="10" class="center aligned fn">#</th>
					<th width="200" class="center aligned fn">No Order</th>
					<th width="200" class="center aligned fn">Peminta Jasa</th>
					<th width="200" class="center aligned fn">Tgl Order</th>
					<th width="200" class="center aligned fn">No Surat Permintaan</th>
					<th width="200" class="center aligned fn">WBS/IO</th>
					<th width="200" class="center aligned fn">No Surat Penawaran</th>
					<th width="200" class="center aligned fn">Layanan / Lingkup</th>

					<th width="200" class="center aligned fn">Jenis Pengujian</th>
					<th width="250" class="center aligned fn">Nama Barang</th>
					<th width="150" class="center aligned fn">Merk</th>
					<th width="150" class="center aligned fn">No Seri</th>
					<th width="150" class="center aligned fn">Tgl Penerimaan Barang Uji</th>
					<th width="300" class="center aligned fn">Pelaksana Verifikasi</th>
					<th width="150" class="center aligned fn">Hasil Verifikasi</th>
					<th width="150" class="center aligned fn">Tgl Verifikasi Barang Uji</th>
					<th width="250" class="center aligned fn">Catatan</th>
					<th width="250" class="center aligned fn">No FPP/FPK</th>
					<th width="300" class="center aligned fn">Pelaksana Pengujian</th>
					<th width="200" class="center aligned fn">Tgl Selesai Pengujian</th>
					<th width="200" class="center aligned fn">Hasil Pengujian</th>
					<th width="300" class="center aligned fn">Detil Pelaksana Member</th>
					<th width="300" class="center aligned fn">Pembuat Laporan</th>
					<th width="200" class="center aligned fn">Barang Uji Kembali</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="24" class="center aligned">
						Tidak Ada Data
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
</div>
@endsection

@section('filterdata')
{{-- d.tanggal_order = $("input[name='filter[tanggal_order]']").val();
d.jenis_pelayanan_id = $("select[name='filter[jenis_pelayanan_id]']").val(); --}}
@endsection

@section('rules')
<script type="text/javascript">
  formRules = {
     username: 'empty'
 };
</script>
@endsection
@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	$(document).ready(function() {
		$('.ui.search.dropdown').css({
        	'width': '400px'
        });
	});

	$('#btn-filter').click(function(e){
		var element = $("#history");
		loadGrid(element);
	});

	$('#btn-reset').click(function(e){
		window.location.reload();
	});

	var loadGrid = function(element){
		// var tgl_order = $('[name="tgl_order"]').val();
		var jenis_pelayanan_id = $('[name="jenis_pelayanan_id"]').val();
		openLoading(element);
		$.ajax({
			url: "{{ url($pageUrl.'grid') }}",
			type: 'GET',
			data: {jenis_pelayanan_id: jenis_pelayanan_id},
		})
		.done(function(response) {
			$(element).html(response);
			table2 = $('#hasil').DataTable( {
				destroy: true,
				scrollX:        true,
				paging:         false,
				filter:         false,
				info:           false,
				ordering:       false,
				lengthChange: 	true,
				responsive: false,
				aaSorting: [],
				columnDefs: [ { width: 200, targets: 3 } ]
			}).columns.adjust()

			fx = new $.fn.dataTable.FixedColumns( table2, {
				leftColumns: 7,
				rightColumns: 2,
			})
			fx.fnRedrawLayout();
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			
		});
	}
	openLoading = function(element) {
        var tag = '<div class="ui inverted loading dimmer"><div class="ui text loader">Loading</div></div>';
        $(element).html('');
        $(element).prepend(tag);
    }
</script>
@append

@section('init-modal')
<script>
</script>
@endsection

