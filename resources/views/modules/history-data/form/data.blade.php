@if($data->count() > 0)
	<div class="ui labeled button" tabindex="0">
		<button class="ui inverted green button">
			<a href="{{ url($pageUrl).'/print/'.$data->first()->layanan_id.'/'.'1' }}">
				<i class="print icon"> </i>XLSX
			</a>
	</button>
	</div>
	</br>
	</br>
@endif
<div class="ui bottom demo active tab" data-tab="first" @if($data->count() > 0) @else style="overflow-x:auto;" @endif>
	<div style="overflow-x:auto;">
		<table class="ui celled compact red table display" width="100%" cellspacing="0">
			<thead>
				<tr class="middle aligned">
					<th width="10" class="center aligned fn">#</th>
					<th width="200" class="center aligned fn">No Order</th>
					<th width="200" class="center aligned fn">Peminta Jasa</th>
					<th width="200" class="center aligned fn">Tgl Order</th>
					<th width="200" class="center aligned fn">No Surat Permintaan</th>
					<th width="200" class="center aligned fn">WBS/IO</th>
					<th width="200" class="center aligned fn">No Surat Penawaran</th>
					<th width="200" class="center aligned fn">Layanan / Lingkup</th>

					<th width="200" class="center aligned fn">Jenis Pengujian</th>
					<th width="250" class="center aligned fn">Nama Barang</th>
					<th width="150" class="center aligned fn">Merk</th>
					<th width="150" class="center aligned fn">No Seri</th>
					<th width="150" class="center aligned fn">Tgl Penerimaan Barang Uji</th>
					<th width="300" class="center aligned fn">Pelaksana Verifikasi</th>
					<th width="150" class="center aligned fn">Hasil Verifikasi</th>
					<th width="150" class="center aligned fn">Tgl Verifikasi Barang Uji</th>
					<th width="250" class="center aligned fn">Catatan</th>
					<th width="250" class="center aligned fn">No FPP/FPK</th>
					<th width="300" class="center aligned fn">Pelaksana Pengujian</th>
					<th width="200" class="center aligned fn">Tgl Selesai Pengujian</th>
					<th width="200" class="center aligned fn">Hasil Pengujian</th>
					<th width="300" class="center aligned fn">Detil Pelaksana Member</th>
					<th width="300" class="center aligned fn">Pembuat Laporan</th>
					<th width="200" class="center aligned fn">Barang Uji Kembali</th>
				</tr>
			</thead>
			<tbody>
				@php 
					$i=0;
					$angka =0;
					$jumlah=0;
					$j=0;
				@endphp
				@foreach($data as $cek)
					@if($cek->kaji_ulang)
						@if($cek->kaji_ulang->surat)
							@if($cek->kaji_ulang->surat->va)
								@if($cek->kaji_ulang->surat->va->konfirmasi)
									@if($cek->kaji_ulang->surat->va->konfirmasi->penerimaan)
										@if($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian)
											@if($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->detailpengujian->count() > 0)
												@if($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->detailpengujian->first()->detailpelaksana)
													@php
														$i++;
													@endphp
													@foreach($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->detail_penerimaan_barang->where('flag', 0)->groupBy('group_id') as $pembanding)
															@php
																$j += $pembanding->count();
															@endphp
													@endforeach

													@php
														$u =0;
													@endphp
													@foreach($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->detail_penerimaan_barang->where('flag', 0)->groupBy('group_id') as $key => $row)
														@foreach($row as $kuy => $data)
															@if($kuy == 0)
																<tr>
																	@if($u == 0)
																	<td rowspan="{{ $j }}">{{$i}}</td>
																	<td rowspan="{{ $j }}">{{$cek->no_order}}</td>
																	<td rowspan="{{ $j }}">{{$cek->user->pelanggans->perusahaan->nama}}</td>
																	<td rowspan="{{ $j }}">{{DateToStringYear($cek->tgl_order)}}</td>
																	<td rowspan="{{ $j }}">{{ $cek->no_surat }}</td>
																	<td rowspan="{{ $j }}">{{ $cek->kaji_ulang->surat->va->konfirmasi->wbs_io }}</td>
																	<td rowspan="{{ $j }}">{{ $cek->kaji_ulang->surat->no_surat }}</td>
																	<td rowspan="{{ $j }}">{{ $cek->pelayanan->nama }} / {{ $cek->lingkup->nama }}</td>
																	@endif
																	<td style="border-left: 1px solid #e8e9e9" rowspan="{{$row->count()}}">{{$data->detail_pp->jenis->nama}}</td>
																	<td>{{ $data->nama_barang }} [{{$data->urutan}}/{{$data->max}}]</td>
																	<td rowspan="{{$row->count()}}">{{ $data->detail_pp->merk }}</td>
																	<td>{{$data->no_seri}}</td>
																	<td class="center aligned">{{ DateToStringYear($data->tanggal_terima) }}</td>
																	<td rowspan="{{$row->count()}}" class="center aligned">
																		{{$data->detail_pengujian->pelaksana->nama}}
																	</td>
																	<td class="center aligned" rowspan="{{$row->count()}}">
																		@if($data->detail_pengujian->verifikasi == 1)
																			<a class="ui tag label fn" style="background-color:#00abffcc;">DITERIMA</a>
																		@elseif($data->detail_pengujian->verifikasi == 1)
																			<a class="ui tag label fn" style="background-color:#dd0000cc;">DITOLAK</a>
																		@else 
																			-
																		@endif
																	</td>
																	<td class="center aligned" rowspan="{{$row->count()}}">
																		@if($data->detail_pengujian)
																			@if($data->detail_pengujian->tgl_selesai)
																				{{ DateToStringYear(Carbon\Carbon::parse($data->detail_pengujian->tgl_selesai)->format('Y-m-d')) }}
																			@else 
																				-
																			@endif
																		@else
																			-
																		@endif
																	</td>
																	<td style="text-align:justify;text-justify:auto;">{!! readMoreText($data->catatan, 50) !!}</td>
																	<td rowspan="{{$row->count()}}">
																		{{ $data->detail_pengujian->fpp_fpk or '-' }}
																	</td>
																	<td rowspan="{{$row->count()}}" class="center aligned">
																		@if($data->detail_pengujian->detailpelaksana)
																			{{$data->detail_pengujian->detailpelaksana->pelaksana->nama}}
																		@else
																			-
																		@endif
																	</td>
																	<td class="center aligned" rowspan="{{$row->count()}}">
																		@if($data->detail_pengujian->detailpelaksana)
																			@if($data->detail_pengujian->detailpelaksana->tgl_selesai)
																				{{ DateToStringYear(Carbon\Carbon::parse($data->detail_pengujian->detailpelaksana->tgl_selesai)->format('Y-m-d')) }}
																			@else 
																				-
																			@endif
																		@else
																			-
																		@endif
																	</td>
																	<td class="center aligned" rowspan="{{$row->count()}}">
																		@if($data->detail_pengujian->detailpelaksana)
																			@if($data->detail_pengujian->detailpelaksana->status_hasil_pengujian == 1)
																				<a class="ui tag label fn" style="background-color:#00FF00;">BERHASIL</a>
																			@elseif($data->detail_pengujian->detailpelaksana->status_hasil_pengujian == 2)
																				<a class="ui tag label fn" style="background-color:#dd0000cc;">GAGAL</a>
																			@else
																				<div class="ui label">
																					<i class="hourglass end icon"></i>Menunggu Hasil Pengujian
																				</div>
																			@endif
																		@else 
																			-
																		@endif
																	</td>
																	<td class="left aligned field" width="500px">
																		<div class="ui ordered list list-more1" data-display="3">
																		@foreach($data->detail_pengujian->detailpelaksana->detailmember as $member)
																			<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian">{{ $member->pelaksana->nama }}</a></div>
																		@endforeach
																		</div>
																	</td>
																	<td rowspan="{{$row->count()}}">
																		@if($data->detail_pengujian->detailpelaksana)
																			@if($data->detail_pengujian->detailpelaksana->status == 0)
																				<div class="ui label">
																					<i class="hourglass end icon"></i>Menunggu Verifikasi Hasil Pengujian
																				</div>
																			@else
																				@if($data->detail_pengujian->detailpelaksana->pelaksana_laporan_id !== 1)
																					{{ $data->detail_pengujian->detailpelaksana->pelaksanalaporan->nama }}
																				@else
																					<div class="ui label">
																						<i class="hourglass end icon"></i>Menunggu Verifikasi Asman
																					</div>
																				@endif
																			@endif
																		@else 
																			-
																		@endif 
																	</td>
																	<td rowspan="{{$row->count()}}">
																		@if($data->detail_pengujian->detailpelaksana)
																			@if($data->detail_pengujian->detailpelaksana->status_barang == 0)
																				<div class="ui label">
																					<i class="close icon"></i> Barang tidak dikembalikan
																				</div>
																			@else
																				<div class="ui label">
																					<i class="check icon"></i> Barang dikembalikan
																				</div>
																			@endif
																		@else 
																		-
																		@endif
																	</td>
																</tr>
															@else
																<tr>
																	<td style="border-left: 1px solid #e8e9e9">{{ $data->nama_barang }} [{{$data->urutan}}/{{$data->max}}] aaa</td>
																	<td>{{$data->no_seri}}</td>
																	<td class="center aligned">{{ DateToStringYear($data->tanggal_terima) }}</td>
																	<td style="text-align:justify;text-justify:auto;">{!! readMoreText($data->catatan, 50) !!}</td>
																	<td class="left aligned field" width="500px">
																		<div class="ui ordered list list-more1" data-display="3">
																		@foreach($data->detail_pengujian->detailpelaksana->detailmember as $member)
																			<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian">{{ $member->pelaksana->nama }}</a></div>
																		@endforeach
																		</div>
																	</td>
																</tr>
															@endif
															@php
																$j=0;
															@endphp
														@endforeach
														@php
															$u++;
														@endphp
													@endforeach
												@else
													{{-- <tr>
														<td colspan="16" class="center aligned">
															Tidak Ada Data
														</td>
													</tr> --}}
												@endif
											@else
												{{-- <tr>
													<td colspan="16" class="center aligned">
														Tidak Ada Data
													</td>
												</tr> --}}
											@endif
										@else
											{{-- <tr>
												<td colspan="16" class="center aligned">
													Tidak Ada Data
												</td>
											</tr> --}}
										@endif
									@else
										{{-- <tr>
											<td colspan="16" class="center aligned">
												Tidak Ada Data
											</td>
										</tr> --}}
									@endif
								@else
									{{-- <tr>
										<td colspan="16" class="center aligned">
											Tidak Ada Data
										</td>
									</tr> --}}
								@endif
							@else
								{{-- <tr>
									<td colspan="16" class="center aligned">
										Tidak Ada Data
									</td>
								</tr> --}}
							@endif
						@else
							{{-- <tr>
								<td colspan="16" class="center aligned">
									Tidak Ada Data
								</td>
							</tr> --}}
						@endif
					@else
						{{-- <tr>
							<td colspan="16" class="center aligned">
								Tidak Ada Data
							</td>
						</tr> --}}
					@endif
				@endforeach
			</tbody>
		</table>
	</div>
</div>