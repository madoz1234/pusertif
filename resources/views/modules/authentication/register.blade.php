@extends('layouts.auth')
@section('styles')
	<style type="text/css">
		::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
			color: #DEDEDE !important;
			opacity: 1; /* Firefox */
		}
	</style>
@append
@section('content')
<div class="ui fixed blue menu" style="border:none;-webkit-box-shadow: none;-moz-box-shadow: none;box-shadow: none;">
	@php
	$dataTrans = 'id';
	if(Auth::check())
	{
		if(auth()->user()->id == true){
			if(\App\Models\Translators::where('created_by',auth()->user()->id)->first()){
				$dataTrans = \App\Models\Translators::where('created_by',auth()->user()->id)->first()->translator;
			}else {
				$dataTrans = 'id';
			}
		}else {
			$dataTrans = 'id';
		}
	}else{
		$dataTrans = \App\Models\Translators::where('created_by',999)->first()->translator;
	}
	@endphp
	<h1 class="ui center aligned heading" style="position: fixed; top: 1rem; left: 50%; transform: translateX(-50%)">
		eLab Pusertif
	</h1>
	{{-- <h1 class="ui center aligned heading red massive label" style="position: fixed; top: 6rem; left: 50%; transform: translateX(-50%)">
        Training
    </h1> --}}
	<div class="ui menu" style="margin-left: 147vh;margin-top: 1vh;">
		<div class="ui pointing dropdown item {{ ($dataTrans == 'id') ? '' : 'translate' }}" style="{{ ($dataTrans == 'id') ? 'background-color: #a2aebd8f;
		color: #fff !important;' : '' }} " data-value="id">
		<a class="" data-value="id" ><i class="ui id flag" style="margin-right: 0"></i> ID</a>
		</div>
		<div class="ui pointing dropdown item {{ ($dataTrans == 'en') ? '' : 'translate' }}" style="{{ ($dataTrans == 'en') ? 'background-color: #a2aebd8f;
		color: #fff !important;' : '' }} " data-value="en">
			<a class="" data-value="en"><i class="ui us flag" style="margin-right: 0"></i> EN</a>
		</div>
	</div>
</div>
<div class="ui middle aligned grid">
    <div class="column auth-container">
    	<div id="load">
			<div class="ui inverted loading dimmer">
				<div class="ui text loader">Loading</div>
			</div>
		</div>
        <div class="ui auth-form register middle aligned grid">
            <div class="computer only eight wide center aligned column" style="display: flex;justify-content: center;">
            	<div>
	                <img src="{{ asset('img/logo-register.png') }}" alt="Logo" style="object-fit: contain; width: auto;height: 40vh;">
	            	<div class="ui card" style="width: 100%; background-color: #01aef0;margin-top: -2rem;">
	            		<div class="content">
	            			<div class="header">
	            				<h4 style="color:#ffffff;font-size: 16px;">Contact Center eLab Pusertif</h4>
	            			</div>
	            			<div class="description" style="text-align: center;">
	            				@php 
		            				$jumlah = $jum->count();
		            				$i =1;
		            				$k =1;
	            				@endphp
	            				<p style="color:#ffffff;font-size: 16px;"><i class="phone icon"></i>
		            				@if($jumlah > 0) 
			            				@foreach($jum as $data)
			            					{{ $data->no_tlp or '-'}}
				            					@if($i != $jumlah)
				            						{{','}}
				            					@else
				            					@endif
				            				@php
				            					$i++;
				            				@endphp
			            				@endforeach
		            				@else
		            					-
		            				@endif
            					</p>
            					<p style="color:#ffffff;font-size: 16px;"><i class="whatsapp icon"></i>
	        						@if($jumlah > 0)
			            				@foreach($jum as $data)
			            					{{ $data->wa or '-'}}
				            					@if($k != $jumlah)
				            						{{','}}
				            					@else
				            					@endif
				            				@php
				            					$k++;
				            				@endphp
			            				@endforeach
		            				@else
		            					-
		            				@endif
            					</p>
	            			</div>
	            		</div>
	                </div>
	                <h2 class="title">www.pln.co.id</h2>
	            </div>
            </div>
            <div class="sixteen wide mobile eight wide computer column center aligned">
                @if (session()->has('message'))
                <div class="ui negative message mobox">
                    <i class="close icon"></i>
                    <div class="header">
                        <strong>Mohon Maaf, </strong>Terjadi Kesalahan<br>
                    </div>
                    {{ session()->get('message') }}
                </div>
                @endif
            <div class="ui segment center aligned mobox" style="background-color: #ffffff; border:1px solid #DEDEDE;box-shadow: 3px 3px #E3E3E3;width: 55vh;">
                <form class="ui data form" id="formRegister" method="POST" action="{{ url('registrasi') }}">
					{!! csrf_field() !!}
					<div class="ui form">
						<div class="field">
							<center>
								<label style="color: #4c4c4c"><h2>{{trans('translator.Daftar Sekarang')}}</h2></label>
								<p style="color: #757575;padding-top: 9px;">{{trans('translator.Sudah Memiliki Akun eLab Pusertif')}} ? <a href="{{ url('login') }}"> {{trans('translator.Masuk')}}</a><br>
									<h4 class="ui horizontal divider header" style="font-weight: normal;color: #DEDEDE;">
										{{trans('translator.atau Daftar di sini')}}
									</h4>
								</p>
							</center>
						</div>
						<table class="tabel" style="background-color: #ffffff;margin: 0;border: none;">
							<tr>
								<td width="150">
									<label for="fruit" style="color: #757575">{{trans('translator.Kategori')}}</label>
								</td>
								<td class="field">
									<div class="ui radio checkbox">
										<input type="radio" name="tipe_customer" value="0" tabindex="0" class="hidden">
										<label>PLN</label>
									</div>
									&nbsp;&nbsp;&nbsp;
									<div class="ui radio checkbox">
										<input type="radio" name="tipe_customer" value="2" tabindex="0" class="hidden">
										<label>AP-PLN</label>
									</div>
									&nbsp;&nbsp;&nbsp;
									<div class="ui radio checkbox">
										<input type="radio" name="tipe_customer" value="1" tabindex="0" class="hidden">
										<label>NON PLN</label>
									</div>
								</td>
							</tr>
							<tr>
								<td width="150">
									<label for="fruit" style="color: #757575">{{trans('translator.Nama Perusahaan')}}</label>
								</td>
								<td>
									<input type="hidden" name="perusahaan_id" value="" id="satu">
									<div class="field">
										<div class="ui search">
											<div class="ui icon input">
												<input class="prompt nama usaha" type="text" style="color:#9e9e9e;" name="nama_perusahaan" placeholder="{{trans('translator.Nama Perusahaan')}}" id="upper">
												<i class="search icon"></i>
											</div>
											<div class="results"></div>
										</div>
									</div> 
								</td>
							</tr>
							<tr>
								<td width="150">
									<label style="color: #757575;">{{trans('translator.No. Telpon Perusahaan')}}</label>
								</td>
								<td>
									<div class="field">
										<input type="text" id="dua" class="angka dua" name="no_tlp" style="color:#9e9e9e;" placeholder="{{trans('translator.No. Telpon Perusahaan')}}">
									</div>
								</td>
							</tr>
							<tr>
								<td width="150">
									<label style="color: #757575;">{{trans('translator.Email Perusahaan')}}</label>
								</td>
								<td>
									<div class="field">
										<input type="text" id="tiga" class="angka tiga" name="email_perusahaan" style="color:#9e9e9e;" placeholder="{{trans('translator.Email Perusahaan')}}">
									</div>
								</td>
							</tr>
							<tr>
								<td width="150">
									<label style="color: #757575">{{trans('translator.Alamat Perusahaan')}}</label>
								</td>
								<td>
									<div class="field">
										<div class="ui left icon input">
											<textarea name="alamat" id="empat" class="areas empat" style="color:#9e9e9e;" placeholder="{{trans('translator.Alamat Perusahaan')}}" rows="3"></textarea>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="width: 100%;">
									<h4 class="ui horizontal divider header" style="font-weight: normal;color: #DEDEDE;font-size: 14px;">
										{{trans('translator.Data Personal')}}
									</h4>
								</td>
							</tr>
							<tr>
								<td width="150">
									<label style="color: #757575">{{trans('translator.Nama Lengkap')}}</label>
								</td>
								<td>
									<div class="field">
										<input type="text" id="lima" class="lima"  placeholder="{{trans('translator.Nama Lengkap')}}" style="color:#9e9e9e;" name="nama_lengkap" value="{{ old('nama') }}">
									</div>
								</td>
							</tr>
							<tr class="nip" style="display: none;">
								<td width="150">
									<label style="color: #757575">NIP</label>
								</td>
								<td>
									<div class="field">
										<input type="text" id="sembilan" class="angka sembilan" style="color:#9e9e9e;"  placeholder="NIP" name="nip">
									</div>
								</td>
							</tr>
							<tr>
								<td width="150">
									<label style="color: #757575">Email</label>
								</td>
								<td>
									<div class="field">
										<input type="text" id="enam" class="enam"  placeholder="Email" style="color: #9e9e9e;" name="email" value="{{ old('email') }}">
									</div>
								</td>
							</tr>
							<tr>
								<td width="150">
									<label style="color: #757575">{{trans('translator.No HP')}}</label>
								</td>
								<td>
									<div class="field">
										<input type="text" id="tujuh" class="angka tujuh" style="color:#9e9e9e;"  placeholder="{{trans('translator.No HP')}}" name="no_hp">
									</div>
								</td>
							</tr>
							<tr>
								<td width="150">
									<label style="color: #757575">{{trans('translator.Pesan')}}</label>
								</td>
								<td>
									<div class="field">
										<div class="ui left icon input">
											<textarea name="pesan" id="delapan" class="areas delapan" style="color:#9e9e9e;margin-bottom: -1em;" placeholder="{{trans('translator.Pesan')}}" rows="3"></textarea>
										</div>
									</div>
								</td>
							</tr>
						</table>
						<br>
						<div class="ui grid">
							<div class="sixteen wide column">
								<div class="field">
									<button type="button" class="ui fluid small simpan register button" style="background-color: #01aef0;;padding-top: 1em;padding-bottom: 1em;color:#ffffff;">{{trans('translator.Daftar')}}</button>
								</div>
								<p class="notif bawah" style="display: none;margin-top: -0.5em;font-size: 12px;color:#9e9e9e;">{{trans('translator.Kami akan mengirim email kode verifikasi')}}</p>
								<p style="margin-top: -0.5em;">{{trans('translator.Dengan mendaftar')}}, {{trans('translator.Saya menyetujui')}} <a href>{{trans('translator.Syarat & Ketentuan')}}</a> {{trans('translator.Serta')}} <a href>{{trans('translator.Kebijakan Privasi')}}</a> {{trans('translator.yang berlaku')}}.</p>
							</div>
						</div>
					</div>
				</form>
			</div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
		$(document).ready(function($) {
			$(document).on('keyup', '#upper', function(e){
		    	var string = $(this).val();
				$(this).val(string.toUpperCase());
		    });

		    $(document).on('keydown', '#upper', function(e){
		    	var string = $(this).val();
				$(this).val(string.toUpperCase());
		    });

		    $(document).on('change', '#upper', function(e){
		    	var string = $(this).val();
				$(this).val(string.toUpperCase());
		    });
		    
			$(document).on('click','.translate',function(){
				var trans = $(this).data('value');
				var ip = '999';
				$.ajax({
					url: '{{ url("langs") }}',
					type: 'POST',
					data: {_token: "{{ csrf_token() }}",translator:trans,ip:ip},
					success: function(resp){
						window.location.reload();
					},
					error : function(resp){
						window.location.reload();
					}
				});
			});

			$('[name=tipe_customer]').change(function(){
				var tipe = $(this).val();
				if(tipe == 0 || tipe == 2){
					$('.nip').show();
				}else{
					$('.nip').hide();
				}
				$('[name=nama_perusahaan]').val('');
				$('[name=alamat]').val('');
				$('[name=no_tlp]').val('');
				$('[name=email_perusahaan]').val('');
			});


			$(".satu").keyup(function(){
				var radioValue = $("input[name='tipe_customer']:checked").val();
				if(radioValue == 0){
					if($("input[name=nama_perusahaan]").val() === '' || $(".lima").val() === '' || $(".empat").val() === '' || $(".tiga").val() === '' || $(".dua").val() === '' || $(".enam").val() === '' || $(".tujuh").val() === '' || $(".sembilan").val() === ''){
						$('.notif.bawah').hide();
					}else{
						$('.notif.bawah').show();
						$('.exclamation.icon').css({
							"color":"red"
						});
					}
				}else{
					if($("input[name=nama_perusahaan]").val() === '' || $(".lima").val() === '' || $(".empat").val() === '' || $(".tiga").val() === '' || $(".dua").val() === '' || $(".enam").val() === '' || $(".tujuh").val() === ''){
						$('.notif.bawah').hide();
					}else{
						$('.notif.bawah').show();
						$('.exclamation.icon').css({
							"color":"red"
						});
					}
				}
			});

			$(".dua").keyup(function(){
				var radioValue = $("input[name='tipe_customer']:checked").val();
				if(radioValue == 0){
					if($("input[name=nama_perusahaan]").val() === '' || $(".lima").val() === '' || $(".empat").val() === '' || $(".tiga").val() === '' || $(".dua").val() === '' || $(".enam").val() === '' || $(".tujuh").val() === '' || $(".sembilan").val() === ''){
						$('.notif.bawah').hide();
					}else{
						$('.notif.bawah').show();
						$('.exclamation.icon').css({
							"color":"red"
						});
					}
				}else{
					if($("input[name=nama_perusahaan]").val() === '' || $(".lima").val() === '' || $(".empat").val() === '' || $(".tiga").val() === '' || $(".dua").val() === '' || $(".enam").val() === '' || $(".tujuh").val() === ''){
						$('.notif.bawah').hide();
					}else{
						$('.notif.bawah').show();
						$('.exclamation.icon').css({
							"color":"red"
						});
					}
				}
			});

			$(".tiga").keyup(function(){
				var radioValue = $("input[name='tipe_customer']:checked").val();
				if(radioValue == 0){
					if($("input[name=nama_perusahaan]").val() === '' || $(".lima").val() === '' || $(".empat").val() === '' || $(".tiga").val() === '' || $(".dua").val() === '' || $(".enam").val() === '' || $(".tujuh").val() === '' || $(".sembilan").val() === ''){
						$('.notif.bawah').hide();
					}else{
						$('.notif.bawah').show();
						$('.exclamation.icon').css({
							"color":"red"
						});
					}
				}else{
					if($("input[name=nama_perusahaan]").val() === '' || $(".lima").val() === '' || $(".empat").val() === '' || $(".tiga").val() === '' || $(".dua").val() === '' || $(".enam").val() === '' || $(".tujuh").val() === ''){
						$('.notif.bawah').hide();
					}else{
						$('.notif.bawah').show();
						$('.exclamation.icon').css({
							"color":"red"
						});
					}
				}
			});

			$(".empat").keyup(function(){
				var radioValue = $("input[name='tipe_customer']:checked").val();
				if(radioValue == 0){
					if($("input[name=nama_perusahaan]").val() === '' || $(".lima").val() === '' || $(".empat").val() === '' || $(".tiga").val() === '' || $(".dua").val() === '' || $(".enam").val() === '' || $(".tujuh").val() === '' || $(".sembilan").val() === ''){
						$('.notif.bawah').hide();
					}else{
						$('.notif.bawah').show();
						$('.exclamation.icon').css({
							"color":"red"
						});
					}
				}else{
					if($("input[name=nama_perusahaan]").val() === '' || $(".lima").val() === '' || $(".empat").val() === '' || $(".tiga").val() === '' || $(".dua").val() === '' || $(".enam").val() === '' || $(".tujuh").val() === ''){
						$('.notif.bawah').hide();
					}else{
						$('.notif.bawah').show();
						$('.exclamation.icon').css({
							"color":"red"
						});
					}
				}
			});

			$(".lima").keyup(function(){
				var radioValue = $("input[name='tipe_customer']:checked").val();
				if(radioValue == 0){
					if($("input[name=nama_perusahaan]").val() === '' || $(".lima").val() === '' || $(".empat").val() === '' || $(".tiga").val() === '' || $(".dua").val() === '' || $(".enam").val() === '' || $(".tujuh").val() === '' || $(".sembilan").val() === ''){
						$('.notif.bawah').hide();
					}else{
						$('.notif.bawah').show();
						$('.exclamation.icon').css({
							"color":"red"
						});
					}
				}else{
					if($("input[name=nama_perusahaan]").val() === '' || $(".lima").val() === '' || $(".empat").val() === '' || $(".tiga").val() === '' || $(".dua").val() === '' || $(".enam").val() === '' || $(".tujuh").val() === ''){
						$('.notif.bawah').hide();
					}else{
						$('.notif.bawah').show();
						$('.exclamation.icon').css({
							"color":"red"
						});
					}
				}
			});

			$(".enam").keyup(function(){
				var radioValue = $("input[name='tipe_customer']:checked").val();
				if(radioValue == 0){
					if($("input[name=nama_perusahaan]").val() === '' || $(".lima").val() === '' || $(".empat").val() === '' || $(".tiga").val() === '' || $(".dua").val() === '' || $(".enam").val() === '' || $(".tujuh").val() === '' || $(".sembilan").val() === ''){
						$('.notif.bawah').hide();
					}else{
						$('.notif.bawah').show();
						$('.exclamation.icon').css({
							"color":"red"
						});
					}
				}else{
					if($("input[name=nama_perusahaan]").val() === '' || $(".lima").val() === '' || $(".empat").val() === '' || $(".tiga").val() === '' || $(".dua").val() === '' || $(".enam").val() === '' || $(".tujuh").val() === ''){
						$('.notif.bawah').hide();
					}else{
						$('.notif.bawah').show();
						$('.exclamation.icon').css({
							"color":"red"
						});
					}
				}
			});

			$(".tujuh").keyup(function(){
				var radioValue = $("input[name='tipe_customer']:checked").val();
				if(radioValue == 0){
					if($("input[name=nama_perusahaan]").val() === '' || $(".lima").val() === '' || $(".empat").val() === '' || $(".tiga").val() === '' || $(".dua").val() === '' || $(".enam").val() === '' || $(".tujuh").val() === '' || $(".sembilan").val() === ''){
						$('.notif.bawah').hide();
					}else{
						$('.notif.bawah').show();
						$('.exclamation.icon').css({
							"color":"red"
						});
					}
				}else{
					if($("input[name=nama_perusahaan]").val() === '' || $(".lima").val() === '' || $(".empat").val() === '' || $(".tiga").val() === '' || $(".dua").val() === '' || $(".enam").val() === '' || $(".tujuh").val() === ''){
						$('.notif.bawah').hide();
					}else{
						$('.notif.bawah').show();
						$('.exclamation.icon').css({
							"color":"red"
						});
					}
				}
			});

			$(".sembilan").keyup(function(){
				if($("input[name=nama_perusahaan]").val() === '' || $(".lima").val() === '' || $(".empat").val() === '' || $(".tiga").val() === '' || $(".dua").val() === '' || $(".enam").val() === '' || $(".tujuh").val() === '' || $(".sembilan").val() === ''){
					$('.notif.bawah').hide();
				}else{
					$('.notif.bawah').show();
					$('.exclamation.icon').css({
						"color":"red"
					});
				}
			});
			
			$(document).on('click','.next',function(){
				var data = $(this).data('tab');
				var dataPrev = $(this).data('prev');
				$('.menu').find("[data-tab='" + dataPrev + "']").removeClass('active');
				$('.menu').find("[data-tab='" + data + "']").addClass('active');
			});

			$('.menu .item').tab();
			$('.next').tab();
			$('.ui.radio.checkbox').checkbox();

			$('.message .close').on('click', function() {
				$(this)
				.closest('.message')
				.transition('fade')
				;
			});

			$('.ui.checkbox').checkbox();
			var tipcustcheck = $('.ui.radio.checkbox.checked:has(input[name="tipe_customer"])').checkbox()
			var check = 99;
			var url = "{{ url('cari-perusahaan') }}?q={query}";
			$('.ui.radio.checkbox:has(input[name="tipe_customer"])').checkbox({
				onChange : function () {
					var check = $(this).val();
					$('.ui.search').search('clear cache');
					$('.ui.search').search({
						minCharacters : 2,
						fullTextSearch: true,
						apiSettings: {
							url: "{{ url('cari-perusahaan') }}?q={query}&checked="+check+"",
						},
						fields: {
							results 	: 'data',
							title   	: 'nama',
							description : 'alamat',
							id      	: 'id'
						},
						onSearchQuery: function(result, response){
							$('[name=alamat]').val('');
							$('[name=no_tlp]').val('');
							$('[name=email_perusahaan]').val('');
							check;
						},
						onSelect: function(result, response){
							$('[name=alamat]').val('');
							$('[name=no_tlp]').val('');
							$('[name=email_perusahaan]').val('');
							$('.nip').hide();
							if(result){
								$('[name=perusahaan_id]').val(result.id);
								$('[name=alamat]').val(result.alamat);
								$('[name=no_tlp]').val(result.no_tlp);
								$('[name=email_perusahaan]').val(result.email);
								$("input[name=tipe_customer][value='"+result.kategori+"']").prop("checked", true);
								if(result.kategori == 0 || result.kategori == 2){
									$('.nip').show();
								}else{
									$('.nip').hide();
								}
							}else{
								$('[name=perusahaan_id]').val('');
								$('.nip').hide();
							}

							$(this).search('clear cache');
						},	
					});
				},
			});
			$('.ui.search').search({
				minCharacters : 2,
				fullTextSearch: true,
				apiSettings: {
					url: "{{ url('cari-perusahaan') }}?q={query}",
				},
				fields: {
					results 	: 'data',
					title   	: 'nama',
					description : 'alamat',
					id      	: 'id'
				},
				onSearchQuery: function(result, response){
					console.log(result);	
					$('[name=alamat]').val('');
					$('[name=no_tlp]').val('');
					$('[name=email_perusahaan]').val('');
					check;
				},
				onSelect: function(result, response){
					$('[name=alamat]').val('');
					$('[name=no_tlp]').val('');
					$('[name=email_perusahaan]').val('');
					$('.nip').hide();
					if(result){
						$('[name=perusahaan_id]').val(result.id);
						$('[name=alamat]').val(result.alamat);
						$('[name=no_tlp]').val(result.no_tlp);
						$('[name=email_perusahaan]').val(result.email);
						$("input[name=tipe_customer][value='"+result.kategori+"']").prop("checked", true);
						if(result.kategori == 0 || result.kategori == 2){
							$('.nip').show();
						}else{
							$('.nip').hide();
						}
					}else{
						$('[name=perusahaan_id]').val('');
						$('.nip').hide();
					}
					$(this).search('clear cache');
				},
			});
		});

		$(document).on('keyup', '.nama.usaha', function(e){
			var code = e.which; // recommended to use e.which, it's normalized across browsers
			if(code!=13){
				$('[name=perusahaan_id]').val('');
			}
		});
</script>
@append
