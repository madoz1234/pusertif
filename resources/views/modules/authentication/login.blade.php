@extends('layouts.auth')
@section('content')
<div class="ui fixed blue menu" style="border:none;-webkit-box-shadow: none;-moz-box-shadow: none;box-shadow: none;">
	@php
	$dataTrans = 'id';
	if(Auth::check())
	{
		if(auth()->user()->id == true){
			if(\App\Models\Translators::where('created_by',auth()->user()->id)->first()){
				$dataTrans = \App\Models\Translators::where('created_by',auth()->user()->id)->first()->translator;
			}else {
				$dataTrans = 'id';
			}
		}else {
			$dataTrans = 'id';
		}
	}else{
		$dataTrans = \App\Models\Translators::where('created_by',999)->first()->translator;
	}
	@endphp
	<h1 class="ui center aligned heading" style="position: fixed; top: 1rem; left: 50%; transform: translateX(-50%)">
		eLab Pusertif
	</h1>
   {{--  <h1 class="ui center aligned heading red massive label" style="position: fixed; top: 6rem; left: 50%; transform: translateX(-50%)">
        Training
    </h1> --}}
	<div class="ui menu" style="margin-left: 147vh;margin-top: 1vh;">
		<div class="ui pointing dropdown item {{ ($dataTrans == 'id') ? '' : 'translate' }}" style="{{ ($dataTrans == 'id') ? 'background-color: #a2aebd8f;
		color: #fff !important;' : '' }} " data-value="id">
		<a class="" data-value="id" ><i class="ui id flag" style="margin-right: 0"></i> ID</a>
		</div>
		<div class="ui pointing dropdown item {{ ($dataTrans == 'en') ? '' : 'translate' }}" style="{{ ($dataTrans == 'en') ? 'background-color: #a2aebd8f;
		color: #fff !important;' : '' }} " data-value="en">
			<a class="" data-value="en"><i class="ui us flag" style="margin-right: 0"></i> EN</a>
		</div>
	</div>
</div>
<div class="ui middle aligned center aligned grid">
    <div class="column auth-container">
        <form class="ui form" role="form" method="POST" action="{{ url('/login') }}">
            {!! csrf_field() !!}
            {{-- <div class="ui horizontal auth-form segments"> --}}
            <div class="ui auth-form middle aligned grid">
                <div class="computer only eight wide middle aligned center aligned column" style="text-align: justify; text-align-last: center;text-justify: inter-word;">
                    <img src="{{ asset('img/logo-login2.png') }}" alt="Logo" style="height: 30vh;">
                    @if($dataTrans == 'id')
                    	<h2 style="color: #363636;text-align: justify;text-justify: inter-word;"><b>{{trans('translator.Selamat datang di Pusertif')}}</b></h2>
                    	<div class="description" style="text-align: center; line-height: 1">
        					<span style="font-size: 11px;">{{trans('translator.Masuk dan penuhi kebutuhan layanan pengujian Anda di sini')}}</span>
                    	</div>
                    @else
                    	<h1 style="color: #363636;text-align: justify;text-justify: inter-word; word-spacing: 1rem;"><b>{{trans('translator.Selamat datang di Pusertif')}}</b></h1>
                    	<div class="description" style="text-align: center; line-height: 1;">
        					<span style="font-size: 14px;text-align: justify;text-justify: inter-word;">{{trans('translator.Masuk dan penuhi kebutuhan layanan pengujian Anda di sini')}}</span>
                   		</div>
                    @endif
                    <h2 class="title" style="margin-top: 0.8rem;">www.pln.co.id</h2>
                </div>
                <div class="sixteen wide mobile eight wide computer column center aligned" style="margin-top: -7rem;">
                    @if (session()->has('message'))
                    <div class="ui negative message mobol">
                        <div class="header">
                            <strong>{{trans('translator.Mohon Maaf')}}, </strong>{{trans('translator.Terjadi Kesalahan')}}<br>
                        </div>
                        {{ session()->get('message') }}
                    </div>
                    @endif

                    <div class="ui raised segment center aligned mobol" style="background-color: #ffffff; border:1px solid #DEDEDE;box-shadow: 3px 3px #E3E3E3;">
                    	<div class="field">
                         <center>
                            <h1 class="" style="color: #544b4b;margin-bottom: -1rem;">{{trans('translator.Masuk')}}</h1>
                            <h4 style="color: #544b4b;font-weight: normal; font-size: 14px;">{{trans('translator.Belum Memiliki Akun eLab Pusertif')}} ? <a href="{{ url('register') }}">{{trans('translator.Daftar')}}</a></4>
                            </center>
                        </div>
                        <div class="field">
                            <div class="ui left icon input">
                                <img src="{{ asset('img/icon-user.png') }}" class="icon" alt="icon" width="100%">
                                <input id="username" type="username" style="color:#9e9e9e;" class="form-control" name="username" value="{{ old('username') }}" placeholder="Username / Email" required autofocus>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui left icon input">
                                {{-- <i class="lock icon"></i> --}}
                                <img src="{{ asset('img/icon-password.png') }}" class="icon" alt="icon" width="100%">
                                <input id="password" type="password" style="color:#9e9e9e;" class="form-control" name="password" placeholder="Password" required>
                            </div>
                        </div>
                        <div class="ui grid field">
                            <div class="sixteen wide mobile eight wide tablet eight wide computer column">

                            </div>
                            <div class="sixteen wide mobile eight wide tablet eight wide computer column">
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __(trans('translator.Lupa Password')) }}
                                </a>
                            </div>
                        </div>
                        <button type="submit" class="ui fluid small submit button">{{trans('translator.Login')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
