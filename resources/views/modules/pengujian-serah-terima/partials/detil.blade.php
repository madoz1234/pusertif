@extends('layouts.form')
@section('styles')
<style type="text/css">
.responsive.table{
width: 100%;
overflow-x: auto;
}
.jus{
text-align: justify;
text-justify: inter-word;
}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
formRules = {
	tipe: ['empty'],
};
</script>
@endsection

@section('content-body')
<form class="ui form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
{!! csrf_field() !!}
<input type="hidden" name="_method" value="PUT">
<input type="hidden" name="id" value="{{ $record->id }}">
<div class="ui top demo tabular menu">
  <div class="active item" data-tab="first">Detil Order</div>
  <div class="item" data-tab="second">Item Uji</div>
  <div class="item" data-tab="third">Riwayat Aktivitas</div>
</div>
<div class="ui bottom demo active tab" data-tab="first">
	<table class="ui compact table" style="border-radius: 0; margin: 0">
		<tr>
			<td width="250px"><label>Tgl Order </label></td>
			<td width="5px">:</td>
			<td>{{ DateToStringYear($record->tgl_order) }}</td>
		</tr>
		<tr>
			<td width="250px"><label>No Order </label></td>
			<td width="5px">:</td>
			<td>{{ $record->no_order }}</td>
		</tr>
		<tr>
			<td width="250px"><label>Layanan </label></td>
			<td width="5px">:</td>
			<td>{{ $record->pelayanan->nama }}</td>
		</tr>
		<tr>
			<td><label>Lingkup</label></td>
			<td>:</td>
			<td>{{ $record->lingkup->nama or '' }}</td>
		</tr>
		<tr>
			<td><label>Rencana Pelaksanaan</label></td>
			<td>:</td>
			<td>{{ BulanToString($record->rencana) }}</td>
		</tr>
		<tr>
			<td><label>Kelas</label></td>
			<td>:</td>
			<td>@if($record->kelas == '1')
					Reguler
				@else
					Prioritas
				@endif
			</td>
		</tr>
		<tr>
			<td><label>No Kontrak</label></td>
			<td>:</td>
			<td>{{ $record->no_kontrak or '-' }}</td>
		</tr>
		<tr>
			<td><label>Tgl Kontrak</label></td>
			<td>:</td>
			<td>{{ DateToStringYear($record->tgl_kontrak) }}</td>
		</tr>
		<tr>
			<td><label>File Surat Permintaan</label></td>
			<td>:</td>
			<td>
				@if($record->bukti)
					<div class="ui button preview" data-id="{{ $record->id }}" data-tooltip="Lihat" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<i class="file image outline icon" style="margin-left: 0px;margin-right: 0px;"></i>
					</div>
					<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
					<a href="{{ url($pageUrl).'/download/'.$record->id }}">
						<i class="download icon"></i></a>
					</div>
				@endif
			</td>
		</tr>
		<tr>
			<td><label>Peminta Jasa</label></td>
			<td>:</td>
			<td>{{ $record->user->pelanggans->perusahaan->nama or '' }}</td>
		</tr>
		<tr>
			<td><label>Kategori</label></td>
			<td>:</td>
			<td>
				@if($record->user->pelanggans->perusahaan->kategori == 0)
					PLN
				@elseif($record->user->pelanggans->perusahaan->kategori == 1)
					NON PLN
				@else
					A-PLN
				@endif
			</td>
		</tr>
		<tr>
			<td><label>Dokumen Pendukung</label></td>
			<td>:</td>
			<td>
				<div class="ui button preview_multiple" data-id="{{ $record->id }}" data-tooltip="Lihat" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<i class="file image outline icon" style="margin-left: 0px;margin-right: 0px;"></i>
				</div>
				@if(isset($record->pp))
					@if(isset($record->pp->files))
						@if($record->pp->files->count() > 0)
						<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
							<a href="{{ url('download', $record->id).'/uji-serah-terima' }}">
								<i class="download icon"></i></a>
							</div>
						@endif
					@endif
				@endif
			</td>
		</tr>
		<tr>
			<td><label>Catatan</label></td>
			<td>:</td>
			<td>{{ $record->catatan or '' }}
			</td>
		</tr>
	</table>
	<div class="actions">
		<div class="ui two column grid">
			<div class="left aligned column">
				<br>
				<div class="ui gray deny labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
		</div>	
	</div>
</div>
<div class="ui bottom demo tab" data-tab="second">
	<table id="example" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
		<thead>
			<tr class="middle aligned">
				<th width="30px" class="center aligned">No</th>
				<th width="300px" class="center aligned">Jenis Pengujian</th>
				<th width="300px"class="center aligned">Merk</th>
				<th width="150px" class="center aligned">Tipe</th>
				<th width="100px" class="center aligned">Jumlah</th>
				<th width="100px" class="center aligned">Satuan</th>
				<th width="200px" class="center aligned">Spesifikasi</th>
				<th width="200px" class="center aligned">Jadwal Mulai</th>
				<th width="200px" class="center aligned">Jadwal Selesai</th>
			</tr>
		</thead>
		<tbody class="detail fluid container">
			@foreach($record->detail as $key => $data)
			<tr class="list-row-{{$data->id}}" id="data-row" data-id="{{$data->id}}">
				<td class="center aligned numbor">{{ $key+1 }}</td>
				<input type="hidden" name="exists[]" value="{{$data->id}}">
				<td class="pjg">
					<div class="ui fluid input">
						<span>{{ $data->jenis->nama }}</span>
					</div>
				</td>
				<td>
					<div class="ui fluid input">
						<span class="sedit-label lrow-{{$data->id}}">{{$data->merk}}</span>
					</div>
				</td>
				<td>
					<div class="ui fluid input">
						<span class="sedit-label lrow-{{$data->id}}">{{$data->tipe}}</span>
					</div>
				</td>
				<td style="text-align: right;">
					<span class="sedit-label lrow-{{$data->id}}">{{ rtrim(rtrim(number_format($data->jumlah,2,',','.'), '0'), ',')}}</span>
				</td>
				<td>
					<div class="ui fluid input">
						<span class="sedit-label lrow-{{$data->id}}">{{ ($data->satuan) ? $data->satuan->satuan : '-' }}</span>
					</div>
				</td>
				<td>
					<div class="ui fluid input">
						<span class="sedit-label lrow-{{$data->id}}">{{$data->spesifikasi}}</span>
					</div>
				</td>
				<td>
					<div class="ui fluid input">
						<span class="sedit-label lrow-{{$data->id}}">{{ DateToStringYear($data->tgl_mulai) }}</span>
					</div>
				</td>
				<td>
					<div class="ui fluid input">
						<span class="sedit-label lrow-{{$data->id}}">{{ DateToStringYear($data->tgl_selesai) }}</span>
					</div>
				</td>
			</tr>			
			@endforeach
		</tbody>
	</table>
	<div class="actions">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
	</div>
</div>
<div class="ui bottom demo tab" data-tab="third">
	{!! riwayatPengujian($record->pp) !!}
	{!! riwayatAktivitas($record->pp) !!}
	<div class="actions">
		<div class="ui two column grid">
			<div class="left aligned column">
				<br>
				<div class="ui gray deny labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
		</div>	
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
        $(document).ready(function(){
	        $(document).on('click', '.preview', function(e){
				var id = $(this).data('id');
				var url = "{{ url($pageUrl) }}/"+id+"/preview";
				console.log(url)
				loadModal({
	                'url' : url,
	                'modal' : 'longer modal',
	                'formId' : '#dataForm',
	                'onShow' : function(){ 
	                    onShow();
	                },
	            })
			});

			$(document).on('click', '.preview_multiple', function(e){
				var id = $(this).data('id');
				var url = "{{ url($pageUrl) }}/"+id+"/preview-multiple/pendaftaran-pengujian";
				loadModal({
	                'url' : url,
	                'modal' : 'longer modal',
	                'formId' : '#dataForm',
	                'onShow' : function(){ 
	                    onShow();
	                },
	            })
			});	
             $('#example').DataTable( {
	            "paging":   10,
	            "ordering": false,
	            "lengthChange": false,
	            "filter": false,
	            "info":     true,
	            language: {
	                url: "{{ asset('plugins/datatables/Indonesian.json') }}"
	            },
        	});
        })
    </script>
@append