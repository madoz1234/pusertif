<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Item Uji</div>
<div class="ui warning list-uji hidden message" style="margin-left: 10px;margin-right: 10px; width: 48%;margin-bottom: -10px;text-align: center;">
	Jenis Pengujian atau Nama Merk Sudah ada.
</div>
<div class="content">
	<form class="ui form" id="formUji" action="{{ url($pageUrl.'request/item-uji') }}" method="POST">
		{!! csrf_field() !!}
		<table class="table" style="border-radius: 0; margin: 0">
			<tr>
				<td style="width: 22%;"><b>Jenis Pengujian</b></td>
				<td width="10">:</td>
				<td style="width: 100%;">
					<div class="field">
						<select name="jenis_id" id="jenis_id" class="watcher ui search pilihan dropdown">
							<option value="">Pilih Jenis Pengujian</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td style="width: 22%;"><b>Merk</b></td>
				<td width="10">:</td>
				<td style="width: 100%;">
					<div class="field">
						<input type="text" name="merk" id="merk" placeholder="Merk">
					</div>
				</td>
			</tr>
			<tr>
				<td style="width: 22%;"><b>Tipe</b></td>
				<td width="10">:</td>
				<td style="width: 100%;">
					<div class="field">
						<input type="text" name="tipe" id="tipe" placeholder="Tipe">
					</div>
				</td>
			</tr>
			<tr>
				<td style="width: 22%;"><b>Jumlah</b></td>
				<td width="10">:</td>
				<td style="width: 100%;">
					<div class="field">
						<input type="text" name="jumlah" id="jumlah" class="number" placeholder="Jumlah">
					</div>
				</td>
			</tr>
			<tr>
				<td style="width: 22%;"><b>Satuan</b></td>
				<td width="10">:</td>
				<td style="width: 100%;">
					<div class="field">
						<select name="satuan_id" id="satuan_id" class="watcher ui fluid search pilihan dropdown">
							{!!
								\App\Models\Master\Satuan::optionsb('satuan', 'id', [
									'filters' => [
									],
									'selected' => old('satuan_id')
								])
								!!}
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td style="width: 22%;"><b>Spesifikasi</b></td>
				<td width="10">:</td>
				<td style="width: 100%;">
					<div class="field">
						<textarea name="spesifikasi" class="areas input" id="spesifikasi" placeholder="Spesifikasi" rows="2"></textarea>
					</div>
				</td>
			</tr>
			<tr>
				<td style="width: 22%;"><b>Jadwal Pelaksanaan</b></td>
				<td width="10">:</td>
				<td style="width: 100%;">
					<div class="two fields">
						<div class="ui calendar labeled input field" id="rangestart">
							<div class="ui input left icon">
								<i class="calendar icon date"></i>
								<input name="tgl_mulai" type="text" id="tgl_mulai" placeholder="Mulai">
							</div>
						</div>
						<div class="ui calendar labeled input field" id="rangeend">
							<div class="ui input left icon">
								<i class="calendar icon date"></i>
								<input name="tgl_selesai" type="text" id="tgl_selesai" placeholder="Selesai">
							</div>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button" style="background-color: #363636;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button add-uji" style="background-color: #00AEEF;">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$('.ui.pilihan').dropdown();
	$('.tgl').calendar({
		ampm: false,
		type: 'date',
		// maxDate: tanggal,
		formatter: {
			date: function (date, settings) {
				if (!date) return '';
				let momentDate = moment(date)
				return momentDate.format('YYYY-MM-DD')
			}
		}
	});
	$("#merk").on("keypress keyup blur",function (e) {    
		$('.ui.warning.list-uji').addClass('hidden');
		$('.add-uji').removeClass('disabled');
	});
	$(".number").on("keypress keyup blur",function (e) {    
		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	});
	$.ajax({
		url: '{{ url('ajax/option/jenisOrder') }}',
		type: 'POST',
		data: {
			_token: "{{ csrf_token() }}",
			jenis_pelayanan_id: $('select[name=jenis_pelayanan_id]').val(),
			lingkup_id: $('select[name=lingkup_id]').val(),
		},
	})
	.done(function(response) {
		$('select[name=jenis_id]').html(response)
	})
	.fail(function() {
		console.log("error");
	});
</script>