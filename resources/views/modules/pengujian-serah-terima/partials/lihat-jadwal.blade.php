<div class="jzdbox1 jzdbasf jzdcal">
	<input type="hidden" name="bulan" value="{{ (int)$bulan }}">
	<input type="hidden" name="prioritas" value="{{ (int)$prioritas }}">
	<input type="hidden" name="tahun" value="{{ (int)$tahun }}">
	<div class="triangle-left"></div>
	<div class="jzdcalt" id="tahun">{{ $tahun }}</div>
    <div class="triangle-right"></div>
	<br>
	<span id="data-1" class="circle warna-{{ $tahun }}-1">Januari</span>
	<span id="data-2" class="circle warna-{{ $tahun }}-2">Februari</span>
	<span id="data-3" class="circle warna-{{ $tahun }}-3">Maret</span>
	<span id="data-4" class="circle warna-{{ $tahun }}-4">April</span>
	<span id="data-5" class="circle warna-{{ $tahun }}-5">Mei</span>
	<span id="data-6" class="circle warna-{{ $tahun }}-6">Juni</span>
	<span id="data-7" class="circle warna-{{ $tahun }}-7">Juli</span>
	<span id="data-8" class="circle warna-{{ $tahun }}-8">Agustus</span>
	<span id="data-9" class="circle warna-{{ $tahun }}-9">September</span>
	<span id="data-10" class="circle warna-{{ $tahun }}-10">Oktober</span>
	<span id="data-11" class="circle warna-{{ $tahun }}-11">November</span>
	<span id="data-12" class="circle warna-{{ $tahun }}-12">Desember</span>
	<table class="ui compact celled table" style="border-radius: 0; margin: 0;background-color: transparent;border:none;">
		<tr class="top aligned">
			<td>
				<div class="ui list">
					<div class="item"></div>
					<div class="item"></div>
					<div class="item"></div>
					<div class="item"></div>
					<div class="item"><i>*Prioritas :</i>  <a class="ui label" style="background-color: #f40d0d;"> </a></div>
					<div class="item"><i>*Reguler : </i> <a class="ui label" style="background-color: #c8e4d6;"> </a></div>
				</div>
			</td>
		</tr>
	</table>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		var currentTime = new Date();
		var year = currentTime.getFullYear();
		$('[name=tahun]').val(year);
		$('#tahun').html(year);
		var x = $('[name=bulan]').val();
		var y = $('[name=prioritas]').val();
		var angka = (parseInt(x)+parseInt(y));
		var akhir = Math.abs((angka - 12));
		if(angka > 12){
			for(var i=x;i<=12;i++){
				$('span.warna-'+year+'-'+i).css({'background-color':'#f40d0d;'});
			}
		}else{
			for(var i=x; i<angka; i++){
				$('span.warna-2019-'+i).css({'background-color':'#f40d0d;'});
			}
			for(var z=angka; z<=12;z++){
				$('span.warna-2019-'+z).css({'background-color':'#c8e4d6;', 'color':'#000000;'});
			}
		}
	});
</script>
