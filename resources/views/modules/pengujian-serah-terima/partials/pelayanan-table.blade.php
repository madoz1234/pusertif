<table class="ui compact celled table" style="border-radius: 0; margin: 5px 0">
	<tbody>
		<tr>
			<td style="text-align: center;font-weight: bold;background: #f9fafb; width: 300px;">Layanan</td>
			<td style="text-align: center;font-weight: bold;background: #f9fafb; width: 300px;">Lingkup</td>
			<td style="text-align: center;font-weight: bold;background: #f9fafb; width: 300px;">Jenis Pengujian</td>
		</tr>
		@if($record->count() != 0)
			@foreach($record as $i => $data)
			<tr class="top aligned">
				<td @if($data->hitung() > 0) rowspan="{{ $data->hitung() }}" @endif style="vertical-align : middle;text-align:left;text-justify: inter-word;">{{ $data->nama }}</td>
				@if($data->lingkup->count() > 0)
				@php 
				$lingkup = $data->lingkup->first();
				$j = 1;
				@endphp
				<td @if($lingkup->pengujian->count() > 0) rowspan="{{ $lingkup->pengujian->count() }}" @endif style="vertical-align : middle;text-align:left;text-justify: inter-word;">
					{{ $lingkup->nama }}
				</td>
				@else
				<td>1</td>
				@endif
				@if($data->lingkup->count() > 0 && $lingkup->pengujian->count() > 0)
				@php 
				$pengujian = $lingkup->pengujian->first();
				$k = 1;
				@endphp
				<td style="text-align: justify;text-justify: inter-word;">
					{{ $pengujian->nama }}
				</td>
				@else
				<td>2</td>
				@endif
			</tr>
			@foreach($data->lingkup as $key => $lingkup)
			@if($j > 1)
			<tr class="top aligned">
				<td @if($lingkup->pengujian->count() > 0) rowspan="{{ $lingkup->pengujian->count() }}" @endif class="lefted" style="text-align: justify;text-justify: inter-word;border-left: 1px solid rgba(34,36,38,.1);">
					{{ $lingkup->nama }}
				</td>
				@php $j=1; @endphp
				@if($lingkup->count() > 0 && $lingkup->pengujian->count() > 0)
				@php 
				$pengujian = $lingkup->pengujian->first();
				$k = 1;
				@endphp
				<td class="lefted" style="text-align: justify;text-justify: inter-word;">
					{{ $pengujian->nama }}
				</td>
				@else
				<td>3</td>
				@endif
			</tr>
			@endif
			@if($j==1)
			@php 
			$pengujians = $lingkup->pengujian;
			@endphp
			@foreach($pengujians as $koy => $pengujian)
			@if($k > 1)
			<tr class="top aligned">
				<td class="lefted" style="text-align: justify;text-justify: inter-word;border-left: 1px solid rgba(34,36,38,.1);">
					{{ $pengujian->nama }}
				</td>
			</tr>
			@endif
			@php $k++; @endphp
			@endforeach
			@php $j++; @endphp
			@else
			<td>4</td>
			@endif
			@endforeach
			@endforeach
		@else
			<tr class="center aligned">
				<td colspan="3"><i>Data Tidak Ditemukan</i></td>
			</tr>
		@endif
	</tbody>
</table>
