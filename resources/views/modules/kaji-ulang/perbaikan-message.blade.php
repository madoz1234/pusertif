<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Perbaikan</div>
<div class="content">
	<p>{!! $record->keterangan !!}</p>
</div>
<div class="actions">
	<div class="ui gray deny labeled icon button kembali">
		<i class="chevron left icon"></i>
		Kembali
	</div>
</div>