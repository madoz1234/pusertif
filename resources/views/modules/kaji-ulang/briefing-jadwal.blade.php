@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
		/*.disposisi.dropdown .menu .item:nth-child(1) {
			background-color: #00dd00cc;
			color: #fff;
		}
		.disposisi.dropdown .menu .item:nth-child(2) {
			background-color: #dd0000cc;
			color: #fff;
		}*/
	</style>
@append
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Briefing Teknis</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id)."/saveBriefingJadwal" }}" method="POST">
      {!! csrf_field() !!}
      <input type="hidden" name="_method" value="PUT">
      <input type="hidden" name="id" value="{{ $record->id }}">
		<div class="ui form">
			<table class="ui compact table" style="border-radius: 0; margin: 0">
				<tr>
					<td style="width: 22%;"><b>Upload Jadwal</b></td>
					<td width="10">:</td>
					<td style="width: 100%;">
						<div class="field">
							<div class="ui action choises input">
								<input type="text" class="isi_jadwal" readonly placeholder="Format .doc, .xls, .pdf, .jpg, .jpeg, .png">
								<input type="file" id="jadwal" name="jadwal[]" style="display: none!important;" multiple>
								<div class="ui icon jadwal button">
									<i class="cloud upload alternate icon"></i>
								</div>
							</div>
						</div>
					</td>
				</tr>
				{{-- <tr>
					<td style="width: 22%;"><b>Upload Notulen</b></td>
					<td width="10">:</td>
					<td style="width: 100%;">
						<div class="field">
							<div class="ui action choises input">
								<input type="text" class="isi_notulen" readonly placeholder="Format .xlsx">
								<input type="file" id="notulen" name="notulen[]" style="display: none!important;" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel" multiple>
								<div class="ui icon notulen button">
									<i class="cloud upload alternate icon"></i>
								</div>
							</div>
						</div>
					</td>
				</tr> --}}
			</table>
		</div>
		<div class="ui divider"></div>
		<div class="actions">
			<div class="ui black deny button" style="background-color: #363636;margin-left: -1px;">
				Batal
			</div>
			<div class="ui right floated save as page button" style="background-color: #00AEEF;">
				Submit
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		switch($('.disposisi.dropdown').dropdown('get text')){
				case 'Dialihkan':
					$('.disposisi.dropdown').css({
						'background-color': '#ffc000!important',
						'color': '#000'
					})
					break;
				case 'Diteruskan':
					$('.disposisi.dropdown').css({
						'background-color': '#00b0f0!important',
						'color': '#000'
					})
					break;
			}

		$(document).on('change', '.disposisi.dropdown', function (e){
			if($(this).find(":selected").val() == 1){
				$('#penerima').dropdown('clear');
				$(this).css({
					'background-color': '#ffc000!important',
					'color': '#000'
				})
			}else if($(this).find(":selected").val() == 2){
				$('#penerima').dropdown('clear');
				$(this).css({
					'background-color': '#00b0f0!important',
					'color': '#000'
				})
			}else{
				$('#penerima').dropdown('clear');
			}

			$.ajax({
				url: '{{ url('ajax/option/disposisi-msb') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					dispo_id: $(this).find(":selected").val(),
					pengirim: $('input[name=pengirim_id]').val(),
					layanan: $('input[name=layanan_id]').val()
				},
			})
			.done(function(response) {
				$('select[name^=penerima_id]').html(response)
			})
			.fail(function() {
				console.log("error");
			});
		});

		$(document).on('change', '[name^=jadwal]', function (e) {
			var count = e.target.files.length;
			if (count > 0) {
				$('.isi_jadwal').val(count+` Files Selected`);
			}
		});

		$(document).on('change', '[name^=notulen]', function (e) {
			var count = e.target.files.length;
			if (count > 0) {
				$('.isi_notulen').val(count+` Files Selected`);
			}
		});
	});
</script>