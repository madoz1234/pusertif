<div class="eight wide column center aligned">
	<canvas id="jadwalData" width="500" height="260"></canvas>
</div>
<script type="text/javascript">
		var jadwalData = $('#jadwalData');
		var myChart = new Chart(jadwalData, {
			type: 'bar',
			data : {
				labels: {!! $data['bulan'] !!},
				datasets: [
					{
						label: "Penerimaan Surat",
						backgroundColor: "#ff0505",
						data: {!! $data['penerimaan_surat'] !!},
					},
					{
						label: "Kaji Ulang",
						backgroundColor: "#f7ff05",
						data: {!! $data['kaji_ulang'] !!},
					},
					{
						label: "Surat Penawaran",
						backgroundColor: "#1cff05",
						data: {!! $data['surat_penawaran'] !!},
					},
					{
						label: "Konfirmasi Pembayaran",
						backgroundColor: "#053aff",
						data: {!! $data['konfirmasi'] !!},
					},
					{
						label: "Pengujian",
						backgroundColor: "#ff05f7",
						data: [0,0,0,0,0,0,0,0,0,0,0,0],
					},
				]
			},
			options:{ 
				scales: {
					yAxes: [{
						ticks: {
							stepSize: 1
						}
					}]
				}
			}
		});
</script>
