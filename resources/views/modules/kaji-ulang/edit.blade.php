@extends('layouts.form')
@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
		.ui.status.dropdown .menu .item:nth-child(1) {
			background-color: #00abffcc;
			color: #333;
		}
		.ui.status.dropdown .menu .item:nth-child(2) {
			background-color: #dd0000cc;
			color: #333;
		}
		.ui.status.dropdown .menu .item:nth-child(3) {
			background-color: #dddd00cc;
			color: #333;
		}
		th.aa{
			width: 10px;
		}
	</style>
@append

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			judul: ['empty'],
		};
	</script>
@endsection
@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
	{!! csrf_field() !!}
	<div class="ui form">
		<div class="ui top demo tabular menu">
		  <div class="active item" data-tab="first">Detil Order</div>
		  <div class="item" data-tab="second">Jenis & Jadwal</div>
		  <div class="item fn" data-tab="third">Riwayat Aktivitas</div>
		</div>
		<div class="ui bottom demo active tab" data-tab="first">
			<div class="two fields">
				<table class="ui compact table" style="border-radius: 0; margin: 0">
					<tr>
						<td width="250px"><label>Tgl Order</label></td>
						<td width="5px">:</td>
						<td>{{ DateToStringYear($record->tgl_order) }}</td>
					</tr>
					<tr>
						<td width="250px"><label>No Order</label></td>
						<td width="5px">:</td>
						<td>{{ $record->no_order }}</td>
					</tr>
					<tr>
						<td width="250px"><label>Layanan</label></td>
						<td width="5px">:</td>
						<td>{{ $record->pelayanan->nama }}</td>
					</tr>
					<tr>
						<td><label>Lingkup</label></td>
						<td>:</td>
						<td>{{ $record->lingkup->nama or '' }}</td>
					</tr>
					<tr>
						<td><label>Rencana Pelaksanaan</label></td>
						<td>:</td>
						<td>{{ BulanToString($record->rencana) }}</td>
					</tr>
					<tr>
						<td><label>Kelas</label></td>
						<td>:</td>
						<td class="field">
							<select name="kelas" id="kelas" class="watcher ui fluid search pilihan dropdown kelas">
								<option value="">Pilih Salah Satu</option>
								<option value="1" @if($record->kelas == 1) selected @endif>Reguler</option>
								<option value="2" @if($record->kelas == 2) selected @endif>Prioritas</option>
							</select>
						</td>
					</tr>
					<tr>
						<td><label>No Surat Permintaan</label></td>
						<td>:</td>
						<td>
							<div class="field" style="width: 14.5%;">
								<input id="no_surat" name="no_surat" type="text" placeholder="No Surat Permintaan" value="{{ $record->no_surat }}">
							</div>
						</td>
					</tr>
					<tr>
						<td><label>Tgl Surat Permintaan</label></td>
						<td>:</td>
						<td>
							<div class="field">
								<div class="ui calendar labeled input tgl" style="width: 14.5%;">
									<div class="ui input left icon">
										<i class="calendar icon date"></i>
										<input name="tgl_surat" type="text" placeholder="Tgl Surat Permintaan" value="{{$record->tgl_surat}}">
									</div>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td><label>Lampiran Surat Resmi Perusahaan</label></td>
						<td>:</td>
						<td>
							<div class="ui button preview" data-id="{{ $record->id }}" data-tooltip="Lihat" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
								<i class="file image outline icon" style="margin-left: 0px;margin-right: 0px;"></i>
							</div>
							<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
								<a href="{{ url($pageUrl).'/download/'.$record->id }}">
									<i class="download icon"></i></a>
								</div>
							</td>
						</tr>
						<tr>
							<td><label>Channel</label></td>
							<td>:</td>
							<td>@if($record->tipe == 1)
								Online
								@elseif($record->tipe == 2)
								AMS
								@else
								SPM
							@endif</td>
						</tr>
						<tr>
							<td><label>Peminta Jasa</label></td>
							<td>:</td>
							<td>{{ $record->user->pelanggans->perusahaan->nama or '' }}</td>
						</tr>
						<tr>
							<td><label>Kategori</label></td>
							<td>:</td>
							<td>
								@if($record->user->pelanggans->perusahaan->kategori == 0)
								PLN
								@elseif($record->user->pelanggans->perusahaan->kategori == 1)
								NON PLN
								@else
								A-PLN
								@endif
							</td>
						</tr>
						<tr>
							<td><label>Dokumen Pendukung</label></td>
							<td>:</td>
							<td>
								@if(isset($record))
								@if(isset($record->files))
								@if($record->files->count() > 0)
								<div class="ui button preview_multiple" data-id="{{ $record->id }}" data-tooltip="Lihat" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
									<i class="file image outline icon" style="margin-left: 0px;margin-right: 0px;"></i>
								</div>
								<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
									<a href="{{ url('download', $record->id).'/pendaftaran-pengujian' }}">
										<i class="download icon"></i></a>
									</div>
									@endif
									@endif
									@endif
								</td>
							</tr>
							<tr>
								<td><label>Catatan</label></td>
								<td>:</td>
								<td>{{ $record->catatan or '-' }}</td>
							</tr>
							<tr>
								<td><label>Keputusan </label></td>
								<td>:</td>
								<td>
									<select name="keputusan" id="keputusan" class="ui status fluid search dropdown">
										<option data-background-color="white" value="">Pilih Status</option>
										<option data-background-color="blue" value="1">DITERIMA</option>
										<option data-background-color="red" value="2">DITOLAK</option>
										<option data-background-color="yellow" value="3">DIDISKUSIKAN</option>
									</select>
								</td>
							</tr>
							<tr>
								<td><label>Keterangan </label></td>
								<td>:</td>
								<td>
									<div class="field" style="width:50%">
										<textarea name="keterangan" class="areas input" id="keterangan" placeholder="Keterangan" rows="3"></textarea>
									</div>
								</td>
							</tr>
				</table>
			</div>
			<div class="actions menu top" style="margin-top: 12px;">
				<div class="ui two column grid">
					<div class="left aligned column">
						<div class="ui gray deny labeled icon button" onclick="window.history.back()">
							Kembali
							<i class="chevron left icon"></i>
						</div>
					</div>
					<div class="right aligned column">
						<div class="ui blue labeled icon button next" data-tab="second">
							Selanjutnya
							<i class="chevron right icon"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="ui bottom demo tab" data-tab="second">
			<div align="right" style="margin-bottom: 10px;">
				Cek Jadwal&nbsp;&nbsp;
				<button type="button" class="ui blue mini icon append lihat_jadwal button"><i class="eye icon"></i></button>
			</div>
			<div style="overflow-x:auto;">
				<table id="example" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;margin-top: 60px;">
					<thead>
						<tr class="middle aligned">
							<th class="center aligned">No.</th>
							<th class="center aligned">Jenis Pengujian</th>
							<th colspan="2" class="center aligned">Lokasi</th>
							<th colspan="2" class="center aligned">Mata Uji</th>
							<th cols="50" class="center aligned aa">Spesifikasi</th>
							<th class="center aligned">Usulan Terkait Tarif</th>
							<th class="center aligned">Jumlah Benda Uji</th>
							<th class="center aligned">Jadwal Pengujian Tentative
								<br><div class="ui label">Rencana : {{ BulanToString($record->rencana) }}</div>
							</th>
						</tr>
					</thead>
					<tbody class="detail container detail">
						@php
							$i=0;
						@endphp
						@foreach($record->detail as $key => $data)
							<tr class="detail">
								<input type="hidden" name="detail[{{ $key }}][jenis_id]" value="{{ $data->id }}">
								<td class="center aligned">{{ $key+1 }}</td>
								<td class="left aligned">{{ $data->jenis->nama }}</td>
								<td class="right aligned lokasi-{{ $key }}" width="100px">
									<div class="ui transparent input" class="detail_lokasi-{{ $key }}">
										<select name="detail[{{ $key }}][data][0][lokasi]" class="ui fluid transparent search dropdown lokasi field disa">
											<option value="" data-id="{{ $key }}" data-parent="0">Pilih Lokasi</option>
											<option value="1" data-id="{{ $key }}" data-parent="0">in-house</option>
											<option value="2" data-id="{{ $key }}" data-parent="0">on-site</option>
										</select>
										<div class="ui transparent fluid input field ket_lokasi-{{ $key }}-0" style="width: 150px;">
											<textarea rows="1" class="inline disa" id="detail[{{ $key }}][data][0][ket_lokasi]" name="detail[{{ $key }}][data][0][ket_lokasi]" placeholder="Lokasi"></textarea>
										</div>
										<div class="ui transparent">
										<button type="button" class="ui red mini icon button disab" data-tooltip="Hapus Lokasi" data-position="top center"><i class="close icon"></i></button>
										</div>
									</div>
								</td>
								<td width="50px;" style="border-left: none;margin-top: -10px;">
									<button type="button" style="top: -6px;" class="ui green mini icon tambah_lokasi button disab" data-tooltip="Tambah Lokasi" data-position="top center" data-id="{{ $key }}"><i class="plus icon"></i></button>
								</td>
								<td class="mata_uji-{{ $key }}" width="200">
									<div class="ui transparent input" class="detail_mata_uji-{{ $key }}">
										<div class="field" style="width: 200px !important;">
											<input type="text" name="detail[{{ $key }}][mata_uji][0][data]" class="mata_uji" placeholder="Mata Uji">
										</div>
										<div>
										<button type="button" class="ui red mini icon button disab" data-tooltip="Hapus Mata Uji" data-position="top center"><i class="close icon"></i></button>
										</div>
									</div>
								</td>
								<td width="50px;" style="border-left: none;">
									<button type="button" class="ui green mini icon tambah_mata_uji button disab" data-tooltip="Tambah Mata Uji" data-position="top center" data-id="{{ $key }}"><i class="plus icon"></i></button>
								</td>
								<td class="right aligned" style="width:300px; display:inline-block; overflow:hidden; text-overflow:clip; white-space:nowrap;">
									<div class="ui transparent fluid input field">
										<textarea name="detail[{{ $key }}][spesifikasi]" id="{{ $key }}" rows="10" cols="200" class="spesifikasi" placeholder="Spesifikasi">{{ $data->spesifikasi }}</textarea>
									</div>
								</td>
								<td class="left aligned">
									<div class="ui transparent fluid input field">
										<input type="text" name="detail[{{ $key }}][tarif]" class="tarif" style="text-align: left;" placeholder="Usulan Terkait Tarif">
									</div>
								</td>
								<td class="right aligned">
									<div class="ui transparent fluid input field">
										<input type="text" name="detail[{{ $key }}][jumlah]" style="text-align: right;" placeholder="Jumlah Benda Uji" value="{{ $data->jumlah }}" data-inputmask="'alias' : 'numeric'">
										<label style="margin-left: 2vh;">{{ $data->satuan->satuan }}</label>
									</div>
								</td>
								<td>
									<div class="ui tentative_start field">
										<div class="ui transparent input left icon">
											<i class="calendar icon"></i><label style="margin-left: 2vh;">Start</label>
											<input name="detail[{{ $key }}][tentative_start]" class="start" type="text" placeholder="Tentative Start">
										</div>
									</div>
									<div class="ui tentative_end field">
										<div class="ui transparent input left icon">
											<i class="calendar icon"></i><label style="margin-left: 2vh;">End &nbsp;</label>
											<input name="detail[{{ $key }}][tentative_end]" class="end" type="text" placeholder="Tentative End">
										</div>
									</div>
								</td>
								{{-- <td>
									<div class="ui tgl_laporan_start field">
										<div class="ui transparent input left icon">
											<i class="calendar icon"></i>
											<input name="detail[{{ $key }}][tgl_laporan_start]" type="text" placeholder="Tanggal Laporan Start">
										</div>
									</div>
									<div class="ui tgl_laporan_end field">
										<div class="ui transparent input left icon">
											<i class="calendar icon"></i>
											<input name="detail[{{ $key }}][tgl_laporan_end]" type="text" placeholder="Tanggal Laporan End">
										</div>
									</div>
								</td> --}}
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="actions menu top" style="margin-top: 12px;">
				<div class="ui two column grid">
					<div class="left aligned column">
						<div class="ui gray deny labeled icon button next" data-tab="first">
							<i class="chevron left icon"></i>
							Sebelumnya
						</div>
					</div>
					<div class="right aligned column">
						<div class="ui green labeled icon button save as page">
							Simpan
							<i class="save icon"></i>
						</div>
					</div>
				</div>		
			</div>
		</div>
		<div class="ui bottom demo tab" data-tab="third">
			{!! riwayatPengujian($record) !!}
			{!! riwayatAktivitas($record) !!}
			<div class="actions">
				<div class="ui two column grid">
					<div class="left aligned column">
						<br>
						<div class="ui gray deny labeled icon button" onclick="window.history.back()">
							<i class="chevron left icon"></i>
							Kembali
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
</form>
@endsection
@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	//Calender
	$(document).ready(function() {
		$(document).on('click', '.preview', function(e){
			var id = $(this).data('id');
			var url = "{{ url($pageUrl) }}/"+id+"/preview";
			console.log(url)
			loadModal({
                'url' : url,
                'modal' : 'longer modal',
                'formId' : '#dataForm',
                'onShow' : function(){ 
                    onShow();
                },
            })
		});

		$(document).on('click', '.preview_multiple', function(e){
			var id = $(this).data('id');
			var url = "{{ url($pageUrl) }}/"+id+"/preview-multiple/pendaftaran-pengujian";
			loadModal({
                'url' : url,
                'modal' : 'longer modal',
                'formId' : '#dataForm',
                'onShow' : function(){ 
                    onShow();
                },
            })
		});
		// $("textarea").prop('disabled', true);
		$('.disa :input').attr('disabled', true);
		$("input.mata_uji").attr("disabled", true);
		$("input.spesifikasi").attr("disabled", true);
		$("input.tarif").attr("disabled", true);
		$("input.start").attr("disabled", true);
		$("input.end").attr("disabled", true);
		$("input.jumlah").attr("disabled", true);
		$('.disab').attr('disabled', true);

		$('select[name=keputusan]').on('change', function(){
			var check = $(this).val();
			if(check == 1){
				// $("textarea").prop('disabled', false);
				$('.disa :input').attr('disabled', false);
				$("input.mata_uji").attr("disabled", false);
				$("input.spesifikasi").attr("disabled", false);
				$("input.tarif").attr("disabled", false);
				$("input.start").attr("disabled", false);
				$("input.end").attr("disabled", false);
				$("input.jumlah").attr("disabled", false);
				$('.disab').prop("disabled", false);
			}else{
				// $("textarea").prop('disabled', true);
				$('.disa :input').attr('disabled', true);
				$("input.mata_uji").attr("disabled", true);
				$("input.spesifikasi").attr("disabled", true);
				$("input.tarif").attr("disabled", true);
				$("input.start").attr("disabled", true);
				$("input.end").attr("disabled", true);
				$("input.jumlah").attr("disabled", true);
				$('.disab').attr('disabled', true);
			}
		})

		$('select[name=kelas]').on('change', function(){
			var check = $(this).val();
			if(check == 1){
				swal({
					text: "Apakah Anda yakin mengganti kelas dari Prioritas ke Reguler!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya',
					cancelButtonText: 'Batal',
				}).then((result) => {
				})
			}else if(check == 2){
				swal({
					text: "Apakah Anda yakin mengganti kelas dari Reguler ke Prioritas!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya',
					cancelButtonText: 'Batal',
				}).then((result) => {
				})
			}else{
				swal({
					text: "Mohon data inputkan kelas!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya',
					cancelButtonText: 'Batal',
				}).then((result) => {
				})
			}
		})

		$(document).on('click','.next',function(){
            var data = $(this).data('tab');
            var dataPrev = $(this).data('previous');
            $('.menu').find('.item').removeClass('active');
            $('.menu').find("[data-tab='" + data + "']").addClass('active');
        });
		$('.menu .item').tab();
        $('.next').tab();

        var nowDate = new Date();
        var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), 0, 0, 0, 0);
        var tanggal = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);

        $('.timepickerdate').calendar({
        	ampm: false,
        	type: 'date',
        	endCalendar: tanggal,
        	formatter: {
        		date: function (date, settings) {
        			if (!date) return '';
        			let momentDate = moment(date)
        			return momentDate.format('YYYY-MM-DD')
        		}
        	}
        });

        $('.watcher').dropdown({
        });

        var startCalendar = null;

    	$('.tentative_start').calendar({
    		type: 'date',
    		formatter: {
    			date: function (date, settings) {
    				if (!date) return '';
    				let momentDate = moment(date)
    				return momentDate.format('YYYY-MM-DD')
    			}
    		},
    		onChange: function () {
    			startCalendar = $(this)
    			var element = $(this).parents('tr').find('td div.tentative_end');
    			element.calendar({
    				type: 'date',
    				startCalendar: startCalendar,
    				formatter: {
    					date: function (date, settings) {
    						if (!date) return '';
    						let momentDate = moment(date)
    						return momentDate.format('YYYY-MM-DD')
    					}
    				}
    			})
    			$(this).parents('tr').find('td div.tentative_end').find('div').find('input').focus();
    		}
    	});

    	$('.tgl_laporan_start').calendar({
    		type: 'date',
    		formatter: {
    			date: function (date, settings) {
    				if (!date) return '';
    				let momentDate = moment(date)
    				return momentDate.format('YYYY-MM-DD')
    			}
    		},
    		onChange: function () {
    			startCalendar = $(this)
    			var element = $(this).parents('tr').find('td div.tgl_laporan_end');
    			element.calendar({
    				type: 'date',
    				startCalendar: startCalendar,
    				formatter: {
    					date: function (date, settings) {
    						if (!date) return '';
    						let momentDate = moment(date)
    						return momentDate.format('YYYY-MM-DD')
    					}
    				}
    			})
    			$(this).parents('tr').find('td div.tgl_laporan_end').find('div').find('input').focus();
    		}
    	});

	    $(".number").on("keypress keyup blur",function (e) {    
            $(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
        });

        $('.ui.lokasi.dropdown').css({
        	'width': '100px'
        });
        $('.ui.status.dropdown').css({
        	'width': '200px'
        });

        $('.ui.kelas.dropdown').css({
        	'width': '200px'
        });

        switch($('.ui.status.dropdown').dropdown('get text')){
			case 'DITERIMA':
				$('.ui.status.dropdown').css({
					'background-color': '#00abffcc!important',
					'color': '#333'
				})
				break;
			case 'DITOLAK':
				$('.ui.status.dropdown').css({
					'background-color': '#dd0000cc!important',
					'color': '#333'
				})
				break;
			case 'DIDISKUSIKAN':
				$('.ui.status.dropdown').css({
					'background-color': '#dddd00cc!important',
					'color': '#333'
				})
				break;
		}

        $('.ui.status.dropdown').dropdown({
        	onChange: function(value, text, $selectedItem) {
        		switch(text){
        			case 'DITERIMA':
        				$(this).css({
        					'background-color': '#00abffcc!important',
        					'color': '#333'
        				})
        				break;
        			case 'DITOLAK':
        				$(this).css({
        					'background-color': '#dd0000cc!important',
        					'color': '#333'
        				})
        				break;
        			case 'DIDISKUSIKAN':
        				$(this).css({
        					'background-color': '#dddd00cc!important',
        					'color': '#333'
        				})
        				break;
        		}
		    }
        })

        $(document).on('click', '.lihat_jadwal', function(event) {
			loadModal({
				'url' : '{{ url($pageUrl) }}/lihatJadwal',
				'modal' : '.{{ $modalSize }}.modal',
				'onShow' : function(){ 
					onShow();
				},
			})
        });

        var num = 1;
	    $(document).on('click', '.tambah_mata_uji', function(e){
	    	var id = $(this).data("id");
	    	var e = num++;
	    	var html = `<div class="ui transparent input">
									<div class="field" style="width: 200px !important;">
										<input type="text" name="detail[`+id+`][mata_uji][`+e+`][data]" class="mata_uji" placeholder="Mata Uji">
									</div>
									<div>
									<button type="button" class="ui red mini icon hapus_mata_uji button disab" data-tooltip="Hapus Mata Uji" data-position="top center"><i class="close icon"></i></button>
									</div>
								</div>`;

	    	$('.mata_uji-'+id).append(html);
	    });

	    var nz = 1;
	    $(document).on('click', '.tambah_lokasi', function(e){
	    	var id = $(this).data("id");
	    	var e = nz++;
		    	var html = `<div class="ui transparent input" class="detail_lokasi-`+id+`">
										<select name="detail[`+id+`][data][`+e+`][lokasi]" class="ui fluid transparent search dropdown lokasi field">
											<option value="" data-id="`+id+`" data-parent="`+e+`">Pilih Lokasi</option>
											<option value="1" data-id="`+id+`" data-parent="`+e+`">in-house</option>
											<option value="2" data-id="`+id+`" data-parent="`+e+`">on-site</option>
										</select>
										<div class="ui transparent fluid input field ket_lokasi-`+id+`-`+e+`" style="width: 150px;">
											<textarea rows="1" class="inline" id="detail[`+id+`][data][`+e+`][ket_lokasi]" name="detail[`+id+`][data][`+e+`][ket_lokasi]" placeholder="Lokasi"></textarea>
										</div>
										<div class="ui transparent">
										<button type="button" class="ui red mini icon hapus_lokasi button disab" data-tooltip="Hapus Lokasi" data-position="top center"><i class="close icon"></i></button>
										</div>
									</div>`;
	    	$('.lokasi-'+id).append(html);
	    	$('.lokasi').dropdown({
	    		onChange: function(value, text, $selectedItem) {
	    			var id = $(this).find(':selected').attr('data-id');
	    			var par = $(this).find(':selected').attr('data-parent')
	    			if(value == 1){
	    				$(this).parents('tr').find('td div.ket_lokasi-'+id+'-'+par).find('textarea').val('');
	    				$(this).parents('tr').find('td div.ket_lokasi-'+id+'-'+par).find('textarea').val('Jln. Laboratorium Duren Tiga, Jakarta Selatan 12760').attr('readonly','readonly');
	    			}else{
	    				$(this).parents('tr').find('td div.ket_lokasi-'+id+'-'+par).find('textarea').val('').removeAttr("readonly");
	    				$(this).parents('tr').find('td div.ket_lokasi-'+id+'-'+par).find('textarea').focus();
	    			}
	    		}
	    	});
	    	$('.ui.lokasi.dropdown').css({
        		'width': '100px'
        	});
	    });

	    $(document).on('click', '.hapus_mata_uji', function (e){
			var row = $(this).closest('div.ui.input');
			row.remove();
		});

		$(document).on('click', '.hapus_lokasi', function (e){
			var row = $(this).closest('div.ui.input');
			row.remove();
		});

		$('.lokasi').dropdown({
	    	onChange: function(value, text, $selectedItem) {
	    		var id = $(this).find(':selected').attr('data-id');
	    		var par = $(this).find(':selected').attr('data-parent')
	    		console.log(id, par)
	    		if(value == 1){
	    			$(this).parents('tr').find('td div.ket_lokasi-'+id+'-'+par).find('textarea').val('');
		    		$(this).parents('tr').find('td div.ket_lokasi-'+id+'-'+par).find('textarea').val('Jln. Laboratorium Duren Tiga, Jakarta Selatan 12760').attr('readonly','readonly');
	    		}else{
	    			$(this).parents('tr').find('td div.ket_lokasi-'+id+'-'+par).find('textarea').val('').removeAttr("readonly");
		    		$(this).parents('tr').find('td div.ket_lokasi-'+id+'-'+par).find('textarea').focus();
	    		}
	    	}
	    });

		$('.tgl').calendar({
			ampm: false,
			type: 'date',
			// maxDate: tanggal,
			formatter: {
				date: function (date, settings) {
					if (!date) return '';
					let momentDate = moment(date)
					return momentDate.format('YYYY-MM-DD')
				}
			}
		});
	});
</script>
@append
@include('scripts.inputmask')
