@extends('layouts.form')

@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
		.ui.status.dropdown .menu .item:nth-child(1) {
			background-color: #00dd00cc;
			color: #fff;
		}
		.ui.status.dropdown .menu .item:nth-child(2) {
			background-color: #dd0000cc;
			color: #fff;
		}
		.ui.status.dropdown .menu .item:nth-child(3) {
			background-color: #dddd00cc;
			color: #333;
		}


		.ui.uji.dropdown .menu .item:nth-child(1) {
			background-color: #00dd00cc;
			color: #fff;
		}
		.ui.uji.dropdown .menu .item:nth-child(2) {
			background-color: #dd0000cc;
			color: #fff;
		}
		.ui.uji.dropdown .menu .item:nth-child(3) {
			background-color: #dddd00cc;
			color: #333;
		}
	</style>
@append

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			judul: ['empty'],
		};
	</script>
@endsection
@section('modals')
<div class="ui large modal add" id="jadwal">
	<div class="content">
		<label class="ui violet ribbon label" style="margin-bottom: 3px;">Cek Jadwal</label>
		<div class="sixteen wide column main-content">
			<div class="ui segments">
				<div class="ui segment">
					<div class="inline fields">
						<div class="ui input focus">
							<div class="ui field" id="tgl">
								<div class="ui input left icon">
									<i class="calendar icon"></i>
									<input name="tanggal" type="text" placeholder="Tanggal">
								</div>
							</div>
						</div>
						&nbsp;&nbsp;&nbsp;
						<div class="ui input focus">
							<select name="detail[0][mata_uji]" class="ui watcher fluid search dropdown>
								<option value="">Pilih Mata Uji</option>
								<option value="Mata Uji 1" selected>Mata Uji 1</option>
								<option value="Mata Uji 2" selected>Mata Uji 2</option>
								<option value="Mata Uji 3" selected>Mata Uji 3</option>
							</select>
						</div>
						<button type="button" class="ui teal icon filter button" data-content="Cari Data">
							<i class="search icon"></i>
							&nbsp;&nbsp;Cari
						</button>
					</div>
				</div>
			</div>
		</div>
		<table id="jadwal_kaji_ulang" class="ui compact celled table" style="border-radius: 0; margin: 5px 0;">
			<thead>
				<tr class="middle aligned">
					<th width="30" class="center aligned">No</th>
					<th class="center aligned" style="width: 200px">Tanggal Kaji Ulang</th>
					<th class="center aligned" style="width: 200px">No Order</th>
					<th class="center aligned">Jenis Pengujian</th>
					<th class="center aligned">Mata Uji</th>
					<th class="center aligned">PIC</th>
					<th class="center aligned">Klien</th>
				</tr>
			</thead>
			<tbody class="test">				
				<tr>
					<td class="center aligned">1</td>
					<td>
						2019-10-10
					</td>
					<td>
						1001-0011-0111
					</td>
					<td>
						Uji serah terima (MDU)
					</td>
					<td class="ui detail">
						<ul class="ui list">
							<li>Mata Uji 1</li>
							<li>Mata Uji 2</li>
							<li>Mata Uji 3</li>
						</ul>
					</td>
					<td class="ui detail">
						<ul class="ui list">
							<li>Rahmad Hidayatullah</li>
							<li>Abdul Wahid</li>
							<li>Adriyana</li>
						</ul>
					</td>
					<td>
						PT.Pragma Informatika
					</td>
				</tr>
				<tr>
					<td class="center aligned">2</td>
					<td>
						2019-10-11
					</td>
					<td>
						1001-0011-0112
					</td>
					<td>
						Uji serah terima (MDU)
					</td>
					<td class="ui detail">
						<ul class="ui list">
							<li>Mata Uji 1</li>
							<li>Mata Uji 2</li>
							<li>Mata Uji 3</li>
						</ul>
					</td>
					<td class="ui detail">
						<ul class="ui list">
							<li>Rahmad Hidayatullah</li>
							<li>Abdul Wahid</li>
							<li>Adriyana</li>
						</ul>
					</td>
					<td>
						PT.Pragma Informatika
					</td>
				</tr>
				<tr>
					<td class="center aligned">3</td>
					<td>
						2019-10-10
					</td>
					<td>
						1001-0011-0111
					</td>
					<td>
						Uji serah terima (MDU)
					</td>
					<td class="ui detail">
						<ul class="ui list">
							<li>Mata Uji 1</li>
							<li>Mata Uji 2</li>
							<li>Mata Uji 3</li>
						</ul>
					</td>
					<td class="ui detail">
						<ul class="ui list">
							<li>Rahmad Hidayatullah</li>
							<li>Abdul Wahid</li>
							<li>Adriyana</li>
						</ul>
					</td>
					<td>
						PT.Pragma Informatika
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
@endsection
@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.'1') }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="1">
	<div class="ui form">
		<div class="ui top demo tabular menu">
		  <div class="active item" data-tab="first">Detil Order</div>
		  <div class="item" data-tab="second">Jenis & Jadwal Pengujian</div>
		  <div class="item" data-tab="third">Usulan Tarif</div>
		</div>

		<div class="ui bottom demo active tab segment" data-tab="first">
			<div class="two fields">
				<table class="ui compact table" style="border-radius: 0; margin: 0">
					<tr>
						<td width="250px"><label>Tgl Order </label></td>
						<td width="5px">:</td>
						<td>2019-10-10</td>
					</tr>
					<tr>
						<td width="250px"><label>No Order </label></td>
						<td width="5px">:</td>
						<td>003-2019-0001</td>
					</tr>
					<tr>
						<td width="250px"><label>Layanan </label></td>
						<td width="5px">:</td>
						<td>Pengujian Sistem Pembangkit</td>
					</tr>
					<tr>
						<td><label>Jenis Alat</label></td>
						<td>:</td>
						<td>Dispenser</td>
					</tr>
					<tr>
						<td><label>Merk </label></td>
						<td>:</td>
						<td>Miyako</td>
					</tr>
					<tr>
						<td><label>Tipe Alat</label></td>
						<td>:</td>
						<td>Rumah Tangga</td>
					</tr>
					<tr>
						<td><label>Jumlah Sampel</label></td>
						<td>:</td>
						<td>2</td>
					</tr>
					<tr>
						<td><label>No Surat Permohonan </label></td>
						<td>:</td>
						<td>012392163976219</td>
					</tr>
					<tr>
						<td><label>Spesifikasi Teknis </label></td>
						<td>:</td>
						<td style="text-align: justify;text-justify: inter-word;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</td>
					</tr>
					<tr>
						<td><label>Lampiran Surat Resmi Perusahaan </label></td>
						<td>:</td>
						<td><a href="">A.jpg</a></td>
					</tr>
					<tr>
						<td><label>Channel </label></td>
						<td>:</td>
						<td>AMS</td>
					</tr>
					<tr>
						<td><label>Nama Klien </label></td>
						<td>:</td>
						<td>100011 - PT. Pragma Informatika</td>
					</tr>
					<tr>
						<td><label>Kategori Klien </label></td>
						<td>:</td>
						<td>PLN</td>
					</tr>
					<tr>
						<td><label>Keputusan </label></td>
						<td>:</td>
						<td>
							<select name="filter[jenis]" class="ui status fluid search dropdown">
								<option data-background-color="white" value="">Pilih Status</option>
								<option data-background-color="green" value="1">Diterima</option>
								<option data-background-color="red" value="2">Ditolak</option>
								<option data-background-color="yellow" value="3">Didiskusikan</option>
							</select>
						</td>
					</tr>
				</table>
			</div>
			<div class="actions menu top" style="margin-top: 12px;">
				<div align="right" class="right aligned column">
					<div class="ui blue deny labeled icon button next" data-tab="second">
						Selanjutnya
						<i class="chevron right icon"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="ui bottom demo tab segment" data-tab="second">
			<div align="right" style="margin-bottom: 10px;">
				<button type="button" class="ui blue mini icon append lihat_jadwal button" data-tooltip="Lihat Jadwal" data-position="left center"><i class="eye icon"></i></button>
			</div>
			<table class="ui compact table" style="border-radius: 0; margin: 0">
				<tr>
					<td style="width: 100px"><label>Jenis Pengujian </label></td>
					<td style="width: 20px">:</td>
					<td>
						<select name="hari" class="ui uji fluid search transparent dropdown">
							<option data-background-color="white" value="">Pilih Jenis Pengujian</option>
							<option data-background-color="green" value="1">UJI SERAH TERIMA (MDU)</option>
							<option data-background-color="red" value="2">UJI RUTIN</option>
							<option data-background-color="yellow" value="3">UJI JENIS</option>
						</select>
					</td>
				</tr>
			</table>
			<table class="ui compact celled table" style="border-radius: 0; margin: 5px 0;">
				<thead>
					<tr class="middle aligned">
						<th rowspan="2" width="30" class="center aligned">No</th>
						<th rowspan="2" class="center aligned">Mata Uji</th>
						<th rowspan="2" class="center aligned" style="width: 200px">Jumlah Benda Uji</th>
						<th rowspan="2" class="center aligned" style="width: 200px">Lokasi</th>
						<th rowspan="2" class="center aligned" style="width: 200px">Jadwal Pelaksanaan</th>
						<th rowspan="2" class="center aligned" style="width: 200px">Lampiran</th>
						<th rowspan="2" width="50" class="center aligned"><button type="button" class="ui green mini icon append tambah_detail button" data-tooltip="Tambah Data" data-position="left center"><i class="plus icon"></i></button></th>
					</tr>
				</thead>
				<tbody class="detail fluid container">				
					<tr>
						<td class="center aligned">1</td>
						<td>
							<select name="detail[0][mata_uji]" class="watcher ui fluid search transparent dropdown>
								<option value="">Pilih Mata Uji</option>
								<option value="Mata Uji 1" selected>Mata Uji 1</option>
								<option value="Mata Uji 2" selected>Mata Uji 2</option>
								<option value="Mata Uji 3" selected>Mata Uji 3</option>
							</select>
						</td>
						<td>
							<div class="ui transparent fluid input field">
								<input class="number" type="text" name="detail[0][jumlah]" placeholder="Jumlah Benda Uji" value="10">
							</div>
						</td>
						<td>
							<div class="ui transparent fluid input field">
								<select name="detail[0][hari]" class="ui lokasi fluid search transparent dropdown">
									<option value="">Pilih Lokasi</option>
									<option value="1">in-house</option>
									<option value="2">on site</option>
								</select>
							</div>
						</td>
						<td>
							<div class="ui timepickerdate field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[0][jadwal_pelaksanaan]" type="text" placeholder="Jadwal Pelaksanaan">
								</div>
							</div>
						</td>
						<td class="lefted">
							<div class="field">
								<div class="ui action choises input">
									<input type="text" readonly>
									<input type="file" name="detail[0][lampiran]" style="display: none!important;">
									<div class="ui icon button">
										<i class="cloud upload alternate icon"></i>
									</div>
								</div>
							</div>
						</td>
						<td class="center aligned">
							<button class="ui red mini icon remove hapus_data button" data-tooltip="Hapus Data" data-position="left center"><i class="remove icon"></i></button>
						</td>
					</tr>
					<tr>
						<td class="center aligned">2</td>
						<td>
							<select name="detail[1][mata_uji]" class="watcher ui fluid search transparent dropdown>
								<option value="">Pilih Mata Uji</option>
								<option value="Mata Uji 1" selected>Mata Uji 1</option>
								<option value="Mata Uji 2" selected>Mata Uji 2</option>
								<option value="Mata Uji 3" selected>Mata Uji 3</option>
							</select>
						</td>
						<td>
							<div class="ui transparent fluid input field">
								<input class="number" type="text" name="detail[1][jumlah]" placeholder="Jumlah Benda Uji" value="10">
							</div>
						</td>
						<td>
							<div class="ui transparent fluid input field">
								<select name="detail[1][hari]" class="ui lokasi fluid search transparent dropdown">
									<option value="">Pilih Lokasi</option>
									<option value="1">in-house</option>
									<option value="2">on site</option>
								</select>
							</div>
						</td>
						<td>
							<div class="ui timepickerdate field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[1][jadwal_pelaksanaan]" type="text" placeholder="Jadwal Pelaksanaan">
								</div>
							</div>
						</td>
						<td class="lefted">
							<div class="field">
								<div class="ui action choises input">
									<input type="text" readonly>
									<input type="file" name="detail[1][lampiran]" style="display: none!important;">
									<div class="ui icon button">
										<i class="cloud upload alternate icon"></i>
									</div>
								</div>
							</div>
						</td>
						<td class="center aligned">
							<button class="ui red mini icon remove hapus_data button" data-tooltip="Hapus Data" data-position="left center"><i class="remove icon"></i></button>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="actions menu top" style="margin-top: 12px;">
				<div class="ui two column grid">
					<div class="left aligned column">
						<div class="ui gray deny labeled icon button next" data-tab="first">
							<i class="chevron left icon"></i>
							Sebelumnya
						</div>
					</div>
					<div class="right aligned column">
						<div class="ui blue deny labeled icon button next" data-tab="third">
							Selanjutnya
							<i class="chevron right icon"></i>
						</div>
					</div>
				</div>		
			</div>
		</div>

		<div class="ui bottom demo tab segment" data-tab="third">
			<h1 class="ui center aligned header" style="line-height: 500px">
				U N D E R &nbsp; &nbsp; C O N S T R U C T I O N
			</h1>
		</div>
	</div>
@endsection
@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	//Calender
	$(document).ready(function() {
		$(document).on('click','.next',function(){
            var data = $(this).data('tab');
            var dataPrev = $(this).data('previous');
            $('.menu').find('.item').removeClass('active');
            $('.menu').find("[data-tab='" + data + "']").addClass('active');
        });
		$('.menu .item').tab();
        $('.next').tab();

        var nowDate = new Date();
        var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), 0, 0, 0, 0);
        var tanggal = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);

        $('.timepickerdate').calendar({
        	ampm: false,
        	type: 'date',
        	endCalendar: tanggal,
        	formatter: {
        		date: function (date, settings) {
        			if (!date) return '';
        			let momentDate = moment(date)
        			return momentDate.format('YYYY-MM-DD')
        		}
        	}
        });

        $('.watcher').dropdown({
        });

	    $(".number").on("keypress keyup blur",function (e) {    
            $(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
        });

        $('.ui.status.dropdown').css({
        	'width': '200px'
        });

        $('.ui.lokasi.dropdown').css({
        	'width': '200px'
        });

        $('.ui.uji.dropdown').css({
        	'width': '200px'
        });

        switch($('.ui.status.dropdown').dropdown('get text')){
			case 'Diterima':
				$('.ui.status.dropdown').css({
					'background-color': '#00dd00cc!important',
					'color': '#fff'
				})
				break;
			case 'Ditolak':
				$('.ui.status.dropdown').css({
					'background-color': '#dd0000cc!important',
					'color': '#fff'
				})
				break;
			case 'Didiskusikan':
				$('.ui.status.dropdown').css({
					'background-color': '#dddd00cc!important',
					'color': '#333'
				})
				break;
		}

        $('.ui.status.dropdown').dropdown({
        	onChange: function(value, text, $selectedItem) {
        		switch(text){
        			case 'Diterima':
        				$(this).css({
        					'background-color': '#00dd00cc!important',
        					'color': '#fff'
        				})
        				break;
        			case 'Ditolak':
        				$(this).css({
        					'background-color': '#dd0000cc!important',
        					'color': '#fff'
        				})
        				break;
        			case 'Didiskusikan':
        				$(this).css({
        					'background-color': '#dddd00cc!important',
        					'color': '#333'
        				})
        				break;
        		}
		    }
        })

        switch($('.ui.uji.dropdown').dropdown('get text')){
			case 'UJI SERAH TERIMA (MDU)':
				$('.ui.uji.dropdown').css({
					'background-color': '#00dd00cc!important',
					'color': '#fff'
				})
				break;
			case 'UJI RUTIN':
				$('.ui.uji.dropdown').css({
					'background-color': '#dd0000cc!important',
					'color': '#fff'
				})
				break;
			case 'UJI JENIS':
				$('.ui.uji.dropdown').css({
					'background-color': '#dddd00cc!important',
					'color': '#333'
				})
				break;
		}

        $('.ui.uji.dropdown').dropdown({
        	onChange: function(value, text, $selectedItem) {
        		switch(text){
        			case 'UJI SERAH TERIMA (MDU)':
        				$(this).css({
        					'background-color': '#00dd00cc!important',
        					'color': '#fff'
        				})
        				break;
        			case 'UJI RUTIN':
        				$(this).css({
        					'background-color': '#dd0000cc!important',
        					'color': '#fff'
        				})
        				break;
        			case 'UJI JENIS':
        				$(this).css({
        					'background-color': '#dddd00cc!important',
        					'color': '#333'
        				})
        				break;
        		}
		    }
        })

        $(document).on('click', '.lihat_jadwal', function(event) {
			$('#jadwal_kaji_ulang').DataTable( {
				"paging":   false,
				"ordering": false,
				"filter": false,
				"info":     true,
				"language": {
						url: "{{ asset('plugins/datatables/Indonesian.json') }}"
				},
			});
        	$('#jadwal').modal({
        		initModal: function(){
        			var nowDate = new Date();
        			var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), 0, 0, 0, 0);
        			var tanggal = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);

        			$('#tgl').calendar({
        				ampm: false,
        				type: 'date',
        				endCalendar: tanggal,
        				formatter: {
        					date: function (date, settings) {
        						if (!date) return '';
        						let momentDate = moment(date)
        						return momentDate.format('YYYY-MM-DD')
        					}
        				}
        			});

        			$('.ui.watcher.dropdown').css({
        				'width': '200px'
        			});

        			return false;
        		},
        		onHidden: function(){
        			$('#formModal').html(`<div class="ui inverted loading dimmer">
        				<div class="ui text loader">Loading</div>
        				</div>`);
        			dismissModal();
        		},
        		onDeny    : function(){
        			window.alert('Wait not yet!');
        			return false;
        		},
        	}).modal('show');
        });

	    var no = 1;
	    $(document).on('click', '.tambah_detail', function(e){
	    	var e = no++;
	    	var f = e+=1;
	    	var html = `
					<tr>
						<td class="center aligned">`+f+`</td>
						<td>
							<div>
								<select name="detail[`+e+`][mata_uji]" class="watcher ui fluid search transparent dropdown>
									<option value="">Pilih Mata Uji</option>
									<option value="Mata Uji 1">Mata Uji 1</option>
									<option value="Mata Uji 2">Mata Uji 2</option>
									<option value="Mata Uji 3">Mata Uji 3</option>
								</select>
							</div>
						</td>
						<td>
							<div class="ui transparent fluid input field">
								<input class="number" type="text" name="detail[`+e+`][jumlah]" placeholder="Jumlah Benda Uji">
							</div>
						</td>
						<td>
							<div class="ui transparent fluid input field">
								<select name="detail[`+e+`][hari]" class="ui watcher lokasi fluid search transparent dropdown">
									<option value="">Pilih Lokasi</option>
									<option value="1">in-house</option>
									<option value="2">on site</option>
								</select>
							</div>
						</td>
						<td>
							<div class="ui timepickerdate field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[`+e+`][jadwal_pelaksanaan]" type="text" placeholder="Jadwal Pelaksanaan">
								</div>
							</div>
						</td>
						<td class="lefted">
							<div class="field">
								<div class="ui action choises input">
									<input type="text" readonly>
									<input type="file" name="detail[`+e+`][lampiran]" style="display: none!important;">
									<div class="ui icon button">
										<i class="cloud upload alternate icon"></i>
									</div>
								</div>
							</div>
						</td>
						<td class="center aligned">
							<button class="ui red mini icon remove hapus_data button" data-tooltip="Hapus Data" data-position="left center"><i class="remove icon"></i></button>
						</td>
					</tr>`;
	    	$('.detail.fluid.container').append(html);
	    	var nowDate = new Date();
	    	var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), 0, 0, 0, 0);
	    	var tanggal = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);

	    	$('.timepickerdate').calendar({
	    		ampm: false,
	    		type: 'date',
	    		endCalendar: tanggal,
	    		formatter: {
	    			date: function (date, settings) {
	    				if (!date) return '';
	    				let momentDate = moment(date)
	    				return momentDate.format('YYYY-MM-DD')
	    			}
	    		}
	    	});

	    	$('.watcher').dropdown({
	    	});

	    	$(".number").on("keypress keyup blur",function (e) {    
	    		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	    	});

	    	$('.ui.status.dropdown').css({
	    		'width': '200px'
	    	});

	    	$('.ui.lokasi.dropdown').css({
	    		'width': '200px'
	    	});

	    	$('.ui.uji.dropdown').css({
	    		'width': '200px'
	    	});
	    });

		$(document).on('click', '.hapus_data', function (e){
			var row = $(this).closest('tr');
			row.remove();
		});
	});
</script>
@append
