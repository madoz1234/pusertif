<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Cek Jadwal</div>
<div class="content">
	<form class="ui filter form">
        <div class="inline fields">
        	<label><i class="filter circular inverted teal icon"></i> FILTER :</label>
        	<div class="field">
				<div class="ui calendar labeled input periode">
					<div class="ui input left icon">
						<i class="calendar icon date"></i>
						<input name="bulan" type="text" placeholder="Bulan">
					</div>
				</div>
			</div>
			<div class="field">
				<select name="lingkup" class="watcher ui search dropdown pilihan">
					{!!
						\App\Models\Master\Lingkup::options('nama', 'id', [], '(Pilih Lingkup)')
					!!}
				</select>
			</div>
			<div class="field">
				<select name="jenis_pengujian" class="watcher ui fluid search pilihan dropdown">
						<option value="">Pilih Jenis Pengujian</option>
				</select>
			</div>
			<button type="button" class="ui teal icon filter-pengujian button" data-content="{{trans('translator.Cari Data')}}">
				<i class="search icon"></i>
			</button>
			<button type="reset" class="ui icon reset-pengujian button" data-content="{{trans('translator.Bersihkan Pencarian')}}">
				<i class="refresh icon"></i>
			</button>
        </div>
    </form>
    <div class="listtable">
		@include('modules.kaji-ulang.jadwal-table')
    </div>
</div>
<div class="actions">
	<div class="ui gray deny labeled icon button kembali">
		<i class="chevron left icon"></i>
		Kembali
	</div>
</div>
<script type="text/javascript">
	var nowDate = new Date();
	var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), 0, 0, 0, 0);
	var tanggal = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);
	$('.periode').calendar({
		type: 'month',
		minDate: today,
		formatter: {
			date: function (date, settings) {
				if (!date) return '';
				var day = date.getDate();
				var month = date.getMonth() + 1;
				var year = date.getFullYear();
				var month = month < 10 ? '0' + month : '' + month;
				return year+'-'+month;
			}
		}
	});

	$(document).on('click', '.filter-pengujian.button', function(event) {
			var bulan = $("input[name='bulan']").val();
			var jenis_pengujian = $("select[name='jenis_pengujian']").val();
			if(bulan){
				$.ajax({
					url: '{{ url($pageUrl.'search-jadwal') }}',
					type: 'POST',
					data: {
						_token: "{{ csrf_token() }}",
						bulan : bulan,
						jenis_pengujian : jenis_pengujian,
					},
				})
				.done(function(response) {
					$('.listtable').html(response)
				})
				.fail(function() {
					console.log("error");
				});
			}else{
				swal({
					text: "Pilih bulan terlebih dahulu !",
					type: 'warning',
					showCancelButton: false,
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'Ok',
				}).then((result) => {
				})
			}
	});

	$('select[name=lingkup]').on('change', function(){
		$.ajax({
			url: '{{ url('ajax/option/jenis') }}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				lingkup_id: $('select[name=lingkup]').val(),
			},
		})
		.done(function(response) {
			$('select[name=jenis_pengujian]').html(response)
		})
		.fail(function() {
			console.log("error");
		});
	})

	$('.ui.search.dropdown').dropdown();
	$(document).on('click', '.kembali', function(e){
		$('#formModal').find('.loading.dimmer').removeClass('active');
	});

	$(".filter-pelayanan").on('change', function(){
		$.ajax({
			url: '{{ url('ajax/option/lingkup') }}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				jenis_pelayanan_id: $("select[name='filter[pelayanan]']").val()
			},
		})
		.done(function(response) {
			$("select[name='filter[lingkup]']").html(response)
		});
	});
</script>
