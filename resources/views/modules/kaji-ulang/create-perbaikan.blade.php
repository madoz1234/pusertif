@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
		/*.perbaikan.dropdown .menu .item:nth-child(1) {
			background-color: #00dd00cc;
			color: #fff;
		}
		.perbaikan.dropdown .menu .item:nth-child(2) {
			background-color: #dd0000cc;
			color: #fff;
		}*/
	</style>
@append
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Perbaikan</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id)."/savePerbaikan" }}" method="POST">
      {!! csrf_field() !!}
      <input type="hidden" name="_method" value="PUT">
      <input type="hidden" name="id" value="{{ $record->id }}">
	  <input type="hidden" name="pengirim_id" value="{{ auth()->user()->roles->first()->id }}">
	  <input type="hidden" name="pengirim" value="{{ auth()->user()->id }}">
	  <input type="hidden" name="layanan_id" value="{{ $record->layanan_id }}">
	  @if(auth()->user()->hasRole(['msb-kalibrasi', 'msb-siskit', 'msb-sistgi', 'msb-tegangan-rendah', 'msb-tegangan-tinggi']))
		  <input type="hidden" name="posisi" value="msb">
	  @elseif(auth()->user()->hasRole(['asman-kalibrasi', 'asman-siskit', 'asman-sistgi', 'asman-tegangan-rendah', 'asman-tegangan-tinggi']))
		  <input type="hidden" name="posisi" value="asmen">
	  @endif
	  <input type="hidden" name="perbaikan" value="1">
		<div class="ui form">
			<table class="ui compact table" style="border-radius: 0; margin: 0">
				<tr>
					<td width="100px"><label>No Permintaan </label></td>
					<td width="5px">:</td>
					<td>{{ $record->no_order }}</td>
				</tr>
				<tr>
					<td width="100px"><label>Disposisi</label></td>
					<td width="5px">:</td>
					<td class="field">
						<select name="tipe" class="ui status fluid search perbaikan dropdown">
							<option value="">Pilih Salah Satu</option>
							<option value="1" style="background-color: #ffc000!important" selected>Diperbaiki</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label>Dari </label></td>
					<td>:</td>
					<td>{{ auth()->user()->nama }}</td>
				</tr>
				<tr>
					<td><label>Kepada </label></td>
					<td>:</td>
					<td>
						<div class="field">
							<select name="penerima_id[]" id="penerima" class="watcher ui fluid search pilihan dropdown" multiple="">
								<option value="">Pilih Salah Satu</option>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td><label>Keterangan </label></td>
					<td>:</td>
					<td>
						<div class="field">
							<textarea name="keterangan" class="areas input" id="keterangan" placeholder="Keterangan" rows="2"></textarea>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="ui divider"></div>
		<div class="actions">
			<div class="ui black deny button" style="background-color: #363636;margin-left: -1px;">
				Batal
			</div>
			<div class="ui right floated save as page button" style="background-color: #00AEEF;">
				Submit
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$.ajax({
				url: '{{ url('ajax/option/perbaikan-msb') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					pp_id: {{ $record->id }},
					dispo_id: $('select[name=tipe]').val(),
					pengirim: $('input[name=pengirim_id]').val(),
					layanan: $('input[name=layanan_id]').val(),
				},
			})
			.done(function(response) {
				$('select[name^=penerima_id]').html(response)
			})
			.fail(function() {
				console.log("error");
			});

		switch($('.perbaikan.dropdown').dropdown('get text')){
				case 'Diperbaiki':
					$('.perbaikan.dropdown').css({
						'background-color': '#ffc000!important',
						'color': '#000'
					})
					break;
				case 'Diteruskan':
					$('.perbaikan.dropdown').css({
						'background-color': '#00b0f0!important',
						'color': '#000'
					})
					break;
			}

		$(document).on('change', '.perbaikan.dropdown', function (e){
			if($(this).find(":selected").val() == 1){
				$('#penerima').dropdown('clear');
				$(this).css({
					'background-color': '#ffc000!important',
					'color': '#000'
				})
			}else if($(this).find(":selected").val() == 2){
				$('#penerima').dropdown('clear');
				$(this).css({
					'background-color': '#00b0f0!important',
					'color': '#000'
				})
			}else{
				$('#penerima').dropdown('clear');
			}

			$.ajax({
				url: '{{ url('ajax/option/perbaikan-msb') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					pp_id: {{ $record->id }},
					dispo_id: $(this).find(":selected").val(),
					pengirim: $('input[name=pengirim_id]').val(),
					layanan: $('input[name=layanan_id]').val()
				},
			})
			.done(function(response) {
				$('select[name^=penerima_id]').html(response)
			})
			.fail(function() {
				console.log("error");
			});
		});
	});
</script>