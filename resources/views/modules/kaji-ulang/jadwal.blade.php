<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Cek Jadwal</div>
<div class="content">
	<form class="ui filter form">
        <div class="inline fields">
        	<label><i class="filter circular inverted teal icon"></i> FILTER :</label>
        	<div class="field" style="width: 300px;">
				<select name="perusahaan_id" class="watcher ui fluid search dropdown zz">
					{!!
						\App\Models\Master\Perusahaan::optionsa('nama', 'id', ['filters' => [function($q){
							return $q->where('status', 1);
						}],
						'selected' => old('id')
					], 'Peminta Jasa')
					!!}
				</select>
			</div>
			<button type="button" class="ui teal icon filter button" data-content="{{trans('translator.Cari Data')}}">
				<i class="search icon"></i>
			</button>
			<button type="reset" class="ui icon reset button" data-content="{{trans('translator.Bersihkan Pencarian')}}">
				<i class="refresh icon"></i>
			</button>
        </div>
    </form>
    <div id="listCalendar">
    </div>
</div>
<div class="actions">
	<div class="ui gray deny labeled icon button kembali">
		<i class="chevron left icon"></i>
		Kembali
	</div>
</div>

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/fullcalendar/scheduler.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/fullcalendar/moment.min.js') }}"></script>
<script src="{{ asset('plugins/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="{{ asset('plugins/fullcalendar/scheduler.min.js') }}"></script>
<script src="{{ asset('plugins/fullcalendar/locale/id.js') }}"></script>
@append

<script type="text/javascript">
	var nowDate = new Date();
	var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), 0, 0, 0, 0);
	var tanggal = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);
	$('.periode').calendar({
		type: 'month',
		minDate: today,
		formatter: {
			date: function (date, settings) {
				if (!date) return '';
				var day = date.getDate();
				var month = date.getMonth() + 1;
				var year = date.getFullYear();
				var month = month < 10 ? '0' + month : '' + month;
				return year+'-'+month;
			}
		}
	});

	$( document ).ready(function() {
		loadJadwal();
	});

	$(document).on('click', '.filter.button', function(event) {
		event.preventDefault();
		$('#listCalendar').fullCalendar('destroy');
		loadJadwal();
	});

	$(document).on('click', '.reset.button', function(event) {
		event.preventDefault();
		$('#listCalendar').fullCalendar('destroy');
		$("select[name=perusahaan_id]").val('')
		loadJadwal();
	});

	$('.ui.search.dropdown').dropdown();
	$(document).on('click', '.kembali', function(e){
		$('#formModal').find('.loading.dimmer').removeClass('active');
	});

	$(".filter-pelayanan").on('change', function(){
		$.ajax({
			url: '{{ url('ajax/option/lingkup') }}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				jenis_pelayanan_id: $("select[name='filter[pelayanan]']").val()
			},
		})
		.done(function(response) {
			$("select[name='filter[lingkup]']").html(response)
		});
	});

	function displayJadwal(resource,jadwal){
	  	$('.loading.dimmer').removeClass('active');
	    $('#listCalendar').fullCalendar({
	      locale: 'id',
	      contentHeight: 400,
	      plugins: [ 'resourceTimeline' ],
	      header: {
	        left: 'today prev,next',
	        center: 'title',
	      },
	      defaultView: 'timelineMonth',
	      resourceGroupField: 'building',
	      resourceColumns: [
	        {
	          labelText: 'Peminta Jasa',
	          field: 'title'
	        },
	      ],
	      resources: resource,
	      events: jadwal,
	      schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
	      viewRender: function(view, element) {
	        //note: this is a hack, i don't know why the view title keep showing "undefined" text in it.
	        //probably bugs in jquery fulllistCalendar
	        $('.fc-center')[0].children[0].innerText = view.title.replace(new RegExp("undefined", 'g'), ""); ;
	      },
	    });
	  }


	  function loadJadwal(){
	  	$('.search.dropdown').css({
	          'width': '300px'
	    });
	    $.ajax({
	      url: '{{ url('ajax/option/jadwal-kaji') }}',
	      type: 'POST',
	      data: {
	        _token: "{{ csrf_token() }}",
	        perusahaan_id: $('select[name=perusahaan_id]').val(),
	      },
	    })
	    .done(function(response) {
	    	console.log(response.jenis, response.jadwal)
	      displayJadwal(response.jenis,response.jadwal)
	    });
	  }
</script>
