@extends('layouts.form')

@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
	</style>
@append

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			judul: ['empty'],
		};
	</script>
@endsection

@section('content-body')
<div class="ui top demo tabular menu">
	<div class="active item" data-tab="first">Detail Order</div>
	<div class="item" data-tab="second">Jenis Pengujian</div>
	<div class="item" data-tab="third">Usulan Tarif</div>
	<div class="item" data-tab="four">Penjadwalan</div>
</div>

<div class="ui bottom demo active tab segment" data-tab="first">
	<div class="two fields">
		<table class="ui compact table" style="border-radius: 0; margin: 0">
			<tr>
				<td width="250px"><label>Tgl Order </label></td>
				<td width="5px">:</td>
				<td>2019-10-10</td>
			</tr>
			<tr>
				<td width="250px"><label>No Order </label></td>
				<td width="5px">:</td>
				<td>003-2019-0001</td>
			</tr>
			<tr>
				<td width="250px"><label>Layanan </label></td>
				<td width="5px">:</td>
				<td>Pengujian Sistem Pembangkit</td>
			</tr>
			<tr>
				<td><label>Jenis Alat</label></td>
				<td>:</td>
				<td>Dispenser</td>
			</tr>
			<tr>
				<td><label>Merk </label></td>
				<td>:</td>
				<td>Miyako</td>
			</tr>
			<tr>
				<td><label>Tipe Alat</label></td>
				<td>:</td>
				<td>Rumah Tangga</td>
			</tr>
			<tr>
				<td><label>Jumlah Sampel</label></td>
				<td>:</td>
				<td>2</td>
			</tr>
			<tr>
				<td><label>Nomor Surat Permohonan </label></td>
				<td>:</td>
				<td>012392163976219</td>
			</tr>
			<tr>
				<td><label>Spesifikasi Teknis </label></td>
				<td>:</td>
				<td style="text-align: justify;text-justify: inter-word;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</td>
			</tr>
			<tr>
				<td><label>Lampiran Surat Resmi Perusahaan </label></td>
				<td>:</td>
				<td><a href="">A.jpg</a></td>
			</tr>
			<tr>
				<td><label>Channel </label></td>
				<td>:</td>
				<td>AMS</td>
			</tr>
			<tr>
				<td><label>Nama Klien </label></td>
				<td>:</td>
				<td>PT. Pragma Informatika</td>
			</tr>
			<tr>
				<td><label>Kategori Klien </label></td>
				<td>:</td>
				<td>PLN</td>
			</tr>
			<tr>
				<td><label>Keputusan </label></td>
				<td>:</td>
				<td class="left aligned"><a class="ui green tag label">Diterima</a></td>
			</tr>
		</table>
	</div>
</div>

<div class="ui bottom demo tab segment" data-tab="second">
	<table class="ui compact table" style="border-radius: 0; margin: 0">
		<tr>
			<td style="width: 100px"><label>Jenis Pengujian </label></td>
			<td style="width: 20px">:</td>
			<td >Ujian Serah Terima (MDU)</td>
		</tr>
	</table>
	<table id="tab1" class="ui compact celled table" style="border-radius: 0; margin: 5px 0;">
		<thead>
			<tr class="middle aligned">
				<th rowspan="2" class="center aligned">No</th>
				<th rowspan="2" class="center aligned" style="width: 200px">Spesifikasi</th>
				<th rowspan="2" class="center aligned" style="width: 200px">Mata Uji</th>
				<th rowspan="2" class="center aligned">Usulan Tarif</th>
				<th rowspan="2" class="center aligned">Jumlah Benda Uji</th>
				<th colspan="2" class="center aligned">Jadwal Tentative</th>
				<th colspan="2" class="center aligned">Jadwal Reschedule</th>
				<th colspan="2" class="center aligned">Realisasi</th>
				<th colspan="2" class="center aligned">Tanggal Laporan</th>
				<th rowspan="2" class="center aligned">Lampiran</th>
			</tr>
			<tr class="middle aligned">
				<th style="">Start</th>
				<th>End</th>
				<th>Start</th>
				<th>End</th>
				<th>Start</th>
				<th>End</th>
				<th>Start</th>
				<th>End</th>
			</tr>
		</thead>
		<tbody>				
			<tr>
				<td class="center aligned">1</td>
				<td>
					Data Spesifikasi
				</td>
				<td class="ui detail">
					<ul class="ui list">
						<li>Mata Uji 1</li>
						<li>Mata Uji 2</li>
						<li>Mata Uji 3</li>
					</ul>
				</td>
				<td class="right aligned">
					Rp. 200.000
				</td>
				<td class="right aligned">
					20
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td class="lefted">
					<a href="">A.Jpg</a>
				</td>
			</tr>
			<tr>
				<td class="center aligned">2</td>
				<td>
					Data Spesifikasi
				</td>
				<td class="ui detail">
					<ul class="ui list">
						<li>Mata Uji 1</li>
						<li>Mata Uji 2</li>
						<li>Mata Uji 3</li>
					</ul>
				</td>
				<td class="right aligned">
					Rp. 200.000
				</td>
				<td class="right aligned">
					20
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td class="lefted">
					<a href="">A.Jpg</a>
				</td>
			</tr>
			<tr>
				<td class="center aligned">3</td>
				<td>
					Data Spesifikasi
				</td>
				<td class="ui detail">
					<ul class="ui list">
						<li>Mata Uji 1</li>
						<li>Mata Uji 2</li>
						<li>Mata Uji 3</li>
					</ul>
				</td>
				<td class="right aligned">
					Rp. 200.000
				</td>
				<td class="right aligned">
					20
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td>
					2019-08-08
				</td>
				<td class="lefted">
					<a href="">A.Jpg</a>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="ui bottom demo tab segment" data-tab="third">
	<h1 class="ui center aligned header" style="line-height: 500px">
		U N D E R &nbsp; &nbsp; C O N S T R U C T I O N
	</h1>
</div>

<div class="ui bottom demo tab segment" data-tab="four">
	<label class="ui violet ribbon label" style="margin-bottom: 3px;">Jadwal Kaji Ulang</label>
	<table id="tab2" class="ui compact celled table" style="border-radius: 0; margin: 5px 0;">
		<thead>
			<tr class="middle aligned">
				<th width="30" class="center aligned">No</th>
				<th class="center aligned" style="width: 200px">Tanggal Kaji Ulang</th>
				<th class="center aligned" style="width: 200px">No Order</th>
				<th class="center aligned">Jenis Pengujian</th>
				<th class="center aligned">Mata Uji</th>
				<th class="center aligned">PIC</th>
				<th class="center aligned">Klien</th>
			</tr>
		</thead>
		<tbody class="detail fluid container">				
			<tr>
				<td class="center aligned">1</td>
				<td>
					2019-10-10
				</td>
				<td>
					1001-0011-0111
				</td>
				<td>
					Uji serah terima (MDU)
				</td>
				<td class="ui detail">
					<ul class="ui list">
						<li>Mata Uji 1</li>
						<li>Mata Uji 2</li>
						<li>Mata Uji 3</li>
					</ul>
				</td>
				<td class="ui detail">
					<ul class="ui list">
						<li>Rahmad Hidayatullah</li>
						<li>Abdul Wahid</li>
						<li>Adriyana</li>
					</ul>
				</td>
				<td>
					PT.Pragma Informatika
				</td>
			</tr>
			<tr>
				<td class="center aligned">2</td>
				<td>
					2019-10-11
				</td>
				<td>
					1001-0011-0112
				</td>
				<td>
					Uji serah terima (MDU)
				</td>
				<td class="ui detail">
					<ul class="ui list">
						<li>Mata Uji 1</li>
						<li>Mata Uji 2</li>
						<li>Mata Uji 3</li>
					</ul>
				</td>
				<td class="ui detail">
					<ul class="ui list">
						<li>Rahmad Hidayatullah</li>
						<li>Abdul Wahid</li>
						<li>Adriyana</li>
					</ul>
				</td>
				<td>
					PT.Pragma Informatika
				</td>
			</tr>
			<tr>
				<td class="center aligned">3</td>
				<td>
					2019-10-10
				</td>
				<td>
					1001-0011-0111
				</td>
				<td>
					Uji serah terima (MDU)
				</td>
				<td class="ui detail">
					<ul class="ui list">
						<li>Mata Uji 1</li>
						<li>Mata Uji 2</li>
						<li>Mata Uji 3</li>
					</ul>
				</td>
				<td class="ui detail">
					<ul class="ui list">
						<li>Rahmad Hidayatullah</li>
						<li>Abdul Wahid</li>
						<li>Adriyana</li>
					</ul>
				</td>
				<td>
					PT.Pragma Informatika
				</td>
			</tr>
		</tbody>
	</table>
</div>
@endsection
@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	//Calender
	$(document).ready(function() {
		$('#tab1').DataTable( {
			"autoWidth" : false,
			"paging":   10,
			"ordering": false,
			"lengthChange": false,
			"filter": false,
			"info":     true,
			language: {
                url: "{{ asset('plugins/datatables/Indonesian.json') }}"
            },
		});

		$('#tab2').DataTable( {
			"paging":   10,
			"ordering": false,
			"lengthChange": false,
			"filter": false,
			"info":     true,
			language: {
                url: "{{ asset('plugins/datatables/Indonesian.json') }}"
            },
		});
        var nowDate = new Date();
        var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), 0, 0, 0, 0);
        var tanggal = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);

        $('.timepickerdate').calendar({
        	ampm: false,
        	type: 'date',
        	endCalendar: tanggal,
        	formatter: {
        		date: function (date, settings) {
        			if (!date) return '';
        			let momentDate = moment(date)
        			return momentDate.format('YYYY-MM-DD')
        		}
        	}
        });

        $('.watcher').dropdown({
        });

	    var startCalendar = null;

    	$('.tentative_start').calendar({
    		type: 'date',
    		formatter: {
    			date: function (date, settings) {
    				if (!date) return '';
    				let momentDate = moment(date)
    				return momentDate.format('YYYY-MM-DD')
    			}
    		},
    		onChange: function () {
    			startCalendar = $(this)
    			var element = $(this).parents('tr').find('td div.tentative_end');
    			element.calendar({
    				type: 'date',
    				startCalendar: startCalendar,
    				formatter: {
    					date: function (date, settings) {
    						if (!date) return '';
    						let momentDate = moment(date)
    						return momentDate.format('YYYY-MM-DD')
    					}
    				}
    			})
    			$(this).parents('tr').find('td div.tentative_end').find('div').find('input').focus();
    		}
    	});

    	$('.reschedule_start').calendar({
    		type: 'date',
    		formatter: {
    			date: function (date, settings) {
    				if (!date) return '';
    				let momentDate = moment(date)
    				return momentDate.format('YYYY-MM-DD')
    			}
    		},
    		onChange: function () {
    			startCalendar = $(this)
    			var element = $(this).parents('tr').find('td div.reschedule_end');
    			element.calendar({
    				type: 'date',
    				startCalendar: startCalendar,
    				formatter: {
    					date: function (date, settings) {
    						if (!date) return '';
    						let momentDate = moment(date)
    						return momentDate.format('YYYY-MM-DD')
    					}
    				}
    			})
    			$(this).parents('tr').find('td div.reschedule_end').find('div').find('input').focus();
    		}
    	});

    	$('.realisasi_start').calendar({
    		type: 'date',
    		formatter: {
    			date: function (date, settings) {
    				if (!date) return '';
    				let momentDate = moment(date)
    				return momentDate.format('YYYY-MM-DD')
    			}
    		},
    		onChange: function () {
    			startCalendar = $(this)
    			var element = $(this).parents('tr').find('td div.realisasi_end');
    			element.calendar({
    				type: 'date',
    				startCalendar: startCalendar,
    				formatter: {
    					date: function (date, settings) {
    						if (!date) return '';
    						let momentDate = moment(date)
    						return momentDate.format('YYYY-MM-DD')
    					}
    				}
    			})
    			$(this).parents('tr').find('td div.realisasi_end').find('div').find('input').focus();
    		}
    	});

    	$('.tgl_laporan_start').calendar({
    		type: 'date',
    		formatter: {
    			date: function (date, settings) {
    				if (!date) return '';
    				let momentDate = moment(date)
    				return momentDate.format('YYYY-MM-DD')
    			}
    		},
    		onChange: function () {
    			startCalendar = $(this)
    			var element = $(this).parents('tr').find('td div.tgl_laporan_end');
    			element.calendar({
    				type: 'date',
    				startCalendar: startCalendar,
    				formatter: {
    					date: function (date, settings) {
    						if (!date) return '';
    						let momentDate = moment(date)
    						return momentDate.format('YYYY-MM-DD')
    					}
    				}
    			})
    			$(this).parents('tr').find('td div.tgl_laporan_end').find('div').find('input').focus();
    		}
    	});

	    $(".number").on("keypress keyup blur",function (e) {    
            $(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
        });

        $('.ui.dropdown').css({
        	'width': '200px'
        })

        $('.spesifikasi').css({
        	'width': '200px'
        })

	    var no = 1;
	    $(document).on('click', '.tambah_detail', function(e){
	    	var e = no++;
	    	var f = e+=1;
	    	var html = `
					<tr>
						<td class="center aligned">`+f+`</td>
						<td>
							<div class="ui transparent fluid input field">
								<input type="text" name="detail[`+e+`][spesifikasi]" placeholder="Spesifikasi">
							</div>
						</td>
						<td>
							<div>
								<select name="detail[`+e+`][mata_uji]" class="watcher ui fluid search transparent dropdown multiple" multiple="">
									<option value="">Pilih Mata Uji</option>
									<option value="Mata Uji 1">Mata Uji 1</option>
									<option value="Mata Uji 2">Mata Uji 2</option>
									<option value="Mata Uji 3">Mata Uji 3</option>
								</select>
							</div>
						</td>
						<td>
							<div class="ui transparent fluid input field">
								<input class="number" type="text" name="detail[`+e+`][tarif]" placeholder="Usulan Tarif">
							</div>
						</td>
						<td>
							<div class="ui transparent fluid input field">
								<input class="number" type="text" name="detail[`+e+`][jumlah]" placeholder="Jumlah Benda Uji">
							</div>
						</td>
						<td>
							<div class="ui tentative_start field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[`+e+`][tentative_start]" type="text" placeholder="Tentative Start">
								</div>
							</div>
						</td>
						<td>
							<div class="ui tentative_end field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[`+e+`][tentative_end]" type="text" placeholder="Tentative End">
								</div>
							</div>
						</td>
						<td>
							<div class="ui reschedule_start field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[`+e+`][reschedule_start]" type="text" placeholder="Reschedule Start">
								</div>
							</div>
						</td>
						<td>
							<div class="ui reschedule_end field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[`+e+`][reschedule_end]" type="text" placeholder="Reschedule End">
								</div>
							</div>
						</td>
						<td>
							<div class="ui realisasi_start field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[`+e+`][realisasi_start]" type="text" placeholder="Realisasi Start">
								</div>
							</div>
						</td>
						<td>
							<div class="ui realisasi_end field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[`+e+`][realisasi_end]" type="text" placeholder="Realisasi End">
								</div>
							</div>
						</td>
						<td>
							<div class="ui tgl_laporan_start field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[`+e+`][tgl_laporan_start]" type="text" placeholder="Tanggal Laporan Start">
								</div>
							</div>
						</td>
						<td>
							<div class="ui tgl_laporan_end field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="detail[`+e+`][tgl_laporan_end]" type="text" placeholder="Tanggal Laporan End">
								</div>
							</div>
						</td>
						<td class="lefted">
							<div class="field">
								<div class="ui action choises input">
									<input type="text" readonly>
									<input type="file" name="detail[`+e+`][lampiran]" style="display: none!important;">
									<div class="ui icon button">
										<i class="cloud upload alternate icon"></i>
									</div>
								</div>
							</div>
						</td>
						<td class="center aligned">
							<button class="ui red mini icon remove hapus_data button" data-tooltip="Hapus Data" data-position="left center"><i class="remove icon"></i></button>
						</td>
					</tr>`;
	    	$('.detail.fluid.container').append(html);
	    	$('.timepickertime').calendar({
	    		ampm: false,
	    		type: 'date'
	    	});

	    	$('.watcher').dropdown({
	    	});

	    	var nowDate = new Date();
	    	var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), 0, 0, 0, 0);
	    	var tanggal = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);

	    	$(".number").on("keypress keyup blur",function (e) {    
	    		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	    	});

	    	var startCalendar = null;

	    	$('.tentative_start').calendar({
	    		type: 'date',
	    		formatter: {
	    			date: function (date, settings) {
	    				if (!date) return '';
	    				let momentDate = moment(date)
	    				return momentDate.format('YYYY-MM-DD')
	    			}
	    		},
	    		onChange: function () {
	    			startCalendar = $(this)
	    			var element = $(this).parents('tr').find('td div.tentative_end');
	    			element.calendar({
	    				type: 'date',
	    				startCalendar: startCalendar,
	    				formatter: {
	    					date: function (date, settings) {
	    						if (!date) return '';
	    						let momentDate = moment(date)
	    						return momentDate.format('YYYY-MM-DD')
	    					}
	    				}
	    			})
	    			$(this).parents('tr').find('td div.tentative_end').find('div').find('input').focus();
	    		}
	    	});

	    	$('.reschedule_start').calendar({
	    		type: 'date',
	    		formatter: {
	    			date: function (date, settings) {
	    				if (!date) return '';
	    				let momentDate = moment(date)
	    				return momentDate.format('YYYY-MM-DD')
	    			}
	    		},
	    		onChange: function () {
	    			startCalendar = $(this)
	    			var element = $(this).parents('tr').find('td div.reschedule_end');
	    			element.calendar({
	    				type: 'date',
	    				startCalendar: startCalendar,
	    				formatter: {
	    					date: function (date, settings) {
	    						if (!date) return '';
	    						let momentDate = moment(date)
	    						return momentDate.format('YYYY-MM-DD')
	    					}
	    				}
	    			})
	    			$(this).parents('tr').find('td div.reschedule_end').find('div').find('input').focus();
	    		}
	    	});

	    	$('.realisasi_start').calendar({
	    		type: 'date',
	    		formatter: {
	    			date: function (date, settings) {
	    				if (!date) return '';
	    				let momentDate = moment(date)
	    				return momentDate.format('YYYY-MM-DD')
	    			}
	    		},
	    		onChange: function () {
	    			startCalendar = $(this)
	    			var element = $(this).parents('tr').find('td div.realisasi_end');
	    			element.calendar({
	    				type: 'date',
	    				startCalendar: startCalendar,
	    				formatter: {
	    					date: function (date, settings) {
	    						if (!date) return '';
	    						let momentDate = moment(date)
	    						return momentDate.format('YYYY-MM-DD')
	    					}
	    				}
	    			})
	    			$(this).parents('tr').find('td div.realisasi_end').find('div').find('input').focus();
	    		}
	    	});

	    	$('.tgl_laporan_start').calendar({
	    		type: 'date',
	    		formatter: {
	    			date: function (date, settings) {
	    				if (!date) return '';
	    				let momentDate = moment(date)
	    				return momentDate.format('YYYY-MM-DD')
	    			}
	    		},
	    		onChange: function () {
	    			startCalendar = $(this)
	    			var element = $(this).parents('tr').find('td div.tgl_laporan_end');
	    			element.calendar({
	    				type: 'date',
	    				startCalendar: startCalendar,
	    				formatter: {
	    					date: function (date, settings) {
	    						if (!date) return '';
	    						let momentDate = moment(date)
	    						return momentDate.format('YYYY-MM-DD')
	    					}
	    				}
	    			})
	    			$(this).parents('tr').find('td div.tgl_laporan_end').find('div').find('input').focus();
	    		}
	    	});
	    });

		$(document).on('click', '.hapus_data', function (e){
			var row = $(this).closest('tr');
			row.remove();
		});
	});
</script>
@append