@extends('layouts.form')

@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
	</style>
@append

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			judul: ['empty'],
		};
	</script>
@endsection

@section('content-body')
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record->id }}">
		{!! csrf_field() !!}
		@php
			$angka = (int)$record->konfirmasi->va->surat->kaji_ulang->pp->detail->count();
			$pembanding = (int)$record->detail_penerimaan_barang->count();
			$cek = 0;
		@endphp
		<div class="ui very basic thin segment">
			<table class="ui compact table">
				<tr>
					<td width="150"></td>
					<td width="10"></td>
					<td class="field"></td>
					<td width="350"><h4><b>BUKTI PENERIMAAN BARANG</b></h4></td>
					<td width="10"></td>
					<td width="300">
					</td>
				</tr>
				<tr>
					<td><b>Dari</b></td>
					<td>:</td>
					<td class="field">
						{{-- <input type="hidden" name="perusahaan" value="{{ $record->id }}"> --}}
						{{ $record->konfirmasi->va->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama }}
					</td>
					<td width="150"><b>WBS / IO</b></td>
					<td>:</td>
					<td class="field">
						<div class="ui transparent fluid input">
							{{ $record->konfirmasi->wbs_io }}
						</div>
					</td>
				</tr>
			</table>
			@foreach($record->konfirmasi->va->surat->kaji_ulang->detail as $key => $data)
				<table class="ui celled table" id="penerimaan" style="border-radius: 0; margin: 0;border: 1px solid #ddd;">
					@php
						if($record->pengujian){
							$status = $record->pengujian->status;
						}else{
							$status =0;
						}

					@endphp
					<thead>
						<tr>
							<th width="400" style="text-align: left;font-weight: bold;" @if($status == 1) colspan="6" @else colspan="5" @endif>{{$data->detail_pendaftaran->jenis->nama}}</th>
						</tr>
					</thead>
						<tbody class="detail-penerimaan-{{ $data->detail_pendaftaran->id }}">
						@foreach($record->detail_penerimaan_barang->where('detail_pp_id', $data->detail_pendaftaran->id) as $key => $val)
							@if($val->detail_pengujian)
								<tr>
									@if($val->detail_pengujian->verifikasi == 2)
										@if($val->detail_pengujian->detailreshcedule->tentative_end_new)
											@if($val->flag == 1 && $status == 1)
												<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][data_id]" value="{{ $val->id }}" class="list-row-{{ $val->id }}" disabled="">
												<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][id]" value="{{ $val->detail_pp_id }}" class="list-row-{{ $val->id }}" disabled="">
												<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][urutan]" value="{{ $val->urutan }}" class="list-row-{{ $val->id }}" disabled="">
												<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][group_id]" value="{{ $val->group_idn }}" class="list-row-{{ $val->id }}" disabled="">
												<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][tentative_start]" value="{{$val->tentative_start}}" class="list-row-{{$val->id}}" disabled="">
												<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][tentative_end]" value="{{$val->tentative_end}}" class="list-row-{{$val->id}}" disabled="">
											@else
												<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][data_id]" value="{{ $val->id }}">
												<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][id]" value="{{ $val->detail_pp_id }}">
												<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][urutan]" value="{{ $val->urutan }}">
												<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][group_id]" value="{{ $val->group_idn }}">
												<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][tentative_start]" placeholder="Nama Barang" value="{{$val->detail_pengujian->detailreshcedule->tentative_start_new}}">
												<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][tentative_end]" placeholder="Nama Barang" value="{{$val->detail_pengujian->detailreshcedule->tentative_end_new}}">
											@endif
										@else 
										@endif
									@else 
										@if($val->flag == 1 && $status == 1)
											<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][data_id]" value="{{ $val->id }}" class="list-row-{{ $val->id }}" disabled="">
											<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][id]" value="{{ $val->detail_pp_id }}" class="list-row-{{ $val->id }}" disabled="">
											<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][urutan]" value="{{ $val->urutan }}" class="list-row-{{ $val->id }}" disabled="">
											<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][group_id]" value="{{ $val->group_idn }}" class="list-row-{{ $val->id }}" disabled="">
											<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][tentative_start]" value="{{$val->tentative_start}}" class="list-row-{{$val->id}}" disabled="">
											<input type="hidden" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][tentative_end]" value="{{$val->tentative_end}}" class="list-row-{{$val->id}}" disabled="">
										@else
										@endif
									@endif
									<td style="width: 50px; text-align: center;border-bottom: 1px solid #e8e9e9;border-top: 1px solid #e8e9e9;" rowspan="3">{{ $val->urutan }}</td>
									<td style="width: 100px;border-left: 1px solid #e8e9e9;">Nama Barang</td>
									<td style="width: 5px;">:</td>
									<td style="width: 400px;">
										<div class="field">
										@if($val->detail_pengujian->verifikasi == 2)
											@if($val->detail_pengujian->detailreshcedule->tentative_end_new)
													@if($val->flag == 1 && $status == 1)
														<div class="list-detail-{{ $val->id }}" style="margin-bottom: -20px;">
															{{$val->nama_barang }} [{{ $val->urutan }}/{{number_format($data->jumlah)}}]
														</div>
														<div class="ui right labeled input">
															<input type="text" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][nama]" placeholder="Nama Barang" value="{{$val->nama_barang }}" class="list-hilang-{{ $val->id }} hidden list-count-{{ $val->id }}" disabled="">
															<label class="ui label list-hilang-{{ $val->id }} hidden"><label>{{ $val->urutan }}/{{number_format($data->jumlah)}}</label></label>
														</div>
													@else 
														@php
															$cek++;
														@endphp
														<div style="margin-bottom: -20px;">
															{{$val->nama_barang }} [{{ $val->urutan }}/{{number_format($data->jumlah)}}]
														</div>
														<div class="ui right labeled input">
															<input type="text" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][nama]" placeholder="Nama Barang" value="{{$val->nama_barang }}">
															<label class="ui label"><label>{{ $val->urutan }}/{{number_format($data->jumlah)}}</label></label>
														</div>
													@endif
											@else 
												{{$val->nama_barang }} [{{ $val->urutan }}/{{number_format($data->jumlah)}}]
											@endif
										@else
											@if($val->flag == 1 && $status == 1)
												<div class="list-detail-{{ $val->id }}" style="margin-bottom: -20px;">
													{{$val->nama_barang }} [{{ $val->urutan }}/{{number_format($data->jumlah)}}]
												</div>
												<div class="ui right labeled input">
													<input type="text" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][nama]" placeholder="Nama Barang" value="{{$val->nama_barang }}" class="list-hilang-{{ $val->id }} hidden list-count-{{ $val->id }}" disabled="">
													<label class="ui label list-hilang-{{ $val->id }} hidden"><label>{{ $val->urutan }}/{{number_format($data->jumlah)}}</label></label>
												</div>
											@else 
												{{$val->nama_barang }} [{{ $val->urutan }}/{{number_format($data->jumlah)}}]
											@endif
										@endif
										</div>
									</td>
									<td rowspan="3" @if($val->flag == 1 && $status == 1) @else colspan="2" @endif>
										@if($val->detail_pengujian->verifikasi == 2)
											@if($val->detail_pengujian->detailreshcedule->tentative_end_new)
												@if($val->flag == 1 && $status == 1)
													<textarea name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][catatan]" id="delapan" class="areas delapan list-text-{{ $val->id }} hidden" style="margin-bottom: -1em;display:none;" placeholder="Catatan" rows="3" disabled="">{{$val->catatan}}</textarea>
													<div class="list-detail-{{ $val->id }}">
														{{$val->catatan}}
													</div>
												@else 
													<textarea name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][catatan]" id="delapan" class="areas delapan" style="margin-bottom: -1em;" placeholder="Catatan" rows="3">{{$val->catatan}}</textarea>
												@endif
											@else 
												{{$val->catatan}}
											@endif
										@else
											@if($val->flag == 1 && $status == 1)
												<textarea name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][catatan]" id="delapan" class="areas delapan list-text-{{ $val->id }} hidden" style="margin-bottom: -1em;display:none;" placeholder="Catatan" rows="3" disabled="">{{$val->catatan}}</textarea>
												<div class="list-detail-{{ $val->id }}">
													{{$val->catatan}}
												</div>
											@else 
												<div class="ui right labeled input">
													{{$val->catatan}}
												</div>
											@endif
										@endif
									</td>
									@if($val->flag == 1 && $status == 1)
										<td style="width: 100px;text-align: center;" rowspan="3">
											<button class="ui red mini icon btn-save button hidden" data-id="{{ $val->id }}" data-tooltip="Klik untuk menonaktifkan data penerimaan barang baru" data-position="left center"><i class="close icon"></i></button>
											<button class="ui green mini icon btn-edit button" data-id="{{ $val->id }}" data-tooltip="Klik untuk mengubah data penerimaan barang baru" data-position="left center"><i class="pencil icon"></i></button>
										</td>
									@else 
									@endif
								</tr>
								<tr>
									<td style="border-left: 1px solid #e8e9e9;">Merek</td>
									<td>:</td>
									<td>
										{{ $val->detail_pp->merk }} - {{ $val->detail_pp->tipe }}
									</td>
								</tr>
								<tr>
									<td style="border-left: 1px solid #e8e9e9;border-bottom: 1px solid #e8e9e9;">No Seri</td>
									<td style="border-bottom: 1px solid #e8e9e9;">:</td>
									<td style="border-bottom: 1px solid #e8e9e9;">
										<div class="field">
										@if($val->detail_pengujian->verifikasi == 2)
											@if($val->detail_pengujian->detailreshcedule->tentative_end_new)
												@if($val->flag == 1 && $status == 1)
													<div class="list-detail-{{ $val->id }}" style="margin-bottom: -20px;">
														{{$val->no_seri}}
													</div>
													<div class="ui input">
														<input type="text" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][seri]" placeholder="No. Seri" value="{{$val->no_seri}}" class="list-hilang-{{ $val->id }} hidden" disabled="">
													</div>
												@else 
													<div class="ui input">
														<input type="text" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][seri]" placeholder="No. Seri" value="{{$val->no_seri}}">
													</div>
												@endif
											@else 
												{{$val->no_seri}}/{{number_format($data->jumlah)}}
											@endif
										@else
											@if($val->flag == 1 && $status == 1)
												<div class="list-detail-{{ $val->id }}" style="margin-bottom: -20px;">
													{{$val->no_seri}}
												</div>
												<div class="ui input">
													<input type="text" name="data[{{ $val->detail_pp_id }}][detail][{{ $val->urutan }}][seri]" placeholder="No. Seri" value="{{$val->no_seri}}" class="list-hilang-{{ $val->id }} hidden" disabled="">
												</div>
											@else 
												<div class="ui right labeled input">
													{{$val->no_seri}}/{{number_format($data->jumlah)}}
												</div>
											@endif
										@endif
										</div>
									</td>
								</tr>
							@else
								@if($val->flag == 1)
									<tr colspan="3">
										<td>
											Barang Belum Datang
										</td>
									</tr>
								@else
									<tr colspan="3">
										<td>
											Data Belum Diproses Verifikasi
										</td>
									</tr>
								@endif
							@endif
						@endforeach
						</tbody>
				</table>
			@endforeach
			<br>
			<table class="ui compact celled table" style="border-radius: 0; margin: 0">
				<tr class="middle aligned">
					<td width="30%" class="center aligned">
					</td>
					<td width="40%" class="center aligned" style="border-left: none;">
					</td>
					<td width="5%" class="center aligned" style="border-left: none; border-right: none;">
						Jakarta, 
					</td>
					<td width="35%" class="left aligned" style="border-left: none;">
					@if($cek > 0)
						<div class="field list-button-1">
							<div class="ui calendar labeled input transparent tgl">
								<div class="ui input left icon">
									<i class="calendar icon date"></i>
									<input name="tanggal_terima" type="text" placeholder="Tgl Penerimaan" value="{{$record->tanggal_terima}}">
								</div>
							</div>
						</div>
					@endif
						<div class="field list-button-2 hidden">
							<div class="ui calendar labeled input transparent tgl">
								<div class="ui input left icon">
									<i class="calendar icon date"></i>
									<input name="tanggal_terima" type="text" placeholder="Tgl Penerimaan" value="{{$record->tanggal_terima}}">
								</div>
							</div>
						</div>
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned" style="border-top: none;">
						Pengirim,
					</td>
					<td width="40%" class="center aligned" style="border-left: none;border-top: none;">
					</td>
					<td colspan="2" width="40%" class="center aligned" style="border-left: none;border-top: none;">
						Penerima,<br>
						(Administrasi)
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned" style="border-top: none;">
					</td>
					<td width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
					<td colspan="2" width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned" style="border-top: none;">
					</td>
					<td width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
					<td colspan="2" width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned" style="border-top: none;">
					</td>
					<td width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
					<td colspan="2" width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned" style="border-top: none;">
					</td>
					<td width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
					<td colspan="2" width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned" style="border-top: none;">
					</td>
					<td width="40%" class="center aligned" style="border-left: none; border-top: none;">
						<div class="description" width="50%">
							<p style="text-align:justify;text-justify:auto;">Barang (sample) uji atau alat yang dikalibrasi tersebut dapat diambil paling lambat 5 (lima) hari sejak pengujian selesai. Kerusakan atau kehilangan diluar waktu tersebut bukan tanggung jawab kami.</p>
						</div>
					</td>
					<td colspan="2" width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned" style="border-top: none;">
					</td>
					<td width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
					<td colspan="2" width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned" style="border-top: none;">
					</td>
					<td width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
					<td colspan="2" width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned field" style="border-top: none;">
						<input type="hidden" name="pengirim" value="{{ $record->pengirim }}">
						<input type="hidden" name="penerima_id" value="{{ $record->penerima_id }}">
						{{-- <input type="hidden" name="tanggal_terima" value="{{ $record->tanggal_terima }}"> --}}
						{{ $record->pengirim }}
					</td>
					<td width="40%" class="center aligned" style="border-left: none;border-top: none;">
					</td>
					<td colspan="2" width="40%" class="center aligned field" style="border-left: none;border-top: none;">
						{{ auth()->user()->nama }}
					</td>
				</tr>
				<input type="hidden" name="cek" value="{{ $cek }}">
			</table>
		</div>
		<div class="ui bottom attached segment">
			<div class="actions">
				<div class="ui two column grid">
					<div class="left aligned column">
						<div class="ui gray deny labeled icon button" onclick="window.history.back()">
							<i class="chevron left icon"></i>
							Kembali
						</div>
					</div>
					@if($cek > 0)
						<div class="right aligned column list-button-1">
							<button type="button" class="ui positive right labeled icon save as page button">
								Simpan
								<i class="save icon"></i>
							</button>
						</div>
					@endif
					<div class="right aligned column list-button-2 hidden">
						<button type="button" class="ui positive right labeled icon save as page button">
							Simpan
							<i class="save icon"></i>
						</button>
					</div>
				</div>	
			</div>
		</div>
	</form>
@endsection
@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	//Calender
	$(document).ready(function() {
        var nowDate = new Date();
        var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), 0, 0, 0, 0);
        var tanggal = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);
        $('.tgl').calendar({
			ampm: false,
			type: 'date',
			// maxDate: tanggal,
			formatter: {
			date: function (date, settings) {
				if (!date) return '';
				let momentDate = moment(date)
				return momentDate.format('YYYY-MM-DD')
				}
			}
		});
        
	    $('.timepicker').calendar({
        	ampm: false,
        	type: 'date',
        	endCalendar: tanggal,
        	formatter: {
        		date: function (date, settings) {
        			if (!date) return '';
        			let momentDate = moment(date)
        			return momentDate.format('DD/MM/YYYY')
        		}
        	}
        });

	    $('.timepickertime').calendar({
	    	ampm: false,
	    	type: 'time'
	    });

	    $('.data_jumlah').keyup(function(e){
			var id = $(this).data("id");
			var max = $(this).data("max");
			if(parseInt($(this).val()) > parseInt(max)){
				$(this).val(max)
			}else if(parseInt($(this).val()) <= 0){
				$(this).val(1)
			}else{
			}
		});
	    $('.merek').dropdown({
	    	onChange: function(value, text, $selectedItem) {
	    		var id = $(this).find(':selected').attr('data-id');
	    		$.ajax({
	    			url: '{{ url('ajax/option/pp_detail') }}',
	    			type: 'POST',
	    			data: {
	    				_token: "{{ csrf_token() }}",
	    				id: value
	    			},
	    		})
	    		.done(function(response) {
	    			console.log('waduk')
	    			$("td").find('label.jumlah-'+id).html("");
	    			$("td").find('label.jumlah-'+id).html(response.jumlah);
	    			$("td").find('label.satuan-'+id).html("");
	    			$("td").find('label.satuan-'+id).html(response.satuan.satuan);
	    			$('input[name="detail['+id+'][nama]"]').val(response.spesifikasi);
	    			$('input[name="detail['+id+'][jumlah_barang]"]').attr("disabled", false);
	    			$('input[name="detail['+id+'][jumlah_barang]"]').attr("data-max", response.jumlah);
	    		})
	    		.fail(function() {
	    			console.log("error");
	    		});
	    	}
	    });

	    $(".number").on("keypress keyup blur",function (e) {    
            $(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
        });

        var num = 1;
		$(document).on('keydown', 'textarea.inline', function(e){
			var el = this;
			setTimeout(function(){
				el.style.cssText = 'height:20px; padding:0';
				// for box-sizing other than "content-box" use:
				// el.style.cssText = '-moz-box-sizing:content-box';
				el.style.cssText = 'height:' + el.scrollHeight + 'px';
			},0);
		});

		$(document).on('click', '.append.button', function(e){
			var max_row = $(this).data("mak");
			var max = $(this).data("max");
			var id = $(this).data("id");
			var rowCount = $('#penerimaan > tbody.detail-penerimaan-'+id+' > tr').length;
			var rowAngka = (rowCount/3);
			var c = rowAngka;

			var merk = $('input[name="data['+id+'][detail][1][merk]"]').val();
			var spefisikasi = $('input[name="data['+id+'][detail][1][nama]"]').val();
			var urutan = $('input[name="data['+id+'][detail][1][urutan]"]').val();
			if(c < max){
				var html = `
					<tr class="cari-`+(id)+` isi-`+(id)+`-`+(c+1)+` data-`+(id)+`" data-id="`+(c+1)+`">
						<input type="hidden" name="data[`+(id)+`][detail][`+(c+1)+`][id]" value="`+(id)+`">
						<input type="hidden" name="data[`+(id)+`][detail][`+(c+1)+`][urutan]" value="`+(c+1)+`">
						<input type="hidden" name="data[`+(id)+`][detail][`+(c+1)+`][max]" value="`+max+`">
						<td style="width: 50px; text-align: center;" rowspan="3" class="numboor-`+(id)+`-`+(c+1)+`">`+(c+1)+`</td>
						<td style="width: 100px;border-left: 1px solid #e8e9e9;">Nama Barang</td>
						<td style="width: 5px;">:</td>
						<td style="width: 400px;"><input type="text" name="data[`+(id)+`][detail][`+(c+1)+`][nama]" placeholder="Nama Barang" value="`+(spefisikasi)+`"></td>
						<td rowspan="3"><textarea name="data[`+(id)+`][detail][`+(c+1)+`][catatan]" id="delapan" class="areas delapan" style="margin-bottom: -1em;" placeholder="Catatan" rows="3"></textarea></td>
						<td rowspan="3" width="50" style="text-align: center;"><button type="button" class="ui red mini icon remove button" data-tooltip="Hapus Barang" data-position="top center" data-id="`+(id)+`" data-urutan="`+(c+1)+`" data-mak="`+max_row+`"><i class="close icon"></i></button></td>
					</tr>
					<tr class="cari-`+(id)+` isi-`+(id)+`-`+(c+1)+`">
						<td style="border-left: 1px solid #e8e9e9;">Merek</td>
						<td>:</td>
						<td>
							`+(merk)+`
						</td>
					</tr>
					<tr class="cari-`+(id)+` isi-`+(id)+`-`+(c+1)+`">
						<td style="border-left: 1px solid #e8e9e9;">No Seri</td>
						<td>:</td>
						<td>
							<div class="ui right labeled input">
								<input type="text" style="width:100%;" name="data[`+(id)+`][detail][`+(c+1)+`][seri]" placeholder="No. Seri"><label class="ui label"><label class="urutan-`+(id)+`-`+(c+1)+`">`+(c+1)+`</label>/`+(max)+`</label>
							</div>
						</td>
					</tr>
				`;


				$('.detail-penerimaan-'+id).append(html);
				$('.dropdown').dropdown();
				$('.timepickertime').calendar({
					ampm: false,
					type: 'time'
				});

				$('.merek').dropdown({
					onChange: function(value, text, $selectedItem) {
						var id = $(this).find(':selected').attr('data-id');
						var nilai = (c+1);
						$.ajax({
							url: '{{ url('ajax/option/pp_detail') }}',
							type: 'POST',
							data: {
								_token: "{{ csrf_token() }}",
								id: value
							},
						})
						.done(function(response) {
							console.log(response)
							$("td").find('label.jumlah-'+id).html("");
							$("td").find('label.jumlah-'+id).html(response.jumlah);
							$("td").find('label.satuan-'+id).html("");
							$("td").find('label.satuan-'+id).html(response.satuan.satuan);
							$('input[name="detail['+id+'][nama]"]').val(response.spesifikasi);
							$('input[name="detail['+id+'][jumlah_barang]"]').attr("disabled", false);
							$('input[name="detail['+id+'][jumlah_barang]"]').attr("data-max", response.jumlah);
						})
						.fail(function() {
							console.log("error");
						});
					}
				});

				$('.data_jumlah').keyup(function(e){
					var id = $(this).data("id");
					var max = $(this).data("max");
					if(parseInt($(this).val()) > parseInt(max)){
						$(this).val(max)
					}else if(parseInt($(this).val()) <= 0){
						$(this).val(1)
					}else{
					}
				});
				$(".number").on("keypress keyup blur",function (e) {    
					$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
				});
			}
		});

		$(document).on('click', '.remove.button', function (e){
			var id = $(this).data("id");
			var urutan = $(this).data("urutan");
			var mak = $(this).data("mak") + 1;
			$('.isi-'+id+'-'+urutan).remove();

			$('.detail-penerimaan-'+id).each(function(){
				var no = $('.cari-'+id).length;
				var rowCount = (no/3);

				$.each($('tr.data-'+id), function(key, value){
					var number = $(this).data('id');
					$('input[name="data['+(id)+'][detail]['+number+'][id]"]').val(id);
					$('input[name="data['+(id)+'][detail]['+number+'][urutan]"]').val(key+mak);
					$('.numboor-'+id+'-'+number).html(key+mak);
					$('.urutan-'+id+'-'+number).html(key+mak);
				});
			});
		});

		$(document).on('click', '.btn-edit', function (e){
			$('.tgl').calendar({
				ampm: false,
				type: 'date',
				// maxDate: tanggal,
				formatter: {
				date: function (date, settings) {
					if (!date) return '';
					let momentDate = moment(date)
					return momentDate.format('YYYY-MM-DD')
					}
				}
			});
			var id = $(this).data('id');
			$('.list-row-'+id).removeAttr("disabled");
			$('.list-hilang-'+id).removeAttr("disabled");
			$('.list-hilang-'+id).removeClass('hidden');
			$('.list-detail-'+id).addClass('hidden');
			$('.list-count-'+id).addClass('coba');

			$('.list-text-'+id).show();
			$('.list-text-'+id).removeAttr("disabled");
			$('.list-text-'+id).removeClass('hidden');
			var cek = $("input[name=cek]").val();
			if(cek > 0){
			}else{
				$('.list-button-2').removeClass('hidden');
			}

			$('.btn-edit').each(function() {
			  if($( this ).data('id') == id){
				$(this).addClass('hidden');
			  }
			});

			$('.btn-save').each(function() {
			  if($( this ).data('id') == id){
				$(this).removeClass('hidden');
			  }
			});

			$('.save.as.page2').addClass('disabled');
		});

		$(document).on('click', '.btn-save', function (e){
			$('.tgl').calendar({
				ampm: false,
				type: 'date',
				// maxDate: tanggal,
				formatter: {
				date: function (date, settings) {
					if (!date) return '';
					let momentDate = moment(date)
					return momentDate.format('YYYY-MM-DD')
					}
				}
			});

			var id = $(this).data('id');
			$('.list-row-'+id).attr('disabled', true);

			$('.list-hilang-'+id).attr('disabled', true);
			$('.list-hilang-'+id).addClass('hidden');
			$('.list-detail-'+id).removeClass('hidden');
			$('.list-count-'+id).removeClass('coba');

			$('.list-text-'+id).hide();
			$('.list-text-'+id).attr('disabled', true);
			$('.list-text-'+id).addClass('hidden');

			var numItems = $('.coba').length;
			var cek = $("input[name=cek]").val();
			if(numItems == 0){
				if(cek == 0){
					$('.list-button-1').addClass('hidden');
				}
			}

			$('.btn-edit').each(function() {
			  if($( this ).data('id') == id){
				$(this).removeClass('hidden');
			  }
			});

			$('.btn-save').each(function() {
			  if($( this ).data('id') == id){
				$(this).addClass('hidden');
			  }
			});
			$('.save.as.page2').removeClass('disabled');
		});
    });
</script>
@append