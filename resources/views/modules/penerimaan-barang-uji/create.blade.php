@extends('layouts.form')

@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
	</style>
@append

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			judul: ['empty'],
		};
	</script>
@endsection

@section('content-body')
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="konfirmasi_id" value="{{ $record->id }}">
		<div class="ui very basic thin segment">
			<table class="ui compact table">
				<tr>
					<td width="150"></td>
					<td width="10"></td>
					<td class="field"></td>
					<td width="350"><h4><b>BUKTI PENERIMAAN BARANG</b></h4></td>
					<td width="10"></td>
					<td width="300">
					</td>
				</tr>
				<tr>
					<td><b>Dari</b></td>
					<td>:</td>
					<td class="field">
						<input type="hidden" name="perusahaan" value="{{ $record->id }}">
						{{ $record->va->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama }}
					</td>
					<td width="150"><b>WBS / IO</b></td>
					<td style="margin-left: -30vh;">:</td>
					<td class="field">
						<div class="ui transparent fluid input">
							{{ $record->wbs_io }}
						</div>
					</td>
				</tr>
			</table>
			@foreach($record->va->surat->kaji_ulang->detail as $key => $data)
				<table class="ui celled table" id="penerimaan" style="border-radius: 0; margin: 0;border: 1px solid #ddd;">
					<input type="hidden" name="data[{{ $data->detail_pendaftaran->id }}][id]" value="{{ $data->detail_pendaftaran->id }}">
					<input type="hidden" name="data[{{ $data->detail_pendaftaran->id }}][urutan]" value="1">
					<input type="hidden" name="data[{{ $data->detail_pendaftaran->id }}][merk]" value="{{ $data->detail_pendaftaran->merk.' - '.$data->detail_pendaftaran->tipe }}">
					<input type="hidden" name="data[{{ $data->detail_pendaftaran->id }}][nama]" placeholder="Nama Barang" value="{{$data->spesifikasi}}">
					<input type="hidden" name="data[{{ $data->detail_pendaftaran->id }}][tentative_start]" placeholder="Nama Barang" value="{{$data->tentative_start}}">
					<input type="hidden" name="data[{{ $data->detail_pendaftaran->id }}][tentative_end]" placeholder="Nama Barang" value="{{$data->tentative_end}}">
					<input type="hidden" name="data[{{ $data->detail_pendaftaran->id }}][group_id]" placeholder="Nama Barang" value="{{$data->id}}">
					<thead>
						<tr>
							<th width="400" style="text-align: left;font-weight: bold;" colspan="5">{{$data->detail_pendaftaran->jenis->nama}}</th>
							<th width="50" style="text-align: center;"><button type="button" class="ui green mini icon append button" data-max="{{rtrim(rtrim($data->jumlah,'0'),'.')}}" data-id="{{ $data->detail_pendaftaran->id }}" data-tooltip="Tambah Barang" data-position="top center"><i class="plus icon"></i></button></th>
						</tr>
					</thead>
					<tbody class="detail-penerimaan-{{ $data->detail_pendaftaran->id }}">
					</tbody>
				</table>
			@endforeach
			<br>
			<table class="ui compact celled table" style="border-radius: 0; margin: 0">
				<tr class="middle aligned">
					<td width="30%" class="center aligned">
					</td>
					<td width="40%" class="center aligned" style="border-left: none;">
					</td>
					<td width="5%" class="center aligned" style="border-left: none; border-right: none;">
						Jakarta, 
					</td>
					<td width="35%" class="left aligned" style="border-left: none;">
						<div class="field">
							<div class="ui calendar labeled input transparent tgl">
								<div class="ui input left icon">
									<i class="calendar icon date"></i>
									<input name="tanggal_terima" type="text" placeholder="Tgl Penerimaan">
								</div>
							</div>
						</div>
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned" style="border-top: none;">
						Pengirim,
					</td>
					<td width="40%" class="center aligned" style="border-left: none;border-top: none;">
					</td>
					<td colspan="2" width="40%" class="center aligned" style="border-left: none;border-top: none;">
						Penerima,<br>
						(Administrasi)
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned" style="border-top: none;">
					</td>
					<td width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
					<td colspan="2" width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned" style="border-top: none;">
					</td>
					<td width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
					<td colspan="2" width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned" style="border-top: none;">
					</td>
					<td width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
					<td colspan="2" width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned" style="border-top: none;">
					</td>
					<td width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
					<td colspan="2" width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned" style="border-top: none;">
					</td>
					<td width="40%" class="center aligned" style="border-left: none; border-top: none;">
						<div class="description" width="50%">
							<p style="text-align:justify;text-justify:auto;">Barang (sample) uji atau alat yang dikalibrasi tersebut dapat diambil paling lambat 5 (lima) hari sejak pengujian selesai. Kerusakan atau kehilangan diluar waktu tersebut bukan tanggung jawab kami.</p>
						</div>
					</td>
					<td colspan="2" width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned" style="border-top: none;">
					</td>
					<td width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
					<td colspan="2" width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned" style="border-top: none;">
					</td>
					<td width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
					<td colspan="2" width="40%" class="center aligned" style="border-left: none; border-top: none;">
					</td>
				</tr>
				<tr class="middle aligned">
					<td width="30%" class="center aligned field" style="border-top: none;">
						<div class="ui transparent input">
							<input type="text" name="pengirim" placeholder="Pengirim">
						</div>
					</td>
					<td width="40%" class="center aligned" style="border-left: none;border-top: none;">
					</td>
					<td colspan="2" width="40%" class="center aligned field" style="border-left: none;border-top: none;">
						<select name="penerima_id" class="watcher ui fluid search transparent dropdown">
							{!! \App\Models\Authentication\User::options('nama', 'id', ['filters' => [function($q){
								return $q->where('id', auth()->user()->id);
							}], 'selected'], 'Pilih Salah Satu') !!}
						</select>
					</td>
				</tr>
			</table>
		</div>
		<div class="ui bottom attached segment">
			<div class="actions">
				<div class="ui two column grid">
					<div class="left aligned column">
						<div class="ui gray deny labeled icon button" onclick="window.history.back()">
							<i class="chevron left icon"></i>
							Kembali
						</div>
					</div>
					<div class="right aligned column">		
						<button type="button" class="ui positive right labeled icon save as page button">
							Simpan
							<i class="save icon"></i>
						</button>
					</div>
				</div>	
			</div>
		</div>
	</form>
@endsection
@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	//Calender
	$(document).ready(function() {
        var nowDate = new Date();
        var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), 0, 0, 0, 0);
        var tanggal = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);

        $('.tgl').calendar({
        	ampm: false,
        	type: 'date',
			// maxDate: tanggal,
			formatter: {
				date: function (date, settings) {
					if (!date) return '';
					let momentDate = moment(date)
					return momentDate.format('YYYY-MM-DD')
				}
			}
		});

	    $('.timepicker').calendar({
        	ampm: false,
        	type: 'date',
        	endCalendar: tanggal,
        	formatter: {
        		date: function (date, settings) {
        			if (!date) return '';
        			let momentDate = moment(date)
        			return momentDate.format('DD/MM/YYYY')
        		}
        	}
        });

	    $('.timepickertime').calendar({
	    	ampm: false,
	    	type: 'time'
	    });
		$('.data_jumlah').keyup(function(e){
			var id = $(this).data("id");
			var max = $(this).data("max");
			if(parseInt($(this).val()) > parseInt(max)){
				$(this).val(max)
			}else if(parseInt($(this).val()) <= 0){
				$(this).val(1)
			}else{
			}
		});

	    $('.merek').dropdown({
	    	onChange: function(value, text, $selectedItem) {
	    		var id = $(this).find(':selected').attr('data-id');
	    		$.ajax({
	    			url: '{{ url('ajax/option/pp_detail') }}',
	    			type: 'POST',
	    			data: {
	    				_token: "{{ csrf_token() }}",
	    				id: value
	    			},
	    		})
	    		.done(function(response) {
	    			console.log('waduk')
	    			$("td").find('label.jumlah-'+id).html("");
	    			$("td").find('label.jumlah-'+id).html(response.jumlah);
	    			$("td").find('label.satuan-'+id).html("");
	    			$("td").find('label.satuan-'+id).html(response.satuan.satuan);
	    			$('input[name="detail['+id+'][nama]"]').val(response.spesifikasi);
	    			$('input[name="detail['+id+'][jumlah_barang]"]').attr("disabled", false);
	    			$('input[name="detail['+id+'][jumlah_barang]"]').attr("data-max", response.jumlah);
	    		})
	    		.fail(function() {
	    			console.log("error");
	    		});
	    	}
	    });

	    $(".number").on("keypress keyup blur",function (e) {    
            $(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
        });

        var num = 1;
		$(document).on('keydown', 'textarea.inline', function(e){
			var el = this;
			setTimeout(function(){
				el.style.cssText = 'height:20px; padding:0';
				// for box-sizing other than "content-box" use:
				// el.style.cssText = '-moz-box-sizing:content-box';
				el.style.cssText = 'height:' + el.scrollHeight + 'px';
			},0);
		});

		$(document).on('click', '.append.button', function(e){
			var max = $(this).data("max");
			var id = $(this).data("id");
			var rowCount = $('#penerimaan > tbody.detail-penerimaan-'+id+' > tr').length;
			var rowAngka = (rowCount/3);
			var c = rowAngka;

			var merk = $('input[name="data['+id+'][merk]"]').val();
			var spefisikasi = $('input[name="data['+id+'][nama]"]').val();
			var urutan = $('input[name="data['+id+'][urutan]"]').val();
			if(c < max){
				var html = `
					<tr class="cari-`+(id)+` isi-`+(id)+`-`+(c+1)+` data-`+(id)+`" data-id="`+(c+1)+`">
						<input type="hidden" name="data[`+(id)+`][detail][`+(c+1)+`][id]" value="`+(id)+`">
						<input type="hidden" name="data[`+(id)+`][detail][`+(c+1)+`][urutan]" value="`+(c+1)+`">
						<input type="hidden" name="data[`+(id)+`][detail][`+(c+1)+`][max]" value="`+max+`">
						<td style="width: 50px; text-align: center;" rowspan="3" class="numboor-`+(id)+`-`+(c+1)+`">`+(c+1)+`</td>
						<td style="width: 100px;border-left: 1px solid #e8e9e9;">Nama Barang</td>
						<td style="width: 5px;">:</td>
						<td style="width: 400px;" class="field">
							<div class="ui right labeled input">
								<input type="text" name="data[`+(id)+`][detail][`+(c+1)+`][nama]" placeholder="Nama Barang" value="`+(spefisikasi)+`">
								<label class="ui label"><label class="urutan-`+(id)+`-`+(c+1)+`">`+(c+1)+`</label>/`+(max)+`</label>
							</div>
						</td>
						<td rowspan="3" class="field"><textarea name="data[`+(id)+`][detail][`+(c+1)+`][catatan]" id="delapan" class="areas delapan" style="margin-bottom: -1em;" placeholder="Catatan" rows="3"></textarea></td>
						<td rowspan="3" width="50" style="text-align: center;"><button type="button" data-id="`+(id)+`" data-urutan="`+(c+1)+`" class="ui red mini icon remove button" data-tooltip="Hapus Barang" data-position="top center"><i class="close icon"></i></button></td>
					</tr>
					<tr class="cari-`+(id)+` isi-`+(id)+`-`+(c+1)+`">
						<td style="border-left: 1px solid #e8e9e9;">Merek</td>
						<td>:</td>
						<td>
							`+merk+`
						</td>
					</tr>
					<tr class="cari-`+(id)+` isi-`+(id)+`-`+(c+1)+`">
						<td style="border-left: 1px solid #e8e9e9;">No Seri</td>
						<td>:</td>
						<td class="field">
							<div class="ui input">
								<input type="text" style="width:80%;" name="data[`+(id)+`][detail][`+(c+1)+`][seri]" placeholder="No. Seri">
							</div>
						</td>
					</tr>
				`;


				$('.detail-penerimaan-'+id).append(html);
				$('.dropdown').dropdown();
				$('.timepickertime').calendar({
					ampm: false,
					type: 'time'
				});

				$('.merek').dropdown({
					onChange: function(value, text, $selectedItem) {
						var id = $(this).find(':selected').attr('data-id');
						var nilai = (c+1);
						$.ajax({
							url: '{{ url('ajax/option/pp_detail') }}',
							type: 'POST',
							data: {
								_token: "{{ csrf_token() }}",
								id: value
							},
						})
						.done(function(response) {
							console.log(response)
							$("td").find('label.jumlah-'+id).html("");
							$("td").find('label.jumlah-'+id).html(response.jumlah);
							$("td").find('label.satuan-'+id).html("");
							$("td").find('label.satuan-'+id).html(response.satuan.satuan);
							$('input[name="detail['+id+'][nama]"]').val(response.spesifikasi);
							$('input[name="detail['+id+'][jumlah_barang]"]').attr("disabled", false);
							$('input[name="detail['+id+'][jumlah_barang]"]').attr("data-max", response.jumlah);
						})
						.fail(function() {
							console.log("error");
						});
					}
				});

				$('.data_jumlah').keyup(function(e){
					var id = $(this).data("id");
					var max = $(this).data("max");
					if(parseInt($(this).val()) > parseInt(max)){
						$(this).val(max)
					}else if(parseInt($(this).val()) <= 0){
						$(this).val(1)
					}else{
					}
				});
				$(".number").on("keypress keyup blur",function (e) {    
					$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
				});
			}
		});

		$(document).on('click', '.remove.button', function (e){
			var id = $(this).data("id");
			var urutan = $(this).data("urutan");
			$('.isi-'+id+'-'+urutan).remove();

			$('.detail-penerimaan-'+id).each(function(){
				var no = $('.cari-'+id).length;
				var rowCount = (no/3);

				$.each($('tr.data-'+id), function(key, value){
					var number = $(this).data('id');
					$('input[name="data['+(id)+'][detail]['+number+'][id]"]').val(id);
					$('input[name="data['+(id)+'][detail]['+number+'][urutan]"]').val(key+1);
					$('.numboor-'+id+'-'+number).html(key+1);
					$('.urutan-'+id+'-'+number).html(key+1);
				});
			});
		});
    });
</script>
@append
@include('scripts.inputmask')