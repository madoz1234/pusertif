@extends('layouts.list-penerimaan-barang')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
		.ui.disposisi.dropdown .menu .item:nth-child(1) {
			background-color: #ffc000;
			color: #000;
		}
		.ui.disposisi.dropdown .menu .item:nth-child(2) {
			background-color: #00b0f0;
			color: #000;
		}
	</style>
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filterdata')
	d.no_order = $("input[name='filter[no_order]']").val();
    d.tanggal_order = $("input[name='filter[tanggal_order]']").val();
    d.no_surat = $("input[name='filter[no_surat]']").val();
    d.jenis_pelayanan_id = $("select[name='filter[jenis_pelayanan_id]']").val();
	d.no_wbs = $("input[name='filter[no_wbs]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			file: ['empty'],
		};
	</script>
@endsection
@section('filters')
    <div class="field">
		<input name="filter[no_order]" placeholder="No Order" type="text">
	</div>
    <div class="field">
        <div class="ui left icon date input">
            <i class="calendar icon"></i>
            <input type="text" name="filter[tanggal_order]" placeholder="Tanggal Order">
        </div>
    </div>
    <div class="field">
      <input type="text" name="filter[no_surat]" placeholder="No Surat Permintaan">
    </div>
    <div class="field">
      <select name="filter[jenis_pelayanan_id]" class="watcher ui fluid search dropdown">
        {!! \App\Models\Master\JenisPelayanan::options('nama', 'id', [], 'Pilih Layanan') !!}
      </select>
    </div>
    <div class="field">
        <input type="text" name="filter[no_wbs]" placeholder="No WBS/IO">
    </div>
    <button type="button" class="ui teal icon filter button" data-tooltip="Cari Data">
      <i class="search icon"></i>
    </button>
    <button type="reset" class="ui icon reset button" data-tooltip="Bersihkan Pencarian">
      <i class="refresh icon"></i>
    </button>
@endsection

@section('tables')
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="ui top demo tabular menu">
	<div class="active item tab-enable" data-tab="first">Baru @if($records > 0)<span style="background-color:red;"class="ui circular label">{{ $records }}</span>@endif</div>
	<div class="item tab-disable" data-tab="second">On Progress</div>
	<div class="item tab-disable" data-tab="third">Historis</div>
</div>

<div class="ui bottom demo active tab" data-tab="first">
	@if(isset($structs['listStruct']))
		<table id="listTable" class="ui celled compact red table display" width="100%" cellspacing="0">
			<thead>
				<tr>
					@foreach ($structs['listStruct'] as $struct)
					<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
					@endforeach
				</tr>
			</thead>
			<tbody>
				@yield('tableBody')
			</tbody>
		</table>
	@endif
</div>
<div class="ui bottom demo tab" data-tab="second">
	@if(isset($structs['listStruct2']))
	<table id="listTable2" class="ui celled compact red table display" width="100%" cellspacing="0">
		<thead>
			<tr>
				@foreach ($structs['listStruct2'] as $struct)
				<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@yield('tableBody')
		</tbody>
	</table>
	@endif
</div>
<div class="ui bottom demo tab" data-tab="third">
	@if(isset($structs['listStruct3']))
	<table id="listTable3" class="ui celled compact red table display" width="100%" cellspacing="0">
		<thead>
			<tr>
				@foreach ($structs['listStruct3'] as $struct)
				<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@yield('tableBody')
		</tbody>
	</table>
	@endif
</div>
@endsection
@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	$('.date').calendar({
        type: 'date',
        formatter: {
          date: function (date, settings) {
            if (!date) return '';
            let momentDate = moment(date)
            return momentDate.format('DD/MM/YYYY')
          }
        }
    })
	$(document).ready(function() {
		$('.ui.watcher.dropdown').css({
			'width': '250px'
		});

		$(document).on('click', '.eye.pengujian', function (e){
			var id = $(this).data('id');
			var url = "{{ url($pageUrl) }}/"+id+"/detail";
			loadModal({
				'url' : url,
				'modal' : 'small modal',
				'formId' : '#dataForm',
				'onShow' : function(){ 
					onShow();
				},
			})
		});

		$(document).on('click', '.tab-enable', function (e){
			$('.download-rekap').removeClass('hidden');
		});

		$(document).on('click', '.tab-disable', function (e){
			$('.download-rekap').addClass('hidden');
		});
	});
</script>
@append

@section('init-modal')
	<script>
		onShow = function(){
        	$('select[name=kaji_ulang]').on('change', function(){
				$.ajax({
					url: '{{ url('ajax/option/kaji_ulang') }}',
					type: 'POST',
					data: {
						_token: "{{ csrf_token() }}",
						kaji_ulang: this.value
					},
				})
				.done(function(response) {
					if(response){
						$("[name=wbs_io]").val(response);
						$('[name=wbs_io]').attr('readonly', true);
					}else{
						$("[name=wbs_io]").val('');
						$('[name=wbs_io]').attr('readonly', false);
					}
				})
				.fail(function() {
					console.log("error");
				});
			})	
        };
	</script>
@endsection
