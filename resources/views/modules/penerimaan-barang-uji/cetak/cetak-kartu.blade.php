<html>
<head>
<style>
        @page {
            /*margin: 60px 25px;*/
            counter-reset: page;
        }

       /* Define now the real margins of every page in the PDF */
       body {
           /*margin-top: 3cm;*/
           margin-left: 0cm;
           margin-right: 0cm;
           /*margin-bottom: 2cm;*/
           font-style: normal;
       }

       .container {
       	width: 100%;
       }

       td {
		  padding-left: 3px;
		}

       .ui.table.bordered {
         border: solid black 1px;
         border-collapse: collapse;
         width: 100%;
         font-family : "Arial", "Verdana", sans-serif;
       }

       .ui.table.bordered td {
         border: solid black .5px;
         border-collapse: collapse;
         /*padding:10px;*/
         padding:5px;
       }

       .ui.table.bordered td.center.aligned {
         text-align : center;
       }

       .page-break {
            page-break-after: auto;
       }

       .pad{
       	padding-top: 5px;
       	padding-bottom: 5px;
       }

       .pad2{
       	padding-left: 5px;
       }

       i.icon.check.circle:before {
       	content: "\f058";
       }

       i.icon.remove.circle.outline:before {
       	content: "\f05c";
       }

       i.icon.check.circle.outline:before {
       	content: "\f05d";
       }

       /* Define the header rules */
        header {
            position: fixed;
            top: -40px;
            left: 0px;
            right: 0px;
            height: 10px;

            /* Extra personal styles */
            text-align: center;
            /*line-height: 35px;*/

            font-size: 10px;
        }

        .column {
        	width: 50%;
        	padding: 10px;
        	height: 300px;
        }

        .row:after {
        	content: "";
        	width: 50%;
        	display: table;
        	clear: both;
        }

        .page-break {
            page-break-after: always;
       }
		
       main {
         position: sticky;
         font-size : 12px;
         padding-top : -1cm;
         margin-top : 2px;
         border : none;
         width: 100%;
       }

       main p {
         margin-left: 5px;
         margin-right: 5px;
       }

       /* Define the footer rules */
       footer {
           position: fixed;
           bottom: -60px;
           left: 0px;
           right: 0px;
           height: 50px;

           /* Extra personal styles */
           text-align: center;
           line-height: 35px;
       }
/*       .page-num:after { 
       	counter-increment: pages; 
       	content: counter(page) " of " counter(pages); 
       }*/
       table.page_content {width: 100%; border: none; padding: 1mm; font-size: 10px;font-family : "Arial", "Verdana", sans-serif;}
       table.page_header {width: 100%; border: none; padding: 2mm; font-size: 10px;font-family : "Arial", "Verdana", sans-serif; text-align: left; margin-left: 50px;margin-top: 50px;}
       table.page_content_header {width: 100%; font-size: 10px;border-collapse: collapse;font-family : "Arial", "Verdana", sans-serif;text-align: center; padding-bottom: 1px;
       	padding-top: 1px;}
       /*.footer .page-number:after {
         content: counter(page);
       }*/
</style>
</head>
<body>
{{-- 	<script type="text/php">
	    if (isset($pdf)) {
	        $x = 532;
	        $y = 85;
	        $text = "Halaman {PAGE_NUM} dari {PAGE_COUNT}";
	        $font = null;
	        $size = 7;
	        $color = array(0,0,0);
	        $word_space = 0.0;  //  default
	        $char_space = 0.0;  //  default
	        $angle = 0.0;   //  default
	        $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
	    }
	</script> --}}
  	<br>
	<main>
		@php
			$current = 1;
			$bukatr = false;
			$tutuptr = false;
			$total = count($records->detail_penerimaan_barang->where('flag', 0));
			$current = 1;
		@endphp
		@foreach($records->detail_penerimaan_barang->where('flag', 0) as $key => $data)
			@php
				$no_order = $records->konfirmasi->va->surat->kaji_ulang->pp->no_order;
				$jenis = $data->detail_pp->jenis->nama;
				$seri = $data->no_seri;
				$urutan = $data->urutan;
				$barcode = formatBarcode($no_order, $jenis, $seri, $urutan);
				if($current%4==1 || $current%4==3){
					$bukatr = true;
					$tutuptr = false;
				}
				if($current%4==2 || $current%4==0 || $current==$total){
					$bukatr = false;
					$tutuptr = true;
				}
			@endphp
			@if($current==1 || $current%4==1)
			<table style="width: 100%;margin-bottom: -10%;margin-top: -3%;">
			@endif
				@if($bukatr)
				<tr>
				@endif
					<td style="width: 50%;height: 370px;">
						<table style="border: 1px solid black;border-collapse: collapse;margin-top: 40px;">
							<tr>
								<td colspan="2" style="border-right: none;padding-top: -5px;padding-bottom: -5px;">
									<img src="{{ asset('img/icon.png') }}" width="30px" height="40px" style="display: block;">
								</td>
								<td style="border-right: none;border-left: none;text-align: left;padding-left: -60px;padding-top: -5px;padding-bottom: -5px;" width="90px">
									<b style="font-size: 10px;">PT PLN (PERSERO)</b>
									<br><b style="font-size: 10px;">PUSAT SERTIFIKASI</b>
								</td>
								<td colspan="2" style="border-right: none;border-left: none;width: 160px;text-align: center;padding-top: -5px;padding-bottom: -5px;">
									<img src="{{ asset('img/lmk.png') }}" width="180px" height="40px" style="display: block;">
								</td>
								<td style="border-left: none;text-align: right;width: 100px;">
									<img style="display: block;position: relative;" src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(45)->margin(0)->generate($barcode)) }} ">
								</td>
							</tr>
							<tr>
								<td colspan="6" style="text-align: center;border-bottom: 1px solid black;border-top: 1px solid black;"><b>KARTU BARANG</b></td>
							</tr>
							<tr>
								<td>No Agenda</td>
								<td style="text-align: right;" style="text-align: right;">:</td>
								<td colspan="4">-</td>
							</tr>
							<tr>
								<td>No WBS/IO</td>
								<td style="text-align: right;">:</td>
								<td colspan="4" style="text-align: left">{{ $records->konfirmasi->wbs_io }}</td>
							</tr>
							<tr>
								<td>Nama Perusahaan</td>
								<td style="text-align: right;">:</td>
								<td colspan="4" style="text-align: left">{{ $records->konfirmasi->va->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama }}</td>
							</tr>
							<tr>
								<td>Nama Barang</td>
								<td style="text-align: right;">:</td>
								<td colspan="4" style="text-align: left">{{ $data->nama_barang }}</td>
							</tr>
							<tr>
								<td>Merek</td>
								<td style="text-align: right;">:</td>
								<td colspan="4" style="text-align: left">{{ $data->detail_pp->merk }} - {{ $data->detail_pp->tipe }}</td>
							</tr>
							<tr>
								<td>Nomor Seri</td>
								<td style="text-align: right;">:</td>
								<td colspan="4" style="text-align: left">{{ $data->no_seri }}-{{$data->urutan}}</td>
							</tr>
							<tr>
								<td>Tanggal Terima</td>
								<td style="text-align: right;">:</td>
								<td colspan="4" style="text-align: left">{{ DateToStringYear($records->created_at) }}</td>
							</tr>

							<tr>
								<td colspan="2" style="border-top:1px solid black;border-right:1px solid black;text-align: center;width: 150px;">Penerima</td>
								<td colspan="4" style="border-top:1px solid black;text-align: center;width: 250px">Catatan</td>
							</tr>
							<tr>
								<td colspan="2" rowspan="3" style="border-right: 1px solid black;"></td>
								<td colspan="4" rowspan="3" style="text-align: center;vertical-align: top;font-size: 8px;">{{ $data->catatan or '-' }}</td>
							</tr>
							<tr>
								
							</tr>
							<tr>
								
							</tr>
							<tr>
								<td colspan="2" style="border-right:1px solid black;text-align: center;width: 150px;"></td>
								<td colspan="4" style="text-align: center;width: 250px">Jadwal</td>
							</tr>
							<tr>
								<td colspan="2" style="border-bottom:1px solid black;border-right:1px solid black;text-align: center;width: 150px;">{{ $records->penerima->nama }}</td>
								<td colspan="4" style="border-bottom:1px solid black;text-align: center;width: 250px">{{ DateToStringYear($records->konfirmasi->va->surat->kaji_ulang->detail->first()->tentative_start) }} s/d {{ DateToStringYear($records->konfirmasi->va->surat->kaji_ulang->detail->first()->tentative_end) }}</td>
							</tr>
							<tr>
								<td colspan="2" style="border-right: 1px solid black;text-align: center;width: 150px;">Status Barang</td>
								<td colspan="4" style="text-align: left;width: 250px;">Tanggal Selesai diuji/Kalibrasi :</td>
							</tr>
							<tr>
								<td colspan="2" style="width: 150px;border-right: 1px solid black;"></td>
								<td colspan="4" style="text-align: left;width: 250px;">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2" style="width: 150px;border-right: 1px solid black;"></td>
								<td colspan="4" style="text-align: left;width: 250px;"></td>
							</tr>
							<tr>
								<td colspan="2" style="width: 150px;border-right: 1px solid black;">
									<img class="ui middle aligned tiny image" src="{{ asset('img/uncheck.png') }}" width="15px" style="display: block;">&nbsp;<span>Belum diuji/kalibrasi</span>
								</td>
								<td colspan="4" style="text-align: left;width: 250px;">Paraf :</td>
							</tr>
							<tr>
								<td colspan="2" style="width: 150px;border-right: 1px solid black;">
									<img src="{{ asset('img/uncheck.png') }}" width="15px" style="display: block;">&nbsp;<span>Sedang diuji/kalibrasi</span>
								</td>
								<td colspan="4" style="text-align: left;width: 250px;">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2" style="width: 150px;border-right: 1px solid black;">
									<img class="ui middle aligned tiny image" src="{{ asset('img/uncheck.png') }}" width="15px" style="display: block;">&nbsp;<span>Selesai diuji/kalibrasi</span>
								</td>
								<td colspan="4" style="text-align: left;width: 250px;">&nbsp;</td>
							</tr>
						</table>
					</td>
				@if($tutuptr)
				</tr>
				@endif
			@if($current==$total || $current%4==0)
			</table>
			@endif
			<?php
				$current++;	
			?>
		@endforeach
	</main>
</body>
</html>