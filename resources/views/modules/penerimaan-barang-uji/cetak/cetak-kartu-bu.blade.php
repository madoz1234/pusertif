<html>
<head>
<style>
        @page {
            margin: 60px 25px;
            counter-reset: page;
        }

       /* Define now the real margins of every page in the PDF */
       body {
           margin-top: 3cm;
           margin-left: 0cm;
           margin-right: 0cm;
           margin-bottom: 2cm;
           font-style: normal;
       }

       .container {
       	width: 100%;
       }

       .ui.table.bordered {
         border: solid black 1px;
         border-collapse: collapse;
         width: 100%;
         font-family : "Arial", "Verdana", sans-serif;
       }

       .ui.table.bordered td {
         border: solid black .5px;
         border-collapse: collapse;
         /*padding:10px;*/
         padding:5px;
       }

       .ui.table.bordered td.center.aligned {
         text-align : center;
       }

       .page-break {
            page-break-after: auto;
       }

       .pad{
       	padding-top: 5px;
       	padding-bottom: 5px;
       }

       .pad2{
       	padding-left: 5px;
       }

       i.icon.check.circle:before {
       	content: "\f058";
       }

       i.icon.remove.circle.outline:before {
       	content: "\f05c";
       }

       i.icon.check.circle.outline:before {
       	content: "\f05d";
       }

       /* Define the header rules */
        header {
            position: fixed;
            top: -40px;
            left: 0px;
            right: 0px;
            height: 10px;

            /* Extra personal styles */
            text-align: center;
            /*line-height: 35px;*/

            font-size: 10px;
        }

        .column {
        	width: 50%;
        	padding: 10px;
        	height: 300px;
        }

        .row:after {
        	content: "";
        	width: 50%;
        	display: table;
        	clear: both;
        }

        .page-break {
            page-break-after: always;
       }
		
       main {
         position: sticky;
         font-size : 12px;
         padding-top : -1cm;
         margin-top : 2px;
         border : none;
         width: 100%;
       }

       main p {
         margin-left: 5px;
         margin-right: 5px;
       }

       /* Define the footer rules */
       footer {
           position: fixed;
           bottom: -60px;
           left: 0px;
           right: 0px;
           height: 50px;

           /* Extra personal styles */
           text-align: center;
           line-height: 35px;
       }
       .page-num:after { 
       	counter-increment: pages; 
       	content: counter(page) " of " counter(pages); 
       }
       table.page_content {width: 100%; border: none; padding: 1mm; font-size: 10px;font-family : "Arial", "Verdana", sans-serif;}
       table.page_header {width: 100%; border: none; padding: 2mm; font-size: 10px;font-family : "Arial", "Verdana", sans-serif; text-align: left; margin-left: 50px;margin-top: 50px;}
       table.page_content_header {width: 100%; font-size: 10px;border-collapse: collapse;font-family : "Arial", "Verdana", sans-serif;text-align: center; padding-bottom: 1px;
       	padding-top: 1px;}
       /*.footer .page-number:after {
         content: counter(page);
       }*/
</style>
</head>
<body>
	<script type="text/php">
	    if (isset($pdf)) {
	        $x = 532;
	        $y = 85;
	        $text = "Halaman {PAGE_NUM} dari {PAGE_COUNT}";
	        $font = null;
	        $size = 7;
	        $color = array(0,0,0);
	        $word_space = 0.0;  //  default
	        $char_space = 0.0;  //  default
	        $angle = 0.0;   //  default
	        $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
	    }
	</script>
  	<br>
	<main>
		@foreach($records->detail_penerimaan_barang as $key => $data)
			@php
				$no_order = $records->konfirmasi->va->surat->kaji_ulang->pp->no_order;
				$jenis = $data->detail_pp->jenis->nama;
				$seri = $data->no_seri;
				$urutan = $data->urutan;
				$barcode = formatBarcode($no_order, $jenis, $seri, $urutan);
			@endphp
			<table class="page_content_header page-break" style="margin-top: -80px;">
				<tr>
					<td class="center aligned" style="border-top: 1px solid black;border-left: 1px solid black;">
						<img class="ui middle aligned tiny image" src="{{ asset('img/PLN.png') }}" width="60px" height="80px" style="display: block; margin-left: -130px;"><p style="margin-top: -45px;margin-left: 69px;"><b style="font-size: 17px;">PLN PUSERTIF</b></p></td>
						<td style="border-top: 1px solid black;"></td>
						<td class="center aligned" style="border-top: 1px solid black;"><img src="{{ asset('img/ilac.png') }}" width="90px" height="90px" style="display: block;"></td>
						<td class="center aligned" style="border-top: 1px solid black;"><img src="{{ asset('img/kan1.png') }}" width="150px" height="90px" style="display: block;"></td>
						<td class="center aligned" style="border-top: 1px solid black;"><img src="{{ asset('img/kan2.png') }}" width="150px" height="90px" style="display: block;"></td>
						<td class="center aligned" style="border-top: 1px solid black; border-right: 1px solid black;"><img style="display: block;" src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(100)->generate($barcode)) }} "></td>
					</tr>
					<tr>
						<td style="text-align: center;border:1px solid black;border-collapse: collapse;" colspan="6" class="pad"><b style="font-size: 14px;">KARTU BARANG</b></td>
					</tr>
					<tr>
						<td style="text-align: left;width: 200px;border-left: 1px solid black;font-size: 11px;" class="pad pad2">No Agenda</td>
						<td style="text-align: left;font-size: 11px;" class="pad">:</td>
						<td style="text-align: left;border-right: 1px solid black;font-size: 11px;" colspan="4" class="pad">-</td>
					</tr>
					<tr>
						<td style="text-align: left; border-left: 1px solid black;font-size: 11px;" class="pad pad2">No KPJ</td>
						<td style="text-align: left;font-size: 11px;" class="pad">:</td>
						<td style="text-align: left;border-right: 1px solid black;font-size: 11px;" colspan="4" class="pad">{{ $records->no_kpj }}</td>
					</tr>
					<tr>
						<td style="text-align: left; border-left: 1px solid black;font-size: 11px;" class="pad pad2">Nama Perusahaan</td>
						<td style="text-align: left;font-size: 11px;" class="pad">:</td>
						<td style="text-align: left;border-right: 1px solid black;font-size: 11px;" colspan="4" class="pad">{{ $records->konfirmasi->va->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama }}</td>
					</tr>
					<tr>
						<td style="text-align: left; border-left: 1px solid black;font-size: 11px;" class="pad pad2">Nama Barang</td>
						<td style="text-align: left;font-size: 11px;" class="pad">:</td>
						<td style="text-align: left;border-right: 1px solid black;font-size: 11px;" colspan="4" class="pad">{{ $data->nama_barang }}</td>
					</tr>
					<tr>
						<td style="text-align: left; border-left: 1px solid black;font-size: 11px;" class="pad pad2">Merek</td>
						<td style="text-align: left;font-size: 11px;" class="pad">:</td>
						<td style="text-align: left;border-right: 1px solid black;font-size: 11px;" colspan="4" class="pad">{{ $data->detail_pp->merk }} - {{ $data->detail_pp->tipe }}</td>
					</tr>
					<tr>
						<td style="text-align: left; border-left: 1px solid black;font-size: 11px;" class="pad pad2">Nomor Seri</td>
						<td style="text-align: left;font-size: 11px;" class="pad">:</td>
						<td style="text-align: left;border-right: 1px solid black;font-size: 11px;" colspan="4" class="pad">{{ $data->no_seri }}-{{$data->urutan}}</td>
					</tr>
					<tr>
						<td style="text-align: left; border-left: 1px solid black;font-size: 11px;" class="pad pad2">Tanggal Terima</td>
						<td style="text-align: left;font-size: 11px;" class="pad">:</td>
						<td style="text-align: left;border-right: 1px solid black;font-size: 11px;" colspan="4" class="pad">{{ $data->created_at }}</td>
					</tr>
					<tr>
						<td style="text-align: center;border-top: 1px solid black;border-left: 1px solid black;font-size: 11px;" colspan="2">Penerimaan</td>
						<td style="text-align: center;border-top: 1px solid black;border-right: 1px solid black;border-left: 1px solid black;font-size: 11px;" colspan="4">Catatan</td>
					</tr>
					<tr>
						<td style="text-align: center; border-left: 1px solid black;font-size: 11px;" colspan="2" rowspan="4">&nbsp;</td>
						<td style="text-align: center;border-right: 1px solid black; border-left: 1px solid black;font-size: 11px;" colspan="4">{{ $data->catatan or '-' }}</td>
					</tr>
					<tr>
						<td style="text-align: center; border-left: 1px solid black;border-right: 1px solid black;font-size: 11px;" colspan="4">&nbsp;</td>
					</tr>
					<tr>
						<td style="text-align: center; border-left: 1px solid black;border-right: 1px solid black;font-size: 11px;" colspan="4">Jadwal</td>
					</tr>
					<tr>
						<td style="text-align: center; border-left: 1px solid black;border-right: 1px solid black;border-bottom: 1px solid black;font-size: 11px;" colspan="4" rowspan="2">{{ DateToStringYear($records->konfirmasi->va->surat->kaji_ulang->detail->first()->tentative_start) }} s/d {{ DateToStringYear($records->konfirmasi->va->surat->kaji_ulang->detail->first()->tentative_end) }}</td>
					</tr>
					<tr>
						<td style="text-align: center; border-left: 1px solid black;border-right: 1px solid black;border-bottom: 1px solid black;font-size: 11px;" colspan="2">{{ $records->penerima->nama }}</td>
					</tr>
					<tr>
						<td style="text-align: center; border-left: 1px solid black;font-size: 11px;" colspan="2">Status Barang</td>
						<td style="text-align: left;border-right: 1px solid black; border-left: 1px solid black;font-size: 11px;padding-left: 10px;" colspan="4">Tanggal Selesai diuji/Kalibrasi</td>
					</tr>
					<tr>
						<td style="text-align: left; border-left: 1px solid black;border-bottom: 1px solid black;font-size: 11px;padding-left: 10px;" colspan="2" rowspan="3">
							<img class="ui middle aligned tiny image" src="{{ asset('img/check1.png') }}" width="20px" style="display: block;">&nbsp;<span>Belum diuji/kalibrasi</span><br>
							<img src="{{ asset('img/uncheck.png') }}" width="20px" style="display: block;">&nbsp;<span>Sedang diuji/kalibrasi</span><br>
							<img class="ui middle aligned tiny image" src="{{ asset('img/uncheck.png') }}" width="20px" style="display: block;">&nbsp;<span>Selesai diuji/kalibrasi</span>
						</td>
						<td style="text-align: left;border-right: 1px solid black; border-left: 1px solid black;font-size: 11px;padding-left: 10px;" colspan="4">{{ DateToStringYear($records->konfirmasi->va->surat->kaji_ulang->detail->first()->tentative_end) }}</td>
					</tr>
					<tr>
						<td style="text-align: center;border-right: 1px solid black; border-left: 1px solid black;font-size: 11px;" colspan="4"></td>
					</tr>
					<tr>
						<td style="text-align: left;border-right: 1px solid black; border-left: 1px solid black;font-size: 11px;border-bottom: 1px solid black;padding-left: 10px;" colspan="4">Paraf :
							<br><br><br><br><br><br><br>
						</td>
					</tr>
			</table>
		@endforeach
	</main>
</body>
</html>