<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Peminta Jasa Baru</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}
		<div class="ui info message" style="margin-top: 0px">
			<div class="header">Informasi Aktivasi Peminta Jasa</div>
			<p>Fitur Buat Peminta Jasa Baru hanya diperuntukkan kategori PLN dan user SPM saja. </br>Kami akan mengirim email kode verifikasi, pastikan email yang dimasukan benar.</p>
		</div>
		<div class="two fields">
				<div class="field">
					<label>Nama Lengkap</label>
					<input name="nama" placeholder="Nama Lengkap" type="text">
				</div>
				<div class="field">
					<label>Kategori</label>
					<select name="tipe_customer" class="watcher ui fluid search dropdown cari">
						<option value="">Pilih Salah Satu</option>
						<option value="2">AP-PLN</option>
						<option value="0">PLN</option>
						<option value="1">NON-PLN</option>
					</select>
				</div>
		</div>
		<div class="two fields">
				<div class="field">
					<label>NIP</label>
					<input name="nip" placeholder="NIP" type="text" readonly="">
				</div>
				<div class="field">
					<label>Perusahaan</label>
					<select name="perusahaan_id" class="watcher ui fluid search dropdown cari perusahaan">
						{!!
							\App\Models\Master\Perusahaan::optionsa('nama', 'id', ['filters' => [function($q){
								return $q->where('status', 1);
							}],
							'selected' => old('perusahaan_id')
						])
						!!}
					</select>
				</div>
		</div>
		{{-- <input type="hidden" name="tipe_customer" id="tipe_customer" value=""> --}}
		<div class="two fields">
			<div class="field">
				<label>No HP</label>
				<input type="text" class="angka" name="no_hp" placeholder="No HP" maxlength="13">
			</div>
			<div class="field">
				<label>Alamat</label>
				<textarea name="alamat" class="area" placeholder="Alamat" rows="1" readonly=""></textarea>
			</div>
		</div>
		<div class="two fields">
			<div class="field">
				<label>Email</label>
				<input type="email" name="email" placeholder="Email">
			</div>
			<div class="field">
				<label>No Telpon</label>
				<input type="text" name="no_tlp" placeholder="No Telpon" readonly="">
			</div>
		</div>
{{-- 		<div class="two fields">
			<div class="field">
			</div>
			<div class="field nip" style="display: none;">
				<label>NIP</label>
				<input type="text" placeholder="NIP" name="nip">
			</div>
		</div> --}}
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>
<script type="text/javascript">
	$('select[name=perusahaan_id]').on('change', function(value, text, $selectedItem){
        $.get('{{ url($pageUrl.'cari') }}/'+$(this).val(), function(ret){
        	$('input[name=no_tlp]').val(ret.no_tlp);
        	$('textarea[name=alamat]').val(ret.alamat);
        });
  	})


  	$('select[name=tipe_customer]').on('change', function(val){
  		if($(this).val()==0){
  			$('input[name=nip]').prop("readonly",false);
  		}else{
  			$('input[name=nip]').val('');
  			$('input[name=nip]').prop("readonly",true);
  		}

  		$.ajax({
			url: '{{ url($pageUrl.'perusahaan') }}/'+$(this).val(),
			type: 'GET',
			success: function (resp) {
				$('select[name="perusahaan_id"]').html(resp);
				var perusahaan = $('.ui.search.dropdown.perusahaan').find('.text');
				perusahaan.addClass('default');
				perusahaan.text('Pilih Salah Satu');
			},
		});
  	})

  	$(".angka").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
</script>