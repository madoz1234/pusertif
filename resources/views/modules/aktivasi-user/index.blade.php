@extends('layouts.list')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
d.email = $("input[name='filter[email]']").val();
d.nama_perusahaan = $("input[name='filter[nama_perusahaan]']").val();
@endsection

@section('toolbars')
    <button type="button" class="ui blue add button">
        <i class="plus icon"></i>
        Buat Peminta Jasa Baru
    </button>
@endsection

@section('filters')
<div class="field">
    <input type="text" name="filter[nama]" placeholder="PIC">
</div>
<div class="field">
    <input type="text" name="filter[email]" placeholder="Email">
</div>
<div class="field">
    <input type="text" name="filter[nama_perusahaan]" placeholder="Nama Perusahaan">
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
    <i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
    <i class="refresh icon"></i>
</button>
@endsection

@section('tables')
<div class="ui inverted loading dimmer">
    <div class="ui text loader">Loading</div>
</div>
<div class="ui top demo tabular menu">
    <div class="active item" data-tab="first">Aktivasi @if($aktivasi > 0)<span style="background-color:red;"class="ui circular label">{{ $aktivasi }}</span>@endif</div>
    <div class="item" data-tab="second">Histori</div>
</div>
<div class="ui bottom demo active tab" data-tab="first">
    @if(isset($structs['aktivasi']))
    <table id="listTable1" class="ui celled compact red table display" width="100%" cellspacing="0">
        <thead>
            <tr>
                @foreach($structs['aktivasi'] as $struct)
                <th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            {{-- @yield('tableBody') --}}
        </tbody>
    </table>
    @endif
</div>
<div class="ui bottom demo tab" data-tab="second">
    @if(isset($structs['histori']))
    <table id="listTable2" class="ui celled compact red table display" width="100%" cellspacing="0">
        <thead>
            <tr>
                @foreach($structs['histori'] as $struct)
                <th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            {{-- @yield('tableBody') --}}
        </tbody>
    </table>
    @endif
</div>
@endsection

@section('init-modal')
<script>
    var dt1 = "";
    var dt2 = "";
    onShow = function(){
    	$(".angka").keypress(function (e) {
    		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
    			return false;
    		}
    	});
        $('.checkbox').checkbox();
        $('.ui.dropdown').dropdown({
            onChange: function(value) {
                var target = $(this).dropdown();
                if (value!="") {
                    target
                    .find('.dropdown.icon')
                    .removeClass('dropdown')
                    .addClass('delete')
                    .on('click', function() {
                        target.dropdown('clear');
                        $(this).removeClass('delete').addClass('dropdown');
                        return false;
                    });
                }
            }
        });
        // force onChange  event to fire on initialization
        $('.ui.dropdown')
	        .closest('.ui.selection')
	        .find('.item.active').addClass('qwerty').end()
	        .dropdown('clear')
	        .find('.qwerty').removeClass('qwerty')
	        .trigger('click');

        $('[name=display_name]').on('change, keyup', function(event) {
            var display_name = $(this).val();
            $('[name=name]').val(slugify(display_name));
        });
        
        return false;
    };

    dt1 = $('#listTable1').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        responsive: true,
        autoWidth: false,
        processing: true,
        serverSide: true,
        lengthChange: false,
        pageLength: 10,
        info:     true,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid",
            type: 'POST',
            data: function (d) {
                d._token = "{{ csrf_token() }}";
                {{-- @yield('js-filters') --}}
                d.nama = $("input[name='filter[nama]']").val();
                d.email = $("input[name='filter[email]']").val();
                d.nama_perusahaan = $("input[name='filter[nama_perusahaan]']").val();
            }
        }, 
        columns: {!! json_encode($structs['aktivasi']) !!},
        drawCallback: function() {
            var api = this.api();

            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i, x, y) {
                cell.innerHTML = parseInt(cell.innerHTML)+i+1;
                // cell.innerHTML = i+1;
            } );

            $('[data-content]').popup({
                hoverable: true,
                position : 'top center',
                delay: {
                    show: 300,
                    hide: 800
                }
            });

            $('.ui.rating').rating('disable');

        },
    });

    // change page list
    $('select[name="filter[page]"]').on('change', function(e) {
        var length = this.value // $("input[name='filter[entri]']").val();
        length = (length != '') ? length : 10;
        dt1.page.len(length).draw();
        e.preventDefault();
    });

    // $('.filter.button').on('click', function(e) {
    //     dt.draw();
    //     // dt.ajax.reload();
    //     e.preventDefault();
    // });

    // $('.reset.button').on('click', function(e) {
    //     $('.dropdown .delete').trigger('click');
    //     setTimeout(function(){
    //         dt.draw();
    //     }, 100);
    // });
    
    // {/{ dd($pageUrl.'show-detail') }}
    /* Formatting function for row details - modify as you need */
    function format (id) {
        // `d` is the original data object for the row
        $.ajax({
            url: "{{ $pageUrl.'grid' }}",
            type: 'POST',
            dataType: 'html',
            data: {_token:'{{ csrf_token() }}', id:id},
        }) 
        .done(function(response) {
            console.log("success");
            $('#content_detail'+id).html(response);
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

        return '<div class="ui slider attached segment" id="content_detail'+id+'" style="width:97%"><center>Loading..</center></div>';
    }

    // Add event listener for opening and closing details
    $('#listTable1 tbody').on('click', 'td.details-control button', function () {
        var nilai = $(this).data('button');
        // alert(nilai);
        var tr = $(this).closest('tr');
        var rows = dt.row( tr );
        console.log(rows);
        if ( rows.child.isShown() ) {
            // This row is already open - close it
            rows.child.hide();
            tr.removeClass('shown');

            $(this).find('i').removeClass('minus');
            $(this).find('i').addClass('plus');
        }
        else {
            rows.child(format(nilai)).show();
            tr.addClass('shown');

            $(this).find('i').removeClass('plus');
            $(this).find('i').addClass('minus');
        }

    });

    $.fn.dataTable.ext.errMode = 'none';

    $('#listTable1').on( 'error.dt', function ( e, settings, techNote, message ) {
        console.log( 'An error has been reported by DataTables: ', message );
    }) ;

    dt2 = $('#listTable2').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        responsive: true,
        autoWidth: false,
        processing: true,
        serverSide: true,
        lengthChange: false,
        pageLength: 10,
        info:     true,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid2",
            type: 'POST',
            data: function (d) {
                d._token = "{{ csrf_token() }}";
                {{-- @yield('js-filters') --}}
                d.nama = $("input[name='filter[nama]']").val();
                d.email = $("input[name='filter[email]']").val();
                d.nama_perusahaan = $("input[name='filter[nama_perusahaan]']").val();
            }
        }, 
        columns: {!! json_encode($structs['histori']) !!},
        drawCallback: function() {
            var api = this.api();

            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i, x, y) {
                cell.innerHTML = parseInt(cell.innerHTML)+i+1;
                // cell.innerHTML = i+1;
            } );

            $('[data-content]').popup({
                hoverable: true,
                position : 'top center',
                delay: {
                    show: 300,
                    hide: 800
                }
            });

            $('.ui.rating').rating('disable');

        },
    });

    // change page list
    $('select[name="filter[page]"]').on('change', function(e) {
        var length = this.value // $("input[name='filter[entri]']").val();
        length = (length != '') ? length : 10;
        dt2.page.len(length).draw();
        e.preventDefault();
    });

    $('.filter.button').on('click', function(e) {
        dt2.draw();
        dt1.draw();
        // dt2.ajax.reload();
        e.preventDefault();
    });

    $('.reset.button').on('click', function(e) {
        $('.dropdown .delete').trigger('click');
        setTimeout(function(){
            dt2.draw();
            dt1.draw();
        }, 100);
        $('.dropdown').dropdown('clear');
    });
    
    // {/{ dd($pageUrl.'show-detail') }}
    /* Formatting function for row details - modify as you need */
    function format (id) {
        // `d` is the original data object for the row
        $.ajax({
            url: "{{ $pageUrl.'grid2' }}",
            type: 'POST',
            dataType: 'html',
            data: {_token:'{{ csrf_token() }}', id:id},
        }) 
        .done(function(response) {
            console.log("success");
            $('#content_detail'+id).html(response);
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

        return '<div class="ui slider attached segment" id="content_detail'+id+'" style="width:97%"><center>Loading..</center></div>';
    }

    // Add event listener for opening and closing details
    $('#listTable2 tbody').on('click', 'td.details-control button', function () {
        var nilai = $(this).data('button');
        // alert(nilai);
        var tr = $(this).closest('tr');
        var rows = dt2.row( tr );
        console.log(rows);
        if ( rows.child.isShown() ) {
            // This row is already open - close it
            rows.child.hide();
            tr.removeClass('shown');

            $(this).find('i').removeClass('minus');
            $(this).find('i').addClass('plus');
        }
        else {
            rows.child(format(nilai)).show();
            tr.addClass('shown');

            $(this).find('i').removeClass('plus');
            $(this).find('i').addClass('minus');
        }

    });

    $.fn.dataTable.ext.errMode = 'none';

    $('#listTable2').on( 'error.dt', function ( e, settings, techNote, message ) {
        console.log( 'An error has been reported by DataTables: ', message );
    }) ;

    $(document).on('click', '.deactive.button', function(e){
        var id = $(this).data('id');
        swal({
            title: 'Apakah anda yakin?',
            // text: "Data yang sudah ditolak, tidak dapat dikembalikan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Nonaktifkan',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result) {
                $.ajax({
                    url: '{{ url($pageUrl) }}/'+id,
                    type: 'POST',
                    data: {_token: "{{ csrf_token() }}", _method: "delete"},
                    success: function(resp){
                        swal(
                            'Berhasil!',
                            'Data berhasil di nonaktifkan.',
                            'success'
                            ).then(function(e){
                                dt1.draw();
                                dt2.draw();
                            });
                        },
                    error : function(resp){
                        swal(
                            'Gagal!',
                            'Data gagal di nonaktifkan, karena sedang dipakai',
                            'error'
                            ).then(function(e){
                                dt1.draw();
                                dt2.draw();
                            });
                        }
                    }
                );
            }
        })
    });

    $(document).on('click', '.aktivasi-ulang.button', function(e){
        event.preventDefault();
        var id = $(this).data('id');
        // /* Act on the event */
        loadModal({
            'url' : '{{ url($pageUrl) }}/'+id+'/ulang',
            'modal' : '.{{ $modalSize }}.modal',
            'formId' : '#dataForm',
            'onShow' : function(){ 
                onShow();
            },
        })
    });
</script>
@endsection
