<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Aktivasi Peminta Jasa</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.'aktivasi-ulang/'.$record->id) }}" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{$record->id}}">
		<input type="hidden" name="user_id" value="{{$record->user_id}}">
		{!! csrf_field() !!}
			<div class="ui info message" style="margin-top: 0px">
				<div class="header">Informasi Aktivasi Peminta Jasa</div>
				<p>Kami akan mengirim email kode verifikasi, pastikan email yang dimasukan benar.</p>
			</div>
			<div class="two fields">
				<div class="field">
					<label>Nama Lengkap</label>
					<input type="text" name="pic" placeholder="Nama Lengkap" value="{{ $record->nama_lengkap or '' }}" readonly="">
				</div>
				<div class="field">
					<label>Kategori</label>
					<input type="hidden" name="jenis_customer" value="{{ $record->tipe_customer }}">
					<input type="text" name="kategori" placeholder="Kategori" value="{{ $record->tipe_customer==0 ? 'PLN' : ($record->tipe_customer==1 ? 'NON-PLN' : 'AP-PLN')  }}" readonly="">
				</div>
			</div>
			<div class="two fields">
				<div class="field">
					<label>NIP</label>
					<input type="text" name="nip_" placeholder="NIP" value="{{ $record->nip or '' }}" readonly="">
				</div>
				<div class="field">
					<label>Nama Perusahaan</label>
					<input type="text" name="perusahaan" placeholder="Nama Perusahaan" value="{{ $record->perusahaan->nama or '' }}" readonly="">
				</div>
			</div>
			<div class="two fields">
				<div class="field">
					<label>No. HP</label>
					<input type="text" class="number" name="no_hp" placeholder="No. HP" value="{{ $record->no_hp or '' }}" readonly="">
				</div>
				<div class="field">
					<label>Alamat</label>
					<textarea name="alamat" class="area" placeholder="Alamat" rows="1" readonly="">{{ $record->perusahaan->alamat or '' }}</textarea>
				</div>
			</div>
			<div class="two fields">
				<div class="field">
					<label>Email</label>
					<input type="email" name="email" placeholder="Email" value="{{ $record->email or '' }}">
				</div>
				<div class="field">
					<label>No. Telpon</label>
					<input type="text" class="number" name="no_tlp" placeholder="No. Telpon" value="{{ $record->perusahaan->no_tlp or '' }}" readonly="">
				</div>
			</div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Aktivasi
		<i class="checkmark icon"></i>
	</div>
</div>