<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Aktivasi Peminta Jasa</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{$record->id}}">
		<input type="hidden" name="user_id" value="{{$record->user_id}}">
		{!! csrf_field() !!}
			<div class="ui info message" style="margin-top: 0px">
				<div class="header">Informasi Aktivasi Peminta Jasa</div>
				<p>Kami akan mengirim email kode verifikasi, pastikan email yang dimasukan benar.</p>
			</div>
			<div class="two fields">
				<div class="field">
					<label>Nama Lengkap</label>
					<input type="text" name="pic" placeholder="Nama Lengkap" value="{{ $record->nama_lengkap or '' }}" readonly="">
				</div>
				<div class="field">
					<label>Kategori</label>
					<input type="hidden" name="jenis_customer" value="{{ $record->tipe_customer }}">
					<input type="text" name="kategori" placeholder="Kategori" value="{{ $record->tipe_customer==0 ? 'PLN' : ($record->tipe_customer==1 ? 'NON-PLN' : 'AP-PLN')  }}" readonly="">
				</div>
			</div>
			<div class="two fields">
				<div class="field">
					<label>NIP</label>
					<input type="text" name="nip_" placeholder="NIP" value="{{ $record->nip or '' }}" readonly="">
				</div>
				<div class="field">
					<label>Nama Perusahaan</label>
					<input type="text" name="perusahaan" placeholder="Nama Perusahaan" value="{{ $record->perusahaan->nama or '' }}" readonly="">
				</div>
			</div>
			<div class="two fields">
				<div class="field">
					<label>No. HP</label>
					<input type="text" class="angka" name="no_hp" placeholder="No. HP" value="{{ $record->no_hp or '' }}" maxlength="15">
				</div>
				<div class="field">
					<label>Alamat</label>
					<textarea name="alamat" class="area" placeholder="Alamat" rows="1" readonly="">{{ $record->perusahaan->alamat or '' }}</textarea>
				</div>
			</div>
			<div class="two fields">
				<div class="field">
					<label>Email</label>
					<input type="email" name="email" placeholder="Email" value="{{ $record->email or '' }}">
				</div>
				<div class="field">
					<label>No. Telpon</label>
					<input type="text" class="number" name="no_tlp" placeholder="No. Telpon" value="{{ $record->perusahaan->no_tlp or '' }}" readonly="">
				</div>
			</div>
			<div class="field">
				<label class="required">Surat Penugasan Resmi Perusahaan</label>
				<div class="ui action choises input field">
					<input type="text" id="label-attachment" readonly placeholder="Format .pdf, .jpeg, .jpg, .bmp multiple">
					<input type="file" id="attachment" name="file[]" style="display: none!important;" multiple="multiple" accept="image/jpeg,image/gif,image/png,application/pdf">
					<div class="ui icon button">
						<i class="cloud upload alternate icon"></i>
					</div>
				</div>
				<div class="ui" style="color:#b58105;padding-top: 0em;padding-bottom: 0em;margin-right: 142px;margin-left: 0px;border-left-width: 0px;border-left-style: solid;padding-left: 0.5em;margin-top: 0em;top: 3px;left: 0px;margin-bottom:3px;">
						     * max 3 File, max size 5 Mb (5210Kb)
						    </div>
			</div>
			<div class="field">
		    	<label>Pesan</label>
				<textarea name="catatan" rows="3" placeholder="Pesan">{{$record->pesan}}</textarea>
			</div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button disabled">
		Aktivasi
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$(document).on('change', '#attachment', function(e){
		var count = this.files.length;
		if (count > 1) {
			$('#label-attachment').val(count+` Files Selected`);
		}
		$('.save.button').removeClass('disabled');
	});
</script>