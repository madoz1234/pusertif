@extends('layouts.form')

@section('styles')
<style type="text/css">
	.responsive.table{
		width: 100%;
		overflow-x: auto;
	}
	.jus{
		text-align: justify;
		text-justify: inter-word;
	}
	.fn{
		font-size: 12px;
	}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		judul: ['empty'],
	};
</script>
@endsection

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
	{!! csrf_field() !!}
	<table id="example" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
		<thead>
			<tr class="middle aligned">
				<th width="10" class="center aligned fn">No.</th>
				<th width="150" class="center aligned fn">Jenis Pengujian</th>
				<th width="250" class="center aligned fn">Spesifikasi</th>
				<th width="150" class="center aligned" style="font-size: 12px;">Jadwal Pengujian Tentative Lama</th>
				<th width="150" class="center aligned" style="font-size: 12px;">Jadwal Pengujian Tentative Baru</th>
			</tr>
		</thead>
		<tbody class="detail fluid container detail">
			@php
				$i=0;
			@endphp
			@foreach($detail as $key => $data)
				@if($data->tentative_start_new)
				@else
					<tr class="detail fn">
						<input type="hidden" name="detail[{{ $data->id }}][detail_id]" value="{{$data->pengujian_detail_id}}">
						<td class="center aligned">{{ $i+1 }}</td>
						<td class="left aligned">{{ $data->detail_pengujian->detailpenerimaan->detail_pp->jenis->nama }}</td>
						<td style="text-align: justify;text-justify: inter-word;">
							{{$data->detail_pengujian->detailpenerimaan->nama_barang or '-'}} [{{ $data->detail_pengujian->detailpenerimaan->urutan }}/{{number_format($data->detail_pengujian->detailpenerimaan->max)}}]
						</td>
						<td class="center aligned">
							<input type="hidden" name="detail[{{ $data->id }}][start]" value="{{$data->detail_pengujian->detailpenerimaan->tentative_start}}">
							<input type="hidden" name="detail[{{ $data->id }}][end]" value="{{$data->detail_pengujian->detailpenerimaan->tentative_end}}">
							{{ DateToStringYear($data->detail_pengujian->detailpenerimaan->tentative_start) }} - <br><label style="margin-left: -6px;">{{ DateToStringYear($data->detail_pengujian->detailpenerimaan->tentative_end) }}</label>
						</td>
						<td class="center aligned">
							<div class="ui tentative_start field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i><label style="margin-left: 2vh;">Start</label>
									<input name="detail[{{ $data->id }}][tentative_start]" class="start" type="text" placeholder="Tentative Start">
								</div>
							</div>
							<div class="ui tentative_end field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i><label style="margin-left: 2vh;">End &nbsp;</label>
									<input name="detail[{{ $data->id }}][tentative_end]" class="end" type="text" placeholder="Tentative End">
								</div>
							</div>
						</td>
					</tr>
					@php
						$i++;
					@endphp
				@endif
			@endforeach
		</tbody>
	</table>
</form>
<div class="actions">
	<div class="ui two column grid">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		<div class="right aligned column">
			<br>
			<button id="simpan" type="button" class="ui large positive right labeled icon save as page button">
				Simpan
				<i class="save icon"></i>
			</button>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">

	$(".number").on("keypress keyup blur",function (e) {    
		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	});

	var nowDate = new Date();
    var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), 0, 0, 0, 0);
    var tanggal = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);

	$('.tentative_start').calendar({
		startCalendar: tanggal, 
		type: 'date',
		formatter: {
			date: function (date, settings) {
				if (!date) return '';
				let momentDate = moment(date)
				return momentDate.format('YYYY-MM-DD')
			}
		},
		onChange: function () {
			startCalendar = $(this)
			var element = $(this).parents('tr').find('td div.tentative_end');
			element.calendar({
				type: 'date',
				startCalendar: startCalendar,
				formatter: {
					date: function (date, settings) {
						if (!date) return '';
						let momentDate = moment(date)
						return momentDate.format('YYYY-MM-DD')
					}
				}
			})
			$(this).parents('tr').find('td div.tentative_end').find('div').find('input').focus();
		}
	});

</script>
@append