<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail Data Schadule</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.'1') }}" method="POST">
		<div class="ui error message">
		</div>
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="1">
		{!! csrf_field() !!}
	      <div class="field">
	    	<label>No Order</label>
	        	<input name="no_order" placeholder="No Order" type="text" value="111/02/12/2019" readonly="">
	      </div>
	      <div class="field date">
	    	<label>Tanggal Order</label>
	        	<input name="tgl_order" placeholder="Tanggal Order" type="text" value="12 Januari 2019" readonly="">
	      </div>
	      <div class="field">
	      	<select name="jenis_pengujin" class="ui fluid search selection dropdown multiple" disabled="">
				<option value="">Pilih Jenis Pengujian</option>
				<option value="Uji serah terima (MDU)" >Uji serah terima (MDU)</option>
				<option value="Uji Rutin" selected>Uji Rutin</option>
				<option value="Uji jenis">Uji jenis</option>
			</select>
	      </div>
	     <div class="field">
	     	<label>Pilih Mata Uji</label>
	     	<select name="mata_uji" class="ui fluid search selection dropdown multiple" multiple="" disabled="">
				<option value="">Pilih Mata Uji</option>
				<option value="Mata Uji 1" selected="">Mata Uji 1</option>
				<option value="Mata Uji 2">Mata Uji 2</option>
				<option value="Mata Uji 3">Mata Uji 3</option>
			</select>
	     </div>
	      <div class="field ">
	    	<label>PIC</label>
	     	<select name="mata_uji" class="ui fluid search selection dropdown multiple" multiple="" disabled="">
				<option value="">PIC</option>
				<option value="Agus Salim" selected="">Agus Salim</option>
				<option value="Agus Salma">Agus Salma</option>
				<option value="Fachri">Fachri</option>
			</select>
	      </div>	
	      <div class="field ">
	    	<label>Client</label>
	        	<input name="klient" class="" placeholder="Klient" value="PT. Pragma Informatika" readonly="">
	      </div>
	    
	
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>