@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
	</style>
@append
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Upload Laporan Final</div>
<div class="content">
	@php 
		$id = 1;
	@endphp
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$id)."/kirimLaporan" }}" method="POST" enctype="multipart/form-data">
      <input type="hidden" name="_method" value="PUT">
      <input type="hidden" name="id" value="{{ $id }}">
      <input type="hidden" name="data" value="{{ $data }}">
      {!! csrf_field() !!}
		<div class="ui form">
			<table class="ui compact table" style="border-radius: 0; margin: 0">
				<tr>
					<td><label>No Nota Dinas</label></td>
					<td>:</td>
					<td>
						<input type="text" name="nota_dinas" placeholder="No Nota Dinas" class="ui input field transparent">
					</td>
				</tr>
				<tr>
					<td><label>Tgl Nota Dinas</label></td>
					<td>:</td>
					<td>
						<div class="field">
							<div class="ui calendar labeled input tgl">
								<div class="ui input left icon">
									<i class="calendar icon date"></i>
									<input name="tgl_nota" type="text" placeholder="Tgl Nota Dinas">
								</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="ui divider"></div>
		<div class="actions">
			<div class="ui black deny button" style="background-color: #363636;margin-left: -1px;">
				Batal
			</div>
			<div class="ui right floated simpan button" style="background-color: #00AEEF;">
				Submit
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	$('.tgl').calendar({
		ampm: false,
		type: 'date',
	// maxDate: tanggal,
		formatter: {
			date: function (date, settings) {
				if (!date) return '';
				let momentDate = moment(date)
				return momentDate.format('YYYY-MM-DD')
			}
		}
	});


	$(document).on('click', '.simpan', function(e){
		var formDom = "dataForm";
		if($(this).data("form") !== undefined){
			formDom = $(this).data('form');
		}
		swal({
			title: 'Apakah Anda Yakin?',
				text: "Data yang sudah diupload, tidak dapat diubah!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Simpan',
				cancelButtonText: 'Batal',
				reverseButtons: true
			}).then((result) => {
				if (result) {
					saveForm(formDom);
				}
			})
	});

	function saveForm(form="dataForm")
	{
		if($("#"+form).form('is valid')){
			$('#formModal').find('.loading.dimmer').addClass('active');
			$('#cover').show();
			$("#"+form).ajaxSubmit({
				success: function(resp){
					console.log('nice');
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							if(postSave(resp)){
								return true;
							}else{
								$('#cover').hide();
								window.location.reload();
								return true;
							}
							$('#formModal').find('.loading.dimmer').removeClass('active');
						})
					},
					error: function(resp){
						if (resp.responseJSON.status !== "undefined" && resp.responseJSON.status === 'false'){
							swal(
								'Oops, Maaf!',
								resp.responseJSON.message,
								'error'
								)
						}

						$('#cover').hide();
						var response = resp.responseJSON;
						$.each(response.errors, function(index, val) {
							clearFormError(index,val);
							showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
						},1000)
						$('#formModal').find('.loading.dimmer').removeClass('active');
					}
				});
		}else{
			$("#"+form).ajaxSubmit({
				success: function(resp){
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							if(postSave(resp)){
								return true;
							}else{
								$('#cover').hide();
								window.location.reload();
								return true;
							}
						})
					},
					error: function(resp){
						$.each(resp.responseJSON, function(index, val) {
							clearFormError(index,val);
							showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.message').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
						},1000)
					}
				});
		}
	}
</script>