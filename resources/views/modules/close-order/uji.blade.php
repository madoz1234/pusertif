@extends('layouts.form')

@section('styles')
<style type="text/css">
	.responsive.table{
		width: 100%;
		overflow-x: auto;
	}
	.jus{
		text-align: justify;
		text-justify: inter-word;
	}
	.fn{
		font-size: 12px;
	}
	.ui.status.dropdown .menu .item:nth-child(1) {
		background-color: #00abffcc;
		color: #333;
	}
	.ui.status.dropdown .menu .item:nth-child(2) {
		background-color: #dd0000cc;
		color: #333;
	}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		judul: ['empty'],
	};
</script>
@endsection

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
{!! csrf_field() !!}
	<table id="listTable" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
		<thead>
			<tr class="middle aligned">
				<th width="10" class="center aligned fn">#</th>
				<th width="200" class="center aligned fn">Jenis Pengujian</th>
				<th width="200" class="center aligned fn">Nama Barang</th>
				<th width="150" class="center aligned fn">Merk</th>
				<th width="150" class="center aligned fn">No Seri</th>
				<th width="250" class="center aligned fn">Catatan</th>
				<th width="250" class="center aligned fn">Pengujian</th>
				<th width="250" class="center aligned fn">Pengiriman Laporan</th>
				<th width="150" class="center aligned fn">Aksi </br><button type="button" class="ui mini button checked-all"><i class="checkmark icon"></i> Checklist All</button></th>
			</tr>
		</thead>
		<tbody>
			@php 
				$i=1;
				$angka =0;
				$cek =0;
				$x=0;
				$cik = $record->detail_penerimaan_barang->where('flag', 0)->where('status_close', 0)->count();
			@endphp
			@foreach($record->detail_penerimaan_barang->where('flag', 0)->groupBy('group_id') as $key => $row)
				@foreach($row as $kuy => $data)
					@if($kuy == 0)
						<tr>
							<td rowspan="{{$row->count()}}">{{$i}}</td>
							<td rowspan="{{$row->count()}}">{{$data->detail_pp->jenis->nama}}</td>
							<td>{{$data->nama_barang}} [{{$data->urutan}}/{{$data->max}}]</td>
							<td rowspan="{{$row->count()}}">{{$data->detail_pp->merk}}</td>
							<td>{{$data->no_seri}}</td>
							<td style="text-align:justify;text-justify:auto;">{!! readMoreText($data->catatan, 50) !!}</td>
							<td class="center aligned" rowspan="{{$row->count()}}">
								@if($data->detail_pengujian)
									@if($data->detail_pengujian->verifikasi == 1)
										<a class="ui green tag label">Diterima</a>
									@elseif($data->detail_pengujian->verifikasi == 2)
										<a class="ui red tag label">Ditolak</a>
									@else
										<a class="ui orange tag label">Belum Selesai</a>
									@endif
								@else
									<a class="ui orange tag label">Belum Selesai</a>
								@endif
							</td>
							<td class="center aligned" rowspan="{{$row->count()}}">
								@if($data->detail_pengujian)
									@if($data->detail_pengujian->detaillaporan)
										@if($data->detail_pengujian->detaillaporan->laporanpengujian->status == 1)
											@php
												$cek =1; 
											@endphp
											<a class="ui green tag label">Sudah Terkirim</a>
										@else
											<a class="ui orange tag label">Belum Terkirim</a>
										@endif
									@else
										<a class="ui orange tag label">Belum Terkirim</a>
									@endif
								@else
									<a class="ui orange tag label">Belum Selesai</a>
								@endif
							</td>
							<td class="field center aligned" rowspan="{{$row->count()}}">
								@if($cek>0)
									@if($data->status_close == 0)
										@php
											$x++;
										@endphp
										<div class="ui fitted checkbox">
											<input name="checklist[]" class="check" type="checkbox" data-id="{{ $data->id }}">
											<label></label>
										</div>
									@else
										<div class="ui green label">
											<i class="check end icon"></i>Sudah Close
										</div>
									@endif
								@else 
									-
								@endif
							</td>
						</tr>
					@else
						<tr>
							<td style="border-left: 1px solid #e8e9e9;">{{$data->nama_barang}} [{{$data->urutan}}/{{$data->max}}]</td>
							<td>{{$data->no_seri}}</td>
							<td style="text-align:justify;text-justify:auto;">{!! readMoreText($data->catatan, 50) !!}</td>
						</tr>
					@endif
				@endforeach
				@php
					$i++;
				@endphp
			@endforeach
			@php
				$angka = 0;
				$jumlah = $record->detail_penerimaan_barang->groupBy('detail_pp_id')->count();
				$pembanding = $record->detail_penerimaan_barang->where('flag', 0)->count();
			@endphp
			@foreach($record->detail_penerimaan_barang->groupBy('detail_pp_id') as $data)
				@php 
					$angka += $data->first()->max;
				@endphp
			@endforeach
			@php
				$nilai = $angka - $pembanding;
			@endphp
			@if($jumlah>0)
				@if($nilai > 0)
					<tr class="middle aligned">
						<td colspan="8">Ada <b>{{$nilai}}</b> barang yang belum diterima dari total <b>{{$angka}}</b> barang. Silahkan Klik pembatalan jika barang tersebut tidak datang.</td>
						<td class="center aligned">
							@if($cik > 0)
								<span data-tooltip="Semua barang harus Close Order terlebih dahulu sebelum melakukan pembatalan." data-position="left center"><i class="question red icon"></i></span>
							@else
								@if($record->status_close == 0)
									<button class="circular ui icon red button pembatalan" data-id="{{ $record->id }}" data-tooltip="Batalkan" data-position="top center">
										<i class="icon window close outline"></i>
									</button>
								@else
									<div class="ui green label">
										<i class="check end icon"></i>Sudah Dibatalkan
									</div>
								@endif
							@endif
						</td>
					</tr>
				@endif
			@endif
		</tbody>
		<input type="hidden" name="max" value="{{ $x }}">
	</table>
</form>
<div class="actions">
	<div class="ui two column grid">
		<div class="left aligned column">
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		@if($x > 0)
			<div class="right aligned column">
				<div class="ui green labeled icon button kirim-data">
					Simpan
					<i class="save icon"></i>
				</div>
			</div>
		@endif
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.tgl').calendar({
		ampm: false,
		type: 'date',
			// maxDate: tanggal,
			formatter: {
				date: function (date, settings) {
					if (!date) return '';
					let momentDate = moment(date)
					return momentDate.format('YYYY-MM-DD')
				}
			}
		});

	$(".number").on("keypress keyup blur",function (e) {    
		$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
	});

$(document).ready(function() {
		$(document).on('click', '.checked-all', function(e){
			var checked		= true;

			$('.check').each(function(e){
				checked = !$(this).prop('checked') ? false : checked;
			});
			$('.check').prop('checked', !checked);
		});

		$(document).on('click', '.close_order', function(event) {
            event.preventDefault();
            var id = $(this).data('id');
            var max = $('input[name="max"]').val();
            var url = "{{ url($pageUrl) }}/"+id+"/closeUji/"+max;
            swal({
            	title: 'Apakah Anda yakin?',
            	text: "Data yang sudah di Close Order, tidak dapat diubah!",
            	type: 'warning',
            	showCancelButton: true,
            	confirmButtonColor: '#3085d6',
            	cancelButtonColor: '#d33',
            	confirmButtonText: 'Ya',
            	cancelButtonText: 'Batal',
            	reverseButtons: true
            }).then((result) => {
            	if (result) {
            		$.ajax({
            			url: url,
            			type: 'GET',
            			success: function(resp){
            				window.location = "{{ url($pageUrl) }}";
            			},
            			error : function(resp){
            				window.location = "{{ url($pageUrl) }}";
            			}
            		});

            	}
            })
        });

        $(document).on('click', '.kirim-data', function(e){
				var arr = [];
				$('.check').each(function(){
					if($(this).is(':checked')){
						arr.push($(this).data('id'));
					}
				});
				var angka = $("input[name=angka]").val();
				var max = $('input[name="max"]').val();
				if (arr == '') {
					swal(
						'Oops !',
						'Checklist data terlebih dahulu.',
						'error'
					).then(function(e){
					});
				}else{
					swal({
						title: 'Apakah Anda yakin?',
						text: "Data yang sudah dichecklist, tidak dapat diubah!",
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Ya',
						cancelButtonText: 'Batal',
						reverseButtons: true
					}).then((result) => {
						if (result) {
							event.preventDefault();
							var url = "{{ url($pageUrl) }}/"+arr+"/closeUji/"+max;
							$.ajax({
								url: url,
								type: 'GET',
								success: function(resp){
									swal(
										'Tersimpan!',
										'Data berhasil disimpan.',
										'success'
									).then((result) => {
										$('#cover').hide();
										if(angka == arr.length){
											window.location = "{{ url($pageUrl) }}";
										}else{
											window.location.reload();
										}
										return true;
									});
								},
								error : function(resp){
									swal(
										'Gagal!',
										'Data gagal disimpan.',
										'error'
									).then((result) => {
										$('#cover').hide();
										if(angka == arr.length){
											window.location = "{{ url($pageUrl) }}";
										}else{
											window.location.reload();
										}
										return true;
									});
								}
							});
						}
					})
				}
		});

        $(document).on('click', '.pembatalan', function(event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = "{{ url($pageUrl) }}/"+id+"/pembatalan";
            swal({
            	title: 'Apakah Anda yakin?',
            	text: "Data yang sudah di Batalkan, tidak dapat diubah!",
            	type: 'warning',
            	showCancelButton: true,
            	confirmButtonColor: '#3085d6',
            	cancelButtonColor: '#d33',
            	confirmButtonText: 'Ya',
            	cancelButtonText: 'Batal',
            	reverseButtons: true
            }).then((result) => {
            	if (result) {
            		$.ajax({
            			url: url,
            			type: 'GET',
            			success: function(resp){
            				window.location = "{{ url($pageUrl) }}";
            			},
            			error : function(resp){
            				window.location = "{{ url($pageUrl) }}";
            			}
            		});

            	}
            })
        });
});
</script>
@append