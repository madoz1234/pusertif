<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail Group</div>
<div class="scrolling content">
	@if(isset($structs['listStruct5']))
	<table id="listTableDetail" class="ui celled compact red table display" width="100%" cellspacing="0">
		<thead>
			<tr>
				@foreach ($structs['listStruct5'] as $struct)
				<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			<?php $i=1; ?>
			<tr>
				<td class="center aligned">{{ $i++ }}</td>
				<td class="center aligned">{{ $record->no_va }}</td>
				<td class="right aligned">{{ FormatNumber($record->nominal) }}</td>
				<td class="center aligned">{{ $record->surat->kaji_ulang->pp->no_order }}</td>
				<td class="center aligned">{{ DateToStringYear($record->surat->kaji_ulang->pp->tgl_order) }}</td>
				<td class="center aligned">{{ $record->surat->no_surat }}</td>
				<td class="center aligned">{{ DateToStringYear($record->surat->tgl_surat) }}</td>
				<td class="center aligned">{{ $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama }} </br> {{ $record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap}} </br> {{$record->surat->kaji_ulang->pp->user->pelanggans->no_hp }}</td>
			</tr>
			@foreach($record->groups as $record)
			<tr>
				<td class="center aligned">{{ $i++ }}</td>
				<td class="center aligned">{{ $record->no_va }}</td>
				<td class="right aligned">{{ FormatNumber($record->nominal) }}</td>
				<td class="center aligned">{{ $record->surat->kaji_ulang->pp->no_order }}</td>
				<td class="center aligned">{{ DateToStringYear($record->surat->kaji_ulang->pp->tgl_order) }}</td>
				<td class="center aligned">{{ $record->surat->no_surat }}</td>
				<td class="center aligned">{{ DateToStringYear($record->surat->tgl_surat) }}</td>
				<td class="center aligned">{{ $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama }} </br> {{ $record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap}} </br> {{$record->surat->kaji_ulang->pp->user->pelanggans->no_hp }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@endif
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
        $('#listTableDetail').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
                destroy: true,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: false,
                lengthChange: false,
                pageLength: 10,
                info:     true,
                filter: false,
                sorting: [],
                language: {
                    url: "{{ asset('plugins/datatables/Indonesian.json') }}"
                },
            }
        );
    });
</script>