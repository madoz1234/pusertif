<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Upload Status Bank</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.'upload') }}" method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}
      	<div class="field">
			<label class="requred">Pilih File</label>
			<div class="ui action choises input">
				<input type="text" id="label-attachment" readonly placeholder="Format .xls">
				<input type="file" id="attachment" name="file" style="display: none!important;">
				<div class="ui icon button">
					<i class="cloud upload alternate icon"></i>
				</div>
			</div>
		</div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>