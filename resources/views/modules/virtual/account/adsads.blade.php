@extends('layouts.list-va')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
		.ui.disposisi.dropdown .menu .item:nth-child(1) {
			background-color: #ffc000;
			color: #000;
		}
		.ui.disposisi.dropdown .menu .item:nth-child(2) {
			background-color: #00b0f0;
			color: #000;
		}
	</style>
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filterdata')
	d.no_order = $("input[name='filter[no_order]']").val();
    d.tanggal_order = $("input[name='filter[tanggal_order]']").val();
    d.no_surat = $("input[name='filter[no_surat]']").val();
    d.jenis_pelayanan_id = $("select[name='filter[jenis_pelayanan_id]']").val();
    d.perusahaan_id = $("select[name='filter[perusahaan_id]']").val();
	d.no_va = $("input[name='filter[no_va]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			file: ['empty'],
		};
	</script>
@endsection

@section('toolbars')
	@if(auth()->user()->hasRole(['keuangan']))
		<button type="button" class="ui blue download-rekap button">
			<i class="download icon"></i>
			Download Rekap VA
		</button>
	@endif
@endsection

@section('filters')
    <div class="field">
		<input name="filter[no_order]" placeholder="No Order" type="text">
	</div>
    <div class="field">
        <div class="ui left icon date input">
            <i class="calendar icon"></i>
            <input type="text" name="filter[tanggal_order]" placeholder="Tanggal Order">
        </div>
    </div>
    <div class="field">
      <input type="text" name="filter[no_surat]" placeholder="No Surat Permintaan">
    </div>
    <div class="field">
      <select name="filter[jenis_pelayanan_id]" class="watcher ui fluid search dropdown">
        {!! \App\Models\Master\JenisPelayanan::options('nama', 'id', [], 'Pilih Layanan') !!}
      </select>
    </div>
    <div class="field">
      <select name="filter[perusahaan_id]" class="watcher ui fluid search dropdown">
        {!!
          \App\Models\Master\Perusahaan::optionsa('nama', 'id', ['filters' => [function($q){
            return $q->where('status', 1);
          }],
          'selected' => old('id')
        ], 'Nama Perusahaan')
        !!}
      </select>
    </div>
    <div class="field">
        <input type="text" name="filter[no_va]" placeholder="No VA">
    </div> 
    <button type="button" class="ui teal icon filter button" data-tooltip="Cari Data">
      <i class="search icon"></i>
    </button>
    <button type="reset" class="ui icon reset button" data-tooltip="Bersihkan Pencarian">
      <i class="refresh icon"></i>
    </button>
@endsection

@section('tables')
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="ui top demo tabular menu">
	<div class="active item tab-enable" data-tab="first">Baru @if($satu > 0)<span style="background-color:red;"class="ui circular label">{{ $satu }}</span>@endif</div>
	<div class="item tab-disable" data-tab="second">Menunggu Teraktivasi @if($dua > 0)<span style="background-color:red;"class="ui circular label">{{ $dua }}</span>@endif</div>
	<div class="item tab-disable" data-tab="third">Aktif @if($tiga > 0)<span style="background-color:red;"class="ui circular label">{{ $tiga }}</span>@endif</div>
  	<div class="item tab-disable" data-tab="four">Nonaktif @if($empat > 0)<span style="background-color:red;"class="ui circular label">{{ $empat }}</span>@endif</div>
  	<div class="item tab-disable" data-tab="five">Riwayat Download</div>
</div>

<div class="ui bottom demo active tab" data-tab="first">
	@if(isset($structs['listStruct']))
		<table id="listTable" class="ui celled compact red table display" width="100%" cellspacing="0">
			<thead>
				<tr>
					@foreach ($structs['listStruct'] as $struct)
					<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
					@endforeach
				</tr>
			</thead>
			<tbody>
				@yield('tableBody')
			</tbody>
		</table>
	@endif
</div>
<div class="ui bottom demo tab" data-tab="second">
	@if(isset($structs['listStruct2']))
	<table id="listTable2" class="ui celled compact red table display" width="100%" cellspacing="0">
		<thead>
			<tr>
				@foreach ($structs['listStruct2'] as $struct)
				<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@yield('tableBody')
		</tbody>
	</table>
	@endif
</div>
<div class="ui bottom demo tab" data-tab="third">
	@if(isset($structs['listStruct3']))
	<table id="listTable3" class="ui celled compact red table display" width="100%" cellspacing="0">
		<thead>
			<tr>
				@foreach ($structs['listStruct3'] as $struct)
				<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@yield('tableBody')
		</tbody>
	</table>
	@endif
</div>
<div class="ui bottom demo tab" data-tab="four">
	@if(isset($structs['listStruct4']))
	<table id="listTable4" class="ui celled compact red table display" width="100%" cellspacing="0">
		<thead>
			<tr>
				@foreach ($structs['listStruct4'] as $struct)
				<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@yield('tableBody')
		</tbody>
	</table>
	@endif
</div>
<div class="ui bottom demo tab" data-tab="five">
	@if(isset($structs['listStruct5']))
	<table id="listTable5" class="ui celled compact red table display" width="100%" cellspacing="0">
		<thead>
			<tr>
				@foreach ($structs['listStruct5'] as $struct)
				<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@yield('tableBody')
		</tbody>
	</table>
	@endif
</div>
@endsection
@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	$('.date').calendar({
        type: 'date',
        formatter: {
          date: function (date, settings) {
            if (!date) return '';
            let momentDate = moment(date)
            return momentDate.format('DD/MM/YYYY')
          }
        }
    })
	$(document).ready(function() {
		$('.ui.watcher.dropdown').css({
			'width': '250px'
		});

		$(document).on('click', '.eye.pengujian', function (e){
			var id = $(this).data('id');
			var url = "{{ url($pageUrl) }}/"+id+"/detail";
			loadModal({
				'url' : url,
				'modal' : 'large modal',
				'formId' : '#dataForm',
				'onShow' : function(){ 
					onShow();
				},
			})
		});

		$(document).on('click', '.select-all', function(e){
			e.preventDefault();
			var checked		= true;
			
			$('.va').each(function(e){
				checked = !$(this).prop('checked') ? false : checked;
				// $(this).prop('checked', !$(this).prop('checked'));
			});

			$('.va').prop('checked', !checked);
		});


		$(document).on('click', '.download-rekap', function(e){
			var arr = [];
			$('.va').each(function(){
				if($(this).is(':checked')){
					arr.push($(this).data('id'));
				}
			});

			if (arr == '') {
				swal(
					'Oops !',
					'Checklist data terlebih dahulu.',
					'error'
				).then(function(e){
				});
			}else{
				swal({
					title: 'Apakah anda yakin?',
					text: "Data yang sudah dichecklist, tidak dapat di download kembali!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya',
					cancelButtonText: 'Batal'
				}).then((result) => {
					if (result) {
						var url = "{{ url($pageUrl) }}/download/"+arr;
						window.open(url, '_blank');
						// window.location = url;
					}
					location.reload()
				})
				
			}
		});

		$(document).on('click', '.tab-enable', function (e){
			$('.download-rekap').removeClass('hidden');
		});

		$(document).on('click', '.tab-disable', function (e){
			$('.download-rekap').addClass('hidden');
		});

		$(document).on('click', '.riwayat-va.button', function(e){
			var id = $(this).data('id');
			var url = "{{ url($pageUrl) }}/group/"+id;
			loadModal(url);
		});

	});
	$(document).on('click', '.aktivasi', function (e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/"+id+"/aktivasi";
		loadModalVa({
			'url' : url,
			'modal' : 'mini modal',
			'formId' : '#dataForm',
			'onShow' : function(){ 
				onShow();
			},
		})
	});

	function loadModalVa(url) {
		$('#formModal').modal({
			// inverted: true, // ver.1 background modal white
			observeChanges: true,
			closable: true,
			detachable: false,
			autofocus: false,
			onApprove : function() {
				dismissModal();
				$("#dataForm").form('validate form');
				if($("#dataForm").form('is valid')){
					$('#formModal').find('.loading.dimmer').addClass('active');
					$("#dataForm").ajaxSubmit({
							success: function(resp){
							var msg =resp.message;
							$("#formModal").modal('hide');
							swal(
								'Tersimpan!',
								'Data berhasil disimpan.',
								'success'
								).then((result) => {
									$('#formModal').find('.loading.dimmer').removeClass('active');
									if(msg){
										var url = "{{ url($pageUrl) }}/downloadUlang/"+msg;
										window.open(url, '_blank');
									}
									location.reload()
								})
							},
							error: function(resp){
								$('#formModal').find('.loading.dimmer').removeClass('active');
								$.each(resp.responseJSON.errors, function(index, val) {
									clearError(index,val);
									showError(index,val);
								});
								time = 7;
								interval = setInterval(function(){
									time--;
									if(time == 0){
										$('.red.error-label').remove();
										$('.pointing.prompt.label.transition.visible').remove();
										$('.error').each(function (index, val) {
											$(val).removeClass('error');
										});
										clearInterval(interval);
									}
								},1000);
							}
						});
				}
				return false;
			},
			onShow: function(){
				dismissModal();
				$('#formModal').find('.loading.dimmer').addClass('active');
				$('#formModal').addClass(url.modal);
				$(this).draggable({
					cancel: ".content"
				});
				$('.menu .item').tab();
		    	$('.next').tab();
				$.get(url, { _token: "{{ csrf_token() }}" } )
				.done(function( response ) {
					$('#formModal').html(response);
					$('#dataForm').form({
						inline: true,
						fields: formRules
					});
					$('.menu .item').tab();
		    	$('.next').tab();
					$('.ui.dropdown').dropdown();
					initModal();
				});
				$('.menu .item').tab();
		    	$('.next').tab();
			},
			onHidden: function(){
				dismissModal();
				$('#formModal').html(`<div class="ui inverted loading dimmer">
					<div class="ui text loader">Loading</div>
					</div>`);
			}
		}).modal('show');
	}
</script>
@append

@section('init-modal')
	<script>
		onShow = function(){
        	$('select[name=kaji_ulang]').on('change', function(){
				$.ajax({
					url: '{{ url('ajax/option/kaji_ulang') }}',
					type: 'POST',
					data: {
						_token: "{{ csrf_token() }}",
						kaji_ulang: this.value
					},
				})
				.done(function(response) {
					if(response){
						$("[name=wbs_io]").val(response);
						$('[name=wbs_io]').attr('readonly', true);
					}else{
						$("[name=wbs_io]").val('');
						$('[name=wbs_io]').attr('readonly', false);
					}
				})
				.fail(function() {
					console.log("error");
				});
			})	
        };
	</script>
@endsection
