<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Aktivasi Ulang
	<h5>No Order : {{ $record->surat->kaji_ulang->pp->no_order }}<br>
	No VA : {{ $record->no_va }} <br> Nominal VA : {{ rtrim(rtrim(number_format($record->nominal,2,',','.'), '0'), ',')}}</h5>
</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id)."/saveAktivasi" }}" method="POST">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{$record->id}}">
		{!! csrf_field() !!}
		<div class="field">
			<label>Tanggal Aktif</label>
			<div class="ui labeled input tgl_aktif">
				<div class="ui label">
					<i class="calendar icon" style="margin-left: 5px;"></i>
				</div>
				<input type="text" name="tgl_aktif" style="text-align: right;" placeholder="Tanggal Aktif">
			</div>
		</div>
		<div class="field">
			<label>Tanggal Kadaluarsa</label>
			<div class="ui labeled input tgl_kadaluarsa">
				<div class="ui label">
					<i class="calendar icon" style="margin-left: 5px;"></i>
				</div>
				<input type="text" name="tgl_kadaluarsa" style="text-align: right;" placeholder="Tanggal Kadaluarsa">
			</div>
		</div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
		var startCalendar = null;
		$('.tgl_aktif').calendar({
    		type: 'date',
    		formatter: {
    			date: function (date, settings) {
    				if (!date) return '';
    				let momentDate = moment(date)
    				return momentDate.format('YYYY-MM-DD')
    			}
    		},
    		onChange: function () {
    			startCalendar = $(this)
    			var element = $('.tgl_kadaluarsa');
    			element.calendar({
    				type: 'date',
    				startCalendar: startCalendar,
    				formatter: {
    					date: function (date, settings) {
    						if (!date) return '';
    						let momentDate = moment(date)
    						return momentDate.format('YYYY-MM-DD')
    					}
    				}
    			})
    			$('.tgl_kadaluarsa').find('input').focus();
    		}
    	});
});
</script>
