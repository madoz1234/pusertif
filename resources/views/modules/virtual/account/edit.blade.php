<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Aktivasi Virtual Account</div>
<div class="content">
	{{-- <form class="ui form"> --}}
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
	{{-- <h4 class="ui dividing header">Virtual Account</h4> --}}
	<div class="field">
		<label>Nomer Permintaan</label>
	    <div class="field">
	    	<input type="text" name="" placeholder="No Virtual Account" value="{{ $record->no_order or '' }}" readonly="">
	    </div>
	</div>
	<div class="field">
		<label>Tanggal Permintaan</label>
	    <div class="field">
	    	<input type="text" name="" placeholder="No Virtual Account" value="{{ $record->tgl_order or '' }}" readonly="">
	    </div>
	</div>
	<div class="field">
		<label>Pemenesan</label>
	    <div class="field">
	    	<input type="text" name="" placeholder="No Virtual Account" value="{{ $record->user->nama or '' }}" readonly="">
	    </div>
	</div>
	<div class="field">
		<label>Layanan</label>
	    <div class="field">
	    	<input type="text" name="" placeholder="No Virtual Account" value="{{ $record->pelayanan->nama or '' }}" readonly="">
	    </div>
	</div>
	<div class="field">
		<label>Lingkup</label>
	    <div class="field">
	    	<input type="text" name="" placeholder="No Virtual Account" value="{{ $record->lingkup->nama or '' }}" readonly="">
	    </div>
	</div>
	<div class="field">
		<label>Jenis Pengujian</label>
	  <div class="field">
	  		<input type="text" name="" placeholder="Jenis Pengujian" value="{{ $record->lingkup->pelayanan->nama or ''}}" readonly="">
	  </div>
	</div>
	<div class="field">
		<label>Nomer Virtual Account</label>
	    <div class="field">
	    	<input type="text" name="no_va" placeholder="No Virtual Account">
	    </div>
	</div>
	<div class="field">
		<label>WBS/IO</label>
	  <div class="field">
	  		<input type="text" name="wbs_io" placeholder="WBS/IO">
	  </div>
	</div>
	<div class="field">
		<label>Tanggal Aktivasi</label>
	    <div class="field">
	    	<input type="date" name="tgl_aktif" placeholder="Tanggal Aktivasi">
	    </div>
	</div>
	<div class="field"> 
		<label>Tanggal Kadaluarsa</label>
	    <div class="field">
	    	<input type="date" name="tgl_kadaluarsa" placeholder="Tanggal Kadaluarsa">
	    </div>
	</div>
	{{-- <h4 class="ui dividing header">Data Pengujian</h4> --}}
	 	<div class="ui segment">
	  	<div class="field">
	    <div class="ui toggle checkbox">
	    	<input type="checkbox">
	    	<label>Aktifasi</label>
	    </div>
	  	</div>
		</div>
	<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
	</div>
</form>
