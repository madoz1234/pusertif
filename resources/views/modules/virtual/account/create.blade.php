<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Aktivasi Virtual Account</div>
<div class="content">
 	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
	      <div class="field">
	    	<label>No Permintaan</label>
	    	<select name="kaji_ulang" id="kaji_ulang" class="watcher ui fluid search pilihan kaji_ulang dropdown">
	    		{!!
	    			\App\Models\FrontEnd\PendaftaranPengujian::optionsa('no_order', 'id', [
	    				'filters' => [
	    					function($q){
	    						return $q->whereHas('kaji_ulang', function($u){
	    							$u->doesntHave('va')
	    							->where('keputusan', 1);
	    						});
	    					}
	    				],
	    				'selected' => old('no_order')
	    			])
	    			!!}
	    		</select>
	      </div>
	      <div class="field">
	    	<label>No Virtual Account</label>
	    	<input name="no_va" placeholder="No Virtual Account" type="text">
	      </div>
	      <div class="field">
	    	<label>WBS / IO</label>
	    	<input name="wbs_io" placeholder="WBS / IO" type="text">
	      </div>
	      <div class="field">
	    	<label>Status</label>
	    	<div class="ui toggle checkbox">
	    		<input type="hidden" name="status" value="1">
	    		<input disabled type="checkbox" name="status" checked>
	    		<label>Aktif</label>
	    	</div>
	      </div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>
<script type="text/javascript">
</script>