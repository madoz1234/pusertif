@extends('layouts.form')
@section('styles')
<style type="text/css">
.responsive.table{
	width: 100%;
	overflow-x: auto;
}
.jus{
	text-align: justify;
	text-justify: inter-word;
}
.triangle-left {
  width: 0;
  height: 0;
  border-top: 5px solid transparent;
  border-right: 10px solid #E8E8E8;
  border-bottom: 5px solid transparent;
  float: right;
  overflow:hidden; 
  display:block; 
  position: relative;
  right: 520px;
  top: 42px;
}

.triangle-right {
  width: 0;
  height: 0;
  border-top: 5px solid transparent;
  border-left: 10px solid #E8E8E8;
  border-bottom: 5px solid transparent;
  float: right;
  overflow:hidden; 
  display:block; 
  position: relative;
  left: -385px;
  top: -36px;
}
.triangle-left:hover{
  border-right: 10px solid#2ECC71;
}
.triangle-right:hover{
  border-left: 10px solid#2ECC71;
}
.jzdbox1 {
  width:950px; 
  background:#332f2e; 
  border-radius:5px; 
  overflow:hidden; 
  display:block; 
  margin-bottom:10px; 
  box-shadow:0 0 10px #201d1c; 
  margin:0 auto; 
}

.jzdcal {
  padding:0 10px 30px 25px; 
  box-sizing:border-box!important; 
  background:#749d9e; 
  background: -webkit-linear-gradient(#749d9e, #b3a68b)!important; 
  background: -o-linear-gradient(#749d9e, #b3a68b)!important; 
  background: -moz-linear-gradient(#749d9e, #b3a68b)!important; 
  background: linear-gradient(#749d9e, #b3a68b)!important;
}

.jzdcalt {
  font:50px 'Times New Rowman'; 
  font-weight:700; 
  color:#f7f3eb; 
  display:block; 
  margin:18px 0 0 0; 
  text-transform:uppercase; 
  text-align:center; 
  letter-spacing:1px;
}

.jzdcal span {
  font:24px 'Times New Rowman'; 
  font-weight:400; 
  color:#f7f3eb; 
  text-align:center; 
  width:150px; 
  height:150px; 
  display:inline-block; 
  float:left; 
  overflow:hidden; 
  line-height:150px;
  background-color: #000000;
}

.jzdcal .jzdb:before {
  opacity:0.3; 
  content:'o';
}

.circle {
  border:3px solid #f7f3eb; 
  box-sizing:border-box!important; 
  border-radius:200px!important;
}

span[data-title]:hover:after, 
div[data-title]:hover:after {
  font:24px 'Times New Rowman'; 
  font-weight:900; 
  content:attr(data-title); 
  position:absolute; 
  margin:0 0 100px; 
  background:#282423; 
  border:1px solid #f7f3eb; 
  color:#f7f3eb; 
  padding:5px; 
  z-index:9999; 
  min-width:500px; 
  max-width:500px;
}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		tipe: ['empty'],
	};
</script>
@endsection

@section('modals')
<div id="waduk">
	<div class="ui grid">
		<div class="ui segment">
			<div class="four wide column">
				<div id='kalender'>Test</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content-body')
<div class="ui warning pendaftaran hidden message">
	Layanan dan Lingkup harus diisi untuk menambahkan Item Uji.
</div>
<div class="ui warning export hidden message">
	Layanan dan Lingkup harus diisi sebelum download template item uji.
</div>
<form class="ui form" id="dataForm" action="{{ url($pageUrl.'save-ams') }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<div class="ui very basic thin segment">
		<table class="tabel" style="margin: 0;border: none;box-sizing: inherit;">
				<input type="hidden" name="prioritas" value="0">
				<input type="hidden" name="pp_id" value="{{ $record->id }}">
				<tr>
					<td width="150"><b>Kategori</b></td>
					<td width="10">:</td>
					<td class="field">
						<select name="kategori" id="kategori" class="watcher ui fluid search pilihan dropdown">
							<option value="">( Pilih Salah Satu )</option>
							<option value="0">PLN</option>
{{-- 							<option value="1">NON PLN</option>
							<option value="2">AP-PLN</option> --}}
						</select>
					</td>
					<td></td>
					<td width="150" style="padding-left: 23px;"><b>Perusahaan</b></td>
					<td width="10">:</td>
					<td class="field">
					<input type="hidden" name="perusahaan_id" value="" id="satu">
						<div class="field">
							<div class="ui search">
								<div class="ui icon input">
									<input class="prompt nama usaha" type="text" style="color:#9e9e9e;" name="nama_perusahaan" placeholder="Nama Perusahaan">
									<i class="search icon"></i>
								</div>
								<div class="results"></div>
							</div>
						</div> 
					</td>
				</tr>
				<tr>
					<td width="150"><b>Peminta Jasa</b></td>
					<td width="10">:</td>
					<td class="field">
						<select name="user_id" class="watcher ui fluid search pilihan dropdown">
							<option value="">( Pilih Salah Satu )</option>
						</select>
					</td>
					<td></td>
					<td width="150" style="padding-left: 23px;"><b>Alamat Perusahaan</b></td>
						<td width="10">:</td>
							<td class="field">
								<div class="field">
									<div class="ui left icon input">
										<textarea name="alamat" id="empat" class="areas empat alamat textarea" style="color:#9e9e9e;" placeholder="Alamat Perusahaan" rows="2"></textarea>
									</div>
								</div>
							</td>
						</td>
					</td>
				</tr>
				<tr>
					<td width="150"><b>Layanan</b></td>
					<td width="10">:</td>
					<td class="field">
					<select name="jenis_pelayanan_id" id="jenis_pelayanan_id" class="watcher ui fluid search pilihan jenis_pelayanan dropdown">
						{!!
							\App\Models\Master\JenisPelayanan::optionsa('nama', 'id', [
								'filters' => [
								],
								'selected' => old('jenis_pelayanan_id')
							])
							!!}
						</select>
					</td>
					<td>
						<span class="layanan info" data-tooltip="Detail Layanan" data-position="left center" style="cursor: pointer;"><i class="question circle outline red icon"></i></span>
					</td>
					<td width="150" style="padding-left: 23px;"><b>Lingkup</b></td>
					<td width="10">:</td>
					<td class="field">
						<select name="lingkup_id" class="watcher ui fluid search pilihan lingkup dropdown">
							<option value="">( Pilih Salah Satu )</option>
						</select>
					</td>
					<td></td>
				</tr>
				<tr>
					<td width="150"><b>Rencana Pelaksanaan</b></td>
					<td width="10">:</td>
					<td class="field">
						<div class="field">
							<div class="ui calendar labeled input periode">
								<div class="ui input left icon">
									<i class="calendar icon date"></i>
									<input name="rencana" type="text" placeholder="Rencana Pelaksanaan">
								</div>
							</div>
						</div>
					</td>
					<td>
						<button type="button" style="background-color: #da251c;" class="ui blue mini icon append lihat button disabled" data-tooltip="Lihat Jadwal" data-position="left center"><i class="eye icon"></i></button>
					</td>
					<td width="150" style="padding-left: 23px;"><b>Kelas</b></td>
					<td width="10">:</td>
					<td class="field">
						<select name="kelas" id="kelas" class="watcher ui fluid search pilihan dropdown kelas">
							<option value="">( Kelas )</option>
							<option value="1">Reguler</option>
							<option value="2">Prioritas</option>
						</select>
					</td>
				</tr>
				<tr>
					<td width="250"><b>No Surat Permintaan</b></td>
					<td width="10">:</td>
					<td class="lefted">
						<div class="field">
							<input id="no_surat" name="no_surat" type="text" placeholder="No Surat Permintaan" value="{{ $record->no_surat }}">
						</div>
					</td>
					<td></td>
					<td width="200" style="padding-left: 23px;"><b>Tgl Surat Permintaan</b></td>
					<td width="10">:</td>
					<td class="field">
						<div class="field">
							<div class="ui calendar labeled input tgl">
								<div class="ui input left icon">
									<i class="calendar icon date"></i>
									<input name="tgl_surat" type="text" placeholder="Tgl Surat Permintaan" value="{{ $record->tgl_surat }}">
								</div>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td width="250"><b>File Surat Permintaan</b></td>
					<td width="10">:</td>
					<td class="lefted">
						<div class="field">
							<div class="ui action choises input">
								<input type="text" class="isi_surat surat" readonly placeholder="Format .pdf, .jpeg, .jpg, .bmp">
								<input type="file" id="surat" name="surat" style="display: none!important;">
								<div class="ui icon button">
									<i class="cloud upload alternate icon"></i>
								</div>
							</div>
						</div>
					</td>
					<td></td>
					<td width="150" style="padding-left: 23px;"><b>Dokumen Pendukung</b></td>
					<td width="10">:</td>
					<td width="300" class="lefted">
						<div class="field">
							<div class="ui action choises input">
								<input type="text" class="isi_lampiran lampiran" readonly placeholder="Format .pdf, .jpeg, .jpg, .bmp">
								<input type="file" id="lampiran" name="lampiran[]" style="display: none!important;" multiple>
								<div class="ui icon button">
									<i class="cloud upload alternate icon"></i>
								</div>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td width="150"><b>Item Uji</b></td>
					<td width="10">:</td>
					<td class="field">
						<button type="button" class="ui blue buat-ams button" style="background-color: #363636;">
							<i class="plus icon"></i>
							Buat Baru
						</button>
						<button type="button" class="ui upload-ams button" style="background-color: #52f10d; color: black;">
							<i class="cloud upload icon"></i>
							Upload
						</button>
						<button type="button" class="ui export button" style="background-color: #00AEEF; color: black;">
							<i class="cloud download icon"></i>
							Template
						</button>
					</td>
					<td></td>
					<td width="150" style="padding-left: 23px;"><b>Catatan</b></td>
					<td width="10">:</td>
					<td width="300" class="lefted">
						<div class="field">
							<div class="ui left icon input">
								<textarea name="catatan" class="areas" style="margin-bottom: -1em;" placeholder="Catatan" rows="2">{!! $record->catatan !!}</textarea>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="ui very basic thin segment">
			<table id="detail" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;line-height: 1;">
				<thead>
					<tr class="middle aligned">
						<th width="30px" class="center aligned">No</th>
						<th width="300px" class="center aligned">Jenis Pengujian</th>
						<th width="300px"class="center aligned">Merk</th>
						<th width="150px" class="center aligned">Tipe</th>
						<th width="100px" class="center aligned">Jumlah</th>
						<th width="100px" class="center aligned">Satuan</th>
						<th width="200px" class="center aligned">Spesifikasi</th>
						<th width="80px" class="center aligned">Aksi</th>
					</tr>
				</thead>
				<tbody class="detail fluid container">				
				</tbody>
			</table>
		</div>
		<div class="actions">
			<div class="ui two column grid">
				<div class="left aligned column">
					<div class="ui gray deny labeled icon button" onclick="window.history.back()">
						<i class="chevron left icon"></i>
						Kembali
					</div>
				</div>
				<div class="right aligned column">		
					<button type="button" class="ui positive right labeled icon save as page2 button" style="background-color: #00AEEF;">
						Submit
						<i class="save icon"></i>
					</button>
				</div>
			</div>	
		</div>
	</form>
	@endsection
	@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			var nowDate = new Date();
			var today   = new Date(nowDate.getFullYear(), nowDate.getMonth() + 1, 0, 0, 0);
			var tanggal = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);
			$(document).on('click', '.layanan.info', function(e){
				event.preventDefault();
				// /* Act on the event */
				loadModal({
					'url' : '{{ url('permintaan/spm') }}/lihat-layanan',
					'modal' : '.{{ $modalSize }}.modal',
					'onShow' : function(){ 
						onShow();
					},
				})
			});

			$(document).on('click', '.export', function(e){
				$('.ui.warning.pendaftaran.message').removeClass('visible');
				$('.ui.warning.pendaftaran.message').addClass('hidden');
				$('.ui.warning.export.message').removeClass('visible');
				$('.ui.warning.export.message').addClass('hidden');
				var layanan = $("select[name='jenis_pelayanan_id']").val();
				var lingkup = $("select[name='lingkup_id']").val();
				if(layanan == '' || lingkup == '' || layanan == null || lingkup == null){
					$('.ui.warning.export.message').removeClass('hidden');
					$('.ui.warning.export.message').addClass('visible');
				}else{
					var url = "{{ url('permintaan/spm') }}/export/"+lingkup;
					// console.log(url)
					window.location = url;
				}
			});

			$(document).on('click', '.buat-ams.button', function(e){
				$('.ui.warning.export.message').removeClass('visible');
				$('.ui.warning.export.message').addClass('hidden');
				$('.ui.warning.pendaftaran.message').removeClass('visible');
				$('.ui.warning.pendaftaran.message').addClass('hidden');
				var layanan = $("select[name='jenis_pelayanan_id']").val();
				var lingkup = $("select[name='lingkup_id']").val();
				if(layanan == '' || lingkup == '' || layanan == null || lingkup == null){
					$('.ui.warning.pendaftaran.message').removeClass('hidden');
					$('.ui.warning.pendaftaran.message').addClass('visible');
				}else{
					var url = "{{ url('permintaan/spm') }}/buat";
					loadModalPengujian({
						'url' : url,
						'modal' : '{{ $modalSize }} modal',
					})
				}
			});

			$(document).on('click', '.upload-ams.button', function(e){
				$('.ui.warning.export.message').removeClass('visible');
				$('.ui.warning.export.message').addClass('hidden');
				$('.ui.warning.pendaftaran.message').removeClass('visible');
				$('.ui.warning.pendaftaran.message').addClass('hidden');
				var layanan = $("select[name='jenis_pelayanan_id']").val();
				var lingkup = $("select[name='lingkup_id']").val();
				if(layanan == '' || lingkup == '' || layanan == null || lingkup == null){
					$('.ui.warning.pendaftaran.message').removeClass('hidden');
					$('.ui.warning.pendaftaran.message').addClass('visible');
				}else{
					var url = "{{ url('permintaan/spm') }}/upload/"+lingkup;
					loadModalUpload({
						'url' : url,
						'modal' : '{{ $modalSize }} modal',
					})
				}
			});
			
			$(document).on('click', '.lihat', function(e){
				e.preventDefault();
				var waktu = $('input[name=prioritas]').val();
				loadModalJadwal({
					'url' : '{{ url('permintaan/spm') }}/lihat-jadwal/'+waktu,
					'modal' : '.{{ $modalSize }}.modal',
					'formId' : '#dataForm',
					'onShow' : function(){ 
						onShow();
					},
				})
			});

			$(document).on('click', '.triangle-right', function(e){
				var tahun = (parseInt($('[name=tahun]').val())+1);
				var now = parseInt($('[name=tahun]').val());
				for(var i=1;i<=12;i++){
					$('#data-'+i).switchClass('warna-'+now+'-'+i, 'warna-'+tahun+'-'+i); 
				}
				$('[name=tahun]').val(tahun);
				$('#tahun').html(tahun);
				loadData(tahun);
			});

			$(document).on('click', '.triangle-left', function(e){
				var tahun = (parseInt($('[name=tahun]').val())-1);
				var now = parseInt($('[name=tahun]').val());

				for(var i=1;i<=12;i++){
					$('#data-'+i).switchClass('warna-'+now+'-'+i, 'warna-'+tahun+'-'+i); 
				}
				$('[name=tahun]').val(tahun);
				$('#tahun').html(tahun);
				loadData(tahun);
			});

			function loadData(tahun) {
				var currentTime = new Date();
				var year = currentTime.getFullYear();
				if(tahun == year){
					var x = $('[name=bulan]').val();
					var y = $('[name=prioritas]').val();
					var angka = (parseInt(x)+parseInt(y));
					var akhir = Math.abs((angka - 12));
					if(angka > 12){
						for(var i=x;i<=12;i++){
							$('span.warna-'+tahun+'-'+i).css({'background-color':'#f40d0d;'});
						}
						for(var i=1;i<x;i++){
							$('span.warna-'+tahun+'-'+i).css({'background-color':'#000000;', 'color':'#ffffff;'});
						}
					}else{
						for(var i=1; i<x; i++){
							$('span.warna-2019-'+i).css({'background-color':'#000000;', 'color':'#ffffff;'});
						}
						for(var i=x; i<angka; i++){
							$('span.warna-2019-'+i).css({'background-color':'#f40d0d;'});
						}
						for(var z=angka; z<=12;z++){
							$('span.warna-2019-'+z).css({'background-color':'#c8e4d6;', 'color':'#000000;'});
						}
					}
				}else if(tahun < year){
					for(var z=1; z<=12;z++){
						$('span.warna-'+tahun+'-'+z).css({'background-color':'#000000;', 'color':'#ffffff;'});
					}
				}else{
					if(tahun == (year+1)){
						var x = $('[name=bulan]').val();
						var y = $('[name=prioritas]').val();
						var angka = (parseInt(x)+parseInt(y));
						var akhir = Math.abs((angka - 12));
						if(angka > 12){
							for(var i=1;i<=akhir;i++){
								$('span.warna-'+tahun+'-'+i).css({'background-color':'#f40d0d;'});
							}
							for(var z=akhir; z<=12;z++){
								$('span.warna-'+tahun+'-'+z).css({'background-color':'#c8e4d6;', 'color':'#000000;'});
							}
						}else{
							for(var z=1; z<=12;z++){
								$('span.warna-'+tahun+'-'+z).css({'background-color':'#c8e4d6;', 'color':'#000000;'});
							}
						}
					}else{
						for(i=1;i<=12;i++){
							$('span.circle').css({'background-color':'#c8e4d6;', 'color':'#000000;'});
						}
					}
				}
			}

			$('select[name=kelas]').on('change', function(e){
				if(this.value == 2){
					$('.lihat').removeClass('disabled');
				}else{
					$('.lihat').addClass('disabled');
				}
			})
			var check = 99;
			$('select[name=kategori]').on('change', function(e){
				var check = $(this).val();
				$('.ui.search').search('clear cache');
				$('.ui.search').search({
					minCharacters : 2,
					fullTextSearch: true,
					apiSettings: {
						url: "{{ url('cari-perusahaan') }}?q={query}&checked="+check+"",
					},
					fields: {
						results 	: 'data',
						title   	: 'nama',
						description : 'alamat',
						id      	: 'id'
					},
					onSearchQuery: function(result, response){
						check;
					},
					onSelect: function(result, response){
						if(result){
							$('[name=perusahaan_id]').val(result.id);
							$.ajax({
								url: '{{ url('ajax/option/pelanggan-spm') }}',
								type: 'POST',
								data: {
									_token: "{{ csrf_token() }}",
									perusahaan_id: result.id
								},
							})
							.done(function(response) {
								$('select[name=user_id]').html(response)
							})
							.fail(function() {
								console.log("error");
							});
							$('[name=alamat]').val(result.alamat);
							$('#kategori').val(result.kategori).change();
							$('.textarea').attr('readonly','readonly');
						}else{
							$('[name=perusahaan_id]').val('');
						}
						$(this).search('clear cache');
					},	
				});
			});

			$('.ui.search').search({
				minCharacters : 2,
				fullTextSearch: true,
				apiSettings: {
					url: "{{ url('cari-perusahaan') }}?q={query}",
				},
				fields: {
					results 	: 'data',
					title   	: 'nama',
					description : 'alamat',
					id      	: 'id'
				},
				onSearchQuery: function(result, response){
					$('[name=alamat]').val('');
					$('[name=no_tlp]').val('');
					$('[name=email_perusahaan]').val('');
					check;
				},
				onSelect: function(result, response){
					$('[name=alamat]').val('');
					$('[name=no_tlp]').val('');
					$('[name=email_perusahaan]').val('');
					$('.nip').hide();
					if(result){
						$('[name=perusahaan_id]').val(result.id);
						$.ajax({
							url: '{{ url('ajax/option/pelanggan-spm') }}',
							type: 'POST',
							data: {
								_token: "{{ csrf_token() }}",
								perusahaan_id: result.id
							},
						})
						.done(function(response) {
							$('select[name=user_id]').html(response)
						})
						.fail(function() {
							console.log("error");
						});
						$('[name=alamat]').val(result.alamat);
						$('#kategori').val(result.kategori).change();
						$('.textarea').attr('readonly','readonly');
					}else{
						$('[name=perusahaan_id]').val('');
					}
					$(this).search('clear cache');
				},
			});

			function monthDifff(d1, d2) {
				var months;
				months = (d2.getFullYear() - d1.getFullYear()) * 12;
				months -= d1.getMonth();
				months += d2.getMonth();
				return months <= 0 ? 0 : months;
			}

			$('.periode').calendar({
				type: 'month',
				minDate: today,
				formatter: {
					date: function (date, settings) {
						if (!date) return '';
						var day = date.getDate();
						var month = date.getMonth() + 1;
						var year = date.getFullYear();
						var month = month < 10 ? '0' + month : '' + month;
						return year+'-'+month;
					}
				},
				onChange: function (e) {
					var nowDate = new Date();
					var cek = monthDifff(nowDate, e);
					var waktu = $('input[name=prioritas]').val();
					if(cek <= waktu){
						swal({
							text: "Anda Memilih Jadwal Prioritas!",
							type: 'info',
							showCancelButton: false,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Ok',
						}).then((result) => {
							$('#kelas').val('2').change();
							$('.lihat').removeClass('disabled');
							$('.kelas').addClass('disabled');
							$("#no_surat").focus(); 
						})
					}else{
						swal({
							text: "Anda Memilih Jadwal Reguler!",
							type: 'info',
							showCancelButton: false,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Ok',
						}).then((result) => {
							$('#kelas').val('1').change();
							$('.lihat').addClass('disabled');
							$('.kelas').addClass('disabled');
							$("#no_surat").focus();
						})
					}
    			}
			});

			$('.tgl').calendar({
				ampm: false,
				type: 'date',
				// maxDate: tanggal,
				formatter: {
					date: function (date, settings) {
						if (!date) return '';
						let momentDate = moment(date)
						return momentDate.format('YYYY-MM-DD')
					}
				}
			});

			$('.ui.pilihan').css({
				'width': '350px'
			})

			$(document).on('click', '.ui.tambah.pengujian', function(event) {
				$('#Pengujian').modal({
					onShow: function(){
						$.ajax({
							url: '{{ url('ajax/option/jenis') }}',
							type: 'POST',
							data: {
								_token: "{{ csrf_token() }}",
								jenis_pelayanan_id: $('select[name=jenis_pelayanan_id]').val(),
								lingkup_id: $('select[name=lingkup_id]').val(),
							},
						})
						.done(function(response) {
							$('select[name=jenis_id]').html(response)
						})
						.fail(function() {
							console.log("error");
						});
					},
				}).modal('show');
			});

			$(document).on('click', '.filter-pelayanan.button', function(event) {
				var pelayanan = $("select[name='filter[pelayanan]']").val();
				var lingkup = $("select[name='filter[lingkup]']").val();
				var jenis_pengujian = $("input[name='filter[jenis_pengujian]']").val();

				$.ajax({
					url: '{{ url('permintaan/spm/search-layanan') }}',
					type: 'POST',
					data: {
						_token: "{{ csrf_token() }}",
						pelayanan : pelayanan,
						lingkup : lingkup,
						jenis_pengujian : jenis_pengujian,
					},
				})
				.done(function(response) {
					console.log(response)
					$('.listtable').html(response)
				})
				.fail(function() {
					console.log("error");
				});
			});

			$(document).on('click', '.reset-pelayanan.button', function(event) {
				$("select[name='filter[pelayanan]']").dropdown('clear');
				$("select[name='filter[lingkup]']").dropdown('clear');
				$("input[name='filter[jenis_pengujian]']").val('');

				$.ajax({
					url: '{{ url('permintaan/spm/search-layanan') }}',
					type: 'POST',
					data: {
						_token: "{{ csrf_token() }}",
					},
				})
				.done(function(response) {
					$('.listtable').html(response)
				})
				.fail(function() {
					console.log("error");
				});
			});
		});

		$.ajax({
			url: '{{ url('ajax/option/lingkup') }}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				jenis_pelayanan_id: $('select[name=jenis_pelayanan_id]').val()
			},
		})
		.done(function(response) {
			$('select[name=lingkup_id]').html(response)
		});

		$('select[name=jenis_pelayanan_id]').on('change', function(){
			$.ajax({
				url: '{{ url('ajax/option/lingkup') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					jenis_pelayanan_id: this.value
				},
			})
			.done(function(response) {
				$('select[name=lingkup_id]').html(response)
			})
			.fail(function() {
				console.log("error");
			});
		})

		$('select[name=jenis_pelayanan_id]').on('change', function(){
			$.ajax({
				url: '{{ url('ajax/option/prioritas') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					jenis_pelayanan_id: this.value
				},
			})
			.done(function(response) {
				$("input[name='prioritas']").val(response);
			})
			.fail(function() {
				console.log("error");
			});
		})

		$('select[name=perusahaan_id]').on('change', function(){
			$.ajax({
				url: '{{ url('ajax/option/pelanggan') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					perusahaan_id: this.value
				},
			})
			.done(function(response) {
				$('select[name=user_id]').html(response)
			})
			.fail(function() {
				console.log("error");
			});
		})

		$(document).on('click', '.hapus_data', function (e){
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#detail');
			var rows = table.find('tbody tr');
			if(rows.length == 0){
				$('.jenis_pelayanan').removeClass('disabled');
				$('.lingkup').removeClass('disabled');
			}
			$.each(rows, function(key, value){
				table.find('.numboor-'+$(this).data("id")).html(key+1);
			});
		});

		$(document).on('click', '.btn-edit', function (e){
			var id = $(this).data('id');

			$('.lrow-'+id).each(function() {
			  $( this ).addClass('hidden');
			});

			$('.inrow-'+id).each(function() {
			  $( this ).removeClass('hidden');
			});

			$('.btn-edit').each(function() {
			  if($( this ).data('id') == id){
				$(this).addClass('hidden');
			  }
			});

			$('.btn-save').each(function() {
			  if($( this ).data('id') == id){
				$(this).removeClass('hidden');
			  }
			});

			$('.save.as.page2').addClass('disabled');
		});

		$(document).on('click', '.btn-save', function (e){
			var id = $(this).data('id');

			$('.inrow-'+id).each(function() {
			  $( this ).addClass('hidden');
			  $(this).parent().find('.lrow-'+id).html($(this).val());
			});

			$('.lrow-'+id).each(function() {
			  $( this ).removeClass('hidden');
			});

			$('.btn-edit').each(function() {
			  if($( this ).data('id') == id){
				$(this).removeClass('hidden');
			  }
			});

			$('.btn-save').each(function() {
			  if($( this ).data('id') == id){
				$(this).addClass('hidden');
			  }
			});
			$('.save.as.page2').removeClass('disabled');
		});

		$(document).on('change', '[name^=lampiran]', function (e) {
			var count = e.target.files.length;
			if (count > 0) {
				$('.isi_lampiran').val(count+` Files Selected`);
			}
		});

		$(document).on('change', '[name=surat]', function (e) {
			var file = $(e.target);
			var name = '';
			var fieldName = $(this).attr('name');
			var res = fieldName.split("[]");
			for (var i=0; i<e.target.files.length; i++) {
				name += e.target.files[i].name + ', ';
				var fileDate = new Date(e.target.files[i].lastModified);
				var day = fileDate.getDate();
				var month = fileDate.getMonth();
				var year = fileDate.getFullYear();
				var hours = fileDate.getHours();
				var minutes = fileDate.getMinutes();
				var seconds = fileDate.getSeconds();

				if(day.toString().length == 1)
				{
					day = '0' + day.toString();
				}	
				if(month.toString().length == 1)
				{
					month = '0' + month.toString();
				}

				if(hours.toString().length == 1)
				{
					hours = '0' + hours.toString();
				}

				if(minutes.toString().length == 1)
				{
					minutes = '0' + minutes.toString();
				}

				if(seconds.toString().length == 1)
				{
					seconds = '0' + seconds.toString();
				}

				if ($('input[name="'+res[0]+'_taken['+i+']"]').length != 0)
				{
					$('input[name="'+res[0]+'_taken['+i+']"]').remove();
				}
				var date = e.target.files[i];
				input = `<input type="hidden" name="`+res[0]+`_taken[`+i+`]" value="`+year+`-`+month+`-`+day+` `+hours+`:`+minutes+`:`+seconds+`">`;
				$(this).append(input);
			}
			name = name.replace(/,\s*$/, '');
			$('.isi_surat').val(name);
		});
	</script>
	@append