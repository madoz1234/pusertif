@extends('layouts.form')
@section('styles')
<style type="text/css">
.responsive.table{
	width: 100%;
	overflow-x: auto;
}
.jus{
	text-align: justify;
	text-justify: inter-word;
}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		tipe: ['empty'],
	};
</script>
@endsection

@section('content-body')
<form class="ui form" id="dataForm" action="{{ url($pageUrl.$record->id).'/saveUbah' }}" method="POST">
	{!! csrf_field() !!}
	<a href="" class="ui violet ribbon label">Detil Lama</a>
	<div class="ui very basic thin segment">
		<table class="tabel" style="margin: 0;border: none;box-sizing: inherit;">
			<tr>
				<td width="150"><b>No Permintaan</b></td>
				<td width="10">:</td>
				<td class="field">
					{{ $record->no_order }}
			</tr>
			<tr>
				<td width="150"><b>Layanan</b></td>
				<td width="10">:</td>
				<td class="field">
					{{ $record->pelayanan->nama }}
			</tr>
			<tr>
				<td width="150"><b>Lingkup</b></td>
				<td width="10">:</td>
				<td class="field">
					{{ $record->lingkup->nama }}
				</td>
			</tr>
		</table>
	</div>
	<div class="ui very basic thin segment">
		<table id="exist" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
			<thead>
				<tr class="middle aligned">
					<th width="30px" class="center aligned">No</th>
					<th width="300px" class="center aligned">Jenis Pengujian</th>
					<th width="300px"class="center aligned">Merk</th>
					<th width="150px" class="center aligned">Tipe</th>
					<th width="100px" class="center aligned">Jumlah</th>
					<th width="100px" class="center aligned">Satuan</th>
					<th width="200px" class="center aligned">Spesifikasi</th>
				</tr>
			</thead>
			<tbody class="detil lama">
				@foreach($record->detail as $key => $detail)
					<tr>
						<td width="30px" class="center aligned">{{$key+1}}</td>
						<td width="300px" class="left aligned">{{ $detail->jenis->nama }}</td>
						<td width="300px"class="left aligned">{{ $detail->merk }}</td>
						<td width="150px" class="left aligned">{{ $detail->tipe }}</td>
						<td width="100px" class="right aligned">{{ rtrim(rtrim(number_format($detail->jumlah,2,',','.'), '0'), ',')}}</td>
						<td width="100px" class="left aligned">{{ $detail->satuan->satuan }}</td>
						<td width="200px" class="left aligned">{{ $detail->spesifikasi }}</td>
					</tr>
				@endforeach	
			</tbody>
		</table>
	</div>
	<br>
	<br>
	<div class="ui warning pendaftaran hidden message">
		Layanan dan Lingkup harus diisi untuk menambahkan Item Uji.
	</div>
	<input type="hidden" name="id" value="{{ $record->id }}">
	<a href="" class="ui orange ribbon label">Detil Baru</a>
	<div class="ui very basic thin segment">
		<table class="tabel" style="margin: 0;border: none;box-sizing: inherit;">
			<tr>
				<td width="150"><b>Layanan</b></td>
				<td width="10">:</td>
				<td class="field">
				<select name="jenis_pelayanan_id" class="watcher ui fluid search pilihan dropdown">
					{!!
						\App\Models\Master\JenisPelayanan::optionsa('nama', 'id', ['filters' => [function($q){
								return $q->whereIn('id', [1,2,4,5]);
							}]], 'Pilih Salah Satu')
						!!}
					</select>
				</td>
			</tr>
			<tr>
				<td width="150"><b>Lingkup</b></td>
				<td width="10">:</td>
				<td class="field">
					<select name="lingkup_id" class="watcher ui fluid search pilihan dropdown">
						<option value="">Pilih Salah Satu</option>
					</select>
				</td>
			</tr>
			<tr>
				<td width="150"></td>
				<td width="10"></td>
				<td>
					<div class="ui checkbox">
						<input type="checkbox" name="example" class="test" disabled="">
						<label>Checklist jika akan mengaktifkan Jenis Pengujian yang sama untuk semua data.</label>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div class="ui very basic thin segment">
		<table id="new" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
			<thead>
				<tr class="middle aligned">
					<th width="30px" class="center aligned">No</th>
					<th width="300px" class="center aligned">Jenis Pengujian</th>
					<th width="300px"class="center aligned">Merk</th>
					<th width="150px" class="center aligned">Tipe</th>
					<th width="100px" class="center aligned">Jumlah</th>
					<th width="100px" class="center aligned">Satuan</th>
					<th width="200px" class="center aligned">Spesifikasi</th>
					{{-- <th width="80px" class="center aligned">Aksi</th> --}}
				</tr>
			</thead>
			<tbody class="detail fluid container">
				@foreach($record->detail as $key => $detail)
				<tr class="list-row-{{$key}}" id="data-row" data-id="{{$key}}">
					<td class="center aligned numbor numboor-{{$key}}">{{$key+1}}</td>
					<td class="field">
						@if($key == 0)
							<select name="detail[{{ $key }}][jenis_id]" id="jenis_id" class="watcher ui transparent search dropdown jenis">
								<option value="">Pilih Salah Satu</option>
							</select>
						@else
							<input type="hidden" name="detail[{{ $key }}][jenis_id]">
							<label class="data-cek isi-{{ $key }}" id="detail[{{ $key }}][jenis_nama]" style="display: none;" data-id="{{ $key }}"></label>
							<div class="cari">
								<select name="detail[{{ $key }}][jenis_id]" id="jenis_id" class="watcher ui transparent search dropdown jenis">
									<option value="">Pilih Salah Satu</option>
								</select>
							</div>
						@endif
					</td>
					<td class="field">
						<div class="ui fluid input">
							<span class="sedit-label lrow-{{$key}}">{{ $detail->merk }}</span>
							<input type="text" class="sedit-input hidden inrow-{{$key}} merk" name="detail[{{$key}}][merk]" value="{{ $detail->merk }}">
						</div>
					</td>
					<td class="field">
						<div class="ui fluid input">
							<span class="sedit-label lrow-{{$key}}">{{ $detail->tipe }}</span>
							<input type="text" class="sedit-input hidden inrow-{{$key}}" name="detail[{{$key}}][tipe]" value="{{ $detail->tipe }}">
						</div>
					</td>
					<td class="field" style="text-align: right;">
						<span class="sedit-label lrow-{{$key}}">{{ rtrim(rtrim(number_format($detail->jumlah,2,',','.'), '0'), ',')}}</span>
						<input type="text" class="sedit-input hidden inrow-{{$key}} number" name="detail[{{$key}}][jumlah]" value="{{ $detail->jumlah }}">
					</td>
					<td class="field">
						<div class="ui fluid input">
							<span>{{ $detail->satuan->satuan }}</span>
							<input type="hidden" name="detail[{{$key}}][satuan_id]" value="{{ $detail->satuan_id }}">
						</div>
					</td>
					<td class="field">
						<div class="ui fluid input">
							<span class="sedit-label lrow-{{$key}}">{{ $detail->spesifikasi }}</span>
							<input type="text" class="sedit-input hidden inrow-{{$key}}" name="detail[{{$key}}][spesifikasi]" value="{{ $detail->spesifikasi }}">
						</div>
					</td>
				</tr>
				@endforeach		
			</tbody>
		</table>
	</div>
	<div class="ui bottom attached">
		<div class="actions">
			<div class="ui two column grid">
				<div class="left aligned column">
					<div class="ui gray deny labeled icon button" onclick="window.history.back()">
						<i class="chevron left icon"></i>
						Kembali
					</div>
				</div>
				<div class="right aligned column">		
					<button type="button" class="ui positive right labeled icon save as page3 button">
						Simpan
						<i class="save icon"></i>
					</button>
				</div>
			</div>	
		</div>
	</div>
</form>
@endsection
@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	$(document).ready(function() {
		$(document).ready(function(){
			$('#exist').DataTable( {
				"paging":   10,
				"ordering": false,
				"lengthChange": false,
				"filter": false,
				"info":     true,
				language: {
					url: "{{ asset('plugins/datatables/Indonesian.json') }}"
				},
			});
		})
		$(document).on('click', '.lihat', function(e){
			event.preventDefault();
			// /* Act on the event */
			loadModalJadwal({
				'url' : '{{ url($pageUrl) }}/lihat-jadwal',
				'modal' : '.{{ $modalSize }}.modal',
				'formId' : '#dataForm',
				'onShow' : function(){ 
					onShow();
				},
			})
		});

		$('select[name=kelas]').on('change', function(e){
			if(this.value == 2){
				$('.lihat').removeClass('disabled');
			}else{
				$('.lihat').addClass('disabled');
			}
		})

		$('.periode').calendar({
			type: 'month',
			// maxDate: today,
			formatter: {
				date: function (date, settings) {
					if (!date) return '';
					var day = date.getDate();
					var month = date.getMonth() + 1;
					var year = date.getFullYear();
					var month = month < 10 ? '0' + month : '' + month;
					return year+'-'+month;
				}
			}
		});

		$('.ui.pilihan').css({
			'width': '400px'
		})

		$('.jenis').css({
			'width': '300px'
		})

		$.ajax({
			url: '{{ url('ajax/option/lingkup') }}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				jenis_pelayanan_id: $('select[name=jenis_pelayanan_id]').val(),
				lingkup_id: $('input[name=data_lingkup]').val()
			},
		})
		.done(function(response) {
			$('select[name=lingkup_id]').html(response)
		});
	});

	$('select[name="detail[0][jenis_id]"]').on('change', function(e){
		if(this.value.length > 0){
			if($('input[name="example"]').is(':checked')){
				var nama = $('select[name="detail[0][jenis_id]"]').find(":selected").text();
				var id = this.value;
				var table = $('#new');
				var rows = table.find('tbody tr');
				$.each(rows, function(key, value){
					if(key>0){
						$('.isi-'+key).show();
						$('.isi-'+key).html(nama)
						$('input[name="detail['+key+'][jenis_id]"]').val(id);
						$('select[name="detail['+key+'][jenis_id]"]').val(id)
					}
				});
			}
		}else{
		}
	})

	$(".test").change(function() {
		if(this.checked) {
			$('.cari').hide();
			$('.data-cek').show();
			if($('select[name="detail[0][jenis_id]"] option:selected').val()){
				var nama = $('select[name="detail[0][jenis_id]"]').find(":selected").text();
				var id = $('select[name="detail[0][jenis_id]"]').find(":selected").val();
				var table = $('#new');
				var rows = table.find('tbody tr');
				$.each(rows, function(key, value){
					if(key>0){
						$('.isi-'+key).show();
						$('.isi-'+key).html(nama)
						$('input[name="detail['+key+'][jenis_id]"]').val(id);
						$('select[name="detail['+key+'][jenis_id]"]').val(id)
					}
				});
			}
		}else{
			$('.data-cek').hide();
			$('.cari').show();
		}
	});

	$('select[name=jenis_pelayanan_id]').on('change', function(){
		$.ajax({
			url: '{{ url('ajax/option/lingkup') }}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				jenis_pelayanan_id: this.value
			},
		})
		.done(function(response) {
			$('select[name=lingkup_id]').html(response)
		})
		.fail(function() {
			console.log("error");
		});
	})

	$('select[name=lingkup_id]').on('change', function(){
		$.ajax({
			url: '{{ url('ajax/option/jenis') }}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				jenis_pelayanan_id: $('select[name=jenis_pelayanan_id]').val(),
				lingkup_id: $('select[name=lingkup_id]').val(),
			},
		})
		.done(function(response) {
			$('input[name="example"]').prop("disabled", false);
			var table = $('#new');
			var rows = table.find('tbody tr');
			$.each(rows, function(key, value){
				$('select[name="detail['+key+'][jenis_id]"]').html(response)
			});
		})
		.fail(function() {
			console.log("error");
		});
	})

	$(document).on('click', '.hapus_data', function (e){
		var row = $(this).closest('tr');
		row.remove();
	});

	$(document).on('click', '.btn-edit', function (e){
		var id = $(this).data('id');

		$('.lrow-'+id).each(function() {
		  $( this ).addClass('hidden');
		});

		$('.inrow-'+id).each(function() {
		  $( this ).removeClass('hidden');
		});

		$('.btn-edit').each(function() {
		  if($( this ).data('id') == id){
			$(this).addClass('hidden');
		  }
		});

		$('.btn-save').each(function() {
		  if($( this ).data('id') == id){
			$(this).removeClass('hidden');
		  }
		});
	});

	$(document).on('click', '.btn-save', function (e){
		var id = $(this).data('id');

		$('.inrow-'+id).each(function() {
		  $( this ).addClass('hidden');
		  $(this).parent().find('.lrow-'+id).html($(this).val());
		});

		$('.lrow-'+id).each(function() {
		  $( this ).removeClass('hidden');
		});

		$('.btn-edit').each(function() {
		  if($( this ).data('id') == id){
			$(this).removeClass('hidden');
		  }
		});

		$('.btn-save').each(function() {
		  if($( this ).data('id') == id){
			$(this).addClass('hidden');
		  }
		});
	});
</script>
@append