<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Disposisi Surat</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">

      {!! csrf_field() !!}
      <input type="hidden" name="_method" value="PUT">
      <input type="hidden" name="id" value="{{ $record->id }}">
	  <input type="hidden" name="pengirim_id" value="{{ auth()->user()->id }}">
		<div class="ui form">
			<table class="ui compact table" style="border-radius: 0; margin: 0">
				<tr>
					<td width="100px"><label>No Permintaan </label></td>
					<td width="5px">:</td>
					<td>{{ $record->no_order }}</td>
				</tr>
				<tr>
					<td><label>Dari </label></td>
					<td>:</td>
					<td>{{ auth()->user()->nama }}</td>
				</tr>
				<tr>
					<td><label>Kepada </label></td>
					<td>:</td>
					<td>
						<select name="penerima_id[]" class="ui watcher fluid search dropdown">
							{!! \App\Models\Authentication\User::options('nama', 'id', ['filters' => [function($q) use ($record){
									return $q->akses($record);
							}]], 'Pilih Nama User') !!}
						</select>
					</td>
				</tr>
				<tr>
					<td><label>Keterangan </label></td>
					<td>:</td>
					<td>
						<div class="field">
							<textarea name="keterangan" class="areas input" id="keterangan" placeholder="Keterangan" rows="2"></textarea>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="ui divider"></div>
		<div class="actions">
			<div class="ui black deny button" style="background-color: #363636;margin-left: -1px;">
				Batal
			</div>
			<div class="ui right floated save as page button" style="background-color: #00AEEF;">
				Submit
			</div>
		</div>
	</form>
</div>