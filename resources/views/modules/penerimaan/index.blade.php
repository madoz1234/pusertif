@extends('layouts.list-penerimaan')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
		.ui.disposisi.dropdown .menu .item:nth-child(1) {
			background-color: #ffc000;
			color: #000;
		}
		.ui.disposisi.dropdown .menu .item:nth-child(2) {
			background-color: #00b0f0;
			color: #000;
		}
	</style>
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filterdata')
	d.no_order = $("input[name='filter[no_order]']").val();
    d.tanggal_order = $("input[name='filter[tanggal_order]']").val();
    d.no_surat = $("input[name='filter[no_surat]']").val();
    d.jenis_pelayanan_id = $("select[name='filter[jenis_pelayanan_id]']").val();
    d.perusahaan_id = $("select[name='filter[perusahaan_id]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			username: 'empty',
			email: 'empty',
			roles: 'empty',
		};
	</script>
@endsection

@section('toolbars')

@endsection

@section('filters')

	<div class="field">
		<input name="filter[no_order]" placeholder="No Order" type="text">
	</div>
    <div class="field">
        <div class="ui left icon date input">
            <i class="calendar icon"></i>
            <input type="text" name="filter[tanggal_order]" placeholder="Tanggal Order">
        </div>
    </div>
    <div class="field">
      <input type="text" name="filter[no_surat]" placeholder="No Surat Permintaan">
    </div>
    <div class="field">
      <select name="filter[jenis_pelayanan_id]" class="watcher ui fluid search dropdown">
        {!! \App\Models\Master\JenisPelayanan::options('nama', 'id', [], 'Pilih Layanan') !!}
      </select>
    </div>
    <div class="field">
      <select name="filter[perusahaan_id]" class="watcher ui fluid search dropdown">
        {!!
          \App\Models\Master\Perusahaan::optionsa('nama', 'id', ['filters' => [function($q){
            return $q->where('status', 1);
          }],
          'selected' => old('id')
        ], 'Nama Perusahaan')
        !!}
      </select>
    </div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('tables')
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="ui top demo tabular menu">
  <div class="active item tes online" data-tab="first">Masuk (Online) @if($online > 0)<span style="background-color:red;"class="ui circular label">{{ $online }}</span>@endif</div>
  <div class="item ams" data-tab="second">Masuk (AMS) @if($ams > 0)<span style="background-color:red;"class="ui circular label">{{ $ams }}</span>@endif</div>
  <div class="item tes manual" data-tab="third">Masuk (Manual) @if($spm > 0)<span style="background-color:red;"class="ui circular label">{{ $spm }}</span>@endif</div>
  <div class="item tes" data-tab="four">Historis</div>
  {{-- <div class="item tes" data-tab="five">Dialihkan</div> --}}
</div>
<div class="ui bottom demo active tab" data-tab="first">
	@if(isset($structs['listStruct']))
	<table id="listTable" class="ui celled compact red table display" width="100%" cellspacing="0">
		<thead>
			<tr>
				@foreach ($structs['listStruct'] as $struct)
				<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@yield('tableBody')
		</tbody>
	</table>
	@endif
</div>
<div class="ui bottom demo tab" data-tab="second">
	@if(isset($structs['listStruct4']))
	<table id="listTable3" class="ui celled compact red table display" width="100%" cellspacing="0">
		<thead>
			<tr>
				@foreach ($structs['listStruct4'] as $struct)
				<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@yield('tableBody')
		</tbody>
	</table>
	@endif
</div>
<div class="ui bottom demo tab" data-tab="third">
	@if(isset($structs['listStruct4']))
	<table id="listTable2" class="ui celled compact red table display" width="100%" cellspacing="0">
		<thead>
			<tr>
				@foreach ($structs['listStruct4'] as $struct)
				<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@yield('tableBody')
		</tbody>
	</table>
	@endif
</div>
<div class="ui bottom demo tab" data-tab="four">
	@if(isset($structs['listStruct3']))
	<table id="listTable4" class="ui celled compact red table display" width="100%" cellspacing="0">
		<thead>
			<tr>
				@foreach ($structs['listStruct3'] as $struct)
				<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@yield('tableBody')
		</tbody>
	</table>
	@endif
</div>
{{-- <div class="ui bottom demo tab" data-tab="five">
	@if(isset($structs['listStruct5']))
	<table id="listTable5" class="ui celled compact red table display" width="100%" cellspacing="0">
		<thead>
			<tr>
				@foreach ($structs['listStruct5'] as $struct)
				<th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@yield('tableBody')
		</tbody>
	</table>
	@endif
</div> --}}
@endsection
@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	$('.date').calendar({
        type: 'date',
        formatter: {
          date: function (date, settings) {
            if (!date) return '';
            let momentDate = moment(date)
            return momentDate.format('DD/MM/YYYY')
          }
        }
    })
	$(document).ready(function() {
		$(document).on('click', '.dispo', function (e){
			loadModal({
				'url' : '{{ url($pageUrl) }}/dispo',
				'modal' : '.{{ $modalSize }}.modal',
				'formId' : '#dataForm',
				'onShow' : function(){ 
					onShow();
				},
			})
		});

		$(document).on('click', '.perbaikan-message', function (e){
			var id = $(this).data("id");
			var url = "{{ url($pageUrl) }}/"+id+"/perbaikan-message"
			loadModal({
				'url' : url,
				'modal' : 'tiny modal',
				'formId' : '#dataForm',
				'onShow' : function(){ 
					onShow();
				},
			})
		});

		
		$('.ui.watcher.dropdown').css({
			'width': '250px'
		});


		$(document).on('click', '.tes', function (e){
			$('#impor').hide();
		});

		@if(auth()->user()->hasRole(['admin']))
		$(document).on('click', '.ams', function (e){
			$('#impor').show();
		});

		$(document).on('click', '#impor', function (e){
			$("#impor").addClass('loading');

			$.ajax({
				url: '{{ url($pageUrl)}}/import-ams',
				type: 'GET',
				success: function(resp){
					swal(
						'Berhasil!',
						'Data berhasil diimport.',
						'success'
						).then(function(e){
							$("#impor").removeClass('loading');
							location.reload();
						});
				},
				error : function(resp){
					swal(
						'Gagal!',
						'Data gagal diimport',
						'error'
						).then(function(e){
							$("#impor").removeClass('loading');
						});
				}
			});
		});
		@endif

		$(document).on('click', '.close.button', function(e){
			swal({
				title: 'Apakah anda yakin?',
				text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Hapus',
				cancelButtonText: 'Batal'
			}).then((result) => {
				swal('Terhapus!',
					'Data berhasil dihapus.',
					'success');
			})
		});
	});
</script>
@append

@section('init-modal')
	<script>
		$(document).on('click', '.tolak.button', function(e){
			var id = $(this).data('id');
			var url = "{{ url($pageUrl) }}/"+id+"/tolakOnline";
			swal({
				title: 'Apakah Anda yakin?',
				text: "Data yang sudah dikirim, tidak dapat dikembalikan!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Tolak',
				cancelButtonText: 'Batal',
				reverseButtons: true
			}).then((result) => {
				$('#lampiran-area').html(`<div class="ui active inverted dimmer">
					<div class="ui text loader">Loading</div>
					</div>`);
				if (result) {
					$.ajax({
						url: url,
						type: 'GET',
						success: function(resp){
							swal(
								'Berhasil!',
								'Data berhasil ditolak.',
								'success'
								).then(function(e){
									window.location.reload();
								});
							},
							error : function(resp){
								swal(
									'Gagal!',
									'Data gagal ditolak.',
									'error'
									).then(function(e){
									});
								}
							});
				}
			})
		});
	</script>
@endsection
