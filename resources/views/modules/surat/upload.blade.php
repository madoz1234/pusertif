@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
	</style>
@append
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Surat Penawaran</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl)."/saveUpload" }}" method="POST">
      {!! csrf_field() !!}
      <input type="hidden" name="_method" value="PUT">
      @foreach($record as $key => $data)
     	 <input type="hidden" name="data[{{ $key }}][id]" value="{{ $data }}">
      @endforeach
		<div class="ui form">
			<table class="ui compact table" style="border-radius: 0; margin: 0">
				<tr>
					<td style="width: 40%;"><b>Upload Scan Surat Penawaran</b></td>
					<td width="10">:</td>
					<td style="width: 100%;">
						<div class="field">
							<div class="ui action choises input">
								<input type="text" class="isi_surat_penawaran" readonly placeholder="Format .pdf">
								<input type="file" id="surat_penawaran" name="surat_penawaran[]" style="display: none!important;" accept="application/pdf">
								<div class="ui icon surat_penawaran button">
									<i class="cloud upload alternate icon"></i>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="ui divider"></div>
		<div class="actions">
			<div class="ui black deny button" style="background-color: #363636;margin-left: -1px;">
				Batal
			</div>
			<div class="ui right floated simpan_upload button" style="background-color: #00AEEF;">
				Submit
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click', '.simpan_upload', function(e){
			var formDom = "dataForm";
			if($(this).data("form") !== undefined){
				formDom = $(this).data('form');
			}
			swal({
				title: 'Apakah Data Sudah Benar?',
				text: "Data yang sudah dikirim, tidak dapat dikembalikan!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Simpan',
				cancelButtonText: 'Batal',
				reverseButtons: true
			}).then((result) => {
				if (result) {
					saveForm(formDom);
				}
			})
		});
		switch($('.disposisi.dropdown').dropdown('get text')){
				case 'Dialihkan':
					$('.disposisi.dropdown').css({
						'background-color': '#ffc000!important',
						'color': '#000'
					})
					break;
				case 'Diteruskan':
					$('.disposisi.dropdown').css({
						'background-color': '#00b0f0!important',
						'color': '#000'
					})
					break;
			}

		$(document).on('change', '.disposisi.dropdown', function (e){
			if($(this).find(":selected").val() == 1){
				$('#penerima').dropdown('clear');
				$(this).css({
					'background-color': '#ffc000!important',
					'color': '#000'
				})
			}else if($(this).find(":selected").val() == 2){
				$('#penerima').dropdown('clear');
				$(this).css({
					'background-color': '#00b0f0!important',
					'color': '#000'
				})
			}else{
				$('#penerima').dropdown('clear');
			}

			$.ajax({
				url: '{{ url('ajax/option/disposisi-msb') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					dispo_id: $(this).find(":selected").val(),
					pengirim: $('input[name=pengirim_id]').val(),
					layanan: $('input[name=layanan_id]').val()
				},
			})
			.done(function(response) {
				$('select[name^=penerima_id]').html(response)
			})
			.fail(function() {
				console.log("error");
			});
		});

		$(document).on('change', '[name^=jadwal]', function (e) {
			var count = e.target.files.length;
			if (count > 0) {
				$('.isi_jadwal').val(count+` Files Selected`);
			}
		});

		$(document).on('change', '[name^=surat_penawaran]', function (e) {
			var count = e.target.files.length;
			if (count > 0) {
				$('.isi_surat_penawaran').val(count+` Files Selected`);
			}
		});
	});
</script>