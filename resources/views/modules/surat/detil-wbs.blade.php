@extends('layouts.form')

@section('styles')
<style type="text/css">
	.responsive.table{
		width: 100%;
		overflow-x: auto;
	}
	.jus{
		text-align: justify;
		text-justify: inter-word;
	}
	.fn{
		font-size: 12px;
	}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		judul: ['empty'],
	};
</script>
@endsection

@section('content-body')
<div class="ui top demo tabular menu">
	<div class="active item fn tes" data-tab="first">Detil Order</div>
	<div class="item fn tes" data-tab="second">Jenis & Jadwal</div>
	<div class="item fn isi" data-tab="third">Surat Penawaran</div>
	<div class="item fn tes" data-tab="four">Riwayat Aktivitas</div>
</div>

<div class="ui bottom demo active tab" data-tab="first">
	<div class="two fields">
		<table class="ui compact table" style="border-radius: 0; margin: 0">
			<tr>
				<td width="250px"><label class="fn">Tgl Order</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">{{ DateToStringYear($data->first()->tgl_order) }}</td>
			</tr>
			<tr>
				<td width="250px" class="fn"><label>No Order</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">
					@if($no_order)
						@foreach($no_order as $cuy)
							<label>{{ $cuy }}</label>, 
						@endforeach
					@else
						{{ $data->first()->no_order }}
					@endif
				</td>
			</tr>
			<tr>
				<td width="250px" class="fn"><label>Layanan</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">{{ $data->first()->pelayanan->nama }}</td>
			</tr>
			<tr>
				<td><label class="fn">Lingkup</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $data->first()->lingkup->nama or '' }}</td>
			</tr>
			<tr>
				<td><label class="fn">Rencana Pelaksanaan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ BulanToString($data->first()->rencana) }}</td>
			</tr>
			<tr>
				<td><label class="fn">Kelas</label></td>
				<td class="fn">:</td>
				<td class="fn">@if($data->first()->kelas == '1')
					Reguler
					@else
					Prioritas
					@endif
				</td>
			</tr>
			<tr>
				<td><label class="fn">No Surat Permintaan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $data->first()->no_surat or '-' }}</td>
			</tr>
			<tr>
				<td><label class="fn">Tgl Surat Permintaan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ DateToStringYear($data->first()->tgl_surat) }}</td>
			</tr>
			<tr>
				<td><label class="fn">File Surat Permintaan</label></td>
				<td class="fn">:</td>
				<td class="fn">
					<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<a href="{{ url($pageUrl).'/download/'.$data->first()->id }}">
							<i class="download icon"></i></a>
					</div>
				</td>
			</tr>
			<tr>
				<td><label class="fn">Channel</label></td>
				<td class="fn">:</td>
				<td class="fn">@if($data->first()->tipe == 1)
					Online
					@elseif($data->first()->tipe == 2)
					AMS
					@else
					Manual
				@endif</td>
			</tr>
			<tr>
				<td><label class="fn">Peminta Jasa</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $data->first()->user->pelanggans->perusahaan->nama or '' }}</td>
			</tr>
			<tr>
				<td><label class="fn">Kategori</label></td>
				<td class="fn">:</td>
				<td class="fn">
					@if($data->first()->user->pelanggans->tipe_customer == 0)
					PLN
					@elseif($data->first()->user->pelanggans->tipe_customer == 1)
					NON PLN
					@else
					A-PLN
					@endif
				</td>
			</tr>
			<tr>
				<td><label class="fn">Dokumen Pendukung</label></td>
				<td class="fn">:</td>
				<td class="fn">
					@if($data->first())
					@if($data->first()->files)
					@if($data->first()->files->count() > 0)
					<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<a href="{{ url('download', $data->first()->id).'/pendaftaran-pengujian' }}">
							<i class="download icon"></i></a>
						</div>
						@endif
						@endif
						@endif
				</td>
			</tr>
			<tr>
				<td><label class="fn">Catatan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $data->first()->catatan or '-' }}
				</td>
			</tr>
			<tr>
				<td><label class="fn">Keputusan </label></td>
				<td class="fn">:</td>
				<td class="left aligned">
					<a class="ui green tag label fn">Diterima</a>
				</td>
			</tr>
		</table>
	</div>
<div class="actions">
	<div class="ui two column grid">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
	</div>
</div>
</div>
<div class="ui bottom demo tab" data-tab="second">
	<table id="tab1" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
		<thead>
			<tr class="middle aligned">
				<th width="10" class="center aligned fn">No.</th>
				<th width="200" class="center aligned fn">Jenis Pengujian</th>
				<th width="200" class="center aligned fn">Lokasi</th>
				<th width="150" class="center aligned fn">Mata Uji</th>
				<th width="250" class="center aligned fn">Spesifikasi</th>
				<th width="50" class="center aligned fn">Tarif</th>
				<th width="50" class="center aligned fn">Jumlah Benda Uji</th>
				<th width="150" class="center aligned fn">Jadwal Pengujian Tentative</th>
			</tr>
		</thead>
		<tbody class="detail fluid container detail">
			@php
			$no =1;
			@endphp
			@foreach($data as $kuy => $value)
			@if($value->kaji_ulang)
			@foreach($value->kaji_ulang->detail as $key => $vil)
			<tr class="detail fn">
				<td class="center aligned">{{ $no }}</td>
				<td class="left aligned">{{ $vil->detail_pendaftaran->jenis->nama }}</td>
				<td>
					{{ $vil->ket_lokasi }}
				</td>
				<td width="200" style="text-align: left;">
					<ul class="ui list">
					@if($vil->mata_uji)
					@foreach($vil->mata_uji as $key => $cek)
						<div class="items">{{$key+1}}. {{ $cek->mata_uji }}</div>
					@endforeach
					</ul>
					@else
					-
					@endif
				</td>
				<td  style=" text-align: justify;text-justify: inter-word;">
					{{$vil->spesifikasi or '-'}}
				</td>
				<td class="left aligned">
					{{$vil->tarif or '-'}}
				</td>
				<td class="right aligned">
					{{$vil->jumlah or '-'}}
				</td>
				<td class="center aligned">
					{{ DateToStringYear($vil->tentative_start) }} - {{ DateToStringYear($vil->tentative_end) }}
				</td>
			</tr>
			@php
			$no++;
			@endphp
			@endforeach
			@else
			<tr class="detail">
				<td class="center aligned fn" colspan="8">Tidak ada Data</td>
			</tr>
			@endif
			@endforeach
		</tbody>
	</table>
<div class="actions">
	<div class="ui two column grid">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
	</div>
</div>
</div>
<div class="ui bottom demo tab" data-tab="third">
	<form class="ui form" id="dataForm" action="{{ url($pageUrl.'updateWbs') }}" method="POST">
		{!! csrf_field() !!}
		@if($surat->count() > 0)
			@foreach($surat as $data_surat)
				<div class="ui yellow segment">
					<div class="field " style="width: 100%">
						<div class="ui form">
							<div class="four fields">
								<div class="field">
									<label>No Order</label>
									<input type="hidden" name="no_order" value="{{ $data_surat->kaji_ulang->pp->no_order }}">
									{{ $data_surat->kaji_ulang->pp->no_order }}
								</div>
								<div class="field">
									<label>No Surat Penawaran</label>
									<input type="hidden" name="no_surat" value="{{ $data_surat->kaji_ulang->pp->no_surat }}">
									{{ $data_surat->kaji_ulang->pp->no_surat }}
								</div>
								<div class="field">
									<label>Tgl Surat Penawaran</label>
									<input type="hidden" name="tgl_surat" value="{{ date("Y-m-d") }}">
									{{DateToStringYear(date("Y-m-d"))}}
								</div>
								<div class="field">
									<label>Adendum</label>
									<div class="field">
										<p style="text-align: justify;text-justify: inter-word;">
											@if($data_surat->adendum_status <= 0)
												Normal
											@else
												Adendum {{ $data_surat->adendum_status }}
											@endif</p>
									</div>
								</div>
								<div class="field">
									<label>No WBS / IO</label>
									@if($data_surat->wbs_io)
										{{ $data_surat->wbs_io }}
									@else
										<input type="hidden" name="detail[{{$data_surat->kaji_ulang->pp->id}}][surat_id]" value="{{$data_surat->id}}">
										<input type="text" name="detail[{{$data_surat->kaji_ulang->pp->id}}][wbs_io]" placeholder="No WBS / IO">
									@endif
								</div>
								<div class="field">
									<label>Jadwal Tetap</label>
									{{ DateToStringYear($data_surat->jadwal_start) }} - {{ DateToStringYear($data_surat->jadwal_end) }}
								</div>
							</div>
						</div>
						<div class="ui top attached">
							<a href="" class="ui red ribbon label">Komponen RAB</a>
							<table id="example" class="ui celled compact red table display" id="table-rab" width="100%" cellspacing="0">
								<thead>
									<tr class="center aligned">
										<th>No</th>
										<th>Item</th>
										<th>Nominal (Rp)</th>
									</tr>
								</thead>
								<tbody class="komponens">
									@foreach($data_surat->detail as $kiy => $cik)
									<tr class="data_komponens" data-id="1">
										<td class="center aligned numbor numboor-1" style="width: 50px;">{{ $kiy+1 }}</td>
										<td width="300px">
											<div class="ui transparent fluid input field">
												{{$cik->komponen}}
											</div>
										</td>
										<td width="200px">
											<div class="ui transparent fluid input field">
												Rp. {{ $cik->nominal }}
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			@endforeach
		@else
			<div class="ui red segment">
				<div class="field " style="width: 100%">
					<div class="ui form" style="text-align: center;">
						<label style="text-align: center;">Buat Surat Penawaran terlebih dahulu !!!</label>
					</div>
				</div>
			</div>
		@endif
	</form>
<div class="actions">
	<div class="ui two column grid">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		@if($surat->count() > 0)
			<div class="right aligned column">
				<br>
				<button id="simpan" type="button" class="ui large positive right labeled icon save as page button">
					Simpan
					<i class="save icon"></i>
				</button>
			</div>
		@else
		@endif
	</div>
</div>
</div>
<div class="ui bottom demo tab" data-tab="four">
	@foreach($data as $kiy => $cik)
		{!! riwayatPengujian($cik) !!}
		{!! riwayatAktivitas($cik) !!}
	@endforeach
<div class="actions">
	<div class="ui two column grid">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
	</div>
</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
</script>
@append