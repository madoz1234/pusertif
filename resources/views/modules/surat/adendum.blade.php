@extends('layouts.form')

@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
		.fn{
			font-size: 12px;
		}
	</style>
@append

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			judul: ['empty'],
		};
	</script>
@endsection

@section('content-body')
<div class="ui top demo tabular menu">
	<div class="active item fn tes" data-tab="first">Detil Order</div>
	<div class="item fn tes" data-tab="second">Jenis & Jadwal</div>
	<div class="item fn isi" data-tab="third">Surat Penawaran</div>
	<div class="item fn tes" data-tab="four">Riwayat Aktivitas</div>
</div>

<div class="ui bottom demo active tab" data-tab="first">
	<div class="two fields">
		<table class="ui compact table" style="border-radius: 0; margin: 0">
			<tr>
				<td width="250px"><label class="fn">Tgl Order</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">{{ DateToStringYear($data->first()->tgl_order) }}</td>
			</tr>
			<tr>
				<td width="250px" class="fn"><label>No Order</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">
					@if($no_order)
						@foreach($no_order as $cuy)
							<label>{{ $cuy }}</label>, 
						@endforeach
					@else
						{{ $data->first()->no_order }}
					@endif
				</td>
			</tr>
			<tr>
				<td width="250px" class="fn"><label>Layanan</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">{{ $data->first()->pelayanan->nama }}</td>
			</tr>
			<tr>
				<td><label class="fn">Lingkup</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $data->first()->lingkup->nama or '' }}</td>
			</tr>
			<tr>
				<td><label class="fn">Rencana Pelaksanaan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ BulanToString($data->first()->rencana) }}</td>
			</tr>
			<tr>
				<td><label class="fn">Kelas</label></td>
				<td class="fn">:</td>
				<td class="fn">@if($data->first()->kelas == '1')
					Reguler
					@else
					Prioritas
					@endif
				</td>
			</tr>
			<tr>
				<td><label class="fn">No Surat Permintaan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $data->first()->no_surat or '-' }}</td>
			</tr>
			<tr>
				<td><label class="fn">Tgl Surat Permintaan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ DateToStringYear($data->first()->tgl_surat) }}</td>
			</tr>
			<tr>
				<td><label class="fn">File Surat Permintaan</label></td>
				<td class="fn">:</td>
				<td class="fn">
					<div class="ui button preview" data-id="{{ $data->first()->id }}" data-tooltip="Lihat" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<i class="file image outline icon" style="margin-left: 0px;margin-right: 0px;"></i>
					</div>
					<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<a href="{{ url($pageUrl).'/download/'.$data->first()->id }}">
							<i class="download icon"></i></a>
					</div>
				</td>
			</tr>
			<tr>
				<td><label class="fn">Channel</label></td>
				<td class="fn">:</td>
				<td class="fn">@if($data->first()->tipe == 1)
					Online
					@elseif($data->first()->tipe == 2)
					AMS
					@else
					Manual
				@endif</td>
			</tr>
			<tr>
				<td><label class="fn">Peminta Jasa</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $data->first()->user->pelanggans->perusahaan->nama or '' }}</td>
			</tr>
			<tr>
				<td><label class="fn">Kategori</label></td>
				<td class="fn">:</td>
				<td class="fn">
					@if($data->first()->user->pelanggans->tipe_customer == 0)
					PLN
					@elseif($data->first()->user->pelanggans->tipe_customer == 1)
					NON PLN
					@else
					A-PLN
					@endif
				</td>
			</tr>
			<tr>
				<td><label class="fn">Dokumen Pendukung</label></td>
				<td class="fn">:</td>
				<td class="fn">
					@if($data->first())
					@if($data->first()->files)
					@if($data->first()->files->count() > 0)
					<div class="ui button preview_multiple" data-id="{{ $data->first()->id }}" data-tooltip="Lihat" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<i class="file image outline icon" style="margin-left: 0px;margin-right: 0px;"></i>
					</div>
					<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<a href="{{ url('download', $data->first()->id).'/pendaftaran-pengujian' }}">
							<i class="download icon"></i></a>
						</div>
						@endif
						@endif
						@endif
				</td>
			</tr>
			<tr>
				<td><label class="fn">Catatan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $data->first()->catatan or '-' }}
				</td>
			</tr>
			<tr>
				<td><label class="fn">Keputusan </label></td>
				<td class="fn">:</td>
				<td class="left aligned">
					<a class="ui tag label fn" style="background-color:#00abffcc;">DITERIMA</a>
				</td>
			</tr>
			<tr>
				<td><label class="fn">Keterangan </label></td>
				<td class="fn">:</td>
				<td class="left aligned fn">
					@if($data->first()->kaji_ulang)
						{{$data->first()->kaji_ulang->keterangan}}
					@else
					-
					@endif
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="ui bottom demo tab" data-tab="second">
	<table id="tab1" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
		<thead>
			<tr class="middle aligned">
				<th width="10" class="center aligned fn">No.</th>
				<th width="200" class="center aligned fn">Jenis Pengujian</th>
				<th width="150" class="center aligned fn">Merk</th>
				<th width="150" class="center aligned fn">Tipe</th>
				<th width="200" class="center aligned fn">Lokasi</th>
				<th width="200" class="center aligned fn">Mata Uji</th>
				<th width="150" class="center aligned fn">Spesifikasi</th>
				<th width="150" class="center aligned fn">Tarif</th>
				<th width="50" class="center aligned fn">Jumlah Benda Uji</th>
				<th width="150" class="center aligned fn">Jadwal Pengujian Tentative
					<br><div class="ui label">Rencana : {{ BulanToString($data->first()->rencana) }}</div>
				</th>
			</tr>
		</thead>
		<tbody class="detail fluid container detail">
			@php
				$no =1;
			@endphp
				@if($data)
					@foreach($data->first()->kaji_ulang->detail as $key => $vil)
						<tr class="detail fn">
							<td class="center aligned">{{ $no }}</td>
							<td style="text-align: left;">{{ $vil->detail_pendaftaran->jenis->nama }}</td>
							<td style="text-align: left;">{{ $vil->detail_pendaftaran->merk }}</td>
							<td style="text-align: left;">{{ $vil->detail_pendaftaran->tipe }}</td>
							<td>
								<div class="ui bulleted list">
									@foreach($vil->lokasi as $kiy => $val)
									<div class="item">
										<label class="isi">
											@if($val->jenis_lokasi == 1)
											In House :
											@else 
											On Site :
											@endif
										</label>
										{{ $val->ket_lokasi }}
									</div>
									@endforeach
								</div>
							</td>
							<td width="200" style="text-align: left;">
								@if($vil->mata_uji)
								<ul class="ui list">
									@foreach($vil->mata_uji as $key => $cek)
										<div class="items">
											<label>{{$key+1}}.&nbsp;&nbsp;{{ $cek->mata_uji }}</label>
										</div>
									@endforeach
								</ul>
								@else
								-
								@endif
							</td>
							<td>
								{{$vil->spesifikasi or '-'}}
							</td>
							<td class="left aligned">
								{{$vil->tarif or '-'}}
							</td>
							<td class="right aligned">
								{{ rtrim(rtrim(number_format($vil->jumlah,2,',','.'), '0'), ',')}}
								<label>{{$vil->detail_pendaftaran->satuan->satuan or '-'}}</label>
							</td>
							<td class="center aligned">
								{{ DateToStringYear($vil->tentative_start) }} - <br><label style="margin-left: -6px;">{{ DateToStringYear($vil->tentative_end) }}</label>
							</td>
						</tr>
						@php
						$no++;
						@endphp
					@endforeach
				@else
				<tr class="detail">
					<td class="center aligned fn" colspan="8">Tidak ada Data</td>
				</tr>
				@endif
		</tbody>
	</table>
</div>

<div class="ui bottom demo tab" data-tab="third">
	<form class="ui form" id="dataForm" action="{{ url($pageUrl.'saveAdendum') }}" method="POST">
	{!! csrf_field() !!}
		@php
			$user = auth()->user();
		@endphp
			@foreach($surat as $data_surat)
			<div class="ui yellow segment">
				<div class="field " style="width: 100%">
					<div class="ui form">
						@php 
							$angka = (int)$data_surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori;
						@endphp
						<div class="three fields">
							<input type="hidden" name="data[{{$data_surat->id}}][id]" value="{{ $data_surat->id }}">
							<input type="hidden" name="tipe_customer" value="{{ $data_surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori }}">
							<div class="field">
								<label>No Surat Penawaran</label>
								<input type="text" name="data[{{$data_surat->id}}][no_surat]" placeholder="No Surat Penawaran" value="{{$data_surat->no_surat}}" disabled="">
							</div>
							<div class="field">
								<label>Tgl Surat Penawaran</label>
								<div class="field">
									<div class="ui calendar labeled input tgl">
										<div class="ui input left icon">
											<i class="calendar icon date"></i>
											<input name="data[{{$data_surat->id}}][tgl_surat]" type="text" placeholder="Tgl Surat Penawaran" value="{{$data_surat->tgl_surat}}">
										</div>
									</div>
								</div>
							</div>
							<div class="field" @if($angka !== 0) style="display: none;" @endif>
								<label>Status</label>
								<div class="field">
									<input type="text" name="data[{{$data_surat->id}}][status]" placeholder="Status" readonly="" value="Adendum {{$data_surat->adendum_status + 1}}">
								</div>
							</div>
						</div>
						<div class="three fields">
							<div class="field" @if($angka !== 0) @else style="display: none;" @endif>
								<label>Status</label>
								<div class="field">
									<input type="text" name="data[{{$data_surat->id}}][status]" placeholder="Status" readonly="" value="Adendum {{$data_surat->adendum_status + 1}}">
								</div>
							</div>
							<div class="field"  @if($angka !== 0) style="display: none;" @endif>
								<label>Tgl SKKI/SKKO/PRK</label>
								<div class="field">
									<div class="ui calendar labeled input tgl">
										<div class="ui input left icon">
											<i class="calendar icon date"></i>
											<input name="data[{{$data_surat->id}}][tgl_skki]" type="text" placeholder="Tgl SKKI/SKKO/PRK" value="{{ $data_surat->tgl_skki }}">
										</div>
									</div>
								</div>
							</div>
							<div class="field" @if($angka !== 0) style="display: none;" @endif>
								<label>No SKKI/SKKO/PRK</label>
								<input type="text" name="data[{{$data_surat->id}}][no_skki]" placeholder="No SKKI/SKKO/PRK" value="{{ $data_surat->no_skki }}">
							</div>
							<div class="field">
								<label>Kelas</label>
								@if($data_surat->kaji_ulang->pp->kelas == '1')
								<input type="text" name="kelas" placeholder="Kelas" value="Reguler" readonly="">
								@else
								<input type="text" name="kelas" placeholder="Kelas" value="Prioritas" readonly="">
								@endif
							</div>
						</div>
						<div class="three fields">
							<div class="field">
								@if($angka !== 0)
									<label>Total Sesudah PPn</label>
								@else 
									<label>Total</label>
								@endif
								<input type="hidden" name="total">
								<input type="text" name="data[{{$data_surat->id}}][total]" placeholder="Total" readonly="">
							</div>
						</div>
					</div>	
					<div class="ui top attached segment">
						<a href="" class="ui violet ribbon label">Komponen RAB</a>
						<table id="example-{{ $data_surat->id }}" class="ui celled compact violet table display" id="table-rab" width="100%" cellspacing="0">
							<thead>
								<tr class="center aligned">
									<th>No</th>
									<th>Uraian</th>
									<th>Nominal (Rp)</th>
									<th width="50" class="center aligned">
										<button type="button" class="ui green mini icon tambah_komponen-{{ $data_surat->id }} button"><i class="plus icon"></i></button>
									</th>
								</tr>
							</thead>
							<tbody class="komponens-{{ $data_surat->id }}">
								@foreach($data_surat->detail as $key => $detail)
									<tr class="data_komponens-{{ $data_surat->id }}" data-id="{{ $detail->id }}">
										<td class="center aligned numbor numboor-{{ $detail->id }}" width="50px">{{ $key+1 }}</td>
										<td width="300px">
											<div class="ui transparent fluid input field">
												<input type="text" name="data[{{$data_surat->id}}][detail][{{ $detail->id }}][item]" placeholder="Item" value="{{ $detail->komponen }}">
											</div>
										</td>
										<td width="200px">
											<div class="ui transparent fluid input field">
												<input type="text" class="hitung-{{$data_surat->id}} number ambil-{{$data_surat->id}}" data-id="{{ $detail->id }}" name="data[{{$data_surat->id}}][detail][{{ $detail->id }}][nominal]" placeholder="Nominal" value="{{ rtrim(rtrim(number_format($detail->nominal,2,',','.'), '0'), ',')}}" data-inputmask="'alias' : 'numeric'" maxlength="16">
											</div>
										</td>
										<td class="center aligned">
											<button class="ui red mini icon button" data-angka="{{$data_surat->id}}"><i class="remove icon" data-angka="{{$data_surat->id}}"></i></button>
										</td>
									</tr>
								@endforeach
							</tbody>
							<tfoot>
								@if($angka !== 0)
									<tr>
										<td style="border-top: 1px solid rgba(34,36,38,.1);"></td>
										<td scope="row" style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);">Total Sebelum PPn</td>
										<td style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);" class="hasil-{{$data_surat->id}}"></td>
										<td style="border-top: 1px solid rgba(34,36,38,.1);"></td>
									</tr>
									<tr>
										<td style="border-top: none;"></td>
										<td scope="row" style="text-align: right;">PPn</td>
										<td style="text-align: right;" class="ppn-{{$data_surat->id}}"></td>
										<td style="border-top: none;"></td>
									</tr>
									<tr>
										<td style="border-top: none;"></td>
										<td scope="row" style="text-align: right;">Total Sesudah PPn</td>
										<td style="text-align: right;" class="total-{{$data_surat->id}}"></td>
										<td style="border-top: none;"></td>
									</tr>
								@else
									<tr>
										<td style="border-top: 1px solid rgba(34,36,38,.1);"></td>
										<td scope="row" style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);">Total</td>
										<td style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);" class="total-{{$data_surat->id}}"></td>
										<td style="border-top: 1px solid rgba(34,36,38,.1);"></td>
									</tr>
								@endif
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		@endforeach
	</form>
</div>
<div class="ui bottom demo tab" data-tab="four">
	@if($data->first()->act_dispo()->count() > 0)
		@php 
			$prev = $data->first()->logsyan()->first(); 
		@endphp
		@foreach($data->first()->act_dispo->sortByDesc('created_at') as $key => $cek)
			<div class="ui feed">
				<div class="event">
					<div class="label">
						<img src="{{ url(asset('img/avatar04.png')) }}">
					</div>
					<div class="content">
						<div class="date">
							{{$cek->created_at->diffForHumans()}}
						</div>
						<div class="summary">
							@php
								$Date1 	= $cek->created_at;
								$prev 	= $cek->prev();
							@endphp

							@if($cek->tipe == 0)
								<a>{{ $cek->pengirim->nama }}</a> mengubah data Layanan, Lingkup dan Jenis Pengujian
							@elseif($cek->tipe == 1)
								<a>{{ $cek->pengirim->nama }}</a>  Mengalihkan surat <a>{{ $data->first()->no_order }}</a> kepada <a>{{ $cek->penerima->nama }}</a> @if($cek->keterangan)<i>dengan catatan : {{ $cek->keterangan }}</i>@endif
							@elseif($cek->tipe == 2)
								<a>{{ $cek->pengirim->nama }}</a>  Mendisposisikan surat <a>{{ $data->first()->no_order }}</a> kepada <a>{{ $cek->penerima->nama }}</a> @if($cek->keterangan)<i>dengan catatan : {{ $cek->keterangan }}</i>@endif
								, dalam waktu &nbsp;&nbsp; {{ timestamp($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name) }}
							@elseif($cek->tipe == 3)
								@if($cek->status_kaji_ulang == 1)
									<a>{{ $cek->pengirim->nama }}</a> menyelesaikan <a>Kaji Ulang</a> dengan status <a>Diterima</a>, dalam waktu &nbsp;&nbsp; {{ timestamp($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name) }}
								@elseif($cek->status_kaji_ulang == 2)
									<a>{{ $cek->pengirim->nama }}</a> menyelesaikan <a>Kaji Ulang</a> dengan status <a>Ditolak</a>, dalam waktu &nbsp;&nbsp; {{ timestamp($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name) }}
								@elseif($cek->status_kaji_ulang == 3)
									<a>{{ $cek->pengirim->nama }}</a> menyelesaikan <a>Kaji Ulang</a> dengan status <a>Didiskusikan</a>, dalam waktu &nbsp;&nbsp; {{ timestamp($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name) }}
								@else
									<a>{{ $cek->pengirim->nama }}</a> menyelesaikan <a>Kaji Ulang</a> dengan status <a>Diterima</a>, dalam waktu &nbsp;&nbsp; {{ timestamp($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name) }}
								@endif
							@elseif($cek->tipe == 4)
								<a>{{ $cek->pengirim->nama }}</a> menyelesaikan <a>Surat Penawaran,</a> No Order <a>{{ $data->first()->no_order }}</a>@if($cek->status_surat_penawaran > 0) dengan <a> Adendum {{$cek->status_surat_penawaran}}</a> @endif
							@elseif($cek->tipe == 5)
								<a>{{ $cek->pengirim->nama }}</a> menyelesaikan <a> Virtual Account,</a> No Order <a>{{ $data->first()->no_order }}</a>@if($cek->status_surat_penawaran > 0) dengan <a> Adendum {{$cek->status_surat_penawaran}}</a> @endif dan mengirim email ke <a>{{ $data->first()->user->pelanggans->perusahaan->nama or '' }}</a>
							@else
							@endif
						</div>
					</div>
				</div>
			</div>
		@endforeach
		<div class="ui feed">
			<div class="event">
				<div class="label">
					<img src="{{ url(asset('img/avatar04.png')) }}">
				</div>
				<div class="content">
					<div class="date">
						{{$data->first()->created_at->diffForHumans()}}
					</div>
					<div class="summary">
						<a>Penerimaan Surat</a>
					</div>
				</div>
			</div>
		</div>
	@else
		<div class="ui feed">
			<div class="event">
				<div class="label">
					<img src="{{ url(asset('img/avatar04.png')) }}">
				</div>
				<div class="content">
					<div class="date">
						{{$data->first()->created_at->diffForHumans()}}
					</div>
					<div class="summary">
						<a>Penerimaan Surat</a>
					</div>
				</div>
			</div>
		</div>
	@endif
</div>
<div class="actions">
	<div class="ui two column grid">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		<div class="right aligned column">
			<br>
			<button id="simpan" style="display: none;" type="button" class="ui large positive right labeled icon save as page button">
				Simpan
				<i class="save icon"></i>
			</button>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
		$(document).ready(function(){
				$(document).on('click', '.preview', function(e){
					var id = $(this).data('id');
					var url = "{{ url($pageUrl) }}/"+id+"/preview";
					console.log(url)
					loadModal({
		                'url' : url,
		                'modal' : 'longer modal',
		                'formId' : '#dataForm',
		                'onShow' : function(){ 
		                    onShow();
		                },
		            })
				});

				$(document).on('click', '.preview_multiple', function(e){
					var id = $(this).data('id');
					var url = "{{ url($pageUrl) }}/"+id+"/preview-multiple/pendaftaran-pengujian";
					loadModal({
			            'url' : url,
			            'modal' : 'longer modal',
			            'formId' : '#dataForm',
			            'onShow' : function(){ 
			                onShow();
			            },
			        })
				});	
			});
		$('.hasil').html('0');
		$('.ppn').html('0');
		// $('.total').html('0');
		// $("input[name=cek]").val('0');
		@php
			$surat;
		@endphp
		var data = <?php echo json_encode($surat) ?>;
		$(data).each(function(key, data){
			var sum = 0;
			var ppn = 0;
			var pph = 0;
			var total = 0;

			$('.ambil-'+data['id']).each(function(){
				var angka = $(this).val();
				var angka_replace = angka.replace(/\./g, "");
				var zz = parseFloat(angka_replace);
				var nn = isNaN(parseInt(zz)) ? 0 : parseInt(zz)
				sum += nn;
			});

			var tipe = $("input[name=tipe_customer]").val();
			if(tipe == 0){
				ppn = 0;
			}else{
				ppn = ((sum/100)*10);
			}

			total = ((sum + ppn));
			$('.hasil-'+data['id']).html(formatMoney(sum));
			$('.ppn-'+data['id']).html(formatMoney(ppn));
			$('.total-'+data['id']).html(formatMoney(total));
			$('input[name="total['+data['id']+']"]').val(formatMoney(total));
			$('input[name="data['+data['id']+'][total]"]').val(formatMoney(total));

			$('.tgl').calendar({
				ampm: false,
				type: 'date',
			// maxDate: tanggal,
			formatter: {
				date: function (date, settings) {
					if (!date) return '';
					let momentDate = moment(date)
					return momentDate.format('YYYY-MM-DD')
				}
			}
			});


			$(".hitung-"+data['id']).keyup(function(e){
				var sum = 0;
				var ppn = 0;
				var pph = 0;
				var total = 0;

				$('.ambil-'+data['id']).each(function(){
					var angka = $(this).val();
					var angka_replace = angka.replace(/\./g, "");
					var zz = parseFloat(angka_replace);
					var nn = isNaN(parseInt(zz)) ? 0 : parseInt(zz)
					sum += nn;
				});

				var tipe = $("input[name=tipe_customer]").val();
				if(tipe == 0){
					ppn = 0;
				}else{
					ppn = ((sum/100)*10);
				}
				total = ((sum + ppn));
				$('.hasil-'+data['id']).html(formatMoney(sum));
				$('.ppn-'+data['id']).html(formatMoney(ppn));
				$('.total-'+data['id']).html(formatMoney(total));
				$('input[name="total['+data['id']+']"]').val(formatMoney(total));
				$('input[name="data['+data['id']+'][total]"]').val(formatMoney(total));
			});

			$('.jadwal_start-'+data['id']).calendar({
				type: 'date',
				formatter: {
					date: function (date, settings) {
						if (!date) return '';
						let momentDate = moment(date)
						return momentDate.format('YYYY-MM-DD')
					}
				},
				onChange: function () {
					startCalendar = $(this)
					var element = $(this).parents('div').find('div.jadwal_end-'+data['id']);
					element.calendar({
						type: 'date',
						startCalendar: startCalendar,
						formatter: {
							date: function (date, settings) {
								if (!date) return '';
								let momentDate = moment(date)
								return momentDate.format('YYYY-MM-DD')
							}
						}
					})
					$(this).parents('div').find('div.jadwal_end-'+data['id']).find('div').find('input').focus();
				}
			});
		});
		function formatMoney(angka) {
			var	number_string = angka.toString(),
			sisa 	= number_string.length % 3,
			rupiah 	= number_string.substr(0, sisa),
			ribuan 	= number_string.substr(sisa).match(/\d{3}/g);

			if (ribuan) {
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}

			return rupiah;
		};

		$(document).on('click', '.isi', function (e){
			$('#simpan').show();
		});

		$(document).on('click', '.tes', function (e){
			$('#simpan').hide();
		});

		$(".number").on("keypress keyup blur",function (e) {    
			$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
		});

		$(data).each(function(key, data){
			$(document).on('click', '.tambah_komponen-'+data['id'], function(e){
				var rowCount = $('#example-'+data['id']+' > tbody > tr').length;
				var c = rowCount-1;
				var html = `
					<tr class="data_komponens-`+data['id']+`" data-id="`+data['id']+`">
						<td class="center aligned numbor numboor-`+data['id']+`" width="50px">`+(c+2)+`</td>
						<td width="300px">
							<div class="ui transparent fluid input field">
							<input type="text" name="data[`+data['id']+`][detail][`+(c+2)+`][item]" placeholder="Item">
						</div>
						</td>
						<td width="200px">
							<div class="ui transparent fluid input field">
							<input type="text" class="max hitung-`+data['id']+` number ambil-`+data['id']+`" data-id="`+(c+2)+`" name="data[`+data['id']+`][detail][`+(c+2)+`][nominal]" data-inputmask="'alias' : 'numeric'" placeholder="Nominal">
						</div>
						</td>
						<td class="center aligned">
							<button class="ui red mini icon remove button"><i class="remove icon" data-angka="`+data['id']+`"></i></button>
						</td>
					</tr>`;

					$('.komponens-'+data['id']).append(html);
					// $(".number").on("keypress keyup blur",function (e) {    
			  //           $(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
			  //       });

					function formatMoney(angka) {
						var	number_string = angka.toString(),
						sisa 	= number_string.length % 3,
						rupiah 	= number_string.substr(0, sisa),
						ribuan 	= number_string.substr(sisa).match(/\d{3}/g);

						if (ribuan) {
							separator = sisa ? '.' : '';
							rupiah += separator + ribuan.join('.');
						}

						return rupiah;
					};
					
					$(".hitung-"+data['id']).keyup(function(e){
						var sum = 0;
						var ppn = 0;
						var pph = 0;
						var total = 0;

						$('.ambil-'+data['id']).each(function(){
							var angka = $(this).val();
							var angka_replace = angka.replace(/\./g, "");
							var zz = parseFloat(angka_replace);
							var nn = isNaN(parseInt(zz)) ? 0 : parseInt(zz)
							sum += nn;
						});

						var tipe = $("input[name=tipe_customer]").val();
						if(tipe == 0){
							ppn = 0;
						}else{
							ppn = ((sum/100)*10);
						}

						total = ((sum + ppn));
						$('.hasil-'+data['id']).html(formatMoney(sum));
						$('.ppn-'+data['id']).html(formatMoney(ppn));
						$('.total-'+data['id']).html(formatMoney(total));
						$('input[name="total['+data['id']+']"]').val(formatMoney(total));
						$('input[name="data['+data['id']+'][total]"]').val(formatMoney(total));
					});

					$(".max").attr('maxlength','16');

					reloadMask();
			});
		});

	var num_tindakan = 1;
	$(document).on('click', '.remove', function (e){
		var sum = 0;
		var ppn = 0;
		var pph = 0;
		var total = 0;

		var id = $(this).data('angka');
		var row = $(this).closest('tr');
		row.remove();
		var table = $('#example-'+id);
		var rows = table.find('tbody tr');
		$.each(rows, function(key, value){
			table.find('.numboor-'+$(this).data("id")).html(key+1);
		});

		$('.ambil-'+id).each(function(){
			var angka = $(this).val();
			var angka_replace = angka.replace(/\./g, "");
			var zz = parseFloat(angka_replace);
			var nn = isNaN(parseInt(zz)) ? 0 : parseInt(zz);
			sum += nn;
		});

		var tipe = $("input[name=tipe_customer]").val();
		if(tipe == 0){
			ppn = 0;
		}else{
			ppn = ((sum/100)*10);
		}

		total = ((sum + ppn));
		$('.hasil-'+id).html(formatMoney(sum));
		$('.ppn-'+id).html(formatMoney(ppn));
		$('.total-'+id).html(formatMoney(total));
		$('input[name="total['+id+']"]').val(formatMoney(total));
		$('input[name="data['+id+'][total]"]').val(formatMoney(total));
	});
</script>
@append
@include('scripts.inputmask')