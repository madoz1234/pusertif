@extends('layouts.list-surat')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('scripts')
    <script type="text/javascript">
        $(document).on('click','.kirim-surat',function(){
            swal({
            title: 'Apakah anda yakin?',
            text: "Kirim Surat Penawaran!",
            type: 'warning',
            showCancelButton: true, 
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then((result) => {
            $('#lampiran-area').html(`<div class="ui active inverted dimmer">
                <div class="ui text loader">Loading</div>
                </div>`);
            if (result) {
               
                swal(
                    'Terkirim!',
                    'Surat berhasil dikirim.',
                    'success'
                    )
                          
            }else{
                swal(
                'Gagal!',
                'Surat gagal dikirim.',
                'error'
                )
            }
        })
        });
    </script>
@endsection

@section('filters')
{{  csrf_field() }}
<div class="field">
	<input name="no_order" placeholder="No Order" type="text" value="{{ isset($filter) ? $filter['no_order'] : '' }}">
</div>
<div class="field">
    <div class="ui left icon date input">
        <i class="calendar icon"></i>
        <input type="text" name="tanggal_order" placeholder="Tanggal Order" value="{{ isset($filter) ? $filter['tanggal_order'] : '' }}">
    </div>
</div>
<div class="field">
  <input type="text" name="no_surat" placeholder="No Surat Permintaan" value="{{ isset($filter) ? $filter['no_surat'] : '' }}">
</div>
<div class="field">
  <select name="jenis_pelayanan_id" class="watcher ui fluid search dropdown">
    {!! \App\Models\Master\JenisPelayanan::options('nama', 'id', [
    	'selected' => isset($filter) ? $filter['jenis_pelayanan_id'] : ''
    	], 'Pilih Layanan') !!}
  </select>
</div>
<div class="field">
  <select name="perusahaan_id" class="watcher ui fluid search dropdown">
    {!!
      \App\Models\Master\Perusahaan::optionsa('nama', 'id', ['filters' => [function($q){
        return $q->where('status', 1);
      }],
      'selected' => isset($filter) ? $filter['perusahaan_id'] : ''
    ], 'Nama Perusahaan')
    !!}
  </select>
</div>
<button type="submit" class="ui teal icon button" data-tooltip="Cari Data">
    <i class="search icon"></i>
</button>
<a href="{{ url($pageUrl) }}" class="ui icon reset button" data-tooltip="Bersihkan Pencarian">
    <i class="refresh icon"></i>
</a>

@endsection

@section('js-filters')
d.display_name = $("input[name='filter[display_name]']").val();
d.name = $("input[name='filter[name]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
	    username: 'empty',
	    email: 'empty',
	    roles: 'empty',
	};
</script>
@endsection

@section('toolbars')

@endsection

@section('subcontent')
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="ui attached">
    <div class="ui top demo tabular menu">
      <div class="active item" data-tab="Order">On Progress @if(count($data) > 0)<span style="background-color:red;"class="ui circular label">{{ count($data) }}</span>@endif</div>
      <div class="item" data-tab="Kirim">Kirim Surat @if(count($data_kirim) > 0)<span style="background-color:red;"class="ui circular label">{{ count($data_kirim) }}</span>@endif</div>
      <div class="item" data-tab="Penolakan">Surat Penolakan @if(count($data_tolak) > 0)<span style="background-color:red;"class="ui circular label">{{ count($data_tolak) }}</span>@endif</div>
      <div class="item" data-tab="History">Historis </div>
    </div>
    <div class="ui active tab" data-tab="Order">
        <table id="tab1" class="ui celled compact red table display" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th class="center aligned">No</th>
                    <th class="center aligned">No Order</th>
                    <th class="center aligned" width="150">Tgl Order</th>
                    <th class="center aligned">No Surat Permintaan</th>
                    <th class="center aligned" width="200">Layanan / Lingkup</th>
                    <th width="200" class="center aligned">Jenis Pengujian</th>
                    <th class="center aligned">Kelas</th>
                    <th class="center aligned" width="250">Peminta Jasa</th>
                    <th class="center aligned" width="50">Aksi</th>
                    <th class="center aligned" width="150">Timestamp</th>
                    <th class="center aligned" width="200">Status</th>
                </tr>
            </thead>
            <tbody>
            	@php
            		$num = 1;
            	@endphp
		    	@foreach($data as $key => $cek)
			    	@php
			    	$cek_data = $cek->first()->cekDone($cek->first()->id);
			    	@endphp
	                <tr>
	                    <td class="center aligned">{{ $num++ }}</td>
	                    <td class="center aligned">
	                    	@if($cek->count() > 1)
		                    	@foreach($cek as $kuy => $cuk)
		                    		@if($cuk->kaji_ulang)
		                    			<label>{{ $cuk->no_order }} </label></br>
		                    		@else
			                    		@if($cuk->act_dispo->max('jenis') == 1)
			                    			<label style="color:red;">{{ $cuk->no_order }} </label><span data-tooltip="Mohon selesaikan dulu tahap Penerimaan Surat" data-position="top center"><i class="question circle outline red icon"></i></span></br>
			                    		@else 
			                    			<label style="color:red;">{{ $cuk->no_order }} </label><span data-tooltip="Mohon selesaikan dulu tahap Kaji Ulang" data-position="top center"><i class="question circle outline red icon"></i></span></br>
			                    		@endif
		                    		@endif
		                    	@endforeach
	                    	@else
	                    		@foreach($cek as $kuy => $cik)
	                    			@if($cik->kaji_ulang)
		                    			<label>{{ $cik->no_order }} </label></br>
		                    		@else
			                    		@if($cik->act_dispo->max('jenis') == 1)
			                    			<label style="color:red;">{{ $cik->no_order }} </label><span data-tooltip="Mohon selesaikan dulu tahap Penerimaan Surat" data-position="top center"><i class="question circle outline red icon"></i></span></br>
			                    		@else 
			                    			<label style="color:red;">{{ $cik->no_order }} </label><span data-tooltip="Mohon selesaikan dulu tahap Kaji Ulang" data-position="top center"><i class="question circle outline red icon"></i></span></br>
			                    		@endif
		                    		@endif
	                    		@endforeach
	                    	@endif
	                    </td>
	                    <td class="center aligned">{{ DateToStringYear($cek->first()->tgl_order) }}</td>
	                    <td class="center aligned">{{ $cek->first()->no_surat }}</td>
	                    <td class="left aligned">
							{{$cek->first()->pelayanan->nama}} </br>  {{$cek->first()->lingkup->nama}}
	                    </td>
	                    <td class="left aligned">
	                    	@if($cek->count() > 0 && $cek->first()->layanan_id == 3)
	                    		<div class="ui checkbox">
	                    			<input class="check_all"  data-id="{{$cek->first()->nomor}}" type="checkbox" name="check_all">
	                    			<label>Check All</label>
	                    		</div>
		                    	<div class="ui list list-more1" data-display="3">
		                    		@foreach($cek as $kuy => $cuk)
			                    		@if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi']))
				                    		@if($cuk->kaji_ulang)
					                    		@foreach($cuk->detail as $kay => $data_jenis)
					                    			@if($data_jenis->pp->kaji_ulang->surat)
					                    				<div class="item">
					                    					<span><i class="check circle outline green icon"></i></span>
					                    					<input type="hidden" name="kirim" class="kirim-{{$cuk->nomor}}"  data-id="{{$cuk->id}}">
					                						<label><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cuk->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a>
					                						</label>
							                    		</div>
							                    	@else
							                    		<div class="item">
							                    			<div class="ui checkbox">
						                						<input class="kepala-{{$cuk->nomor}}"  data-id="{{$cuk->id}}" type="checkbox" name="pilihan">
						                						<label><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cuk->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a>
						                						</label>
								                    		</div>
							                    		</div>
							                    	@endif
					                    		@endforeach
					                    	@else
					                    		@foreach($cuk->detail as $data_jenis)
					                    			<div class="item">
					                    				{{$kuy+1}}.&nbsp;&nbsp;{{ $data_jenis->jenis->nama }}
						                    		</div>
					                    		@endforeach
				                    		@endif
				                    	@else
				                    		@if($cuk->kaji_ulang)
					                    		@foreach($cuk->detail as $data_jenis)
					                    			@if($data_jenis->pp->kaji_ulang->surat)
					                    				<div class="item">
					                    					<span><i class="check circle outline green icon"></i></span>
					                    					<input type="hidden" name="kirim" class="kirim-{{$cuk->nomor}}"  data-id="{{$cuk->id}}">
					                						<label><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cuk->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a>
					                						</label>
							                    		</div>
							                    	@else
							                    		<div class="item">
							                    			<div class="item">
							                    				<span><i class="close outline red icon"></i></span>
							                    				<input type="hidden" name="kirim" class="kirim-{{$cuk->nomor}}"  data-id="{{$cuk->id}}">
							                    				<label><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cuk->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a>
							                    				</label>
							                    			</div>
							                    		</div>
							                    	@endif
					                    		@endforeach
					                    	@else
					                    		@foreach($cuk->detail as $data_jenis)
					                    			<div class="item">
					                    				{{$kuy+1}}.&nbsp;&nbsp;{{ $data_jenis->jenis->nama }}
						                    		</div>
					                    		@endforeach
				                    		@endif
				                    	@endif
		                    		@endforeach
		                    	</div>
	                    	@else
		                    	<div class="ui list list-more1" data-display="3">
		                    		@foreach($cek as $kiy => $cok)
			                    		@if($cok->kaji_ulang)
				                    		@foreach($cok->detail as $kiy => $data_jenis)
				                    			<div class="item">{{$kiy+1}}.&nbsp;&nbsp;<a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cok->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a></div>
				                    		@endforeach
				                    	@else
				                    		@foreach($cok->detail as $kiy => $data_jenis)
				                    			<div class="item">{{$kiy+1}}.&nbsp;&nbsp;{{ $data_jenis->jenis->nama }}</div>
				                    		@endforeach
			                    		@endif
		                    		@endforeach
		                    	</div>
	                    	@endif
	                    </td>
	                    <td class="center aligned">
	                    	@if($cek->first()->kelas == 1)
	                    		Reguler
	                    	@else 
	                    		Prioritas
	                    	@endif
	                    </td>
	                    <td class="center aligned">{{ $cek->first()->user->pelanggans->perusahaan->nama }} </br> {{ $cek->first()->user->pelanggans->nama_lengkap }} </br> {{ $cek->first()->user->pelanggans->no_hp }} </td>
	                    <td class="center aligned">
	                    	@if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi']))
		                    	@if($cek_data['jumlah'] > 0)
		                    		@if($cek_data['exist'])
			            				@if($cek_data['surat'] > 0)
			            					<a data-position="left center" data-tooltip='Buat Surat Penawaran' data-nomor='{{ $cek->first()->nomor }}' data-id='{{ $cek->first()->id }}' data-tipe='{{ $cek->first()->layanan_id }}' class='ui mini blue icon baru button'><i class="plus icon"></i></a>
			            				@endif
		                    		@else
	            						<a data-position="left center" data-tooltip='Buat Surat Penawaran' data-nomor='{{ $cek->first()->nomor }}' data-id='{{ $cek->first()->id }}' data-tipe='{{ $cek->first()->layanan_id }}' class='ui mini blue icon baru button'><i class="plus icon"></i></a>
		                    		@endif
		                    	@else
		                    		-
		                    	@endif
	                    	@else
	                    		-
	                    	@endif
	                    </td>
	                    @php
	                    	$time = $cek->first()->updated_at;
	                    	$time = \Carbon\Carbon::parse($time)->addDays(getHariKerja(url($pageUrl),$cek->first()->layanan_id));
	                    @endphp
	                    <td class="center aligned timestamp">{{ $time->format('Y-m-d H:i:s') }}</td>
	                    <td class="center aligned"><label class="ui red tag label"> Menunggu Surat Penawaran</label></td>
	                </tr>
		    	@endforeach
            </tbody>
        </table>
    </div>
    <div class="ui bottom attached tab" data-tab="Kirim">
        <table id="tab2" class="ui celled compact red table display" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th class="center aligned">No</th>
                    <th class="center aligned">No Order</th>
                    <th class="center aligned" width="150">Tgl Order</th>
                    <th class="center aligned">No Surat Permintaan</th>
                    <th class="center aligned" width="200px">Layanan / Lingkup</th>
                    <th width="150" class="center aligned">Jenis Pengujian</th>
                    <th class="center aligned">Kelas</th>
                    <th class="center aligned" width="250px">Peminta Jasa</th>
                    <th class="center aligned">No Surat Penawaran</th>
                    <th class="center aligned">No VA</th>
                    <th class="center aligned">Detil</th>
                    <th class="center aligned">Aksi</th>
                    <th class="center aligned" width="150px">Timestamp</th>
                    <th class="center aligned" width="150px">Status</th>
                </tr>
            </thead>
            <tbody>
            	@php
            		$num = 1;
            	@endphp
		    	@foreach($data_kirim as $key => $cek)
			    	@php
			    	$cek_data = $cek->first()->cekDone($cek->first()->id);
			    	@endphp
	                <tr>
	                    <td class="center aligned">{{ $num++ }}</td>
	                    <td class="center aligned">
	                    	@if($cek->count() > 1)
		                    	@foreach($cek as $kuy => $cuk)
		                    		@if($cuk->kaji_ulang)
		                    			<label>{{ $cuk->no_order }} </label></br>
		                    		@else
			                    		@if($cuk->act_dispo->max('jenis') == 1)
			                    			<label style="color:red;">{{ $cuk->no_order }} </label><span data-tooltip="Mohon selesaikan dulu tahap Penerimaan Surat" data-position="top center"><i class="question circle outline red icon"></i></span></br>
			                    		@else 
			                    			<label style="color:red;">{{ $cuk->no_order }} </label><span data-tooltip="Mohon selesaikan dulu tahap Kaji Ulang" data-position="top center"><i class="question circle outline red icon"></i></span></br>
			                    		@endif
		                    		@endif
		                    	@endforeach
	                    	@else
	                    		@foreach($cek as $kuy => $cik)
	                    			@if($cik->kaji_ulang)
		                    			<label>{{ $cik->no_order }} </label></br>
		                    		@else
			                    		@if($cik->act_dispo->max('jenis') == 1)
			                    			<label style="color:red;">{{ $cik->no_order }} </label><span data-tooltip="Mohon selesaikan dulu tahap Penerimaan Surat" data-position="top center"><i class="question circle outline red icon"></i></span></br>
			                    		@else 
			                    			<label style="color:red;">{{ $cik->no_order }} </label><span data-tooltip="Mohon selesaikan dulu tahap Kaji Ulang" data-position="top center"><i class="question circle outline red icon"></i></span></br>
			                    		@endif
		                    		@endif
	                    		@endforeach
	                    	@endif
	                    </td>
	                    <td class="center aligned">{{ DateToStringYear($cek->first()->tgl_order) }}</td>
	                    <td class="center aligned">{{ $cek->first()->no_surat }}</td>
	                    <td class="left aligned">
							{{$cek->first()->pelayanan->nama}} </br>  {{$cek->first()->lingkup->nama}}
	                    </td>
	                    <td class="left aligned">
	                    	@if($cek->count() > 1)
	                    	<div class="ui list list-more2" data-display="3">
	                    		@foreach($cek as $kuy => $cuk)
		                    		@if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi']))
			                    		@if($cuk->kaji_ulang)
				                    		@foreach($cuk->detail as $data_jenis)
					                    		<div class="items">
					                    			<div class="ui checkbox">
					                    				<input class="cek_data-{{$cuk->nomor}}"  data-id="{{$cuk->id}}" type="checkbox" name="pilihan">
					                    					<label><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cuk->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a>
					                    				</label>
						                    		</div>
					                    		</div>
				                    		@endforeach
				                    	@else
				                    		@foreach($cuk->detail as $data_jenis)
				                    			<div class="items">
				                    				{{$kuy+1}}.&nbsp;&nbsp;{{ $data_jenis->jenis->nama }}
					                    		</div>
				                    		@endforeach
			                    		@endif
			                    	@else
			                    		@if($cuk->kaji_ulang)
				                    		@foreach($cuk->detail as $data_jenis)
				                    			@if($data_jenis->pp->kaji_ulang->surat)
				                    				<div class="items">
				                    					<span><i class="check circle outline green icon"></i></span>
				                    					<input type="hidden" name="kirim" class="kirim-{{$cuk->nomor}}"  data-id="{{$cuk->id}}">
				                						<label><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cuk->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a>
				                						</label>
						                    		</div>
						                    	@else
						                    		<div class="items">
						                    			<div class="items">
						                    				<span><i class="close outline red icon"></i></span>
						                    				<input type="hidden" name="kirim" class="kirim-{{$cuk->nomor}}"  data-id="{{$cuk->id}}">
						                    				<label><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cuk->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a>
						                    				</label>
						                    			</div>
						                    		</div>
						                    	@endif
				                    		@endforeach
				                    	@else
				                    		@foreach($cuk->detail as $data_jenis)
				                    			<div class="items">
				                    				{{$kuy+1}}.&nbsp;&nbsp;{{ $data_jenis->jenis->nama }}
					                    		</div>
				                    		@endforeach
			                    		@endif
			                    	@endif
	                    		@endforeach
	                    	</div>
	                    	@else
	                    	<div class="ui list list-more2" data-display="3">
	                    		@foreach($cek as $kiy => $cok)
		                    			@if($cok->layanan_id == 3)
		                    				@if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi']))
					                    		@if($cok->kaji_ulang)
						                    		@foreach($cok->detail as $data_jenis)
							                    		<div class="items">
							                    			<div class="ui checkbox">
						                						<input class="cek_data-{{$cok->nomor}}"  data-id="{{$cok->id}}" type="checkbox" name="pilihan">
						                						<label><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cok->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a>
						                						</label>
								                    		</div>
							                    		</div>
						                    		@endforeach
						                    	@else
						                    		@foreach($cok->detail as $data_jenis)
						                    			<div class="items">
						                    				{{$kuy+1}}.&nbsp;&nbsp;{{ $data_jenis->jenis->nama }}
							                    		</div>
						                    		@endforeach
					                    		@endif
					                    	@else
					                    		@if($cok->kaji_ulang)
						                    		@foreach($cok->detail as $data_jenis)
						                    			@if($data_jenis->pp->kaji_ulang->surat)
						                    				<div class="items">
						                    					<span><i class="check circle outline green icon"></i></span>
						                    					<input type="hidden" name="kirim" class="kirim-{{$cok->nomor}}"  data-id="{{$cok->id}}">
						                						<label><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cok->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a>
						                						</label>
								                    		</div>
								                    	@else
								                    		<div class="items">
								                    			<div class="items">
								                    				<span><i class="close outline red icon"></i></span>
								                    				<input type="hidden" name="kirim" class="kirim-{{$cok->nomor}}"  data-id="{{$cok->id}}">
								                    				<label><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cok->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a>
								                    				</label>
								                    			</div>
								                    		</div>
								                    	@endif
						                    		@endforeach
						                    	@else
						                    		@foreach($cok->detail as $data_jenis)
						                    			<div class="items">
						                    				{{$kuy+1}}.&nbsp;&nbsp;{{ $data_jenis->jenis->nama }}
							                    		</div>
						                    		@endforeach
					                    		@endif
					                    	@endif
		                    			@else
				                    		@if($cok->kaji_ulang)
					                    		@foreach($cok->detail as $kiy => $data_jenis)
						                    		<div class="item">{{$kiy+1}}.&nbsp;&nbsp;<a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cok->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a></div>
					                    		@endforeach
					                    	@else
					                    		@foreach($cok->detail as $kiy => $data_jenis)
					                    			<div class="item">{{$kiy+1}}.&nbsp;&nbsp;{{ $data_jenis->jenis->nama }}</div>
					                    		@endforeach
				                    		@endif
		                    			@endif
	                    		@endforeach
	                    	</div>
	                    	@endif
	                    </td>
	                    <td class="center aligned">
	                    	@if($cek->first()->kelas == 1)
	                    		Reguler
	                    	@else 
	                    		Prioritas
	                    	@endif
	                    </td>
	                    <td class="center aligned">{{ $cek->first()->user->pelanggans->perusahaan->nama }} </br> {{ $cek->first()->user->pelanggans->nama_lengkap }} </br> {{ $cek->first()->user->pelanggans->no_hp }} </td>
	                    <td class="center aligned">{{ $key }}</td>
	                    <td class="center aligned">
	                    	@foreach($cek as $data_va)
	                    		@if($data_va->kaji_ulang->surat->va)
	                    			@if($data_va->user->pelanggans->perusahaan->kategori == 0)
	                    				- </br>
	                    			@else
		                    			{{$data_va->kaji_ulang->surat->va->no_va}} </br>
	                    			@endif
	                    		@else
	                    			- </br>
	                    		@endif
	                    	@endforeach
	                    </td>
	                    <td class="center aligned">
	                    	@if($cek_data['exist'])
	                    		@if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi']))
		                    		<a data-position="left center" data-tooltip='Detil' data-id='{{ $cek->first()->id }}' data-nomor='{{ $cek->first()->nomor }}' data-tipe='{{ $cek->first()->layanan_id }}' class='ui mini teal icon data_detail button'><i class="eye icon"></i></a>
		                    	@else
		                    		-
		                    	@endif
	                    	@else 
	                    		-
	                    	@endif
	                    </td>
	                    <td class="center aligned" width="150px;">
	                    	@if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi']))
		                    	@if($cek_data['jumlah'] > 0)
		                    		@if($cek_data['exist'])
		                    			<a data-position="left center" data-tooltip='Ubah Surat Penawaran' data-nomor='{{ $cek->first()->nomor }}' data-id='{{ $cek->first()->id }}' data-tipe='{{ $cek->first()->layanan_id }}' class='ui mini blue icon data_ubah button'><i class="edit icon"></i></a>

		                    			<a data-position="left center" data-tooltip='Buat Virtual Account' data-id='{{ $cek->first()->id }}' data-tipe='{{ $cek->first()->layanan_id }}' data-nomor='{{ $cek->first()->nomor }}' class='ui mini green icon data_virtual button'><i class="money icon"></i></a>

		            					<a data-position="left center" data-tooltip='Kirim Surat Penawaran' data-id='{{ $cek->first()->id }}' data-tipe='{{ $cek->first()->layanan_id }}' data-nomor='{{ $cek->first()->nomor }}' class='ui mini teal icon data_kirim button'><i class="mail icon"></i></a>
		                    		@else
			                    		-
		                    		@endif
		                    	@else
		                    		-
		                    	@endif
	                    	@elseif(auth()->user()->hasRole(['keuangan']))
	                    		@if($cek_data['nilai'] == $cek_data['cek_jumlah'])
	                    			-
	                    		@else 
		                    		<a data-position="left center" data-tooltip='Ubah Surat Penawaran' data-nomor='{{ $cek->first()->nomor }}' data-id='{{ $cek->first()->id }}' data-tipe='{{ $cek->first()->layanan_id }}' class='ui mini blue icon wbs button'><i class="edit icon"></i></a>
	                    		@endif
	                    	@else
	                    		-
	                    	@endif
	                    </td>
	                    @php
	                    	$time = $cek->first()->updated_at;
	                    	$time = \Carbon\Carbon::parse($time)->addDays(getHariKerja(url($pageUrl),$cek->first()->layanan_id));
	                    @endphp
	                    <td class="center aligned">{{ $time->format('Y-m-d H:i:s') }}</td>
	                    <td class="center aligned"><label class="ui blue tag label">Menunggu Surat Penawaran Terkirim</label></td>
	                </tr>
		    	@endforeach
            </tbody>
        </table>
    </div>
    <div class="ui bottom attached tab" data-tab="Penolakan">
        <table id="tab2" class="ui celled compact red table display" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th class="center aligned">No</th>
                    <th class="center aligned">No Order</th>
                    <th class="center aligned" width="150">Tgl Order</th>
                    <th class="center aligned">No Surat Permintaan</th>
                    <th class="center aligned" width="200px">Layanan / Lingkup</th>
                    <th width="150" class="center aligned">Jenis Pengujian</th>
                    <th class="center aligned">Kelas</th>
                    <th class="center aligned" width="250px">Peminta Jasa</th>
                    <th class="center aligned">Aksi</th>
                </tr>
            </thead>
            <tbody>
            	@php
            		$num = 1;
            	@endphp
            	@if($data_tolak->count() > 0)
			    	@foreach($data_tolak as $key => $tolak)
				    	<tr>
					    	<td class="center aligned">{{ $key+1 }}</td>
					    	<td class="center aligned">{{ $tolak->no_order }}</td>
					    	<td class="center aligned">{{ DateToStringYear($tolak->tgl_order) }}</td>
					    	<td class="center aligned">{{ $tolak->no_surat }}</td>
					    	<td class="left aligned">
								{{$tolak->pelayanan->nama}} </br>  {{$tolak->lingkup->nama}}
		                    </td>
					    	<td class="left aligned">
					    		<div class="ui list list-more2" data-display="3">
		                    		@if($tolak->kaji_ulang)
			                    		@foreach($tolak->detail as $kiy => $data_jenis)
			                    			<div class="item">{{$kiy+1}}.&nbsp;&nbsp;<a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $tolak->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a></div>
			                    		@endforeach
			                    	@else
			                    		@foreach($tolak->detail as $kiy => $data_jenis)
			                    			<div class="item">{{$kiy+1}}.&nbsp;&nbsp;{{ $data_jenis->jenis->nama }}</div>
			                    		@endforeach
		                    		@endif
		                    	</div>
					    	</td>
					    	<td class="center aligned">
		                    	@if($tolak->kelas == 1)
		                    		Reguler
		                    	@else 
		                    		Prioritas
		                    	@endif
		                    </td>
					    	<td class="center aligned">{{ $tolak->user->pelanggans->perusahaan->nama }} </br> {{ $tolak->user->pelanggans->nama_lengkap }} </br> {{ $tolak->user->pelanggans->no_hp }} </td>
					    	<td class="center aligned">
					    		@if($tolak->kaji_ulang->bukti)
						    		<div class="ui button preview_penolakan" data-id="{{ $tolak->kaji_ulang->id }}" data-tooltip="Lihat" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						    			<i class="file image outline icon" style="margin-left: 0px;margin-right: 0px;"></i>
						    		</div>
						    		<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						    			<a href="{{ url($pageUrl).'/download-penolakan/'.$tolak->kaji_ulang->id }}">
						    				<i class="download icon"></i></a>
						    		</div>
					    		@else
						    		<div class="ui small basic icon buttons">
										<button type="button" class="ui green upload_penolakan button" data-id="{{ $tolak->kaji_ulang->id }}" data-tooltip="Upload Laporan Penolakan" data-position="top center"><i class="upload icon"></i></button>
									</div>
					    		@endif
					    	</td>
				    	</tr>
			    	@endforeach
			    @else
			    	<tr>
			    		<td colspan="9" style="text-align: center;">
			    			Tidak Ada Data
			    		</td>
			    	</tr>
			    @endif
            </tbody>
        </table>
    </div>
    <div class="ui bottom attached tab" data-tab="History">
       <table id="tab3" class="ui celled compact red table display" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th class="center aligned">No</th>
                    <th class="center aligned" width="100px">No Order</th>
                    <th class="center aligned" width="150">Tgl Order</th>
                    <th class="center aligned">No Surat Permintaan</th>
                    <th class="center aligned" width="200px">Layanan / Lingkup</th>
                    <th width="150" class="center aligned">Jenis Pengujian</th>
                    <th class="center aligned">Kelas</th>
                    <th class="center aligned" width="250px">Peminta Jasa</th>
                    <th class="center aligned" width="150px">No Surat Penawaran</th>
                    <th class="center aligned">Tgl Surat Penawaran</th>
                    <th class="center aligned" width="150px">Adendum</th>
                    <th class="center aligned">Detil</th>
                    <th class="center aligned">File Surat Penawaran</th>
                    <th class="center aligned" width="150px">Status</th>
                </tr>
            </thead>
            <tbody>
            	@php
            		$num = 1;
            	@endphp
		    	@foreach($histori as $key => $cek)
			    	@php
			    	$cek_data = $cek->first()->cekDone($cek->first()->id);
			    	@endphp
	                <tr>
	                    <td class="center aligned">{{ $num++ }}</td>
	                    <td class="center aligned">
	                    	@if($cek->count() > 1)
		                    	@foreach($cek as $kuy => $cuk)
		                    		@if($cuk->kaji_ulang)
		                    			<label>{{ $cuk->no_order }} </label></br>
		                    		@else
			                    		@if($cuk->act_dispo->max('jenis') == 1)
			                    			<label style="color:red;">{{ $cuk->no_order }} </label><span data-tooltip="Mohon selesaikan dulu tahap Penerimaan Surat" data-position="top center"><i class="question circle outline red icon"></i></span></br>
			                    		@else 
			                    			<label style="color:red;">{{ $cuk->no_order }} </label><span data-tooltip="Mohon selesaikan dulu tahap Kaji Ulang" data-position="top center"><i class="question circle outline red icon"></i></span></br>
			                    		@endif
		                    		@endif
		                    	@endforeach
	                    	@else
	                    		@foreach($cek as $kuy => $cik)
	                    			@if($cik->kaji_ulang)
		                    			<label>{{ $cik->no_order }} </label></br>
		                    		@else
			                    		@if($cik->act_dispo->max('jenis') == 1)
			                    			<label style="color:red;">{{ $cik->no_order }} </label><span data-tooltip="Mohon selesaikan dulu tahap Penerimaan Surat" data-position="top center"><i class="question circle outline red icon"></i></span></br>
			                    		@else 
			                    			<label style="color:red;">{{ $cik->no_order }} </label><span data-tooltip="Mohon selesaikan dulu tahap Kaji Ulang" data-position="top center"><i class="question circle outline red icon"></i></span></br>
			                    		@endif
		                    		@endif
	                    		@endforeach
	                    	@endif
	                    </td>
	                    <td class="center aligned">{{ DateToStringYear($cek->first()->tgl_order) }}</td>
	                    <td class="center aligned">{{ $cek->first()->no_surat }}</td>
	                    <td class="left aligned">
							{{$cek->first()->pelayanan->nama}} </br>  {{$cek->first()->lingkup->nama}}
	                    </td>
	                    <td class="left aligned">
	                    	@if($cek->count() > 1)
	                    	<div class="ui list list-more3" data-display="3">
	                    		@foreach($cek as $kuy => $cuk)
		                    		@if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi']))
			                    		@if($cuk->kaji_ulang)
				                    		@foreach($cuk->detail as $data_jenis)
					                    		<div class="items">
					                    			<div class="ui checkbox">
				                						<input class="adendum-{{$cuk->nomor}}"  data-id="{{$cuk->id}}" type="checkbox" name="pilihan">
				                						<label><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cuk->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a>
				                						</label>
						                    		</div>
					                    		</div>
				                    		@endforeach
				                    	@else
				                    		@foreach($cuk->detail as $data_jenis)
				                    			<div class="items">
				                    				{{$kuy+1}}.&nbsp;&nbsp;{{ $data_jenis->jenis->nama }}
					                    		</div>
				                    		@endforeach
			                    		@endif
			                    	@else
			                    		@if($cuk->kaji_ulang)
				                    		@foreach($cuk->detail as $data_jenis)
				                    			@if($data_jenis->pp->kaji_ulang->surat)
				                    				<div class="items">
				                    					<span><i class="check circle outline green icon"></i></span>
				                    					<input type="hidden" name="kirim" class="kirim-{{$cuk->nomor}}"  data-id="{{$cuk->id}}">
				                						<label><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cuk->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a>
				                						</label>
						                    		</div>
						                    	@else
						                    		<div class="items">
						                    			<div class="items">
						                    				<span><i class="close outline red icon"></i></span>
						                    				<input type="hidden" name="kirim" class="kirim-{{$cuk->nomor}}"  data-id="{{$cuk->id}}">
						                    				<label><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cuk->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a>
						                    				</label>
						                    			</div>
						                    		</div>
						                    	@endif
				                    		@endforeach
				                    	@else
				                    		@foreach($cuk->detail as $data_jenis)
				                    			<div class="items">
				                    				{{$kuy+1}}.&nbsp;&nbsp;{{ $data_jenis->jenis->nama }}
					                    		</div>
				                    		@endforeach
			                    		@endif
			                    	@endif
	                    		@endforeach
	                    	</div>
	                    	@else
	                    	<div class="ui list list-more3" data-display="3">
	                    		@foreach($cek as $kiy => $cok)
		                    			@if($cok->layanan_id == 3)
		                    				@if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi']))
					                    		@if($cok->kaji_ulang)
						                    		@foreach($cok->detail as $data_jenis)
							                    		<div class="items">
							                    			<div class="ui checkbox">
						                						<input class="adendum-{{$cok->nomor}}"  data-id="{{$cok->id}}" type="checkbox" name="pilihan">
						                						<label><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cok->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a>
						                						</label>
								                    		</div>
							                    		</div>
						                    		@endforeach
						                    	@else
						                    		@foreach($cok->detail as $data_jenis)
						                    			<div class="items">
						                    				{{$kuy+1}}.&nbsp;&nbsp;{{ $data_jenis->jenis->nama }}
							                    		</div>
						                    		@endforeach
					                    		@endif
					                    	@else
					                    		@if($cok->kaji_ulang)
						                    		@foreach($cok->detail as $data_jenis)
						                    			@if($data_jenis->pp->kaji_ulang->surat)
						                    				<div class="items">
						                    					<span><i class="check circle outline green icon"></i></span>
						                    					<input type="hidden" name="kirim" class="kirim-{{$cok->nomor}}"  data-id="{{$cok->id}}">
						                						<label><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cok->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a>
						                						</label>
								                    		</div>
								                    	@else
								                    		<div class="items">
								                    			<div class="items">
								                    				<span><i class="close outline red icon"></i></span>
								                    				<input type="hidden" name="kirim" class="kirim-{{$cok->nomor}}"  data-id="{{$cok->id}}">
								                    				<label><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cok->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a>
								                    				</label>
								                    			</div>
								                    		</div>
								                    	@endif
						                    		@endforeach
						                    	@else
						                    		@foreach($cok->detail as $data_jenis)
						                    			<div class="items">
						                    				{{$kuy+1}}.&nbsp;&nbsp;{{ $data_jenis->jenis->nama }}
							                    		</div>
						                    		@endforeach
					                    		@endif
					                    	@endif
		                    			@else
				                    		@if($cok->kaji_ulang)
					                    		@foreach($cok->detail as $kiy => $data_jenis)
					                    			<div class="item">{{$kiy+1}}.&nbsp;&nbsp;<a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="{{ $cok->kaji_ulang->id }}" data-jenis="{{ $data_jenis->id }}">{{ $data_jenis->jenis->nama }}</a></div>
					                    		@endforeach
					                    	@else
					                    		@foreach($cok->detail as $kiy => $data_jenis)
					                    			<div class="item">{{$kiy+1}}.&nbsp;&nbsp;{{ $data_jenis->jenis->nama }}</div>
					                    		@endforeach
				                    		@endif
		                    			@endif
	                    		@endforeach
	                    	</div>
	                    	@endif
	                    </td>
	                    <td class="center aligned">
	                    	@if($cek->first()->kelas == 1)
	                    		Reguler
	                    	@else 
	                    		Prioritas
	                    	@endif
	                    </td>
	                    <td class="center aligned">{{ $cek->first()->user->pelanggans->perusahaan->nama }} </br> {{ $cek->first()->user->pelanggans->nama_lengkap }} </br> {{ $cek->first()->user->pelanggans->no_hp }} </td>
	                    <td class="center aligned">
	                    	@foreach($cek as $vil)
		                    	@if($vil->kaji_ulang)
			                    	@if($vil->kaji_ulang->surat)
			                    		@if($vil->kaji_ulang->surat->no_surat)
				                    		{{ $vil->kaji_ulang->surat->no_surat }} <br>
			                    		@else
			                    			- <br>
			                    		@endif
			                    	@else
			                    		- <br>
			                    	@endif
		                    	@else 
		                    	- <br>
		                    	@endif
	                    	@endforeach
	                	</td>
	                	<td class="center aligned">
	                    	@foreach($cek as $vil)
		                    	@if($vil->kaji_ulang)
			                    	@if($vil->kaji_ulang->surat)
			                    		@if($vil->kaji_ulang->surat->tgl_surat)
				                    		{{ DateToStringYear($vil->kaji_ulang->surat->tgl_surat) }} <br>
			                    		@else
			                    			- <br>
			                    		@endif
			                    	@else
			                    		- <br>
			                    	@endif
		                    	@else 
		                    	- <br>
		                    	@endif
	                    	@endforeach
	                	</td>
	                	<td class="left aligned">
		                    	@foreach($cek as $vil)
			                    	@if($vil->kaji_ulang)
				                    	@if($vil->kaji_ulang->surat)
				                    			@if($vil->kaji_ulang->surat->adendum_status <= 0)
						                    		Normal (Adendum 0) </br>
				                    			@else
						                    		Adendum {{ $vil->kaji_ulang->surat->adendum_status }} </br>
				                    			@endif
				                    	@else
				                    		- <br>
				                    	@endif
			                    	@else 
			                    	- <br>
			                    	@endif
		                    	@endforeach
	                	</td>
	                	<td class="center aligned">
	                    	@if($cek_data['exist'])
	                    		@if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi']))
	                    			<a data-position="left center" data-tooltip='Detil' data-id='{{ $cek->first()->id }}' data-nomor='{{ $cek->first()->nomor }}' data-tipe='{{ $cek->first()->layanan_id }}' class='ui mini teal icon detil button'><i class="eye icon"></i></a>
	                    		@else
	                    			-
	                    		@endif
	                    	@else 
	                    		-
	                    	@endif
	                    </td>
	                    <td class="center aligned">
	                    	<div class="ui button preview" data-id="{{ $cek->first()->id }}" data-tooltip="Lihat" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
								<i class="file image outline icon" style="margin-left: 0px;margin-right: 0px;"></i>
							</div>
							<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
								<a href="{{ url($pageUrl).'/download-surat/'.$cek->first()->id }}">
									<i class="download icon"></i></a>
							</div>
						</td>
	                    <td class="center aligned"><label class="ui green tag label">Surat Penawaran Terkirim</label></td>
	                </tr>
		    	@endforeach
            </tbody>
        </table>
    </div>
</div> 
@endsection
@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	$('.date').calendar({
        type: 'date',
        formatter: {
          date: function (date, settings) {
            if (!date) return '';
            let momentDate = moment(date)
            return momentDate.format('DD/MM/YYYY')
          }
        }
    })
	$(document).ready(function() {
		$(document).on('click', '.preview', function(e){
			var id = $(this).data('id');
			var url = "{{ url($pageUrl) }}/"+id+"/preview-surat";
			console.log(url)
			loadModalSurat({
                'url' : url,
                'modal' : 'longer modal',
                'formId' : '#dataForm',
                'onShow' : function(){ 
                    onShow();
                },
            })
		});
		$(document).on('click', '.preview_penolakan', function(e){
			var id = $(this).data('id');
			var url = "{{ url($pageUrl) }}/"+id+"/preview-penolakan";
			loadModalSurat({
                'url' : url,
                'modal' : 'longer modal',
                'formId' : '#dataForm',
                'onShow' : function(){ 
                    onShow();
                },
            })
		});
		$(document).on('click', '.baru.button', function(e){
			var tipe = $(this).data('tipe');
			var id = $(this).data('id');
			if(tipe == 3){
				var nomor = $(this).data('nomor');
				var arr = [];
				$('.kepala-'+nomor).each(function(){
					if($(this).is(':checked')){
						arr.push($(this).data('id'));
					}else{

					}
				});
				if(arr.length > 0){
					var url = "{{ url($pageUrl) }}/"+id+"/buat/"+arr+"/"+tipe;
					window.location = url;
				}else {
					swal({
						text: "Pilih jenis pengujian yang akan dibuat surat penawaran !",
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'Ok',
					}).then((result) => {
					})
				}
			}else{
				var arr = [];
				var url = "{{ url($pageUrl) }}/"+id+"/buat/"+tipe;
				window.location = url;
			}
		});

		$(document).on('click', '.check_all', function(e){
			var id = $(this).data('id');
			if(this.checked){
				$(".kepala-"+id).prop("checked", true);
			}else{
				$(".kepala-"+id).prop("checked", false);
			}
		});

		$(document).on('click', '.data_ubah.button', function(e){
			var tipe = $(this).data('tipe');
			var id = $(this).data('id');
			if(tipe == 3){
				var nomor = $(this).data('nomor');
				var data = [];
				$('.cek_data-'+nomor).each(function(){
					if($(this).is(':checked')){
						data.push($(this).data('id'));
					}else{

					}
				});
				if(data.length > 0){
					if(data.length > 1){
						swal({
							text: "Pilih salah satu jenis pengujian yang akan di Ubah !",
							type: 'warning',
							showCancelButton: false,
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'Ok',
						}).then((result) => {
						})
					}else{
						$.ajax({
							url: '{{ url('ajax/option/cek-va') }}',
							type: 'POST',
							data: {
								_token: "{{ csrf_token() }}",
								id: data
							},
						}).done(function(response) {
							if(response == 0){
								var url = "{{ url($pageUrl) }}/"+id+"/adendum/"+data+"/"+tipe;
								window.location = url;
							}else{
								swal({
									text: "Virtual Account Sudah Dibuat !",
									type: 'warning',
									showCancelButton: false,
									confirmButtonColor: '#3085d6',
									confirmButtonText: 'Ok',
								}).then((result) => {
								})
							}
						})
					}
				}else {
					swal({
						text: "Pilih jenis pengujian yang akan di Ubah !",
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'Ok',
					}).then((result) => {
					})
				}
			}else{
				$.ajax({
					url: '{{ url('ajax/option/cek-va2') }}',
					type: 'POST',
					data: {
						_token: "{{ csrf_token() }}",
						id: id
					},
				}).done(function(response) {
					if(response == 0){
						var url = "{{ url($pageUrl) }}/"+id+"/adendum/"+tipe;
						window.location = url;
					}else{
						swal({
							text: "Virtual Account Sudah Dibuat !",
							type: 'warning',
							showCancelButton: false,
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'Ok',
						}).then((result) => {
						})
					}
				})
			}
		});

		$(document).on('click', '.wbs.button', function(e){
			var nomor = $(this).data('nomor');
			var url = "{{ url($pageUrl) }}/"+nomor+"/wbs";
			window.location = url;
		});

		$(document).on('click', '.data_virtual.button', function(e){
			var tipe = $(this).data('tipe');
			var id = $(this).data('id');
			if(tipe == 3){
				var nomor = $(this).data('nomor');
				var data = [];
				$('.cek_data-'+nomor).each(function(){
					if($(this).is(':checked')){
						data.push($(this).data('id'));
					}else{

					}
				});
				if(data.length > 0){
					if(data.length > 1){
						swal({
							text: "Pilih salah satu jenis pengujian yang akan dibuat Virtual Account !",
							type: 'warning',
							showCancelButton: false,
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'Ok',
						}).then((result) => {
						})
					}else{
						$('.loading.dimmer').addClass('active');
						$.ajax({
							url: '{{ url('ajax/option/cek-va') }}',
							type: 'POST',
							data: {
								_token: "{{ csrf_token() }}",
								id: data
							},
						})
						.done(function(response) {
							if(response == 0){
								$.ajax({
									url: '{{ url($pageUrl) }}/'+data+'/virtual',
									type: 'POST',
									data: {
										_token: "{{ csrf_token() }}",
										id: data
									},
								})
								.done(function(response) {
									$('.loading.dimmer').removeClass('active');
									swal({
									text: "Virtual Account Berhasil Dibuat !",
									type: 'success',
									showCancelButton: false,
									confirmButtonColor: '#3085d6',
									confirmButtonText: 'Ok',
									}).then((result) => {
										window.location.reload();
									})
								});
							}else{
								$('.loading.dimmer').removeClass('active');
								swal({
									text: "Virtual Account Sudah Dibuat !",
									type: 'warning',
									showCancelButton: false,
									confirmButtonColor: '#3085d6',
									confirmButtonText: 'Ok',
								}).then((result) => {
								})
							}
						})
					}
				}else {
					$('.loading.dimmer').removeClass('active');
					swal({
						text: "Pilih jenis pengujian yang akan dibuat Virtual Account !",
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'Ok',
					}).then((result) => {
					})
				}
			}else{
				$('.loading.dimmer').addClass('active');
				$.ajax({
					url: '{{ url('ajax/option/cek-va2') }}',
					type: 'POST',
					data: {
						_token: "{{ csrf_token() }}",
						id: id
					},
				})
				.done(function(response) {
					if(response == 0){
						$.ajax({
							url: '{{ url($pageUrl) }}/'+id+'/virtual2',
							type: 'POST',
							data: {
								_token: "{{ csrf_token() }}",
								id: id
							},
						})
						.done(function(response) {
							$('.loading.dimmer').removeClass('active');
							swal({
							text: "Virtual Account Berhasil Dibuat !",
							type: 'success',
							showCancelButton: false,
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'Ok',
							}).then((result) => {
								window.location.reload();
							})
						});
					}else{
						$('.loading.dimmer').removeClass('active');
						swal({
							text: "Virtual Account Sudah Dibuat !",
							type: 'warning',
							showCancelButton: false,
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'Ok',
						}).then((result) => {
						})
					}
				})
			}
		});

		$(document).on('click', '.data_kirim.button', function(e){
			var tipe = $(this).data('tipe');
			var id = $(this).data('id');
			if(tipe == 3){
				var nomor = $(this).data('nomor');
				var data = [];
				$('.cek_data-'+nomor).each(function(){
					if($(this).is(':checked')){
						data.push($(this).data('id'));
					}else{

					}
				});

				if(data.length > 0){
					var url = "{{ url($pageUrl) }}/"+id+"/kirim/"+data;
					loadModal({
						'url' : url,
						'modal' : 'tiny modal',
						'formId' : '#dataForm',
						'onShow' : function(){ 
							onShow();
						},
					})
				}else {
					swal({
						text: "Pilih jenis pengujian yang akan Dikirim !",
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'Ok',
					}).then((result) => {
					})
				}
			}else{
				var data = [id];
				var url = "{{ url($pageUrl) }}/"+id+"/kirim/"+data;
				loadModal({
					'url' : url,
					'modal' : 'tiny modal',
					'formId' : '#dataForm',
					'onShow' : function(){ 
						onShow();
					},
				})
			}
		});

		$(document).on('click', '.detil.button', function(e){
			var tipe = $(this).data('tipe');
			var id = $(this).data('id');
			if(tipe == 3){
				var nomor = $(this).data('nomor');
				var data = [];
				$('.adendum-'+nomor).each(function(){
					if($(this).is(':checked')){
						data.push($(this).data('id'));
					}else{

					}
				});

				if(data.length > 0){
					if(data.length > 1){
						swal({
							text: "Pilih salah satu jenis pengujian yang akan ditampilkan detail nya !",
							type: 'warning',
							showCancelButton: false,
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'Ok',
						}).then((result) => {
						})
					}else{
						var url = "{{ url($pageUrl) }}/"+data+"/detil";
						window.location = url;
					}
				}else {
					swal({
						text: "Pilih jenis pengujian yang akan ditampilkan detail nya !",
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'Ok',
					}).then((result) => {
					})
				}
			}else{
				var url = "{{ url($pageUrl) }}/"+id+"/detil";
				window.location = url;
			}
		});

		$(document).on('click', '.data_detail.button', function(e){
			var tipe = $(this).data('tipe');
			var id = $(this).data('id');
			if(tipe == 3){
				var nomor = $(this).data('nomor');
				var data = [];
				$('.cek_data-'+nomor).each(function(){
					if($(this).is(':checked')){
						data.push($(this).data('id'));
					}else{

					}
				});

				if(data.length > 0){
					if(data.length > 1){
						swal({
							text: "Pilih salah satu jenis pengujian yang akan ditampilkan detail nya !",
							type: 'warning',
							showCancelButton: false,
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'Ok',
						}).then((result) => {
						})
					}else{
						var url = "{{ url($pageUrl) }}/"+data+"/detil";
						window.location = url;
					}
				}else {
					swal({
						text: "Pilih jenis pengujian yang akan ditampilkan detail nya !",
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'Ok',
					}).then((result) => {
					})
				}
			}else{
				var url = "{{ url($pageUrl) }}/"+id+"/detil";
				window.location = url;
			}
		});

		$(document).on('click', '.eye.pengujian', function (e){
			var id = $(this).data('id');
			var jenis = $(this).data('jenis');
			var url = "{{ url($pageUrl) }}/"+id+"/detail/"+jenis;
			loadModal({
				'url' : url,
				'modal' : 'small modal',
				'formId' : '#dataForm',
				'onShow' : function(){ 
					onShow();
				},
			})
		});

		$('.demo.tabular.menu .item').tab();
		$('#tab1').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
		        destroy: true,
				responsive: true,
				autoWidth: false,
				processing: true,
				lengthChange: false,
				pageLength: 10,
				info:     true,
				filter: false,
				sorting: [],
                language: {
					url: "{{ asset('plugins/datatables/Indonesian.json') }}"
				},
				drawCallback: function() {
            		readMoreItem('list-more1');
            		var api = this.api();
            		api.column(9, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
						var time = cell.innerHTML;
		            	var d = new Date(time);
		            	d.setDate(d.getDate());
		            	var countDownDate = new Date(d).getTime();
						// Update the count down every 1 second
						var x = setInterval(function() {
						// Get todays date and time
						var now = new Date().getTime();
						// Find the distance between now and the count down date
						var distance = countDownDate - now;
						// Time calculations for days, hours, minutes and seconds
						var days = Math.floor(distance / (1000 * 60 * 60 * 24));
						var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
						var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
						var seconds = Math.floor((distance % (1000 * 60)) / 1000);
						// Output the result in an element with id="demo"
		                start = cell.innerHTML;
		                cell.innerHTML = "<div class='text-center'><a class='ui yellow label'><b>-</b> &nbsp;&nbsp;" + days + "H&nbsp;&nbsp;" + hours + "J&nbsp;&nbsp;"
						+ minutes + "M&nbsp;&nbsp;" + seconds + "D&nbsp;&nbsp;" + "</a></div>";
						if (distance < 1000) {
							clearInterval(x);
							// Update the count down every 1 second
							var dd = new Date(time);
			            	dd.setDate(dd.getDate());
			            	var untuk_lampau = new Date(dd).getTime();
							var z = setInterval(function() {

							// Get todays date and time
							var sekarang = new Date().getTime();

							// Find the distance between sekarang an the count down date
							var panjang = sekarang - untuk_lampau;

							// Time calculations for days, hours, minutes and seconds
							var sdays = Math.floor(panjang / (1000 * 60 * 60 * 24));
							var shours = Math.floor((panjang % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
							var sminutes = Math.floor((panjang % (1000 * 60 * 60)) / (1000 * 60));
							var sseconds = Math.floor((panjang % (1000 * 60)) / 1000);

							// Output the result in an element with id="demo"
							start = cell.innerHTML;
							cell.innerHTML = "<div class='text-center'><a class='ui red label'><b>+</b> &nbsp;&nbsp;" + sdays + "H&nbsp;&nbsp;" + shours + "J&nbsp;&nbsp;"
							+ sminutes + "M&nbsp;&nbsp;" + sseconds + "D&nbsp;&nbsp;" + "</a></div>";
							}, 1000);

						}
						}, 1000);
            		});
            	}
            }
        );

		$('#tab2').DataTable( {
			"paging":   10,
			"ordering": false,
			"lengthChange": false,
			"filter": false,
			"info":     true,
            language: {
                url: "{{ asset('plugins/datatables/Indonesian.json') }}"
            },
            drawCallback: function() {
            	readMoreItem('list-more2');
            	var api = this.api();
        		api.column(12, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					var time = cell.innerHTML;
	            	var d = new Date(time);
	            	d.setDate(d.getDate()+4);
	            	var countDownDate = new Date(d).getTime();
					// Update the count down every 1 second
					var x = setInterval(function() {
					// Get todays date and time
					var now = new Date().getTime();
					// Find the distance between now and the count down date
					var distance = countDownDate - now;
					// Time calculations for days, hours, minutes and seconds
					var days = Math.floor(distance / (1000 * 60 * 60 * 24));
					var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
					var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);
					// Output the result in an element with id="demo"
	                start = cell.innerHTML;
	                cell.innerHTML = "<div class='text-center'><a class='ui yellow label'><b>-</b> &nbsp;&nbsp;" + days + "H&nbsp;&nbsp;" + hours + "J&nbsp;&nbsp;"
					+ minutes + "M&nbsp;&nbsp;" + seconds + "D&nbsp;&nbsp;" + "</a></div>";
					if (distance < 1000) {
						clearInterval(x);
						// Update the count down every 1 second
						var dd = new Date(time);
		            	dd.setDate(dd.getDate());
		            	var untuk_lampau = new Date(dd).getTime();
						var z = setInterval(function() {

						// Get todays date and time
						var sekarang = new Date().getTime();

						// Find the distance between sekarang an the count down date
						var panjang = sekarang - untuk_lampau;

						// Time calculations for days, hours, minutes and seconds
						var sdays = Math.floor(panjang / (1000 * 60 * 60 * 24));
						var shours = Math.floor((panjang % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
						var sminutes = Math.floor((panjang % (1000 * 60 * 60)) / (1000 * 60));
						var sseconds = Math.floor((panjang % (1000 * 60)) / 1000);

						// Output the result in an element with id="demo"
						start = cell.innerHTML;
						cell.innerHTML = "<div class='text-center'><a class='ui red label'><b>+</b> &nbsp;&nbsp;" + sdays + "H&nbsp;&nbsp;" + shours + "J&nbsp;&nbsp;"
						+ sminutes + "M&nbsp;&nbsp;" + sseconds + "D&nbsp;&nbsp;" + "</a></div>";
						}, 1000);

					}
					}, 1000);
        		});
            }
		});

		$('#tab3').DataTable({
			"paging":   10,
			"ordering": false,
			"lengthChange": false,
			"filter": false,
			"info":     true,
            language: {
                url: "{{ asset('plugins/datatables/Indonesian.json') }}"
            },
            drawCallback: function() {
            	readMoreItem('list-more3');
            }
		});

		$(document).on('click', '.upload_penolakan.button', function(event) {
			event.preventDefault();
			var id = $(this).data('id');
			var url = "{{ url($pageUrl) }}/"+id+"/upload-penolakan";
			/* Act on the event */
			loadModal({
				'url' : url,
				'formId' : '#dataForm',
				'modal' : 'tiny',
				'onShow' : function(){ 
					onShow();
				},
			})
		});
	});
</script>
@append
@include('scripts.readmoreitem')



