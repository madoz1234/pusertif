@extends('layouts.form')

@section('styles')
	<style type="text/css">
		.responsive.table{
			width: 100%;
			overflow-x: auto;
		}
		.jus{
			text-align: justify;
			text-justify: inter-word;
		}
		.fn{
			font-size: 12px;
		}
	</style>
@append

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
	<script type="text/javascript">
		formRules = {
			judul: ['empty'],
		};
	</script>
@endsection

@section('content-body')
<div class="ui top demo tabular menu">
	<div class="active item fn tes" data-tab="first">Detail Order</div>
	<div class="item fn tes" data-tab="second">Jenis & Jadwal Pengujian</div>
	<div class="item fn isi" data-tab="third">Surat Penawaran</div>
	<div class="item fn tes" data-tab="four">Riwayat Aktivitas</div>
</div>

<div class="ui bottom demo active tab" data-tab="first">
	<div class="two fields">
		<table class="ui compact table" style="border-radius: 0; margin: 0">
			<tr>
				<td width="250px"><label class="fn">Tgl Order</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">{{ DateToStringYear($data->first()->tgl_order) }}</td>
			</tr>
			<tr>
				<td width="250px" class="fn"><label>No Order</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">
					@if($data->count() > 1)
					<ul class="ui list">
						@foreach($data as $cek)
						<li>{{ $cek->no_order }}</li>
						@endforeach
					</ul>
					@else
					{{ $data->first()->no_order }}
					@endif
				</td>
			</tr>
			<tr>
				<td width="250px" class="fn"><label>Layanan</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">{{ $data->first()->pelayanan->nama }}</td>
			</tr>
			<tr>
				<td><label class="fn">Lingkup</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $data->first()->lingkup->nama or '' }}</td>
			</tr>
			<tr>
				<td><label class="fn">Rencana Pelaksanaan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ BulanToString($data->first()->rencana) }}</td>
			</tr>
			<tr>
				<td><label class="fn">Kelas</label></td>
				<td class="fn">:</td>
				<td class="fn">@if($data->first()->kelas == '1')
					Reguler
					@else
					Prioritas
					@endif
				</td>
			</tr>
			<tr>
				<td><label class="fn">No Surat Permintaan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $data->first()->no_surat or '-' }}</td>
			</tr>
			<tr>
				<td><label class="fn">Tgl Surat Permintaan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ DateToStringYear($data->first()->tgl_surat) }}</td>
			</tr>
			<tr>
				<td><label class="fn">File Surat Permintaan</label></td>
				<td class="fn">:</td>
				<td class="fn">
					<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<a href="{{ url($pageUrl).'/download/'.$data->first()->id }}">
							<i class="download icon"></i></a>
					</div>
				</td>
			</tr>
			<tr>
				<td><label class="fn">Channel</label></td>
				<td class="fn">:</td>
				<td class="fn">@if($data->first()->tipe == 1)
					Online
					@elseif($data->first()->tipe == 2)
					AMS
					@else
					Manual
				@endif</td>
			</tr>
			<tr>
				<td><label class="fn">Peminta Jasa</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $data->first()->user->pelanggans->perusahaan->nama or '' }}</td>
			</tr>
			<tr>
				<td><label class="fn">Kategori</label></td>
				<td class="fn">:</td>
				<td class="fn">
					@if($data->first()->user->pelanggans->tipe_customer == 0)
					PLN
					@elseif($data->first()->user->pelanggans->tipe_customer == 1)
					NON PLN
					@else
					A-PLN
					@endif
				</td>
			</tr>
			<tr>
				<td><label class="fn">Dokumen Pendukung</label></td>
				<td class="fn">:</td>
				<td class="fn">
					@if($data->first())
					@if($data->first()->files)
					@if($data->first()->files->count() > 0)
					<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<a href="{{ url('download', $data->first()->id).'/pendaftaran-pengujian' }}">
							<i class="download icon"></i></a>
						</div>
						@endif
						@endif
						@endif
				</td>
			</tr>
			<tr>
				<td><label class="fn">Catatan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $data->first()->catatan or '-' }}
				</td>
			</tr>
			<tr>
				<td><label class="fn">Keputusan </label></td>
				<td class="fn">:</td>
				<td class="left aligned">
					<a class="ui tag label fn" style="background-color:#00abffcc;">DITERIMA</a>
				</td>
			</tr>
			<tr>
				<td><label class="fn">Keterangan </label></td>
				<td class="fn">:</td>
				<td class="left aligned fn">
					@if($data->first()->kaji_ulang)
						{{$data->first()->kaji_ulang->keterangan}}
					@else
					-
					@endif
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="ui bottom demo tab" data-tab="second">
	<table id="tab1" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
		<thead>
			<tr class="middle aligned">
				<th width="10" class="center aligned fn">No.</th>
				<th width="200" class="center aligned fn">Jenis Pengujian</th>
				<th width="200" class="center aligned fn">Lokasi</th>
				<th width="200" class="center aligned fn">Mata Uji</th>
				<th width="150" class="center aligned fn">Spesifikasi</th>
				<th width="150" class="center aligned fn">Tarif</th>
				<th width="50" class="center aligned fn">Jumlah Benda Uji</th>
				<th width="150" class="center aligned fn">Jadwal Pengujian Tentative
					<br><div class="ui label">Rencana : {{ BulanToString($data->first()->rencana) }}</div>
				</th>
			</tr>
		</thead>
		<tbody class="detail fluid container detail">
			@php
			$no =1;
			@endphp
			@foreach($data as $kuy => $value)
			@if($value->kaji_ulang)
			@foreach($value->kaji_ulang->detail as $key => $vil)
			<tr class="detail fn">
				<td class="center aligned">{{ $no }}</td>
				<td class="center aligned">{{ $vil->detail_pendaftaran->jenis->nama }}</td>
				<td>
					<div class="ui bulleted list">
						@foreach($vil->lokasi as $kiy => $val)
						<div class="item">
							<label class="isi">
								@if($val->jenis_lokasi == 1)
								In House :
								@else 
								On Site :
								@endif
							</label>
							{{ $val->ket_lokasi }}
						</div>
						@endforeach
					</div>
				</td>
				<td width="200" style="text-align: center;">
					@if($vil->mata_uji)
					@foreach($vil->mata_uji as $key => $cek)
					<ul class="ui list">
						<li>{{ $cek->mata_uji }}</li>
					</ul>
					@endforeach
					@else
					-
					@endif
				</td>
				<td  style=" text-align: justify;text-justify: inter-word;">
					{{$vil->spesifikasi or '-'}}
				</td>
				<td class="right aligned">
					{{$vil->tarif or '-'}}
				</td>
				<td class="right aligned">
					{{ rtrim(rtrim(number_format($vil->jumlah,2,',','.'), '0'), ',')}}
					<label>{{$vil->detail_pendaftaran->satuan->satuan or '-'}}</label>
				</td>
				<td class="center aligned">
					{{ DateToStringYear($vil->tentative_start) }} - <br><label style="margin-left: -6px;">{{ DateToStringYear($vil->tentative_end) }}</label>
				</td>
			</tr>
			@php
			$no++;
			@endphp
			@endforeach
			@else
			<tr class="detail">
				<td class="center aligned fn" colspan="8">Tidak ada Data</td>
			</tr>
			@endif
			@endforeach
		</tbody>
	</table>
</div>
<div class="ui bottom demo tab" data-tab="third">
	<form class="ui form" id="dataForm" action="{{ url($pageUrl.$data->first()->id).'/updateKomponen' }}" method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $data->first()->id }}">
		<div class="ui grid">
			<div class="field " style="width: 100%">
				<div class="ui form">
					<div class="four fields">
						<div class="field">
							<label>No Surat Penawaran</label>
							<input type="hidden" name="no_surat" value="{{ $data->first()->no_surat }}">
							{{ $data->first()->no_surat }}
						</div>
						<div class="field">
							<label>Tgl Surat Penawaran</label>
							<input type="hidden" name="tgl_surat" value="{{ date("Y-m-d") }}">
							{{DateToStringYear(date("Y-m-d"))}}
						</div>
						<div class="field">
							<label>Adendum 
								{{ $surat->first()->adendum_status }}
							</label>
							<div class="field">
								<textarea name="adendum" class="areas input" id="adendum" placeholder="Adendum" rows="2"></textarea>
							</div>
						</div>
						<div class="field">
							<label>No WBS / IO</label>
							<input type="text" name="wbs_io" placeholder="No WBS / IO" value="{{ $data->first()->kaji_ulang->surat->wbs_io }}">
						</div>
						<div class="field">
							<label>Jadwal Tetap</label>
							<div class="ui jadwal_start field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="jadwal_start" type="text" placeholder="Jadwal Start" value="{{ $surat->first()->jadwal_start}}">
								</div>
							</div>
							<div class="ui jadwal_end field">
								<div class="ui transparent input left icon">
									<i class="calendar icon"></i>
									<input name="jadwal_end" type="text" placeholder="Jadwal End" value="{{ $surat->first()->jadwal_end}}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="ui top attached segment">
					<a href="" class="ui violet ribbon label">Komponen RAB</a>
					<table id="example" class="ui celled compact violet table display" id="table-rab" width="100%" cellspacing="0">
						<thead>
							<tr class="center aligned">
								<th>No</th>
								<th>Item</th>
								<th>Nominal (Rp)</th>
								<th width="50" class="center aligned">
									<button type="button" class="ui green mini icon tambah_komponen button"><i class="plus icon"></i></button>
								</th>
							</tr>
						</thead>
						<tbody class="komponens">
							@foreach($surat as $cek)
								<input type="hidden" name="data_id[]" value="{{ $cek->id}}">
								<input type="hidden" name="va_id[]" value="{{ $cek->va_id}}">
							@endforeach
							@foreach($surat->first()->detail as $key => $data)
								<tr class="data_komponens" data-id="{{ $data->id }}">
									<td class="center aligned numbor numboor-1" width="50px">{{ $key+1 }}</td>
									<td width="300px">
										<div class="ui transparent fluid input field">
											<input type="text" name="detail[{{$data->id}}][item]" placeholder="Item" value="{{ $data->komponen }}">
										</div>
									</td>
									<td width="200px">
										<div class="ui transparent fluid input field">
											<input type="text" class="number" name="detail[{{$data->id}}][nominal]" placeholder="Nominal" value="{{ $data->nominal }}">
										</div>
									</td>
									<td class="center aligned">
										<button class="ui red mini icon button"><i class="remove icon"></i></button>
									</td>
								</tr>
							@endforeach
							@if($surat->first()->detail->count() > 0)
								<input type="hidden" id="last_key" value="{{$data->id+1}}">
							@else
								<input type="hidden" id="last_key" value="{{0}}">
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</form>
</div>
<div class="ui bottom demo tab" data-tab="four">
	@foreach($data as $kiy => $cik)
		@if($cik->act_dispo()->count() > 0)
		@php 
			$prev = $cik->logsyan()->first(); 
		@endphp
		@foreach($cik->act_dispo->sortByDesc('created_at') as $key => $cek)
			<div class="ui feed">
				<div class="event">
					<div class="label">
						<img src="{{ url(asset('img/avatar04.png')) }}">
					</div>
					<div class="content">
						<div class="date">
							{{$cek->created_at->diffForHumans()}}
						</div>
						<div class="summary">
							@php
								$Date1 	= $cek->created_at;
								$prev 	= $cek->prev();
							@endphp

							@if($cek->tipe == 0)
								<a>{{ $cek->pengirim->nama }}</a> mengubah data Layanan, Lingkup dan Jenis Pengujian
							@elseif($cek->tipe == 1)
								<a>{{ $cek->pengirim->nama }}</a>  Mengalihkan surat <a>{{ $cik->no_order }}</a> kepada <a>{{ $cek->penerima->nama }}</a> @if($cek->keterangan)<i>dengan catatan : {{ $cek->keterangan }}</i>@endif
							@elseif($cek->tipe == 2)
								<a>{{ $cek->pengirim->nama }}</a>  Mendisposisikan surat <a>{{ $cik->no_order }}</a> kepada <a>{{ $cek->penerima->nama }}</a> @if($cek->keterangan)<i>dengan catatan : {{ $cek->keterangan }}</i>@endif
								, dalam waktu &nbsp;&nbsp; {{ timestamp($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name) }}
							@elseif($cek->tipe == 3)
								@if($cek->status_kaji_ulang == 1)
									<a>{{ $cek->pengirim->nama }}</a> menyelesaikan <a>Kaji Ulang</a> dengan status <a>Diterima</a>, dalam waktu &nbsp;&nbsp; {{ timestamp($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name) }}
								@elseif($cek->status_kaji_ulang == 2)
									<a>{{ $cek->pengirim->nama }}</a> menyelesaikan <a>Kaji Ulang</a> dengan status <a>Ditolak</a>, dalam waktu &nbsp;&nbsp; {{ timestamp($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name) }}
								@elseif($cek->status_kaji_ulang == 3)
									<a>{{ $cek->pengirim->nama }}</a> menyelesaikan <a>Kaji Ulang</a> dengan status <a>Didiskusikan</a>, dalam waktu &nbsp;&nbsp; {{ timestamp($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name) }}
								@else
									<a>{{ $cek->pengirim->nama }}</a> menyelesaikan <a>Kaji Ulang</a> dengan status <a>Diterima</a>, dalam waktu &nbsp;&nbsp; {{ timestamp($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name) }}
								@endif
							@elseif($cek->tipe == 4)
								<a>{{ $cek->pengirim->nama }}</a> menyelesaikan <a>Surat Penawaran,</a> No Order <a>{{ $cik->no_order }}</a>@if($cek->status_surat_penawaran > 0) dengan <a> Adendum {{$cek->status_surat_penawaran}}</a> @endif
							@elseif($cek->tipe == 5)
								<a>{{ $cek->pengirim->nama }}</a> menyelesaikan <a> Virtual Account,</a> No Order <a>{{ $cik->no_order }}</a>@if($cek->status_surat_penawaran > 0) dengan <a> Adendum {{$cek->status_surat_penawaran}}</a> @endif dan mengirim email ke <a>{{ $cik->user->pelanggans->perusahaan->nama or '' }}</a>
							@else
							@endif
						</div>
					</div>
				</div>
			</div>
		@endforeach
			<div class="ui feed">
				<div class="event">
					<div class="label">
						<img src="{{ url(asset('img/avatar04.png')) }}">
					</div>
					<div class="content">
						<div class="date">
							{{$cik->created_at->diffForHumans()}}
						</div>
						<div class="summary">
							<a>Penerimaan Surat</a>
						</div>
					</div>
				</div>
			</div>
		@else
			<div class="ui feed">
				<div class="event">
					<div class="label">
						<img src="{{ url(asset('img/avatar04.png')) }}">
					</div>
					<div class="content">
						<div class="date">
							{{$cik->created_at->diffForHumans()}}
						</div>
						<div class="summary">
							<a>Penerimaan Surat</a>
						</div>
					</div>
				</div>
			</div>
		@endif
	@endforeach
</div>
<div class="actions">
	<div class="ui two column grid">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		<div class="right aligned column">
			<br>
			<button id="simpan" style="display: none;" type="button" class="ui large positive right labeled icon save as page button">
				Simpan
				<i class="save icon"></i>
			</button>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
		$(document).on('click', '.isi', function (e){
			$('#simpan').show();
		});

		$(document).on('click', '.tes', function (e){
			$('#simpan').hide();
		});

		$(".number").on("keypress keyup blur",function (e) {    
			$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
		});

		$('.jadwal_start').calendar({
    		type: 'date',
    		formatter: {
    			date: function (date, settings) {
    				if (!date) return '';
    				let momentDate = moment(date)
    				return momentDate.format('YYYY-MM-DD')
    			}
    		},
    		onChange: function () {
    			startCalendar = $(this)
    			var element = $(this).parents('div').find('div.jadwal_end');
    			element.calendar({
    				type: 'date',
    				startCalendar: startCalendar,
    				formatter: {
    					date: function (date, settings) {
    						if (!date) return '';
    						let momentDate = moment(date)
    						return momentDate.format('YYYY-MM-DD')
    					}
    				}
    			})
    			$(this).parents('div').find('div.jadwal_end').find('div').find('input').focus();
    		}
    	});

	var num_key = $('#last_key').val();
	$(document).on('click', '.tambah_komponen', function(e){
	var e = num_key++;
	var rowCount = $('#example > tbody > tr').length;
	var c = rowCount-1;
	var html = `
		<tr class="data_komponens" data-id="`+(c+2)+`">
			<td class="center aligned numbor numboor-`+(c+2)+`" width="50px">`+(c+2)+`</td>
			<td width="300px">
				<div class="ui transparent fluid input field">
				<input type="text" name="detail[`+(e)+`][item]" placeholder="Item">
			</div>
			</td>
			<td width="200px">
				<div class="ui transparent fluid input field">
				<input type="text" class="number" maxlength="10" name="detail[`+(e)+`][nominal]" placeholder="Nominal">
			</div>
			</td>
			<td class="center aligned">
				<button class="ui red mini icon remove button"><i class="remove icon"></i></button>
			</td>
		</tr>`;

		$('.komponens').append(html);
		$(".number").on("keypress keyup blur",function (e) {    
            $(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
        });
	});

	$(document).on('click', '.remove.button', function (e){
		var row = $(this).closest('tr');
		row.remove();
		var table = $('#example');
		var rows = table.find('tbody tr');
		$.each(rows, function(key, value){
			table.find('.numboor-'+$(this).data("id")).html(key+1);
		});
	});
</script>
@append