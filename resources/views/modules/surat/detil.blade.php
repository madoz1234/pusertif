@extends('layouts.form')

@section('styles')
<style type="text/css">
	.responsive.table{
		width: 100%;
		overflow-x: auto;
	}
	.jus{
		text-align: justify;
		text-justify: inter-word;
	}
	.fn{
		font-size: 12px;
	}
</style>
@append

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		judul: ['empty'],
	};
</script>
@endsection

@section('content-body')
<div class="ui top demo tabular menu">
	<div class="active item fn tes" data-tab="first">Detil Order</div>
	<div class="item fn tes" data-tab="second">Jenis & Jadwal</div>
	<div class="item fn isi" data-tab="third">Surat Penawaran</div>
	<div class="item fn tes" data-tab="four">Riwayat Aktivitas</div>
</div>

<div class="ui bottom demo active tab" data-tab="first">
	<div class="two fields">
		<table class="ui compact table" style="border-radius: 0; margin: 0">
			<tr>
				<td width="250px"><label class="fn">Tgl Order</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">{{ DateToStringYear($data->first()->tgl_order) }}</td>
			</tr>
			<tr>
				<td width="250px" class="fn"><label>No Order</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">
					@if($no_order)
						@foreach($no_order as $cuy)
							<label>{{ $cuy }}</label>, 
						@endforeach
					@else
						{{ $data->first()->no_order }}
					@endif
				</td>
			</tr>
			<tr>
				<td width="250px" class="fn"><label>Layanan</label></td>
				<td width="5px" class="fn">:</td>
				<td class="fn">{{ $data->first()->pelayanan->nama }}</td>
			</tr>
			<tr>
				<td><label class="fn">Lingkup</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $data->first()->lingkup->nama or '' }}</td>
			</tr>
			<tr>
				<td><label class="fn">Rencana Pelaksanaan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ BulanToString($data->first()->rencana) }}</td>
			</tr>
			<tr>
				<td><label class="fn">Kelas</label></td>
				<td class="fn">:</td>
				<td class="fn">@if($data->first()->kelas == '1')
					Reguler
					@else
					Prioritas
					@endif
				</td>
			</tr>
			<tr>
				<td><label class="fn">No Surat Permintaan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $data->first()->no_surat or '-' }}</td>
			</tr>
			<tr>
				<td><label class="fn">Tgl Surat Permintaan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ DateToStringYear($data->first()->tgl_surat) }}</td>
			</tr>
			<tr>
				<td><label class="fn">File Surat Permintaan</label></td>
				<td class="fn">:</td>
				<td class="fn">
					<div class="ui button preview" data-id="{{ $data->first()->id }}" data-tooltip="Lihat" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<i class="file image outline icon" style="margin-left: 0px;margin-right: 0px;"></i>
					</div>
					<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<a href="{{ url($pageUrl).'/download/'.$data->first()->id }}">
							<i class="download icon"></i></a>
					</div>
				</td>
			</tr>
			<tr>
				<td><label class="fn">Channel</label></td>
				<td class="fn">:</td>
				<td class="fn">@if($data->first()->tipe == 1)
					Online
					@elseif($data->first()->tipe == 2)
					AMS
					@else
					Manual
				@endif</td>
			</tr>
			<tr>
				<td><label class="fn">Peminta Jasa</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $data->first()->user->pelanggans->perusahaan->nama or '' }}</td>
			</tr>
			<tr>
				<td><label class="fn">Kategori</label></td>
				<td class="fn">:</td>
				<td class="fn">
					@if($data->first()->user->pelanggans->tipe_customer == 0)
					PLN
					@elseif($data->first()->user->pelanggans->tipe_customer == 1)
					NON PLN
					@else
					A-PLN
					@endif
				</td>
			</tr>
			<tr>
				<td><label class="fn">Dokumen Pendukung</label></td>
				<td class="fn">:</td>
				<td class="fn">
					<div class="ui button preview_multiple" data-id="{{ $data->first()->id }}" data-tooltip="Lihat" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<i class="file image outline icon" style="margin-left: 0px;margin-right: 0px;"></i>
					</div>
					@if($data->first())
					@if($data->first()->files)
					@if($data->first()->files->count() > 0)
					<div class="ui button" data-tooltip="Download" data-position="top center" style="padding-top: 11px;padding-bottom: 9px;padding-right: 9px;padding-left: 9px;">
						<a href="{{ url('download', $data->first()->id).'/pendaftaran-pengujian' }}">
							<i class="download icon"></i></a>
						</div>
						@endif
						@endif
						@endif
				</td>
			</tr>
			<tr>
				<td><label class="fn">Catatan</label></td>
				<td class="fn">:</td>
				<td class="fn">{{ $data->first()->catatan or '-' }}
				</td>
			</tr>
			<tr>
				<td><label class="fn">Keputusan </label></td>
				<td class="fn">:</td>
				<td class="left aligned">
					<a class="ui tag label fn" style="background-color:#00abffcc;">DITERIMA</a>
				</td>
			</tr>
			<tr>
				<td><label class="fn">Keterangan </label></td>
				<td class="fn">:</td>
				<td class="left aligned fn">
					@if($data->first()->kaji_ulang)
						{{$data->first()->kaji_ulang->keterangan}}
					@else
					-
					@endif
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="ui bottom demo tab" data-tab="second">
	<table id="tab1" class="ui compact red celled table" style="border-radius: 0; margin: 5px 0;">
		<thead>
			<tr class="middle aligned">
				<th width="10" class="center aligned fn">No.</th>
				<th width="200" class="center aligned fn">Jenis Pengujian</th>
				<th width="150" class="center aligned fn">Merk</th>
				<th width="150" class="center aligned fn">Tipe</th>
				<th width="200" class="center aligned fn">Lokasi</th>
				<th width="150" class="center aligned fn">Mata Uji</th>
				<th width="150" class="center aligned fn">Spesifikasi</th>
				<th width="150" class="center aligned fn">Tarif</th>
				<th width="50" class="center aligned fn">Jumlah Benda Uji</th>
				<th width="150" class="center aligned fn">Jadwal Pengujian Tentative
					<br><div class="ui label">Rencana : {{ BulanToString($data->first()->rencana) }}</div>
				</th>
			</tr>
		</thead>
		<tbody class="detail fluid container detail">
			@php
			$no =1;
			@endphp
			@foreach($data as $kuy => $value)
			@if($value->kaji_ulang)
			@foreach($value->kaji_ulang->detail as $key => $vil)
			<tr class="detail fn">
				<td class="center aligned">{{ $no }}</td>
				<td class="left aligned">{{ $vil->detail_pendaftaran->jenis->nama }}</td>
				<td class="left aligned">{{ $vil->detail_pendaftaran->merk }}</td>
				<td class="left aligned">{{ $vil->detail_pendaftaran->tipe }}</td>
				<td>
					<div class="ui bulleted list">
						@foreach($vil->lokasi as $kiy => $val)
						<div class="item">
							<label class="isi">
								@if($val->jenis_lokasi == 1)
								In House :
								@else 
								On Site :
								@endif
							</label>
							{{ $val->ket_lokasi }}
						</div>
						@endforeach
					</div>
				</td>
				<td width="200" style="text-align: left;">
					<ul class="ui list">
					@if($vil->mata_uji)
					@foreach($vil->mata_uji as $key => $cek)
						<div class="items">{{$key+1}}. {{ $cek->mata_uji }}</div>
					@endforeach
					</ul>
					@else
					-
					@endif
				</td>
				<td  style=" text-align: justify;text-justify: inter-word;">
					{{$vil->spesifikasi or '-'}}
				</td>
				<td class="left aligned">
					{{$vil->tarif or '-'}}
				</td>
				<td class="right aligned">
					{{ rtrim(rtrim(number_format($vil->jumlah,2,',','.'), '0'), ',')}}
					<label>{{$vil->detail_pendaftaran->satuan->satuan or '-'}}</label>
				</td>
				<td class="center aligned">
					{{ DateToStringYear($vil->tentative_start) }} - <br><label style="margin-left: -6px;">{{ DateToStringYear($vil->tentative_end) }}</label>
				</td>
			</tr>
			@php
			$no++;
			@endphp
			@endforeach
			@else
			<tr class="detail">
				<td class="center aligned fn" colspan="8">Tidak ada Data</td>
			</tr>
			@endif
			@endforeach
		</tbody>
	</table>
</div>
<div class="ui bottom demo tab" data-tab="third">
	<form class="ui form" id="dataForm" action="{{ url($pageUrl.'saveKomponen') }}" method="POST">
		{!! csrf_field() !!}
			<div class="ui yellow segment">
				<div class="field " style="width: 100%">
					@php 
						$angka = (int)$data->first()->user->pelanggans->tipe_customer;
					@endphp
					<div class="ui form">
						<div class="four fields">
							<div class="field">
								<label>No Surat Penawaran</label>
								<input type="hidden" name="no_surat" value="{{ $surat->no_surat }}">
								{{ $surat->no_surat }}
							</div>
							<div class="field">
								<label>Tgl Surat Penawaran</label>
								<input type="hidden" name="tgl_surat" value="{{ date("Y-m-d") }}">
								{{DateToStringYear(date("Y-m-d"))}}
							</div>
							<div class="field">
								<label>Status</label>
								<div class="field">
									<p style="text-align: justify;text-justify: inter-word;">
										@if($surat->adendum_status <= 0)
											Normal (Adendum 0)
										@else
											Adendum {{ $surat->adendum_status }}
										@endif</p>
								</div>
							</div>
							<div class="field">
								<label>Kelas</label>
								<div class="field">
									@if($data->first()->kelas == 1)
										Reguler
									@else 
										Prioritas
									@endif
								</div>
							</div>
							<div class="field">
								@if($angka !== 0)
									<label>Total Sesudah PPn</label>
								@else
									<label>Total</label>
								@endif
								<div class="field">
									Rp. {{ rtrim(rtrim(number_format($surat->total,2,',','.'), '0'), ',')}}
								</div>
							</div>
							@if($angka !== 0)
							@else 
								<div class="field">
									<label>No SKKI/SKKO/PRK</label>
									{{ $surat->no_skki }}
								</div>
								<div class="field">
									<label>Tgl SKKI/SKKO/PRK</label>
									{{ DateToStringYear($surat->tgl_skki) }}
								</div>
							@endif
						</div>
					</div>
					<div class="ui top attached">
						<a href="" class="ui red ribbon label">Komponen RAB</a>
						<table id="example" class="ui celled compact red table display" id="table-rab" width="100%" cellspacing="0">
							<thead>
								<tr class="center aligned">
									<th>No</th>
									<th>Uraian</th>
									<th>Nominal (Rp)</th>
								</tr>
							</thead>
							<tbody class="komponens">
								@php
									$total = 0;
								@endphp
								@foreach($surat->detail as $kiy => $cik)
								<tr class="data_komponens" data-id="1">
									<td class="center aligned numbor numboor-1" style="width: 50px;">{{ $kiy+1 }}</td>
									<td width="300px">
										<div class="ui transparent fluid input field">
											{{$cik->komponen}}
										</div>
									</td>
									<td width="200px">
										<div class="ui transparent" style="text-align: right;">
											{{ rtrim(rtrim(number_format($cik->nominal,2,',','.'), '0'), ',')}}
											@php
												$total += $cik->nominal;
											@endphp
										</div>
									</td>
								</tr>
								@endforeach
								@php
									if($angka !== 0){
										$ppn = (($total/100)*10);
									}else{
										$ppn = 0;
									}
									$total_ppn = ($total + $ppn);
								@endphp
							</tbody>
							<tfoot>
								@if($angka !== 0)
									<tr>
										<td style="border-top: 2px solid rgba(34,36,38,.1);"></td>
										<td scope="row" style="text-align: right;border-top: 2px solid rgba(34,36,38,.1);border-left: none;">Total Sebelum PPn</td>
										<td style="text-align: right;border-top: 2px solid rgba(34,36,38,.1);" class="total">{{ rtrim(rtrim(number_format($total,2,',','.'), '0'), ',')}}</td>
									</tr>
									<tr>
										<td style="border-top: 1px solid rgba(34,36,38,.1);"></td>
										<td scope="row" style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);border-left: none;">PPn</td>
										<td style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);" class="total">{{ rtrim(rtrim(number_format($ppn,2,',','.'), '0'), ',')}}</td>
									</tr>
									<tr>
										<td style="border-top: 1px solid rgba(34,36,38,.1);"></td>
										<td scope="row" style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);border-left: none;">Total Sesudah PPn</td>
										<td style="text-align: right;border-top: 1px solid rgba(34,36,38,.1);" class="total">{{ rtrim(rtrim(number_format($total_ppn,2,',','.'), '0'), ',')}}</td>
									</tr>
								@else
									<tr>
										<td style="border-top: 2px solid rgba(34,36,38,.1);"></td>
										<td scope="row" style="text-align: right;border-top: 2px solid rgba(34,36,38,.1);border-left: none;">Total Sebelum PPn</td>
										<td style="text-align: right;border-top: 2px solid rgba(34,36,38,.1);" class="total">{{ rtrim(rtrim(number_format($total_ppn,2,',','.'), '0'), ',')}}</td>
									</tr>
								@endif
							</tfoot>
						</table>
					</div>
				</div>
			</div>
	</form>
</div>
<div class="ui bottom demo tab" data-tab="four">
	@foreach($data as $kiy => $cik)
		{!! riwayatPengujian($cik) !!}
		{!! riwayatAktivitas($cik) !!}
	@endforeach
</div>
<div class="actions">
	<div class="ui two column grid">
		<div class="left aligned column">
			<br>
			<div class="ui gray deny labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
	$(document).on('click', '.preview', function(e){
			var id = $(this).data('id');
			var url = "{{ url($pageUrl) }}/"+id+"/preview";
			console.log(url)
			loadModal({
                'url' : url,
                'modal' : 'longer modal',
                'formId' : '#dataForm',
                'onShow' : function(){ 
                    onShow();
                },
            })
	});

	$(document).on('click', '.preview_multiple', function(e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/"+id+"/preview-multiple/pendaftaran-pengujian";
		loadModal({
            'url' : url,
            'modal' : 'longer modal',
            'formId' : '#dataForm',
            'onShow' : function(){ 
                onShow();
            },
        })
	});	
});
</script>
@append