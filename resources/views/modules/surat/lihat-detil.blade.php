<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Lihat Detil Surat Penawaran</div>
<div class="content">
	<form class="ui form" id="formPengujian" action="{{ url($pageUrl.'lihat-detil') }}" method="POST">
		{!! csrf_field() !!}
		<table class="table" style="border-radius: 0; margin: 0">
			<input type="hidden" name="nomor" value="{{ $nomor }}">
			<tr>
				<td style="width: 22%;"><b>No Order</b></td>
				<td width="10">:</td>
				<td style="width: 100%;">
					<div class="field">
						<select name="no_order" id="no_order" class="watcher ui fluid search pilihan dropdown">
							{!!
							\App\Models\FrontEnd\PendaftaranPengujian::optionsa('no_order', 'id', ['filters' => [function($q) use ($nomor){
								return $q->where('nomor', $nomor);
							}],
							'selected' => old('perusahaan_id')
							])
							!!}
						</select>
					</div>
				</td>
			</tr>
		</table>
	</form>
</div>
<div class="actions">
	<div class="ui positive right labeled icon save button add-uji" style="background-color: #00AEEF;">
		Lihat
		<i class="checkmark icon"></i>
	</div>
</div>
<script type="text/javascript">
</script>