<script type="text/javascript">
	function goBack() {
		window.history.back();
	}

	$(document).on('click', '.simpan.register', function(e){
		e.preventDefault()
		var textArea = $('.areas').val();
		var btnBatal = '{{trans('translator.Batal')}}';
		var btnDaftar = '{{trans('translator.Daftar2')}}';
		var alert = "{{trans('translator.Swal Register')}}";
		swal({
		// title: 'Apakah Anda Yakin?',
			text: alert,
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			confirmButtonText: btnDaftar,
			cancelButtonColor: '#808080',
			cancelButtonText: btnBatal,
			reverseButtons: true
		}).then((result) => {
			if (result) {
				saveForm();
			}
		})
	});
	function saveForm(form="formRegister")
	{
		var header = '{{trans('translator.Alert Header')}}';
		var body = '{{trans('translator.Alert Body')}}';
		if($("#"+form).form('is valid')){
			$('#load').find('.loading.dimmer').addClass('active');
			$("#"+form).ajaxSubmit({
				success: function(resp){
					$('#load').find('.loading.dimmer').removeClass('active');
					swal(
						header,
						body,
						'success'
					).then((result) => {
						document.getElementById("formRegister").reset(); 
						window.location.reload();
					})
				},
				error: function(resp){
					$('#load').find('.loading.dimmer').removeClass('active');
					$.each(resp.responseJSON.errors, function(index, val) {
						clearFormErrorRegister(index,val);
						showFormErrorRegister(index,val);
					});
					time = 5;
					interval = setInterval(function(){
						time--;
						if(time == 0){
							clearInterval(interval);
							$('.pointing.prompt.label.transition.visible').remove();
							$('.error').each(function (index, val) {
								$(val).removeClass('error');
							});
						}
					},1000)
				}
			});
		}
	}

	clearErrorRegister = function(key, value)
	{
		if(key.includes("."))
		{
			res = key.split('.');
			key = res[0] + '[' + res[1] + ']';
			if(res[1] == 0)
			{
				key = res[0] + '\\[\\]';
			}
		}
		var elm = $('#formRegister' + ' [name=' + key + ']').closest('.field');
		$(elm).removeClass('error');

		var showerror = $('#formRegister' + ' [name=' + key + ']').closest('.field').find('.ui.basic.red.pointing.prompt.label.transition.visible').remove();
	}

	showErrorRegister = function(key, value)
	{
		console.log(key)
		if(key.includes("."))
		{
			res = key.split('.');
			key = res[0] + '[' + res[1] + ']';
			if(res[1] == 0)
			{
				key = res[0] + '\\[\\]';
			}
		}

		var elm = $('#formRegister' + ' [name=' + key + ']').closest('.field');
		$(elm).addClass('error');
		var message = `<div class="ui basic red pointing prompt label transition visible">`+ value +`</div>`;

		var showerror = $('#formRegister' + ' [name=' + key + ']').closest('.field');
		$(showerror).append('<div class="ui basic red pointing prompt label transition visible">' + value + '</div>');
	}

	clearElementErrorRegister = function(key, value, element)
	{
		if(key.includes("."))
		{
			res = key.split('.');
			key = res[0] + '[' + res[1] + ']';
			if(res[1] == 0)
			{
				key = res[0] + '\\[\\]';
			}
		}
		var elm = $(element + ' [name=' + key + ']').closest('.field');
		$(elm).removeClass('error');

		var showerror = $(element + ' [name=' + key + ']').closest('.field').find('.ui.basic.red.pointing.prompt.label.transition.visible').remove();
	}

	showElementErrorRegister = function(key, value, element)
	{
		if(key.includes("."))
		{
			res = key.split('.');
			key = res[0] + '[' + res[1] + ']';
			if(res[1] == 0)
			{
				key = res[0] + '\\[\\]';
			}
		}

		var elm = $(element + ' [name=' + key + ']').closest('.field');
		$(elm).addClass('error');
		var message = `<div class="ui basic red pointing prompt label transition visible">`+ value +`</div>`;

		var showerror = $(element + ' [name=' + key + ']').closest('.field');
		$(showerror).append('<div class="ui basic red pointing prompt label transition visible">' + value + '</div>');
	}

	showFormErrorRegister = function(key, value)
	{
		if(key.includes("."))
		{
			res = key.split('.');
			key = res[0] + '[' + res[1] + ']';
			if(res[1] == 0)
			{
				key = res[0] + '\\[\\]';
				var exist = $('#formRegister' + ' [name="' + res[0] + '[' + res[1] + ']' + '"]');
				if(exist.length > 0)
				{
					key = res[0] + '[' + res[1] + ']';
				}
			}
			if(res[2])
			{
				key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']';
				if(res[2] == 0)
				{
					key = res[0] + '['+ res[1] +']' + '\\[\\]';
				}
			}
			if(res[3])
			{
				key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']';
				if(res[3] == 0)
				{
					key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '\\[\\]';
				}
			}
			if(res[4])
			{
				key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']';
				if(res[4] == 0)
				{
					key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '\\[\\]';
				}
			}
			if(res[5])
			{
				key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']' + '[' + res[5] + ']';
				if(res[5] == 0)
				{
					key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '['+ res[4] +']' + '\\[\\]';
				}
			}
		}
		var elm = $('#formRegister' + ' [name^="' + key + '"]').closest('.field');
		var tabs = $('#formRegister' + ' [name^="' + key + '"]').parents('.tab.segment');
		if(tabs.length > 0)
		{
			// selectedTabs(tabs);
		}
		var message = `<div class="ui basic red pointing prompt label transition visible">`+ value +`</div>`;
		var showerror = $('#formRegister' + ' [name^="' + key + '"]').closest('.field');
		var multipleCheckbox = $(showerror).parents('.multiple-checkbox');
		if(multipleCheckbox.length > 0)
		{
			multipleCheckboxLabel = multipleCheckbox.find('label:first-child');
			$(multipleCheckboxLabel).append('<span class="red error-label" style="color:#9f3a38 !important;">' + value + '</span>');
		}else{
			$(elm).addClass('error');
			$(showerror).append('<div class="ui basic red pointing prompt label transition visible">' + value + '</div>');
		}

	}

	clearFormErrorRegister = function(key, value)
	{
		if(key.includes("."))
		{
			res = key.split('.');
			key = res[0] + '[' + res[1] + ']';
			if(res[1] == 0)
			{
				key = res[0] + '\\[\\]';
				var exist = $('#formRegister' + ' [name="' + res[0] + '[' + res[1] + ']' + '"]');
				if(exist.length > 0)
				{
					key = res[0] + '[' + res[1] + ']';
				}
			}
			if(res[2])
			{
				key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']';
				if(res[2] == 0)
				{
					key = res[0] + '['+ res[1] +']' + '\\[\\]';
				}
			}
			if(res[3])
			{
				key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']';
				if(res[3] == 0)
				{
					key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '\\[\\]';
				}
			}
			if(res[4])
			{
				key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']';
				if(res[4] == 0)
				{
					key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '\\[\\]';
				}
			}
			if(res[5])
			{
				key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']' + '[' + res[5] + ']';
				if(res[5] == 0)
				{
					key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '['+ res[4] +']' + '\\[\\]';
				}
			}
		}
		var elm = $('#formRegister' + ' [name^="' + key + '"]').closest('.field');
		$(elm).removeClass('error');

		var showerror = $('#formRegister' + ' [name^="' + key + '"]').closest('.field').find('.ui.basic.red.pointing.prompt.label.transition.visible').remove();
	}
</script>
