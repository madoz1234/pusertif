@include('scripts.readmoreitem')
<script type="text/javascript">
    // global
    var dt = "";
    var dt2 = "";
    var dt4 = "";
    var dt5 = "";
    var filterdata = function(d){
        d._token = "{{ csrf_token() }}";
        @yield('filterdata')
    }
    $(document).ready(function() {
        $.fn.dataTable.ext.errMode = 'none';
        /*Start Of Online*/
        dt = $('#listTable').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
                destroy: true,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                lengthChange: false,
                pageLength: 100,
                info:     true,
                filter: false,
                sorting: [],
                language: {
                    url: "{{ asset('plugins/datatables/Indonesian.json') }}"
                },
                ajax:  {
                    url: "{{ url($pageUrl.'grid') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct']) !!},
                drawCallback: function() {
                    readMoreItem('list-more1');
                },
                // headerCallback: function( thead, data, start, end, display ) {
                //     $(thead).find('th').eq(10).html( 'Cheklist </br><button type="button" class="ui mini button select-all"><i class="checkmark icon"></i> Select All</button>');
                // }
            }
        );

        dt.on('draw.dt', function () {
            dt.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
        }).draw();

        $('#btn-filter').on('click', function(e) {
            dt.draw();
            e.preventDefault();
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt.draw();
                e.preventDefault();
            }
        })
        /*End Of Online*/

        /*Start Of SPM*/
        dt2 = $('#listTable2').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
                destroy: true,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                lengthChange: false,
                pageLength: 10,
                info:     true,
                filter: false,
                sorting: [],
                language: {
                    url: "{{ asset('plugins/datatables/Indonesian.json') }}"
                },
                ajax:  {
                    url: "{{ url($pageUrl.'historis') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct2']) !!},
                drawCallback: function() {
                    readMoreItem('list-more2');
                }
            }
        );

        dt2.on('draw.dt2', function () {
            dt2.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
            // readMoreItem('list-more2');

        }).draw();

        $('#btn-filter').on('click', function(e) {
            dt2.draw();
            e.preventDefault();
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt2.draw();
                e.preventDefault();
            }
        })

        $('.filter.button').on('click', function(e) {
            dt.draw();
            dt2.draw();
            // dt2.ajax.reload();
            e.preventDefault();
        });

        $('.reset.button').on('click', function(e) {
            $('.dropdown .delete').trigger('click');
            setTimeout(function(){
                dt.draw();
                dt2.draw();
            }, 100);
            $('.dropdown').dropdown('clear');
        });
        /*End Of SPM*/
    });
</script>
