@include('scripts.readmoreitem')
<script type="text/javascript">
    // global
    var filterdata = function(d){
        d._token = "{{ csrf_token() }}";
        @yield('filterdata')
    }
    var dt = "";
    var dt2 = "";
    var dt3 = "";
    var dt4 = "";
    var dt5 = "";
    $(document).ready(function() {
        $.fn.dataTable.ext.errMode = 'none';
        /*Start Of Online*/
        dt = $('#listTable').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
                destroy: true,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                lengthChange: false,
                pageLength: 10,
                info:     true,
                filter: false,
                sorting: [],
                language: {
                    url: "{{ asset('plugins/datatables/Indonesian.json') }}"
                },
                ajax:  {
                    url: "{{ url($pageUrl.'grid') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct']) !!},
                drawCallback: function() {
                    readMoreItem('list-more1');
                },
                headerCallback: function( thead, data, start, end, display ) {
                    $(thead).find('th').eq(12).html( 'Cheklist </br><button type="button" class="ui mini button select-all"><i class="checkmark icon"></i> Select All</button>');
                }
            }
        );

        dt.on('draw.dt', function () {
            dt.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
            dt.column(9, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                var time = cell.innerHTML;
                var d = new Date(time);
                d.setDate(d.getDate());
                var countDownDate = new Date(d).getTime();
                // Update the count down every 1 second
                var x = setInterval(function() {
                // Get todays date and time
                var now = new Date().getTime();
                // Find the distance between now and the count down date
                var distance = countDownDate - now;
                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                // Output the result in an element with id="demo"
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'><a class='ui yellow label'><b>-</b> &nbsp;&nbsp;" + days + "H&nbsp;&nbsp;" + hours + "J&nbsp;&nbsp;"
                + minutes + "M&nbsp;&nbsp;" + seconds + "D&nbsp;&nbsp;" + "</a></div>";
                if (distance < 1000) {
                    clearInterval(x);
                    // Update the count down every 1 second
                    var dd = new Date(time);
                    dd.setDate(dd.getDate());
                    var untuk_lampau = new Date(dd).getTime();
                    var z = setInterval(function() {

                    // Get todays date and time
                    var sekarang = new Date().getTime();

                    // Find the distance between sekarang an the count down date
                    var panjang = sekarang - untuk_lampau;

                    // Time calculations for days, hours, minutes and seconds
                    var sdays = Math.floor(panjang / (1000 * 60 * 60 * 24));
                    var shours = Math.floor((panjang % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var sminutes = Math.floor((panjang % (1000 * 60 * 60)) / (1000 * 60));
                    var sseconds = Math.floor((panjang % (1000 * 60)) / 1000);

                    // Output the result in an element with id="demo"
                    start = cell.innerHTML;
                    // cell.innerHTML = "<div class='text-center'><a class='ui red label'> EXPIRED </a></div>";
                    
                    cell.innerHTML = "<div class='text-center'><a class='ui red label'><b>+</b> &nbsp;&nbsp;" + sdays + "H&nbsp;&nbsp;" + shours + "J&nbsp;&nbsp;"
                    + sminutes + "M&nbsp;&nbsp;" + sseconds + "D&nbsp;&nbsp;" + "</a></div>";
                    }, 1000);

                }
                }, 1000);
            });
            // readMoreItem('list-more1');

        }).draw();

        $('#btn-filter').on('click', function(e) {
            dt.draw();
            e.preventDefault();
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt.draw();
                e.preventDefault();
            }
        })
        /*End Of Online*/

        /*Start Of SPM*/
        dt2 = $('#listTable2').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
                destroy: true,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                lengthChange: false,
                pageLength: 10,
                info:     true,
                filter: false,
                sorting: [],
                language: {
                    url: "{{ asset('plugins/datatables/Indonesian.json') }}"
                },
                ajax:  {
                    url: "{{ url($pageUrl.'menunggu') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct2']) !!},
                drawCallback: function() {
                    readMoreItem('list-more2');
                }
            }
        );

        dt2.on('draw.dt2', function () {
            dt2.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
            // readMoreItem('list-more2');

        }).draw();

        $('#btn-filter').on('click', function(e) {
            dt2.draw();
            e.preventDefault();
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt2.draw();
                e.preventDefault();
            }
        })
        /*End Of SPM*/


        /*Start Of SPM*/
        dt3 = $('#listTable3').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
                destroy: true,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                lengthChange: false,
                pageLength: 10,
                info:     true,
                filter: false,
                sorting: [],
                language: {
                    url: "{{ asset('plugins/datatables/Indonesian.json') }}"
                },
                ajax:  {
                    url: "{{ url($pageUrl.'submit') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct3']) !!},
                drawCallback: function() {
                    readMoreItem('list-more3');
                }
            }
        );

        dt3.on('draw.dt3', function () {
            dt3.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
            // readMoreItem('list-more2');

        }).draw();

        $('#btn-filter').on('click', function(e) {
            dt3.draw();
            e.preventDefault();
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt3.draw();
                e.preventDefault();
            }
        })
        /*End Of SPM*/

        /*Start Of SPM*/
        dt4 = $('#listTable4').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
                destroy: true,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                lengthChange: false,
                pageLength: 10,
                info:     true,
                filter: false,
                sorting: [],
                language: {
                    url: "{{ asset('plugins/datatables/Indonesian.json') }}"
                },
                ajax:  {
                    url: "{{ url($pageUrl.'nonaktif') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct4']) !!},
                drawCallback: function() {
                    readMoreItem('list-more4');
                }
            }
        );

        dt4.on('draw.dt4', function () {
            dt4.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
            // readMoreItem('list-more3');

        }).draw();

        $('#btn-filter').on('click', function(e) {
            dt4.draw();
            e.preventDefault();
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt4.draw();
                e.preventDefault();
            }
        })
        /*End Of SPM*/

        /*Start Of SPM*/
        dt5 = $('#listTable5').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
                destroy: true,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                lengthChange: false,
                pageLength: 10,
                info:     true,
                filter: false,
                sorting: [],
                language: {
                    url: "{{ asset('plugins/datatables/Indonesian.json') }}"
                },
                ajax:  {
                    url: "{{ url($pageUrl.'download') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct5']) !!},
                drawCallback: function() {
                    readMoreItem('list-more5');
                    $('.riwayat-va.button')
                      .popup({
                        popup : $('.riwayat-va.popup'),
                        on    : 'hover'
                      })
                    ;

                }
            }
        );

        dt5.on('draw.dt5', function () {
            dt5.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
            // dt5.column(9, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            //     var time = cell.innerHTML;
            //     var d = new Date(time);
            //     d.setDate(d.getDate()+4);
            //     var countDownDate = new Date(d).getTime();
            //     // Update the count down every 1 second
            //     var x = setInterval(function() {
            //     // Get todays date and time
            //     var now = new Date().getTime();
            //     // Find the distance between now and the count down date
            //     var distance = countDownDate - now;
            //     // Time calculations for days, hours, minutes and seconds
            //     var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            //     var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            //     var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            //     var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            //     // Output the result in an element with id="demo"
            //     start = cell.innerHTML;
            //     cell.innerHTML = "<div class='text-center'><a class='ui yellow label'><b>-</b> &nbsp;&nbsp;" + days + "H&nbsp;&nbsp;" + hours + "J&nbsp;&nbsp;"
            //     + minutes + "M&nbsp;&nbsp;" + seconds + "D&nbsp;&nbsp;" + "</a></div>";
            //     if (distance < 1000) {
            //         clearInterval(x);
            //         // Update the count down every 1 second
            //         var dd = new Date(time);
            //         dd.setDate(dd.getDate());
            //         var untuk_lampau = new Date(dd).getTime();
            //         var z = setInterval(function() {

            //         // Get todays date and time
            //         var sekarang = new Date().getTime();

            //         // Find the distance between sekarang an the count down date
            //         var panjang = sekarang - untuk_lampau;

            //         // Time calculations for days, hours, minutes and seconds
            //         var sdays = Math.floor(panjang / (1000 * 60 * 60 * 24));
            //         var shours = Math.floor((panjang % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            //         var sminutes = Math.floor((panjang % (1000 * 60 * 60)) / (1000 * 60));
            //         var sseconds = Math.floor((panjang % (1000 * 60)) / 1000);

            //         // Output the result in an element with id="demo"
            //         start = cell.innerHTML;
            //         // cell.innerHTML = "<div class='text-center'><a class='ui red label'> EXPIRED </a></div>";
                    
            //         cell.innerHTML = "<div class='text-center'><a class='ui red label'><b>+</b> &nbsp;&nbsp;" + sdays + "H&nbsp;&nbsp;" + shours + "J&nbsp;&nbsp;"
            //         + sminutes + "M&nbsp;&nbsp;" + sseconds + "D&nbsp;&nbsp;" + "</a></div>";
            //         }, 1000);

            //     }
            //     }, 1000);
            // });
            // readMoreItem('list-more4');

        }).draw();

        $('.filter.button').on('click', function(e) {
            dt.draw();
            dt2.draw();
            dt3.draw();
            dt4.draw();
            dt5.draw();
            // dt2.ajax.reload();
            e.preventDefault();
        });

        $('.reset.button').on('click', function(e) {
            $('.dropdown .delete').trigger('click');
            setTimeout(function(){
                dt.draw();
                dt2.draw();
                dt3.draw();
                dt4.draw();
                dt5.draw();
            }, 100);
            $('.dropdown').dropdown('clear');
        });

        $('#btn-filter').on('click', function(e) {
            dt5.draw();
            e.preventDefault();
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt5.draw();
                e.preventDefault();
            }
        })
        /*End Of SPM*/
    });
</script>
