@include('scripts.readmoreitem')
<script type="text/javascript">
    // global
    var filterdata = function(d){
        d._token = "{{ csrf_token() }}";
        @yield('filterdata')
    }
    var dt = "";
    $(document).ready(function() {
    	var url = "{{ url($pageUrl.'grid') }}";
    	console.log(url)
        $.fn.dataTable.ext.errMode = 'none';
        /*Start Of Online*/
        dt = $('#listTable').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
                destroy: true,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                lengthChange: false,
                pageLength: 10,
                info:     true,
                filter: false,
                sorting: [],
                language: {
                    url: "{{ asset('plugins/datatables/Indonesian.json') }}"
                },
                ajax:  {
                    url: "{{ url($pageUrl.'grid') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct']) !!},
                drawCallback: function() {
                    readMoreItem('list-more1');
                },
            }
        );

        dt.on('draw.dt', function () {
            dt.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
        }).draw();

        $('.filter.button').on('click', function(e) {
            dt.draw();
            e.preventDefault();
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt.draw();
                e.preventDefault();
            }
        })
        /*End Of Online*/
    });
</script>
