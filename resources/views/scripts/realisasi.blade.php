@include('scripts.readmoreitem')
<script type="text/javascript">
    // global
    var filterdata = function(d){
        d._token = "{{ csrf_token() }}";
        @yield('filterdata')
    }
    var dt = "";
    var dt2 = "";
    $(document).ready(function() {
        /*Start Of Online*/
        dt = $('#listTable').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
		        destroy: true,
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				lengthChange: false,
				pageLength: 10,
				info:     true,
				filter: false,
				sorting: [],
                language: {
					url: "{{ asset('plugins/datatables/Indonesian.json') }}"
				},
                ajax:  {
                    url: "{{ url($pageUrl.'grid') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct']) !!},
                drawCallback: function() {
                    readMoreItem('list-more1');
                }
            }
        );

        dt.on('draw.dt', function () {
            dt.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
        }).draw();

        $('#btn-filter').on('click', function(e) {
            dt.draw();
            e.preventDefault();
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt.draw();
                e.preventDefault();
            }
        })
        /*End Of Online*/

        /*Start Of Diskusi*/
        dt2 = $('#listTable2').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
		        destroy: true,
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				lengthChange: false,
				pageLength: 10,
				info:     true,
				filter: false,
				sorting: [],
                language: {
					url: "{{ asset('plugins/datatables/Indonesian.json') }}"
				},
                ajax:  {
                    url: "{{ url($pageUrl.'histori') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct2']) !!},
                drawCallback: function() {
                    readMoreItem('list-more2');
                }
            }
        );

        dt2.on('draw.dt2', function () {
            dt2.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
            dt2.column(9, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            	var indexNumber = 0;

            	function colorChange() {
            		var colors = ["one", "two", "three"];
            		$('.top-barz').removeClass("one two three").addClass(colors[indexNumber]);
            	}
            	setInterval(function () {
            		colorChange();
            		indexNumber++;
            		if (indexNumber == 3) {
            			indexNumber = 0
            		}
            	}, 1000);
            });
        }).draw();

        $('#btn-filter').on('click', function(e) {
            dt2.draw();
            e.preventDefault();
        });
        $(".number").on("keypress keyup blur",function (e) {    
                $(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
                // $(this).val($(this).val());
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt2.draw();
                e.preventDefault();
            }
        })


        $('.filter.button').on('click', function(e) {
            dt.draw();
            dt2.draw();
            // dt2.ajax.reload();
            e.preventDefault();
        });

        $('.reset.button').on('click', function(e) {
            $('.dropdown .delete').trigger('click');
            setTimeout(function(){
                dt.draw();
                dt2.draw();
            }, 100);
            $('.dropdown').dropdown('clear');
        });
        /*End Of Diskusi*/
    });
</script>