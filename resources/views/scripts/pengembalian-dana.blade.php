@include('scripts.readmoreitem')
<script type="text/javascript">
    // global
    var filterdata = function(d){
        d._token = "{{ csrf_token() }}";
        @yield('filterdata')
    }
    var dt = "";
    var dt2 = "";
    var dt3 = "";
    var dt4 = "";
    $(document).ready(function() {
        /*Start Of Online*/
        dt = $('#listTable').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
		        destroy: true,
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				lengthChange: false,
				pageLength: 10,
				info:     true,
				filter: false,
				sorting: [],
                language: {
					url: "{{ asset('plugins/datatables/Indonesian.json') }}"
				},
                ajax:  {
                    url: "{{ url($pageUrl.'grid') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct']) !!},
                drawCallback: function() {
                    readMoreItem('list-more1');
                }
            }
        );

        dt.on('draw.dt', function () {
            dt.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
            dt.column(12, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            	var time = cell.innerHTML;
            	var d = new Date(time);
            	d.setDate(d.getDate()+4);
            	var countDownDate = new Date(d).getTime();
				// Update the count down every 1 second
				var x = setInterval(function() {
				// Get todays date and time
				var now = new Date().getTime();
				// Find the distance between now and the count down date
				var distance = countDownDate - now;
				// Time calculations for days, hours, minutes and seconds
				var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);
				// Output the result in an element with id="demo"
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'><a class='ui yellow label'><b>-</b> &nbsp;&nbsp;" + days + "H&nbsp;&nbsp;" + hours + "J&nbsp;&nbsp;"
				+ minutes + "M&nbsp;&nbsp;" + seconds + "D&nbsp;&nbsp;" + "</a></div>";
				if (distance < 1000) {
					clearInterval(x);
					// Update the count down every 1 second
					var dd = new Date(time);
	            	dd.setDate(dd.getDate());
	            	var untuk_lampau = new Date(dd).getTime();
					var z = setInterval(function() {

					// Get todays date and time
					var sekarang = new Date().getTime();

					// Find the distance between sekarang an the count down date
					var panjang = sekarang - untuk_lampau;

					// Time calculations for days, hours, minutes and seconds
					var sdays = Math.floor(panjang / (1000 * 60 * 60 * 24));
					var shours = Math.floor((panjang % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
					var sminutes = Math.floor((panjang % (1000 * 60 * 60)) / (1000 * 60));
					var sseconds = Math.floor((panjang % (1000 * 60)) / 1000);

					// Output the result in an element with id="demo"
					start = cell.innerHTML;
					cell.innerHTML = "<div class='text-center'><a class='ui red label'><b>+</b> &nbsp;&nbsp;" + sdays + "H&nbsp;&nbsp;" + shours + "J&nbsp;&nbsp;"
					+ sminutes + "M&nbsp;&nbsp;" + sseconds + "D&nbsp;&nbsp;" + "</a></div>";
					}, 1000);

				}
				}, 1000);
            });
        }).draw();

        $('#btn-filter').on('click', function(e) {
            dt.draw();
            e.preventDefault();
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt.draw();
                e.preventDefault();
            }
        })
        /*End Of Online*/

        /*Start Of Diskusi*/
        dt2 = $('#listTable2').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
		        destroy: true,
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				lengthChange: false,
				pageLength: 10,
				info:     true,
				filter: false,
				sorting: [],
                language: {
					url: "{{ asset('plugins/datatables/Indonesian.json') }}"
				},
                ajax:  {
                    url: "{{ url($pageUrl.'histori') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct2']) !!},
                drawCallback: function() {
                    readMoreItem('list-more2');
                }
            }
        );

        dt2.on('draw.dt2', function () {
            dt2.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
            dt2.column(9, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            	var indexNumber = 0;

            	function colorChange() {
            		var colors = ["one", "two", "three"];
            		$('.top-barz').removeClass("one two three").addClass(colors[indexNumber]);
            	}
            	setInterval(function () {
            		colorChange();
            		indexNumber++;
            		if (indexNumber == 3) {
            			indexNumber = 0
            		}
            	}, 1000);
            });
        }).draw();

        $('#btn-filter').on('click', function(e) {
            dt2.draw();
            e.preventDefault();
        });
        $(".number").on("keypress keyup blur",function (e) {    
                $(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
                // $(this).val($(this).val());
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt2.draw();
                e.preventDefault();
            }
        })
        /*End Of Diskusi*/


        /*Start Of Diskusi*/
        dt3 = $('#listTable3').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
		        destroy: true,
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				lengthChange: false,
				pageLength: 10,
				info:     true,
				filter: false,
				sorting: [],
                language: {
					url: "{{ asset('plugins/datatables/Indonesian.json') }}"
				},
                ajax:  {
                    url: "{{ url($pageUrl.'kembali') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct3']) !!},
                // drawCallback: function() {
                //     readMoreItem('list-more2');
                // }
            }
        );

        dt3.on('draw.dt3', function () {
            dt3.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
            dt3.column(9, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            	var indexNumber = 0;

            	function colorChange() {
            		var colors = ["one", "two", "three"];
            		$('.top-barz').removeClass("one two three").addClass(colors[indexNumber]);
            	}
            	setInterval(function () {
            		colorChange();
            		indexNumber++;
            		if (indexNumber == 3) {
            			indexNumber = 0
            		}
            	}, 1000);
            });
        }).draw();

        $('#btn-filter').on('click', function(e) {
            dt3.draw();
            e.preventDefault();
        });
        $(".number").on("keypress keyup blur",function (e) {    
                $(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
                // $(this).val($(this).val());
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt3.draw();
                e.preventDefault();
            }
        })
        /*End Of Diskusi*/


         /*Start Of Diskusi*/
        dt4 = $('#listTable4').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
		        destroy: true,
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				lengthChange: false,
				pageLength: 10,
				info:     true,
				filter: false,
				sorting: [],
                language: {
					url: "{{ asset('plugins/datatables/Indonesian.json') }}"
				},
                ajax:  {
                    url: "{{ url($pageUrl.'historiKembali') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct4']) !!},
                // drawCallback: function() {
                //     readMoreItem('list-more2');
                // }
            }
        );

        dt4.on('draw.dt4', function () {
            dt4.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
            dt4.column(9, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            	var indexNumber = 0;

            	function colorChange() {
            		var colors = ["one", "two", "three"];
            		$('.top-barz').removeClass("one two three").addClass(colors[indexNumber]);
            	}
            	setInterval(function () {
            		colorChange();
            		indexNumber++;
            		if (indexNumber == 3) {
            			indexNumber = 0
            		}
            	}, 1000);
            });
        }).draw();

        $('#btn-filter').on('click', function(e) {
            dt4.draw();
            e.preventDefault();
        });
        $(".number").on("keypress keyup blur",function (e) {    
                $(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
                // $(this).val($(this).val());
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt4.draw();
                e.preventDefault();
            }
        })

        $('.filter.button').on('click', function(e) {
            dt.draw();
            dt2.draw();
            dt3.draw();
            dt4.draw();
            // dt2.ajax.reload();
            e.preventDefault();
        });

        $('.reset.button').on('click', function(e) {
            $('.dropdown .delete').trigger('click');
            setTimeout(function(){
                dt.draw();
                dt2.draw();
                dt3.draw();
                dt4.draw();
            }, 100);
            $('.dropdown').dropdown('clear');
        });
        /*End Of Diskusi*/
    });
</script>