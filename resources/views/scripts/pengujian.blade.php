@include('scripts.readmoreitem')
<script type="text/javascript">
    // global
    var filterdata = function(d){
        d._token = "{{ csrf_token() }}";
        @yield('filterdata')
    }
    var dt = "";
    var dt2 = "";
    var dt3 = "";
    var dt4 = "";
    var dt5 = "";
    $(document).ready(function() {
        $.fn.dataTable.ext.errMode = 'none';
        /*Start*/
        dt = $('#listTable').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
                destroy: true,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                lengthChange: false,
                pageLength: 10,
                info:     true,
                filter: false,
                sorting: [],
                language: {
                    url: "{{ asset('plugins/datatables/Indonesian.json') }}"
                },
                ajax:  {
                    url: "{{ url($pageUrl.'grid') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct']) !!},
                drawCallback: function() {
                    readMoreItem('list-more1');
                },
            }
        );

        dt.on('draw.dt', function () {
            dt.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });

        }).draw();

        $('#btn-filter').on('click', function(e) {
            dt.draw();
            e.preventDefault();
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt.draw();
                e.preventDefault();
            }
        })
        /*End*/

        /*Start*/
        dt2 = $('#listTable2').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
                destroy: true,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                lengthChange: false,
                pageLength: 10,
                info:     true,
                filter: false,
                sorting: [],
                language: {
                    url: "{{ asset('plugins/datatables/Indonesian.json') }}"
                },
                ajax:  {
                    url: "{{ url($pageUrl.'grid2') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct2']) !!},
                drawCallback: function() {
                    readMoreItem('list-more2');
                }
            }
        );

        dt2.on('draw.dt2', function () {
            dt2.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
            // readMoreItem('list-more2');

        }).draw();

        $('#btn-filter').on('click', function(e) {
            dt2.draw();
            e.preventDefault();
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt2.draw();
                e.preventDefault();
            }
        })
        /*End*/

        /*Start*/
        dt3 = $('#listTable3').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
                destroy: true,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                lengthChange: false,
                pageLength: 10,
                info:     true,
                filter: false,
                sorting: [],
                language: {
                    url: "{{ asset('plugins/datatables/Indonesian.json') }}"
                },
                ajax:  {
                    url: "{{ url($pageUrl.'grid3') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct3']) !!},
                drawCallback: function() {
                    readMoreItem('list-more3');
                }
            }
        );

        dt3.on('draw.dt3', function () {
            dt3.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
            // readMoreItem('list-more2');

        }).draw();

        $('#btn-filter').on('click', function(e) {
            dt3.draw();
            e.preventDefault();
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt3.draw();
                e.preventDefault();
            }
        })
        /*End*/

        /*Start*/
        dt4 = $('#listTable4').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
                destroy: true,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                lengthChange: false,
                pageLength: 10,
                info:     true,
                filter: false,
                sorting: [],
                language: {
                    url: "{{ asset('plugins/datatables/Indonesian.json') }}"
                },
                ajax:  {
                    url: "{{ url($pageUrl.'grid4') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct4']) !!},
                drawCallback: function() {
                    readMoreItem('list-more4');
                }
            }
        );

        dt4.on('draw.dt4', function () {
            dt4.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
            // readMoreItem('list-more2');

        }).draw();

        $('#btn-filter').on('click', function(e) {
            dt4.draw();
            e.preventDefault();
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt4.draw();
                e.preventDefault();
            }
        })
        /*End*/

        /*Start*/
        dt5 = $('#listTable5').DataTable(
            {
                dom: 'rt<"bottom"ip><"clear">',
                destroy: true,
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                lengthChange: false,
                pageLength: 10,
                info:     true,
                filter: false,
                sorting: [],
                language: {
                    url: "{{ asset('plugins/datatables/Indonesian.json') }}"
                },
                ajax:  {
                    url: "{{ url($pageUrl.'grid5') }}",
                    type: 'POST',
                    data: filterdata
                },
                columns: {!! json_encode($structs['listStruct5']) !!},
                drawCallback: function() {
                    readMoreItem('list-more5');
                    readMoreItem('list-more6');
                }
            }
        );

        dt5.on('draw.dt5', function () {
            dt5.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                start = cell.innerHTML;
                cell.innerHTML = "<div class='text-center'>" + (parseInt(start) + (i+1))+ "</div>";
            });
            // readMoreItem('list-more2');

        }).draw();

        $('#btn-filter').on('click', function(e) {
            dt5.draw();
            e.preventDefault();
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt5.draw();
                e.preventDefault();
            }
        })

        $('.filter.button').on('click', function(e) {
            dt.draw();
            dt2.draw();
            dt3.draw();
            dt4.draw();
            dt5.draw();
            // dt2.ajax.reload();
            e.preventDefault();
        });

        $('.reset.button').on('click', function(e) {
            $('.dropdown .delete').trigger('click');
            setTimeout(function(){
                dt.draw();
                dt2.draw();
                dt3.draw();
                dt4.draw();
                dt5.draw();
            }, 100);
            $('.dropdown').dropdown('clear');
        });
        /*End*/
    });
</script>
