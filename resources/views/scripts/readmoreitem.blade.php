<script type="text/javascript">
	function readMoreItem(element){
		$("."+element).each(function() {
			var display = parseInt($(this).data('display'));
			var list = 0;
			$(this).find('.item').each(function(){
				if(list>=display){
					$(this).addClass('hidden-item')
					$(this).prop("style", "display:none")
				}
				list++;
			});
			if(list-1 >= display){
				var html = `<a class="button-more" style="cursor:pointer;color:blue;">Selengkapnya.....</a>`;
				$(this).append(html);
			}
		});

		$(document).on('click', '.'+element+' .button-more', function (e) {
			$(this).closest('.'+element).find('.hidden-item').each(function(){
				$(this).prop('style','');
			});
			$(this).removeClass('button-more');
			$(this).addClass('button-less');
			$(this).text('Kecilkan.....');
		});

		$(document).on('click', '.'+element+' .button-less', function (e) {
			$(this).closest('.'+element).find('.hidden-item').each(function(){
				$(this).prop("style", "display:none")
			});
			$(this).removeClass('button-less');
			$(this).addClass('button-more');
			$(this).text('Selengkapnya.....');
		});
	}
</script>