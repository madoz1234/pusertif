<script type="text/javascript">
function goBack() {
	window.history.back();
}

$('.special.cards .image').dimmer({
	on: 'hover'
});

$('.ui.vertical.footer.segment.seconds').hide();
$(document).on('click', '.item', function(e) {

	$('.ui.vertical.footer.segment.firsts').hide();
	$('.ui.vertical.footer.segment.seconds').show();
});

$(document).on('click', '.add-page.button', function(e){
	var url = "{{ url($pageUrl) }}/create";
	window.location = url;
});

$(document).on('click', '.add.page.button', function(e){
	var url = "{{ url($pageUrl) }}/create";
	window.location = url;
});

$(document).on('change', '.show.child', function(e) {
	var flagChild = $(this).data('show');
	var dataChild = $(this).data('target');
	var check = false;
	if($(this).prop('checked') == true)
	{
		var check = true;
	}
	$(".attached.segment").each(function(index, value) {
		if($(value).data('parent') == dataChild && $(value).data('flag') == flagChild)
		{
			if(check == true)
			{
				$(value).show();
				$(value).find('input:checkbox').prop('checked', true);
				$(value).find('.attached.segment').show();
				$(value).find('input:checkbox').trigger('checked');
			}else{
				$(value).hide();
			}
		}
	});
});

$(document).on('click', '.edit-page.button', function(e){
	var id = $(this).data('id');
	var url = "{{ url($pageUrl) }}/"+id+"/edit";
	window.location = url;
});
$(document).on('click', '.view-page.button', function(e){
	var id = $(this).data('id');
	var url = "{{ url($pageUrl) }}/"+id;
	window.location = url;
});

$(document).on('click', '.others-page.button', function(e){
	var id = $(this).data('id');
	var urls = $(this).data('url');
	var url = "{{ url($pageUrl) }}/"+urls+'/'+id;
	window.location = url;
});

$(document).on('click', '.detail-page.button', function(e){
	var id = $(this).data('id');
	var url = "{{ url($pageUrl) }}/detail/"+id;
	window.location = url;
});

$(document).on('submit', '#dataForm', function(e){
	return false;
});

	{{--  $(document).on('keydown', '.field input', function(e){
		if(e.keyCode == 13){
			e.preventDefault();
			$( ".save.button" ).trigger( "click" );
		}
	});  --}}

	$(document).on('click', '.add.button', function(e){
		var url = "{{ url($pageUrl) }}/create";
		loadModal(url);
	});

	$(document).on('click', '.buat.button', function(e){
		$('.ui.warning.export.message').removeClass('visible');
		$('.ui.warning.export.message').addClass('hidden');
		$('.ui.warning.pendaftaran.message').removeClass('visible');
		$('.ui.warning.pendaftaran.message').addClass('hidden');
		var layanan = $("select[name='jenis_pelayanan_id']").val();
		var lingkup = $("select[name='lingkup_id']").val();
		if(layanan == '' || lingkup == '' || layanan == null || lingkup == null){
			$('.ui.warning.pendaftaran.message').removeClass('hidden');
			$('.ui.warning.pendaftaran.message').addClass('visible');
		}else{
			var url = "{{ url($pageUrl) }}/buat";
			loadModalPengujian({
				'url' : url,
				'modal' : '{{ $modalSize }} modal',
			})
		}
	});

	$(document).on('click', '.upload.button', function(e){
		$('.ui.warning.export.message').removeClass('visible');
		$('.ui.warning.export.message').addClass('hidden');
		$('.ui.warning.pendaftaran.message').removeClass('visible');
		$('.ui.warning.pendaftaran.message').addClass('hidden');
		var layanan = $("select[name='jenis_pelayanan_id']").val();
		var lingkup = $("select[name='lingkup_id']").val();
		if(layanan == '' || lingkup == '' || layanan == null || lingkup == null){
			$('.ui.warning.pendaftaran.message').removeClass('hidden');
			$('.ui.warning.pendaftaran.message').addClass('visible');
		}else{
			var url = "{{ url($pageUrl) }}/upload/"+lingkup;
			loadModalUpload({
				'url' : url,
				'modal' : '{{ $modalSize }} modal',
			})
		}
	});
	$(document).on('click', '.edit.button', function(e){
		event.preventDefault();
		var id = $(this).data('id');
		// /* Act on the event */
		loadModal({
			'url' : '{{ url($pageUrl) }}/'+id+'/edit',
			'modal' : '.{{ $modalSize }}.modal',
			'formId' : '#dataForm',
			'onShow' : function(){ 
				onShow();
			},
		})
	});

	$(document).on('click', '.detail.button', function(e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/"+id+"/detail";

		loadModal(url);
	});

	$(document).on('click', '.catatan.button', function(e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/"+id+"/catatan";

		loadModal(url);
	});

	$(document).on('click', 'i.download.icon', function (e) {
		$(this).closest('.icon.button').trigger('click');
	});

	$(document).on('click', '.ui.red.icon.button.hapus.file', function(e){
		var id = $(e.target).parent().parent().find('input:text').data('id');
		swal({
			title: 'Apakah anda yakin?',
			text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Hapus',
			cancelButtonText: 'Batal'
		}).then((result) => {
			$('#lampiran-area').html(`<div class="ui active inverted dimmer">
				<div class="ui text loader">Loading</div>
				</div>`);
			if (result) {
				$.ajax({
					url: '{{ url("delete")}}/'+id,
					type: 'GET',
					success: function(resp){
						swal(
							'Terhapus!',
							'Data berhasil dihapus.',
							'success'
							).then(function(e){
								$('#lampiran-area').html(resp);
								if($('#hadirinUploadModal').length == 1){
									location.reload();
								}
							});
						},
						error : function(resp){
							swal(
								'Gagal!',
								'Data gagal dihapus.',
								'error'
								).then(function(e){
							// location.reload();
						});
							}
						});

			}
		})
	});
	
	$(document).on('click', '.ui.red.icon.button.hapus-picture.file', function(e){
		var id = $(e.target).parent().parent().find('input:text').data('id');

		swal({
			title: 'Apakah anda yakin?',
			text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Hapus',
			cancelButtonText: 'Batal'
		}).then((result) => {
			$('#foto-area').html(`<div class="ui active inverted dimmer">
				<div class="ui text loader">Loading</div>
				</div>`);
			if (result) {
				$.ajax({
					url: '{{ url("hapus-picture")}}/'+id,
					type: 'GET',
					success: function(resp){
						swal(
							'Terhapus!',
							'Data berhasil dihapus.',
							'success'
							).then(function(e){
								$('#foto-area').html(resp);
							});
						},
						error : function(resp){
							swal(
								'Gagal!',
								'Data gagal dihapus.',
								'error'
								).then(function(e){
								});
							}
						});

			}
		})
	});

	$(document).on('click', '.delete.button', function(e){
		var id = $(this).data('id');
		swal({
			title: 'Apakah anda yakin?',
			text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Hapus',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
					url: '{{ url($pageUrl) }}/'+id,
					type: 'POST',
					data: {_token: "{{ csrf_token() }}", _method: "delete"},
					success: function(resp){
						swal(
							'Terhapus!',
							'Data berhasil dihapus.',
							'success'
							).then(function(e){
								dt.draw();
							});
						},
						error : function(resp){
							swal(
								'Gagal!',
								'Data gagal dihapus, karena sedang dipakai',
								'error'
								).then(function(e){
									dt.draw();
								});
							}
						});

			}
		})
	});

	$(document).on('click', '.publish.button', function(e){
		var id = $(this).data('id');

		swal({
			title: 'Apakah anda yakin?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Publish',
			cancelButtonText: 'Batal'
		});
	});

	$(document).on('click', '.approve.button', function(e){
		var id = $(this).data('id');

		swal({
			title: 'Apakah anda yakin?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Setujui',
			cancelButtonText: 'Batal'
		});
	});

	$(document).on('click', '.reject.button', function(e){
		var id = $(this).data('id');
		swal({
			title: 'Apakah anda yakin?',
			text: "Data yang sudah ditolak, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Tolak',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
					url: '{{ url($pageUrl) }}/'+id,
					type: 'POST',
					data: {_token: "{{ csrf_token() }}", _method: "delete"},
					success: function(resp){
						swal(
							'Berhasil!',
							'Data berhasil ditolak.',
							'success'
							).then(function(e){
								window.location.reload()
							});
						},
						error : function(resp){
							swal(
								'Gagal!',
								'Data gagal ditolak, karena sedang dipakai',
								'error'
								).then(function(e){
									window.location.reload()
								});
							}
						});

			}
		})
	});

	$(document).on('click', '.terima.button', function(e){
		var id = $(this).data('id');

		swal({
			title: 'Apakah Data Sudah Benar?',
			// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Terima',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
					url: '{{ url($pageUrl) }}/'+id,
					type: 'POST',
					data: {_token: "{{ csrf_token() }}", _method: "delete"},
					success: function(resp){
						swal(
							'Berhasil!',
							'Kartu Telah Diterima',
							'success'
							).then(function(e){
								dt.draw();
							});
						},
						error : function(resp){
							swal(
								'Gagal!',
								'error'
								).then(function(e){
									dt.draw();
								});
							}
						});

			}
		})
	});

	function loadModal(url) {
		$('#formModal').modal({
			// inverted: true, // ver.1 background modal white
			observeChanges: true,
			closable: true,
			detachable: false,
			autofocus: false,
			onApprove : function() {
				dismissModal();
				$("#dataForm").form('validate form');
				if($("#dataForm").form('is valid')){
					$('#formModal').find('.loading.dimmer').addClass('active');
					$("#dataForm").ajaxSubmit({
						success: function(resp){
							$("#formModal").modal('hide');
							swal(
								'Tersimpan!',
								'Data berhasil disimpan.',
								'success'
								).then((result) => {
									$('#formModal').find('.loading.dimmer').removeClass('active');
									if(document.getElementById('listTable') !=null){
										if(document.getElementById('listTable2') !=null){
											window.location.reload();
										}else{
											dt.draw();
										}
									}else{
										window.location.reload();
									}
									return true;
								})
							},
							error: function(resp){
								$('#formModal').find('.loading.dimmer').removeClass('active');
								$.each(resp.responseJSON.errors, function(index, val) {
									clearError(index,val);
									showError(index,val);
								});
								time = 7;
								interval = setInterval(function(){
									time--;
									if(time == 0){
										$('.red.error-label').remove();
										$('.pointing.prompt.label.transition.visible').remove();
										$('.error').each(function (index, val) {
											$(val).removeClass('error');
										});
										clearInterval(interval);
									}
								},1000);
							}
						});
				}
				return false;
			},
			onShow: function(){
				dismissModal();
				$('#formModal').find('.loading.dimmer').addClass('active');
				$('#formModal').addClass(url.modal);
				$(this).draggable({
					cancel: ".content"
				});
				$('.menu .item').tab();
		    	$('.next').tab();
				$.get(url, { _token: "{{ csrf_token() }}" } )
				.done(function( response ) {
					$('#formModal').html(response);
					$('#dataForm').form({
						inline: true,
						fields: formRules
					});
					$('.menu .item').tab();
		    	$('.next').tab();
					$('.ui.dropdown').dropdown();
					initModal();
				});
				$('.menu .item').tab();
		    	$('.next').tab();
			},
			onHidden: function(){
				dismissModal();
				$('#formModal').html(`<div class="ui inverted loading dimmer">
					<div class="ui text loader">Loading</div>
					</div>`);
			}
		}).modal('show');
	}

	function loadModalSurat(url) {
		$('#formModal').modal({
			// inverted: true, // ver.1 background modal white
			observeChanges: true,
			closable: true,
			detachable: false,
			autofocus: false,
			onApprove : function() {
				dismissModal();
				$("#dataForm").form('validate form');
				if($("#dataForm").form('is valid')){
					$('#formModal').find('.loading.dimmer').addClass('active');
					$("#dataForm").ajaxSubmit({
						success: function(resp){
							$("#formModal").modal('hide');
							swal(
								'Tersimpan!',
								'Data berhasil disimpan.',
								'success'
								).then((result) => {
									$('#formModal').find('.loading.dimmer').removeClass('active');
									if(document.getElementById('listTable') !=null){
										if(document.getElementById('listTable2') !=null){
											window.location.reload();
										}else{
											dt.draw();
										}
									}else{
										window.location.reload();
									}
									return true;
								})
							},
							error: function(resp){
								$('#formModal').find('.loading.dimmer').removeClass('active');
								$.each(resp.responseJSON.errors, function(index, val) {
									clearError(index,val);
									showError(index,val);
								});
								time = 7;
								interval = setInterval(function(){
									time--;
									if(time == 0){
										$('.red.error-label').remove();
										$('.pointing.prompt.label.transition.visible').remove();
										$('.error').each(function (index, val) {
											$(val).removeClass('error');
										});
										clearInterval(interval);
									}
								},1000);
							}
						});
				}
				return false;
			},
			onShow: function(){
				dismissModal();
				$('#formModal').find('.loading.dimmer').addClass('active');
				$('#formModal').removeClass('mini'); 
				$('#formModal').removeClass('tiny'); 
				$('#formModal').addClass(url.modal); 
				$(this).draggable({
					cancel: ".content"
				});
				$('.menu .item').tab();
		    	$('.next').tab();
				$.get(url, { _token: "{{ csrf_token() }}" } )
				.done(function( response ) {
					$('#formModal').html(response);
					$('#dataForm').form({
						inline: true,
						fields: formRules
					});
					$('.menu .item').tab();
		    	$('.next').tab();
					$('.ui.dropdown').dropdown();
					initModal();
				});
				$('.menu .item').tab();
		    	$('.next').tab();
			},
			onHidden: function(){
				dismissModal();
				$('#formModal').addClass('mini'); 
				$('#formModal').addClass('tiny'); 
				$('#formModal').html(`<div class="ui inverted loading dimmer">
					<div class="ui text loader">Loading</div>
					</div>`);
			}
		}).modal('show');
	}

	function loadModalJadwal(url) {
		$('#formModal').modal({
			// inverted: true, // ver.1 background modal white
			observeChanges: true,
			closable: true,
			detachable: false,
			autofocus: false,
			refresh: true,
			onShow: function(){
				$('#formModal').addClass(url.modal);
				$(this).draggable({
					cancel: ".content"
				});
				$('.menu .item').tab();
		    	$('.next').tab();
				$.get(url, { _token: "{{ csrf_token() }}" } )
				.done(function( response ) {
					$('#formModal').html(response);
					$('.menu .item').tab();
		    	$('.next').tab();
					$('.ui.dropdown').dropdown();
					initModal();
				});
				$('.menu .item').tab();
		    	$('.next').tab();
			},
			onHidden: function(){
				$('#formModal').html(`<div class="ui inverted loading dimmer">
					<div class="ui text loader">Loading</div>
					</div>`);
				dismissModal();
			}
		}).modal('show');
	}

	function loadModalPengujian(url) {
		$('#formModal').modal({
			// inverted: true, // ver.1 background modal white
			observeChanges: true,
			closable: true,
			detachable: false,
			autofocus: false,
			onApprove : function() {
				$("#formPengujian").form('validate form');
				if($("#formPengujian").form('is valid')){
					$("#formPengujian").ajaxSubmit({
						success: function(resp){
							function humanizeNumber(n) {
								var parts = n.toString().split(".");
								parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
								return parts.join(",");
							}
							function escapeRegExp(str) {
								return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
							}

							function replaceAll(str, find, replace) {
								return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
							}
							// alert('sukses');
							// $("#formModal").modal('hide');
							var jenis_id = $('#jenis_id').val();
							var nama_jenis = $("#jenis_id option:selected").text();
							var merk = $('#merk').val();
							var tipe = $('#tipe').val();
							var jumlah = $('#jumlah').val();
							var satuan_id = $('#satuan_id').val();
							var nama_satuan = $("#satuan_id option:selected").text();
							var spesifikasi = $('#spesifikasi').val();
							var table = $('#detail');
							var row   = table.find('tr#data-row').last();
							var idx   = row.data('id');
							if(typeof idx !== 'undefined'){
								var idy = parseInt(idx) + 1;
							}else{
								var idy =1;
							}
							var html = `<tr class="list-row-`+idy+`" id="data-row" data-id="`+idy+`">
								<td class="center aligned numbor numboor-`+idy+`">`+idy+`</td>
								<td class="pjg">
									<div class="ui fluid input">
										<span>`+nama_jenis+`</span>
										<input type="hidden" name="detail[`+idy+`][jenis_id]" class="jenis_id" value="`+jenis_id+`">
									</div>
								</td>
								<td class="field">
									<div class="ui fluid input">
										<span class="sedit-label lrow-`+idy+`">`+merk+`</span>
										<input type="text" class="sedit-input hidden inrow-`+idy+` merk" name="detail[`+idy+`][merk]" value="`+merk+`">
									</div>
								</td>
								<td class="field">
									<div class="ui fluid input">
										<span class="sedit-label lrow-`+idy+`">`+tipe+`</span>
										<input type="text" class="sedit-input hidden inrow-`+idy+`" name="detail[`+idy+`][tipe]" value="`+tipe+`">
									</div>
								</td>
								<td style="text-align:right;">
									<span class="sedit-label lrow-`+idy+`">`+humanizeNumber(jumlah)+`</span>
									<input type="text" class="sedit-input hidden inrow-`+idy+` number" name="detail[`+idy+`][jumlah]" value="`+jumlah+`" data-inputmask="'alias' : 'numeric'">
								</td>
								<td>
									<div class="ui fluid input">
										<span>`+nama_satuan+`</span>
										<input type="hidden" name="detail[`+idy+`][satuan_id]" value="`+satuan_id+`">
									</div>
								</td>
								<td class="field">
									<div class="ui fluid input">
										<span class="sedit-label lrow-`+idy+`">`+spesifikasi+`</span>
										<input type="text" class="sedit-input hidden inrow-`+idy+`" name="detail[`+idy+`][spesifikasi]" value="`+spesifikasi+`">
									</div>
								</td>
								<td colspan="2" class="center aligned deletor kcl">
									<button class="ui green mini icon btn-save button hidden" data-id="`+idy+`" data-tooltip="{{trans('translator.Simpan Item Uji')}}" data-position="left center"><i class="save icon"></i></button>
									<button class="ui orange mini icon btn-edit button" data-id="`+idy+`" data-tooltip="{{trans('translator.Ubah Item Uji')}}" data-position="left center"><i class="pencil icon"></i></button>
									<button class="ui red mini icon hapus_data button" data-id="`+idy+`" data-tooltip="{{trans('translator.Hapus Item Uji')}}" data-position="left center"><i class="remove icon"></i></button>
								</td>
								</tr>`;
								var table = $('#detail');
								var rows = table.find('tbody tr');
								var jumlah =0;
								$.each(rows.find('.merk'), function(key, value){
									if(merk === $(this).val()){
										jumlah +=1;
									}
								});

								$("#formModal").modal('hide');
								$('.jenis_pelayanan').addClass('disabled');
								$('.lingkup').addClass('disabled');
								$('.detail.fluid.container').append(html);

								var table = $('#detail');
								var rows = table.find('tbody tr');
								$.each(rows, function(key, value){
									table.find('.numbor').last().html(key+1);
								});
							

								$('.buat.button').replaceWith("<button type='button' class='ui blue buat button' style='background-color: #363636;'><i class='plus icon'></i>{{trans('translator.Tambah Data')}}</button>");
								$(".number").on("keypress keyup blur",function (e) {    
									$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
								});
							},
							error: function(resp){
								$.each(resp.responseJSON.errors, function(index, val) {
									clearError(index,val);
									showError(index,val);
								});
								time = 7;
								interval = setInterval(function(){
									time--;
									if(time == 0){
										$('.red.error-label').remove();
										$('.pointing.prompt.label.transition.visible').remove();
										$('.error').each(function (index, val) {
											$(val).removeClass('error');
										});
										clearInterval(interval);
									}
								},1000);
							}
						});
				}
				return false;
			},
			onShow: function(){
				$('#formModal').find('.loading.dimmer').addClass('active');
				$('#formModal').addClass(url.modal);
				$(this).draggable({
					cancel: ".content"
				});
				$('.menu .item').tab();
		    	$('.next').tab();
				$.get(url, { _token: "{{ csrf_token() }}" } )
				.done(function( response ) {
					$('#formModal').html(response);
					$('#dataForm').form({
						inline: true,
					});
					$('.menu .item').tab();
		    	$('.next').tab();
					$('.ui.dropdown').dropdown();
					initModal();
				});
				$('.menu .item').tab();
		    	$('.next').tab();
			},
			onHidden: function(){
				$('#formModal').html(`<div class="ui inverted loading dimmer">
					<div class="ui text loader">Loading</div>
					</div>`);
				// dismissModal();
			}
		}).modal('show');
	}

	function loadModalUpload(url) {
		$('#formModal').modal({
			// inverted: true, // ver.1 background modal white
			observeChanges: true,
			closable: true,
			detachable: false,
			autofocus: false,
			onApprove : function() {
				$('#formModal').find('.loading.dimmer').addClass('active');
				$("#formUpload").form('validate form');
				if($("#formUpload").form('is valid')){
					$("#formUpload").ajaxSubmit({
						success: function(resp){
							function humanizeNumber(n) {
								var parts = n.toString().split(".");
								parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
								return parts.join(",");
							}
							if(resp.status == true){
								$.each(resp.data, function(key, value){
									var jenis_id = value['id_jenis_pengujian'];
									var nama_jenis = value['nama_jenis'];
									var merk = value['merk'];
									var tipe = value['tipe'];
									var jumlah = value['jumlah'];
									var satuan_id = value['id_satuan'];
									var nama_satuan = value['nama_satuan'];
									var spesifikasi = value['spesifikasi'];
									var table = $('#detail');
									var row   = table.find('tr#data-row').last();
									var idx   = row.data('id');
									if(typeof idx !== 'undefined'){
										var idy = parseInt(idx) + 1;
									}else{
										var idy =1;
									}
									var html = `<tr class="list-row-`+idy+`" id="data-row" data-id="`+idy+`">
										<td class="center aligned numbor numboor-`+idy+`">`+idy+`</td>
										<td class="pjg">
											<div class="ui fluid input">
												<span>`+nama_jenis+`</span>
												<input type="hidden" name="detail[`+idy+`][jenis_id]" class="jenis_id" value="`+jenis_id+`">
											</div>
										</td>
										<td class="field">
											<div class="ui fluid input">
												<span class="sedit-label lrow-`+idy+`">`+merk+`</span>
												<input type="text" class="sedit-input hidden inrow-`+idy+` merk" name="detail[`+idy+`][merk]" value="`+merk+`">
											</div>
										</td>
										<td class="field">
											<div class="ui fluid input">
												<span class="sedit-label lrow-`+idy+`">`+tipe+`</span>
												<input type="text" class="sedit-input hidden inrow-`+idy+`" name="detail[`+idy+`][tipe]" value="`+tipe+`">
											</div>
										</td>
										<td style="text-align:right;">
											<span class="sedit-label lrow-`+idy+`">`+humanizeNumber(jumlah)+`</span>
											<input type="text" class="sedit-input hidden inrow-`+idy+` number" name="detail[`+idy+`][jumlah]" value="`+jumlah+`" data-inputmask="'alias' : 'numeric'">
										</td>
										<td>
											<div class="ui fluid input">
												<span>`+nama_satuan+`</span>
												<input type="hidden" name="detail[`+idy+`][satuan_id]" value="`+satuan_id+`">
											</div>
										</td>
										<td class="field">
											<div class="ui fluid input">
												<span class="sedit-label lrow-`+idy+`">`+spesifikasi+`</span>
												<input type="text" class="sedit-input hidden inrow-`+idy+`" name="detail[`+idy+`][spesifikasi]" value="`+spesifikasi+`">
											</div>
										</td>
										<td colspan="2" class="center aligned deletor kcl">
											<button class="ui green mini icon btn-save button hidden" data-id="`+idy+`" data-tooltip="{{trans('translator.Simpan Item Uji')}}" data-position="left center"><i class="save icon"></i></button>
											<button class="ui orange mini icon btn-edit button" data-id="`+idy+`" data-tooltip="{{trans('translator.Ubah Item Uji')}}" data-position="left center"><i class="pencil icon"></i></button>
											<button class="ui red mini icon hapus_data button" data-id="`+idy+`" data-tooltip="{{trans('translator.Hapus Item Uji')}}" data-position="left center"><i class="remove icon"></i></button>
										</td>
										</tr>`;
										var table = $('#detail');
										var rows = table.find('tbody tr');
										var jumlah =0;
										$.each(rows.find('.merk'), function(key, value){
											if(merk === $(this).val()){
												jumlah +=1;
											}
										});

										$('.ui.warning.list-uji').addClass('hidden');
										$("#formModal").modal('hide');
										$('.jenis_pelayanan').addClass('disabled');
										$('.lingkup').addClass('disabled');
										$('.detail.fluid.container').append(html);

										var table = $('#detail');
										var rows = table.find('tbody tr');
										$.each(rows, function(key, value){
											table.find('.numbor').last().html(key+1);
										});

										$('.buat.button').replaceWith("<button type='button' class='ui blue buat button' style='background-color: #363636;'><i class='plus icon'></i>{{trans('translator.Tambah Data')}}</button>");
										$(".number").on("keypress keyup blur",function (e) {    
											$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
										});
								});
							}else{
								swal({
									text: resp.message,
									type: 'warning',
									confirmButtonColor: '#3085d6',
									confirmButtonText: 'Ya'
								}).then((result) => {
									$('#formModal').find('.loading.dimmer').removeClass('active');
									if(resp.data){
										$.each(resp.data, function(key, value){
											var jenis_id = value['id_jenis_pengujian'];
											var nama_jenis = value['nama_jenis'];
											var merk = value['merk'];
											var tipe = value['tipe'];
											var jumlah = value['jumlah'];
											var satuan_id = value['id_satuan'];
											var nama_satuan = value['nama_satuan'];
											var spesifikasi = value['spesifikasi'];
											var table = $('#detail');
											var row   = table.find('tr#data-row').last();
											var idx   = row.data('id');
											if(typeof idx !== 'undefined'){
												var idy = parseInt(idx) + 1;
											}else{
												var idy =1;
											}
											var html = `<tr class="list-row-`+idy+`" id="data-row" data-id="`+idy+`">
												<td class="center aligned numbor numboor-`+idy+`">`+idy+`</td>
												<td class="pjg">
													<div class="ui fluid input">
														<span>`+nama_jenis+`</span>
														<input type="hidden" name="detail[`+idy+`][jenis_id]" class="jenis_id" value="`+jenis_id+`">
													</div>
												</td>
												<td class="field">
													<div class="ui fluid input">
														<span class="sedit-label lrow-`+idy+`">`+merk+`</span>
														<input type="text" class="sedit-input hidden inrow-`+idy+` merk" name="detail[`+idy+`][merk]" value="`+merk+`">
													</div>
												</td>
												<td class="field">
													<div class="ui fluid input">
														<span class="sedit-label lrow-`+idy+`">`+tipe+`</span>
														<input type="text" class="sedit-input hidden inrow-`+idy+`" name="detail[`+idy+`][tipe]" value="`+tipe+`">
													</div>
												</td>
												<td style="text-align:right;">
													<span class="sedit-label lrow-`+idy+`">`+humanizeNumber(jumlah)+`</span>
													<input type="text" class="sedit-input hidden inrow-`+idy+` number" name="detail[`+idy+`][jumlah]" value="`+jumlah+`" data-inputmask="'alias' : 'numeric'">
												</td>
												<td>
													<div class="ui fluid input">
														<span>`+nama_satuan+`</span>
														<input type="hidden" name="detail[`+idy+`][satuan_id]" value="`+satuan_id+`">
													</div>
												</td>
												<td style="text-align:right;">
													<span class="sedit-label lrow-`+idy+`">`+spesifikasi+`</span>
													<input type="text" class="sedit-input hidden inrow-`+idy+`" name="detail[`+idy+`][spesifikasi]" value="`+spesifikasi+`">
												</td>
												<td colspan="2" class="center aligned deletor kcl">
													<button class="ui green mini icon btn-save button hidden" data-id="`+idy+`" data-tooltip="{{trans('translator.Simpan Item Uji')}}" data-position="left center"><i class="save icon"></i></button>
													<button class="ui orange mini icon btn-edit button" data-id="`+idy+`" data-tooltip="{{trans('translator.Ubah Item Uji')}}" data-position="left center"><i class="pencil icon"></i></button>
													<button class="ui red mini icon hapus_data button" data-id="`+idy+`" data-tooltip="{{trans('translator.Hapus Item Uji')}}" data-position="left center"><i class="remove icon"></i></button>
												</td>
												</tr>`;
												var table = $('#detail');
												var rows = table.find('tbody tr');
												var jumlah =0;
												$.each(rows.find('.merk'), function(key, value){
													if(merk === $(this).val()){
														jumlah +=1;
													}
												});

												$('.ui.warning.list-uji').addClass('hidden');
												$("#formModal").modal('hide');
												$('.jenis_pelayanan').addClass('disabled');
												$('.lingkup').addClass('disabled');
												$('.detail.fluid.container').append(html);

												var table = $('#detail');
												var rows = table.find('tbody tr');
												$.each(rows, function(key, value){
													table.find('.numbor').last().html(key+1);
												});

												$('.buat.button').replaceWith("<button type='button' class='ui blue buat button' style='background-color: #363636;'><i class='plus icon'></i>{{trans('translator.Tambah Data')}}</button>");
												$(".number").on("keypress keyup blur",function (e) {    
													$(this).val($(this).val().replace(/[^0-9]/g, '').replace(/^0+/, ''));
											});
										});
									}
								})
							}
						},
						error: function(resp){
							swal({
								text: 'Mohon periksa kembali file yang anda upload',
								type: 'warning',
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'Ya'
							}).then((result) => {
								$('#formModal').find('.loading.dimmer').removeClass('active');
							})
						}
					});
				}
				return false;
			},
			onShow: function(){
				$('#formModal').find('.loading.dimmer').addClass('active');
				$('#formModal').addClass(url.modal);
				$(this).draggable({
					cancel: ".content"
				});
				$('.menu .item').tab();
		    	$('.next').tab();
				$.get(url, { _token: "{{ csrf_token() }}" } )
				.done(function( response ) {
					$('#formModal').html(response);
					$('#dataForm').form({
						inline: true,
					});
					$('.menu .item').tab();
		    	$('.next').tab();
					$('.ui.dropdown').dropdown();
					initModal();
				});
				$('.menu .item').tab();
		    	$('.next').tab();
			},
			onHidden: function(){
				$('#formModal').html(`<div class="ui inverted loading dimmer">
					<div class="ui text loader">Loading</div>
					</div>`);
				// dismissModal();
			}
		}).modal('show');
	}

	function showLoadingInput(elemchild)
	{
		var loading = `<div class="ui active mini centered inline loader"></div>`;

		$('#'+elemchild).parent().closest('.field').addClass('disabled');
		$('#'+elemchild).parent().closest('.field').append(loading);
	}

	function  stopLoadingInput(elemchild)
	{
		$('#'+elemchild).parent().closest('.field').removeClass('disabled');
		$('#'+elemchild).parent().closest('.field').find('.inline.loader').remove();
	}

	function injectLoading(element)
	{
		$(element).html(`
			<div class="ui active inverted dimmer">
			<div class="ui text loader">Sedang Mengambil Data</div>
			</div>`);
	}

	function stopInjectLoading(element)
	{
		$(element).html(``);
	}

	function saveFormSafetyPatrol(){
		$('.negative.message').hide();
		$('.pointing.prompt.label.transition.visible').hide();
		$('.error').each(function (index, val) {
			$(val).removeClass('error');
		});
		if($("#dataForm").form('is valid')){
			$('#cover').show();
			$("#dataForm").ajaxSubmit({
				success: function(resp){

					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							$('#cover').hide();
							window.location = "{{ url($pageUrl) }}";
							return true;
						})
					},
					error: function(resp){

						var html = `<div class="ui negative message">
						<i class="close icon" id="close"></i>
						<div class="header">
						Data harus dilengkapi terlebih dahulu
						</div>
						<p>Semua data mohon diisi dan dilengkapi terlebih dahulu, Dan pastikan isi data Tindak Lanjut.
						</p></div>`;
						$('#cover').hide();
						$('#inner2bottom').prepend(html);
						$.each(resp.responseJSON, function(index, val) {
							console.log('index',index);
							console.log('val',val);
							clearFormErrorSafety(index,val);
							showFormErrorSafety(index,val);

						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);

								$('.pointing.prompt.label.transition.visible').remove();

							}
						},1000)
					}
				});
		}
	}


	function saveFormAnotherID()
	{
		if($("#dataForms").form('is valid')){
			$('#cover').show();
			$("#dataForms").ajaxSubmit({
				success: function(resp){
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							$('#cover').hide();
							window.location = "{{ url($pageUrl) }}";
							return true;
						})
					},
					error: function(resp){
						$('#cover').hide();
						$.each(resp.responseJSON, function(index, val) {
							clearFormError(index,val);
							showFormError(index,val);
						});
					}
				});
		}
	}

	function saveForm(form="dataForm")
	{
		if($("#"+form).form('is valid')){
			$('#formModal').find('.loading.dimmer').addClass('active');
			$('#cover').show();
			$("#"+form).ajaxSubmit({
				success: function(resp){
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							if(postSave(resp)){
								return true;
							}else{
								$('#cover').hide();
								window.location = "{{ url($pageUrl) }}";
								return true;
							}
							$('#formModal').find('.loading.dimmer').removeClass('active');
						})
					},
					error: function(resp){
						if (resp.responseJSON.status !== "undefined" && resp.responseJSON.status === 'false'){
							swal(
								'Oops, Maaf!',
								resp.responseJSON.message,
								'error'
								)
						}

						$('#cover').hide();
						var response = resp.responseJSON;
						$.each(response.errors, function(index, val) {
							clearFormError(index,val);
							showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
						},1000)
						$('#formModal').find('.loading.dimmer').removeClass('active');
					}
				});
		}else{
			$("#"+form).ajaxSubmit({
				success: function(resp){
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							if(postSave(resp)){
								return true;
							}else{
								$('#cover').hide();
								window.location = "{{ url($pageUrl) }}";
								return true;
							}
						})
					},
					error: function(resp){
						$.each(resp.responseJSON, function(index, val) {
							clearFormError(index,val);
							showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.message').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
						},1000)
					}
				});
		}
	}

	function saveFormDaftar(form="dataForm")
	{
		if($("#"+form).form('is valid')){
			$('#cover').show();
			$("#"+form).ajaxSubmit({
				success: function(resp){
					var msg =resp.message;
					console.log(msg);
					$("#formModal").modal('hide');
					swal(
						'Berhasil!',
						'Order Anda telah berhasil diproses <br> dengan No. Order : '+ msg,
						'success'
						).then((result) => {
							if(postSave(resp)){
								return true;
							}else{
								$('#cover').hide();
								window.location = "{{ url($pageUrl) }}";
								return true;
							}
						})
					},
					error: function(resp){
						if (resp.responseJSON.status !== "undefined" && resp.responseJSON.status === 'false'){
							swal(
								'Oops, Maaf!',
								resp.responseJSON.message,
								'error'
								)
						}

						$('#cover').hide();
						$.each(resp.responseJSON.errors, function(index, val) {
								clearFormError(index,val);
								showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.ui.left.pointing.red.basic.label').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
						},1000)
					}
				});
		}else{
			$("#"+form).ajaxSubmit({
				success: function(resp){
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							if(postSave(resp)){
								return true;
							}else{
								$('#cover').hide();
								window.location = "{{ url($pageUrl) }}";
								return true;
							}
						})
					},
					error: function(resp){
						$.each(resp.responseJSON, function(index, val) {
							clearFormError(index,val);
							showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.message').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
						},1000)
					}
				});
		}
	}

	function saveFormSurat(form="dataForm")
	{
		if($("#"+form).form('is valid')){
			$('#cover').show();
			$("#"+form).ajaxSubmit({
				success: function(resp){
					$('#formModal').find('.loading.dimmer').addClass('active');
					var msg =resp.message;
					var tipe =resp.kategori;
					if(tipe == 0){
						var pesan = 'Surat Penawaran berhasil tersimpan';
					}else if(msg){
						var pesan = 'Surat Penawaran berhasil tersimpan <br> dengan No. Virtual Account (VA) :'+msg;
					}else{
						var pesan = 'Surat Penawaran berhasil tersimpan';
					}
					$("#formModal").modal('hide');
					swal(
						'Berhasil!',
						pesan,
						'success'
						).then((result) => {
							if(postSave(resp)){
								return true;
							}else{
								$('#cover').hide();
								window.location = "{{ url($pageUrl) }}";
								return true;
							}
						})
					},
					error: function(resp){
						if (resp.responseJSON.status !== "undefined" && resp.responseJSON.status === 'false'){
							swal(
								'Oops, Maaf!',
								resp.responseJSON.message,
								'error'
								)
						}

						$('#cover').hide();
						$.each(resp.responseJSON.errors, function(index, val) {
								clearFormError(index,val);
								showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.ui.left.pointing.red.basic.label').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
						},1000)
					}
				});
		}else{
			$("#"+form).ajaxSubmit({
				success: function(resp){
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							if(postSave(resp)){
								return true;
							}else{
								$('#cover').hide();
								window.location = "{{ url($pageUrl) }}";
								return true;
							}
						})
					},
					error: function(resp){
						$.each(resp.responseJSON, function(index, val) {
							clearFormError(index,val);
							showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.message').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
						},1000)
					}
				});
		}
	}

	function saveFormRedirect()
	{
		if($("#dataForm").form('is valid')){
			$('#cover').show();
			$("#dataForm").ajaxSubmit({
				success: function(resp){
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							console.log(resp);
							$('#cover').hide();
							window.location = "{{ url($pageUrl) }}";
							return true;
						})
					},
					error: function(resp){
						$('#cover').hide();
						$.each(resp.responseJSON, function(index, val) {
							clearFormError(index,val);
							showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
						},1000)
					}
				});
		}
	}

	function saveFormSelfRedirect()
	{
		if($("#dataForm").form('is valid')){
			$('#cover').show();
			$("#dataForm").ajaxSubmit({
				success: function(resp){
					$("#formModal").modal('hide');
					swal({
						title: 'Tersimpan!',
						text: 'Data berhasil disimpan.',
						type: 'success',
						timer: 1000,
						showConfirmButton: false,
						onOpen: function(e){
							console.log(resp);
							location.reload();
						}
					}).catch(swal.noop);
					// .then((result) => {
					// 	console.log(resp);
					// 	$('#cover').hide();
					// 	location.reload();
					// 	return true;
					// })
				},
				error: function(resp){
					$('#cover').hide();
					$.each(resp.responseJSON, function(index, val) {
						clearFormError(index,val);
						showFormError(index,val);
					});
					time = 5;
					interval = setInterval(function(){
						time--;
						if(time == 0){
							clearInterval(interval);
							$('.pointing.prompt.label.transition.visible').remove();
							$('.error').each(function (index, val) {
								$(val).removeClass('error');
							});
						}
					},1000)
				}
			});
		}
	}

	function saveFormUrlRedirect(target)
	{
		if($("#dataForm").form('is valid')){
			$('#cover').show();
			$("#dataForm").ajaxSubmit({
				success: function(resp){
					$("#formModal").modal('hide');
					swal({
						title: 'Tersimpan!',
						text: 'Data berhasil disimpan.',
						type: 'success',
						timer: 1000,
						showConfirmButton: false,
						onOpen: function(e){
							console.log(resp);
							window.location = target;
						}
					}).catch(swal.noop);
					return true;
				},
				error: function(resp){
					if (resp.responseJSON.status !== "undefined" && resp.responseJSON.status === 'false'){
						swal(
							'Oops, Maaf!',
							resp.responseJSON.message,
							'error'
							)
					}

					$('#cover').hide();
					$.each(resp.responseJSON, function(index, val) {
						clearFormError(index,val);
						showFormError(index,val);
					});
					time = 5;
					interval = setInterval(function(){
						time--;
						if(time == 0){
							clearInterval(interval);
							$('.pointing.prompt.label.transition.visible').remove();
							$('.error').each(function (index, val) {
								$(val).removeClass('error');
							});
						}
					},1000)
				}
			});
		}
	}

	function readURL(input) {
		var container = $(input).closest('.image-container');
		if(input.files[0].size > 5242880){
			swal(
				"Gagal",
				"Ukuran file terlalu besar, silahkan upload file dengan ukuran dibawah 5MB",
				"error"
				)
			container.find(".attachment").val("");
		}else{
			if(input.files && input.files[0])
			{
				var reader = new FileReader();

				reader.onload = function (e) {
					container.find('.image-preview').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}
	}

	$(document).on('change', '.attachment', function () {
		readURL(this);
	});

	$(document).ready(function() {
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$(input).closest('.area.picture').find('.show.picture').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}
		var lengthAttachment = $('input[name^="attachment"]').length;

		function getAttachmentInput(length) {
			$('input[name="attachment['+length+']"]').click()
		}

		function multiAppendUrl(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('img[name="img['+lengthAttachment+']"]').parent().parent().show();
					$('img[name="img['+lengthAttachment+']"]').attr('src', e.target.result);
				}
				lengthAttachment = lengthAttachment + 1;
				var html = `<div class="card">
				<div class="image">
				<img class="ui image" src="" name="img[`+lengthAttachment+`]">
				<input type="file" name="attachment[`+lengthAttachment+`]" style="display:none;" accept="image/*">
				</div>
				</div>`;
				$('#multiShowPic').append(html);
				reader.readAsDataURL(input.files[0]);

				$('input[name="attachment['+lengthAttachment+']"]').change(function () {
					multiAppendUrl(this);
				});
			}
		}

		$(document)
		.on('click', '.ui.teal.button.safety-file', function(e) {
			getAttachmentInput(lengthAttachment);
		});

		$(".browse.picture").change(function () {
			readURL(this);
		});

		$('input[name="attachment['+lengthAttachment+']"]').change(function () {
			multiAppendUrl(this);
		});

		clearError = function(key, value)
		{
			if(key.includes("."))
			{
				res = key.split('.');
				key = res[0] + '[' + res[1] + ']';
				if(res[1] == 0)
				{
					key = res[0] + '\\[\\]';
				}
				console.log(key);
			}
			var elm = $('#formModal' + ' [name=' + key + ']').closest('.field');
			$(elm).removeClass('error');
			console.log($('#formModal' + ' [name=' + key + ']').closest('.field').parents('td'));

			var showerror = $('#formModal' + ' [name=' + key + ']').closest('.field').find('.ui.basic.red.pointing.prompt.label.transition.visible').remove();
		}

		showError = function(key, value)
		{
			if(key.includes("."))
			{
				res = key.split('.');
				key = res[0] + '[' + res[1] + ']';
				if(res[1] == 0)
				{
					key = res[0] + '\\[\\]';
				}
				console.log(key);
			}

			var elm = $('#formModal' + ' [name=' + key + ']').closest('.field');
			$(elm).addClass('error');
			var message = `<div class="ui basic red pointing prompt label transition visible">`+ value +`</div>`;
			var showerror = $('#formModal' + ' [name=' + key + ']').closest('.field');
			$(showerror).append('<div class="ui basic red pointing prompt label transition visible">' + value + '</div>');
		}

		clearElementError = function(key, value, element)
		{
			if(key.includes("."))
			{
				res = key.split('.');
				key = res[0] + '[' + res[1] + ']';
				if(res[1] == 0)
				{
					key = res[0] + '\\[\\]';
				}
				console.log(key);
			}
			var elm = $(element + ' [name=' + key + ']').closest('.field');
			$(elm).removeClass('error');

			var showerror = $(element + ' [name=' + key + ']').closest('.field').find('.ui.basic.red.pointing.prompt.label.transition.visible').remove();
		}

		showElementError = function(key, value, element)
		{
			if(key.includes("."))
			{
				res = key.split('.');
				key = res[0] + '[' + res[1] + ']';
				if(res[1] == 0)
				{
					key = res[0] + '\\[\\]';
				}
				console.log(key);
			}

			var elm = $(element + ' [name=' + key + ']').closest('.field');
			$(elm).addClass('error');
			var message = `<div class="ui basic red pointing prompt label transition visible">`+ value +`</div>`;

			var showerror = $(element + ' [name=' + key + ']').closest('.field');
			$(showerror).append('<div class="ui basic red pointing prompt label transition visible">' + value + '</div>');
		}

		showFormError = function(key, value)
		{
			if(key.includes("."))
			{
				res = key.split('.');
				key = res[0] + '[' + res[1] + ']';
				if(res[1] == 0)
				{
					key = res[0] + '\\[\\]';
					var exist = $('#dataForm' + ' [name="' + res[0] + '[' + res[1] + ']' + '"]');
					if(exist.length > 0)
					{
						key = res[0] + '[' + res[1] + ']';
					}
				}
				if(res[2])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']';
					if(res[2] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '\\[\\]';
					}
				}
				if(res[3])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']';
					if(res[3] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '\\[\\]';
					}
				}
				if(res[4])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']';
					if(res[4] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '\\[\\]';
					}
				}
				if(res[5])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']' + '[' + res[5] + ']';
					if(res[5] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '['+ res[4] +']' + '\\[\\]';
					}
				}
			}
			var elm = $('#dataForm' + ' [name^="' + key + '"]').closest('.field');
			var tabs = $('#dataForm' + ' [name^="' + key + '"]').parents('.tab.segment');
			if(tabs.length > 0)
			{
				selectedTabs(tabs);
			}
			var message = `<div class="ui basic red pointing prompt label transition visible">`+ value +`</div>`;
			var showerror = $('#dataForm' + ' [name^="' + key + '"]').closest('.field');
			var multipleCheckbox = $(showerror).parents('.multiple-checkbox');
			if($('#dataForm' + ' [name^="' + key + '"]').closest('.field').find('input').length > 0 && $('#dataForm' + ' [name^="' + key + '"]').closest('.field').find('input').hasClass('hidden'))
			{
				$(elm).addClass('error');
				if($('#dataForm' + ' [name^="' + key + '"]').closest('.field').find('div').length > 0)
				{
					$('#dataForm' + ' [name^="' + key + '"]').closest('.field').find('div').append('<div class="ui left pointing red basic label">' + value + '</div>');
				}else{
					$(showerror).append('<div class="ui basic red pointing prompt label transition visible">' + value + '</div>');
				}
			}else{
				if(multipleCheckbox.length > 0)
				{
					multipleCheckboxLabel = multipleCheckbox.find('label:first-child');
					$(multipleCheckboxLabel).append('<span class="red error-label" style="color:#9f3a38 !important;">' + value + '</span>');
				}else{
					$(elm).addClass('error');
					$(showerror).append('<div class="ui basic red pointing prompt label transition visible">' + value + '</div>');
				}
			}
		}

		clearFormError = function(key, value)
		{
			if(key.includes("."))
			{
				res = key.split('.');
				key = res[0] + '[' + res[1] + ']';
				if(res[1] == 0)
				{
					key = res[0] + '\\[\\]';
					var exist = $('#dataForm' + ' [name="' + res[0] + '[' + res[1] + ']' + '"]');
					if(exist.length > 0)
					{
						key = res[0] + '[' + res[1] + ']';
					}
				}
				if(res[2])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']';
					if(res[2] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '\\[\\]';
					}
				}
				if(res[3])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']';
					if(res[3] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '\\[\\]';
					}
				}
				if(res[4])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']';
					if(res[4] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '\\[\\]';
					}
				}
				if(res[5])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']' + '[' + res[5] + ']';
					if(res[5] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '['+ res[4] +']' + '\\[\\]';
					}
				}
			}
			if($('#dataForm' + ' [name^="' + key + '"]').closest('.field').find('div').length > 0){
				var elm = $('#dataForm' + ' [name^="' + key + '"]').closest('.field');
				$(elm).removeClass('error');

				var showerror = $('#dataForm' + ' [name^="' + key + '"]').closest('.field').find('.ui.left.pointing.red.basic.label').remove();
			}else{
				var elm = $('#dataForm' + ' [name^="' + key + '"]').closest('.field');
				$(elm).removeClass('error');

				var showerror = $('#dataForm' + ' [name^="' + key + '"]').closest('.field').find('.ui.basic.red.pointing.prompt.label.transition.visible').remove();
			}
		}

		showFormErrorSafety = function(key, value)
		{
			if(key.includes("."))
			{
				res = key.split('.');
				key = res[0] + '[' + res[1] + ']';
				if(res[1] == 0)
				{
					key = res[0] + '\\[\\]';
				}
				console.log(key);
			}
			var elm = $('#dataForm' + ' [name=' + key + ']').closest('.field');
			$(elm).addClass('error');
			var message = `<div class="ui basic red pointing prompt label transition visible">`+ value +`</div>`;

			var showerror = $('#dataForm' + ' [name=' + key + ']').closest('.field');
			$(showerror).append('<div class="ui basic red pointing prompt label transition visible">' + value + '</div>');
		}

		clearFormErrorSafety = function(key, value)
		{
			if(key.includes("."))
			{
				res = key.split('.');
				key = res[0] + '[' + res[1] + ']' + '[]';
				if(res[1] == 0)
				{
					key = res[0] + '\\[\\]';
				}
				console.log('clearFormErrorSafety',key);
			}
			var elm = $('#dataForm' + ' input[name=' + key + ']').closest('.field');
			$(elm).removeClass('error');

			var showerror = $('#dataForm' + ' [name=' + key + ']').closest('.field').find('.ui.basic.red.pointing.prompt.label.transition.visible').remove();
		}

		showFormErrorEquip = function(key, value)
		{
			if(key.includes("."))
			{
				res = key.split('.');
				key = res[0] + '[' + res[1] + ']' + '[]';
				if(res[1] == 0)
				{
					key = res[0] + '\\[\\]';
				}
				console.log('showFormErrorEquip',key);
			}
			var elm = $('#dataFormss' + ' input[name=' + key + ']').closest('.field');
			$(elm).addClass('error');
			var message = `<div class="ui basic red pointing prompt label transition visible">`+ value +`</div>`;

			var showerror = $('#dataFormss' + ' [name=' + key + ']').closest('.field');
			$(showerror).append('<div class="ui basic red pointing prompt label transition visible">' + value + '</div>');
		}

		clearFormErrorEquip = function(key, value)
		{
			if(key.includes("."))
			{
				res = key.split('.');
				key = res[0] + '[' + res[1] + ']';
				if(res[1] == 0)
				{
					key = res[0] + '\\[\\]';
				}
				console.log(key);
			}
			var elm = $('#dataFormss' + ' [name=' + key + ']').closest('.field');
			$(elm).removeClass('error');

			var showerror = $('#dataFormss' + ' [name=' + key + ']').closest('.field').find('.ui.basic.red.pointing.prompt.label.transition.visible').remove();
		}
	});

$(document).on('click', '.close', function(e){
	$('.ui.negative').hide();
})

$(document).on('change', '.child.target', function () {
	var elemchild = $(this).find('select').data('child');
	var departemen_id = $(this).find('select').val();
	showLoadingInput(elemchild);
	if(departemen_id != null)
	{
		$.ajax({
			url: '{{ url("option") }}/'+ elemchild +'/'+ departemen_id,
			type: 'GET',
			success: function(resp){
				stopLoadingInput(elemchild);
				$('#'+elemchild).html(resp);
			},
			error : function(resp){

			}
		});
	}
});

$(document).on('click', '.save.as.drafting', function(e){

	$('#dataForm').find('input[name="status"]').val("0");
	saveForm();
});

$(document).on('click', '.save.as.publicity', function(e){swal({
	title: 'Apakah Anda Yakin Sudah Siap Publish?',
			// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Publish',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$('#dataForm').find('input[name="status"]').val("1");
				saveForm();
			}
		})
	});
$(document).on('click', '.save.as.page', function(e){
	var formDom = "dataForm";
	if($(this).data("form") !== undefined){
		formDom = $(this).data('form');
	}
	swal({
		title: 'Apakah Anda Yakin?',
			text: "Data yang sudah dikirim, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Simpan',
			cancelButtonText: 'Batal',
			reverseButtons: true
		}).then((result) => {
			if (result) {
				saveForm(formDom);
			}
		})
	});

$(document).on('click', '.save.as.page2', function(e){
	var formDom = "dataForm";
	if($(this).data("form") !== undefined){
		formDom = $(this).data('form');
	}
	swal({
		title: 'Apakah Anda Yakin?',
			text: "Pesanan Anda akan diproses sekarang !",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Simpan',
			cancelButtonText: 'Batal',
			reverseButtons: true
		}).then((result) => {
			if (result) {
				saveFormDaftar(formDom);
			}
		})
	});

$(document).on('click', '.save.as.page3', function(e){
	var formDom = "dataForm";
	if($(this).data("form") !== undefined){
		formDom = $(this).data('form');
	}
	swal({
		title: 'Apakah Anda Yakin?',
			text: "Data yang sudah diubah, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ubah',
			cancelButtonText: 'Batal',
			reverseButtons: true
		}).then((result) => {
			if (result) {
				saveForm(formDom);
			}
		})
	});

$(document).on('click', '.save.as.page4', function(e){
	var formDom = "dataForm";
	if($(this).data("form") !== undefined){
		formDom = $(this).data('form');
	}
	swal({
		title: 'Apakah Anda Yakin?',
			text: "Data yang sudah dikirim, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Simpan',
			cancelButtonText: 'Batal',
			reverseButtons: true
		}).then((result) => {
			if (result) {
				saveFormSurat(formDom);
			}
		})
	});

$(document).on('click', '.save.as.redirect', function(e){swal({
	title: 'Apakah Anda Yakin?',
			// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Simpan',
			cancelButtonText: 'Batal',
			reverseButtons: true
		}).then((result) => {
			if (result) {
				saveFormRedirect();
			}
		})
	});

$(document).on('click', '.save.as.self-redirect', function(e){
	saveFormSelfRedirect();
		// swal({
		// 	title: 'Apakah Anda Yakin?',
		// 	// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
		// 	type: 'warning',
		// 	showCancelButton: true,
		// 	confirmButtonColor: '#3085d6',
		// 	cancelButtonColor: '#d33',
		// 	confirmButtonText: 'Simpan',
		// 	cancelButtonText: 'Batal'
		// }).then((result) => {
		// 	if (result) {
		// 		saveFormSelfRedirect();
		// 	}
		// })
	});

$(document).on('click', '.save.as.url-redirect', function(e){
	var url = $(this).data('url');
	swal({
		title: 'Apakah Anda Yakin?',
			// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Simpan',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				saveFormUrlRedirect(url);
			}
		})
	});

$(document).on('click', '.save.then.redirect', function(e){
	var url = $(this).data('url');
	saveFormUrlRedirect(url);
});

$(document).on('click', '.ui.green.button.append-lampiran', function () {
	html = `<div class="sixteen wide column" style="padding: .5rem 0">
	<div class="ui fluid file input action">
	<input type="text" readonly>
	<input type="file" class="six wide column" name="lampiran[]" autocomplete="off" multiple>
	<div class="ui button file">
	Cari...
	</div>
	<div class="ui red button remove-lampiran">
	Hapus &nbsp;&nbsp;
	</div>
	</div>
	</div>`;

	$(this).closest('.ui.inline.grid.field').append(html);
});

$(document).on('click', '.ui.red.button.remove-lampiran', function () {
	$(this).closest('.sixteen.wide.column').remove();
});

$(document).on('click', '.ui.green.button.append-foto', function () {
	html = `<div class="sixteen wide column" style="padding: .5rem 0">
	<div class="ui fluid file input action">
	<input type="text" readonly>
	<input type="file" class="six wide column" name="foto[]" autocomplete="off" multiple>
	<div class="ui button file">
	Cari...
	</div>
	<div class="ui red button remove-foto">
	Hapus &nbsp;&nbsp;
	</div>
	</div>
	</div>`;

	$(this).closest('.ui.inline.grid.field').append(html);
});

$(document).on('click', '.ui.red.button.remove-foto', function () {
	$(this).closest('.sixteen.wide.column').remove();
});

$(document).on('keypress', 'input[name^="filter"]', function (e) {
	if (e.which == 13) {
		$('.filter.button').click();
		return false;
	}
});

$(document).on('click', '.ui.green.button.append-hadir', function () {
	html = `<div class="sixteen wide column" style="padding: .5rem 0">
	<div class="ui fluid file input action">
	<input type="text" readonly>
	<input type="file" class="six wide column" name="hadir[]" autocomplete="off" multiple>
	<div class="ui button file">
	Cari...
	</div>
	<div class="ui red button remove-hadir">
	Hapus &nbsp;&nbsp;
	</div>
	</div>
	</div>`;

	$(this).closest('.ui.inline.grid.field').append(html);
});

$(document).on('click', '.ui.red.button.remove-hadir', function () {
	$(this).closest('.sixteen.wide.column').remove();
});

$(document).on('click', '.approved', function(e){
	var id = $(this).data('id');
	var url = $(this).data('url');
	var purl = url+'/'+id;
	console.log(purl);
	loadModal(purl);
});

$(document).on('click', '.save-app.button', function(e){
	var id = $('input[name="id"]').val();
	var metode = $('input[name="method"]').val();
	var keterangan = $('textarea[name="keterangan"]').val();
	swal({
		title: 'Apakah Anda Yakin ?',
				// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Simpan',
				cancelButtonText: 'Batal'
			}).then((result) => {
				if (result) {
					$.ajax({
						url: '{{ url("induksi/upload/saveApprove")}}',
						type: 'POST',
						data: {_token: "{{ csrf_token() }}", _method: "post",'id':id,'metode':metode,'keterangan':keterangan},
						success: function(data){
							$("#formModal").modal('hide');
							swal(
								'Berhasil!',
								'Data berhasil.',
								'success'
								).then(function(e){
									$('#formModal').find('.loading.dimmer').removeClass('active');
									if(document.getElementById('listTable') !=null){
										dt.draw();
									}else{
										window.location.reload();
									}
									return true;
								});
							},
							error : function(data){
								swal(
									'Gagal!',
									'Data gagal diinput.',
									'error'
									).then(function(e){
									// location.reload();
								});
								}
							});
				}
			})
		});

$(document).on('click', '.reupload', function(e){
			// var formdate = $("#FormAksi").serialize();
			swal({
				title: 'Apakah Anda Yakin ?',
				// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Simpan',
				cancelButtonText: 'Batal'
			}).then((result) => {
				if (result) {
					$("#FormAksi").ajaxSubmit({
						success: function(resp){
							$("#formModal").modal('hide');
							swal(
								'Tersimpan!',
								'Data berhasil disimpan.',
								'success'
								).then((result) => {
									$('#formModal').find('.loading.dimmer').removeClass('active');
									if(document.getElementById('listTable') !=null){
										dt.draw();
									}else{
										window.location.reload();
									}
									return true;
								})
							},
							error: function(resp){
								swal(
									'Gagal!',
									'Data gagal diupload.',
									'error'
									).then(function(e){
									// location.reload();
								});
								}
							});
				}

			})
		});

$(document).on('click', '.save.entry.button', function(e){swal({
	title: 'Apakah Anda Yakin?',
				// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Simpan',
				cancelButtonText: 'Batal'
			}).then((result) => {
				if (result) {
					if($("#entryDataForm").form('is valid')){
						$('#cover').show();
						$("#entryDataForm").ajaxSubmit({
							success: function(resp){
								$("#formModal").modal('hide');
								swal(
									'Tersimpan!',
									'Data berhasil disimpan.',
									'success'
									).then((result) => {
										if(postSave(resp)){
											return true;
										}else{
											$('#cover').hide();
											window.location = "{{ url($pageUrl) }}";
											return true;
										}
									})
								},
								error: function(resp){
									$('#cover').hide();
									$.each(resp.responseJSON, function(index, val) {
										clearFormErrorEntry(index,val);
										showFormErrorEntry(index,val);
									});
									time = 5;
									interval = setInterval(function(){
										time--;
										if(time == 0){
											clearInterval(interval);
											$('.pointing.prompt.label.transition.visible').remove();
											$('.error').each(function (index, val) {
												$(val).removeClass('error');
											});
										}
									},1000)
								}
							});
					}else{
						$("#entryDataForm").ajaxSubmit({
							success: function(resp){
								$("#formModal").modal('hide');
								swal(
									'Tersimpan!',
									'Data berhasil disimpan.',
									'success'
									).then((result) => {
										if(postSave(resp)){
											return true;
										}else{
											$('#cover').hide();
											window.location = "{{ url($pageUrl) }}";
											return true;
										}
									})
								},
								error: function(resp){
									$.each(resp.responseJSON, function(index, val) {
										clearFormErrorEntry(index,val);
										showFormErrorEntry(index,val);
									});
									time = 5;
									interval = setInterval(function(){
										time--;
										if(time == 0){
											clearInterval(interval);
											$('.pointing.prompt.label.transition.visible').remove();
											$('.message').remove();
											$('.error').each(function (index, val) {
												$(val).removeClass('error');
											});
										}
									},1000)
								}
							});
					}
				}
			})
		});

clearFormErrorEntry = function(key, value)
{
	if(key.includes("."))
	{
		res = key.split('.');
		key = res[0] + '[' + res[1] + ']';
		if(res[1] == 0)
		{
			key = res[0] + '\\[\\]';
			var exist = $('#entryDataForm' + ' [name="' + res[0] + '[' + res[1] + ']' + '"]');
			if(exist.length > 0)
			{
				key = res[0] + '[' + res[1] + ']';
			}
		}
		if(res[2])
		{
			key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']';
			if(res[2] == 0)
			{
				key = res[0] + '['+ res[1] +']' + '\\[\\]';
			}
		}
		if(res[3])
		{
			key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']';
			if(res[3] == 0)
			{
				key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '\\[\\]';
			}
		}
		if(res[4])
		{
			key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']';
			if(res[4] == 0)
			{
				key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '\\[\\]';
			}
		}
		if(res[5])
		{
			key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']' + '[' + res[5] + ']';
			if(res[5] == 0)
			{
				key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '['+ res[4] +']' + '\\[\\]';
			}
		}
	}
	var elm = $('#entryDataForm' + ' [name^="' + key + '"]').closest('.field');
	$(elm).removeClass('error');

	var showerror = $('#entryDataForm' + ' [name^="' + key + '"]').closest('.field').find('.ui.basic.red.pointing.prompt.label.transition.visible').remove();
}

showFormErrorEntry = function(key, value)
{
	if(key.includes("."))
	{
		res = key.split('.');
		key = res[0] + '[' + res[1] + ']';
		if(res[1] == 0)
		{
			key = res[0] + '\\[\\]';
			var exist = $('#entryDataForm' + ' [name="' + res[0] + '[' + res[1] + ']' + '"]');
			if(exist.length > 0)
			{
				key = res[0] + '[' + res[1] + ']';
			}
		}
		if(res[2])
		{
			key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']';
			if(res[2] == 0)
			{
				key = res[0] + '['+ res[1] +']' + '\\[\\]';
			}
		}
		if(res[3])
		{
			key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']';
			if(res[3] == 0)
			{
				key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '\\[\\]';
			}
		}
		if(res[4])
		{
			key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']';
			if(res[4] == 0)
			{
				key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '\\[\\]';
			}
		}
		if(res[5])
		{
			key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']' + '[' + res[5] + ']';
			if(res[5] == 0)
			{
				key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '['+ res[4] +']' + '\\[\\]';
			}
		}
	}
	var elm = $('#entryDataForm' + ' [name^="' + key + '"]').closest('.field');
	var tabs = $('#entryDataForm' + ' [name^="' + key + '"]').parents('.tab.segment');
	if(tabs.length > 0)
	{
		selectedTabs(tabs);
	}
	var message = `<div class="ui basic red pointing prompt label transition visible">`+ value +`</div>`;
	var showerror = $('#entryDataForm' + ' [name^="' + key + '"]').closest('.field');
	var multipleCheckbox = $(showerror).parents('.multiple-checkbox');
	if(multipleCheckbox.length > 0)
	{
		multipleCheckboxLabel = multipleCheckbox.find('label:first-child');
		$(multipleCheckboxLabel).append('<span class="red error-label" style="color:#9f3a38 !important;">' + value + '</span>');
	}else{
		$(elm).addClass('error');
		$(showerror).append('<div class="ui basic red pointing prompt label transition visible">' + value + '</div>');
	}

}

	// --------------------- BATAS AMPAS NEW CODE ------------------------ //
	
	$(document).on('click','.translate',function(){
		var trans = $(this).data('value');
		var cekBahasa = 'Are you sure';
		var cekTextBahasa = ' you want change to english language !';
		var Ya = 'Yes';
		var Noo = 'No';
		if(trans == 'id'){
			cekBahasa = 'Apakah anda yakin ';
			cekTextBahasa = 'akan mengubah bahasa ke indonesia !';
			Ya = 'Ya';
			Noo = 'Tidak';
		}
		$.ajax({
			url: '{{ url("langs") }}',
			type: 'POST',
			data: {_token: "{{ csrf_token() }}",translator:trans},
			success: function(resp){
				location.reload();
			},
			error : function(resp){
				location.reload();
			}
		});
	});
</script>
