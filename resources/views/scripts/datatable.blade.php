@php
	$dataTrans = 'id';
	if(Auth::check())
	{
		if(auth()->user()->id == true){
			if(\App\Models\Translators::where('created_by',auth()->user()->id)->first()){
				$dataTrans = \App\Models\Translators::where('created_by',auth()->user()->id)->first()->translator;
			}else {
				$dataTrans = 'id';
			}
		}else {
			$dataTrans = 'id';
		}
	}
@endphp
@include('scripts.readmoreitem')
<script type="text/javascript">
	$(document).ready(function() {
		$('button[data-content]').popup({
			hoverable: true,
			position : 'top center',
			delay: {
				show: 300,
				hide: 800
			}
		});

		dt = $('#listTable').DataTable({
	        dom: 'rt<"bottom"ip><"clear">',
			responsive: true,
			autoWidth: false,
			processing: true,
			serverSide: true,
			lengthChange: false,
			pageLength: 10,
			filter: false,
			sorting: [],
			language: {
				url: "{{ asset('plugins/datatables/Indonesian.json') }}"
			},
			ajax:  {
				url: "{{ url($pageUrl) }}/grid",
				type: 'POST',
				data: function (d) {
					d._token = "{{ csrf_token() }}";
					@yield('js-filters')
				}
			},
			columns: {!! json_encode($tableStruct) !!},
			drawCallback: function() {
				readMoreItem('list-more1');
				var api = this.api();
				api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					start = cell.innerHTML;
					cell.innerHTML = (parseInt(start) + (i+1));
				});

				$('[data-content]').popup({
					hoverable: true,
					position : 'top center',
					delay: {
						show: 300,
						hide: 800
					}
				});

				//Calender
				// $('.ui.calendar').calendar({
				// 	type: 'date'
				// });

				//Popup
				$('.checked.checkbox')
				  .popup({
				    popup : $('.custom.popup'),
				    on    : 'click'
				  })
				;
			}
		});

		$('#btn-filter').on('click', function(e) {
            dt.draw();
            e.preventDefault();
        });

        $('.filter.button').on('click', function(e) {
            dt.draw();
            e.preventDefault();
        });

        $('.reset.button').on('click', function(e) {
            $('.dropdown .delete').trigger('click');
            setTimeout(function(){
                dt.draw();
            }, 100);
            $('.dropdown').dropdown('clear');
        });

        $('.text-filter').on('keydown', function(e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
                dt.draw();
                e.preventDefault();
            }
        })
	});
</script>