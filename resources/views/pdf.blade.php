<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">File Preview</div>
<div class="modal content">
	@if($link !== 0)
	<div style="min-height:1000px;">
		<iframe title="Advertisement" id="test.pdf" src="{{ $link }}#toolbar=0" frameborder="0" marginwidth="0" marginheight="0" scrolling="yes" width="100%" style="position: relative; height: 1000px;"> </iframe>
	</div>
	@else
	<p style="text-align: center;font-weight: bold;">
		File Tidak Ditemukan
	</p>
	@endif
</div>