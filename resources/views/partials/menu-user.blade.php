<div class="ui content">
    <div class="accordion">
	 	<a href="{{ url('/frontend/pendaftaran-pengujian') }}" class="{{ (Request::route()->getName() === 'pendaftaran-pengujian.index' ? 'active' : '') }} item">
	 		<i class="envelope close outline icon"></i>{{trans('translator.Pesanan Berjalan')}}
	 	</a>
    </div>
	<div class="accordion">
 		<a href="{{ url('/frontend/daftar-order') }}" class="{{ (Request::route()->getName() === 'daftar-order.index' ? 'active' : '') }} item">
 		<i class="envelope open icon"></i>{{trans('translator.Riwayat Permintaan')}}
 		</a>
    </div>
    <div class="accordion">
	 	<a href="{{ url('/frontend/account-setting') }}" class="{{ (Request::route()->getName() === 'account-setting.index' ? 'active' : '') }} item">
	 		<i class="list icon"></i>{{trans('translator.Pengaturan Akun')}}
	 	</a>
    </div>
    {{-- <div class="accordion">
	 	<a href="{{ url('/frontend/pembayaran') }}" class=" item">
	 		<i class="money icon"></i>{{trans('translator.Register')}}
	 	</a>
    </div> --}}
</div>
