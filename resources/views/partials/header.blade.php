<div class="ui fixed blue menu">
    <a href="{{ url('/backend') }}" class="header item" style="letter-spacing: 0px;">
        <img class="logo" src="{{ asset('img/logo.png')}}" style="width: 2em;padding-top: 2px;padding-left:2px;">&nbsp;&nbsp;
        {{ config('app.name') }}
    </a>
    <div class="menu">
        <a href="#" class="item" onclick="toggleSidebar()">
            <i class="sidebar icon"></i>
        </a>
    </div>
    
    <div class="right menu">
        {{-- @if(auth()->user()->hasRole('user')) --}}
       {{--  <a href="#" class="icon item">
            <i class="gb uk flag" style="margin-right: 0"></i>
        </a>
        <a href="#" class="icon item">
            <i class="id flag" style="margin-right: 0"></i>
        </a> --}}
        {{-- @endif --}}
        {{-- <a href="{{ url('/') }}" class="icon item">
            <i class="desktop icon"></i>
        </a> --}}
        {{-- <div class="ui right dropdown item">
            <a class="ui red label">Training</a>
        </div> --}}
        <div class="ui right dropdown item" id="notifLoader">
            <i class="ui bell icon" style="margin-right: 0"></i>
            {{-- @include('partials.notif') --}}
        </div>
        <div class="ui pointing dropdown item" tabindex="0">
            <img class="ui avatar image" src="{{ url(auth()->user()->photo ? 'storage/'.auth()->user()->photo : 'img/user-default.jpg') }}"> &nbsp;&nbsp;
            {{ (Auth::check()) ? isset(auth()->user()->username) ? auth()->user()->username : '' : ''  }}
            <i class="dropdown icon"></i>
            <div class="menu transition hidden" tabindex="-1">
                <a class="item" href="{{ url('/backend/profile') }}"><i class="user icon"></i> Profil</a>
                <a class="item" href="{{ url('/logout') }}"><i class="sign out icon"></i> Logout</a>
            </div>
        </div>
    </div>
</div>