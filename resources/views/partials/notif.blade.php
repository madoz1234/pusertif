{{-- @if(auth()->user()->unreadNotifications->count() > 0)
<div class="floating ui red small label" id="count-notif">{{ auth()->user()->unreadNotifications->count() }}</div>
@endif --}}
<i class="ui bell icon" style="margin-right: 0"></i>
<div id="notifContainer" class="nano menu">
    <div id="area-notif" class="nano-content">
        <div class="ui padded divided list">
            <a href="#" class="mark all notif as read item"><h5 class="ui center aligned header"><i class="check icon"></i>Mark all as Read</h5></a>
              <a href="#" class="item"> 
                  <div class="right floated middle aligned content">
                    <i class="circle green icon"></i>
                  </div>
                  <div class="middle aligned content" style="white-space: normal !important;">
                      <div class="header">Lorem</div>
                      Lorem Ipsum<br>
                      <small>date</small>
                  </div>
              </a>
        </div>
    </div>
</div>