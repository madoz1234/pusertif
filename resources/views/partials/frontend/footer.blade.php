@php
$data = \App\Models\Master\Contact::where('status', 1)->first();
@endphp
<div class="ui vertical footer segment">
	<div class="ui container">
		<div class="ui stackable divided equal height stackable grid">
			<div class="three wide column">
				<h4 class="ui header">{{trans('translator.Tentang Kami')}}</h4>
				<div class="ui link list">
					<a href="#" class="item"><i class="home icon"></i>Jln. Laboratorium Duren &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tiga, Jakarta Selatan 12760</a>
					<a href="#" class="item"><i class="phone icon"></i>{{ $data->no_tlp or '-' }}</a>
					<a href="#" class="item"><i class="whatsapp icon"></i>{{ $data->wa or '-' }}</a>
					<a href="#" class="item"><i class="fax icon"></i>7994149, 7982034</a>
				</div>
			</div>
			<div class="three wide column">
				<h4 class="ui header">{{trans('translator.Peta Situs')}}</h4>
				<div class="ui link list">
					<a href="#" class="item">{{trans('translator.Layanan Pelanggan')}}</a>
					<a href="{{ url('hubungi') }}" class="item">{{trans('translator.Hubungi Kami')}}</a>
					<a href="#" class="item">Weblogin</a>
				</div>
			</div>
			<div class="three wide column">
				<h4 class="ui header">{{trans('translator.Pengunjung')}}</h4>
				<div class="ui link list">
					
					<a href="#" class="item"><i class="user outline icon"></i>1 Online</a>
					<a href="#" class="item"><i class="user outline icon"></i>1 {{trans('translator.Hari Ini')}}</a>
					<a href="#" class="item"><i class="user outline icon"></i>1 {{trans('translator.Kemarin')}}</a>
					<a href="#" class="item"><i class="user outline icon"></i>1 {{trans('translator.Minggu Lalu')}}</a>
					<a href="#" class="item"><i class="user outline icon"></i>1 {{trans('translator.Bulan Lalu')}}</a>
					<a href="#" class="item"><i class="user outline icon"></i>{{ app('counter')->count() }} {{trans('translator.Total Hit')}}</a>
					<a href="#" class="item"><i class="user outline icon"></i>{{ app('counter')->unique()->count() }} {{trans('translator.Total Kunjungan')}}</a>
				</div>
			</div>
			<div class="seven wide column">
				<img src="{{ asset('img/lmklogo.jpg') }}" alt="sertifikasi logo" class="ui inline image">
			</div>
		</div>
	</div>
</div>

<div class="ui inverted real-footer center aligned segment">
	<div class="ui container">
		2019 &copy; PLN (Persero) PUSERTIF
	</div>
</div>