  @php
  $dataTrans = 'id';
  if(Auth::check())
  	{
  		if(auth()->user()->id == true){
  			if(\App\Models\Translators::where('created_by',auth()->user()->id)->first()){
  				$dataTrans = \App\Models\Translators::where('created_by',auth()->user()->id)->first()->translator;
  			}else {
  				$dataTrans = 'id';
  			}
  		}else {
  			$dataTrans = 'id';
  		}
  	}else{
  		$dataTrans = \App\Models\Translators::where('created_by',999)->first()->translator;
  	}
  	@endphp
  	<nav id="nav" class="ui masthead-menu container">
  		<div class="ui large secondary pointing overlay menu">
  			<a class="toc item">
  				<i class="sidebar icon"></i>
  			</a>
  			<a class="item" href="{{ url('/') }}">{{trans('translator.Tentang Kami')}}</a>
  			<a class="item" onclick="$('#modal-sert-manajemen').modal('show')">{{trans('translator.Pengujian TT')}}</a>
  			<a class="item" onclick="$('#modal-sert-produk').modal('show')">{{trans('translator.Pengujian TR')}}</a>
  			<a style="max-width: 147px !important;" class="item" onclick="$('#modal-sert-operasi').modal('show')">{{trans('translator.Pengujian SISTGI')}}</a>
  			<a style="max-width: 147px !important;" class="item" onclick="$('#modal-uji').modal('show')">{{trans('translator.Pengujian SISKIT')}}</a>
  			<a class="item" onclick="$('#modal-kal').modal('show')">{{trans('translator.Kalibrasi')}}</a>
  			<a style="max-width: 147px !important;" class="item" onclick="$('#modal-ases').modal('show')">Assesment</a>
  			<a class="item" href="{{ url('hubungi') }}" style="margin-left: -14px;margin-right: 25px;">{{trans('translator.Hubungi Kami')}}</a>
  			<a class="special item" href="{{ url('tracking') }}">{{trans('translator.Tracking')}}</a>
  			<div class="right item">
  				<a class="icon item {{ ($dataTrans == 'id') ? '' : 'translate' }}" style="{{ ($dataTrans == 'id') ? 'border-radius: 5px;background-color: #a2aebd8f;color: #fff !important;height: 12px' : '' }}" data-value="id"><img src="{{ asset('img/icon_id.png') }}" alt="ID" width="28" class="ui image" data-value="id"> &nbsp;ID</a>
  				&nbsp;
  				<a class="icon item {{ ($dataTrans == 'en') ? '' : 'translate' }}" style="{{ ($dataTrans == 'en') ? 'border-radius: 5px;background-color: #a2aebd8f;color: #fff !important;height: 12px' : '' }}" data-value="en"><img src="{{ asset('img/icon_uk.png') }}" alt="EN" width="28" class="ui image"> &nbsp;EN</a>



  				@if(auth()->user())
  				<a href="{{ url('frontend') }}" class="ui default button">{{trans('translator.User CP')}}</a>
  				<a href="{{ url('logout') }}" class="ui red button">{{trans('translator.Sign Out')}}</a>
  				@else
  			 <a href="{{ url('register') }}" class="ui default button">{{trans('translator.Register')}}</a>
          <a href="{{ url('login') }}" class="ui teal button">{{trans('translator.Sign In')}}</a>
  				@endif
  			</div>
  		</div>
  	</nav>
@section('modals')
<div class="ui basic feature modal" id="modal-uji">
	<div class="content">
		<div class="ui centered cards">
			<div class="card" style="width: 100%;">
				<div class="card" style="height: 150px;background-color: #a2aebd8f;">
					<div class="center aligned header">
					<h3 class="ui green color header" style="padding-top: 75px;font-size: 17px;">PENGUJIAN SISTEM PEMBANGKITAN (SISKIT)</h3>
					</div>
				</div>
				<div class="content">
					<div class="ui list">
						<div class="item">
							<div class="content">
								<div class="header">Condition Based Maintenance (CBM)</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Vibrasi</li>
										<li>Motor current signature analysis (MCSA)</li>
										<li>Infrared thermography</li>
										<li>Tribologi</li>
										<li>Power quality</li>
										<li>Laser alignment</li>
									</ul>
								</div>
								<div class="header">Material</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Thickness tube</li>
										<li>Insitu & lab. Metallography</li>
										<li>Penetrant test</li>
										<li>Ultrasonic flaw detector</li>
										<li>Magnetic particle inspection</li>
										<li>Insitu & lab hardness test</li>
										<li>Creep rupture testing</li>
										<li>Scaning electron microscope</li>
										<li>Spectrometri (EDX, OES, XRF, XRD)</li>
										<li>Borescope testing</li>
										<li>Inside & outside micrometer test</li>
										<li>Crack depth test</li>
										<li>Eddy current test</li>
										<li>Inner oxide scale measurement</li>
									</ul>
								</div>
								<div class="header">Efisiensi Pembangkit</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Pengujian Genset</li>
										<li>Pengujian performance pembangkit (heat rate, sfc, debit/kwh dll)</li>
									</ul>
								</div>
								<div class="header">Energi Primer</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Preparasi dan penentuan air drying loss (ADL)</li>
										<li>Penentuan total moisture</li>
										<li>Nilai kalor</li>
										<li>Total sulfur</li>
										<li>Analisis ultimate (chn)</li>
										<li>Analisis proximate (moisture, ash, VM dan fix carbon)</li>
										<li>Analisis kadar fame (biodiesel)</li>
										<li>Analisis density</li>
										<li>Temperatur destilasi</li>
										<li>Analisis kadar abu</li>
										<li>Analisis titik nyala</li>
										<li>Analisis karbon residu</li>
										<li>Analisis kandungan air dan sedimen</li>
										<li>Analisis Komposisi Gas Alam</li>
										<li>Analisa Nilai Kalor Gas Alam</li>
										<li>Uji Karakteristik Modul Surya</li>
									</ul>
								</div>
								<div class="header">Sipil</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Uji kuat tekan beton (ndt)</li>
										<li>Uji kehomogenan / keseragaman mutu beton</li>
										<li>Pemetaan struktur beton</li>
										<li>Uji korosi beton bertulang</li>
										<li>Uji kedalaman pondasi pile</li>
										<li>Uji kuat tekan beton laboratorium</li>
										<li>Survey Resistivity bawah permukaan tanah</li>
										<li>Survey Ground Penetrating Radar</li>
										<li>Uji berat jenis</li>
										<li>Uji geser</li>
										<li>Uji triaxial tanah</li>
										<li>Uji analisa ukuran butiran tanah</li>
										<li>Pengukuran Debit Air</li>
										<li>Pemetaan (Pengukuran Poligon Dan Situasi)</li>
										<li>Levelling Stub Tower Transmisi</li>
										<li>Pengukuran Eksentrisitas Tower</li>
										<li>Pengukuran Koordinat Berbasis Satelite</li>
										<li>Levelling Base Frame Pondasi Mesin</li>
										<li>Pemetaan geologi permukaan</li>
										<li>Interpretasi geologi bawah permukaan</li>
										<li>Regional geologi</li>
										<li>Pemetaan stratigrafi</li>
										<li>Analisa struktur tanah</li>
									</ul>
								</div>
								<div class="header">Kimia</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Tegangan tembus</li>
										<li>Kadar air</li>
										<li>DDF</li>
										<li>Korosi</li>
										<li>Oksidasi</li>
										<li>Tegangan antar muka</li>
										<li>Viskositas</li>
										<li>Densitas</li>
										<li>Sedimen</li>
										<li>Foaming test</li>
										<li>Warna</li>
										<li>Tan</li>
										<li>Furan</li>
										<li>PCB</li>
										<li>Anti oksidan</li>
										<li>DBDS</li>
										<li>Derajat polimerisasi</li>
										<li>Titik nyala</li>
										<li>Titik tuang</li>
										<li>DGA.</li>
										<li>Asesmen Lingkup Air Dan Uap</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<h4 class="ui green ribbon label">Dukungan Akreditasi</h4>
					<div class="ui list">
						<div class="item">
							<div class="content">
								<div class="header">Sertifikasi Akreditasi KAN</div>
								<div class="description">No. LP-005-IDN (SNI ISO/IEC 17025:2017)</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ui basic feature modal" id="modal-kal">
	<div class="content">
		<div class="ui centered cards">
			<div class="card" style="width: 100%;">
				<div class="card" style="height: 150px;background-color: #a2aebd8f;">
					<div class="center aligned header">
					<h3 class="ui orange color header" style="padding-top: 75px;font-size: 17px;">KALIBRASI</h3>
					</div>
				</div>
				<div class="content">
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Arus</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Clamp Meter</li>
										<li>Multimeter</li>
										<li>Power Meter</li>
										<li>AC/DC Ampere Meter</li>
										<li>AC/DC V-A Meter</li>
										<li>Relay Tester</li>
										<li>Power Analyzer</li>
										<li>Power Quality</li>
										<li>Tera Table</li>
										<li>CT TTR</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Tegangan</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Clamp Meter</li>
										<li>Multimeter</li>
										<li>Power Meter</li>
										<li>AC/DC Ampere Meter</li>
										<li>AC/DC V-A Meter</li>
										<li>Relay Tester</li>
										<li>AC/DC High Voltage Tester</li>
										<li>Power Analyzer</li>
										<li>Power Quality</li>
										<li>Tera Table</li>
										<li>PT TTR</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Watt</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Single Phase Watt Meter</li>
										<li>Three Phase Watt Meter</li>
										<li>Power Meter</li>
										<li>Power Quality</li>
										<li>Power Analyzer</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Energy Meter</div>
								<div class="description">KWh Meter, KVArh Meter</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Frequency</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Frequency Meter</li>
										<li>Power Meter</li>
										<li>Relay Tester</li>
										<li>Clamp Meter</li>
										<li>Multimeter</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Ohm</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Portable Wheatstone Bridge</li>
										<li>Resistor Standard</li>
										<li>Portable Double Bridge</li>
										<li>Precision Double Bridge</li>
										<li>Earth Tester</li>
										<li>Decade Resistor</li>
										<li>Insulation Tester</li>
										<li>Micro Ohm Meter</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Degree</div>
								<div class="description">Phase Meter</div>
							</div>
						</div>
					</div>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Temperature</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Glass Thermometer</li>
										<li>Temp. Calibrator</li>
										<li>Temp. Detector</li>
										<li>Temp. Recorded</li>
										<li>Temp.Control</li>
										<li>Thermacouple</li>
										<li>RTD</li>
										<li>Handy Calibrator</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Pressure</div>
								<div class="description">Vacuum Gauge, Pressure Gauge</div>
							</div>
						</div>
					</div>
					<h4 class="ui orange ribbon label">Dukungan Akreditasi</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Sertifikasi Akreditasi KAN</div>
								<div class="description">No. LK-007-IDN (SNI ISO/IEC 17025.2017)</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@if($dataTrans == 'id')
<div class="ui basic feature modal" id="modal-sert-operasi">
	<div class="content">
		<div class="ui centered cards">
			<div class="card" style="width: 100%;">
				<div class="card" style="height: 150px;background-color: #a2aebd8f;">
					<div class="center aligned header">
					<h3 class="ui red color header" style="padding-top: 75px;font-size: 17px;">PENGUJIAN SISTEM TRANSMISI dan GARDU INDUK (SISTGI)</h3>
					</div>
				</div>
				<div class="content">
					<div class="ui list">
						<div class="item">
							<div class="content">
								<div class="header">Real Time Digital Simulator (RTDS)</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Relai Jarak</li>
										<li>Relai Differensial Saluran</li>
										<li>Relai Pengaman Busbar</li>
										<li>Relai Differensial Transformator Tenaga</li>
										<li>Relai Circulating Current Protection</li>
									</ul>
								</div>
								<div class="header">Fungsional</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Relai Arus Lebih</li>
										<li>Relai Arus Lebih Negatif</li>
										<li>Relai Thermal dan TDL</li>
										<li>Relai Negative Sequence</li>
										<li>Relai Penutup Balik Otomatis</li>
									</ul>
								</div>
								<div class="header">Fungsional Individual (Site)</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Relai Arah</li>
										<li>Relai Frekuensi Lebih/Relai Frekuensi Kurang</li>
										<li>Relai Tegangan Lebih/Relai Tegangan Kurang</li>
										<li>Relai Arus Lebih</li>
										<li>Relai Arus Lebih Netral</li>
										<li>Relai Jarak</li>
										<li>Relai Differensial Saluran</li>
										<li>Relai Pengaman Busbar</li>
									</ul>
								</div>
								<div class="header">Individual (Site)</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Relai Differensial Transformator Tenaga</li>
										<li>Relai Circulating Current Protection</li>
										<li>Relai Frekuensi Lebih/Relai Frekuensi Kurang</li>
										<li>Relai Tegangan Lebih/Relai Tegangan Kurang</li>
										<li>Relai Arah</li>
										<li>Relai Negative Sequence</li>
										<li>Relai Tunda Waktu</li>
										<li>Relai Overfluks</li>
										<li>Relai Reverse Power</li>
										<li>Relai Cek Sinkron</li>
										<li>Relai Gangguan Tanah Terbatas</li>
										<li>Relai Penutup Balik Otomatis</li>
										<li>Loss of excitation relay</li>
										<li>Lokal HMI</li>
										<li>IED BCU</li>
										<li>IED Proteksi</li>
										<li>IED kWh meter</li>
										<li>Relai Overfluks</li>
										<li>Relai Reverse Power</li>
										<li>Relai Cek Sinkron</li>
										<li>Relai Gangguan Tanah Terbatas</li>
										<li>Relai Penutup Balik Otomatis</li>
										<li>Loss of excitation relay</li>
									</ul>
								</div>
								<div class="header">Interopability IEC 61850 / Uji Fungsi Protokol IEC 60870-101 dan 104</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Lokal HMI</li>
										<li>IED BCU</li>
										<li>IED Proteksi</li>
										<li>IED kWh meter</li>
										<li>IED AVR</li>
										<li>IED Autoreclose</li>
										<li>Gateway IEC 61850 dan IEC 60870 -5- 101 slave</li>
										<li>Gateway IEC 61850 dan IEC 60870 -5- 104 slave</li>
										<li>RTU Concentrator  IEC 60870 -5- 101 dan   IEC 60870 -5- 104 slave</li>
										<li>RTU Concentrator  IEC 60870 -5- 104 dan   IEC 60870 -5- 101 slave</li>
										<li>RTU IEC 60870 -5- 101</li>
										<li>RTU IEC 60870 -5- 104</li>
										<li>RTU IEC 60870 -5- 101  dan IEC 60870 -5- 104</li>
										<li>RTU, RTU LBS, Recloser</li>
									</ul>
								</div>
								<div class="header">Koordinasi Setting Proteksi</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Sistem Pembangkit, Transmisi dan Distribusi</li>
									</ul>
								</div>
								<div class="header">Uji Dinamik</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Sistem Ketenagalistrikan</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<h4 class="ui red ribbon label">Dukungan Akreditasi</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Sertifikasi Akreditasi KAN</div>
								<div class="description">No. LI-026-IDN (SNI ISO.IEC 17020:2012)</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Sertifikasi Akreditasi DJK</div>
								<div class="description">No. 5Stf/20/DJL.4/2016</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">SK Penunjukan DJK</div>
								<div class="description">No. 219/20/DJL.4/2016</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@else
<div class="ui basic feature modal" id="modal-sert-operasi">
	<div class="content">
		<div class="ui centered cards">
			<div class="card" style="width: 100%;">
				<div class="card" style="height: 150px;background-color: #a2aebd8f;">
					<div class="center aligned header">
					<h3 class="ui red color header" style="padding-top: 75px;font-size: 17px;">SCOPE OF OPERATIONAL WORTHINESS CERTIFICATION AND COMMISSIONING</h3>
					</div>
				</div>
				<div class="content">
					<div class="ui list">
						<div class="item">
							<div class="content">
								<div class="header">Real Time Digital Simulator (RTDS)</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Relai Jarak</li>
										<li>Relai Differensial Saluran</li>
										<li>Relai Pengaman Busbar</li>
										<li>Relai Differensial Transformator Tenaga</li>
										<li>Relai Circulating Current Protection</li>
									</ul>
								</div>
								<div class="header">Fungsional</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Relai Arus Lebih</li>
										<li>Relai Arus Lebih Negatif</li>
										<li>Relai Thermal dan TDL</li>
										<li>Relai Negative Sequence</li>
										<li>Relai Penutup Balik Otomatis</li>
									</ul>
								</div>
								<div class="header">Fungsional Individual (Site)</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Relai Arah</li>
										<li>Relai Frekuensi Lebih/Relai Frekuensi Kurang</li>
										<li>Relai Tegangan Lebih/Relai Tegangan Kurang</li>
										<li>Relai Arus Lebih</li>
										<li>Relai Arus Lebih Netral</li>
										<li>Relai Jarak</li>
										<li>Relai Differensial Saluran</li>
										<li>Relai Pengaman Busbar</li>
									</ul>
								</div>
								<div class="header">Individual (Site)</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Relai Differensial Transformator Tenaga</li>
										<li>Relai Circulating Current Protection</li>
										<li>Relai Frekuensi Lebih/Relai Frekuensi Kurang</li>
										<li>Relai Tegangan Lebih/Relai Tegangan Kurang</li>
										<li>Relai Arah</li>
										<li>Relai Negative Sequence</li>
										<li>Relai Tunda Waktu</li>
										<li>Relai Overfluks</li>
										<li>Relai Reverse Power</li>
										<li>Relai Cek Sinkron</li>
										<li>Relai Gangguan Tanah Terbatas</li>
										<li>Relai Penutup Balik Otomatis</li>
										<li>Loss of excitation relay</li>
										<li>Lokal HMI</li>
										<li>IED BCU</li>
										<li>IED Proteksi</li>
										<li>IED kWh meter</li>
										<li>Relai Overfluks</li>
										<li>Relai Reverse Power</li>
										<li>Relai Cek Sinkron</li>
										<li>Relai Gangguan Tanah Terbatas</li>
										<li>Relai Penutup Balik Otomatis</li>
										<li>Loss of excitation relay</li>
									</ul>
								</div>
								<div class="header">Interopability IEC 61850 / Uji Fungsi Protokol IEC 60870-101 dan 104</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Lokal HMI</li>
										<li>IED BCU</li>
										<li>IED Proteksi</li>
										<li>IED kWh meter</li>
										<li>IED AVR</li>
										<li>IED Autoreclose</li>
										<li>Gateway IEC 61850 dan IEC 60870 -5- 101 slave</li>
										<li>Gateway IEC 61850 dan IEC 60870 -5- 104 slave</li>
										<li>RTU Concentrator  IEC 60870 -5- 101 dan   IEC 60870 -5- 104 slave</li>
										<li>RTU Concentrator  IEC 60870 -5- 104 dan   IEC 60870 -5- 101 slave</li>
										<li>RTU IEC 60870 -5- 101</li>
										<li>RTU IEC 60870 -5- 104</li>
										<li>RTU IEC 60870 -5- 101  dan IEC 60870 -5- 104</li>
										<li>RTU, RTU LBS, Recloser</li>
									</ul>
								</div>
								<div class="header">Koordinasi Setting Proteksi</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Sistem Pembangkit, Transmisi dan Distribusi</li>
									</ul>
								</div>
								<div class="header">Uji Dinamik</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Sistem Ketenagalistrikan</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<h4 class="ui red ribbon label">Dukungan Akreditasi</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Sertifikasi Akreditasi KAN</div>
								<div class="description">No. LI-026-IDN (SNI ISO.IEC 17020:2012)</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Sertifikasi Akreditasi DJK</div>
								<div class="description">No. 5Stf/20/DJL.4/2016</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">SK Penunjukan DJK</div>
								<div class="description">No. 219/20/DJL.4/2016</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif

@if($dataTrans == 'id')
<div class="ui basic feature modal" id="modal-sert-produk">
	<div class="content">
		<div class="ui centered cards">
			<div class="card" style="width: 100%;">
				<div class="card" style="height: 150px;background-color: #a2aebd8f;">
					<div class="center aligned header">
					<h3 class="ui blue color header" style="padding-top: 75px;font-size: 17px;">PENGUJIAN PRODUK TEGANGAN RENDAH</h3>
					</div>
				</div>
				<div class="content">
					<div class="ui list">
						<div class="item">
							<div class="content">
								<div class="header">Elektrik, Magnetik, Pengukuran Elektrik dan Magnetik</div>
								<div class="description">KWh Meter</div>
							</div>
						</div>
						<div class="item">
							<div class="content">
								<div class="header">Kabel Elektrik</div>
								<div class="description">Kabel Listrik Berinsulasi PVC, Kabel Daya, TR, TM dan TT</div>
							</div>
						</div>
						<div class="item">
							<div class="content">
								<div class="header">Aksesoris Listrik</div>
								<div class="description">Tusuk Kontak dan Kotak Kontak, Sakelar, Miniature Circuit Breaker (MCB), Pemutus Sirkuit Arus Sisa Tanpa Proteksi Arus Lebih Terpadu (RCCB)</div>
							</div>
						</div>
					</div>
					<div class="ui list">
						<div class="item">
							<div class="content">
								<div class="header">Material Distribusi Utama</div>
							</div>
						</div>
					</div>
					<h4 class="ui blue ribbon label">Dukungan Akreditasi</h4>
					<div class="ui list">
						<div class="item">
							<div class="content">
								<div class="header">Sertifikasi Akreditasi KAN</div>
								<div class="description">No. LLSPr-005-IDN (SNI ISO.IEC 17065:2012)</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@else
<div class="ui basic feature modal" id="modal-sert-produk">
	<div class="content">
		<div class="ui centered cards">
			<div class="card" style="width: 100%;">
				<div class="card" style="height: 150px;background-color: #a2aebd8f;">
					<div class="center aligned header">
					<h3 class="ui blue color header" style="padding-top: 75px;font-size: 17px;">SCOPE OF PRODUCT CERTIFICATION</h3>
					</div>
				</div>
				<div class="content">
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Power Cable</div>
								<div class="description">
									<li>PVC Insulated Power Cable</li>
									<ul style="columns: 1; padding-left: 1.25rem; margin: 0">
										<li>Fixed Wiring Non Sheath Cable</li>
										<li>Fixed Wiring Sheath Cable</li>
										<li>Flexible Cable</li>
									</ul>
									<li>Power Cable</li>
									<ul style="columns: 1; padding-left: 1.25rem; margin: 0">
										<li>Nominal Voltage 1 kV and 3 kV</li>
										<li>Nominal Voltage 6 kV and 30 kV</li>
									</ul>
								</div>
								<div class="header">Aksesoris Listrik</div>
								<div class="description">
									<ul style="columns: 1; padding-left: 1.25rem; margin: 0">
										<li>Plugs and Sockets</li>
										<li>Switches</li>
										<li>Miniature Circuit Breaker (MCB)</li>
										<li>Residual Current Circuit Breaker (RCCB)</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<h4 class="ui blue ribbon label">ACCREDITATION</h4>
					<div class="ui list">
						<div class="item">
							<div class="content">
								<div class="header">KAN Accreditation</div>
								<div class="description">No. LI-026-IDN ISNI ISO/IEC 1720.2012</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif

@if($dataTrans == 'id')
<div class="ui basic feature modal" id="modal-sert-manajemen">
	<div class="content">
		<div class="ui centered cards">
			<div class="card" style="width: 100%;">
				<div class="card" style="height: 150px;background-color: #a2aebd8f;">
					<div class="center aligned header">
					<h3 class="ui orange color header" style="padding-top: 75px;font-size: 17px;">PENGUJIAN PRODUK TEGANGAN TINGGI</h3>
					</div>
				</div>
				<div class="content">
					<div class="ui list">
						<div class="item">
							<div class="content">
								<div class="header">Sertifikasi Sistem Manajemen Mutu (Berbasis ISO 9001:2015)</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Peralatan Listrik dan Peralatan Optik</li>
										<li>Penyediaan Kelistrikan</li>
										<li>Teknologi Informasi</li>
										<li>Jasa Engineering</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="content">
								<div class="header">Sertifikasi Sistem Manajemen Lingkungan (Berbasis ISO 14001:2015)</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Peralatan Listrik dan Peralatan Optik</li>
										<li>Penyediaan Kelistrikan</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="content">
								<div class="header">Sertifikasi Sistem Manajemen K3 (Berbasis ISO 45001:2018)</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Single Phase Watt Meter</li>
										<li>Three Phase Watt Meter</li>
										<li>Power Meter</li>
										<li>Power Quality</li>
										<li>Power Analyzer</li>
										<li>Peralatan Listrik dan Peralatan Optik</li>
										<li>Penyediaan Kelistrikan</li>
										<li>Teknologi Informasi</li>
										<li>Jasa Engineering</li>
										<li>Trafo Tenaga</li>
										<li>Jasa Engineering</li>
										<li>Trafo Tenaga</li>
										<li>Trafo Distribusi</li>
										<li>LBS (Load Break Switch)</li>
										<li>PHB TM</li>
										<li>PHB TR</li>
										<li>CT / VT</li>
										<li>Isolator</li>
										<li>Arester</li>
										<li>Re-closer</li>
										<li>Peralatan PKDB TM/TT/TET/GI/Jaringan</li>
										<li>FCO (Fuse Cut Out)</li>
										<li>Aksesoris Kabel TM/TT/ (Jointing, terminasi dll.</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<h4 class="ui orange ribbon label">Dukungan Akreditasi</h4>
					<div class="ui list">
						<div class="item">
							<div class="content">
								<div class="ui bulleted list">
									<div class="item">
										<div class="content">
											<div class="header">Sertifikasi Akreditasi KAN</div>
											<div class="description">No. LSSM-011-IDN dan LSSML-009-IDN (SNI ISO.IEC 17021-1:2015)</div>
										</div>
									</div>
									<div class="item">
										<div class="content">
											<div class="header">SK Dirjen Inwasnaker & K3</div>
											<div class="description">No. 278 tahun 2016</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@else
<div class="ui basic feature modal" id="modal-sert-manajemen">
	<div class="content">
		<div class="ui centered cards">
			<div class="card" style="width: 100%;">
				<div class="card" style="height: 150px;background-color: #a2aebd8f;">
					<div class="center aligned header">
					<h3 class="ui orange color header" style="padding-top: 75px;font-size: 17px;">SCOPE OF MANAGEMENT SYSTEM CERTIFICATION</h3>
					</div>
				</div>
				<div class="content">
					<div class="ui list">
						<div class="item">
							<div class="content">
								<div class="header">Quality Management System Certification (based on ISO 9001:2015)</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Electrical Equipment and Optical Equipment</li>
										<li>Electricity Provision</li>
										<li>Information Technology</li>
										<li>Engineering Services</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="content">
								<div class="header">Environmental Management System Certification (based on ISO 140001:2015)</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Electrical Equipment and Optical Equipment</li>
										<li>Electricity Provision</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="content">
								<div class="header">OHSAS Management System Certification (based on OHSAS 18001:20075)</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Single Phase Watt Meter</li>
										<li>Three Phase Watt Meter</li>
										<li>Power Meter</li>
										<li>Power Quality</li>
										<li>Power Analyzer</li>
										<li>Electrical Equipment and Optical Equipment</li>
										<li>Electricity Provision</li>
										<li>Information Technology</li>
										<li>Engineering Services</li>
										<li>Trafo Tenaga</li>
										<li>Trafo Tenaga</li>
										<li>Trafo Distribution</li>
										<li>LBS (Load Break Switch)</li>
										<li>PHB TM</li>
										<li>PHB TR</li>
										<li>CT / VT</li>
										<li>Isolator</li>
										<li>Arester</li>
										<li>Re-closer</li>
										<li>Equipment PKDB TM/TT/TET/GI/Network</li>
										<li>FCO (Fuse Cut Out)</li>
										<li>Accessories Cable TM/TT/ (Jointing, terminasi dll.</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<h4 class="ui orange ribbon label">Accreditation</h4>
					<div class="ui list">
						<div class="item">
							<div class="content">
								<div class="ui bulleted list">
									<div class="item">
										<div class="content">
											<div class="header">Certificate of Accreditation KAN</div>
											<div class="description">No. LSSM-011-IDN dan LSSML-009-IDN (SNI ISO.IEC 17021-1:2015)</div>
										</div>
									</div>
									<div class="item">
										<div class="content">
											<div class="header">SK Dirjen Inwasnaker & K3</div>
											<div class="description">No. 278 tahun 2016</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
<div class="ui basic feature modal" id="modal-ases">
	<div class="content">
		<div class="ui centered cards">
			<div class="card" style="width: 100%;">
				<div class="card" style="height: 150px;background-color: #a2aebd8f;">
					<div class="center aligned header">
					<h3 class="ui orange color header" style="padding-top: 75px;font-size: 17px;">Assesment</h3>
					</div>
				</div>
				<div class="content">
					<div class="ui list">
						<div class="item">
							<div class="content">
								<div class="header">Assesment Pembangkit</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Assesment PLTU</li>
										<li>Assesment PLTD</li>
										<li>Assesment PLTG/U</li>
										<li>Assesment PLTA</li>
										<li>Assesment PLTS</li>
									</ul>
								</div>
								<div class="header">Assesment Elektrikal</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Assesment Transformator Tenaga</li>
										<li>Assesment Generator</li>
										<li>Assesment Gardu Induk</li>
									</ul>
								</div>
								<div class="header">Assesment Sipil (Struktur, Geo Teknik/Mekanika teknik, Hidrologi dan Geodesi)</div>
								<div class="description">
									<ul style="columns: 4; padding-left: 1.25rem; margin: 0">
										<li>Assesmen Bendungan</li>
										<li>Assesmen Pondasi</li>
										<li>Assesmen Tower Transmisi</li>
										<li>Assesmen Daerah Aliran Sungai (DAS)</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ui basic feature modal" id="modal-tracking">
	<div class="content">
		<div class="ui centered cards">
			<div class="card" style="width: 100%;">
				<div class="content">
					<h3 class="ui green color header">Cari Data Tracking</h3>

					<div class="ui active inverted dimmer">
						<div class="ui text loader">Loading</div>
					</div>
					
					<form action="" class="ui form">
						<div class="field">
							<div class="ui action input">
								<input type="text" name="filter[display_name]" placeholder="No Order">
								<button type="button" class="ui teal icon filter button" data-content="Cari Data" onclick="openTrackingResult()">
									<i class="search icon"></i> Cari Data Order
								</button>
							</div>
						</div>
					</form>

					<table id="tracking" class="ui celled compact red table" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th class="ui center aligned">#</th>
								<th class="ui center aligned ten wide column">Layanan</th>
								{{-- <th class="ui center aligned">Jenis Alat</th>
								<th class="ui center aligned">Tipe Alat</th>
								<th class="ui center aligned">Merk</th> --}}
								<th class="ui center aligned" colspan="2">Status</th>
							</tr>
						</thead>
						<tbody>
							<tr class="center aligned">
								<td>1</td>
								<td class="left aligned">Pengujian KWh Meter</td>
								{{-- <td>Taping Connector</td>
								<td>Cable T-001</td>
								<td>Dzumba</td> --}}
								<td><a class="ui teal fluid label">Kaji Ulang</a></td>
								<td>01 Januari 2019</td>
							</tr>
							<tr class="center aligned">
								<td>2</td>
								<td class="left aligned">Pengujian Relay Arah</td>
								{{-- <td>PHB (Box MCB)</td>
								<td>MCB M-001</td>
								<td>MCB</td> --}}
								<td><a class="ui teal fluid label">Surat Penawaran</a></td>
								<td>02 Januari 2019</td>
							</tr>
							<tr class="center aligned">
								<td>3</td>
								<td class="left aligned">Kalibrasi Clamp Meter</td>
								{{-- <td>Elektrode Pembumian (Arde)</td>
								<td>Arde Arde-001</td>
								<td>Arde</td> --}}
								<td><a class="ui blue fluid label">Kalibrasi</a></td>
								<td>03 Januari 2019</td>
							</tr>
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection