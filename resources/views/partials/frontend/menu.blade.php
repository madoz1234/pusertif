<!-- Sidebar Menu -->
<div class="ui vertical inverted sidebar menu">
	<a class="item" href="#about">Tentang Kami</a>
	<a class="item" href="#e-uji">E-Uji</a>
	<a class="item" href="#e-kal">E-Kal</a>
	<a class="item" href="#e-slo">E-SLO</a>
	<a class="item" href="#e-sertifikasi">E-Sertifikasi Produk</a>
	<a class="item" href="#e-manajemen">E-Sistem Manajemen</a>
	<a class="item" href="{{ url('tracking') }}">Tracking</a>
</div>