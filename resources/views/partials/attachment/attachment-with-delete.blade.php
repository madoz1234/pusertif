
<div class="card">
	<div class="card-body">
		<label>{{ $foto or 'Upload Foto' }}</label>
		<input type="file" class="form-control" name="attachment[]" required="" autocomplete="off" accept="image/*" {{ $multi or '' }}>
	</div>
</div><br>

@if(isset($record))
@if(isset($record->attachments))
<div class="card">
	<div class="card-body">
		<div class="row">
			@if($record->attachments->count() > 0)
			@foreach($record->attachments as $file)
			<div class="col-md-4">	
				<div class="form-group">
					<input type="hidden" class="form-control" name="attachment_exists[{{$file->id or ''}}]" autocomplete="off" accept="image/*">
					<center><div class="card" style="width: 13rem;">
						<img class="card-img-top" src="{{ url('storage/'.$file->url) }}" alt="Card image cap" style="height: 165px">
					</div></center>
				</div>
			</div>
			@endforeach
			@endif
		</div>
	</div>
</div>
@endif
@endif
