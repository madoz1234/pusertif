<div class="ui top attached segment">
  <a href="javascript:void(0)" class="ui purple  ribbon label">{{ $label or trans('translator.Isi Lampiran') }}</a>
  <div id="daftar-hadir-area">
  </div>
  <div class="ui inline grid field" data-tooltip="{{ $tooltip or 'File Maksimal 2MB' }}" style="width: 100%">
		<div class="ui action choises input " style="width: 615px;padding: .5rem 0">
			<input type="text" value="" readonly>
			<input type="file" name="attachment[]" accept="{{ $accept or 'image/*'}}" {{ $multipe or '' }} style="display: none!important;">
			<div class="ui icon button">
				<i class="cloud upload alternate icon"></i>
			</div>
		</div>
  </div>
</div>

@if(isset($record))
@if(isset($record->attachments))
<div class="card" style="padding: 2.5rem 0">
	<div class="card-body">
		<div class="ui grid ">
			@if($record->attachments->count() > 0)
			@foreach($record->attachments as $file)
			<div class="col-md-4">	
				<div class="form-group">
					<div class="card" style="width: 13rem;">
						<img class="card-img-top" src="{{ url('storage/'.$file->url) }}" alt="Card image cap" style="height: 165px">
					</div>
				</div>
			</div>
			@endforeach
			@endif
		</div>
	</div>
</div>
@endif
@endif