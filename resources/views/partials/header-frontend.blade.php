<div class="ui fixed blue menu">
    <a href="{{ url('/backend') }}" class="header item" style="letter-spacing: 0px;">
        <img class="logo" src="{{ asset('img/logo.png')}}" style="width: 2em;padding-top: 2px;padding-left:2px;">&nbsp;&nbsp;
        {{ config('app.name') }}
    </a>
    <div class="menu">
        <a href="#" class="item" onclick="toggleSidebar()">
            <i class="sidebar icon"></i>
        </a>
    </div>
    @php
    $dataTrans = 'id';
	    if(Auth::check()){
          if(auth()->user()->id == true){
              if(\App\Models\Translators::where('created_by',auth()->user()->id)->first()){
              		$dataTrans = \App\Models\Translators::where('created_by',auth()->user()->id)->first()->translator;
	          }else {
	              	$dataTrans = 'id';
	          }
	      }else {
	          $dataTrans = 'id';
	      }
	  	}
  	@endphp
  <div class="right menu">
	    <div class="ui pointing dropdown item {{ ($dataTrans == 'id') ? '' : 'translate' }}" style="{{ ($dataTrans == 'id') ? 'background-color: #a2aebd8f;
	    color: #fff !important;' : '' }} " data-value="id">
	    	<a class="  " data-value="id" ><i class="ui id flag" style="margin-right: 0"></i> ID</a>
	    </div>
	    <div class="ui pointing dropdown item {{ ($dataTrans == 'en') ? '' : 'translate' }}" style="{{ ($dataTrans == 'en') ? 'background-color: #a2aebd8f;
	    color: #fff !important;' : '' }} " data-value="en">
	    	<a class="" data-value="en"><i class="ui us flag" style="margin-right: 0"></i> EN</a>
	    </div>
	    <div class="ui right dropdown item" id="notifLoader">
	        <i class="ui bell icon" style="margin-right: 0"></i>
	        {{-- @include('partials.notif') --}}
	    </div>

		<div class="ui pointing dropdown item" tabindex="0">
		    <img class="ui avatar image" src="{{ url(auth()->user()->photo ? 'storage/'.auth()->user()->photo : 'img/user-default.jpg') }}"> &nbsp;&nbsp;
		    {{ (Auth::check()) ? isset(auth()->user()->username) ? auth()->user()->username : '' : ''  }}
		    <i class="dropdown icon"></i>
		    <div class="menu transition hidden" tabindex="-1">
		        <a class="item" href="{{ url('/frontend/profile') }}"><i class="user icon"></i> {{trans('translator.Profile')}}</a>
		        <a class="item" href="{{ url('/frontend/ganti-password') }}"><i class="key out icon"></i> {{trans('translator.Ganti Password')}}</a>
		        <a class="item" href="{{ url('/logout') }}"><i class="sign out icon"></i> {{trans('translator.Sign Out')}}</a>
		    </div>
		</div>
	</div>
</div>