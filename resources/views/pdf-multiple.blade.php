<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Dokumen Pendukung</div>
<div class="modal content">
@if(count($data) > 0)
	<div class="ui top demo tabular menu">
		@foreach($data as $key => $val)
			<div @if($key == 0) class="active item fn" @else class="item fn" @endif data-tab="data{{$key}}">{{ $val['new'] }}</div>
		@endforeach
	</div>
	@foreach($data as $kiy => $val)
		<div @if($kiy == 0) class="ui bottom demo active tab" @else class="ui bottom demo tab" @endif data-tab="data{{$kiy}}">
			@php
				$link = asset('/storage/'.$val['old']);
			@endphp
			<div style="min-height:1000px;">
				<iframe title="Advertisement" id="test.pdf" src="{{ $link }}#toolbar=0" frameborder="0" marginwidth="0" marginheight="0" scrolling="yes" width="100%" style="position: relative; height: 1000px;"> </iframe>
			</div>
		</div>
	@endforeach
@else
	<p style="text-align: center;font-weight: bold;">
		File Tidak Ditemukan
	</p>
@endif
</div>
<script type="text/javascript">
        $('.menu .item').tab();
    	$('.next').tab();    
    </script>