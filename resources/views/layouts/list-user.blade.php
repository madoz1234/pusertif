@extends('layouts.base-user')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables/responsive.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.semanticui.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/fullcalendar/scheduler.min.css') }}">
    <style type="text/css">
        td.details-control.button {
            background: green no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.details-control.button {
            background: red no-repeat center center;
        }
        .text-center {
            text-align: center !important;
        }
    </style>
@append

@section('js')
    <script src="{{ asset('plugins/fullcalendar/moment.min.js') }}"></script>
	<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/sweetalert/sweetalert2.js') }}"></script>
	<script src="{{ asset('plugins/fullcalendar/fullcalendar.min.js') }}"></script>
	<script src="{{ asset('plugins/fullcalendar/scheduler.min.js') }}"></script>
@append

@section('scripts')
    <script type="text/javascript">
        // global
        $('.menu .item').tab();
        $('.next').tab();
        var dt = "";
        var formRules = [];
        var initModal = function(){
             $('.menu .item').tab();
        	 $('.next').tab();
          return false;
        };
        var dismissModal = function(){
            return false;
        };
        var postSave = function(response){
            return false;
        };
        var onShow = function(){
            $('.checkbox').checkbox();
            $('.ui.dropdown').dropdown({
                onChange: function(value) {
                    var target = $(this).dropdown();
                    if (value!="") {
                        target
                            .find('.dropdown.icon')
                            .removeClass('dropdown')
                            .addClass('delete')
                            .on('click', function() {
                                target.dropdown('clear');
                                $(this).removeClass('delete').addClass('dropdown');
                                return false;
                            });
                    }
                }
            });
            // force onChange  event to fire on initialization
            $('.ui.dropdown')
                .closest('.ui.selection')
                .find('.item.active').addClass('qwerty').end()
                .dropdown('clear')
                    .find('.qwerty').removeClass('qwerty')
                .trigger('click');

            return false;
        };

        $.fn.form.settings.prompt = {
            empty                : '{name} tidak boleh kosong',
            checked              : '{name} harus dipilih',
            email                : '{name} tidak valid',
            url                  : '{name} tidak valid',
            regExp               : '{name} is not formatted correctly',
            integer              : '{name} must be an integer',
            decimal              : '{name} must be a decimal number',
            number               : '{name} hanya boleh berisikan angka',
            is                   : '{name} must be "{ruleValue}"',
            isExactly            : '{name} must be exactly "{ruleValue}"',
            not                  : '{name} cannot be set to "{ruleValue}"',
            notExactly           : '{name} cannot be set to exactly "{ruleValue}"',
            contain              : '{name} cannot contain "{ruleValue}"',
            containExactly       : '{name} cannot contain exactly "{ruleValue}"',
            doesntContain        : '{name} must contain  "{ruleValue}"',
            doesntContainExactly : '{name} must contain exactly "{ruleValue}"',
            minLength            : '{name} setidaknya haru memiliki {ruleValue} karakter',
            length               : '{name} must be at least {ruleValue} characters',
            exactLength          : '{name} must be exactly {ruleValue} characters',
            maxLength            : '{name} tidak boleh lebih dari {ruleValue} karakter',
            match                : '{name} must match {ruleValue} field',
            different            : '{name} must have a different value than {ruleValue} field',
            creditCard           : '{name} must be a valid credit card number',
            minCount             : '{name} must have at least {ruleValue} choices',
            exactCount           : '{name} must have exactly {ruleValue} choices',
            maxCount             : '{name} must have {ruleValue} or less choices'
        };

        $(document).on('click', '.ui.tambah.data', function(event) {
        	$('#dataForm')
        		.modal('show')
        	;
        });

        $(document).on('click', '.ui.ubah.data', function(event) {
        	$('#ubahForm')
        		.modal('show')
        	;
        });

        $(document).on('click', '.ui.tambah.pln', function(event) {
        	$('#PLN')
        		.modal('show')
        	;
        });

        $(document).on('click', '.ui.tambah.non', function(event) {
        	$('#non-PLN')
        		.modal('show')
        	;
        });

        $(document).on('click', '.ui.detail.data', function(event) {
        	$('#lihat-PLN')
        		.modal('show')
        	;
        });

        $(document).on('click', '.lihat_jadwal', function(event) {
	     //    	$('#jadwal').modal({
	     //    		onShow: function(){
	     //    			$('#fulljadwal').fullCalendar({
	     //    				header: {
	     //    					left: 'today prev,next',
	     //    					center: 'title',
	     //    					right: 'month,timelineWeek'
	     //    				},
	     //    				defaultView: 'timelineMonth',
	     //    				resourceColumns: [
	     //    				{
	     //    					labelText: 'Lingkup KPJ',
	     //    					field: 'title'
	     //    				},
	     //    				],
	     //    				resources: [
	     //    				{ id: 'a', title: 'Kalibrasi 5 (lima) unit alat ukur' },
	     //    				{ id: 'b', title: 'Kalibrasi 1 (satu) unit 3-in-1 Test System for Medium & High Voltage Circuit Creakers' },
	     //    				{ id: 'c', title: 'Kalibrasi 15 (lima belas) unit alat dipuslitbag' },
	     //    				{ id: 'd', title: 'Kalibrasi 4 (empat) unit alat ukur di PLN Puslitbang' },
	     //    				{ id: 'e', title: 'Kalibrasi 3 (tiga) unit Energi Meter (fungsi Kwh 7 Kvarh)' },
	     //    				{ id: 'f', title: 'Kalibrasi 5 (lima) unit alat dipuslitbag' },
	     //    				{ id: 'g', title: 'Kalibrasi 5 (lima) unit alat ukur di PLN Puslitbang' },
	     //    				{ id: 'h', title: 'Kalibrasi 5 (lima) unit alat ukur' },
	     //    				{ id: 'i', title: 'Kalibrasi 1 (satu) unit 3-in-1 Test System for Medium & High Voltage Circuit Creakers' },
	     //    				{ id: 'j', title: 'Kalibrasi 15 (lima belas) unit alat dipuslitbag' },
	     //    				{ id: 'k', title: 'Kalibrasi 4 (empat) unit alat ukur di PLN Puslitbang' },
	     //    				{ id: 'l', title: 'Kalibrasi 3 (tiga) unit Energi Meter (fungsi Kwh 7 Kvarh)' },
	     //    				{ id: 'm', title: 'Kalibrasi 5 (lima) unit alat dipuslitbag' },
	     //    				{ id: 'n', title: 'Kalibrasi 5 (lima) unit alat ukur di PLN Puslitbang' },
	     //    				]
	     //    			});
						// //fullcalendar
	     //    		},
	     //    		onHidden: function(){
	     //    			$('#formModal').html(`<div class="ui inverted loading dimmer">
	     //    				<div class="ui text loader">Loading</div>
	     //    				</div>`);
	     //    			dismissModal();
	     //    		},
	     //    		onDeny    : function(){
	     //    			window.alert('Wait not yet!');
	     //    			return false;
	     //    		},
	     //    	}).modal('show');
        });
        
        // $(document).on('click', '.ui.add.button', function(event) {
        //     event.preventDefault();
        //     // /* Act on the event */
        //     loadModal({
        //         'url' : '{{ url($pageUrl) }}/create',
        //         'modal' : '.small.modal',
        //         'formId' : '#dataForm',
        //         'onShow' : function(){ 
        //             onShow();
        //         },
        //     })
        // });
        
        $(document).on('click', '.ui.edit.button', function(event) {
            event.preventDefault();
            var id = $(this).data('id');
            // /* Act on the event */
            loadModal({
                'url' : '{{ url($pageUrl) }}/'+id+'/edit',
                'modal' : '.{{ $modalSize }}.modal',
                'formId' : '#dataForm',
                'onShow' : function(){ 
                    onShow();
                },
            })
        });

        $(document).on('click', '.ui.lihat.button', function(event) {
        	event.preventDefault();
            // /* Act on the event */
            loadModal({
                'url' : '{{ url($pageUrl) }}/show',
                'modal' : '.small.modal',
                'formId' : '#dataForm',
                'onShow' : function(){ 
                    onShow();
                },
            })
        });

        $(document).on('click', '.ui.konfirmasi.button', function(event) {
        	swal({
        		title: 'Apakah anda yakin?',
        		text: "Data yang sudah dikirim, tidak dapat dikembalikan!",
        		type: 'warning',
        		showCancelButton: true,
        		confirmButtonColor: '#3085d6',
        		cancelButtonColor: '#d33',
        		confirmButtonText: 'Terbitkan',
        		cancelButtonText: 'Batal'
        	}).then((result) => {
        	})
        });

        $(document).on('click', '.ui.hapus.button', function(event) {
        	swal({
        		title: 'Apakah anda yakin?',
        		text: "Data yang sudah dikirim, tidak dapat dikembalikan!",
        		type: 'warning',
        		showCancelButton: true,
        		confirmButtonColor: '#3085d6',
        		cancelButtonColor: '#d33',
        		confirmButtonText: 'Hapus',
        		cancelButtonText: 'Batal'
        	}).then((result) => {
        	})
        });

        var tinymceOpt = {
            selector: '.editor',
            statusbar: false,
            height: '250px',
            menubar:false,
            branding: false,
            font_formats: 'Lato=Lato,sans-serif;Open Sans=Open Sans,sans-serif;Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
            plugins: "lists, advlist, responsivefilemanager, image, table, link, code",
            toolbar: "undo redo | styleselect | fontselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link table image | code",

            external_filemanager_path:"/plugins/filemanager/",
            filemanager_title:"Kelola File" ,
            external_plugins: { "filemanager" : "/plugins/filemanager/plugin.min.js"},
            
            setup : function(editor) {
                editor.on('change', function () {
                    tinymce.triggerSave();
                });
            }
        };
    </script>
    @yield('rules')
    @yield('init-modal')
    
    @include('scripts.datatable')
    @include('scripts.action')
@append

@section('content')
    @section('content-header')
    <div class="ui breadcrumb">
        <a class="active section" href="{{ url('/') }}"><i class="home icon"></i> Home</a>
        <i class="right chevron icon divider"></i>
        <?php $i=1; $last=count($breadcrumb);?>
        @foreach ($breadcrumb as $name => $link)
            @if($i++ != $last)
                <a href="{{ $link }}" class="section">{{ $name }}</a>
                <i class="right chevron icon divider"></i>
            @else
                <div class="active section">{{ $name }}</div>
            @endif
        @endforeach
    </div>
    <h2 class="ui header">
      <div class="content">
        {!! $title or '-' !!}
        <div class="sub header">{!! $subtitle or ' ' !!}</div>
      </div>
    </h2>
    @show

    <div class="ui clearing divider" style="border-top: none !important; margin:10px"></div>

    @section('content-body')
    <div class="ui grid">
        <div class="sixteen wide column main-content">
            <div class="ui segments" style="border:none; box-shadow: none;">
                <div class="ui segment">
                    <form class="ui filter form">
                        <div class="inline fields">
                            {{-- <div class="field">
                                <select name="filter[entri]" id="" class="ui compact selection dropdown">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                            </div> --}}
                            @section('filters')
                                <div class="ui one wide field">
                                    <select name="filter[page]">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                                <div class="ui three wide field">
                                    <input name="filter[search]" placeholder="No.Pendaftaran/Nama/UN" type="text">
                                </div>
                                <button type="button" class="ui teal icon filter button" data-content="Cari Data">
                                    <i class="search icon"></i>
                                    &nbsp;&nbsp;Cari
                                </button>
                            @show
                            <div style="margin-left: auto; margin-right: 1px;">
                                @section('toolbars')
                                    @if($pagePerms == '' || auth()->user()->can($pagePerms.'-add'))
                                    <button type="button" class="ui blue @if($pagePerms != '' && !auth()->user()->can($pagePerms.'-add')) disabled @endif button add">
                                        <i class="add icon"></i>
                                        {{trans('translator.Tambah Data')}}
                                    </button>
                                    @endif
                                @show
                            </div>
                        </div>
                    </form>
                    @section('subcontent')
                        @if($mockup == true)
                            @yield('tables')
                        @else
                            @if(isset($tableStruct))
                            <table id="listTable" class="ui celled compact red table display" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        @foreach ($tableStruct as $struct)
                                            <th class="center aligned">{{ trans('translator.'.$struct['label']) }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @yield('tableBody')
                                </tbody>
                            </table>
                            @endif
                        @endif
                    @show
                </div>
            </div>
        </div>
    </div>
    @show
@endsection

@section('modals')
<div class="ui {{ $modalSize or 'mini' }} modal" id="formModal">
    <div class="ui inverted loading dimmer">
        <div class="ui text loader">Loading</div>
    </div>
</div>
@append
