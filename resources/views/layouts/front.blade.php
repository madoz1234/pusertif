<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>{{ config('app.name') }}</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon-1.ico') }}">

    {{-- Style --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/semantic.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/front.css') }}">
</head>

<body id="app">

    @include('partials.frontend.menu')


    <!-- Page Contents -->
    <div class="pusher">
        @yield('content')
    
        @include('partials.frontend.footer')
    </div>

    @yield('modals')

    {{-- Script --}}
    <script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
        ]) !!}
    </script>

    <script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('semantic/semantic.min.js') }}"></script>

    <script>
         $(document).on('click','.translate',function(){
            var trans = $(this).data('value');
            var ip = '999';
                $.ajax({
                  url: '{{ url("langs") }}',
                  type: 'POST',
                  data: {_token: "{{ csrf_token() }}",translator:trans,ip:ip},
                  success: function(resp){
                    location.reload();
                  },
                  error : function(resp){
                    location.reload();
                  }
                });
          });
        $(document).ready(function() {
            $('.masthead').visibility({
                once: false,
                onBottomPassed: function() {
                    $('.fixed.menu').transition('fade in');
                },
                onBottomPassedReverse: function() {
                    $('.fixed.menu').transition('fade out');
                }
            });

            $('.masthead-menu .overlay').visibility({
              type   : 'fixed',
              offset : 8 // give some space from top of screen
            });

            $('.ui.sidebar').sidebar('attach events', '.toc.item');


            // Select all links with hashes
            $('a[href*="#"]')
              // Remove links that don't actually link to anything
              .not('[href="#"]')
              .not('[href="#0"]')
              .click(function(event) {
                // On-page links
                if (
                  location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
                  && 
                  location.hostname == this.hostname
                ) {
                  $('.masthead').find('a').removeClass('active');
                  $('#nav').find('a').removeClass('active');

                  // Figure out element to scroll to
                  var target = $(this.hash);
                  target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                  // Does a scroll target exist?
                  if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    // $(this).addClass('active');
                    $('.masthead').find('a[href="' + $(this).attr("href") + '"]').addClass('active');
                    $('#nav').find('a[href="' + $(this).attr("href") + '"]').addClass('active');
                    $('html, body').animate({
                      scrollTop: target.offset().top
                    }, 1000, function() {
                      // Callback after animation
                      // Must change focus!
                      var $target = $(target);
                      $target.focus();
                      if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                      } else {
                        $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                      };
                    });
                  }

                  $('.masthead').find('a').removeClass('active');
                }
              });

        });
    </script>

    @yield('scripts')
</body>
</html>
