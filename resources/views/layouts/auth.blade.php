<!DOCTYPE html>
<html>
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"> --}}

    <!-- Site Properties -->
    <title>{{ config('app.longname') }}</title>

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon-1.ico') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/lato/stylesheet.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/semantic.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/login-style.css') }}">

    @yield('styles')
</head>
<body>
    <div class="overlay"></div>
    @yield('content')

    <script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <script src="{{ asset('plugins/jQuery/jquery.form.min.js') }}"></script>
    <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
    <script src="{{ asset('plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('semantic/semantic.min.js') }}"></script>

    <script type="text/javascript">
        $('.menu .item').tab();
        $('.next').tab();  
        var dt = "";
        var formRules = [];
        var initModal = function(){
            return true;
        };
        var dismissModal = function(){
            return false;
        };
        var postSave = function(response){
            return false;
        };

        $.fn.form.settings.prompt = {
            empty                : '{name} tidak boleh kosong',
            checked              : '{name} harus dipilih',
            email                : '{name} tidak valid',
            url                  : '{name} tidak valid',
            regExp               : '{name} is not formatted correctly',
            integer              : '{name} must be an integer',
            decimal              : '{name} must be a decimal number',
            number               : '{name} hanya boleh berisikan angka',
            is                   : '{name} must be "{ruleValue}"',
            isExactly            : '{name} must be exactly "{ruleValue}"',
            not                  : '{name} cannot be set to "{ruleValue}"',
            notExactly           : '{name} cannot be set to exactly "{ruleValue}"',
            contain              : '{name} cannot contain "{ruleValue}"',
            containExactly       : '{name} cannot contain exactly "{ruleValue}"',
            doesntContain        : '{name} must contain  "{ruleValue}"',
            doesntContainExactly : '{name} must contain exactly "{ruleValue}"',
            minLength            : '{name} setidaknya haru memiliki {ruleValue} karakter',
            length               : '{name} must be at least {ruleValue} characters',
            exactLength          : '{name} must be exactly {ruleValue} characters',
            maxLength            : '{name} tidak boleh lebih dari {ruleValue} karakter',
            match                : '{name} must match {ruleValue} field',
            different            : '{name} must have a different value than {ruleValue} field',
            creditCard           : '{name} must be a valid credit card number',
            minCount             : '{name} must have at least {ruleValue} choices',
            exactCount           : '{name} must have exactly {ruleValue} choices',
            maxCount             : '{name} must have {ruleValue} or less choices'
        };

        $(document).on('click', '.ui.hapus.button', function(event) {
            swal({
                title: 'Apakah anda yakin?',
                text: "Data yang sudah dikirim, tidak dapat dikembalikan!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal'
            }).then((result) => {
            })
        });



        var tinymceOpt = {
            selector: '.editor',
            height: '250px',
            menubar:false,
            branding: false,
            font_formats: 'Lato=Lato,sans-serif;Open Sans=Open Sans,sans-serif;Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
            plugins: "lists, advlist, responsivefilemanager, image, table, link, code",
            toolbar: "undo redo | styleselect | fontselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link table image | code",

            external_filemanager_path:"/plugins/filemanager/",
            filemanager_title:"Kelola File" ,
            external_plugins: { "filemanager" : "/plugins/filemanager/plugin.min.js"},

            setup : function(editor) {
                editor.on('change', function () {
                    tinymce.triggerSave();
                });
            }
        };

    </script>

    @include('scripts.register')
    
    <script>
        $(document)
        .ready(function() {
        	$(document).on('click','.translate',function(){
				var trans = $(this).data('value');
				var ip = '999';
				$.ajax({
					url: '{{ url("langs") }}',
					type: 'POST',
					data: {_token: "{{ csrf_token() }}",translator:trans,ip:ip},
					success: function(resp){
						window.location.reload();
					},
					error : function(resp){
						window.location.reload();
					}
				});
			});
            $('.ui.form')
            .form({
                fields: {
                    username: {
                        identifier  : 'text',
                        rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your username'
                        }
                        ]
                    },
                    password: {
                        identifier  : 'password',
                        rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your password'
                        }
                        ]
                    }
                }
            })
            ;
        })
        ;
    </script>

    @yield('scripts')
    @yield('scripts.register')
    @yield('rules')
    @yield('init-modal')
</body>
</html>
