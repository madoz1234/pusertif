@extends('layouts.base')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.semanticui.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert/sweetalert2.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    {{-- <script src="{{ asset('plugins/datatables/dataTables.semanticui.js') }}"></script> --}}
    <script src="{{ asset('plugins/sweetalert/sweetalert2.js') }}"></script>
    <script src="{{ asset('plugins/tinymce/tinymce.min.js') }}"></script>
@append

@section('scripts')

    <script type="text/javascript">
        // global
        $('.menu .item').tab();
        $('.next').tab();  
        var dt = "";
        var formRules = [];
        var initModal = function(){
            onShow();
        };
        var dismissModal = function(){
        	if($('#formModal').hasClass('.small.modal')){
        		$('#formModal').removeClass('.small.modal');
        		$('#formModal').removeClass('small');
        	}
            return false;
        };
        var postSave = function(response){
            return false;
        };

        $.fn.form.settings.prompt = {
            empty                : '{name} tidak boleh kosong',
            checked              : '{name} harus dipilih',
            email                : '{name} tidak valid',
            url                  : '{name} tidak valid',
            regExp               : '{name} is not formatted correctly',
            integer              : '{name} must be an integer',
            decimal              : '{name} must be a decimal number',
            number               : '{name} hanya boleh berisikan angka',
            is                   : '{name} must be "{ruleValue}"',
            isExactly            : '{name} must be exactly "{ruleValue}"',
            not                  : '{name} cannot be set to "{ruleValue}"',
            notExactly           : '{name} cannot be set to exactly "{ruleValue}"',
            contain              : '{name} cannot contain "{ruleValue}"',
            containExactly       : '{name} cannot contain exactly "{ruleValue}"',
            doesntContain        : '{name} must contain  "{ruleValue}"',
            doesntContainExactly : '{name} must contain exactly "{ruleValue}"',
            minLength            : '{name} setidaknya haru memiliki {ruleValue} karakter',
            length               : '{name} must be at least {ruleValue} characters',
            exactLength          : '{name} must be exactly {ruleValue} characters',
            maxLength            : '{name} tidak boleh lebih dari {ruleValue} karakter',
            match                : '{name} must match {ruleValue} field',
            different            : '{name} must have a different value than {ruleValue} field',
            creditCard           : '{name} must be a valid credit card number',
            minCount             : '{name} must have at least {ruleValue} choices',
            exactCount           : '{name} must have exactly {ruleValue} choices',
            maxCount             : '{name} must have {ruleValue} or less choices'
        };

        $(document).on('click', '.ui.add.button', function(event) {
            event.preventDefault();
            // /* Act on the event */
            loadModal({
                'url' : '{{ url($pageUrl) }}/create',
                'modal' : '.small.modal',
                'formId' : '#dataForm',
                'onShow' : function(){ 
                    onShow();
                },
            })
        });
        
        $(document).on('click', '.ui.edit.button', function(event) {
            event.preventDefault();
            var id = $(this).data('id');
            // /* Act on the event */
            loadModal({
                'url' : '{{ url($pageUrl) }}/'+id+'/edit',
                'modal' : '.{{ $modalSize }}.modal',
                'formId' : '#dataForm',
                'onShow' : function(){ 
                    onShow();
                },
            })
        });

        $(document).on('click', '.ui.lihat.button', function(event) {
        	event.preventDefault();
            // /* Act on the event */
            loadModal({
                'url' : '{{ url($pageUrl) }}/show',
                'modal' : '.small.modal',
                'formId' : '#dataForm',
                'onShow' : function(){ 
                    onShow();
                },
            })
        });

        $(document).on('click', '.ui.konfirmasi.button', function(event) {
        	swal({
        		title: 'Apakah anda yakin?',
        		text: "Data yang sudah dikirim, tidak dapat dikembalikan!",
        		type: 'warning',
        		showCancelButton: true,
        		confirmButtonColor: '#3085d6',
        		cancelButtonColor: '#d33',
        		confirmButtonText: 'Terbitkan',
        		cancelButtonText: 'Batal'
        	}).then((result) => {
        	})
        });

        $(document).on('click', '.ui.hapus.button', function(event) {
        	swal({
        		title: 'Apakah anda yakin?',
        		text: "Data yang sudah dikirim, tidak dapat dikembalikan!",
        		type: 'warning',
        		showCancelButton: true,
        		confirmButtonColor: '#3085d6',
        		cancelButtonColor: '#d33',
        		confirmButtonText: 'Hapus',
        		cancelButtonText: 'Batal'
        	}).then((result) => {
        	})
        });
        
        var tinymceOpt = {
            selector: '.editor',
            height: '250px',
            menubar:false,
            branding: false,
            font_formats: 'Lato=Lato,sans-serif;Open Sans=Open Sans,sans-serif;Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
            plugins: "lists, advlist, responsivefilemanager, image, table, link, code",
            toolbar: "undo redo | styleselect | fontselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link table image | code",

            external_filemanager_path:"/plugins/filemanager/",
            filemanager_title:"Kelola File" ,
            external_plugins: { "filemanager" : "/plugins/filemanager/plugin.min.js"},
            
            setup : function(editor) {
                editor.on('change', function () {
                    tinymce.triggerSave();
                });
            }
        };

    </script>
    @yield('rules')
    @yield('init-modal')

    @include('scripts.pengembalian')
    @include('scripts.action')
@append

@section('content')
    @section('content-header')
    <div class="ui breadcrumb">
        <a class="active section" href="{{ url('/') }}"><i class="home icon"></i> Home</a>
        <i class="right chevron icon divider"></i>
        <?php $i=1; $last=count($breadcrumb);?>
        @foreach ($breadcrumb as $name => $link)
            @if($i++ != $last)
                <a href="{{ $link }}" class="section">{{ $name }}</a>
                <i class="right chevron icon divider"></i>
            @else
                <div class="active section">{{ $name }}</div>
            @endif
        @endforeach
    </div>
    <h2 class="ui header">
      <div class="content">
        {!! $title or '-' !!}
        <div class="sub header">{!! $subtitle or ' ' !!}</div>
      </div>
    </h2>
    @show

    <div class="ui clearing divider" style="border-top: none !important; margin:10px"></div>

    @section('content-body')
    @section('beforecontent')
    @show
    <div class="ui grid">
        <div class="sixteen wide column main-content">
            <div class="ui segments" style="border:none; box-shadow: none;">
                <div class="ui segment" style="padding-right: 0px;padding-top: 0px;padding-bottom: 0px;padding-left: 0px;">
                    <form class="ui filter form">
                        <div class="inline fields">
                            @section('filters')
                                <div class="field">
                                    <input name="filter[kode]" placeholder="Kode Area" type="text">
                                </div>
                                <div class="field">
                                    <input name="filter[area]" placeholder="Area" type="text">
                                </div>
                                <button type="button" class="ui teal icon filter button" data-content="Cari Data">
                                    <i class="search icon"></i>
                                </button>
                                <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
                                    <i class="refresh icon"></i>
                                </button>
                            @show
                            <div style="margin-left: auto; margin-right: 1px;">
                                @section('toolbars')
                                <button type="button" class="ui blue add button">
                                    <i class="plus icon"></i>
                                    Tambah Data
                                </button>
                                @show
                            </div>
                        </div>
                    </form>
                    @section('subcontent')
                        @if($mockup == true)
                                @yield('tables')
                        @else
                            {{-- @if(isset($tableStruct))
                            <table id="listTable" class="ui celled compact red table display" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        @foreach ($tableStruct as $struct)
                                            <th class="center aligned">{{ $struct['label'] or $struct['name'] }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @yield('tableBody')
                                </tbody>
                            </table>
                            @endif --}}
                        @endif
                       
                    @show
                </div>
            </div>
        </div>
    </div>
    @show
@endsection

@section('modals')
<div class="ui tiny modal" id="formModal">
    <div class="ui inverted loading dimmer">
        <div class="ui text loader">Loading</div>
    </div>
</div>
@append
