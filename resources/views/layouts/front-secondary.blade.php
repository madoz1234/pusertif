<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>{{ config('app.name') }}</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon-1.ico') }}">

  {{-- Style --}}
  <link rel="stylesheet" type="text/css" href="{{ asset('semantic/semantic.min.css') }}">

  <link rel="stylesheet" type="text/css" href="{{ asset('css/front.css') }}">
  @stack('css')

  @stack('styles')
</head>

<body id="app">

  @include('partials.frontend.menu-secondary')


  <!-- Page Contents -->
  <div class="pusher"> 
    @yield('content')

    @include('partials.frontend.footer')
  </div>

  @stack('modals')

  {{-- Script --}}
  <script>
  window.Laravel = {!! json_encode([
    'csrfToken' => csrf_token(),
    ]) !!}
  </script>

  <script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
  <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
  <script src="{{ asset('semantic/semantic.min.js') }}"></script>

  @stack('js')

  @yield('scripts')

  <script>
 
  $(document).ready(function() {

            // Select all links with hashes
            $('a[href*="#"]')
              // Remove links that don't actually link to anything
              .not('[href="#"]')
              .not('[href="#0"]')
              .click(function(event) {
                // On-page links
                if (
                  location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
                  && 
                  location.hostname == this.hostname
                  ) {
                  // Figure out element to scroll to
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                  // Does a scroll target exist?
                  if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                      scrollTop: target.offset().top
                    }, 1000, function() {
                      // Callback after animation
                      // Must change focus!
                      var $target = $(target);
                      $target.focus();
                      if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                      } else {
                        $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                      };
                    });
                  }
                }
              });

            });
          </script>

          @stack('scripts')
        </body>
        </html>
