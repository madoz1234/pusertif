<div class="ui vertical stripe segment" id="e-uji">
	<div class="ui middle aligned stackable grid container">
		<div class="row">
			<div class="eight wide column">
				<div class="ui green segment">
					<h3 class="ui green color header">Scope of Test</h3>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Sipil</div>
								<div class="description">Tanah</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Mekanik</div>
								<div class="description">Vibraasi, Tube</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Kimia</div>
								<div class="description">Minyak Insulasi, Batubara</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Fisika</div>
								<div class="description">Batubara</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Kelistrikan</div>
								<div class="description">KWh Meter, Indeks Pengaman, MCB, Kabel Listrik Berinsulasi PVC, Kabel Daya, Kabel Pilih Udara, Isolator, Stick Isolasi, PHB TR, PHB TM, Lengkapan Kabel, Trafo Distribusi, Trafo Arus (CT), Transformer Tegangan (PT), Relay Jarak, Reklay Differensial Kawat Pilot, Relay Arus Lebih, Relay Tegangan Lebih/Kurang, Relay Arah, Relay Cek Sinkron, Relay Gangguan Tanah Terbatas (REF), Relay Penutup Balik, Relay Diferensial Trafo Tenaga, Relay Tunda Waktu, Relay Penguat Medan Hilang, Relay Thermal Overload, Transformator Tenaga, Penangkap Petir/Arrester</div>
							</div>
						</div>
					</div>
					<h5 class="ui green ribbon label">Accreditation</h5>
					<ul>
						<li><b>KAN Accreditation</b> No. LP-005-IDN (SNI ISO/IEC 17025.2017)</li>
					</ul>
				</div>
			</div>
			<div class="six wide right floated column">
				<img src="{{asset('img/white-image.png')}}" class="ui large bordered rounded image">
			</div>
		</div>
	</div>
</div>


<div class="ui vertical stripe segment" id="e-kal">
	<div class="ui middle aligned stackable grid container">
		<div class="row">
			<div class="six wide left floated column">
				<img src="{{asset('img/white-image.png')}}" class="ui large bordered rounded image">
			</div>
			<div class="eight wide column">
				<div class="ui orange segment">
					<h3 class="ui orange header">Scope of Calibration</h3>
					<h4>Electrical Tool Calibration</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Arus</div>
								<div class="description">Clamp Meter, Multimeter, Power Meter, AC/DC Ampere Meter, AC/DC V-A Meter, Relay Tester, Power Analyzer, Power Quality, Tera Table, CT TTR</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Tegangan</div>
								<div class="description">Clamp Meter, Multimeter, Power Meter, AC/DC Ampere Meter, AC/DC V-A Meter, Relay Tester, AC/DC High Voltage Tester, Power Analyzer, Power Quality, Tera Table, PT TTR</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Watt</div>
								<div class="description">Single Phase Watt Meter, Three Phase Watt Meter, Power Meter, Power Quality, Power Analyzer</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Energy Meter</div>
								<div class="description">KWh Meter, KVArh Meter</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Frequency</div>
								<div class="description">Frequency Meter, Power Meter, Relay Tester, Clamp Meter, Multimeter</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Ohm</div>
								<div class="description">Portable Wheatstone Bridge, Resistor Standard, Portable Double Bridge, Precision Double Bridge, Earth Tester, Decade Resistor, Insulation Tester, Micro Ohm Meter</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Degree</div>
								<div class="description">Phase Meter</div>
							</div>
						</div>
					</div>
					<h4>Non Electrical Tool Calibration</h4>
					<div class="ui list">
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Temperature</div>
								<div class="description">Glass Thermometer, Temp. Calibrator, Temp. Detector, Temp. Recorded, Temp.Control, Thermacouple, RTD, Handy Calibrator</div>
							</div>
						</div>
						<div class="item">
							{{-- <i class="circle icon"></i> --}}
							<div class="content">
								<div class="header">Pressure</div>
								<div class="description">Vacuum Gauge, Pressure Gauge</div>
							</div>
						</div>
					</div>
					<h5 class="ui orange ribbon label">Accreditation</h5>
					<ul>
						<li><b>KAN Accreditation</b> No. LK-007-IDN (SNI ISO/IEC 17025.2017)</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>