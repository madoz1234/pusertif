<div class="ui vertical stripe segment" id="about">
	<div class="ui top aligned stackable grid container">
		<div class="row">
			<div class="eight wide column">
				<h3 class="ui header">Tentang Kami</h3>
				<p>PT PLN (PERSERO) Pusat Sertifikasi was formed in 2003, which implicity started with the establishment of "Laboratorium PLN" as an independent institution from 1961 until 1975. In 1975, the institution was changed to "Pusat Penyelidikan Masalah Kelistrikan" (PPMK). In 1996, PPMK was designated as PLN Business Unit under the name of "PT PLN (Persero) Jasa Sertifikasi". Then in 2003, "PT PLN (Persero) Jasa Sertifikasi" was formed as a unit that performs certification in electricity secrot which then in 2015 its name was changed into "PT PLN (Persero) Pusat Sertifikasi".</p>
			</div>
			<div class="six wide right floated column">
				<h3 class="ui header">Vision</h3>
				<p>Recognized as a world-class company that grows up, excelled and trusted by relying on human potentials.</p>
				
				<div class="ui divider"></div>

				<h3 class="ui header">Mision</h3>
				<ol>
					<li>Conducting Business Certification in electricity sector which includes Product Certification and Quality Management System, Environment and OHSAS and Worthiness of Electrical Installation Certification (Technical Inspection) excellentlyand in accordance with the standards/regulations applied with sound business principles to ensure its existence and development with the motto of <b>Faster, Better and Competitive</b>.</li>
					<li>Achieving Innovations and creativities in product developments and certification services in electricity sector in accordance with international standards and technological developments for certification facilities and maintaining local touch to meet quality standards.</li>
					<li>Fulfilling the market demands by prioritizing customer satisfaction and providing the best results to employess and stakeholders.</li>
				</ol>
			</div>
		</div>
	</div>
</div>