<div class="ui vertical stripe segment" id="e-slo">
	<div class="ui middle aligned stackable grid container">
		<div class="row">
			<div class="eight wide column">
				<div class="ui red segment">
					<h3 class="ui red color header">Scope of Operational Wortiniess Certification and Commissioning</h3>
					<h5 class="red text">Operational Worthiness Certification of Electrical Installation</h5>
					<ul>
						<li>Power Generation</li>
						<li>Power Transmission (Transmission Line, Substation)</li>
						<li>Power Distribution (Medium Voltage, Low Voltage)</li>
						<li>Power Utilization (High Voltage, Medium Voltage)</li>
					</ul>
					<h5 class="red text">Testing and Commissioning of Electrical Installation</h5>
					<ul>
						<li>Power Generation</li>
						<li>Power Transmission (Transmission Line, Substation)</li>
						<li>Performance Test and Net Dependable Capacity Test of Power Generation</li>
						<li>Factory Acceptance Test of Electrical Equipments (Power Generation, Power Transmission, Power Distribution)</li>
					</ul>
					<h5 class="ui red ribbon label">Accreditation</h5>
					<ul>
						<li><b>KAN Accreditation</b> No. LI-026-IDN (SNI ISO/IEC 17020.2012)</li>
						<li><b>DJK Accreditation</b> No. 5 StI/20/DJL.4/2016</li>
						<li><b>DJK Letter of Appointment</b> No. 219 K/20/DJL.4/2016</li>
					</ul>
				</div>
			</div>
			<div class="six wide right floated column">
				<img src="{{asset('img/white-image.png')}}" class="ui large bordered rounded image">
			</div>
		</div>
	</div>
</div>

<div class="ui vertical stripe segment" id="e-sertifikasi">
	<div class="ui middle aligned stackable grid container">
		<div class="row">
			<div class="six wide left floated column">
				<img src="{{asset('img/white-image.png')}}" class="ui large bordered rounded image">
			</div>
			<div class="eight wide column">
				<div class="ui blue segment">
					<h3 class="ui blue color header">Scope of Management System Certification</h3>
					<h5>Quality Management System Certification (based on ISO 9001:2015)</h5>
					<ul>
						<li>Electrical and Optical Equipment</li>
						<li>Electric Utility</li>
						<li>Information Technology</li>
						<li>Engineering Service</li>
					</ul>
					<h5 class="blue text">Quality Management System Certification (based on ISO 14001:2015)</h5>
					<ul>
						<li>Electrical and Optical Equipment</li>
						<li>Electric Utility</li>
					</ul>
					<h5 class="blue text">OHSAS Management System Certification (based on OHSAS 18001:20075)</h5>
					<ul>
						<li>Electrical and Optical Equipment</li>
						<li>Electric Utility</li>
						<li>Information Technology</li>
						<li>Engineering Service</li>
					</ul>
					<h5>Audit of Occupational Safety and Health Management System 9based on Ministry of Manpower Regulation No. 26 year 2014)</h5>
					<h5 class="ui blue ribbon label">Accreditation</h5>
					<ul>
						<li><b>KAN Accreditation</b> No. LI-026-IDN (SNI ISO/IEC 17020.2012)</li>
						<li><b>DJK Accreditation</b> No. 5 StI/20/DJL.4/2016</li>
						<li><b>DJK Letter of Appointment</b> No. 219 K/20/DJL.4/2016</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ui vertical stripe segment" id="e-manajemen">
	<div class="ui middle aligned stackable grid container">
		<div class="row">
			<div class="eight wide column">
				<div class="ui green segment">
					<h3 class="ui green color header">Scope of Product Certification</h3>
					<h5 class="green text">Measurements Devices (SNI)</h5>
					<ul>
						<li>
							Electric, Magnetic, Electric and Magnetic Measurements
							<ul>
								<li>KWh Meter</li>
							</ul>
						</li>
					</ul>
					<h5 class="green text">Electrical Devices (SNI)</h5>
					<ul>
						<li>Power Cable
							<ul>
								<li>PVC Insulated Power Cable
									<ul>
										<li>Fixed Wiring Non Sheath Cable</li>
										<li>Fixed Wiring Sheath Cable</li>
										<li>Flexible Cable</li>
									</ul>
								</li>
								<li>Power Cable
									<ul>
										<li>Nominal Voltage 1 KV and 3 KV</li>
										<li>Nominal Voltage 6 KV and 30 KV</li>
									</ul>
								</li>
							</ul>
						</li>
						<li>Electrical Accessories
							<ul>
								<li>Plugs & Sockets</li>
								<li>Switches</li>
								<li>Miniature Circuit Breaker (MCB)</li>
								<li>Residual Current Circuit Breaker (RCCB)</li>
							</ul>
						</li>
					</ul>
					<h5 class="green text">Quality Control System for Eletrical Equipments used by PLN</h5>
					<h5 class="ui green ribbon label">Accreditation</h5>
					<ul>
						<li><b>KAN Accreditation</b> No. LSPr-005-IDN (ISO/IEC 17065.2012)</li>
					</ul>
				</div>
			</div>
			<div class="six wide right floated column">
				<img src="{{asset('img/white-image.png')}}" class="ui large bordered rounded image">
			</div>
		</div>
	</div>
</div>