@extends('layouts.front-secondary')

@section('content')
<div class="ui vertical masthead segment" style="height: 30vh">
</div>
<div class="ui vertical stripe segment" style="min-height: 70vh">
	<div class="ui top aligned stackable grid container">
		<div class="row">
			<div class="ui center aligned container">
				<h1>Tracking Data Kalibrasi</h1>
			</div>
		</div>
		<div class="row">
			<div class="six wide column">
				<div class="ui teal segment">
					<form class="ui form">
						<div class="field">
							<label for="">Cari data tracking</label>
							<div class="ui action input">
								<input type="text" placeholder="Masukkan nomor tracking...">
								<button class="ui teal button">Cari</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="ten wide column">
				<div class="ui teal top attached segment">
					<h5 class="ui header">Hasil Pencarian Tracking</h5>
				</div>
				<div class="ui bottom attached segment">
					<table class="ui celled table">
						<thead>
							<tr>
								<th>Name</th>
								<th>Status</th>
								<th>Notes</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>No Name Specified</td>
								<td>Unknown</td>
								<td class="negative">None</td>
							</tr>
							<tr class="positive">
								<td>Jimmy</td>
								<td><i class="icon checkmark"></i> Approved</td>
								<td>None</td>
							</tr>
							<tr>
								<td>Jamie</td>
								<td>Unknown</td>
								<td class="positive"><i class="icon close"></i> Requires call</td>
							</tr>
							<tr class="negative">
								<td>Jill</td>
								<td>Unknown</td>
								<td>None</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection