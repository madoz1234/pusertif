<div class="ui inverted vertical masthead segment">

	<div class="ui container">
		<div class="ui large secondary inverted pointing menu">
			<a class="toc item">
				<i class="sidebar icon"></i>
			</a>
			<a class="item" href="#about">Tentang Kami</a>
			<a class="item" href="#e-uji">E-Uji</a>
			<a class="item" href="#e-kal">E-Kal</a>
			<a class="item" href="#e-slo">E-SLO</a>
			<a class="item" href="#e-sertifikasi">E-Sertifikasi Produk</a>
			<a class="item" href="#e-manajemen">E-Sistem Manajemen</a>
			<a class="item" href="{{ url('tracking') }}">Tracking</a>
			<a class="item" href="{{ url('pengujian') }}">Testing</a>
			<a class="item" href="{{ url('pembayaran') }}">Payment</a>
			<a class="item" href="{{ url('list') }}">List Order</a>
			<div class="right item">
				<a href="{{ url('register') }}" class="ui inverted button">Register</a>
				<a href="{{ url('login') }}" class="ui inverted button">Sign In</a>
			</div>
		</div>
	</div>

	<div class="ui text container">
		<img src="{{ asset('img/logo-1.png') }}" alt="logo" height="200">
		<h1 class="ui inverted header">
			PT PLN (PERSERO)
		</h1>
		<h2>PUSAT SERTIFIKASI</h2>
	</div>
</div>