@extends('layouts.front')

@section('content')
@include('frontend.home')
@include('frontend.about')
@include('frontend.work-order')
@include('frontend.scope')
@include('frontend.certifications')
@endsection