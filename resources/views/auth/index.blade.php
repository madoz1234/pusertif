@extends('layouts.base-user')

@section('scripts')
<script type="text/javascript">
	//
</script>
@append
@section('content')

@section('content-header')
<div class="ui breadcrumb">
	<a href="#" class="section">Akun</a>
	<i class="right chevron icon divider"></i>
	<div class="active section">Ganti Password</div>
</div>
<h2 class="ui header">
	<div class="content">
		Ganti Password
		<div class="sub header">{{ $subtitle or ' ' }}</div>
	</div>
</h2>
@show
{{-- dorr --}}
<div class="ui centered grid">
	<div class="ten wide column">
		<div class="ui segments fluid">
			<div class="ui default top attached menu">
				<div class="header item">
					<i class="key icon"></i>  
					<div>
						<span>Ganti Password</span>
					</div>
				</div>
			</div>
			{{-- start message --}}
			@if ($message = Session::get('gagal'))
			<div class="ui icon message warning">
				<i class="close icon"></i>
				<i class="remove icon"></i>
				<div class="content">
					<div class="alert alert-warning center">
						<p>{{ $message }}</p>
					</div>
				</div>
			</div>
			@endif
			{{-- end message --}}
			{{-- start message --}}
			@if ($message = Session::get('success'))
			<div class="ui icon message success">
				<i class="close icon"></i>
				<i class="checkmark icon"></i>
				<div class="content">
					<div class="alert alert-success center">
						<p>{{ $message }}</p>
					</div>
				</div>
			</div>
			@endif
			{{-- end message --}}

			<form class="ui form" action="{{ url('/frontend/password-reset') }}" method="post" role="form">
				<div class="ui bottom segment attached">
					{{csrf_field()}}

					<div class="ui floating icon message">
						<i class="privacy icon"></i>
						<div class="content">
							<div class="header">
								Ubah Password
							</div>
							<p>Isi form di bawah ini untuk mengubah password.</p>
						</div>
					</div>

					<div class="field">
						<label for="password" class="col-md-4 control-label">Password Lama</label>

						<div class="col-md-6">
							<input id="password" type="password" class="form-control" name="oldPassword">
						</div>
					</div>

					<div class="field">
						<label for="password" class="col-md-4 control-label">Password Baru</label>

						<div class="col-md-6">
							<input id="password" type="password" class="form-control" name="newPassword">
						</div>
					</div>

					{{-- <div class="field">
						<label for="password-confirm" class="col-md-4 control-label">Konfirmasi Password</label>

						<div class="col-md-6">
							<input id="password-confirm" type="password" class="form-control" name="password_confirmation">
						</div>
					</div> --}}

					{{-- <div class="field">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="ui button primary">Submit</button>
						</div>
					</div> --}}
					<div class="actions">
						<a href="/frontend" class="ui red deny button">
							Batal
						</a>
						<button type="submit" class="ui positive right labeled icon save button">
							Simpan
							<i class="checkmark icon"></i>
						</button>
					</div>
				</div>
			</form>

		</div>
	</div>
</div>
{{-- dorr --}}
</div>
@endsection
