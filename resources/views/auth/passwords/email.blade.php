@extends('layouts.auth')

@section('content')
<div class="ui middle aligned center aligned grid">
    <div class="column">
        <form class="ui form" role="form" method="POST" action="{{ url('/password/email') }}">
            {!! csrf_field() !!}
            {{-- <div class="ui horizontal auth-form segments"> --}}
            <div class="ui auth-form grid">
                <div class="computer only eight wide middle aligned center aligned column">
                    <img src="{{ asset('img/icon.png') }}" alt="Logo" width="200">

                    <h2 class="title">www.pln.co.id</h2>
                </div>
                <div class="sixteen wide mobile eight wide computer column">

                  {{--   @if (session()->has('message'))
                    <div class="ui negative message">
                        <i class="close icon"></i>
                        <div class="header">
                            <strong>Mohon Maaf, </strong>Terjadi Kesalahan<br>
                        </div>
                        {{ session()->get('message') }}
                    </div>
                    @endif --}}
                    @if (count($errors) > 0)
                    <div class="ui negative message">
                        <i class="close icon"></i>
                        <div class="header">
                            <strong>Mohon Maaf, </strong>Terjadi Kesalahan<br>
                        </div>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (session('status'))
                    <div class="ui positive message">
                        <i class="close icon"></i>
                        <span>{{ session('status') }}</span>
                   </div>
                    @endif

                    <img src="{{ asset('img/icon-form.png') }}" alt="icon" width="120">
                    <h1 class="title no-margin">{{ config('app.name') }}</h1>

                    <div class="field">
                        <div class="ui left icon input">
                            {{-- <i class="user icon"></i> --}}
                            <img src="{{ asset('img/icon-user.png') }}" class="icon" alt="icon" width="100%">
                            {{-- <input id="username" type="username" class="form-control" name="username" value="{{ old('username') }}" placeholder="Username / Email" required autofocus> --}}
                             <input id="email" type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required>
                        </div>
                    </div>
                    {{-- <button type="submit" class="ui fluid large submit button">{{ __('Reset Password') }}</button> --}}
                    <button type="submit" class="ui fluid large blue submit button">{{ __('Send Password Reset Link') }}</button>
                    <a href="{{ url('login') }}"> Login </a>
                </div>

                <div class="center-border"></div>
            </div>
        </form>
    </div>
</div>
@endsection
