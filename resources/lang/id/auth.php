<?php
return [
    'failed'   => 'Identitas tersebut tidak cocok dengan data kami.',
    'notactivated' => 'Pengguna tidak dapat mengakses untuk saat ini.',
    'user_name'   => 'Username tersebut tidak cocok dengan data kami.',
    'password'   => 'Password Salah.',
    'throttle' => 'Terlalu banyak usaha masuk. Silahkan coba lagi dalam :seconds detik.',
];
