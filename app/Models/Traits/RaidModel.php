<?php

namespace App\Models\Traits;

use App\Models\Attachments;
use App\Models\Embed;
use App\Models\Files;

trait RaidModel
{
    public function scopeSort($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    public static function prepare($request, $identifier = 'id')
    {
        $record = new static;

        if ($request->has($identifier) && $request->get($identifier) != null && $request->get($identifier) != 0) {
            $record = static::find($request->get($identifier));
        }

        return $record;
    }

    public function postSave($request, $identifier = 'id')
    {
        # code didieu
    }

    public static function saveData($request, $identifier = 'id')
    {
        $record = static::prepare($request, $identifier);
        $record->fillData($request);
        $record->save();

        $record->postSave($request, $identifier);

        if($request->attachment)
        {
              $record->uploadAttachmentWithoutDelete($request->attachment);
        }



        return $record;
    }

    public function fillData($request)
    {
        $this->fill($request->all());
    }

    public static function getSorted()
    {
        return static::sort()->get();
    }

    public static function generateCode()
    {
        if (\Schema::hasColumn(with(new static )->getTable(), 'kode')) {
            $last = static::orderBy('kode', 'desc')->first();
            $kode = (!is_null($last) && $k = $last->kode) ? intval($last->kode) : 0;

            return str_pad($kode + 1, 5, "0", STR_PAD_LEFT);
        }

        return "";
    }

    public function uploadAttachmentWithoutDelete($attachments, $taken = null, $exist = null)
    {
        if(count($attachments) > 0)
        {
            foreach($attachments as $key => $attachment)
            {
                if($attachment != null)
                {

                  $data['filename'] = $attachment->getClientOriginalName();
                  $data['url'] = $attachment->storeAs($this->filesMorphClass(), md5($attachment->getClientOriginalName()).''.strtotime('now').''.$attachment->getClientOriginalExtension(), 'public');
                  $data['target_type'] = $this->filesMorphClass();
                  $data['target_id'] = $this->id;

                  if($taken != null){
                      if(count($taken) > 0)
                      {
                        $data['taken_at'] = $taken[$key];
                      }
                  }

                  $save = new Attachments;
                  $save->fill($data);
                  $save->save();
                }
            }
        }
    }
}
