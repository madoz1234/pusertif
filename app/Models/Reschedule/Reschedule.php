<?php
namespace App\Models\Reschedule;

use App\Models\Model;

use App\Models\Authentication\User;
use App\Models\Pengujian\Pengujian;
use App\Models\Reschedule\RescheduleDetail;
use App\Models\Reschedule\Reschedule;

class Reschedule extends Model
{
    /* default */
    protected $table 		= 'trans_reschedule';
    protected $fillable 	= ['pengujian_id','status'];

    protected $log_table    = 'log_trans_reschedule';
    protected $log_table_fk = 'ref_id';

    public function pengujian(){
        return $this->belongsTo(Pengujian::class, 'pengujian_id' , 'id');
    }

    public function detailreschedule(){
        return $this->hasMany(RescheduleDetail::class, 'reschedule_id' , 'id');
    }

    public function saveDetail($detail, $start, $end)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			$cari = RescheduleDetail::where('pengujian_detail_id', $detail[$key])->first();
    			if($cari){
    				$reschedule_detail 							= RescheduleDetail::find($cari->id);
		    		$reschedule_detail->pengujian_detail_id 	= $detail[$key];
		    		$reschedule_detail->tentative_start_old 	= $start[$key];
		    		$reschedule_detail->tentative_end_old 		= $end[$key];
		    		$reschedule_detail->tentative_start_new 	= NULL;
		    		$reschedule_detail->tentative_end_new 		= NULL;
		            $this->detailreschedule()->save($reschedule_detail);

    			}else{
		    		$reschedule_detail 							= new RescheduleDetail;
		    		$reschedule_detail->pengujian_detail_id 	= $detail[$key];
		    		$reschedule_detail->tentative_start_old 	= $start[$key];
		    		$reschedule_detail->tentative_end_old 		= $end[$key];
		    		// $reschedule_detail->tentative_start_new 	= $value['tentative_start'];
		    		// $reschedule_detail->tentative_end_new 		= $value['tentative_end'];
		            $this->detailreschedule()->save($reschedule_detail);
    			}

	            $pembanding = RescheduleDetail::whereNull('tentative_start_new')->get()->count();
	            if($pembanding > 0){
	            	if($cari){
			            $reschedule = Reschedule::find($cari->reschedule_id);
			            $reschedule->status = 0;
			            $reschedule->save();
	            	}
	            }
    		}
    	}
    }

    public function updateDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
	    		$data 										= RescheduleDetail::where('pengujian_detail_id', $value['detail_id'])->first();
	    		$reschedule_detail 							= RescheduleDetail::find($data->id);
	    		$reschedule_detail->tentative_start_old 	= $value['start'];
	    		$reschedule_detail->tentative_end_old 		= $value['end'];
	    		$reschedule_detail->tentative_start_new 	= $value['tentative_start'];
	    		$reschedule_detail->tentative_end_new 		= $value['tentative_end'];
	            $this->detailreschedule()->save($reschedule_detail);
    		}
    	}
    }
}
