<?php
namespace App\Models\Reschedule;

use App\Models\Model;

use App\Models\Authentication\User;
use App\Models\Reschedule\Reschedule;
use App\Models\Pengujian\PengujianDetail;

class RescheduleDetail extends Model
{
    /* default */
    protected $table 		= 'trans_reschedule_detail';
    protected $fillable 	= ['reschedule_id','pengujian_detail_id','tentative_start_old','tentative_end_old','tentative_start_new','tentative_end_new'];

    protected $log_table    = 'log_trans_reschedule_detail';
    protected $log_table_fk = 'ref_id';
    
    public function reschedule(){
        return $this->belongsTo(Reschedule::class, 'reschedule_id' , 'id');
    }

    public function detail_pengujian(){
        return $this->belongsTo(PengujianDetail::class, 'pengujian_detail_id', 'id');
    }
}
