<?php
namespace App\Models\FrontEnd;

use App\Models\Model;
use App\Models\Users;
use App\Models\Master\Survei;
// use App\Models\PenerimaanSurat\PenerimaanSurat;
use Carbon;

class TransSurvei extends Model
{
    /* default */
    protected $table 		= 'trans_survei';
    protected $fillable 	= [
        'pp_id',
        'survei_id',
        'sum',
    ];

    /* data ke log */
    protected $log_table    = 'log_trans_survei';
    protected $log_table_fk = 'ref_id';
    /* relation */
    
    public function pp(){
        return $this->belongsTo(PendaftaranPengujian::class, 'pp_id');
    }

    public function survei(){
        return $this->belongsTo(Survei::class, 'survei_id');
    }

    public static function getGrafik($id)
    {
    	$sample = TransSurvei::where('survei_id', $id)->get();
    	$data = array();
    	foreach ($sample as $key => $value) {
			$sums = ($value->sum / $value->survei->detail()->count());
			$data[0][$sums]= (int)round($sums);
    	}
    	return $data;
    }
}
