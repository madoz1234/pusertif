<?php
namespace App\Models\FrontEnd;

use App\Models\Model;
use App\Models\Users;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\Master\JenisPengujian;
use App\Models\Master\Satuan;
use App\Models\PenerimaanBarang\PenerimaanBarangDetail;
use Carbon;
// use App\Models\Attachments;

class PendaftaranPengujianDetail extends Model
{
    /* default */
    protected $table 		= 'trans_pp_detail';
    protected $fillable 	= [
        'pp_id',
        'jenis_id',
        'merk',
        'tipe',
        'jumlah',
        'satuan_id',
        'spesifikasi'
    ];

    protected $log_table    = 'log_trans_pp_detail';
    protected $log_table_fk = 'ref_id';

    public function pp(){
        return $this->belongsTo(PendaftaranPengujian::class, 'pp_id');
    }
    
    public function jenis(){
        return $this->belongsTo(JenisPengujian::class, 'jenis_id');
    }

    public function satuan(){
        return $this->belongsTo(Satuan::class, 'satuan_id');
    }

    public function detail_penerimaan(){
        return $this->hasMany(PenerimaanBarangDetail::class, 'detail_pp_id' , 'id');
    }

}
