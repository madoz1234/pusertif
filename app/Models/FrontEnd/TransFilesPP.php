<?php
namespace App\Models\FrontEnd;

use App\Models\Model;
use App\Models\FrontEnd\PendaftaranPengujian;
use Carbon;
use App\Models\Attachments;

class TransFilesPP extends Model
{
    /* default */
    protected $table 		= 'trans_files_pp';
    protected $fillable 	= [
        'pp_id',
        'bukti',
        'filename'
    ];
}
