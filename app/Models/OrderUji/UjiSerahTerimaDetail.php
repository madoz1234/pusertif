<?php
namespace App\Models\OrderUji;

use App\Models\Model;
use App\Models\Users;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\Master\JenisPengujian;
use App\Models\Master\Satuan;
use App\Models\PenerimaanBarang\PenerimaanBarangDetail;
use Carbon;
// use App\Models\Attachments;

class UjiSerahTerimaDetail extends Model
{
    /* default */
    protected $table 		= 'trans_uji_serah_terima_detail';
    protected $fillable 	= [
        'uji_id',
        'jenis_id',
        'merk',
        'tipe',
        'jumlah',
        'satuan_id',
        'spesifikasi',
        'tgl_mulai',
        'tgl_selesai',
    ];

    protected $log_table    = 'log_trans_uji_serah_terima_detail';
    protected $log_table_fk = 'ref_id';

    public function uji(){
        return $this->belongsTo(UjiSerahTerima::class, 'uji_id');
    }
    
    public function jenis(){
        return $this->belongsTo(JenisPengujian::class, 'jenis_id');
    }

    public function satuan(){
        return $this->belongsTo(Satuan::class, 'satuan_id');
    }

}
