<?php
namespace App\Models\OrderUji;

use App\Models\Act\ActDisposisi;
use App\Models\Attachments;
use App\Models\Files;
use App\Models\FrontEnd\Survei;
use App\Models\KajiUlang\KajiUlang;
use App\Models\Master\JenisPelayanan;
use App\Models\Master\Lingkup;
use App\Models\Master\Pelanggan;
use App\Models\Model;
use App\Models\Users;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\VirtualAccount\KonfirmasiPembayaran;
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\FrontEnd\PendaftaranPengujian;
use Carbon;
use Illuminate\Support\Facades\DB;

class UjiSerahTerima extends Model
{
    /* default */
    protected $table 		= 'trans_uji_serah_terima';
    protected $fillable 	= [
        'no_order',
        'tgl_order',
        'user_id',
        'layanan_id',
        'lingkup_id',
        'rencana',
        'kelas',
        'status',
        'no_kontrak',
        'tgl_kontrak',
        'no_wbs',
        'tipe',
        'nomor',
        'catatan'
    ];

    protected $appends = [
    	'last_dispo',
        'attachment_bukti',
    ];

    protected $log_table    = 'log_trans_uji_serah_terima';
    protected $log_table_fk = 'ref_id';

    /* relation */
    public function getLastDispoAttribute() 
    {
    	if($this->act_dispo)
    	{
    		return $this->act_dispo()->latest()->first();
    	}

    	return null;
    }

    public function ld(){
        return $this->hasOne(ActDisposisi::class, 'parent_id')->latest();
    }

    public function pelayanan(){
        return $this->belongsTo(JenisPelayanan::class, 'layanan_id');
    }

    public function lingkup(){
        return $this->belongsTo(Lingkup::class, 'lingkup_id');
    }

    public function survei(){
        return $this->hasOne(Survei::class, 'pp_id');
    }

    public function act_dispo(){
        return $this->hasMany(ActDisposisi::class, 'parent_id' , 'id');
    }

    public function kaji_ulang(){
        return $this->hasOne(KajiUlang::class, 'pp_id' , 'id');
    }
    
    public function pp()
    {
        return $this->belongsTo(PendaftaranPengujian::class, 'pp_id', 'id');
    }

    public static function getFormNumber()
    {
        $data = static::orderBy('created_at','desc')->first();
        if($data){
            $count = static::orderBy('created_at','desc')->first()->id + 1;
            if($count){
                $number = sprintf('%03d', $count);
            }else{
                $number = sprintf('%03d', 0);
            }
        }else{
            $number = sprintf('%03d', 0);
        }

        return ''.Carbon::now()->format('m').''.Carbon::now()->format('Y').''.$number;
    }
    
    public function filesMorphClass()
    {
        return 'uji-serah-terima';
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }

    public function user(){
        return $this->belongsTo(Users::class, 'user_id');
    }

    public function detail(){
        return $this->hasMany(UjiSerahTerimaDetail::class, 'uji_id' , 'id');
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }

    public function saveDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
	    		$detail = New UjiSerahTerimaDetail;
	    		$detail->jenis_id = $value['jenis_id'];
		        $detail->merk = $value['merk'];
		        $detail->tipe = $value['tipe'];
		        $detail->jumlah = $value['jumlah'];
		        $detail->satuan_id = $value['satuan_id'];
		        $detail->spesifikasi = $value['spesifikasi'];
                $detail->tgl_mulai = $value['tgl_mulai'];
                $detail->tgl_selesai = $value['tgl_selesai'];
	            $this->detail()->save($detail);
    		}
    	}
    }

    // public function virtual()
    // {
    //     return $this->belongsTo(VirtualAccount::class, 'pp_id', 'id');
    // }

    public function updateDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
	    		$detail = UjiSerahTerimaDetail::find($key);
	    		if(!is_null($detail)){
	    			$detail->jenis_id = $value['jenis_id'];
	    			$detail->merk = $value['merk'];
	    			$detail->tipe = $value['tipe'];
	    			$detail->jumlah = $value['jumlah'];
	    			$detail->satuan_id = $value['satuan_id'];
	    			$detail->spesifikasi = $value['spesifikasi'];
                    $detail->tgl_mulai = $value['tgl_mulai'];
                    $detail->tgl_selesai = $value['tgl_selesai'];
		            $this->detail()->save($detail);
	    		}else{
	    			$detail = New UjiSerahTerimaDetail();
		            $detail->jenis_id = $value['jenis_id'];
	    			$detail->merk = $value['merk'];
	    			$detail->tipe = $value['tipe'];
	    			$detail->jumlah = $value['jumlah'];
	    			$detail->satuan_id = $value['satuan_id'];
	    			$detail->spesifikasi = $value['spesifikasi'];
                    $detail->tgl_mulai = $value['tgl_mulai'];
                    $detail->tgl_selesai = $value['tgl_selesai'];
		            $this->detail()->save($detail);
	    		}
    		}
    	}
    }

    public static function generateNoOrder($id)
    {
    	$pelanggan = Pelanggan::where('user_id', $id)->first();
        $prefix = $pelanggan->perusahaan->kode. '' . Carbon::now()->format('Y') . '';
        $rcd = static::where('no_order', 'like', $prefix . '%')->orderBy('no_order', 'desc')->first();
        $nomor = 1;
        if ($rcd) {
            $nomor = (int) substr($rcd->no_order, -3);
            $nomor++;
        }

        return $prefix . str_pad($nomor, 3, "0", STR_PAD_LEFT);
    }

    public static function generateNomor()
    {
    	$prefix = Carbon::now()->format('Y') . '';
        $rcd = static::where('nomor', 'like', $prefix . '%')->orderBy('nomor', 'desc')->first();
        $no = 1;
        if($rcd) {
            $no = (int) substr($rcd->nomor, -5);
            $no++;
        }

        return $prefix . str_pad($no, 5, "0", STR_PAD_LEFT);
    }

    public function logs()
    {
    	return DB::table($this->log_table)->where($this->log_table_fk, $this->id)->where('status', 1)->get();
    }

    public function logsyan()
    {
    	return DB::table($this->log_table)->where($this->log_table_fk, $this->id)->where('status', 1)->get();
    }

    public function logsmsb()
    {
    	return DB::table($this->log_table)->where($this->log_table_fk, $this->id)->where('status', 2)->get();
    }

    public static function cekDone($id)
    {
    	$data 		= static::find($id);
        $cek 		= static::whereHas('kaji_ulang', function($u){
        						$u->where('keputusan', 1);
        				  	})
        				  	->where('nomor', $data->nomor)->get();
        $exist 		= SuratPenawaran::where('nomor', $data->nomor)->first();
        $cek_email 	= SuratPenawaran::where('nomor', $data->nomor)
        						  	->where('surat_status', 0)
        						  	->whereNotNull('wbs_io')
        						  	->get();
    	$surat 		= static::whereHas('kaji_ulang', function($z){
    							$z->doesntHave('surat')
    							  ->where('keputusan', 1);
    						 })->where('nomor', $data->nomor)->get();
    	$ada_surat  = static::whereHas('kaji_ulang', function($u){
    							$u->whereHas('surat', function($z){
    								$z->where('surat_status', 0);
    							})->where('keputusan', 1);
        				  	})->get();

        $jumlah 	= $cek->count();
        $total 		= 0;

        foreach ($cek as $key => $value) {
        	if($value->kaji_ulang){
        		$total += 1;
        	}else{
				$total += 0;
        	}
        }
        $cek_data['jumlah']	= $jumlah;
        $cek_data['total']	= $total;
        $cek_data['exist']	= $exist;
        $cek_data['nilai']	= $cek_email->count();
        $cek_data['surat']	= $surat->count();
        $cek_data['cek_jumlah']	= $ada_surat->count();
        
        return $cek_data;
    }

    public static function isDone()
    {
    	return static::whereHas('kaji_ulang', function($u){
    							$u->doesntHave('surat')->where('keputusan', 1);
        				  	})->orderBy('tgl_order','desc')->get()->groupBy(['nomor']);
    }

    public static function isKirim()
    {
    	return static::whereHas('kaji_ulang', function($u){
    							$u->whereHas('surat', function($z){
    								$z->where('surat_status', 0);
    							})->where('keputusan', 1);
        				  	})->orderBy('tgl_order','desc')->get()->groupBy(['nomor']);
    }

    public static function isHistori()
    {
    	return static::whereHas('kaji_ulang', function($u){
    							$u->whereHas('surat', function($z){
    								$z->where('surat_status', 1);
    						})->where('keputusan', 1);
    					})->orderBy('tgl_order','desc')->get()->groupBy(['nomor']);
    }

    public static function getPenerimaanSurat($bulan, $jenis)
    {	
       	$total = static::whereHas('detail', function($a) use ($jenis){
	   						$a->where('jenis_id', $jenis);
	   					})
       					->whereYear('tgl_order', '=', date('Y'))
	   					->whereMonth('tgl_order', '=', $bulan)
	   					->where('status', 1)
	   					->doesntHave('kaji_ulang')
	   					->get();
       	return $total->count();
    }

    public static function getKajiUlang($bulan, $jenis)
    {	
       	$total = static::whereYear('tgl_order', '=', date('Y'))
       					->whereMonth('tgl_order', '=', $bulan)
       					->where('status', 2)
       					->whereHas('detail', function($a) use($jenis){
	   						$a->where('jenis_id', $jenis);
	   					})
	        			->doesntHave('kaji_ulang')
       					->get();
       	return $total->count();
    }

    public static function getSuratPenawaran($bulan, $jenis)
    {	
       	$total = static::whereHas('kaji_ulang', function($u){
    						$u->doesntHave('surat')->where('keputusan', 1)
								->whereYear('created_at', '=', date('Y'))
								->whereMonth('created_at', '=', $bulan);
        				})
       					->whereHas('detail', function($a) use ($jenis){
	   						$a->where('jenis_id', $jenis);
	   					})
       					->get();
       	return $total->count();
    }

    public static function getKonfirmasi($bulan, $jenis)
    {	
       	$total = static::whereHas('kaji_ulang', function($u){
    						$u->whereHas('surat', function($z){
    							$z->whereHas('va', function($y){
    								$y->where('status', 1)
                                	  ->doesnthave('konfirmasi')
                                	  ->whereYear('tgl_aktif', '=', date('Y'))
									  ->whereMonth('tgl_aktif', '=', $bulan);
    							});
    						});
        				})
       					->whereHas('detail', function($a) use ($jenis){
	   						$a->where('jenis_id', $jenis);
	   					})
       					->get();
       	return $total->count();
    }
    
    public function getAttachmentBuktiAttribute()
    {
        return url('storage/'.$this->bukti);
    }
}
