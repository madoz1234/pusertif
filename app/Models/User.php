<?php

namespace App\Models;

use App\Models\Traits\Utilities;
use App\Models\Authentication\Role;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Models\Master\Pegawai;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Models\Master\Pelanggan;
use App\Models\UsersDetail;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, Utilities;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'username ', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = ['last_activity'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = 'sys_users';
    // protected $primaryKey = 'id';

    public function pelanggan()
    {
        return $this->hasOne(Pelanggan::class, 'user_id');
    }
    public function detail()
    {
        return $this->belongsTo(UsersDetail::class, 'user_id');
    }
}
