<?php
namespace App\Models;

use App\Models\Model;
use App\Models\Master\JenisPelayanan;
use App\Models\User;

class UsersDetail extends Model
{
    /* default */
    protected $table 		= 'ref_detail_user';
    protected $fillable 	= ['user_id','jenis_pelayanan_id','nama_lengkap','nip','jabatan','alamat','no_tlp'];

    /* Relation */
    public function pelayanan()
    {
        return $this->belongsTo(JenisPelayanan::class, 'jenis_pelayanan_id');
    }
    public function user()
    {
    	return $this->hasOne(User::class, 'user_id');
    }

}
