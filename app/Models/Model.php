<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Base;
use App\Models\Traits\EntryBy;
use App\Models\Traits\RaidModel;
use App\Models\Traits\Utilities;
use App\Models\Files;
use App\Models\Picture;
use App\Models\DaftarHadir;

use Carbon\Carbon;

class Model extends Base
{
    use RaidModel, Utilities, EntryBy;

    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray($append=true)
    {
    	if(!$append){
	        return $this->attributes;
    	}
        return array_merge($this->attributesToArray(), $this->relationsToArray());
    }

    public static function getLastId()
    {
        return Static::orderBy('id', 'DESC')->first()->id + 1;
    }

    public function versionUpdate($files)
    {
        if($files[0])
        {
            if(isset($this->versi))
            {
                $this->versi = $this->versi + 1;
                $this->save();
            }
        }
    }

     public function RevisiUpdate($files)
    {
        if($files[0])
        {
            if(isset($this->revisi))
            {
                $this->revisi = $this->revisi + 1;
                $this->save();
            }
        }
    }

    public function multipleFileUpload($files, $taken = null)
    {
        if($files)
        {
            foreach($files as $key => $file)
            {
                if($file != null)
                {
                  if($this->files->count() > 0)
                  {
                      foreach($this->files as $fileExist)
                      {
                        if(file_exists(public_path('storage/'.$fileExist->url)))
                        {
                            unlink(public_path('storage/'.$fileExist->url));
                        }
                        $fileExist->delete();
                      }
                  }

                  $data['filename'] = $file->getClientOriginalName();
                  $data['url'] = $file->store($this->filesMorphClass(), 'public');
                  $data['target_type'] = $this->filesMorphClass();
                  $data['target_id'] = $this->id;
                  // dd('taken',$taken,'key',$key);
                  if(isset($taken))
                  {
                    foreach ($this->taken as $Takenvalue) {
                       $data['taken_at'] = $Takenvalue;
                    }
                  }

                  $files = new Files;
                  $files->fill($data);
                  $files->save();
                }
            }
        }
    }

    public function multipleFileUploadNotulen($files, $taken = null)
    {
        if($files)
        {
            foreach($files as $key => $file)
            {
                if($file != null)
                {
                  if($this->files->count() > 0)
                  {
                      foreach($this->files as $fileExist)
                      {
                        if(file_exists(public_path('storage/'.$fileExist->url)))
                        {
                            unlink(public_path('storage/'.$fileExist->url));
                        }
                        $fileExist->delete();
                      }
                  }

                  $data['filename'] = $file->getClientOriginalName();
                  $data['url'] = $file->store($this->filesMorphClass(), 'public');
                  $data['target_type'] = $this->filesMorphClass();
                  $data['target_id'] = $this->id;
                  $data['type'] = 2;
                  // dd('taken',$taken,'key',$key);
                  if(isset($taken))
                  {
                    foreach ($this->taken as $Takenvalue) {
                       $data['taken_at'] = $Takenvalue;
                    }
                  }

                  $files = new Files;
                  $files->fill($data);
                  $files->save();
                }
            }
        }
    }

    public function multipleFileUploadFinal($files, $taken = null)
    {
        if($files)
        {
            foreach($files as $key => $file)
            {
                if($file != null)
                {
                  if($this->files->count() > 0)
                  {
                      foreach($this->files as $fileExist)
                      {
                        if(file_exists(public_path('storage/'.$fileExist->url)))
                        {
                            unlink(public_path('storage/'.$fileExist->url));
                        }
                        $fileExist->delete();
                      }
                  }

                  $data['filename'] = $file->getClientOriginalName();
                  $data['url'] = $file->store($this->filesMorphClass(), 'public');
                  $data['target_type'] = $this->filesMorphClass();
                  $data['target_id'] = $this->id;
                  $data['type'] = 2;
                  // dd('taken',$taken,'key',$key);
                  if(isset($taken))
                  {
                    foreach ($this->taken as $Takenvalue) {
                       $data['taken_at'] = $Takenvalue;
                    }
                  }

                  $files = new Files;
                  $files->fill($data);
                  $files->save();
                }
            }
        }
    }

    public function multipleFileUploadLaporan($files, $taken = null)
    {
        if($files)
        {
            foreach($files as $key => $file)
            {
                if($file != null)
                {
                  if($this->files->count() > 0)
                  {
                      foreach($this->files as $fileExist)
                      {
                        if(file_exists(public_path('storage/'.$fileExist->url)))
                        {
                            unlink(public_path('storage/'.$fileExist->url));
                        }
                        $fileExist->delete();
                      }
                  }

                  $data['filename'] = $file->getClientOriginalName();
                  $data['url'] = $file->store($this->filesMorphClass(), 'public');
                  $data['target_type'] = $this->filesMorphClass();
                  $data['target_id'] = $this->id;
                  $data['type'] = 2;
                  // dd('taken',$taken,'key',$key);
                  if(isset($taken))
                  {
                    foreach ($this->taken as $Takenvalue) {
                       $data['taken_at'] = $Takenvalue;
                    }
                  }

                  $files = new Files;
                  $files->fill($data);
                  $files->save();
                }
            }
        }
    }

    public function multipleFileUploadJadwal($files, $taken = null)
    {
        if($files)
        {
            foreach($files as $key => $file)
            {
                if($file != null)
                {
                  if($this->files->count() > 0)
                  {
                      foreach($this->files as $fileExist)
                      {
                        if(file_exists(public_path('storage/'.$fileExist->url)))
                        {
                            unlink(public_path('storage/'.$fileExist->url));
                        }
                        $fileExist->delete();
                      }
                  }

                  $data['filename'] = $file->getClientOriginalName();
                  $data['url'] = $file->store($this->filesMorphClass(), 'public');
                  $data['target_type'] = $this->filesMorphClass();
                  $data['target_id'] = $this->id;
                  $data['type'] = 1;
                  if(isset($taken))
                  {
                    foreach ($this->taken as $Takenvalue) {
                       $data['taken_at'] = $Takenvalue;
                    }
                  }

                  $files = new Files;
                  $files->fill($data);
                  $files->save();
                }
            }
        }
    }

    public function multipleUpload($files, $taken = null)
    {
        if($files)
        {
            foreach($files as $key => $file)
            {
                if($file != null)
                {
                  $data['filename'] = $file->getClientOriginalName();
                  $data['url'] = $file->store($this->filesMorphClass(), 'public');
                  $data['target_type'] = $this->filesMorphClass();
                  $data['target_id'] = $this->id;
                  if(isset($taken))
                  {
                    foreach ($this->taken as $Takenvalue) {
                       $data['taken_at'] = $Takenvalue;
                    }
                  }
                  $files = new Files;
                  $files->fill($data);
                  $files->save();
                }
            }
        }
    }

    public function multiplePictureUpload($files, $taken = null)
    {
      // dump('asdas');
        if($files)
        {
            foreach($files as $key => $file)
            {
                if($file != null)
                {
                  if($this->picture->count() > 0)
                  {
                      foreach($this->picture as $fileExist)
                      {
                        if(file_exists(public_path('storage/'.$fileExist->url)))
                        {
                            unlink(public_path('storage/'.$fileExist->url));
                        }
                        $fileExist->delete();
                      }
                  }
                  $data['filename'] = $file->getClientOriginalName();
                  $data['url'] = $file->store($this->filesMorphClass(), 'public');
                  $data['target_type'] = $this->filesMorphClass();
                  $data['target_id'] = $this->id;
                  if(count($taken) > 0)
                  {
                    foreach ($taken as $Takenvalue) {
                       $data['taken_at'] = $Takenvalue;
                    }
                  }

                  $files = new Picture;
                  $files->fill($data);
                  $files->save();
                }
            }
        }
    }

    public function multipleDaftarHadirUpload($files, $taken = null)
    {
        if($files)
        {
            foreach($files as $key => $file)
            {
                if($file != null)
                {
                  if($this->daftarHadir->count() > 0)
                  {
                      foreach($this->daftarHadir as $fileExist)
                      {
                        if(file_exists(public_path('storage/'.$fileExist->url)))
                        {
                            unlink(public_path('storage/'.$fileExist->url));
                        }
                        $fileExist->delete();
                      }
                  }
                  $data['filename'] = $file->getClientOriginalName();
                  $data['url'] = $file->store($this->filesMorphClass(), 'public');
                  $data['target_type'] = $this->filesMorphClass();
                  $data['target_id'] = $this->id;
                  if(count($taken) > 0)
                  {
                    foreach ($taken as $Takenvalue) {
                       $data['taken_at'] = $Takenvalue;
                    }
                  }

                  $files = new DaftarHadir;
                  $files->fill($data);
                  $files->save();
                }
            }
        }
    }

    public function multipleFileUploadWithoutDelete($files, $taken = null)
    {
        if($files)
        {
            foreach($files as $key => $file)
            {
                if($file != null)
                {
                  $save = new Files;
                  $save->filename = $file->getClientOriginalName();
                  $save->url = $file->store($this->filesMorphClass(), 'public');
                  $save->target_type = $this->filesMorphClass();
                  $save->target_id = $this->id;
                  if(count($taken) > 0)
                  {
                    foreach ($taken as $Takenvalue) {
                       $save->taken_at = $Takenvalue;
                    }
                  }
                  // if($taken[$key])
                  // {
                  //    $save->taken_at = $taken[$key];
                  // }
                  $save->save();
                }
            }
        }
    }

    public function multipleDaftarHadirUploadWithoutDelete($files, $taken = null)
    {
        if($files)
        {
            foreach($files as $key => $file)
            {
                if($file != null)
                {
                  $save = new DaftarHadir;
                  $save->filename = $file->getClientOriginalName();
                  $save->url = $file->store($this->filesMorphClass(), 'public');
                  $save->target_type = $this->filesMorphClass();
                  $save->target_id = $this->id;
                  if(count($taken) > 0)
                  {
                    foreach ($taken as $Takenvalue) {
                       $save->taken_at = $Takenvalue;
                    }
                  }
                  $save->save();
                }
            }
        }
    }

    public function multiplePictureUploadWithoutDelete($files, $taken = null)
    {
        if($files)
        {
            foreach($files as $key => $file)
            {
                if($file != null)
                {
                  $save = new Picture;
                  $save->filename = $file->getClientOriginalName();
                  $save->url = $file->store($this->filesMorphClass(), 'public');
                  $save->target_type = $this->filesMorphClass();
                  $save->target_id = $this->id;
                   if(count($taken) > 0)
                  {
                    foreach ($taken as $Takenvalue) {
                       $save->taken_at = $Takenvalue;
                    }
                  }
                  $save->save();
                }
            }
        }
    }

    public function uploadPicture($gambar)
    {
        if($gambar)
        {
            if($this->picture != null)
            {
                if(file_exists(public_path('storage/'.$this->picture)))
                {
                    unlink(public_path('storage/'.$this->picture));
                }
                $this->picture = null;
                $this->save();
            }
            $this->picture = $gambar->store($this->picturePath(), 'public');
            $this->save();
        }
    }

    public function fileDelete()
    {
        if($this->files)
        {
            foreach($this->files as $file)
            {
                if(file_exists(public_path('storage/'.$file->url)))
                {
                    unlink(public_path('storage/'.$file->url));
                }
                $file->delete();
            }
        }
    }

    public function pictureDelete()
    {
        if($this->picture->count() > 0)
        {
            foreach($this->picture as $pict)
            {
                if(file_exists(public_path('storage/'.$pict->url)))
                {
                    unlink(public_path('storage/'.$pict->url));
                }
                $pict->delete();
            }
        }
    }
}
