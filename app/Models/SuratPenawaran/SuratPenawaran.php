<?php

namespace App\Models\SuratPenawaran;

use App\Models\Model;
use App\Models\Attachments;
use App\Models\Files;
use App\Models\KajiUlang\KajiUlang;
use App\Models\SuratPenawaran\SuratPenawaranDetail;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\VirtualAccount\PengembalianDana;
use App\Models\Authentication\User;
use Carbon;
use Illuminate\Support\Facades\DB;


class SuratPenawaran extends Model
{
      /* default */
    protected $table 		= 'trans_surat_penawaran';
    protected $fillable 	= [
        'kaji_id',
        'no_surat',
        'tgl_surat',
        'jadwal_start',
        'jadwal_end',
        'adendum_status',
        'adendum',
        'surat_status',
        'wbs_io',
        'nomor',
        'total',
        'pph',
        'no_skki',
        'tgl_skki',
    ];

    /* data ke log */
    protected $log_table    = 'log_trans_surat_penawaran';
    protected $log_table_fk = 'ref_id';
    

    /* relation */
    public function filesMorphClass()
    {
        return 'surat-penawaran';
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }

    public function kaji_ulang(){
        return $this->belongsTo(KajiUlang::class, 'kaji_id');
    }

    public function detail(){
        return $this->hasMany(SuratPenawaranDetail::class, 'surat_id' , 'id');
    }

    public function va(){
        return $this->hasOne(VirtualAccount::class, 'surat_id' , 'id');
    }

    public function pengembalian_dana()
    {
        return $this->hasOne(PengembalianDana::class, 'surat_id', 'id');
    }

    public function penerima(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function saveDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
	    		$detail = new SuratPenawaranDetail;
	    		$detail->komponen = $value['item'];
		        $detail->nominal = decimal_for_save($value['nominal']);
	            $this->detail()->save($detail);
    		}
    	}
    }
}
