<?php

namespace App\Models\SuratPenawaran;

use App\Models\Model;
use App\Models\SuratPenawaran\SuratPenawaran;


class SuratPenawaranDetail extends Model
{
      /* default */
    protected $table 		= 'trans_surat_penawaran_detail';
    protected $fillable 	= [
        'surat_id',
        'komponen',
        'nominal',
    ];

    /* data ke log */
    protected $log_table    = 'log_trans_surat_penawaran_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function surat(){
        return $this->belongsTo(SuratPenawaran::class, 'surat_id');
    }
    // public function kaji_ulang()
    // {
    //     return $this->belongsTo(KajiUlang::class, 'pp_id');
    // }
}
