<?php
namespace App\Models\KajiUlang;

use App\Models\Model;
use App\Models\KajiUlang\DetailMataUji;
use App\Models\KajiUlang\DetailLokasi;
use App\Models\Master\JenisPengujian;
use App\Models\FrontEnd\PendaftaranPengujianDetail;
/* Init Model Master */

class Detail extends Model
{
    /* default */
    protected $table 		= 'trans_kaji_ulang_detail';
    protected $fillable 	= ['kaji_ulang_id','jenis_id','jenis_lokasi','ket_lokasi','spesifikasi','tarif','jumlah','tentative_start','tentative_end','reschedule_start','reschedule_end','realisasi_start','realisasi_end','laporan_start','laporan_end'];
    // protected $append 		= ['total'];

    /* data ke log */
    protected $log_table    = 'log_trans_kaji_ulang_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function detail_pendaftaran(){
        return $this->belongsTo(PendaftaranPengujianDetail::class, 'jenis_id' , 'id');
    }

    public function kajiulang(){
        return $this->belongsTo(KajiUlang::class, 'kaji_ulang_id' , 'id');
    }

    public function mata_uji(){
        return $this->hasMany(DetailMataUji::class, 'detail_id' , 'id');
    }

    public function lokasi(){
        return $this->hasMany(DetailLokasi::class, 'detail_id' , 'id');
    }


    public function saveMataUji($mata_uji)
    {
    	if($mata_uji)
    	{
    		foreach ($mata_uji as $key => $value) {
	    		$data = new DetailMataUji;
	    		$data->mata_uji = $value['data'];
	            $this->mata_uji()->save($data);
    		}
    	}
    }

    public function saveLokasi($lokasi)
    {
    	if($lokasi)
    	{
    		foreach ($lokasi as $kiy => $val) {
	    		$data_lokasi 					= new DetailLokasi;
	    		$data_lokasi->jenis_lokasi 		= $val['lokasi'];
	    		$data_lokasi->ket_lokasi 		= $val['ket_lokasi'];
	            $this->lokasi()->save($data_lokasi);
    		}
    	}
    }
    /* scope */
    // insert code here


    /* custom function */
    // insert code here

}
