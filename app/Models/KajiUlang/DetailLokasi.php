<?php
namespace App\Models\KajiUlang;

use App\Models\Model;
/* Init Model Master */

class DetailLokasi extends Model
{
    /* default */
    protected $table 		= 'trans_kaji_ulang_detail_lokasi';
    protected $fillable 	= ['detail_id','jenis_lokasi','ket_lokasi'];
    // protected $append 		= ['total'];

    /* data ke log */
    protected $log_table    = 'log_trans_kaji_ulang_detail_lokasi';
    protected $log_table_fk = 'ref_id';
    /* relation */
    /* scope */
    // insert code here


    /* custom function */
    // insert code here

}
