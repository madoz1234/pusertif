<?php
namespace App\Models\KajiUlang;

use App\Models\Model;
/* Init Model Master */

class DetailMataUji extends Model
{
    /* default */
    protected $table 		= 'trans_kaji_ulang_detail_mata_uji';
    protected $fillable 	= ['detail_id','mata_uji'];
    // protected $append 		= ['total'];

    /* data ke log */
    protected $log_table    = 'log_trans_kaji_ulang_detail_mata_uji';
    protected $log_table_fk = 'ref_id';
    /* relation */
    /* scope */
    // insert code here


    /* custom function */
    // insert code here

}
