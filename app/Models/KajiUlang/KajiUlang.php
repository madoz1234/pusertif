<?php
namespace App\Models\KajiUlang;

use App\Models\Attachments;
use App\Models\Files;
use App\Models\Model;
use App\Models\KajiUlang\Detail;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\SuratPenawaran\SuratPenawaran;
use Carbon;
use Illuminate\Support\Facades\DB;

/* Init Model Master */

class KajiUlang extends Model
{
    /* default */
    protected $table 		= 'trans_kaji_ulang';
    protected $fillable 	= ['pp_id','keputusan'];

    /* data ke log */
    protected $appends = [
        'attachment_tolak',
    ];

    protected $log_table    = 'log_trans_kaji_ulang';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function detail(){
        return $this->hasMany(Detail::class, 'kaji_ulang_id' , 'id');
    }

    public function saveDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
	    		$detail = new Detail;
	    		$detail->jenis_id 			= $value['jenis_id'];
	    		// $detail->jenis_lokasi   	= $value['lokasi'];
	    		// $detail->ket_lokasi   		= $value['ket_lokasi'];
	    		$detail->spesifikasi   		= $value['spesifikasi'];
	    		$detail->tarif   			= $value['tarif'];
	    		$detail->jumlah   			= decimal_for_save($value['jumlah']);
	    		$detail->tentative_start   	= $value['tentative_start'];
	    		$detail->tentative_end   	= $value['tentative_end'];
	    		// $detail->laporan_start   	= $value['tgl_laporan_start'];
	    		// $detail->laporan_end   		= $value['tgl_laporan_end'];
	            $this->detail()->save($detail);
	            if($value['mata_uji'])
	            {
				    $detail->saveMataUji($value['mata_uji']);
	            }
	            if($value['data'])
	            {
				    $detail->saveLokasi($value['data']);
	            }
    		}
    	}
    }

    public function saveDetailSerahTerima($detail,$pp_id)
    {
    	if($detail)
    	{
    		$pp = PendaftaranPengujian::find($pp_id);
    		$i=1;
    		foreach ($pp->detail as $key => $value) {
    			# code...
	    		$data = new Detail;
	    		$data->jenis_id 			= $value->id;
	    		$data->spesifikasi   		= $detail[$i]['spesifikasi'];
	    		$data->tarif   				= '-';
	    		$data->jumlah   			= $detail[$i]['jumlah'];
	    		$data->tentative_start   	= $detail[$i]['tgl_mulai'];
	    		$data->tentative_end   		= $detail[$i]['tgl_selesai'];
		        $i++;
	            $this->detail()->save($data);
    		}
    	}
    }
    public function pp()
    {
        return $this->belongsTo(PendaftaranPengujian::class, 'pp_id', 'id');
    }

    public function surat()
    {
        return $this->hasOne(SuratPenawaran::class, 'kaji_id', 'id');
    }

    public function filesMorphClass()
    {
        return 'briefing-teknis';
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }
    public function getAttachmentTolakAttribute()
    {
        return url('storage/'.$this->bukti);
    }

}
