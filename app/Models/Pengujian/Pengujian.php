<?php
namespace App\Models\Pengujian;

use App\Models\Model;

use App\Models\Attachments;
use App\Models\Files;
use App\Models\Authentication\User;
use App\Models\VirtualAccount\KonfirmasiPembayaran;
use App\Models\PenerimaanBarang\PenerimaanBarang;
use App\Models\PenerimaanBarang\PenerimaanBarangDetail;
use App\Models\Pengujian\PengujianDetail;
use App\Models\Reschedule\Reschedule;
use App\Models\LaporanPengujian\LaporanPengujian;
use App\Models\Act\ActDisposisi;
use App\Models\Act\ActPengujian;
use Carbon;
use OneSignal;

class Pengujian extends Model
{
    /* default */
    protected $table 		= 'trans_pengujian';
    protected $fillable 	= ['penerimaan_id','status','status_dispo','status_data','fpp_fpk'];

    protected $log_table    = 'log_trans_pengujian';
    protected $log_table_fk = 'ref_id';

    public function penerimaan(){
        return $this->belongsTo(PenerimaanBarang::class, 'penerimaan_id' , 'id');
    }

    public function reschedule(){
        return $this->hasOne(Reschedule::class, 'pengujian_id');
    }

    public function laporan_pengujian(){
        return $this->hasMany(LaporanPengujian::class, 'pengujian_id' , 'id');
    }

    public function detailpengujian(){
        return $this->hasMany(PengujianDetail::class, 'pengujian_id' , 'id');
    }

    public function act_dispo(){
        return $this->hasMany(ActDisposisi::class, 'parent_id' , 'id');
    }

    public function pengirim(){
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function saveDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			$penerimaan_barang = PenerimaanBarangDetail::find($key);
	    		$detail_pengujian 							= new PengujianDetail;
	    		$detail_pengujian->detail_penerimaan_id 	= $key;
	    		$detail_pengujian->pelaksana_id 			= $value['pelaksana_id'];
	    		$detail_pengujian->group_id 				= $value['group_id'];
	    		if($penerimaan_barang->flag == 1){
		    		$detail_pengujian->flag 				= 1;
		    	}
	            $this->detailpengujian()->save($detail_pengujian);
    		}
    	}
    }

    public function saveDetailUji($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			$cari =PenerimaanBarangDetail::where('group_id', $value['group_id'])->get();
    			foreach ($cari as $key => $val) {
    				$penerimaan_barang = PenerimaanBarangDetail::find($val->id);
		    		$detail_pengujian 							= new PengujianDetail;
		    		$detail_pengujian->detail_penerimaan_id 	= $val->id;
		    		$detail_pengujian->pelaksana_id 			= $value['pelaksana_id'];
		    		$detail_pengujian->group_id 				= $value['group_id'];
		    		if($penerimaan_barang->flag == 1){
		    			$detail_pengujian->flag 				= 1;
		    		}
		            $this->detailpengujian()->save($detail_pengujian);
    			}
    		}
    	}
    }

    public function updateDetail($detail)
    {
    	$user = auth()->user();
    	if($detail)
    	{	
    		$id 				= array();
    		$data 				= array();
    		$tentative_start 	= array();
    		$tentative_end 		= array();

    		foreach($detail as $key => $value){
	    		$detail_pengujian 				= PengujianDetail::find($value['detail_id']);
	    		$cari = ActDisposisi::where('parent_id', $detail_pengujian->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->id)->orderBy('created_at', 'desc')->first();

	    		$data_user = User::find($cari->pengirim_id);

	    		$detail_pengujian->verifikasi 	= $value['verifikasi'];
	    		$detail_pengujian->status 		= 2;
	    		$detail_pengujian->tgl_selesai 	= Carbon::parse(date('Y-m-d'));
	    		$keterangan = '<a>'.$user->nama.'</a> memverifikasi No Seri : <a>'.$detail_pengujian->detail_penerimaan_barang->no_seri.'-'.$detail_pengujian->detail_penerimaan_barang->urutan.'</a> dengan hasil <a>Diterima</a>';

	    		if($value['verifikasi'] == 2){
	    			$data[]				= $detail_pengujian->id;
	    			$tentative_start[]	= $detail_pengujian->detailpenerimaan->tentative_start;
	    			$tentative_end[]	= $detail_pengujian->detailpenerimaan->tentative_end;
	    			$id[]				= $detail_pengujian->pengujian->id;
	    			$keterangan 		= '<a>'.$user->nama.'</a> memverifikasi No Seri : <a>'.$detail_pengujian->detail_penerimaan_barang->no_seri.'-'.$detail_pengujian->detail_penerimaan_barang->urutan.'</a> dengan hasil <a>Ditolak</a>';
	    		}

				$act = new ActPengujian;
				$act->parent_id 		= $detail_pengujian->id;
				$act->pengirim_id 		= $user->id;
				$act->penerima_id 		= $cari->pengirim_id;
				$act->tipe 				= 2;
				$act->jenis 			= 1;
				$act->keterangan 		= $keterangan;
				$act->save();

	            $this->detailpengujian()->save($detail_pengujian);
    		}
    		$result = array_unique($id);
    		if(isset($result[0])){
    			$cari_reschedule = Reschedule::where('pengujian_id', $result[0])->first();
    			if($cari_reschedule){
		    		$reschedule 				= Reschedule::find($cari_reschedule->id);
			    	$reschedule->saveDetail($data, $tentative_start, $tentative_end);

                    if($reschedule->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==1){
                        $notif = User::whereHas('roles',function($role){
                                    $role->where('name','asman-dal-kalibrasi')
                                            ->orWhere('name','asman-lola-kalibrasi');
                                })->get();
                    }
                    if($reschedule->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==2){
                        $notif = User::whereHas('roles',function($role){
                                    $role->where('name','asman-dal-siskit')
                                            ->orWhere('name','asman-lola-siskit');
                                })->get();
                    }
                    if($reschedule->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==3){
                        $notif = User::whereHas('roles',function($role){
                                    $role->where('name','asman-dal-sistgi')
                                            ->orWhere('name','asman-lola-sistgi');
                                })->get();
                    }
                    if($reschedule->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==4){
                        $notif = User::whereHas('roles',function($role){
                                    $role->where('name','asman-dal-tegangan-rendah')
                                            ->orWhere('name','asman-lola-tegangan-rendah');
                                })->get();
                    }
                    if($reschedule->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==5){
                        $notif = User::whereHas('roles',function($role){
                                    $role->where('name','asman-dal-tegangan-tinggi')
                                            ->orWhere('name','asman-lola-tegangan-tinggi');
                                })->get();   
                    }

                    foreach ($notif as $not) {
                        if(!is_null($not->device_id)){
                            $kirim = $reschedule;
                            $kirim['menu'] = 'reschedule';
                            $kirim['tab'] = 'onprogress';
                            OneSignal::sendNotificationToUser(
                                'Reschedule Baru dengan nomor order '.$reschedule->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,
                                $not->device_id,
                                $url = null,
                                $data = $kirim->toArray(),
                                $buttons = null,
                                $schedule = null
                            );
                        }
                    }


    			}else{
    				$reschedule 				= new Reschedule;
			    	$reschedule->pengujian_id 	= $result[0];
			    	$reschedule->save();
			    	$reschedule->saveDetail($data, $tentative_start, $tentative_end);

                    if($reschedule->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==1){
                        $notif = User::whereHas('roles',function($role){
                                    $role->where('name','asman-dal-kalibrasi')
                                            ->orWhere('name','asman-lola-kalibrasi');
                                })->get();
                    }
                    if($reschedule->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==2){
                        $notif = User::whereHas('roles',function($role){
                                    $role->where('name','asman-dal-siskit')
                                            ->orWhere('name','asman-lola-siskit');
                                })->get();
                    }
                    if($reschedule->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==3){
                        $notif = User::whereHas('roles',function($role){
                                    $role->where('name','asman-dal-sistgi')
                                            ->orWhere('name','asman-lola-sistgi');
                                })->get();
                    }
                    if($reschedule->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==4){
                        $notif = User::whereHas('roles',function($role){
                                    $role->where('name','asman-dal-tegangan-rendah')
                                            ->orWhere('name','asman-lola-tegangan-rendah');
                                })->get();
                    }
                    if($reschedule->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==5){
                        $notif = User::whereHas('roles',function($role){
                                    $role->where('name','asman-dal-tegangan-tinggi')
                                            ->orWhere('name','asman-lola-tegangan-tinggi');
                                })->get();   
                    }

                    foreach ($notif as $not) {
                        if(!is_null($not->device_id)){
                            $kirim = $reschedule;
                            $kirim['menu'] = 'reschedule';
                            $kirim['tab'] = 'onprogress';
                            OneSignal::sendNotificationToUser(
                                'Reschedule Baru dengan nomor order '.$reschedule->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,
                                $not->device_id,
                                $url = null,
                                $data = $kirim->toArray(),
                                $buttons = null,
                                $schedule = null
                            );
                        }
                    }
    			}
    		}
    	}
    }


    public function updateDetailUji($detail)
    {
    	$user = auth()->user();
    	if($detail)
    	{	
    		$id 				= array();
    		$data 				= array();
    		$tentative_start 	= array();
    		$tentative_end 		= array();
    		foreach ($detail as $key => $vil) {
    			$cek 				= PengujianDetail::where('group_id', $vil['group_id'])->get();
	    		foreach($cek->where('flag', 0) as $key => $value){
		    		$detail_pengujian 				= PengujianDetail::find($value->id);
		    		$cari = ActDisposisi::where('parent_id', $detail_pengujian->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->id)->orderBy('created_at', 'desc')->first();

		    		$data_user = User::find($cari->pengirim_id);

		    		$detail_pengujian->verifikasi 	= $vil['verifikasi'];
		    		$detail_pengujian->status 		= 2;
		    		$detail_pengujian->tgl_selesai 	= Carbon::parse(date('Y-m-d'));
		    		$keterangan = '<a>'.$user->nama.'</a> memverifikasi No Seri : <a>'.$detail_pengujian->detail_penerimaan_barang->no_seri.'-'.$detail_pengujian->detail_penerimaan_barang->urutan.'</a> dengan hasil <a>Diterima</a>';

		    		if($vil['verifikasi'] == 2){
		    			$data[]				= $detail_pengujian->id;
		    			$tentative_start[]	= $detail_pengujian->detailpenerimaan->tentative_start;
		    			$tentative_end[]	= $detail_pengujian->detailpenerimaan->tentative_end;
		    			$id[]				= $detail_pengujian->pengujian->id;
		    			$keterangan 		= '<a>'.$user->nama.'</a> memverifikasi No Seri : <a>'.$detail_pengujian->detail_penerimaan_barang->no_seri.'-'.$detail_pengujian->detail_penerimaan_barang->urutan.'</a> dengan hasil <a>Ditolak</a>';
		    		}

					$act = new ActPengujian;
					$act->parent_id 		= $detail_pengujian->id;
					$act->pengirim_id 		= $user->id;
					$act->penerima_id 		= $cari->pengirim_id;
					$act->tipe 				= 2;
					$act->jenis 			= 1;
					$act->keterangan 		= $keterangan;
					$act->save();

		            $this->detailpengujian()->save($detail_pengujian);
	    		}
	    		$result = array_unique($id);
	    		if(isset($result[0])){
	    			$cari_reschedule = Reschedule::where('pengujian_id', $result[0])->first();
	    			if($cari_reschedule){
			    		$reschedule 				= Reschedule::find($cari_reschedule->id);
				    	$reschedule->saveDetail($data, $tentative_start, $tentative_end);
	    			}else{
	    				$reschedule 				= new Reschedule;
				    	$reschedule->pengujian_id 	= $result[0];
				    	$reschedule->save();
				    	$reschedule->saveDetail($data, $tentative_start, $tentative_end);
	    			}
	    		}
    		}
    	}
    }

    public function scopeByLayanan($query, $layanan_id)
    {
        $query->whereHas('konfirmasi', function($k) use ($layanan_id){
            $k->whereHas('va', function($v) use ($layanan_id){
                $v->whereHas('surat', function($s) use ($layanan_id){
                    $s->whereHas('kaji_ulang', function($ku) use ($layanan_id){
                        $ku->whereHas('pp', function($p) use ($layanan_id){
                            return $p->where('layanan_id', $layanan_id);
                        });
                    });
                });
            });
        });
    }

    public function filesMorphClass()
    {
        return 'pengujian';
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }
}
