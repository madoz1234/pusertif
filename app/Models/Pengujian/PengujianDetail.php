<?php
namespace App\Models\Pengujian;

use App\Models\Model;

use App\Models\Authentication\User;
use App\Models\Pengujian\Pengujian;
use App\Models\Pengujian\PengujianDetailPelaksana;
use App\Models\PenerimaanBarang\PenerimaanBarangDetail;
use App\Models\LaporanPengujian\LaporanPengujianDetail;
use App\Models\Reschedule\RescheduleDetail;
use App\Models\Act\ActPengujian;

class PengujianDetail extends Model
{
    /* default */
    protected $table 		= 'trans_pengujian_detail';
    protected $fillable 	= ['pengujian_id','detail_penerimaan_id','verifikasi','pelaksana_id'];

    protected $log_table    = 'log_trans_pengujian_detail';
    protected $log_table_fk = 'ref_id';

    public function pengujian(){
        return $this->belongsTo(Pengujian::class, 'pengujian_id' , 'id');
    }

    public function detail_penerimaan_barang(){
        return $this->belongsTo(PenerimaanBarangDetail::class, 'detail_penerimaan_id');
    }

    public function pelaksana(){
        return $this->belongsTo(User::class, 'pelaksana_id');
    }

    public function detailpelaksana(){
        return $this->hasOne(PengujianDetailPelaksana::class, 'pengujian_detail_id' , 'id');
    }

    public function detaillaporan(){
        return $this->hasOne(LaporanPengujianDetail::class, 'pengujian_detail_id' , 'id');
    }

    public function detailpenerimaan(){
        return $this->belongsTo(PenerimaanBarangDetail::class, 'detail_penerimaan_id' , 'id');
    }

    public function detailreshcedule(){
        return $this->hasOne(RescheduleDetail::class, 'pengujian_detail_id' , 'id');
    }

    public function act_pengujian(){
        return $this->hasMany(ActPengujian::class, 'parent_id' , 'id');
    }

    // public function savePelaksana($data)
    // {
    // 	if($data)
    // 	{
    //         dd($data);
    // 	}
    // }
}
