<?php
namespace App\Models\Pengujian;

use App\Models\Model;

use App\Models\Authentication\User;
use App\Models\Pengujian\Pengujian;
use App\Models\Pengujian\PengujianDetailPelaksana;
use App\Models\Pengujian\PengujianDetailMember;

class PengujianDetailLaporan extends Model
{
    /* default */
    protected $table 		= 'trans_pengujian_detail_laporan';
    protected $fillable 	= ['detail_pelaksana_id','pelaksana_id'];

    protected $log_table    = 'log_trans_pengujian_detail_laporan';
    protected $log_table_fk = 'ref_id';

    public function detail(){
        return $this->belongsTo(PengujianDetailPelaksana::class, 'detail_pelaksana_id' , 'id');
    }

    public function pelaksana(){
        return $this->belongsTo(User::class, 'pelaksana_id');
    }
}
