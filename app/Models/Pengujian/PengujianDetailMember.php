<?php
namespace App\Models\Pengujian;

use App\Models\Model;

use App\Models\Authentication\User;
use App\Models\Pengujian\PengujianDetailPelaksana;

class PengujianDetailMember extends Model
{
    /* default */
    protected $table 		= 'trans_pengujian_detail_member';
    protected $fillable 	= ['detail_pelaksana_id','pelaksana_id'];

    protected $log_table    = 'log_trans_pengujian_detail_member';
    protected $log_table_fk = 'ref_id';

    public function detailpelaksana(){
        return $this->belongsTo(PengujianDetailPelaksana::class, 'detail_pelaksana_id' , 'id');
    }

    public function pelaksana(){
        return $this->belongsTo(User::class, 'pelaksana_id');
    }
}
