<?php
namespace App\Models\Pengujian;

use App\Models\Model;

use App\Models\Authentication\User;
use App\Models\Pengujian\Pengujian;
use App\Models\Pengujian\PengujianDetail;
use App\Models\Pengujian\PengujianDetailMember;
use App\Models\Picture;
use App\Models\Files;

/* Libraries */
use Storage;
use Zipper;
use Excel;

class PengujianDetailPelaksana extends Model
{
    /* default */
    protected $table 		= 'trans_pengujian_detail_pelaksana';
    protected $fillable 	= ['pengujian_detail_id','pelaksana_id','pelaksana_laporan_id','status','status_barang'];

    protected $log_table    = 'log_trans_pengujian_detail_pelaksana';
    protected $log_table_fk = 'ref_id';

    public function detail(){
        return $this->belongsTo(PengujianDetail::class, 'pengujian_detail_id' , 'id');
    }

    public function pelaksana(){
        return $this->belongsTo(User::class, 'pelaksana_id');
    }

    public function pelaksanalaporan(){
        return $this->belongsTo(User::class, 'pelaksana_laporan_id');
    }

    public function detailmember(){
        return $this->hasMany(PengujianDetailMember::class, 'detail_pelaksana_id' , 'id');
    }

    public function saveMember($pelaksana)
    {
    	if($pelaksana)
    	{
            foreach ($pelaksana as $key => $value) {
            	$detail_member 							= new PengujianDetailMember;
	    		$detail_member->pelaksana_id 			= $value;
	            $this->detailmember()->save($detail_member);
            }
    	}
    }

    public function saveLaporan($bukti)
    {
    	if($bukti)
    	{
        	$detail_laporan 					= new PengujianDetailLaporan;
    		if($file = $bukti){
    			$path = $file->store('uploads/pengujian', 'public');
    			$detail_laporan->filename = $file->getClientOriginalName();
    			$detail_laporan->bukti = $path;
    		}
            $this->detaillaporan()->save($detail_laporan);
    	}
    }
}
