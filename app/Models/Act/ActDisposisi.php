<?php
namespace App\Models\Act;

use App\Models\Model;

use App\Models\Authentication\User;
use App\Models\FrontEnd\PendaftaranPengujian;

class ActDisposisi extends Model
{
    /* default */
    protected $table 		= 'act_disposisi';
    protected $fillable 	= ['parent_id','pengirim_id','penerima_id','tipe','jenis','keterangan','status_kaji_ulang','status_surat_penawaran', 'status_pengujian'];

    /* data ke log */
    protected $log_table    = 'log_act_disposisi';
    protected $log_table_fk = 'act_id';
    
    public function pengirim(){
        return $this->belongsTo(User::class, 'pengirim_id');
    }

    public function penerima(){
        return $this->belongsTo(User::class, 'penerima_id');
    }

    public function pp(){
        return $this->belongsTo(PendaftaranPengujian::class, 'parent_id');
    }

    public function prev(){
    	$ret = $this->pp->act_dispo()
    					->where('id', '<', $this->id)
					   	->orderBy('id', 'desc')
					   	->first();
					   	
    	return $ret 
    		 ? $ret
    		 : $this->pp->logsyan()->first(); 
    }
}
