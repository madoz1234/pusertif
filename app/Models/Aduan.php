<?php
namespace App\Models;

use App\Models\Model;

class Aduan extends Model
{
    /* default */
    protected $table 		= 'trans_aduan';
    protected $fillable 	= ['pilihan','nama','email','no_hp','pesan','lampiran','filename','status','user_id','tgl','keterangan','tanggapan','path'];

    protected $log_table    = 'log_trans_aduan';
    protected $log_table_fk = 'ref_id';


    public function filesMorphClass()
    {
        return 'pengaduan';
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }

    public function user(){
        return $this->belongsTo(Users::class, 'user_id');
    }
}
