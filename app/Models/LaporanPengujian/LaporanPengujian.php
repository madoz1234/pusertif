<?php
namespace App\Models\LaporanPengujian;

use App\Models\Model;

use App\Models\Attachments;
use App\Models\Files;
use App\Models\Authentication\User;
use App\Models\Pengujian\PengujianDetail;
use App\Models\LaporanPengujian\LaporanPengujianDetail;
use App\Models\Pengujian\Pengujian;
use App\Models\Act\ActPengujian;

class LaporanPengujian extends Model
{
    /* default */
    protected $table 		= 'trans_laporan_pengujian';
    protected $fillable 	= ['pengujian_id','no_laporan','tgl_laporan','status'];

    protected $log_table    = 'log_trans_laporan_pengujian';
    protected $log_table_fk = 'ref_id';

    public function pengujian(){
        return $this->belongsTo(Pengujian::class, 'pengujian_id' , 'id');
    }

    public function detaillaporan(){
        return $this->hasMany(LaporanPengujianDetail::class, 'laporan_pengujian_id' , 'id');
    }

    public function pengirim(){
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function saveDetail($detail)
    {	
    	$user = auth()->user();
    	if($detail)
    	{
			$cari = LaporanPengujianDetail::get();
    		if($cari->count() > 0){
	    		$group = ($cari->max('group_id') + 1);
    		}else{
    			$group = 1;
    		}

    		foreach ($detail as $key => $value) {
    			$detail_pengujian 						= PengujianDetail::find($value);
    			$data 									= PengujianDetail::where('group_id', $detail_pengujian->group_id)->where('flag',0)->get();
    			foreach ($data as $key => $val) {
		    		$laporan_pengujian 							= new LaporanPengujianDetail;
		    		$laporan_pengujian->pengujian_detail_id 	= $val->id;
		    		$laporan_pengujian->group_id 				= $group;
		            $this->detaillaporan()->save($laporan_pengujian);

		            $pengujian_detail 					= PengujianDetail::find($val->id);
		            $pengujian_detail->status_laporan 	= 1;
		            $pengujian_detail->save();
		            
	    			$act = new ActPengujian;
					$act->parent_id 		= $val->id;
					$act->pengirim_id 		= $user->id;
					$act->tipe 				= 2;
					$act->jenis 			= 2;
					if($pengujian_detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id == 1){
						$data_user = User::whereHas('roles', function($u){
		    									$u->where('name', 'yan-uji');
		    				  	 			})->first();
						$act->penerima_id 		= $data_user->id;
						$act->keterangan 		= '<a>'.$user->nama.'</a> Menyelesaikan Laporan Final Pengujian dengan Item <a>'.$pengujian_detail->detail_penerimaan_barang->nama_barang.'</a> dan dilaporkan kepada <a>'.$data_user->nama.'</a> dengan No Laporan : <a>'.$pengujian_detail->pengujian->laporan_pengujian->first()->no_laporan.'</a>  untuk dilakukan <a>Pengiriman Laporan</a>';
					}else{
		           		$data_user = User::whereHas('roles', function($u){
		    									$u->where('name', 'yan-uji');
		    				  	 			})->first();
						$act->penerima_id 		= $data_user->id;
						$act->keterangan 		= '<a>'.$user->nama.'</a> Menyelesaikan Laporan Final Pengujian dengan Item <a>'.$pengujian_detail->detail_penerimaan_barang->nama_barang.'</a> dan dilaporkan kepada <a>'.$data_user->nama.'</a> dengan No Laporan : <a>'.$pengujian_detail->pengujian->laporan_pengujian->first()->no_laporan.'</a>  untuk dilakukan <a>Pengiriman Laporan</a>';
					}
					$act->save();


		            $data_count = PengujianDetail::where('pengujian_id', $pengujian_detail->pengujian_id)
		            									->where('flag',0)
		    											->where('status_laporan', '<', 1)
		    											->count();
		    		if($data_count == 0){
		    			$pengujian 						 	= Pengujian::find($pengujian_detail->pengujian_id);
			    		$pengujian->status_data_pengujian 	= 1;
			    		$pengujian->save();
		    		}
    			}
    		}
    	}
    }

    public function saveDetailKalibrasi($detail)
    {
    	if($detail)
    	{
    		$user = auth()->user();
			$cari = LaporanPengujianDetail::get();
    		if($cari->count() > 0){
	    		$group = ($cari->max('group_id') + 1);
    		}else{
    			$group = 1;
    		}

    		foreach ($detail as $key => $value) {
    			$laporan_pengujian 							= new LaporanPengujianDetail;
	    		$laporan_pengujian->pengujian_detail_id 	= $value;
	    		$laporan_pengujian->group_id 				= $group;
	            $this->detaillaporan()->save($laporan_pengujian);

	            $pengujian_detail 					= PengujianDetail::find($value);
	            $pengujian_detail->status_laporan 	= 1;
	            $pengujian_detail->save();

	            $pengujian_detail 					= PengujianDetail::find($value);
	            $pengujian_detail->status_laporan 	= 1;
	            $pengujian_detail->save();

	            $act = new ActPengujian;
				$act->parent_id 		= $pengujian_detail->id;
				$act->pengirim_id 		= $user->id;
				$act->tipe 				= 2;
				$act->jenis 			= 2;
				if($pengujian_detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id == 1){
					$data_user = User::whereHas('roles', function($u){
	    									$u->where('name', 'yan-uji');
	    				  	 			})->first();
					$act->penerima_id 		= $data_user->id;
					$act->keterangan 		= '<a>'.$user->nama.'</a> Menyelesaikan Laporan Final Pengujian dengan Item <a>'.$pengujian_detail->detail_penerimaan_barang->nama_barang.'</a> dan dilaporkan kepada <a>'.$data_user->nama.'</a> dengan No Laporan : <a>'.$pengujian_detail->pengujian->laporan_pengujian->first()->no_laporan.'</a>  untuk dilakukan <a>Pengiriman Laporan</a>';
				}else{
	           		$data_user = User::whereHas('roles', function($u){
	    									$u->where('name', 'yan-uji');
	    				  	 			})->first();
					$act->penerima_id 		= $data_user->id;
					$act->keterangan 		= '<a>'.$user->nama.'</a> Menyelesaikan Laporan Final Pengujian dengan Item <a>'.$pengujian_detail->detail_penerimaan_barang->nama_barang.'</a> dan dilaporkan kepada <a>'.$data_user->nama.'</a> dengan No Laporan : <a>'.$pengujian_detail->pengujian->laporan_pengujian->first()->no_laporan.'</a>  untuk dilakukan <a>Pengiriman Laporan</a>';
				}
				$act->save();


	            $data_count = PengujianDetail::where('pengujian_id', $pengujian_detail->pengujian_id)
	            									->where('flag',0)
	    											->where('status_laporan', '<', 1)
	    											->count();
	    		if($data_count == 0){
	    			$pengujian 						 	= Pengujian::find($pengujian_detail->pengujian_id);
		    		$pengujian->status_data_pengujian 	= 1;
		    		$pengujian->save();
	    		}
    		}
    	}
    }

    public function filesMorphClass()
    {
        return 'laporan-pengujian';
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }
}
