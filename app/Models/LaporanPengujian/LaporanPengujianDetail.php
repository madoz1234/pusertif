<?php
namespace App\Models\LaporanPengujian;

use App\Models\Model;

use App\Models\Authentication\User;
use App\Models\LaporanPengujian\LaporanPengujian;
use App\Models\Pengujian\PengujianDetail;

class LaporanPengujianDetail extends Model
{
    /* default */
    protected $table 		= 'trans_laporan_pengujian_detail';
    protected $fillable 	= ['laporan_pengujian_id','pengujian_detail_id','group_id'];

    protected $log_table    = 'log_trans_laporan_pengujian_detail';
    protected $log_table_fk = 'ref_id';

    public function laporanpengujian(){
        return $this->belongsTo(LaporanPengujian::class, 'laporan_pengujian_id', 'id');
    }

    public function detail_pengujian(){
        return $this->belongsTo(PengujianDetail::class, 'pengujian_detail_id', 'id');
    }
}
