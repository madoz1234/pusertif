<?php

namespace App\Models\Authentication;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

// entrust
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Models\Authentication\Role;

// Models
use App\Models\UsersDetail;

use App\Models\Master\Karyawan;
use App\Models\Project\Project;
use App\Models\Project\Task;
use App\Models\Traits\Utilities;

use App\Models\Master\Perusahaan;
use App\Models\Master\Pelanggan;

class User extends Authenticatable
{
    use Utilities;
    use Notifiable;
    use EntrustUserTrait;

    public $table = 'sys_users';
    public $remember_token = false;

    protected $dates = ['last_login'];
    protected $fillable = [
      'username', 'password', 'email', 'deleted_at', 'last_login','device_id',
    ];

    /* Relation */
    public function karyawan()
    {
        return $this->hasOne(Karyawan::class, 'user_id');
    }

    public function pelanggan()
    {
        return $this->hasOne(Pelanggan::class, 'user_id');
    }

    public function detail()
    {
        return $this->hasOne(UsersDetail::class, 'user_id');
    }

    public function scopeAkses($query, $record)
    {
        return $query->whereHas('roles', function($a) use ($record){
        	if($record == 1){
        		$a->where('name', 'msb-kalibrasi');
        	}elseif($record == 2){
        		$a->where('name', 'msb-siskit');
        	}elseif($record == 3){
				$a->where('name', 'msb-sistgi');
        	}elseif($record == 4){
        		$a->where('name', 'msb-tegangan-rendah');
        	}else{
        		$a->where('name', 'msb-tegangan-tinggi');
        	}
        });
    }

    public function scopeAksesa($query, $record)
    {
        return $query->whereHas('roles', function($a) use ($record){
    		if($record == 5){
    			$a->where('name', 7);
    		}elseif($record == 4){
    			$a->where('name', 8);
    		}else{

    		}
        });
    }

    public function scopeDispoMsb($query, $record)
    {
        return $query->whereHas('roles', function($a) use ($record){
    		if($record == 1){
        		$a->whereIn('name', ['asman-dal-kalibrasi','asman-lola-kalibrasi']);
        	}elseif($record == 2){
        		$a->whereIn('name', ['asman-dal-siskit','asman-lola-siskit']);
        	}elseif($record == 3){
				$a->whereIn('name', ['asman-dal-sistgi','asman-lola-sistgi']);
        	}elseif($record == 4){
        		$a->whereIn('name', ['asman-dal-tegangan-rendah','asman-lola-tegangan-rendah']);
        	}else{
        		$a->whereIn('name', ['asman-dal-tegangan-tinggi','asman-lola-tegangan-tinggi']);
        	}
        });
    }

    public function scopeDispo($query, $layanan, $tipe, $status, $status_data)
    {
    	return $query->whereHas('roles', function($a) use ($layanan, $tipe, $status, $status_data){
    		if($layanan == 1){
    			if($status == 1){
	    			if($tipe == 1){
		        		$a->whereIn('name', ['asman-dal-kalibrasi','asman-lola-kalibrasi']); //ASMEN
	    			}elseif($tipe == 2){
	    				if($status_data == 1){
		    				$a->where('name', 'pelaksana'); // PELAKSANA
	    				}else{
	    					$a->where('name', 'msb-kalibrasi'); //MSB
	    				}
	    			}elseif($tipe == 4){
	    				$a->whereIn('name', ['asman-dal-kalibrasi','asman-lola-kalibrasi']);
	    			}else{
	    				$a;
	    			}
    			}elseif($status ==2){
    				if($tipe == 1){
		        		$a->whereIn('name', ['asman-dal-kalibrasi','asman-lola-kalibrasi']); //ASMEN
	    			}elseif($tipe == 2){
	    				$a->where('name', 'pelaksana'); // PELAKSANA
	    			}elseif($tipe == 4){
	    				$a->whereIn('name', ['asman-dal-kalibrasi','asman-lola-kalibrasi']);
	    			}else{
	    				$a;
	    			}
    			}else{
    				if($tipe == 1){
		        		$a->whereIn('name', ['asman-dal-kalibrasi','asman-lola-kalibrasi']); // ASMEN
	    			}elseif($tipe == 2){
	    				$a->where('name', 'pelaksana'); // PELAKSANA
	    			}else{
	    				$a;
	    			}
    			}
        	}elseif($layanan == 2){
        		if($status == 1){
	    			if($tipe == 1){
		        		$a->whereIn('name', ['asman-dal-siskit','asman-lola-siskit']); //ASMEN
	    			}elseif($tipe == 2){
	    				if($status_data == 1){
		    				$a->where('name', 'pelaksana'); // PELAKSANA
	    				}else{
	    					$a->where('name', 'msb-siskit'); //MSB
	    				}
	    			}elseif($tipe == 4){
	    				$a->whereIn('name', ['asman-dal-siskit','asman-lola-siskit']);
	    			}else{
	    				$a;
	    			}
    			}elseif($status ==2){
    				if($tipe == 1){
		        		$a->whereIn('name', ['asman-dal-siskit','asman-lola-siskit']); //ASMEN
	    			}elseif($tipe == 2){
	    				$a->where('name', 'pelaksana'); // PELAKSANA
	    			}elseif($tipe == 4){
	    				$a->whereIn('name', ['asman-dal-siskit','asman-lola-siskit']);
	    			}else{
	    				$a;
	    			}
    			}else{
    				if($tipe == 1){
		        		$a->whereIn('name', ['asman-dal-siskit','asman-lola-siskit']); // ASMEN
	    			}elseif($tipe == 2){
	    				$a->where('name', 'pelaksana'); // PELAKSANA
	    			}else{
	    				$a;
	    			}
    			}
        	}elseif($layanan == 3){
        		if($status == 1){
	    			if($tipe == 1){
		        		$a->whereIn('name', ['asman-dal-sistgi','asman-lola-sistgi']); //ASMEN
	    			}elseif($tipe == 2){
	    				if($status_data == 1){
		    				$a->where('name', 'pelaksana'); // PELAKSANA
	    				}else{
	    					$a->where('name', 'msb-sistgi'); //MSB
	    				}
	    			}elseif($tipe == 4){
	    				$a->whereIn('name', ['asman-dal-sistgi','asman-lola-sistgi']);
	    			}else{
	    				$a;
	    			}
    			}elseif($status ==2){
    				if($tipe == 1){
		        		$a->whereIn('name', ['asman-dal-sistgi','asman-lola-sistgi']); //ASMEN
	    			}elseif($tipe == 2){
	    				$a->where('name', 'pelaksana'); // PELAKSANA
	    			}elseif($tipe == 4){
	    				$a->whereIn('name', ['asman-dal-sistgi','asman-lola-sistgi']);
	    			}else{
	    				$a;
	    			}
    			}else{
    				if($tipe == 1){
		        		$a->whereIn('name', ['asman-dal-sistgi','asman-lola-sistgi']); // ASMEN
	    			}elseif($tipe == 2){
	    				$a->where('name', 'pelaksana'); // PELAKSANA
	    			}else{
	    				$a;
	    			}
    			}
        	}elseif($layanan == 4){
        		if($status == 1){
	    			if($tipe == 1){
		        		$a->whereIn('name', ['asman-dal-tegangan-rendah','asman-lola-tegangan-rendah']); //ASMEN
	    			}elseif($tipe == 2){
	    				if($status_data == 1){
		    				$a->where('name', 'pelaksana'); // PELAKSANA
	    				}else{
	    					$a->where('name', 'msb-tegangan-rendah'); //MSB
	    				}
	    			}elseif($tipe == 4){
	    				$a->whereIn('name', ['asman-dal-tegangan-rendah','asman-lola-tegangan-rendah']);
	    			}else{
	    				$a;
	    			}
    			}elseif($status ==2){
    				if($tipe == 1){
		        		$a->whereIn('name', ['asman-dal-tegangan-rendah','asman-lola-tegangan-rendah']); //ASMEN
	    			}elseif($tipe == 2){
	    				$a->where('name', 'pelaksana'); // PELAKSANA
	    			}elseif($tipe == 4){
	    				$a->whereIn('name', ['asman-dal-tegangan-rendah','asman-lola-tegangan-rendah']);
	    			}else{
	    				$a;
	    			}
    			}else{
    				if($tipe == 1){
		        		$a->whereIn('name', ['asman-dal-tegangan-rendah','asman-lola-tegangan-rendah']); // ASMEN
	    			}elseif($tipe == 2){
	    				$a->where('name', 'pelaksana'); // PELAKSANA
	    			}else{
	    				$a;
	    			}
    			}
        	}else{
        		if($status == 1){
	    			if($tipe == 1){
		        		$a->whereIn('name', ['asman-dal-tegangan-tinggi','asman-lola-tegangan-tinggi']); //ASMEN
	    			}elseif($tipe == 2){
	    				if($status_data == 1){
		    				$a->where('name', 'pelaksana'); // PELAKSANA
	    				}else{
	    					$a->where('name', 'msb-tegangan-tinggi'); //MSB
	    				}
	    			}elseif($tipe == 4){
	    				$a->whereIn('name', ['asman-dal-tegangan-tinggi','asman-lola-tegangan-tinggi']);
	    			}else{
	    				$a;
	    			}
    			}elseif($status ==2){
    				if($tipe == 1){
		        		$a->whereIn('name', ['asman-dal-tegangan-tinggi','asman-lola-tegangan-tinggi']); //ASMEN
	    			}elseif($tipe == 2){
	    				$a->where('name', 'pelaksana'); // PELAKSANA
	    			}elseif($tipe == 4){
	    				$a->whereIn('name', ['asman-dal-tegangan-tinggi','asman-lola-tegangan-tinggi']);
	    			}else{
	    				$a;
	    			}
    			}else{
    				if($tipe == 1){
		        		$a->whereIn('name', ['asman-dal-tegangan-tinggi','asman-lola-tegangan-tinggi']); // ASMEN
	    			}elseif($tipe == 2){
	    				$a->where('name', 'pelaksana'); // PELAKSANA
	    			}else{
	    				$a;
	    			}
    			}
        	}
        });
    }
}
