<?php 
namespace App\Models\Authentication;

use Zizaco\Entrust\EntrustRole;
use App\Models\Traits\Utilities;
use Config;

class Role extends EntrustRole
{
    use Utilities;
    protected $fillable = [
      'display_name', 'name', 'description'
    ];

    public function users()
	{
		return $this->belongsToMany(Config::get('auth.providers.users.model'),Config::get('entrust.role_user_table'),Config::get('entrust.role_foreign_key'),Config::get('entrust.user_foreign_key'));
	}
	
}