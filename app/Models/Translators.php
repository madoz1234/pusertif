<?php

namespace App\Models;

use App\Models\Model;
use App\Models\User;
use CyrildeWit\EloquentViewable\Viewable;
use CyrildeWit\EloquentViewable\Contracts\Viewable as ViewableContract;

class Translators extends Model implements ViewableContract
{
    use Viewable;
    
    protected $table 		= 'sys_translator';

    protected $fillable 	= [
        'translator',
        'created_by',
    ];

}
