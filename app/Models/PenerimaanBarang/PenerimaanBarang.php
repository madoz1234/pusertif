<?php
namespace App\Models\PenerimaanBarang;

use App\Models\Model;

use App\Models\Pengujian\Pengujian;
use App\Models\Authentication\User;
use App\Models\VirtualAccount\KonfirmasiPembayaran;
use App\Models\PenerimaanBarang\PenerimaanBarangDetail;
use App\Models\CloseOrder\CloseOrder;
use App\Models\Pengujian\PengujianDetail;
use App\Models\FrontEnd\PendaftaranPengujianDetail;
use Carbon;

class PenerimaanBarang extends Model
{
    /* default */
    protected $table 		= 'trans_penerimaan_barang';
    protected $fillable 	= ['konfirmasi_id','no_kpj','no_agenda','status','pengirim','penerima_id'];

    /* data ke log */
    protected $log_table    = 'log_trans_penerimaan_barang';
    protected $log_table_fk = 'ref_id';

    public function pengujian()
    {
        return $this->hasOne(Pengujian::class, 'penerimaan_id', 'id');
    }

    public function detail_penerimaan_barang(){
        return $this->hasMany(PenerimaanBarangDetail::class, 'penerimaan_id' , 'id');
    }

    public function konfirmasi(){
        return $this->belongsTo(KonfirmasiPembayaran::class, 'konfirmasi_id');
    }

    public function penerima(){
        return $this->belongsTo(User::class, 'penerima_id');
    }

    public function closeorder(){
        return $this->hasOne(CloseOrder::class, 'penerimaan_id', 'id');
    }

    public function saveDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			if(isset($value['detail'])){
	    			$i = $value['detail'][1]['max'] - count($value['detail']);
	    			foreach($value['detail'] as $key => $val) {
			    		$data 					= new PenerimaanBarangDetail;
			    		$data->detail_pp_id 	= $val['id'];
			    		$data->no_seri 			= $val['seri'];
			    		$data->nama_barang 		= $val['nama'];
			    		$data->urutan 			= $val['urutan'];
			    		$data->catatan 			= $val['catatan'];
			    		$data->max 				= $val['max'];
			    		$data->tentative_start 	= $value['tentative_start'];
			    		$data->tentative_end 	= $value['tentative_end'];
			    		$data->group_id 		= $value['group_id'];
			    		$data->status 			= 1;
			    		$data->flag 			= 0;

			    		$pp_detail = PendaftaranPengujianDetail::find($val['id']);
			            $this->detail_penerimaan_barang()->save($data);
	    			}
	    			$cek_urutan = $val['urutan'];
	    			if($i > 0){
	    				for($j=1;$j<=$i;$j++){
	    					$data 					= new PenerimaanBarangDetail;
				    		$data->detail_pp_id 	= $val['id'];
				    		$data->no_seri 			= $val['nama'];
				    		$data->nama_barang 		= $val['nama'];
				    		$data->urutan 			= ($val['urutan']+$j);
				    		$data->catatan 			= 'Barang Belum Datang';
				    		$data->max 				= $val['max'];
				    		$data->tentative_start 	= $value['tentative_start'];
				    		$data->tentative_end 	= $value['tentative_end'];
				    		$data->group_id 		= $value['group_id'];
				    		$data->status 			= 1;
				    		$data->flag 			= 1;
				            $this->detail_penerimaan_barang()->save($data);
	    				}
	    			}
    			}
    		}
    	}
    }

    public function updateDetail($detail, $tgl)
    {	
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			foreach($value['detail'] as $key => $val) {
    				if(isset($val['data_id'])){
    					$data 					= PenerimaanBarangDetail::find($val['data_id']);
			    		$data->no_seri 			= $val['seri'];
			    		$data->nama_barang 		= $val['nama'];
			    		$data->urutan 			= $val['urutan'];
			    		$data->catatan 			= $val['catatan'];
			    		$data->tentative_start 	= $val['tentative_start'];
			    		$data->tentative_end 	= $val['tentative_end'];
			    		$data->tanggal_terima 	= Carbon::parse($tgl)->format('Y-m-d H:i:s');
			    		$data->status 			= 1;
			    		$data->flag 			= 0;
			            $cari							 	= PengujianDetail::where('detail_penerimaan_id', $val['data_id'])->first();
			            $pengujian_detail 					= PengujianDetail::find($cari->id);
			            $pengujian_detail->verifikasi 		= 0;
			            $pengujian_detail->status 			= 0;
			            $pengujian_detail->flag 			= 0;
			            $pengujian_detail->save();
			            $this->detail_penerimaan_barang()->save($data);

    				}else{
    					$data 					= new PenerimaanBarangDetail;
			    		$data->detail_pp_id 	= $val['id'];
			    		$data->no_seri 			= $val['seri'];
			    		$data->nama_barang 		= $val['nama'];
			    		$data->urutan 			= $val['urutan'];
			    		$data->catatan 			= $val['catatan'];
			    		$data->tentative_start 	= $value['tentative_start'];
			    		$data->tentative_end 	= $value['tentative_end'];
			    		$data->tanggal_terima 	= Carbon::parse($tgl)->format('Y-m-d H:i:s');
			    		$data->status 			= 1;
			            $this->detail_penerimaan_barang()->save($data);
    				}
    			}
    		}
    	}
    }

    public function scopeByLayanan($query, $layanan_id)
    {
        $query->whereHas('konfirmasi', function($k) use ($layanan_id){
            $k->whereHas('va', function($v) use ($layanan_id){
                $v->whereHas('surat', function($s) use ($layanan_id){
                    $s->whereHas('kaji_ulang', function($ku) use ($layanan_id){
                        $ku->whereHas('pp', function($p) use ($layanan_id){
                            return $p->where('layanan_id', $layanan_id);
                        });
                    });
                });
            })->where('tipe', 0);
        });
    }

    public function scopeByLayananV2($query, $layanan_id)
    {
        $query->whereHas('konfirmasi', function($k) use ($layanan_id){
            $k->whereHas('va', function($v) use ($layanan_id){
                $v->whereHas('surat', function($s) use ($layanan_id){
                    $s->whereHas('kaji_ulang', function($ku) use ($layanan_id){
                        $ku->whereHas('pp', function($p) use ($layanan_id){
                            return $p->whereIn('layanan_id', $layanan_id);
                        });
                    });
                });
            })->where('tipe', 0);
        });
    }
}
