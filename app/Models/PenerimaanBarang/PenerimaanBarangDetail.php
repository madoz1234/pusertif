<?php
namespace App\Models\PenerimaanBarang;

use App\Models\Model;

use App\Models\Authentication\User;
use App\Models\PenerimaanBarang\PenerimaanBarang;
use App\Models\Pengujian\PengujianDetail;
use App\Models\FrontEnd\PendaftaranPengujianDetail;

class PenerimaanBarangDetail extends Model
{
    /* default */
    protected $table 		= 'trans_penerimaan_barang_detail';
    protected $fillable 	= ['penerimaan_id','detail_pp_id','no_seri','nama_barang','catatan','status','urutan','max','tentative_start','tentative_end'];

    /* data ke log */
    protected $log_table    = 'log_trans_penerimaan_barang_detail';
    protected $log_table_fk = 'ref_id';

    public function penerimaan(){
        return $this->belongsTo(PenerimaanBarang::class, 'penerimaan_id');
    }

    public function detail_pp(){
        return $this->belongsTo(PendaftaranPengujianDetail::class, 'detail_pp_id');
    }

    public function detail_pengujian(){
    	return $this->hasOne(PengujianDetail::class, 'detail_penerimaan_id', 'id');
    }
}
