<?php
namespace App\Models\PengembalianBarang;

use App\Models\Model;

use App\Models\Pengujian\Pengujian;
use App\Models\Authentication\User;
use App\Models\VirtualAccount\KonfirmasiPembayaran;
use App\Models\PengembalianBarang\PengembalianBarangDetail;
use App\Models\PenerimaanBarang\PenerimaanBarang;
use App\Models\PenerimaanBarang\PenerimaanBarangDetail;
use App\Models\Pengujian\PengujianDetailPelaksana;
use App\Models\Pengujian\PengujianDetail;

class PengembalianBarang extends Model
{
    /* default */
    protected $table 		= 'trans_pengembalian_barang';
    protected $fillable 	= ['penerimaan_id','no_nota_dinas','tgl_nota_dinas','status'];

    /* data ke log */
    protected $log_table    = 'log_trans_pengembalian_barang';
    protected $log_table_fk = 'ref_id';
    
    public function penerimaan(){
        return $this->belongsTo(PenerimaanBarang::class, 'penerimaan_id');
    }

    public function detail_pengembalian(){
        return $this->hasMany(PengembalianBarangDetail::class, 'pengembalian_id' , 'id');
    }

    public function saveDetail($list)
    {
    	if($list)
    	{
    		foreach ($list as $key => $value) {
    			$cari = PenerimaanBarangDetail::find($value);
	            $pengujian_pelaksana = PengujianDetailPelaksana::find($cari->detail_pengujian->detailpelaksana->id);
				$pengujian_pelaksana->status_pengembalian =1;
				$pengujian_pelaksana->save();

	    		$data 							= new PengembalianBarangDetail;
	    		$data->penerimaan_detail_id 	= $value;
	    		$data->group_id 				= $cari->group_id;
	            $this->detail_pengembalian()->save($data);
    		}

    		$cek = PenerimaanBarangDetail::find($list[0]);
    		$cek_detail_pengujian = PengujianDetail::whereHas('detailpelaksana', function($u){
    													$u->where('status_barang', 1)
    													  ->where('status_pengembalian', '<', 1);
    												})
    												->where('flag', 0)
    												->where('pengujian_id', $cek->detail_pengujian->pengujian_id)
    												->count();
    		if($cek_detail_pengujian == 0){
    			$data_detail = Pengujian::find($cek->detail_pengujian->pengujian_id);
    			$penerimaan_barang = PenerimaanBarang::find($data_detail->penerimaan->id);
    			$penerimaan_barang->status_pengembalian = 1;
    			$penerimaan_barang->save();
    		}
    	}
    }
}
