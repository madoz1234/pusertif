<?php
namespace App\Models\PengembalianBarang;

use App\Models\Model;

use App\Models\Authentication\User;
use App\Models\PengembalianBarang\PengembalianBarang;
use App\Models\PenerimaanBarang\PenerimaanBarangDetail;

class PengembalianBarangDetail extends Model
{
    /* default */
    protected $table 		= 'trans_pengembalian_barang_detail';
    protected $fillable 	= ['pengembalian_id','penerimaan_detail_id','group_id'];

    /* data ke log */
    protected $log_table    = 'log_trans_pengembalian_barang_detail';
    protected $log_table_fk = 'ref_id';

    public function pengembalian(){
        return $this->belongsTo(PengembalianBarang::class, 'pengembalian_id');
    }

    public function penerimaandetail(){
        return $this->belongsTo(PenerimaanBarangDetail::class, 'penerimaan_detail_id');
    }
}
