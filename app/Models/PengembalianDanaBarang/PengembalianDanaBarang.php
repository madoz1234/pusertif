<?php
namespace App\Models\PengembalianDanaBarang;

use App\Models\Model;

use App\Models\Attachments;
use App\Models\Files;
use App\Models\Authentication\User;
use App\Models\PengembalianDanaBarang\PengembalianDanaBarangDetail;
use App\Models\CloseOrder\CloseOrder;
use App\Models\CloseOrder\CloseOrderDetail;

class PengembalianDanaBarang extends Model
{
    /* default */
    protected $table 		= 'trans_pengembalian_dana_barang';
    protected $fillable 	= ['close_id','no_nota','tgl_nota','dana_kembali','status'];

    protected $log_table    = 'log_trans_pengembalian_dana_barang';
    protected $log_table_fk = 'ref_id';

    public function closeorder(){
        return $this->belongsTo(CloseOrder::class, 'close_id' , 'id');
    }

    public function detailkembali(){
        return $this->hasMany(PengembalianDanaBarangDetail::class, 'dana_id' , 'id');
    }

    public function saveDetail($id)
    {
    	if($id)
    	{
			$cari = PengembalianDanaBarangDetail::get();
    		if($cari->count() > 0){
	    		$group = ($cari->max('group_id') + 1);
    		}else{
    			$group = 1;
    		}
    		$cari 						= CloseOrder::find($id);
    		foreach ($cari->detailclose as $key => $value) {
    			if($value->detail_penerimaan->detail_pengujian->verifikasi != 1){
		    		$barang_kembali_detail 							= new PengembalianDanaBarangDetail;
		    		$barang_kembali_detail->close_detail_id 		= $value->id;
		    		$barang_kembali_detail->group_id 				= $group;
		            $this->detailkembali()->save($barang_kembali_detail);
    			}
    		}
    	}
    }
}
