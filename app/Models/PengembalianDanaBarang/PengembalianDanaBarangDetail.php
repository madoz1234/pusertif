<?php
namespace App\Models\PengembalianDanaBarang;

use App\Models\Model;

use App\Models\Authentication\User;
use App\Models\PengembalianDanaBarang\PengembalianDanaBarang;
use App\Models\CloseOrder\CloseOrderDetail;

class PengembalianDanaBarangDetail extends Model
{
    /* default */
    protected $table 		= 'trans_pengembalian_dana_barang_detail';
    protected $fillable 	= ['dana_id','close_detail_id','group_id'];

    protected $log_table    = 'log_trans_pengembalian_dana_barang_detail';
    protected $log_table_fk = 'ref_id';

    public function pengembalian(){
        return $this->belongsTo(PengembalianDanaBarang::class, 'dana_id', 'id');
    }

    public function detail_penerimaan(){
        return $this->belongsTo(CloseOrderDetail::class, 'close_detail_id', 'id');
    }
}
