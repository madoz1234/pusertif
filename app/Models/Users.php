<?php
namespace App\Models;

use App\Models\Model;
use App\Models\Master\JenisPelayanan;
use App\Models\Master\Pegawai;
use App\Models\UsersDetail;

use App\Models\Master\Perusahaan;
use App\Models\Master\Pelanggan;

class Users extends Model
{
    /* default */
    protected $table 		= 'sys_users';
    protected $fillable 	= ['username','email','password','nama','status','no_hp','peminta_jasa','pelanggan','no_tlp','last_login'];
    protected $dates = ['last_login'];

    public function jenis(){
        return $this->belongsTo(JenisPelayanan::class, 'jenis_pelayanan_id');
    }

     public function detail()
    {
        return $this->hasOne(UsersDetail::class, 'user_id');
    }

    public function pelanggans()
    {
        return $this->hasOne(Pelanggan::class, 'user_id');
    }
}
