<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\JenisPelayanan;

class Prioritas extends Model
{
    /* default */
    protected $table 		= 'ref_prioritas';
    protected $fillable 	= ['layanan_id','waktu'];

    /* data ke log */
    protected $log_table    = 'log_ref_prioritas';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function pelayanan(){
        return $this->belongsTo(JenisPelayanan::class, 'layanan_id');
    }
}
