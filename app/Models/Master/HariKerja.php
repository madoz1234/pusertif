<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\JenisPelayanan;

class HariKerja extends Model
{
	/* default */
    protected $table	= 'ref_hari_kerja';
    protected $fillable	= ['description','hk','jenis_pelayanan_id','menu','display_menu'];

    /* data ke log */
    protected $log_table 	= 'log_ref_hari_kerja';
    protected $log_table_fk	= 'ref_id';	

    /* relation */

    public function pelayanan(){
        return $this->belongsTo(JenisPelayanan::class, 'jenis_pelayanan_id' , 'id');
    }
    
}
