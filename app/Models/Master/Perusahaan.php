<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\Pelanggan;
use App\Models\FrontEnd\PendaftaranPengujian;

class Perusahaan extends Model
{
    /* default */
    protected $table 		= 'ref_perusahaan';
    protected $fillable 	= ['nama','kode','no_tlp','alamat','status','kategori','email'];

    /* data ke log */
    protected $log_table    = 'log_ref_perusahaan';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function pelanggan(){
    	return $this->hasMany(Pelanggan::class, 'perusahaan_id' , 'id');
    }

    public static function generatePerusahaan()
    {
        $rcd = static::orderBy('kode', 'desc')->first();
        $nomor = 1;
        if ($rcd) {
            $nomor = (int) substr($rcd->kode, -4);
            $nomor++;
        }

        return str_pad($nomor, 4, "0", STR_PAD_LEFT);
    }
    // insert code here


    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
