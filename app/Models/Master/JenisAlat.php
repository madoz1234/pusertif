<?php
namespace App\Models\Master;

use App\Models\Model;

class JenisAlat extends Model
{
    /* default */
    protected $table 		= 'ref_jenis_alat';
    protected $fillable 	= ['nama_jenis_alat'];

    /* data ke log */
    protected $log_table    = 'log_ref_jenis_alat';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here


    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
