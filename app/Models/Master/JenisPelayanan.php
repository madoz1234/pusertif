<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\JenisPengujian;

class JenisPelayanan extends Model
{
    /* default */
    protected $table 		= 'ref_jenis_pelayanan';
    protected $fillable 	= ['nama'];

    /* data ke log */
    protected $log_table    = 'log_ref_jenis_pelayanan';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function lingkup(){
        return $this->hasMany(Lingkup::class, 'jenis_pelayanan_id' , 'id');
    }

    public function pengujian(){
        return $this->hasManyThrough(JenisPengujian::class, Lingkup::class, 'jenis_pelayanan_id' , 'lingkup_id');
    }

    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    public function hitung(){
        return $this->pengujian->count() + $this->lingkup()->doesntHave('pengujian')->count();
    }
    // insert code here    
}
