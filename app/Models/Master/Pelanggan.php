<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Authentication\User;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\Users;
use App\Models\Files;
use App\Models\Master\Perusahaan;

class Pelanggan extends Model
{
    /* default */
    protected $table        = 'ref_pelanggan';
    protected $fillable     = ['user_id','perusahaan_id','tipe_customer','nama_lengkap','no_hp','email','no_tlp','alamat','status','nip', 'keterangan'];

    /* data ke log */
    protected $log_table    = 'log_ref_pelanggan';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here


    public function perusahaan()
    {
        return $this->belongsTo(Perusahaan::class, 'perusahaan_id');
    }

    public function pp(){
        return $this->hasMany(PendaftaranPengujian::class, 'user_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function users(){
        return $this->belongsTo(Users::class, 'user_id');
    }
    
    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
    public function filesMorphClass()
    {
        return 'aktivasi-user';
    }
}
