<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\Lingkup;
use App\Models\Attachments;
use App\Models\FrontEnd\PendaftaranPengujianDetail;
use App\Models\OrderUji\UjiSerahTerimaDetail;

class JenisPengujian extends Model
{
    /* default */
    protected $table 		= 'ref_jenis_pengujian';
    protected $fillable 	= ['lingkup_id','nama'];

    /* data ke log */
    protected $log_table    = 'log_ref_jenis_pengujian';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function lingkup(){
        return $this->belongsTo(Lingkup::class, 'lingkup_id');
    }

    public function ppdetail(){
        return $this->hasMany(PendaftaranPengujianDetail::class, 'jenis_id' , 'id');
    }

    public function ujiserahterimadetail(){
        return $this->hasMany(UjiSerahTerimaDetail::class, 'jenis_id' , 'id');
    }
   
    public function filesMorphClass()
    {
        return 'jenis-pengujian';
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }
}
