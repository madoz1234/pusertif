<?php
namespace App\Models\Master;

use App\Models\Model;

class MataUji extends Model
{
    /* default */
    protected $table 		= 'ref_mata_uji';
    protected $fillable 	= ['nama_mata_uji'];

    /* data ke log */
    protected $log_table    = 'log_ref_mata_uji';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here


    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
