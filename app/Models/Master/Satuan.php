<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\FrontEnd\PendaftaranPengujianDetail;
use App\Models\OrderUji\UjiSerahTerimaDetail;

class Satuan extends Model
{
    /* default */
    protected $table 		= 'ref_satuan';
    protected $fillable 	= ['satuan'];

    /* data ke log */
    protected $log_table    = 'log_ref_satuan';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function ppdetail(){
        return $this->hasMany(PendaftaranPengujianDetail::class, 'satuan_id' , 'id');
    }

    public function ujiserahterimadetail(){
        return $this->hasMany(UjiSerahTerimaDetail::class, 'satuan_id' , 'id');
    }
    // insert code here


    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
