<?php
namespace App\Models\Master;

use App\Models\Model;

class SurveiDetail extends Model
{
    /* default */
    protected $table 		= 'ref_survei_detail';
    protected $fillable 	= ['survei_id','pertanyaan'];

    /* data ke log */
    protected $log_table    = 'log_ref_survei_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function survei(){
        return $this->belongsTo(Survei::class, 'survei_id', 'id');
    }

    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
