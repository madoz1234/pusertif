<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\FrontEnd\TransSurvei;

class Survei extends Model
{
    /* default */
    protected $table 		= 'ref_survei';
    protected $fillable 	= ['nama','deskripsi','status'];

    /* data ke log */
    protected $log_table    = 'log_ref_survei';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function detail(){
        return $this->hasMany(SurveiDetail::class, 'survei_id' , 'id');
    }

    public function transsurvei(){
        return $this->hasMany(TransSurvei::class, 'survei_id' , 'id');
    }

    public static function changeStatus()
    {
        $count = static::select('*')->count();

        $record = static::where('status', 1)->first();
        if ($count != 1) {
            if ($record) {
                $record->status = !$record->status;
                $record->save();
            }
        }

        return $record;
    }
}
