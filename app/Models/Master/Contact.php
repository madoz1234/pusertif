<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\Pelanggan;

class Contact extends Model
{
    /* default */
    protected $table 		= 'ref_contact';
    protected $fillable 	= ['no_tlp','wa','status'];

    /* data ke log */
    protected $log_table    = 'log_ref_contact';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here


    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
