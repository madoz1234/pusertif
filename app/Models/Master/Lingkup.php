<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\JenisPelayanan;
use App\Models\Master\JenisPengujian;
use App\Models\Attachments;
use App\Models\OrderUji\UjiSerahTerima;
use App\Models\FrontEnd\PendaftaranPengujian;

class Lingkup extends Model
{
    /* default */
    protected $table 		= 'ref_lingkup';
    protected $fillable 	= ['jenis_pelayanan_id','nama'];

    /* data ke log */
    protected $log_table    = 'log_ref_lingkup';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function pelayanan(){
        return $this->belongsTo(JenisPelayanan::class, 'jenis_pelayanan_id');
    }

    public function pengujian(){
        return $this->hasMany(JenisPengujian::class, 'lingkup_id' , 'id');
    }

    public function ujiserahterima(){
        return $this->hasMany(UjiSerahTerima::class, 'lingkup_id' , 'id');
    }

    public function pp(){
        return $this->hasMany(PendaftaranPengujian::class, 'lingkup_id' , 'id');
    }
   
    public function filesMorphClass()
    {
        return 'lingkup';
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }
}
