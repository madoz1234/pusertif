<?php

namespace App\Models\VirtualAccount;

use App\Models\Model;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\VirtualAccount\RealisasiBiaya;
use App\Models\VirtualAccount\PengembalianDana;
use App\Models\PenerimaanBarang\PenerimaanBarang;
use App\Models\Authentication\User;

use Carbon;

class KonfirmasiPembayaran extends Model
{
      /* default */
    protected $table 		= 'trans_konfirmasi_pembayaran';
    protected $fillable 	= [
        'va_id',
        'dana_masuk',
        'status', //  0:Belum Bayar,1:Kurang Bayar,2:Lebih Bayar,3:Lunas 
        'status_dokumen',
        'catatan_dokumen',
        'wbs_io',
        'catatan'
    ];

    /* data ke log */
    protected $log_table    = 'log_trans_konfirmasi_pembayaran';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function va(){
        return $this->belongsTo(VirtualAccount::class, 'va_id');
    }

    public function realisasi(){
        return $this->hasOne(RealisasiBiaya::class, 'konfirmasi_id' , 'id');
    }

    public function pengembalian()
    {
        return $this->hasOne(PengembalianDana::class, 'konfirmasi_id', 'id');
    }

    public function penerimaan()
    {
        return $this->hasOne(PenerimaanBarang::class, 'konfirmasi_id', 'id');
    }

    public function penerima(){
        return $this->belongsTo(User::class, 'created_by');
    }
}
