<?php

namespace App\Models\VirtualAccount;

use App\Models\Model;
use App\Models\VirtualAccount\RealisasiBiaya;
use App\Models\Attachments;
use App\Models\Files;
use Illuminate\Support\Facades\DB;

use Carbon;

class RealisasiDetail extends Model
{
      /* default */
    protected $table 		= 'trans_realisasi_biaya_detail';
    protected $fillable 	= [
        'realisasi_id',
        'periode',
        'jumlah',
        'file_upload',
        'path',
        'tgl_upload',
        'start',
        'end',
    ];
    protected $appends = [
        'attachment_file',
    ];

    protected $log_table    = 'log_trans_realisasi_biaya_detail';
    protected $log_table_fk = 'ref_id';

    /* relation */
    public function realisasi(){
        return $this->belongsTo(RealisasiBiaya::class, 'realisasi_id');
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }
    public function getAttachmentFileAttribute()
    {
        return url('storage/'.$this->path);
    }
}
