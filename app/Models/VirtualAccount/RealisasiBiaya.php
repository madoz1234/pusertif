<?php

namespace App\Models\VirtualAccount;

use App\Models\Model;
use App\Models\VirtualAccount\KonfirmasiPembayaran;
use App\Models\VirtualAccount\RealisasiDetail;
use Illuminate\Support\Facades\DB;
use App\Models\Picture;
use App\Models\Files;

use Carbon;

class RealisasiBiaya extends Model
{
      /* default */
    protected $table 		= 'trans_realisasi_biaya';
    protected $fillable 	= [
        'konfirmasi_id',
        'status', //  0:Belum Bayar,1:Kurang Bayar,2:Lebih Bayar,3:Lunas 
    ];

    protected $log_table    = 'log_trans_realisasi_biaya';
    protected $log_table_fk = 'ref_id';

    /* relation */
    public function realisasi_detail(){
        return $this->hasMany(RealisasiDetail::class, 'realisasi_id' , 'id');
    }

    public function konfirmasi(){
        return $this->belongsTo(KonfirmasiPembayaran::class, 'konfirmasi_id');
    }

    public function filesMorphClass()
    {
        return 'realisasi-biaya';
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }

    public function saveDetail($detail, $nominal)
    {
    	if($detail)
    	{
	        $path 							= $detail['data_upload']->store('realisasi-biaya', 'public');
	        $nama 							= $detail['data_upload']->getClientOriginalName();
    		$nilai 							= decimal_for_save($nominal);
    		$realisasi_detail 				= new RealisasiDetail;
    		$realisasi_detail->periode 		= $detail['periode'];
            $realisasi_detail->start        = $detail['start'];
            $realisasi_detail->end          = $detail['end'];
	        $realisasi_detail->jumlah 		= (int)$nilai;
    		$realisasi_detail->file_upload 	= $nama;
    		$realisasi_detail->path 		= $path;
	        $realisasi_detail->tgl_upload 	= date('Y-m-d');
            $this->realisasi_detail()->save($realisasi_detail);
    	}
    }
}
