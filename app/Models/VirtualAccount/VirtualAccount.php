<?php

namespace App\Models\VirtualAccount;

use App\Models\Model;
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\Authentication\User;
use Carbon;

class VirtualAccount extends Model
{
      /* default */
    protected $table 		= 'trans_va';
    protected $fillable 	= [
        'surat_id',
        'no_va',
        'tipe_customer',
        'wbs_io',
        'tgl_aktif',
        'tgl_kadaluarsa',
        'nominal',
        'status', // Status 0:default, 1:Aktif, 2:Tidak Aktif, 3:Terbayar,
        'bukti_url',
        'bukti_filename',
        'tgl_bayar',
        'download_by',
        'download_date',
        'parent_id'
    ];
    protected $appends = [
        'attachment_bukti',
    ];
    /* data ke log */
    protected $log_table    = 'log_trans_va';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function surat(){
        return $this->belongsTo(SuratPenawaran::class, 'surat_id');
    }

    public function konfirmasi(){
        return $this->hasOne(KonfirmasiPembayaran::class, 'va_id' , 'id');
    }

    public function groups(){
        return $this->hasMany(VirtualAccount::class, 'parent_id' , 'id');
    }

    public function penerima(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public static function setDownloaded($va_id)
    {
        $record = static::find($va_id);
        if ($record) {
            $record->download_by = auth()->user()->id;
            $record->download_date = now();
            $record->status = 1; // sementara
            $record->save();
        }

        return $record;
    }

    public function hapus()
    {
        unlink(public_path('storage/'.$this->path));
        parent::delete();
    }

    public function getAttachmentBuktiAttribute()
    {
        return url('storage/'.$this->bukti_url);
    }
    // public static function generateVa($no)
    // {
    //     $prefix = '88480'.$no;

    //     $rcd = static::where('no_va', 'like', $prefix . '%')->orderBy('no_va', 'desc')->first();
    //     $nomor = 1;
    //     if ($rcd) {
    //         $nomor = (int) substr($rcd->no_va, -3);
    //         $nomor++;
    //     }

    //     return $prefix . str_pad($nomor, 3, "0", STR_PAD_LEFT);
    // }

    // public function kaji_ulang()
    // {
    //     return $this->belongsTo(KajiUlang::class, 'pp_id');
    // }
}
