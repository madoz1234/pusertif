<?php

namespace App\Models\VirtualAccount;

use App\Models\Model;
use App\Models\VirtualAccount\KonfirmasiPembayaran;

use Carbon;

class PengembalianDana extends Model
{
      /* default */
    protected $table 		= 'trans_pengembalian_dana';
    protected $fillable 	= [
        'konfirmasi_id',
        'nota_batal',
        'nota_kembali',
        'jumlah_dana',
        'tgl_kembali',
    ];

    protected $log_table    = 'log_trans_pengembalian_dana';
    protected $log_table_fk = 'ref_id';

    /* relation */
    public function konfirmasi()
    {
        return $this->belongsTo(KonfirmasiPembayaran::class, 'konfirmasi_id');
    }
}
