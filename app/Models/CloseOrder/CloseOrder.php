<?php
namespace App\Models\CloseOrder;

use App\Models\Model;

use App\Models\Attachments;
use App\Models\Files;
use App\Models\Authentication\User;
use App\Models\CloseOrder\CloseOrderDetail;
use App\Models\PenerimaanBarang\PenerimaanBarang;
use App\Models\PenerimaanBarang\PenerimaanBarangDetail;
use App\Models\PengembalianDanaBarang\PengembalianDanaBarang;

class CloseOrder extends Model
{
    /* default */
    protected $table 		= 'trans_close_order';
    protected $fillable 	= ['penerimaan_id','no_nota','tgl_nota','status'];

    protected $log_table    = 'log_trans_close_order';
    protected $log_table_fk = 'ref_id';

    public function penerimaan(){
        return $this->belongsTo(PenerimaanBarang::class, 'penerimaan_id' , 'id');
    }

    public function detailclose(){
        return $this->hasMany(CloseOrderDetail::class, 'close_id' , 'id');
    }

    public function pengembaliandana(){
        return $this->hasOne(PengembalianDanaBarang::class, 'close_id');
    }

    public function pengirim(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function saveDetail($id)
    {
    	if($id)
    	{
			$cari = CloseOrderDetail::get();
    		if($cari->count() > 0){
	    		$group = ($cari->max('group_id') + 1);
    		}else{
    			$group = 1;
    		}
    		$cari 						= PenerimaanBarang::find($id);
    		foreach ($cari->detail_penerimaan_barang->where('flag', 0) as $key => $value) {
		    		$close_detail 							= new CloseOrderDetail;
		    		$close_detail->penerimaan_detail_id 	= $value->id;
		    		$close_detail->group_id 				= $group;
		            $this->detailclose()->save($close_detail);
    		}
    	}
    }

    public function filesMorphClass()
    {
        return 'laporan-pengujian';
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }
}
