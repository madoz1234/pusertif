<?php
namespace App\Models\CloseOrder;

use App\Models\Model;

use App\Models\Authentication\User;
use App\Models\CloseOrder\CloseOrder;
use App\Models\PenerimaanBarang\PenerimaanBarangDetail;

class CloseOrderDetail extends Model
{
    /* default */
    protected $table 		= 'trans_close_order_detail';
    protected $fillable 	= ['close_id','penerimaan_detail_id','group_id'];

    protected $log_table    = 'log_trans_close_order_detail';
    protected $log_table_fk = 'ref_id';

    public function closeorder(){
        return $this->belongsTo(CloseOrder::class, 'close_id', 'id');
    }

    public function detail_penerimaan(){
        return $this->belongsTo(PenerimaanBarangDetail::class, 'penerimaan_detail_id', 'id');
    }
}
