<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Carbon;
use Schema;
use Lang;
use Session;
use App\Models\Translators;
use Auth;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        

        setlocale(LC_ALL, env('APP_LOCALE', 'id'));
        Carbon::setLocale('id');
        Schema::defaultStringLength(191); //191

        Relation::morphMap([
              //Providers
              'pendaftaran-pengujian' 	=> 'App\Models\FrontEnd\PendaftaranPengujian',
              'pengujian'			 	=> 'App\Models\Pengujian\Pengujian',
              'briefing-teknis' 		=> 'App\Models\KajiUlang\KajiUlang',
              'surat-penawaran' 		=> 'App\Models\SuratPenawaran\SuratPenawaran',
              'jenis-pengujian' 		=> 'App\Models\Master\JenisPengujian',
              'aktivasi-user' 			=> 'App\Models\Master\Pelanggan',
        ]);

        $this->app->singleton('counter', function ($app) {
		    return \App\Libraries\Helpers::counter();
		});
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
