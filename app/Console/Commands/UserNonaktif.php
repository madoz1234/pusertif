<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

/* model */
use App\Models\Authentication\User;
use App\Models\Master\Pelanggan;

use Carbon\Carbon;

class UserNonaktif extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:nonaktif';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'User yang telah 3 bulan tidak login di nonaktifkan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Proses Checking User...');
        
        $now      = Carbon::now()->subMonths(3);
         // get all user
        $all_user = User::with('pelanggan')->where('last_login', '<=', $now)->where('status', 1)->get();

        if ($all_user->count() > 0) {
            $this->info('Jumlah User yang ditemukan tidak aktif lebih dari 3 bulan : '.$all_user->count().' User');
            $this->info('Proses Nonaktif User...');
            foreach ($all_user as $key => $user) {
                $record = User::with('pelanggan')->find($user->id);
                // dd($record->pelanggan);
                // dd($record->toArray()['pelanggan']);
                if ($record) {
                    $record->status = 0; //nonaktif
                    $record->save();

                    if (isset($record->toArray()['pelanggan'])) {
                        $pelanggan = Pelanggan::find($record->toArray()['pelanggan']['id']);
                        $pelanggan->status = 1; //nonaktif
                        $pelanggan->save();
                    }

                    $this->info('Telah Dinonaktifkan User Dengan Email :'.$record->email);
                }
            }

            $this->info('Update User Done.'); # code...
        }else{
            $this->info('Tidak ditemukan user yang nonaktif lebih dari 3 bulan.');
        }
        
    }
}
