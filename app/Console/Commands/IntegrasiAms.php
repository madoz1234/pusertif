<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\FrontEnd\PendaftaranPengujianDetail;
use App\Models\Master\Perusahaan;
use Carbon\Carbon;


use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class IntegrasiAms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:ams';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Data From API for AMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client(['base_uri' => 'http://202.162.216.68:9000/']);
        $res = $client->request('GET', 'api_ams/');
        $data = json_decode($res->getBody()->getContents());
        foreach ($data->data as $ams) {
            $cek = PendaftaranPengujian::where('no_surat',$ams->no_surat)->first();
            if(!$cek){
                $pp = new PendaftaranPengujian;
                $pp->no_order = $ams->no_surat;
                $pp->tgl_order = $ams->tgl_terima;
                $pp->user_id = 1;
                $pp->layanan_id = 1;
                $pp->lingkup_id = 1;
                $pp->status = 1;
                $pp->bukti = 'null';
                $pp->filename = 'null';
                $pp->no_surat = $ams->no_surat;
                $pp->tgl_surat = $ams->tgl_surat;
                $pp->tipe = 2;
                $pp->catatan = $ams->perihal;
                $pp->kelas = 1;
                $pp->rencana = Carbon::now()->addMonths(5)->format('Y-m');
                $pp->nomor = 0;
                $pp->status_ams = 0;
                $pp->save();
            }
        }

        return $this->info('Get Data From API for AMS DONE');    
    }
}
