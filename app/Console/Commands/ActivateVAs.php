<?php

namespace App\Console\Commands;

use App\Models\VirtualAccount\VirtualAccount;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\Act\ActDisposisi;

class ActivateVAs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'activate:va';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check and activate downloaded VAs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $due = Carbon::now()->subDay();
        $vas = VirtualAccount::whereNotNull('download_date')->whereIn('status', [0,2]);


        if($vas->count() > 0){
            $data_va = $vas->get();
            foreach ($data_va as $key => $va) {
	            $act = new ActDisposisi;
	            $act->parent_id = $va->surat->kaji_ulang->pp->id;
	            $act->pengirim_id = 1;
	            $act->penerima_id = 1;
	            $act->tipe = 7;
	            $act->jenis = 6;
	            $act->keterangan = 'Virtual Account : <a>'.$va->no_va.'</a> berhasil <a>Teraktivasi</a>, saat ini surat masuk ke tahap <a> Konfirmasi Pembayaran</a>';
	            $act->save();
            }
            $vas->update(['status' => 1, 'updated_at' => Carbon::now()]);
            return $this->info('Downloaded VA from last 24 hour succesfully activated');
        }else{
            return $this->info('Downloaded VA from last 24 hour Not Found');
        }
    }
}
