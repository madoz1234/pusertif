<?php

namespace App\Console\Commands;

use App\Models\VirtualAccount\VirtualAccount;
use Carbon\Carbon;
use Illuminate\Console\Command;
use DB;

class VaNonaktif extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nonactive:va';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check expired date and nonactivate VAs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $due = Carbon::now()->format("Y-m-d");
        $vas = VirtualAccount::where(\DB::raw('cast(tgl_kadaluarsa as date)'),'<=', $due)
                             ->where('status', 1);

        if($vas->count() > 0){
            $vas->update(['status' => 2, 'updated_at' => Carbon::now()]);
            return $this->info('VA Expired succesfully Non-activate');
        }else{
            return $this->info('VA Expired Not Found');
        }
    }
}
