<?php

namespace App\Http\Requests\VirtualAccount;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class AktivasiUlangRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'tgl_aktif'             	=> 'required',
            'tgl_kadaluarsa'            => 'required',
        ];

		return $return;
    }

    public function messages()
    {
    	return [
         'tgl_aktif.required' 			=> 'Data Tanggal Aktif tidak boleh kosong',
         'tgl_kadaluarsa.required' 		=> 'Data Tanggal Kadaluarsa tidak boleh kosong',
       ];
    }
}
