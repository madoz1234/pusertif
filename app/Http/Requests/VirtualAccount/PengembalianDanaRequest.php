<?php

namespace App\Http\Requests\VirtualAccount;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class PengembalianDanaRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        if($input['cek'] == 0){
            $return = [
                 'nota_batal' => 'required',
                 'nota_kembali' => 'required',
                 'jumlah_dana' => 'required',
                // 'tgl_kembali' => 'required',
            ];
        }else{
            $return = [
                'tgl_kembali' => 'required',
            ];
        }

        return $return;
    }

    public function messages()
    {
    	return [
         'nota_batal.required' 		=> 'Nota Dinas Pembatalan tidak boleh kosong',
         'nota_kembali.required'    => 'Nota Dinas Pengembalian Dana tidak boleh kosong',
         'jumlah_dana.required'     => 'Jumlah Pengembalian Dana tidak boleh kosong',
         'tgl_kembali.required' 	=> 'Tanggal Pengembalian Dana tidak boleh kosong',
       ];
    }
}
