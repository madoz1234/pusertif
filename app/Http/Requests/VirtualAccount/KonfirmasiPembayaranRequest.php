<?php

namespace App\Http\Requests\VirtualAccount;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class KonfirmasiPembayaranRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$input = $this->all();
        if ($input['status'] == 0) {
        	$return = [
                'status'             	=> 'required',
            ];
        }elseif($input['status'] == 1){
            $return = [
                'status'              => 'required',
                'dana_masuk'          => 'required',
                'status_kelengkapan'  => 'required',
                'catatan_kelengkapan' => 'required',
                'catatan'             => 'required',
                'wbs_io'              => 'required',
            ];
        }elseif($input['status'] == 2){
            $return = [
                'status'              => 'required',
                'dana_masuk'          => 'required',
                'catatan'             => 'required',
                'wbs_io'              => 'required',
            ];
        }else{
        	$return = [
                'status'             	=> 'required',
                'dana_masuk'            => 'required',
                'catatan' 			  	=> 'required',
                'wbs_io'                => 'required',
            ];
        }


		return $return;
    }

    public function messages()
    {
    	return [
         'status.required' 					=> 'Data Status tidak boleh kosong',
         'status_kelengkapan.required'      => 'Data Status Kelengkapan Dokumen tidak boleh kosong',
         'catatan_kelengkapan.required'     => 'Data Catatan Kelengkapan Dokumen tidak boleh kosong',
         'catatan.required'     			=> 'Data Catatan Dokumen tidak boleh kosong',
         'wbs_io.required' 					=> 'Data No WBS / IO tidak boleh kosong',
         'dana_masuk.required'              => 'Data Dana Masuk tidak boleh kosong',
         'wbs_io.unique' 					=> 'Data No WBS / IO tidak boleh sama',
       ];
    }
}
