<?php

namespace App\Http\Requests\VirtualAccount;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class DanaBarangRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        $return = [
             'no_nota' 		=> 'required',
             'tgl_nota' 	=> 'required',
             'dana_kembali' => 'required',
        ];
       
        return $return;
    }

    public function messages()
    {
    	return [
         'no_nota.required' 		=> 'Nota Dinas Pengembalian Dana tidak boleh kosong',
         'tgl_nota.required'    	=> 'Tanggal Nota Pengembalian Dana tidak boleh kosong',
         'dana_kembali.required'    => 'Dana Kembali Pengembalian Dana tidak boleh kosong',
       ];
    }
}
