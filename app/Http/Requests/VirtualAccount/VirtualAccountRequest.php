<?php

namespace App\Http\Requests\VirtualAccount;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class VirtualAccountRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'kaji_ulang'             	=> 'required',
            'no_va'            			=> 'required|unique:trans_va,no_va,'.$this->get('id'),
            // 'wbs_io'             		=> 'required|unique:trans_va,wbs_io,'.$this->get('id'),
        ];

		return $return;
    }

    public function messages()
    {
    	return [
         'kaji_ulang.required' 				=> 'Data No Permintaan tidak boleh kosong',
         'no_va.required' 					=> 'Data No Virtual Account tidak boleh kosong',
         'no_va.unique' 					=> 'Data No Virtual Account tidak boleh sama',
         'wbs_io.required' 					=> 'Data No WBS / IO tidak boleh kosong',
         // 'wbs_io.unique' 					=> 'Data No WBS / IO tidak boleh sama',
       ];
    }
}
