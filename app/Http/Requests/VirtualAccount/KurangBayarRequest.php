<?php

namespace App\Http\Requests\VirtualAccount;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class KurangBayarRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$input = $this->all();
        $return = [
            'status'              	=> 'required',
            'dana_kurang'          	=> 'required',
            'catatan' 			  	=> 'required',
        ];

		return $return;
    }

    public function messages()
    {
    	return [
         'status.required' 						=> 'Data Status tidak boleh kosong',
         'catatan.required'     				=> 'Data Catatan tidak boleh kosong',
         'dana_kurang.required'              	=> 'Data Dana Kurang tidak boleh kosong',
       ];
    }
}
