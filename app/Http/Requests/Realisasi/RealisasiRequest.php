<?php

namespace App\Http\Requests\Realisasi;

use App\Http\Requests\Request;

class RealisasiRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'periode'            				=> 'required',
            'data_upload'            			=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        'periode.required'            			=> 'Data Periode tidak boleh kosong',
        'data_upload.required'            		=> 'File Realisasi Biaya tidak boleh kosong',
       ];
    }
}
