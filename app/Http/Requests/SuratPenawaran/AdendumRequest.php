<?php

namespace App\Http\Requests\SuratPenawaran;

use App\Http\Requests\Request;

class AdendumRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	if($input['tipe_customer'] !== 0){
    		$return = [
	            'data.*.tgl_surat'            			=> 'required',
	            // 'jadwal_start'           		=> 'required',
	            // 'jadwal_end'      				=> 'required',
	            'data.*.detail.*.item'          		=> 'required',
	            'data.*.detail.*.nominal'          	=> 'required',
	        ];
    	}else{
	    	$return = [
	            'data.*.tgl_surat'            			=> 'required',
	            // 'jadwal_start'           		=> 'required',
	            // 'jadwal_end'      				=> 'required',
	            'data.*.detail.*.item'          		=> 'required',
	            'data.*.detail.*.nominal'          	=> 'required',
	            'data.*.no_skki'          				=> 'required',
	            'data.*.tgl_skki'          			=> 'required',
	        ];
    	}

		return $return;
    }

    public function messages()
    {
    	return [
        'data.*.adendum.required'            	=> 'Data Adendum tidak boleh kosong',
        'data.*.no_skki.required'            	=> 'Data No SKKI/SKKO/PRK tidak boleh kosong',
        'data.*.tgl_skki.required'            	=> 'Data Tanggal SKKI/SKKO/PRK tidak boleh kosong',
        'data.*.tgl_surat.required'            => 'Data Tanggal Surat tidak boleh kosong',
        // 'pph.required'            		=> 'Data PPH tidak boleh kosong',
        // 'wbs_io.required'            	=> 'Data No WBS / IO tidak boleh kosong',
        // 'wbs_io.unique'            		=> 'Data No WBS / IO tidak boleh sama',
        // 'jadwal_start.required'         => 'Data Jadwal Start tidak boleh kosong',
        // 'jadwal_end.required'          	=> 'Data Jadwal End tidak boleh kosong',
        'data.*.detail.*.item.required'        => 'Data Uraian tidak boleh kosong',
        'data.*.detail.*.nominal.required'     => 'Data Nominal tidak boleh kosong',
       ];
    }
}
