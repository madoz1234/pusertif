<?php

namespace App\Http\Requests\SuratPenawaran;

use App\Http\Requests\Request;

class PenolakanRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
		$return = [
            'bukti'             		    => 'required|max:5120',
        ];

		return $return;
    }

    public function messages()
    {
    	return [
        'bukti.required'            		=> 'Data Surat Penolakan tidak boleh kosong',
        'bukti.max' 		   				=> 'Max File Upload 5Mb (5120Kb)',
       ];
    }
}
