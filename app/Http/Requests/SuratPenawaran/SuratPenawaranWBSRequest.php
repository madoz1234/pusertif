<?php

namespace App\Http\Requests\SuratPenawaran;

use App\Http\Requests\Request;

class SuratPenawaranWBSRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'detail.*.wbs_io'            	=> 'required|distinct|unique:trans_surat_penawaran,wbs_io,'.$this->get('id'),
        ];

		return $return;
    }

    public function messages()
    {
    	return [
        'detail.*.wbs_io.required'          => 'Data WBS / IO tidak boleh kosong',
        'detail.*.wbs_io.unique'          	=> 'Data WBS / IO tidak boleh sama',
        'detail.*.wbs_io.distinct'          => 'Data WBS / IO tidak boleh sama',
       ];
    }
}
