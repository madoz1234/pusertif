<?php

namespace App\Http\Requests\SuratPenawaran;

use App\Http\Requests\Request;

class SuratPenawaranRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	if($input['tipe_customer'] !== 0){
    		$return = [
	            'no_surat'            			=> 'required|unique:trans_surat_penawaran,no_surat,'.$this->get('id'),
	            'tgl_surat'            			=> 'required',
	            // 'jadwal_start'           		=> 'required',
	            // 'jadwal_end'      				=> 'required',
	            'detail.*.item'          		=> 'required',
	            'detail.*.nominal'          	=> 'required',
	        ];
    	}else{
	    	$return = [
	            'no_surat'            			=> 'required|unique:trans_surat_penawaran,no_surat,'.$this->get('id'),
	            'tgl_surat'            			=> 'required',
	            // 'jadwal_start'           		=> 'required',
	            // 'jadwal_end'      				=> 'required',
	            'detail.*.item'          		=> 'required',
	            'detail.*.nominal'          	=> 'required',
	            'no_skki'          				=> 'required',
	            'tgl_skki'          			=> 'required',
	        ];
    	}

		return $return;
    }

    public function messages()
    {
    	return [
        'adendum.required'            	=> 'Data Adendum tidak boleh kosong',
        'no_skki.required'            	=> 'Data No SKKI/SKKO/PRK tidak boleh kosong',
        'tgl_skki.required'            	=> 'Data Tanggal SKKI/SKKO/PRK tidak boleh kosong',
        'tgl_surat.required'            => 'Data Tanggal Surat tidak boleh kosong',
        // 'pph.required'            		=> 'Data PPH tidak boleh kosong',
        // 'wbs_io.required'            	=> 'Data No WBS / IO tidak boleh kosong',
        // 'wbs_io.unique'            		=> 'Data No WBS / IO tidak boleh sama',
        'no_surat.required'            	=> 'Data No Surat Penawaran tidak boleh kosong',
        'no_surat.unique'            	=> 'Data No Surat Penawaran tidak boleh sama',
        // 'jadwal_start.required'         => 'Data Jadwal Start tidak boleh kosong',
        // 'jadwal_end.required'          	=> 'Data Jadwal End tidak boleh kosong',
        'detail.*.item.required'        => 'Data Uraian tidak boleh kosong',
        'detail.*.nominal.required'     => 'Data Nominal tidak boleh kosong',
       ];
    }
}
