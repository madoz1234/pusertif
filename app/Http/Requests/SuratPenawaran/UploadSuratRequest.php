<?php

namespace App\Http\Requests\SuratPenawaran;

use App\Http\Requests\Request;

class UploadSuratRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
		$return = [
            'surat_penawaran.*'             		    => 'required|max:1000',
            'surat_penawaran'             				=> 'max:3',
        ];

		return $return;
    }

    public function messages()
    {
    	return [
        'surat_penawaran.*.required'            		=> 'Data Scan Surat Penawaran tidak boleh kosong',
        'surat_penawaran.*.max' 		   				=> 'Max File Upload 1Mb (1024Kb)',
        'surat_penawaran.max' 		   					=> 'Data Lampiran Max 3 File',
       ];
    }
}
