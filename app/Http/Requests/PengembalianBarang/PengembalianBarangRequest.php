<?php

namespace App\Http\Requests\PengembalianBarang;

use App\Http\Requests\Request;

class PengembalianBarangRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'nota_dinas'     => 'required',
            'tgl_nota'       => 'required',
        ];

		return $return;
    }

    public function messages()
    {
    	return [
         'nota_dinas.required' 			=> 'Bukti Penerimaan Barang tidak boleh kosong',
         'tgl_nota.required' 			=> 'Tgl Pengembalian tidak boleh kosong',
       ];
    }
}
