<?php

namespace App\Http\Requests\CloseOrder;

use App\Http\Requests\Request;

class CloseOrderRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'no_nota'            			=> 'required',
            'tgl_nota'            			=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        'no_nota.required'            		=> 'No Nota Dinas Close Order tidak boleh kosong',
        'tgl_nota.required'            		=> 'Tanggal Nota Dinas Close Order tidak boleh kosong',
       ];
    }
}
