<?php

namespace App\Http\Requests\FrontEnd;

use App\Http\Requests\Request;

class ProfileRequest extends Request
{
    
     /* Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // ambil validasi dasar
        // $input = $this->all();
        $rules = [
            'user.username'             => 'required|max:191',
            'pelanggan.nama_lengkap'    => 'required|max:150',
            'pelanggan.no_hp'           => 'required|max:16',
            'pelanggan.email'           => 'required|email|max:150',
        ];

        if(isset($this->photo)){
            $rules['photo'] = 'mimes:jpeg,bmp,png,jpg';
        }

        // if(!$this->get('id') || $this->old_password){
        if(!is_null($this->old_password)){
            $rules['password'] = 'min:6|required_with:confirm_password|same:confirm_password';
            $rules['confirm_password'] = 'min:6';
            // $rules['password'] = 'min:6|required_with:password_confirmation|same:password_confirmation';
            // $rules['password_confirmation'] = 'min:6';
        }

        return $rules;
    }

    public function attributes()
    {
         // ambil validasi dasar
        // $attributes = $this->attr;

        // validasi tambahan
        $attributes['user.username']                = 'Username';
        $attributes['pelanggan.nama_lengkap']       = 'Nama Lengkap';
        $attributes['pelanggan.no_hp']              = 'Nomor Telpon';
        $attributes['pelanggan.email']              = 'Email';
        $attributes['photo']                        = 'Photo';
        $attributes['password']                     = 'Password Baru';
        $attributes['password_confirmation']        = 'Konfirmasi Password';
        return $attributes;
    }
}