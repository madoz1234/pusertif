<?php

namespace App\Http\Requests\FrontEnd;

use App\Http\Requests\Request;

class CekItemUjiRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jenis_id' 		=> 'required',
            'merk' 			=> 'required',
            'tipe' 			=> 'required',
            'jumlah' 		=> 'required',
            'satuan_id' 	=> 'required',
            'spesifikasi' 	=> 'required',
        ];
    }


    public function attributes()
    {
        return [
            'jenis_id' 		=> 'Jenis Pengujian',
            'merk' 			=> 'Merk',
            'tipe' 			=> 'Tipe',
            'jumlah' 		=> 'Jumlah',
            'satuan_id' 	=> 'Satuan',
            'spesifikasi' 	=> 'Spesifikasi',
        ];
    }
}
