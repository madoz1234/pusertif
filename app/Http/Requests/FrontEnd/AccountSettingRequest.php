<?php

namespace App\Http\Requests\FrontEnd;

use App\Http\Requests\Request;

class AccountSettingRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$input = $this->all();
		$return = [
            'email' 					=> 'required|unique:ref_pelanggan,email,'.$this->get('id'),
            'nama' 						=> 'required',
            'no_hp' 					=> 'required',
        ];

		return $return;
    }

    public function messages()
    {
    	return [
         'email.required' 				=> 'Data Email tidak boleh kosong',
         'nama.required' 				=> 'Data Nama tidak boleh kosong',
         'no_hp.required' 				=> 'Data No HP tidak boleh kosong',
       ];
    }
}
