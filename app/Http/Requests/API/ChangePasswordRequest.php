<?php
namespace App\Http\Requests\API;

use App\Http\Requests\Request;

class ChangePasswordRequest extends Request
{

    
     /* Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(!$this->get('id') || $this->old_password){
            $rules['password'] = 'min:2|required_with:password_confirmation|same:password_confirmation';
            $rules['password_confirmation'] = 'min:2';
        }

        return $rules;
    }

    public function attributes()
    {
        // ambil validasi dasar
        // $attributes = $this->attr;

        // validasi tambahan
        // $attributes['username']             = 'Username';
        // $attributes['nama_lengkap']         = 'Nama Lengkap';
        // $attributes['jenis_pelayanan_id']   = 'Jenis Pelayanan';
        // $attributes['nip']                  = 'NIP';
        // $attributes['jabatan']              = 'Jabatan';
        // $attributes['no_tlp']               = 'Nomot Telpon';
        // $attributes['alamat']               = 'Alamat';
        // $attributes['photo']                = 'Photo';
        // $attributes['email']                = 'E-Mail';
        $attributes['password']             = 'Password Baru';
        $attributes['password_confirmation']     = 'Konfirmasi Password';
        return $attributes;
    }

}
