<?php
namespace App\Http\Requests\API;

use App\Http\Requests\Request;

class ProfileRequest extends Request
{

    
     /* Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // ambil validasi dasar
        $rules = [
            'pegawai.nama' => 'required|max:255',
            'pegawai.nip' => 'required|max:255',
            'pegawai.telpon' => 'required|max:12',
            'user.email' => 'required|unique:sys_users,email,'.$this->get('id'),
            'user.username' => 'required|unique:sys_users,username,'.$this->get('id'),
        ];

        if(isset($this->photo)){
            $rules['user.photo'] = 'mimes:jpeg,bmp,png,jpg';
        }

        return $rules;
    }

    public function attributes()
    {
        // ambil validasi dasar
        $attributes = $this->attr;

        // validasi tambahan
        $attributes['user.username']        = 'Username';
        $attributes['user.email']           = 'E-Mail';
        $attributes['user.photo']           = 'Photo';
        $attributes['pegawai.nama']         = 'Nama Lengkap';
        $attributes['pegawai.nip']          = 'NIP';
        $attributes['pegawai.telpon']       = 'Nomot Telpon';
        return $attributes;
    }

}
