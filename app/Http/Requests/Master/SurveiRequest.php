<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class SurveiRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required|max:150|unique:ref_survei,nama,'.$this->get('id'),
            'detail.*.pertanyaan' => 'required',
        ];

    }

    public function attributes()
    {
        return [
            'nama'          => 'Nama',
            'detail.*.pertanyaan' => 'Pertanyaan'
        ];
    }
}
