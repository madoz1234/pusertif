<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class LingkupRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required|max:300|unique:ref_lingkup,nama,'.$this->get('id'),
        ];
    }
}
