<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class DisposisiRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama'          = 'required|max:150|unique:ref_disposisi,nama,'.$this->get('id'),
            'description'   = 'required',
            'hk'            = 'required',
        ];
    }
}
