<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class SatuanRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'satuan' => 'required|max:150|unique:ref_satuan,satuan,'.$this->get('id'),
        ];
    }
}
