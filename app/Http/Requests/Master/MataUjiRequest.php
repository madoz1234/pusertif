<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class MataUjiRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_mata_uji' => 'required|max:150|unique:ref_mata_uji,nama_mata_uji,'.$this->get('id'),
        ];
    }
}
