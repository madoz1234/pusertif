<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class PerusahaanRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'kode' => 'required|max:4|unique:ref_perusahaan,kode,'.$this->get('id'),
            'nama' => 'required|max:150|unique:ref_perusahaan,nama,'.$this->get('id'),
            'alamat' => 'required|max:200|unique:ref_perusahaan,alamat,'.$this->get('id'),
            'no_tlp' => 'required',
            'email' => 'required|email',
            'status' => 'required',
            'kategori' => 'required',
        ];
    }

    public function messages()
    {
        return[
            'nama.required' => 'Nama Perusahaan Harus Di Isi',
            'nama.max' => 'Nama Perusahaan Lebih Dari 150 Karakter',
            'nama.unique' => 'Nama Perusahaan Sudah Digunakan',
            'alamat.required' => 'Alamat Perusahaan Harus Di Isi',
            'alamat.max' => 'Alamat Lebih Dari 200 Karakter',
            'alamat.unique' => 'Alamat Perusahaan Sudah Digunakan',
            'no_tlp.required' => 'Nomer Telpon Harap Di Isi',
            'email.required' => 'Alamat Email Harap Di Isi',
            'email.email' => 'Format Email Tidak Cocok',
            'status.required' => 'Status Harap Di Isi',
            'kategori.required' => 'Kategori Harap Di Isi',
        ];
    }
}
