<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class PrioritasRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jenis_pelayanan_id' => 'required|unique:ref_prioritas,layanan_id,'.$this->get('id'),
        ];
    }
}
