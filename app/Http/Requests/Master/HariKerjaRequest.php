<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class HariKerjaRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'nama' => 'required|max:150|unique:ref_hari_kerja,nama,'.$this->get('id'),
            'hk' => 'required|numeric',
            'jenis_pelayanan_id' => 'required|unique_with:ref_hari_kerja,jenis_pelayanan_id,menu,'.$this->get('id'),
            'menu' => 'required',
        ];

    }

    public function attributes()
    {
        return [
            'nama'          => 'Nama',
            'hk' => 'Hari Kerja',
            'jenis_pelayanan_id' => 'Jenis Pelayanan',
            'menu' => 'Menu',
        ];
    }
}
