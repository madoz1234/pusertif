<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class JenisPengujianRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required|max:150|unique:ref_jenis_pengujian,nama,'.$this->get('id'),
        ];
    }
}
