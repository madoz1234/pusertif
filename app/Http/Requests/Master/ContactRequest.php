<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class ContactRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_tlp' => 'required|max:150|unique:ref_contact,no_tlp,'.$this->get('id'),
            'wa' => 'required|max:150|unique:ref_contact,wa,'.$this->get('id'),
        ];
    }
}
