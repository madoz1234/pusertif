<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class JenisAlatRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_jenis_alat' => 'required|max:150|unique:ref_jenis_alat,nama_jenis_alat,'.$this->get('id'),
        ];
    }
}
