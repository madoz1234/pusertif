<?php

namespace App\Http\Requests\Permintaan;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class PermintaanPengujianRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$input = $this->all();
    	$id = $this->get('id');
    	$return = [
            'detail.*.merk'             	=> 'required',
            'detail.*.tipe'             	=> 'required',
            'detail.*.jumlah'             	=> 'required',
            'detail.*.spesifikasi'          => 'required',
            'jenis_pelayanan_id'            => 'required',
            'lingkup_id'             		=> 'required',
            'kelas'             			=> 'required',
            'rencana'             			=> 'required',
            'user_id'             			=> 'required',
            'tgl_surat'             		=> 'required',
            'no_surat'             			=> 'required',
            'kategori'             			=> 'required',
            'nama_perusahaan'             	=> 'required',
            'surat'             			=> 'required|max:5210',
            'lampiran.*'             		=> 'max:5210',
            'lampiran'             			=> 'max:3',
        ];

		return $return;
    }

    public function messages()
    {
    	return [
         'detail.*.merk.required' 			=> 'Data Merk tidak boleh kosong',
         'detail.*.tipe.required' 			=> 'Data Tipe tidak boleh kosong',
         'detail.*.jumlah.required' 		=> 'Data Jumlah tidak boleh kosong',
         'detail.*.spesifikasi.required' 	=> 'Data Spesifikasi tidak boleh kosong',
         'jenis_pelayanan_id.required' 		=> 'Data Jenis Pelayanan tidak boleh kosong',
         'lingkup_id.required' 				=> 'Data Lingkup tidak boleh kosong',
         'kelas.required' 					=> 'Data Kelas tidak boleh kosong',
         'rencana.required' 				=> 'Data Rencana Pelaksanaan tidak boleh kosong',
         'user_id.required' 				=> 'Data Peminta Jasa tidak boleh kosong',
         'tgl_surat.required' 				=> 'Data Tanggal Surat Permintaan tidak boleh kosong',
         'no_surat.required' 				=> 'Data No Surat Permintaan tidak boleh kosong',
         'kategori.required' 				=> 'Data Kategori tidak boleh kosong',
         'nama_perusahaan.required' 		=> 'Data Nama Perusahaan tidak boleh kosong',
         'surat.required' 					=> 'Data Surat Resmi Perusahaan tidak boleh kosong',
         'surat.max' 						=> 'Max File Upload 5Mb (5210Kb)',
         // 'lampiran.*.required' 				=> 'Data Lampiran tidak boleh kosong',
         'lampiran.*.max' 					=> 'Max File Upload 5Mb (5210Kb)',
         'lampiran.max' 					=> 'Data Lampiran Max 3 File',
       ];
    }
}
