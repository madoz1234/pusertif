<?php

namespace App\Http\Requests\KajiUlang;

use App\Http\Requests\Request;

class DisposisiRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
    	$input = $this->all();
    	if($input['perbaikan'] == 0){
    		$return = [
	            'penerima_id.*' 		=> 'required',
	            'tipe' 					=> 'required',
	        ];
    	}else{
			$return = [
	            'penerima_id.*' 		=> 'required',
	            'tipe' 					=> 'required',
	            'keterangan' 			=> 'required',
	        ];
    	}
		// $return = [
  //           'penerima_id' 		=> 'required',
  //           'tipe' 				=> 'required',
  //           'keterangan' 		=> 'required_if:tipe,1',
  //       ];

		return $return;
    }

    public function messages()
    {
    	return [
         'penerima_id.*.required' 		=> 'Data Penerima tidak boleh kosong',
         'tipe.required' 				=> 'Data Disposisi tidak boleh kosong',
         'keterangan.required_if' 		=> 'Data Keterangan tidak boleh kosong',
       ];
    }
}
