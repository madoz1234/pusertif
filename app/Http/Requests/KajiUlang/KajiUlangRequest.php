<?php

namespace App\Http\Requests\KajiUlang;

use App\Http\Requests\Request;

class KajiUlangRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	if($this->keputusan == 1){
	    	$return = [
	            'detail.*.data.*.lokasi'        => 'required',
	            // 'keterangan'        			=> 'required',
	            'detail.*.data.*.ket_lokasi'    => 'required',
	            'detail.*.mata_uji.*.data'      => 'required',
	            'detail.*.spesifikasi'          => 'required',
	            // 'detail.*.tarif'          		=> 'required',
	            'detail.*.jumlah'          		=> 'required',
	            'detail.*.tentative_start'      => 'required',
	            'detail.*.tentative_end'      	=> 'required',
	            // 'detail.*.tgl_laporan_start'    => 'required',
	            // 'detail.*.tgl_laporan_end'     	=> 'required',
	        ];
    	}else{
    		$return = [];
    	}

		return $return;
    }

    public function messages()
    {
    	return [
        'detail.*.data.*.lokasi.required'       => 'Data Lokasi tidak boleh kosong',
        // 'keterangan.required'       			=> 'Data Keterangan tidak boleh kosong',
        'detail.*.data.*.ket_lokasi.required'   => 'Data Lokasi tidak boleh kosong',
        'detail.*.mata_uji.*.data.required'     => 'Data Mata Uji tidak boleh kosong',
        'detail.*.spesifikasi.required'        	=> 'Data Spesifikasi tidak boleh kosong',
        // 'detail.*.tarif.required'          		=> 'Data Usulan Tarif tidak boleh kosong',
        'detail.*.jumlah.required'          	=> 'Data Jumlah tidak boleh kosong',
        'detail.*.tentative_start.required'     => 'Data Tentative Start tidak boleh kosong',
        'detail.*.tentative_end.required'      	=> 'Data Tentative End tidak boleh kosong',
        // 'detail.*.tgl_laporan_start.required'   => 'Data Tanggal Laporan Start tidak boleh kosong',
        // 'detail.*.tgl_laporan_end.required'     => 'Data Tanggal Laporan End tidak boleh kosong',
       ];
    }
}
