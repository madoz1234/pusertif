<?php
namespace App\Http\Requests\Konfigurasi;

use App\Http\Requests\Request;

class ProfileRequest extends Request
{

    
     /* Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // ambil validasi dasar
        $rules = [
            'username' => 'required|unique:sys_users,username,'.$this->get('id'),
            'nama_lengkap' => 'required|max:255',
            'jenis_pelayanan_id' => 'required',
            'nip' => 'required|max:255',
            'jabatan' => 'required|max:255',
            'no_tlp' => 'required|max:12',
            'email' => 'required|unique:sys_users,email,'.$this->get('id'),
            'alamat' => 'required',
        ];

        if(isset($this->photo)){
            $rules['photo'] = 'mimes:jpeg,bmp,png,jpg';
        }

        if(!$this->get('id') || $this->old_password){
            $rules['password'] = 'min:2|required_with:confirm_password|same:confirm_password';
            $rules['confirm_password'] = 'min:2';
        }

        return $rules;
    }

    public function attributes()
    {
        // ambil validasi dasar
        // $attributes = $this->attr;

        // validasi tambahan
        $attributes['username']             = 'Username';
        $attributes['nama_lengkap']         = 'Nama Lengkap';
        $attributes['jenis_pelayanan_id']   = 'Jenis Pelayanan';
        $attributes['nip']                  = 'NIP';
        $attributes['jabatan']              = 'Jabatan';
        $attributes['no_tlp']               = 'Nomot Telpon';
        $attributes['alamat']               = 'Alamat';
        $attributes['photo']                = 'Photo';
        $attributes['email']                = 'E-Mail';
        $attributes['password']             = 'Password Baru';
        $attributes['confirm_password']     = 'Konfirmasi Password';
        return $attributes;
    }

}
