<?php
namespace App\Http\Requests\Konfigurasi;

use App\Http\Requests\Request;

class UsersRequest extends Request
{

    
     /* Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // ambil validasi dasar
        $input = $this->all();
        $rules = [
            // 'password_lama'    => 'required',
            'username' => 'required|unique:sys_users,username,'.$this->get('id'),
            'nama_lengkap' => 'required|max:50',
            'nip' => 'required|max:255',
            'alamat' => 'required',
            'no_tlp' => 'required|max:12',
            'email' => 'required|email|unique:sys_users,email,'.$this->get('id'),
            'roles.*' => 'required'
        ];

        if(!$this->get('id') || $this->password_lama){
            $rules['password'] = 'min:2|required_with:confirm_password|same:confirm_password';
            $rules['confirm_password'] = 'min:2';
        }

        return $rules;
    }

    public function messages()
    {
    	return [
         'username.required' 			=> 'Username tidak boleh kosong',
         'username.unique' 				=> 'Username sudah ada',
         'nama_lengkap.required' 		=> 'Nama Lengkap tidak boleh kosong',
         'nip.required' 				=> 'NIP tidak boleh kosong',
         'no_tlp.required' 				=> 'No HP tidak boleh kosong',
         'email.required' 				=> 'Email tidak boleh kosong',
         'alamat.required'              => 'Alamat tidak boleh kosong',
         'email.unique' 				=> 'Email sudah ada',
         // 'email.regex' 					=> ' Data Email harus berakhiran @pln.co.id',
         'email.email' 					=> 'Email tidak valid',
         'roles.*.required' 			=> 'Hak Akses tidak boleh kosong',
         'password.required' 			=> 'Password tidak boleh kosong',
         'confirm_password.required' 	=> 'Password Konfirmasi tidak boleh kosong',
         'confirm_password.min' 		=> 'Password Konfirmasi minimal 2 karakter',
         'password.min' 				=> 'Password minimal 2 karakter',
       ];
    }
}


