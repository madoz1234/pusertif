<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AduanRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
    	$input = $this->all();
		$return = [
            'pilihan' 			=> 'required',
            'nama' 				=> 'required',
            'email' 			=> 'required|email',
            'no_hp' 			=> 'required',
            'pesan' 			=> 'required',
        ];

		return $return;
    }

    public function messages()
    {
    	return [
         'pilihan.required' 		=> 'Data Perihal tidak boleh kosong',
         'nama.required' 			=> 'Data Nama tidak boleh kosong',
         'email.required' 			=> 'Data Email tidak boleh kosong',
         'no_hp.required' 			=> 'Data No HP tidak boleh kosong',
         'pesan.required' 			=> 'Data Pesan tidak boleh kosong',
         'email.email' 				=> 'Data Email tidak valid',
       ];
    }
}
