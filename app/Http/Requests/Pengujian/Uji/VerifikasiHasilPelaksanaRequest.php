<?php

namespace App\Http\Requests\Pengujian\Uji;

use App\Http\Requests\Request;

class VerifikasiHasilPelaksanaRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'detail.*.verifikasi'            		=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        'detail.*.verifikasi.required'            => 'Hasil Verifikasi tidak boleh kosong',
       ];
    }
}
