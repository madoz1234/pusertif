<?php

namespace App\Http\Requests\Pengujian\Uji;

use App\Http\Requests\Request;

class HasilRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            // 'keterangan'            				=> 'required',
            'detail.*.status_hasil'            		=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        'detail.*.status_hasil.required'            => 'Hasil Pengujian tidak boleh kosong',
       ];
    }
}
