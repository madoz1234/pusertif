<?php

namespace App\Http\Requests\Pengujian\Uji;

use App\Http\Requests\Request;

class SimpanDataRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            // 'keterangan'            				=> 'required',
            'pilihan'            	=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        'pilihan.required'           => 'Status Laporan tidak boleh kosong',
       ];
    }
}
