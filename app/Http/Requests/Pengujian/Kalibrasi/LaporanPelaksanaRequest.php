<?php

namespace App\Http\Requests\Pengujian\Kalibrasi;

use App\Http\Requests\Request;

class LaporanPelaksanaRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'detail.*.pelaksana'           => 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        'detail.*.pelaksana.required'     => 'Pelaksana tidak boleh kosong',
       ];
    }
}
