<?php

namespace App\Http\Requests\Pengujian\Kalibrasi;

use App\Http\Requests\Request;

class PelaksanaRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'detail.*.fpp_fpk'            		=> 'required',
            'detail.*.pelaksana_leader'         => 'required',
            'detail.*.pelaksana_id.*'           => 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        'detail.*.fpp_fpk.required'            => 'No FPP/FPK tidak boleh kosong',
        'detail.*.pelaksana_leader.required'   => 'Pelaksana Leader tidak boleh kosong',
        'detail.*.pelaksana_id.*.required'     => 'Pelaksana tidak boleh kosong',
       ];
    }
}
