<?php

namespace App\Http\Requests\Pengujian\Kalibrasi;

use App\Http\Requests\Request;

class VerifikasiPelaksanaRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'detail.*.pelaksana_id'            		=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        'detail.*.pelaksana_id.required'            => 'Pelaksana tidak boleh kosong',
       ];
    }
}
