<?php

namespace App\Http\Requests\Pengujian\Kalibrasi;

use App\Http\Requests\Request;

class VerifikasiRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            // 'keterangan'            				=> 'required',
            'penerima_id.*'            				=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        'penerima_id.*.required'            			=> 'Penerima tidak boleh kosong',
        // 'keterangan.required'            			=> 'Keterangan tidak boleh kosong',
       ];
    }
}
