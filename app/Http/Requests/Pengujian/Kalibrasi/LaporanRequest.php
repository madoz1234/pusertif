<?php

namespace App\Http\Requests\Pengujian\Kalibrasi;

use App\Http\Requests\Request;

class LaporanRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'bukti.*'           => 'required|max:1000',
            'no_laporan'        => 'required',
            'tgl_laporan'       => 'required',
            'bukti'             => 'max:3',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        'bukti.*.required'     => 'Bukti tidak boleh kosong',
        'no_laporan.required'  => 'No Laporan tidak boleh kosong',
        'tgl_laporan.required' => 'Tgl Laporan tidak boleh kosong',
        'bukti.*.max' 		   => 'Max File Upload 1Mb (1024Kb)',
        'bukti.max' 		   => 'Data Lampiran Max 3 File',
       ];
    }
}
