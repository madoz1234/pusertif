<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegistrasiRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
    	$input = $this->all();
		$return = [
            'nama_lengkap' 			=> 'required',
            'no_hp' 				=> 'required',
            'no_tlp' 				=> 'required',
            'nama_perusahaan' 		=> 'required',
            'email' 				=> 'required|email|unique:sys_users,username,'.$this->get('id'),
            'email_perusahaan' 		=> 'required|email',
            'alamat' 				=> 'required',
            'tipe_customer' 		=> 'required',
        ];
        if($this->request->get('tipe_customer') == 0)
        {
        	$return['nip'] = 'required';
        	$return['email'] = 'required|regex:/(.*)@pln\.co.id/i';
        }else{

        }

		return $return;
    }

    public function messages()
    {
    	return [
         'nama_lengkap.required' 		=> 'Data Nama tidak boleh kosong',
         'no_hp.required' 				=> 'Data No HP tidak boleh kosong',
         'no_tlp.required' 				=> 'Data Nomor Telepon tidak boleh kosong',
         'nama_perusahaan.required' 	=> 'Data Nama Perusahaan tidak boleh kosong',
         'email.required' 				=> 'Data Email tidak boleh kosong',
         'email.email' 					=> 'Data Email tidak valid',
         'email.unique' 				=> 'Data Email sudah ada',
         'email_perusahaan.required' 	=> 'Data Email Perusahaan tidak boleh kosong',
         'email_perusahaan.email' 		=> 'Data Email Perusahaan tidak valid',
         'alamat.required' 				=> 'Data Alamat tidak boleh kosong',
         'tipe_customer.required' 		=> 'Data Tipe Customer tidak boleh kosong',
         'nip.required' 				=> 'Data NIP tidak boleh kosong',
         'email.regex' 				=> ' Data Email harus berakhiran @pln.co.id',
       ];
    }
}
