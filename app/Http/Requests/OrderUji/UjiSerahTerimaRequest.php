<?php

namespace App\Http\Requests\OrderUji;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class UjiSerahTerimaRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$input = $this->all();
    	$id = $this->get('id');
    	$return = [
            // 'detail.*.merk'        => 'required',
            // 'detail.*.tipe'        => 'required',
            'detail.*.jumlah'      => 'required',
            'detail.*.spesifikasi' => 'required',
            'detail.*.tgl_mulai'   => 'required',
            'detail.*.tgl_selesai' => 'required',
            'jenis_pelayanan_id'   => 'required',
            'lingkup_id'           => 'required',
            'kelas'                => 'required',
            'rencana'              => 'required',
            'user_id'              => 'required',
            'no_kontrak'           => 'required',
            'kategori'             => 'required',
            'nama_perusahaan'      => 'required',
            'no_wbs'               => 'required',
            'tgl_kontrak'          => 'required',
            'surat'                => 'required|max:5210',
            'lampiran.*'           => 'max:5210',
            'lampiran'             => 'max:3',
        ];

		return $return;
    }

    public function messages()
    {
    	return [
         // 'detail.*.merk.required'        => 'Data Merk tidak boleh kosong',
         // 'detail.*.tipe.required'        => 'Data Tipe tidak boleh kosong',
         'detail.*.jumlah.required'      => 'Data Jumlah tidak boleh kosong',
         'detail.*.spesifikasi.required' => 'Data Spesifikasi tidak boleh kosong',
         'detail.*.tgl_mulai.required'   => 'Data Spesifikasi tidak boleh kosong',
         'detail.*.tgl_selesai.required' => 'Data Spesifikasi tidak boleh kosong',
         'jenis_pelayanan_id.required'   => 'Data Jenis Pelayanan tidak boleh kosong',
         'lingkup_id.required'           => 'Data Lingkup tidak boleh kosong',
         'kelas.required'                => 'Data Kelas tidak boleh kosong',
         'rencana.required'              => 'Data Rencana Pelaksanaan tidak boleh kosong',
         'user_id.required'              => 'Data Peminta Jasa tidak boleh kosong',
         'no_kontrak.required'           => 'Data No Surat Permintaan tidak boleh kosong',
         'kategori.required'             => 'Data Kategori tidak boleh kosong',
         'nama_perusahaan.required'      => 'Data Nama Perusahaan tidak boleh kosong',
         'no_wbs.required'               => 'Data No WBS tidak boleh kosong',
         'tgl_kontrak.required'          => 'Data Tgl Kontrak tidak boleh kosong',
         'surat.required' 				 => 'Data Surat Resmi Perusahaan tidak boleh kosong',
         'surat.max' 					 => 'Max File Upload 5Mb (5210Kb)',
         // 'lampiran.*.required' 			 => 'Data Lampiran tidak boleh kosong',
         'lampiran.*.max' 				 => 'Max File Upload 5Mb (5210Kb)',
         'lampiran.max' 				 => 'Data Lampiran Max 3 File',
       ];
    }
}
