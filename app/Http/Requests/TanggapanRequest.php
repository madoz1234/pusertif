<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TanggapanRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
    	$input = $this->all();
		$return = [
            'keterangan' 			=> 'required',
        ];

		return $return;
    }

    public function messages()
    {
    	return [
         'keterangan.required' 		=> 'Data Keterangan tidak boleh kosong',
       ];
    }
}
