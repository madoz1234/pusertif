<?php

namespace App\Http\Requests\Reschedule;

use App\Http\Requests\Request;

class RescheduleRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'detail.*.tentative_start'        => 'required',
            'detail.*.tentative_end'          => 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        'detail.*.tentative_start.required'   => 'Data Tentative Start tidak boleh kosong',
        'detail.*.tentative_end.required'     => 'Data Tentative End tidak boleh kosong',
       ];
    }
}
