<?php

namespace App\Http\Requests\PenerimaanBarang;

use App\Http\Requests\Request;

class PenerimaanBarangRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'data.*.detail.*.nama'     => 'required',
            'data.*.detail.*.seri'     => 'required',
            'pengirim'       		   => 'required',
            'penerima_id'       	   => 'required',
            'tanggal_terima'       	   => 'required',
        ];

		return $return;
    }

    public function messages()
    {
    	return [
         'data.*.detail.*.nama.required' 	=> 'Data Nama Barang tidak boleh kosong',
         'data.*.detail.*.seri.required' 	=> 'Data No Seri tidak boleh kosong',
         'pengirim.required' 				=> 'Data Pengirim tidak boleh kosong',
         'penerima_id.required' 			=> 'Data Penerima tidak boleh kosong',
         'tanggal_terima.required' 			=> 'Data Tanggal Penerimaan tidak boleh kosong',
       ];
    }
}
