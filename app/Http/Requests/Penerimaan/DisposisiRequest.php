<?php

namespace App\Http\Requests\Penerimaan;

use App\Http\Requests\Request;

class DisposisiRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
    	$input = $this->all();
		$return = [
            'penerima_id' 		=> 'required',
            'tipe' 				=> 'required',
            // 'keterangan' 		=> 'required',
        ];

		return $return;
    }

    public function messages()
    {
    	return [
         'penerima_id.required' 		=> 'Data Penerima tidak boleh kosong',
         'tipe.required' 				=> 'Data Disposisi tidak boleh kosong',
         // 'keterangan.required' 			=> 'Data Keterangan tidak boleh kosong',
       ];
    }
}
