<?php

namespace App\Http\Requests\Penerimaan;

use App\Http\Requests\Request;

class PenerimaanRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	if(!is_null($this->example)){
	    	$return = [
	            // 'detail.*.jenis_id'             => 'required',
	            'detail.*.merk'             	=> 'required',
	            'detail.*.tipe'             	=> 'required',
	            'detail.*.jumlah'             	=> 'required',
	            'detail.*.satuan_id'            => 'required',
	            'detail.*.spesifikasi'          => 'required',
	            'jenis_pelayanan_id'            => 'required',
	            'lingkup_id'             		=> 'required',
	        ];
    	}else{
	    	$return = [
	            'detail.*.jenis_id'             => 'required',
	            'detail.*.merk'             	=> 'required',
	            'detail.*.tipe'             	=> 'required',
	            'detail.*.jumlah'             	=> 'required',
	            'detail.*.satuan_id'            => 'required',
	            'detail.*.spesifikasi'          => 'required',
	            'jenis_pelayanan_id'            => 'required',
	            'lingkup_id'             		=> 'required',
	        ];
    	}

		return $return;
    }

    public function messages()
    {
    	return [
         'detail.*.jenis_id.required' 		=> 'Data Jenis Pengujian tidak boleh kosong',
         'detail.*.merk.required' 			=> 'Data Merk tidak boleh kosong',
         'detail.*.tipe.required' 			=> 'Data Tipe tidak boleh kosong',
         'detail.*.jumlah.required' 		=> 'Data Jumlah tidak boleh kosong',
         'detail.*.satuan_id.required' 		=> 'Data Satuan tidak boleh kosong',
         'detail.*.spesifikasi.required' 	=> 'Data Spesifikasi tidak boleh kosong',
         'jenis_pelayanan_id.required' 		=> 'Data Jenis Pelayanan tidak boleh kosong',
         'lingkup_id.required' 				=> 'Data Lingkup tidak boleh kosong',
       ];
    }
}
