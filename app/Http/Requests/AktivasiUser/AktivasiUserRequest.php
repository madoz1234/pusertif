<?php

namespace App\Http\Requests\AktivasiUser;

use App\Http\Requests\Request;

class AktivasiUserRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$input = $this->all();
		$return = [
            'email' 			=> 'required|string|email|max:255|unique:ref_pelanggan,email,'.$this->get('id'),
            'file.*'        	=> 'max:5210',
            'file'          	=> 'max:3',
            'no_hp' 			=> 'required|max:15'
        ];

		return $return;
    }

    public function messages()
    {
    	return [
         'email.required' 			=> 'Data Email tidak boleh kosong',
         'no_hp.required' 			=> 'Data No HP tidak boleh kosong',
         'no_hp.max' 				=> 'Data No HP maks 15 karakter',
         'email.email' 				=> 'Data Email tidak valid',
         'file.*.max'               => 'Max File Upload 5Mb (5210Kb)',
         'file.max' 				=> 'Data File Max 3 File',
       ];
    }
}
