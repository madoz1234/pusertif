<?php

namespace App\Http\Requests\AktivasiUser;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'nama' => 'required',
            // 'username' => 'required|max:255|unique:sys_users,username,'.$this->get('id'),
            'email' => 'required|string|email|max:255|unique:ref_pelanggan,email,'.$this->get('id'),
            'file.*' => 'required|mimes:jpeg,bmp,jpg,pdf',
            // 'no_hp' => ''
            // 'no_tlp'
            // 'roles' => 'required',
            // 'password' => 'required|string|min:6|confirmed',
            // 'tipe_customer' => 'required',
            // 'nama_perusahaan' => 'required',
        ];
    }
}
