<?php

namespace App\Http\Requests\AktivasiUser;

use App\Http\Requests\Request;

class DaftarUserRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$input = $this->all();
    	if($input['tipe_customer'] == 0){
			$return = [
	            'perusahaan_id' 	=> 'required',
	            'nama' 				=> 'required',
	            'email' 			=> 'required|email|regex:/(.*)@pln\.co.id/i|unique:sys_users,username,'.$this->get('id'),
	            'no_hp' 			=> 'required',
	            'alamat' 			=> 'required',
	            'tipe_customer'     => 'required',
	        ];
    	}else{
    		$return = [
	            'perusahaan_id' 	=> 'required',
	            'nama' 				=> 'required',
	            'email' 			=> 'required|email|unique:sys_users,username,'.$this->get('id'),
	            'no_hp' 			=> 'required',
	            'alamat' 			=> 'required',
	            'tipe_customer'     => 'required',
	        ];
    	}

		return $return;
    }

    public function messages()
    {
    	return [
         'perusahaan_id.required' 	=> 'Data Perusahaan tidak boleh kosong',
         'nama.required' 			=> 'Data Nama tidak boleh kosong',
         'tipe_customer.required'   => 'Data Kategori tidak boleh kosong',
         'email.required' 			=> 'Data Email tidak boleh kosong',
         'email.regex' 				=> ' Data Email harus berakhiran @pln.co.id',
         'no_hp.required' 			=> 'Data No HP tidak boleh kosong',
         'alamat.required' 			=> 'Data Alamat tidak boleh kosong',
         'email.email' 				=> 'Data Email tidak valid',
       ];
    }
}
