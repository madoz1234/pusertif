<?php

namespace App\Http\Requests\AktivasiUser;

use App\Http\Requests\Request;

class AktivasiUlangUserRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$input = $this->all();
		$return = [
            'email' => 'required|string|email|max:255|unique:ref_pelanggan,email,'.$this->get('id'),
        ];

		return $return;
    }

    public function messages()
    {
    	return [
         'email.required' 			=> 'Data Email tidak boleh kosong',
         'email.email' 				=> 'Data Email tidak valid',
       ];
    }
}
