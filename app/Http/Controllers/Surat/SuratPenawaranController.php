<?php

namespace App\Http\Controllers\Surat;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\SuratPenawaran\SuratPenawaranRequest;
use App\Http\Requests\SuratPenawaran\PenolakanRequest;
use App\Http\Requests\SuratPenawaran\AdendumRequest;
use App\Http\Requests\SuratPenawaran\UploadSuratRequest;
use App\Http\Requests\SuratPenawaran\SuratPenawaranWBSRequest;
/* Models */
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\SuratPenawaran\SuratPenawaranDetail;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\Act\ActDisposisi;
use App\Models\KajiUlang\KajiUlang;
use App\Models\KajiUlang\Detail;
use App\Models\Authentication\Role;
use App\Models\Authentication\User;
use App\Models\Users;
use App\Models\Picture;
use App\Models\Files;
use App\Models\Master\JenisPelayanan;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Zipper;

class SuratPenawaranController extends Controller
{
    protected $link = 'surat-penawaran/';
    protected $perms = 'surat-penawaran';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Surat Penawaran");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Surat Penawaran' => '#']);

        // Header Grid Datatable
        $this->data = [
            'pageUrl' => $this->link,
        ];
    }

    public function index()
    {	
    	if(auth()->user()->hasRole(['yan-kalibrasi','admin'])){
	    	$records 	= PendaftaranPengujian::isDoneKal();
	    	$kirim 		= PendaftaranPengujian::isKirimKal();
	    	$tolak 		= PendaftaranPengujian::isTolakKal();
	    	$histori 	= PendaftaranPengujian::isHistoriKal();
    	}elseif(auth()->user()->hasRole(['yan-uji','admin'])){
    		$records 	= PendaftaranPengujian::isDoneUji();
	    	$kirim 		= PendaftaranPengujian::isKirimUji();
	    	$tolak 		= PendaftaranPengujian::isTolakUji();
	    	$histori 	= PendaftaranPengujian::isHistoriUji();
    	}else{
    		$records 	= PendaftaranPengujian::where('id', 0)->get();
	    	$kirim 		= PendaftaranPengujian::where('id', 0)->get();
	    	$tolak 		= PendaftaranPengujian::where('id', 0)->get();
	    	$histori 	= PendaftaranPengujian::where('id', 0)->get();
    	}
    	return $this->render('modules.surat.index', [
        	'mockup' => true,
        	'data' => $records,
        	'data_kirim' => $kirim,
        	'data_tolak' => $tolak,
        	'histori' => $histori,
        ]);
    }

    public function indexFilter(Request $request)
    {   
        if(auth()->user()->hasRole(['yan-uji','admin'])){
            $records    = PendaftaranPengujian::when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->where('no_order', 'like','%'.$no_order.'%');
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->where('no_surat', 'like','%'.$no_surat.'%');
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->where('layanan_id',$jenis_pelayanan_id);
                                                })
                                                ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                    $q->whereHas('user', function($user) use ($perusahaan_id){
                                                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                                              $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                        });
                                                    });
                                                })
                                                ->whereHas('kaji_ulang', function($u){
                                                        $u->doesntHave('surat')->where('keputusan', 1);
                                                    })
                                                ->where('layanan_id', '!=', 1)
                                                ->where('tipe_surat', 0)
                                                ->orderBy('tgl_order','desc')->get()->groupBy(['nomor']);


            $kirim      = PendaftaranPengujian::when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->where('no_order', 'like','%'.$no_order.'%');
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->where('no_surat', 'like','%'.$no_surat.'%');
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->where('layanan_id',$jenis_pelayanan_id);
                                                })
                                                ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                    $q->whereHas('user', function($user) use ($perusahaan_id){
                                                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                                              $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                        });
                                                    });
                                                })
                                                ->whereHas('kaji_ulang', function($u){
                                                    $u->whereHas('surat', function($z){
                                                        $z->where('surat_status', 0);
                                                    })->where('keputusan', 1);
                                                })
                                                ->where('layanan_id', '!=', 1)
                                                ->where('tipe_surat', 0)
                                                ->orderBy('tgl_order','desc')->get()->groupBy(['nomor']);


            $tolak      = PendaftaranPengujian::when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->where('no_order', 'like','%'.$no_order.'%');
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->where('no_surat', 'like','%'.$no_surat.'%');
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->where('layanan_id',$jenis_pelayanan_id);
                                                })
                                                ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                    $q->whereHas('user', function($user) use ($perusahaan_id){
                                                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                                              $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                        });
                                                    });
                                                })
                                                ->whereHas('kaji_ulang', function($u){
                                                    $u->where('keputusan', 2);
                                                })
                                                ->where('layanan_id', '!=', 1)
                                                ->where('tipe_surat', 0)
                                                ->orderBy('tgl_order','desc')->get();


            $histori    = PendaftaranPengujian::when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->where('no_order', 'like','%'.$no_order.'%');
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->where('no_surat', 'like','%'.$no_surat.'%');
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->where('layanan_id',$jenis_pelayanan_id);
                                                })
                                                ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                    $q->whereHas('user', function($user) use ($perusahaan_id){
                                                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                                              $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                        });
                                                    });
                                                })
                                                ->whereHas('kaji_ulang', function($u){
                                                        $u->whereHas('surat', function($z){
                                                            $z->where('surat_status', 1);
                                                    })->where('keputusan', 1);
                                                })
                                                ->where('layanan_id', '!=', 1)
                                                ->where('tipe_surat', 0)
                                                ->orderBy('tgl_order','desc')->get()->groupBy(['nomor']);
        }elseif(auth()->user()->hasRole(['yan-kalibrasi','admin'])){
            $records    = PendaftaranPengujian::when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->where('no_order', 'like','%'.$no_order.'%');
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->where('no_surat', 'like','%'.$no_surat.'%');
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->where('layanan_id',$jenis_pelayanan_id);
                                                })
                                                ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                    $q->whereHas('user', function($user) use ($perusahaan_id){
                                                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                                              $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                        });
                                                    });
                                                })
                                                ->whereHas('kaji_ulang', function($u){
                                                        $u->doesntHave('surat')->where('keputusan', 1);
                                                    })
                                                ->where('layanan_id', 1)
                                                ->where('tipe_surat', 0)
                                                ->orderBy('tgl_order','desc')->get()->groupBy(['nomor']);


            $kirim      = PendaftaranPengujian::when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->where('no_order', 'like','%'.$no_order.'%');
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->where('no_surat', 'like','%'.$no_surat.'%');
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->where('layanan_id',$jenis_pelayanan_id);
                                                })
                                                ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                    $q->whereHas('user', function($user) use ($perusahaan_id){
                                                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                                              $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                        });
                                                    });
                                                })
                                                ->whereHas('kaji_ulang', function($u){
                                                    $u->whereHas('surat', function($z){
                                                        $z->where('surat_status', 0);
                                                    })->where('keputusan', 1);
                                                })
                                                ->where('layanan_id', 1)
                                                ->where('tipe_surat', 0)
                                                ->orderBy('tgl_order','desc')->get()->groupBy(['nomor']);


            $tolak      = PendaftaranPengujian::when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->where('no_order', 'like','%'.$no_order.'%');
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->where('no_surat', 'like','%'.$no_surat.'%');
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->where('layanan_id',$jenis_pelayanan_id);
                                                })
                                                ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                    $q->whereHas('user', function($user) use ($perusahaan_id){
                                                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                                              $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                        });
                                                    });
                                                })
                                                ->whereHas('kaji_ulang', function($u){
                                                    $u->where('keputusan', 2);
                                                })
                                                ->where('layanan_id', 1)
                                                ->where('tipe_surat', 0)
                                                ->orderBy('tgl_order','desc')->get();


            $histori    = PendaftaranPengujian::when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->where('no_order', 'like','%'.$no_order.'%');
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->where('no_surat', 'like','%'.$no_surat.'%');
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->where('layanan_id',$jenis_pelayanan_id);
                                                })
                                                ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                    $q->whereHas('user', function($user) use ($perusahaan_id){
                                                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                                              $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                        });
                                                    });
                                                })
                                                ->whereHas('kaji_ulang', function($u){
                                                        $u->whereHas('surat', function($z){
                                                            $z->where('surat_status', 1);
                                                    })->where('keputusan', 1);
                                                })
                                                ->where('layanan_id', 1)
                                                ->where('tipe_surat', 0)
                                                ->orderBy('tgl_order','desc')->get()->groupBy(['nomor']);
        }else{
           $records     = PendaftaranPengujian::where('id', 0)->get();
            $kirim      = PendaftaranPengujian::where('id', 0)->get();
            $tolak      = PendaftaranPengujian::where('id', 0)->get();
            $histori    = PendaftaranPengujian::where('id', 0)->get();
        }
        return $this->render('modules.surat.index', [
            'mockup' => true,
            'data' => $records,
            'data_kirim' => $kirim,
            'data_tolak' => $tolak,
            'histori' => $histori,
            'filter' => $request->all(),
        ]);
    }
    
    public function detail($id, $jenis)
    {
    	$detail = Detail::where('kaji_ulang_id', $id)->where('jenis_id', $jenis)->get();
    	// $pp = PendaftaranPengujian::find($id);
        return $this->render('modules.surat.detail-pengujian',[
        	'record' => $detail,
        	// 'pp' => $pp,
        ]);
    }

    public function buat($id, $arr, $tipe)
    {	
    	$this->setTitle('Buat Surat Penawaran');
        $this->setBreadcrumb(['Surat Penawaran' => '#', 'Buat' => '#']);
		$array = explode(',', $arr);
    	$record = PendaftaranPengujian::whereIn('id', $array)->get();
    	$data_arr = array();
    	$data = '';
    		foreach($record as $value) {
    			$data_arr[]= $value->no_order;
    		}
    	$count = count($data_arr);
    	$i=1;
    		foreach ($data_arr as $key => $val) {
    			if($i == $count){
	    			$data .=$val;
    			}else{
	    			$data .=$val.', ';
    			}
    			$i++;
    		}
    	$this->setSubtitle('No Order : '.$data);
    	$no_order = PendaftaranPengujian::whereIn('id', $array)->pluck('no_order');
        return $this->render('modules.surat.create', [
        	'data' => $record,
        	'no_order' => $no_order,
        ]);
    }

    public function adendum($id, $arr, $tipe)
    {
    	$array = explode(',', $arr);
		$record = PendaftaranPengujian::find($array);
    	$no_order = PendaftaranPengujian::where('id', $array)->pluck('no_order');
    	$surat = SuratPenawaran::where('id', $record->first()->kaji_ulang->surat->id)->get();
    	$kaji_ulang = KajiUlang::where('pp_id', $array)->first();
        return $this->render('modules.surat.adendum', [
        	'data' => $record,
        	'no_order' => $no_order,
        	'surat' => $surat,
        	'kaji_ulang' => $kaji_ulang,
        ]);
    }

    public function buatAdendum($id, $tipe)
    {
    	$record = PendaftaranPengujian::find($id);
    	$no_order = PendaftaranPengujian::where('id', $id)->pluck('no_order');
    	$surat = SuratPenawaran::where('id', $record->kaji_ulang->surat->id)->get();
    	$kaji_ulang = KajiUlang::where('pp_id', $id)->first();
        return $this->render('modules.surat.adendum-umum', [
        	'data' => $record,
        	'no_order' => $no_order,
        	'surat' => $surat,
        	'kaji_ulang' => $kaji_ulang,
        ]);
    }

    public function buatData($id, $tipe)
    {
    	$this->setTitle('Buat Surat Penawaran');
		$data = PendaftaranPengujian::find($id);
    	$this->setSubtitle('No Order : '.$data->no_order);
        $this->setBreadcrumb(['Surat Penawaran' => '#', 'Buat' => '#']);
		$record = PendaftaranPengujian::where('nomor', $data->nomor)->get();
	    return $this->render('modules.surat.create', [
	    	'data' => $record,
	    	'no_order' => '',
	    ]);
    }

    public function ubah($id)
    {
    	$data = PendaftaranPengujian::find($id);
    	$record = PendaftaranPengujian::where('no_surat', $data->no_surat)->get();
    	$surat = SuratPenawaran::where('no_surat', $data->no_surat)->get();
        return $this->render('modules.surat.edit', [
        	'data' => $record,
        	'surat' => $surat,
        ]);
    }

    public function detil($nomor)
    {
    	$record = PendaftaranPengujian::whereHas('kaji_ulang', function($u){
    										$u->whereHas('surat');
    									})->where('id', $nomor)->where('tipe_surat', 0)->get();
    	$surat = SuratPenawaran::find($record->first()->kaji_ulang->surat->id);
    	$no_order = PendaftaranPengujian::whereHas('kaji_ulang', function($u){
    										$u->whereHas('surat');
    									})->where('id', $nomor)->where('tipe_surat', 0)->pluck('no_order');
        return $this->render('modules.surat.detil', [
        	'data' => $record,
        	'surat' => $surat,
        	'no_order' => $no_order,
        ]);
    }

    public function ubahWbs($nomor)
    {
    	$record = PendaftaranPengujian::where('nomor', $nomor)->where('tipe_surat', 0)->get();
    	$surat = SuratPenawaran::where('nomor', $nomor)->where('tipe', 0)->get();
    	$no_order = PendaftaranPengujian::where('nomor', $nomor)->where('tipe_surat', 0)->pluck('no_order');
        return $this->render('modules.surat.detil-wbs', [
        	'data' => $record,
        	'surat' => $surat,
        	'no_order' => $no_order,
        ]);
    }

    public function updateWbs(SuratPenawaranWBSRequest $request)
    {
    	try {
    		if($request->detail){
		    	foreach ($request->detail as $key => $value) {
		    		$surat 				= SuratPenawaran::find($value['surat_id']);
		    		$surat->wbs_io 		= $value['wbs_io'];
		    		$surat->save();
		    	}
    		}
	    	DB::commit();
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true
        ]); 
    }

    public function saveKomponen(SuratPenawaranRequest $request)
    {
    	$array = explode(',', $request->data_id);
    	$array = preg_replace('/[^A-Za-z0-9\-]/', '', $array);
    	try {
    		if($array){
		    	$data = PendaftaranPengujian::whereIn('no_order', $array)->where('tipe_surat', 0)->get();
	    		foreach ($data as $key => $vil) {
	    			$cik = $vil->kaji_ulang->detail->sortByDesc('tentative_start')->first();
	    			$jadwal[] = $cik->tentative_start;
	    		}

	    		usort($jadwal, function($a, $b) {
	    			$dateTimestamp1 = strtotime($a);
	    			$dateTimestamp2 = strtotime($b);

	    			return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
	    		});

	    		$jdwl = $jadwal[count($jadwal) - 1];
    		}else{
    			$data = PendaftaranPengujian::where('id', $request->id)->get();
    		}
	    	foreach ($data as $key => $value) {
	    		$surat = new SuratPenawaran;
	    		$surat->kaji_id 		= $value->kaji_ulang->id;
	    		$surat->no_surat 		= $request->no_surat;
	    		$surat->tgl_surat 		= $request->tgl_surat;
	    		// $surat->jadwal_start 	= $request->jadwal_start;
	    		// $surat->jadwal_end 		= $request->jadwal_end;
	    		$surat->adendum 		= $request->adendum;
	    		$surat->wbs_io 			= $request->wbs_io;
	    		$surat->nomor 			= $request->nomor;
	    		$surat->total 			= $request->total;

	    		if($request->tipe_customer == 3){
	    		}else{
		    		$surat->no_skki 		= $request->no_skki;
		    		$surat->tgl_skki 		= $request->tgl_skki;
	    		}
	    		$surat->save();
	    		$surat->saveDetail($request->detail);

	    		$act = new ActDisposisi;
	    		$act->parent_id = $value->id;
	    		$act->pengirim_id = auth()->user()->id;
	    		$act->penerima_id = 1;
	    		$act->tipe = 4;
	    		$act->jenis = 3;
	    		$act->save();
	    	}
	    	DB::commit();
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true,
          // 'message' => $nomor,
          // 'kategori' => $kategori
        ]); 
    }

    public function updateKomponen(SuratPenawaranRequest $request, $id)
    {
    	try {
    		foreach ($request->data_id as $key => $val) {
    			$surat = SuratPenawaran::find($val);
	    		$surat->no_surat 		= $request->no_surat;
	    		$surat->tgl_surat 		= $request->tgl_surat;
	    		$surat->adendum_status 	= ($surat->adendum_status+1);
	    		$surat->adendum 		= $request->adendum;
	    		$surat->wbs_io 			= $request->wbs_io;
	    		$surat->save();
	    		$hapus = SuratPenawaranDetail::where('surat_id', $val)
	                        ->delete();
	    		$surat->saveDetail($request->detail);
    		}

	    	DB::commit();
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true
        ]); 
    }

    public function saveAdendum(AdendumRequest $request)
    {
    	foreach ($request->data as $key => $value) {
    		if($value['detail']){
    		}else{
    			return response()->json([
    				'status' => 'false',
    				'message' => 'Data tidak boleh kosong'
    			],402);
    		}
    	}

    	try {
    		foreach ($request->data as $key => $val) {
    			$text = str_replace('.', '', $val['total']);
    			$text = str_replace(',', '', $text);
    			$text = str_replace(' ', '', $text);
    			$text = str_replace('Rp', '', $text);

    			$surat = SuratPenawaran::find($val['id']);
	    		$surat->tgl_surat 		= $val['tgl_surat'];
	    		$surat->no_skki 		= $val['no_skki'];
	    		$surat->tgl_skki 		= $val['tgl_skki'];
	    		$surat->adendum_status 	= ($surat->adendum_status +1);
	    		$surat->total 			= $text;
	    		$surat->surat_status 	= 0;
	    		if($request->tipe_customer == 0){
	    		}else{
		    		$surat->no_skki 		= $request->no_skki;
		    		$surat->tgl_skki 		= $request->tgl_skki;
	    		}
	    		$surat->save();

	    		$hapus = SuratPenawaranDetail::where('surat_id', $val['id'])
	                        				   ->delete();
	    		$surat->saveDetail($val['detail']);

	    		$act = new ActDisposisi;
	    		$act->parent_id = $surat->kaji_ulang->pp->id;
	    		$act->pengirim_id = auth()->user()->id;
	    		$act->penerima_id = 1;
	    		$act->tipe = 4;
	    		$act->jenis = 3;
	    		$act->status_surat_penawaran = ($surat->adendum_status);
	    		$act->save();
    		}
	    	DB::commit();
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true
        ]); 
    }

    public function edit($id)
    {
        return $this->render('modules.surat.penawaran.edit');
    }

    public function show($id)
    {
        return $this->render('modules.surat.penawaran.detail');
    }

    public function upload($id, $arr)
    {	
    	$array = explode(',', $arr);
    	$array = preg_replace('/[^A-Za-z0-9\-]/', '', $array);
        return $this->render('modules.surat.upload', [
            'record' => $array,
        ]);
    }

    public function uploadPenolakan($id)
    {
        return $this->render('modules.surat.upload-penolakan', [
            'id' => $id,
        ]);
    }

    public function saveUpload(UploadSuratRequest $request)
    {
    	DB::beginTransaction();
        try {
        	foreach ($request->data as $key => $vil) {
        		$pp = PendaftaranPengujian::find($vil['id']);
        		$surat = SuratPenawaran::find($pp->kaji_ulang->surat->id);
        		$surat->multipleFileUpload($request->surat_penawaran);
        		$surat->surat_status = 1;
        		$surat->save();
        		
        		$act = new ActDisposisi;
        		$act->parent_id = $pp->id;
        		$act->pengirim_id = auth()->user()->id;
        		$act->penerima_id = 1;
        		$act->tipe = 5;
        		$act->jenis = 4;
        		$act->save();

		    	$rightnow = Carbon::parse(date('Y-m-d'));
		    	$nomor = array();
		    	$jadwal = array();
		    	$cek = PendaftaranPengujian::where('id', $pp->id)->where('tipe_surat', 0)->get();
		    	foreach($cek as $key => $vil) {
		    		$cik = $vil->kaji_ulang->detail->sortByDesc('tentative_start')->first();
		    		$jadwal[] = $cik->tentative_start;
		    	}

		    	usort($jadwal, function($a, $b) {
		    		$dateTimestamp1 = strtotime($a);
		    		$dateTimestamp2 = strtotime($b);
		    		return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
		    	});

		    	$jdwl = $jadwal[count($jadwal) - 1];
		    	$data = PendaftaranPengujian::where('id', $pp->id)->get();

		    	$date_jdwal = Carbon::parse($jdwl);

		    	$a =0;
		    	foreach($data as $key => $value){
		    		if($value->kelas == 2){
		    			$cek_kadaluarsa = $date_jdwal->subWeekdays(2);
		    			if($cek_kadaluarsa <= $rightnow){
		    				$kadaluarsa = Carbon::parse($rightnow)->addWeekdays(2);
		    			}else{
		    				$kadaluarsa = $cek_kadaluarsa;
		    			}
		    		}else{
		    			$cek_kadaluarsa = $date_jdwal->subWeekdays(10);
		    			if($cek_kadaluarsa <= $rightnow){
		    				$kadaluarsa = Carbon::parse($rightnow)->addWeekdays(2);
		    			}else{
		    				$kadaluarsa = $cek_kadaluarsa;
		    			}
		    		}

		    		$seri 		= $surat->kaji_ulang->pp->pelayanan->seri;
		    		$kode 		= $surat->kaji_ulang->pp->pelayanan->kode;
		    		$urut_seri 	= $seri;

		    		if($seri < 10){
		    			$seri = '00'.$seri;
		    		}elseif($seri > 10 && $seri < 100){
		    			$seri = '0'.$seri;
		    		}elseif($seri >= 100){
		    			$seri = $seri;
		    		}else{
		    			$seri = 000;
		    		}

		    		$cari_va 			    	= VirtualAccount::where('surat_id', $surat->id)->where('tipe', 0)->first();
		    		if($cari_va){
		    			$va = $cari_va;
		    		}else{
		    			$va 			     	= new VirtualAccount;
		    			$va->surat_id 		 	= $surat->id;
		    			$va->no_va 	 		 	= '89319'.sprintf("%02d",$kode).$surat->kaji_ulang->pp->user->pelanggans->perusahaan->kode.Carbon::now()->format('y').sprintf("%03d",$seri);
		    			$va->tgl_aktif  	 	= $rightnow;
		    			$va->tgl_kadaluarsa  	= $kadaluarsa;
		    			$va->nominal  			= $surat->total;
		    			if($value->user->pelanggans->perusahaan->kategori == 0){
		    				$va->status  		 	= 1;
		    			}else{
		    				$va->status  		 	= 0;
		    			}
		    			$va->tipe_customer  	= $value->user->pelanggans->perusahaan->kategori;
		    			$va->save();

		    			$jenis_layanan 			= JenisPelayanan::find($surat->kaji_ulang->pp->pelayanan->id);
		    			$jenis_layanan->seri 		= ($urut_seri+1);
		    			$jenis_layanan->save();
		    		}
		    		if($a % 2 == 0){
		    			$nomor[]= $va->no_va.'<br>';
		    		}else{
		    			$nomor[]= $va->no_va;
		    		}

		    		$kategori = $value->user->pelanggans->perusahaan->kategori;

		    		$a++;

		    		$user = User::whereHas('roles',function($query){
		    			$query->where('name','keuangan');
		    		})->get();
		    		foreach ($user as $row) {
		    			if(!is_null($row->device_id)){
		    				$this->onesignal('Virtual Account Baru dengan nomor '.$va->no_va,$row->device_id,$va->toArray(),'virtual-account','baru');
		    			}
		    		}
		    	}

	        	$data_email=[
	        		'email'     => $pp->user->email,
	        		'url'     	=> url('download', $surat->id).'/surat-penawaran',
	        	];
	        	sendEmailSuratPenawaran($data_email);
        	}
            DB::commit();
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true,
        ]);  
    }

    public function virtual(Request $request)
    {
    	DB::beginTransaction();
        try {
    		$pp = PendaftaranPengujian::find($request->id[0]);
    		$surat = SuratPenawaran::find($pp->kaji_ulang->surat->id);
    		$surat->save();

	    	$rightnow = Carbon::parse(date('Y-m-d'));
	    	$nomor = array();
	    	$jadwal = array();
	    	$cek = PendaftaranPengujian::where('id', $pp->id)->where('tipe_surat', 0)->get();
	    	foreach($cek as $key => $vil) {
	    		$cik = $vil->kaji_ulang->detail->sortByDesc('tentative_start')->first();
	    		$jadwal[] = $cik->tentative_start;
	    	}

	    	usort($jadwal, function($a, $b) {
	    		$dateTimestamp1 = strtotime($a);
	    		$dateTimestamp2 = strtotime($b);
	    		return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
	    	});

	    	$jdwl = $jadwal[count($jadwal) - 1];

	    	$data = PendaftaranPengujian::where('id', $pp->id)->get();

	    	$date_jdwal = Carbon::parse($jdwl);

	    	$a =0;
	    	foreach($data as $key => $value){
	    		if($value->kelas == 2){
	    			$cek_kadaluarsa = $date_jdwal->subWeekdays(2);
	    			if($cek_kadaluarsa <= $rightnow){
	    				$kadaluarsa = Carbon::parse($rightnow)->addWeekdays(2);
	    			}else{
	    				$kadaluarsa = $cek_kadaluarsa;
	    			}
	    		}else{
	    			$cek_kadaluarsa = $date_jdwal->subWeekdays(10);
	    			if($cek_kadaluarsa <= $rightnow){
	    				$kadaluarsa = Carbon::parse($rightnow)->addWeekdays(2);
	    			}else{
	    				$kadaluarsa = $cek_kadaluarsa;
	    			}
	    		}

	    		$seri 		= $surat->kaji_ulang->pp->pelayanan->seri;
	    		$kode 		= $surat->kaji_ulang->pp->pelayanan->kode;
	    		$urut_seri 	= $seri;

	    		if($seri < 10){
	    			$seri = '00'.$seri;
	    		}elseif($seri > 10 && $seri < 100){
	    			$seri = '0'.$seri;
	    		}elseif($seri >= 100){
	    			$seri = $seri;
	    		}else{
	    			$seri = 000;
	    		}

	    		$cari_va 			    	= VirtualAccount::where('surat_id', $surat->id)->where('tipe', 0)->first();
	    		if($cari_va){
	    			$va = $cari_va;
	    		}else{
	    			$va 			     	= new VirtualAccount;
	    			$va->surat_id 		 	= $surat->id;
	    			$va->no_va 	 		 	= '89319'.sprintf("%02d",$kode).$surat->kaji_ulang->pp->user->pelanggans->perusahaan->kode.Carbon::now()->format('y').sprintf("%03d",$seri);
	    			$va->tgl_aktif  	 	= $rightnow;
	    			$va->tgl_kadaluarsa  	= $kadaluarsa;
	    			$va->nominal  			= $surat->total;
	    			if($value->user->pelanggans->perusahaan->kategori == 0){
	    				$va->status  		 	= 1;
	    			}else{
	    				$va->status  		 	= 0;
	    			}
	    			$va->tipe_customer  	= $value->user->pelanggans->perusahaan->kategori;
	    			$va->save();

	    			$jenis_layanan 			= JenisPelayanan::find($surat->kaji_ulang->pp->pelayanan->id);
	    			$jenis_layanan->seri 		= ($urut_seri+1);
	    			$jenis_layanan->save();
	    		}

	    		if($a % 2 == 0){
	    			$nomor[]= $va->no_va.'<br>';
	    		}else{
	    			$nomor[]= $va->no_va;
	    		}

	    		$kategori = $value->user->pelanggans->perusahaan->kategori;

	    		$a++;

	    		$user = User::whereHas('roles',function($query){
	    			$query->where('name','keuangan');
	    		})->get();
	    		foreach ($user as $row) {
	    			if(!is_null($row->device_id)){
	    				$this->onesignal('Virtual Account Baru dengan nomor '.$va->no_va,$row->device_id,$va->toArray(),'virtual-account','baru');
	    			}
	    		}
	    	}
            DB::commit();
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true,
        ]);  
    }

    public function virtual2(Request $request)
    {
    	DB::beginTransaction();
        try {
    		$pp = PendaftaranPengujian::find($request->id);
    		$surat = SuratPenawaran::find($pp->kaji_ulang->surat->id);
    		$surat->save();

	    	$rightnow = Carbon::parse(date('Y-m-d'));
	    	$nomor = array();
	    	$jadwal = array();
	    	$cek = PendaftaranPengujian::where('id', $pp->id)->where('tipe_surat', 0)->get();
	    	foreach($cek as $key => $vil) {
	    		$cik = $vil->kaji_ulang->detail->sortByDesc('tentative_start')->first();
	    		$jadwal[] = $cik->tentative_start;
	    	}

	    	usort($jadwal, function($a, $b) {
	    		$dateTimestamp1 = strtotime($a);
	    		$dateTimestamp2 = strtotime($b);
	    		return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
	    	});

	    	$jdwl = $jadwal[count($jadwal) - 1];

	    	$data = PendaftaranPengujian::where('id', $pp->id)->get();

	    	$date_jdwal = Carbon::parse($jdwl);

	    	$a =0;
	    	foreach($data as $key => $value){
	    		if($value->kelas == 2){
	    			$cek_kadaluarsa = $date_jdwal->subWeekdays(2);
	    			if($cek_kadaluarsa <= $rightnow){
	    				$kadaluarsa = Carbon::parse($rightnow)->addWeekdays(2);
	    			}else{
	    				$kadaluarsa = $cek_kadaluarsa;
	    			}
	    		}else{
	    			$cek_kadaluarsa = $date_jdwal->subWeekdays(10);
	    			if($cek_kadaluarsa <= $rightnow){
	    				$kadaluarsa = Carbon::parse($rightnow)->addWeekdays(2);
	    			}else{
	    				$kadaluarsa = $cek_kadaluarsa;
	    			}
	    		}

	    		$seri 		= $surat->kaji_ulang->pp->pelayanan->seri;
	    		$kode 		= $surat->kaji_ulang->pp->pelayanan->kode;
	    		$urut_seri 	= $seri;

	    		if($seri < 10){
	    			$seri = '00'.$seri;
	    		}elseif($seri > 10 && $seri < 100){
	    			$seri = '0'.$seri;
	    		}elseif($seri >= 100){
	    			$seri = $seri;
	    		}else{
	    			$seri = 000;
	    		}

	    		$cari_va 			    	= VirtualAccount::where('surat_id', $surat->id)->where('tipe', 0)->first();
	    		if($cari_va){
	    			$va = $cari_va;
	    		}else{
	    			$va 			     	= new VirtualAccount;
	    			$va->surat_id 		 	= $surat->id;
	    			$va->no_va 	 		 	= '89319'.sprintf("%02d",$kode).$surat->kaji_ulang->pp->user->pelanggans->perusahaan->kode.Carbon::now()->format('y').sprintf("%03d",$seri);
	    			$va->tgl_aktif  	 	= $rightnow;
	    			$va->tgl_kadaluarsa  	= $kadaluarsa;
	    			$va->nominal  			= $surat->total;
	    			if($value->user->pelanggans->perusahaan->kategori == 0){
	    				$va->status  		 	= 1;
	    			}else{
	    				$va->status  		 	= 0;
	    			}
	    			$va->tipe_customer  	= $value->user->pelanggans->perusahaan->kategori;
	    			$va->save();

	    			$jenis_layanan 			= JenisPelayanan::find($surat->kaji_ulang->pp->pelayanan->id);
	    			$jenis_layanan->seri 		= ($urut_seri+1);
	    			$jenis_layanan->save();
	    		}

	    		if($a % 2 == 0){
	    			$nomor[]= $va->no_va.'<br>';
	    		}else{
	    			$nomor[]= $va->no_va;
	    		}

	    		$kategori = $value->user->pelanggans->perusahaan->kategori;

	    		$a++;

	    		$user = User::whereHas('roles',function($query){
	    			$query->where('name','keuangan');
	    		})->get();
	    		foreach ($user as $row) {
	    			if(!is_null($row->device_id)){
	    				$this->onesignal('Virtual Account Baru dengan nomor '.$va->no_va,$row->device_id,$va->toArray(),'virtual-account','baru');
	    			}
	    		}
	    	}
            DB::commit();
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true,
        ]);  
    }

    public function update(Request $request, $id)
    {
        return response([
          'status' => true
        ]); 
    }

    public function destroy($id)
    {
        return response([
            'status' => true,
        ]);
    }

    public function preview($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function previewMultiple($id, $type)
    {
    	if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $c->filename,
                        ];
                    }
                }
                return $this->render('pdf-multiple', [
		        	'mockup' => true,
		        	'data' => $rename,
		        ]);
            }
        }
    }

    public function download($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

    public function downloadSurat($id)
    {	
    	$daftar = PendaftaranPengujian::find($id);
    	$type ='surat-penawaran';
    	if($type != "undefined")
        {
            $check = Files::where('target_id', $daftar->kaji_ulang->surat->id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        Storage::disk('public')->move($c->url, $newname.$c->filename);
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $newname.$c->filename,
                        ];
                    }
                }
                $now = Carbon::now()->format('Ymdhis');
                Zipper::make(public_path('storage/'.$type.$now.'.zip'))->add($files)->close();
                
                for ($i=0; $i < count($rename) ; $i++) { 
                    Storage::disk('public')->move($rename[$i]['new'], $rename[$i]['old']);
                }

                if(file_exists(public_path('storage/'.$type.$now.'.zip')))
                {
                    return response()->download(public_path('storage/'.$type.$now.'.zip'));
                }
                return response()->view('errors.download');
            }

            return response()->view('errors.download');
        }
    }

    public function previewSurat($id)
    {	
    	$daftar = PendaftaranPengujian::find($id);
    	$check = Files::where('target_id', $daftar->kaji_ulang->surat->id)->where('target_type', 'surat-penawaran')->get();
        $files = [];
        if($check->count() > 0)
        {
            $rename = [];
            foreach($check as $c)
            {
                if(file_exists(public_path('storage/'.$c->url)))
                {
                    $newname = '';
                    $splitname = explode("/",$c->url);
                    for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                        $newname .= $splitname[$i].'/';
                    }
                    $files[] = public_path('storage/'.$newname.$c->filename);
                    $rename[] = [
                        'old' => $c->url,
                        'new' => $c->filename,
                    ];
                }
            }
            return $this->render('pdf-multiple', [
	        	'mockup' => true,
	        	'data' => $rename,
	        ]);
        }
    }

    public function previewPenolakan($id)
    {
    	$daftar = KajiUlang::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function downloadPenolakan($id)
    {
    	$daftar = KajiUlang::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

    public function konfirmasiPembayaran(Request $request, $id){
        return $this->render('modules.surat.penawaran.create');
    }

    public function savePenolakan(PenolakanRequest $request)
    {
        DB::beginTransaction();
    	try {
    		$kaji_ulang 			= KajiUlang::find($request->id);
    		if($file = $request->bukti){
	    		$path 					= $file->store('uploads/penolakan', 'public');
				$kaji_ulang->filename 	= $file->getClientOriginalName();
				$kaji_ulang->bukti 		= $path;
			}
    		$kaji_ulang->save();
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}

    	return response([
            'status' => true,
        ]);
    }
}
