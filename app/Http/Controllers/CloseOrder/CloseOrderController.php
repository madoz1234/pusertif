<?php

namespace App\Http\Controllers\CloseOrder;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\CloseOrder\CloseOrderRequest;

/* Models */
use App\Models\KajiUlang\KajiUlang;
use App\Models\CloseOrder\CloseOrder;
use App\Models\CloseOrder\CloseOrderDetail;
use App\Models\PenerimaanBarang\PenerimaanBarang;
use App\Models\PenerimaanBarang\PenerimaanBarangDetail;
use App\Models\VirtualAccount\KonfirmasiPembayaran;
use App\Models\LaporanPengujian\LaporanPengujian;
use App\Models\LaporanPengujian\LaporanPengujianDetail;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\Act\ActPengujian;
use App\Models\Authentication\Role;
use App\Models\Picture;
use App\Models\Files;
use Illuminate\Support\Facades\Storage;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Zipper;


class CloseOrderController extends Controller
{
    protected $link = 'close-order/';
    protected $perms = 'close-order';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Close Order");
        $this->setPerms($this->perms);
        $this->setModalSize("small");
        $this->setBreadcrumb(['Close Order' => '#']);

        // Header Grid Datatable
        $this->listStructs = [
            'listStruct' => [
	            [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian dan Merk',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '80px',
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
            ],
            'listStruct2' => [
	            [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian dan Merk',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
            ],
        ];
    }

    public function grid(Request $request)
    {
        $user = auth()->user();
        if($user->hasRole(['msb-kalibrasi','admin'])){
            $records = PenerimaanBarang::doesntHave('closeorder')->byLayanan(1)->select('*');
        }elseif($user->hasRole(['msb-siskit','admin'])){
        	$records = PenerimaanBarang::doesntHave('closeorder')->byLayanan(2)->select('*');
        }elseif($user->hasRole(['msb-sistgi','admin'])){
        	$records = PenerimaanBarang::doesntHave('closeorder')->byLayanan(3)->select('*');
        }elseif($user->hasRole(['msb-tegangan-rendah','admin'])){
        	$records = PenerimaanBarang::doesntHave('closeorder')->byLayanan(4)->select('*');
        }elseif($user->hasRole(['msb-tegangan-tinggi','admin'])){
        	$records = PenerimaanBarang::doesntHave('closeorder')->byLayanan(5)->select('*');
        }else{
        	$records = PenerimaanBarang::where('id', 0);
        }

        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        if($no_order = $request->no_order){
            $records->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                $pp->where('no_order','like','%'.$no_order.'%');
            });
        }
        if($no_surat = $request->no_surat){
            $records->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                $pp->where('no_surat','like','%'.$no_surat.'%');
            });
        }
        if($tanggal_order = $request->tanggal_order){
            $records->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            });
        }
        if($jenis_pelayanan_id = $request->jenis_pelayanan_id){
            $records->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                $pp->where('layanan_id',$jenis_pelayanan_id);
            });
        }
        if($no_wbs = $request->no_wbs){
            $records->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                $pp->where('wbs_io','like','%'.$no_wbs.'%');
            });
        }
        $records = $records->get();

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->konfirmasi->va->surat->kaji_ulang->pp->detail){
                    $jenis_pengujian .= '<div class="ui ordered list list-more1" data-display="3">';
                    foreach ($record->konfirmasi->va->surat->kaji_ulang->pp->detail as $key => $value) {
                        $kaji_ulang = KajiUlang::find($record->konfirmasi->va->surat->kaji_ulang->id);
                        if($kaji_ulang){
                            foreach ($kaji_ulang->detail as $kuy => $val) {
                                if($val->jenis_id == $value->id){
                                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
                                }
                            }
                        }else{
                            $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-id="0">'.$value->jenis->nama.'</a></div>';
                        }
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('wbs_io', function($record){
                return $record->konfirmasi->wbs_io;
            })
            ->editColumn('detil', function($record){
                $btn = '';
                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->id.'/detail')
                ]);

                return $btn;
            })
            ->addColumn('action', function ($record) use ($link){
            	$btn = '';
            	if($record->status_close == 1){
            		$btn .= $this->makeButton([
            			'type' => 'modal',
            			'tooltip' => 'Close Order',
            			'class' => 'green icon button closeOrder',
            			'label' => '<i class="window close outline icon"></i>',
            			'data' => [
            				'id' => $record->id,
            			],
            			'id'   => $record->id
            		]);
            	}else{
            		$btn .= $this->makeButton([
	                	'type' => 'url',
	                	'tooltip' => 'Buat Close Order',
	                	'class' => 'ui violet mini icon button',
	                	'label' => '<i class="edit icon"></i>',
	                	'url' => url($this->link.$record->id.'/edit')
	                ]);
            	}
                return $btn;
            })
            ->rawColumns(['action','status','detil','jenis_pengujian','layanan_lingkup','penerimaan','pengujian'])
            ->make(true);
    }

    public function historis(Request $request)
    {
        $user = auth()->user();
        if($user->hasRole(['msb-kalibrasi','admin'])){
            $records = PenerimaanBarang::has('closeorder')->byLayanan(1)->select('*');
        }elseif($user->hasRole(['msb-siskit','admin'])){
        	$records = PenerimaanBarang::has('closeorder')->byLayanan(2)->select('*');
        }elseif($user->hasRole(['msb-sistgi','admin'])){
        	$records = PenerimaanBarang::has('closeorder')->byLayanan(3)->select('*');
        }elseif($user->hasRole(['msb-tegangan-rendah','admin'])){
        	$records = PenerimaanBarang::has('closeorder')->byLayanan(4)->select('*');
        }elseif($user->hasRole(['msb-tegangan-tinggi','admin'])){
        	$records = PenerimaanBarang::has('closeorder')->byLayanan(5)->select('*');
        }else{
        	$records = PenerimaanBarang::where('id', 0);
        }

        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        if($no_order = $request->no_order){
            $records->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                $pp->where('no_order','like','%'.$no_order.'%');
            });
        }
        if($no_surat = $request->no_surat){
            $records->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                $pp->where('no_surat','like','%'.$no_surat.'%');
            });
        }
        if($tanggal_order = $request->tanggal_order){
            $records->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            });
        }
        if($jenis_pelayanan_id = $request->jenis_pelayanan_id){
            $records->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                $pp->where('layanan_id',$jenis_pelayanan_id);
            });
        }
        if($no_wbs = $request->no_wbs){
            $records->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                $pp->where('wbs_io','like','%'.$no_wbs.'%');
            });
        }
        $records = $records->get();

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->konfirmasi->va->surat->kaji_ulang->pp->detail){
                    $jenis_pengujian .= '<div class="ui ordered list list-more2" data-display="3">';
                    foreach ($record->konfirmasi->va->surat->kaji_ulang->pp->detail as $key => $value) {
                        $kaji_ulang = KajiUlang::find($record->konfirmasi->va->surat->kaji_ulang->id);
                        if($kaji_ulang){
                            foreach ($kaji_ulang->detail as $kuy => $val) {
                                if($val->jenis_id == $value->id){
                                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
                                }
                            }
                        }else{
                            $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
                        }
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('wbs_io', function($record){
                return $record->konfirmasi->wbs_io;
            })
            ->addColumn('action', function ($record) use ($link){
            	$btn = '';
        		$btn .= $this->makeButton([
                	'type' => 'url',
                	'tooltip' => 'Detail',
                	'class' => 'ui violet mini icon button',
                	'label' => '<i class="eye icon"></i>',
                	'url' => url($this->link.$record->id.'/detail')
                ]);
                return $btn;
            })
            ->rawColumns(['action','status','detil','jenis_pengujian','layanan_lingkup','penerimaan','pengujian'])
            ->make(true);
    }

    public function index()
    {	
    	$user = auth()->user();
    	if($user->hasRole(['msb-kalibrasi','admin'])){
            $satu = PenerimaanBarang::doesntHave('closeorder')->byLayanan(1)->get()->count();
        }elseif($user->hasRole(['msb-siskit','admin'])){
        	$satu = PenerimaanBarang::doesntHave('closeorder')->byLayanan(2)->get()->count();
        }elseif($user->hasRole(['msb-sistgi','admin'])){
        	$satu = PenerimaanBarang::doesntHave('closeorder')->byLayanan(3)->get()->count();
        }elseif($user->hasRole(['msb-tegangan-rendah','admin'])){
        	$satu = PenerimaanBarang::doesntHave('closeorder')->byLayanan(4)->get()->count();
        }elseif($user->hasRole(['msb-tegangan-tinggi','admin'])){
        	$satu = PenerimaanBarang::doesntHave('closeorder')->byLayanan(5)->get()->count();
        }else{
        	$satu = 0;
        }

    	$data = [
            'mockup'    => true,
            'structs'   => $this->listStructs,
            'satu'   	=> $satu,
        ];
        return $this->render('modules.close-order.index', $data);
    }

    public function create()
    {
        return $this->render('modules.reschedule.data.create');
    }

    public function store(Request $request)
    {
        // $record = new Role;
        // $record->fill($request->all());
        // $record->save();

        return response([
            'status' => true
        ]);
    }
 
    public function edit($id)
    {
        $penerimaan = PenerimaanBarang::find($id);
        $layanan = $penerimaan->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama;
        if($penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id == 1){
	       	$this->setTitle('Detail Close Order Kalibrasi');
	       	$this->setSubtitle('No WBS/IO : '.$penerimaan->konfirmasi->wbs_io);
	        $this->setBreadcrumb(['Close Order' => '#', 'Close Order Detail' => '#']);
	        return $this->render('modules.close-order.kalibrasi',[
	        	'record' => $penerimaan,
	    	]);
        }else{
        	$this->setTitle('Detail Close Order '.$layanan);
	       	$this->setSubtitle('No WBS/IO : '.$penerimaan->konfirmasi->wbs_io);
	        $this->setBreadcrumb(['Close Order' => '#', 'Close Order Detail' => '#']);
	        return $this->render('modules.close-order.uji',[
	        	'record' => $penerimaan,
	    	]);
        }
    }

    public function detail($id)
    {
        $penerimaan = PenerimaanBarang::find($id);
        $layanan = $penerimaan->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama;
        if($penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id == 1){
	       	$this->setTitle('Detail Close Order Kalibrasi');
	       	$this->setSubtitle('No WBS/IO : '.$penerimaan->konfirmasi->wbs_io);
	        $this->setBreadcrumb(['Close Order' => '#', 'Close Order Detail' => '#']);
	        return $this->render('modules.close-order.detail-kalibrasi',[
	        	'record' => $penerimaan,
	    	]);
        }else{
        	$this->setTitle('Detail Close Order '.$layanan);
	       	$this->setSubtitle('No WBS/IO : '.$penerimaan->konfirmasi->wbs_io);
	        $this->setBreadcrumb(['Close Order' => '#', 'Close Order Detail' => '#']);
	        return $this->render('modules.close-order.detail-uji',[
	        	'record' => $penerimaan,
	    	]);
        }
    }

    public function closeOrderKalibrasi($id)
    {
    	$data = PenerimaanBarang::find($id);
        return $this->render('modules.close-order.close-order-kalibrasi', [
            'record' => $data,
        ]);
    }

    public function preview($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	$link = 0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function previewMultiple($id, $type)
    {
    	if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $c->filename,
                        ];
                    }
                }
                return $this->render('pdf-multiple', [
		        	'mockup' => true,
		        	'data' => $rename,
		        ]);
            }
        }
    }

    public function download($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

    public function destroy($id)
    {
        return response([
            'status' => true,
        ]);
    }

    public function simpanClose($data, $max)
    {
    	DB::beginTransaction();
    	try {
    		$list = explode(',', $data);
			foreach ($list as $key => $value) {
	    		$penerimaan_barang_detail = PenerimaanBarangDetail::find($value);
	    		$penerimaan_barang_detail->status_close = 1;
	    		$penerimaan_barang_detail->save();

	    		$penerimaan = PenerimaanBarang::find($penerimaan_barang_detail->penerimaan_id);

    			$pembanding = $penerimaan->detail_penerimaan_barang->where('status_close', '<', 1)->count();
    			if($pembanding == 1){
    				$penerimaan_barang = PenerimaanBarang::find($penerimaan_barang_detail->penerimaan_id);
		    		$penerimaan_barang->status_close = 1;
		    		$penerimaan_barang->save();
    			}
			}
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    // public function closeOrder($id)
    // {
    // 	DB::beginTransaction();
    // 	try {
    // 		$penerimaan_barang_detail = PenerimaanBarangDetail::find($id);
    // 		$penerimaan_barang_detail->status_close = 1;
    // 		$penerimaan_barang_detail->save();
    // 		DB::commit();
    // 	}catch (\Exception $e) {
    // 		DB::rollback();
    // 		return response([
    // 			'status' => 'error',
    // 			'message' => 'An error occurred!',
    // 			'error' => $e->getMessage(),
    // 		], 500);
    // 	}
    // }

    public function closeOrderUji($data, $max)
    {
    	DB::beginTransaction();
    	try {
    		$list = explode(',', $data);
    		foreach ($list as $key => $value) {
	    		$cari 			= PenerimaanBarangDetail::find($value);
	    		$caridata 		= PenerimaanBarangDetail::where('flag',0)->where('group_id',$cari->group_id)->get();
	    		foreach ($caridata as $key => $value) {
		    		$penerimaan = PenerimaanBarang::find($value->penerimaan_id);

		    		$penerimaan_barang_detail = PenerimaanBarangDetail::find($value->id);
		    		$penerimaan_barang_detail->status_close = 1;
		    		$penerimaan_barang_detail->save();
		    		
	    			$pembanding = $penerimaan->detail_penerimaan_barang->where('status_close', '<', 1)->count();
	    			if($pembanding == 1){
	    				$penerimaan_barang = PenerimaanBarang::find($cari->penerimaan_id);
			    		$penerimaan_barang->status_close = 1;
			    		$penerimaan_barang->save();
	    			}
	    		}
	    	}
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function saveCloseKalibrasi(CloseOrderRequest $request, $id)
    {	
    	$user = auth()->user();
    	DB::beginTransaction();
    	try {
    		$close_oder = new CloseOrder;
    		$close_oder->penerimaan_id 	= $id;
    		$close_oder->no_nota 		= $request->no_nota;
    		$close_oder->tgl_nota 		= $request->tgl_nota;
    		$close_oder->status 		= 1;
    		$close_oder->save();
    		$close_oder->saveDetail($id);

    		$penerimaan 				= PenerimaanBarang::find($id);
    		$pendaftaran 				= PendaftaranPengujian::find($penerimaan->konfirmasi->va->surat->kaji_ulang->pp->id);
    		$pendaftaran->status 		= 6;
    		$pendaftaran->save();

    		$act = new ActPengujian;
	        $act->parent_id 		= $penerimaan->pengujian->detailpengujian->first()->id;
	        $act->pengirim_id 		= $user->id;
	        $act->penerima_id 		= 1;
	        $act->tipe 				= 3;
	        $act->jenis 			= 3;
	        $act->keterangan 		= '<a>'.$user->nama.'</a> menyelesaikan <a>Close Order</a> dengan No WBS: <a>'.$penerimaan->konfirmasi->wbs_io.'</a>';
	        $act->save();
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function pembatalan($id)
    {
    	DB::beginTransaction();
    	try {
    		$penerimaan_barang = PenerimaanBarang::find($id);
    		$penerimaan_barang->status_close = 1;
    		$penerimaan_barang->save();
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }
}
