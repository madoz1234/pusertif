<?php

namespace App\Http\Controllers\KajiUlang;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Models */
use App\Models\Authentication\Role;
use App\Models\Authentication\User;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\KajiUlang\KajiUlang;
use App\Models\KajiUlang\Detail;
use App\Models\Master\Pelanggan;
use App\Models\Users;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\KajiUlang\KajiUlangRequest;
use App\Http\Requests\KajiUlang\DisposisiRequest;
use App\Models\Act\ActDisposisi;
use App\Models\Picture;
use App\Models\Files;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Zipper;

class KajiUlangController extends Controller
{
    protected $link = 'kaji-ulang/proses-kaji-ulang/';
    protected $perms = 'kaji-ulang';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Kaji Ulang");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Kaji Ulang' => '#', 'Kaji Ulang' => '#']);

        // Header Grid Datatable
        $this->listStructs = [
        	'listStruct' => [
        		[
        			'data' => 'num',
        			'name' => 'num',
        			'label' => '#',
        			'orderable' => false,
        			'searchable' => false,
        			'className' => "center aligned",
        			'width' => '40px',
        		],
        		/* --------------------------- */
        		[
        			'data' => 'no_order',
        			'name' => 'no_order',
        			'label' => 'No Order',
        			'className' => "center aligned",
        			'searchable' => false,
        			'width' => '150px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'tgl_order',
        			'name' => 'tgl_order',
        			'label' => 'Tgl Order',
        			'className' => "center aligned",
        			'searchable' => false,
        			'width' => '150px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'no_surat',
        			'name' => 'no_surat',
        			'label' => 'No Surat Permintaan',
        			'className' => "center aligned",
        			'searchable' => false,
        			'width' => '150px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'layanan',
        			'name' => 'layanan',
        			'label' => 'Layanan',
        			'searchable' => false,
        			'className' => "center aligned",
        			'width' => '200px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'lingkup',
        			'name' => 'lingkup',
        			'label' => 'Lingkup',
        			'searchable' => false,
        			'className' => "center aligned",
        			'width' => '200px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'jenis_pengujian',
        			'name' => 'jenis_pengujian',
        			'label' => 'Jenis Pengujian',
        			'searchable' => false,
        			'className' => "left aligned",
        			'width' => '150px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'pemesan',
        			'name' => 'pemesan',
        			'label' => 'Peminta Jasa',
        			'searchable' => false,
        			'className' => "center aligned",
        			'width' => '300px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'action',
        			'name' => 'action',
        			'label' => 'Aksi',
        			'searchable' => false,
        			'sortable' => false,
        			'className' => "center aligned",
        			'width' => '100px',
        		],
        		[
        			'data' => 'timestamp',
        			'name' => 'timestamp',
        			'label' => 'Time Stamp',
        			'searchable' => false,
        			'className' => "center aligned timestamp",
        			'width' => '150px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'status',
        			'name' => 'status',
        			'label' => 'Status',
        			'searchable' => false,
        			'sortable' => true,
        			'className' => "center aligned",
        			'width' => '150px',
        		],
        	],
        	'listStruct2' => [
        		[
        			'data' => 'num',
        			'name' => 'num',
        			'label' => '#',
        			'orderable' => false,
        			'searchable' => false,
        			'className' => "center aligned",
        			'width' => '40px',
        			'sortable' => true,
        		],
        		/* --------------------------- */
        		[
        			'data' => 'no_order',
        			'name' => 'no_order',
        			'label' => 'No Order',
        			'className' => "center aligned",
        			'searchable' => false,
        			'width' => '150px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'tgl_order',
        			'name' => 'tgl_order',
        			'label' => 'Tgl Order',
        			'className' => "center aligned",
        			'searchable' => false,
        			'width' => '150px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'no_surat',
        			'name' => 'no_surat',
        			'label' => 'No Surat Permintaan',
        			'className' => "center aligned",
        			'searchable' => false,
        			'width' => '150px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'layanan',
        			'name' => 'layanan',
        			'label' => 'Layanan',
        			'searchable' => false,
        			'className' => "center aligned",
        			'width' => '200px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'lingkup',
        			'name' => 'lingkup',
        			'label' => 'Lingkup',
        			'searchable' => false,
        			'className' => "center aligned",
        			'width' => '200px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'jenis_pengujian',
        			'name' => 'jenis_pengujian',
        			'label' => 'Jenis Pengujian',
        			'searchable' => false,
        			'className' => "left aligned",
        			'width' => '150px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'pemesan',
        			'name' => 'pemesan',
        			'label' => 'Peminta Jasa',
        			'searchable' => false,
        			'className' => "center aligned",
        			'width' => '300px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'briefing',
        			'name' => 'briefing',
        			'label' => 'Briefing Teknis',
        			'searchable' => false,
        			'className' => "center aligned",
        			'width' => '50px',
        			'sortable' => false,
        		],
        		[
        			'data' => 'action',
        			'name' => 'action',
        			'label' => 'Aksi',
        			'searchable' => false,
        			'sortable' => false,
        			'className' => "center aligned",
        			'width' => '100px',
        		],
        		[
        			'data' => 'timestamp',
        			'name' => 'timestamp',
        			'label' => 'Time Stamp',
        			'searchable' => false,
        			'className' => "center aligned waktu",
        			'width' => '150px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'status',
        			'name' => 'status',
        			'label' => 'Status',
        			'searchable' => false,
        			'sortable' => true,
        			'className' => "center aligned",
        			'width' => '150px',
        		],
        	],
        	'listStruct3' => [
        		[
        			'data' => 'num',
        			'name' => 'num',
        			'label' => '#',
        			'orderable' => false,
        			'searchable' => false,
        			'className' => "center aligned",
        			'width' => '40px',
        			'sortable' => true,
        		],
        		/* --------------------------- */
        		[
        			'data' => 'no_order',
        			'name' => 'no_order',
        			'label' => 'No Order',
        			'className' => "center aligned",
        			'searchable' => false,
        			'width' => '150px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'tgl_order',
        			'name' => 'tgl_order',
        			'label' => 'Tgl Order',
        			'className' => "center aligned",
        			'searchable' => false,
        			'width' => '150px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'no_surat',
        			'name' => 'no_surat',
        			'label' => 'No Surat Permintaan',
        			'className' => "center aligned",
        			'searchable' => false,
        			'width' => '150px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'layanan',
        			'name' => 'layanan',
        			'label' => 'Layanan',
        			'searchable' => false,
        			'className' => "center aligned",
        			'width' => '200px',
        			'sortable' => true,
        		],
        		[
        			'data' => 'lingkup',
        			'name' => 'lingkup',
        			'label' => 'Lingkup',
        			'searchable' => false,
        			'sortable' => true,
        			'className' => "center aligned",
        			'width' => '200px',
        		],
        		[
        			'data' => 'jenis_pengujian',
        			'name' => 'jenis_pengujian',
        			'label' => 'Jenis Pengujian',
        			'searchable' => false,
        			'sortable' => true,
        			'className' => "left aligned",
        			'width' => '150px',
        		],
        		[
        			'data' => 'pemesan',
        			'name' => 'pemesan',
        			'label' => 'Peminta Jasa',
        			'searchable' => false,
        			'sortable' => true,
        			'className' => "center aligned",
        			'width' => '300px',
        		],
        		[
        			'data' => 'briefing',
        			'name' => 'briefing',
        			'label' => 'Briefing Teknis',
        			'searchable' => false,
        			'sortable' => false,
        			'className' => "center aligned",
        			'width' => '50px',
        		],
        		[
        			'data' => 'action',
        			'name' => 'action',
        			'label' => 'Aksi',
        			'searchable' => false,
        			'sortable' => false,
        			'className' => "center aligned",
        			'width' => '100px',
        		],
        		[
        			'data' => 'status',
        			'name' => 'status',
        			'label' => 'Status Kaji Ulang',
        			'searchable' => false,
        			'sortable' => true,
        			'className' => "center aligned",
        			'width' => '150px',
        		],
        		[
        			'data' => 'status_va',
        			'name' => 'status_va',
        			'label' => 'Status VA',
        			'searchable' => false,
        			'sortable' => true,
        			'className' => "center aligned",
        			'width' => '150px',
        		],
        		[
        			'data' => 'status_bayar',
        			'name' => 'status_bayar',
        			'label' => 'Status Bayar',
        			'searchable' => false,
        			'sortable' => true,
        			'className' => "center aligned",
        			'width' => '200px',
        		],
        	],
        ];
    }

    public function grid(Request $request)
    {
    	$user = auth()->user();
    	if($user->hasRole(['msb-kalibrasi', 'msb-siskit', 'msb-sistgi', 'msb-tegangan-rendah', 'msb-tegangan-tinggi','admin'])){
	        $records = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->where('penerima_id', $user->id)
	                            				  ->where('jenis', 1);
	                          				})
	        								->whereIn('status', [2,12])
	        								->doesntHave('kaji_ulang')
	        								->where('tipe_surat', 0)
	        								->select('*');
	     	if (!isset(request()->order[0]['column'])) {
            	$records->orderBy('created_at','desc');
        	}

            if ($no_surat = $request->no_surat) {
              $records->where('no_surat','like', '%'.$no_surat.'%');
            }
            if ($no_order = $request->no_order) {
              $records->where('no_order','like', '%'.$no_order.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
              $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
              $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
              });
            }

            $records = $records->get();
    	}elseif($user->hasRole(['asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-tinggi','asman-dal-tegangan-rendah', 'asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-tinggi','asman-lola-tegangan-rendah','admin'])){
    		$records = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->where('penerima_id', $user->id)
	                            				  ->where('jenis', 2);
	                          				})
	        								->where('status', 2)
	        								->doesntHave('kaji_ulang')
	        								->where('tipe_surat', 0)
	        								->select('*');
	        if (!isset(request()->order[0]['column'])) {
            	$records->orderBy('created_at','desc');
        	}

            if ($no_surat = $request->no_surat) {
              $records->where('no_surat','like', '%'.$no_surat.'%');
            }
            if ($no_order = $request->no_order) {
              $records->where('no_order','like', '%'.$no_order.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
              $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
              $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
              });
            }
            $records = $records->get();
    	}else{
    		$records = collect([]);
    	}
        //Init Sort
       
        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('tgl_order', function ($record) use ($request) {
            	return DateToStringYear($record->tgl_order);
            })
            ->addColumn('pemesan', function ($record) use ($request) {
            	$pemesan = $record->user->pelanggans->perusahaan->nama.'</br>'.$record->user->pelanggans->nama_lengkap.'</br>'.$record->user->pelanggans->no_hp;
            	return $pemesan;
            })
            ->addColumn('layanan', function ($record) use ($request) {
            	return $record->pelayanan->nama;
            })
            ->addColumn('lingkup', function ($record) use ($request) {
            	return $record->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
            	$jenis_pengujian = '';
            	if($record->detail){
	            	$jenis_pengujian .= '<div class="ui ordered list list-more1" data-display="3">';
	            	foreach ($record->detail as $key => $value) {
	            		$kaji_ulang = KajiUlang::where('pp_id', $record->id)->first();
	            		if($kaji_ulang){
	            			foreach ($kaji_ulang->detail as $kuy => $val) {
	            				if($val->jenis_id == $value->id){
				            		$jenis_pengujian .= '<div class="item">&nbsp;&nbsp;&nbsp;&nbsp;<a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
	            				}
	            			}
	            		}else{
	            			$jenis_pengujian .= '<div class="item">&nbsp;&nbsp;&nbsp;&nbsp;<a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
	            		}
	            	}
	            	$jenis_pengujian .='</div>';
            	}
            	return $jenis_pengujian;
            })
            ->editColumn('timestamp', function ($record) {
                $time = $record->act_dispo->where('tipe', 2)->where('jenis', 1)->first()->created_at;
                $time = Carbon::parse($time)->addWeekdays(getHariKerja(url($this->link),$record->layanan_id));
                return $time->format('Y-m-d H:i:s');
            })
            ->editColumn('catatan', function ($record) {
            	return '<span class="ccount more" data-ccount="80">'.readMoreText(strip_tags($record->act_dispo->where('tipe', 2)->where('jenis', 1)->first()->keterangan),150).'</span>';
            })
            ->editColumn('status', function ($record) {
            	$status ='<a class="ui teal tag label">Menunggu Kaji Ulang</a>';
            	$kaji_ulang = KajiUlang::where('pp_id', $record->id)->first();
            	if($kaji_ulang){
            		if($kaji_ulang->keputusan == 1){
						$status ='<a class="ui tag label" style="background-color:#00abffcc;">DITERIMA</a>';
            		}elseif($kaji_ulang->keputusan == 2){
            			$status ='<a class="ui tag label" style="background-color:#dd0000cc;">DITOLAK</a>';
            		}else{
            			$status ='<a class="ui tag label" style="background-color:#dddd00cc;">DIDISKUSIKAN</a>';
            		}
            	}
                if($record->status==12){
                    $status .='<br><br> <a class="ui orange tag label perbaikan-message" data-id="'.$record->id.'">Perbaikan</a>';
                }
            	return $status;
            })
            ->addColumn('action', function ($record) use ($link){
                $kaji_ulang = KajiUlang::where('pp_id', $record->id)->first();

                $btn = '';

                $btn .= $this->makeButton([
                    'type' => 'url',
                    'class'   => 'teal icon',
                    'label'   => '<i class="eye text icon"></i>',
                    'tooltip' => 'Detil Data',
                    'url'  => url($link.$record->id)
                ]);
                if($kaji_ulang){
                }else{
                	$user = auth()->user();
        			$penerima_id = $record->act_dispo->sortByDesc('created_at')->first()->penerima_id;
                	if($user->hasRole(['msb-kalibrasi', 'msb-siskit', 'msb-sistgi', 'msb-tegangan-rendah', 'msb-tegangan-tinggi'])){
                		// return $record->act_kaji_ulang->count();
            			// $jenis = $record->act_dispo->sortByDesc('created_at')->first()->jenis;
            			// $tipe = $record->act_dispo->sortByDesc('created_at')->first()->tipe;

            			if($penerima_id = $record->act_dispo->sortByDesc('created_at')->first()->penerima_id == $user->id){
            				if($record->status==12){
            				}else{
		            			$btn .= $this->makeButton([
		                			'type' => 'modal',
		                			'class'   => 'user blue icon dispo',
		                			'label'   => '<i class="user icon"></i>',
		                			'tooltip' => 'Disposisi',
		                			'datas' => [
		                				'id' => $record->id
		                			],
		                			'id'   => $record->id
		                		]);
            				}
            			}
                	}elseif($user->hasRole(['asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-tinggi','asman-dal-tegangan-rendah', 'asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-tinggi','asman-lola-tegangan-rendah'])){
                		if($penerima_id = $record->act_dispo->sortByDesc('created_at')->first()->penerima_id == $user->id){
		                	$btn .= $this->makeButton([
			                	'type' => 'url',
			                	'tooltip' => 'Buat Kaji Ulang',
			                	'class' => 'ui violet mini icon button',
			                	'label' => '<i class="edit icon"></i>',
			                	'url' => url($this->link.$record->id.'/edit')
			                ]);

	                        $btn .= $this->makeButton([
	                            'type' => 'modal',
	                            'class'   => 'user orange icon perbaikan',
	                            'label'   => '<i class="undo icon"></i>',
	                            'tooltip' => 'Perbaikan',
	                            'datas' => [
	                                'id' => $record->id
	                            ],
	                            'id'   => $record->id
	                        ]);
                		}
                	}else{

                	}
                }
                if($record->status==12 || $record->act_dispo->where('jenis', 2)->count() == 0){
                    $btn .= $this->makeButton([
                        'type' => 'modal',
                        'class'   => 'user orange icon perbaikan',
                        'label'   => '<i class="undo icon"></i>',
                        'tooltip' => 'Perbaikan',
                        'datas' => [
                            'id' => $record->id
                        ],
                        'id'   => $record->id
                    ]);
                }
                return $btn;
            })
            ->rawColumns(['briefing','pemesan','jenis_pengujian','action','detil','catatan','status'])
            ->make(true);
    }

    public function diskusi(Request $request)
    {
    	$user = auth()->user();
    	if($user->hasRole(['msb-kalibrasi', 'msb-siskit', 'msb-sistgi', 'msb-tegangan-rendah', 'msb-tegangan-tinggi','admin'])){
	        $records = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->where('penerima_id', $user->id)
	                            				  ->where('jenis', 1);
	                          				})
	        								->where('status', 2)
	        								->whereHas('kaji_ulang', function($u){
	        									$u->where('keputusan', 3);
	        								})
	        								->where('tipe_surat', 0)
	        								->select('*');
	        if (!isset(request()->order[0]['column'])) {
            	$records->orderBy('created_at','desc');
        	}

            if ($no_surat = $request->no_surat) {
              $records->where('no_surat','like', '%'.$no_surat.'%');
            }
            if ($no_order = $request->no_order) {
              $records->where('no_order','like', '%'.$no_order.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
              $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
              $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
              });
            }
            $records = $records->get();
    	}elseif($user->hasRole(['asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-tinggi','asman-dal-tegangan-rendah', 'asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-tinggi','asman-lola-tegangan-rendah'])){
    		$records = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->where('penerima_id', $user->id)
	                            				  ->where('jenis', 2);
	                          				})
	        								->where('status', 2)
	        								->whereHas('kaji_ulang', function($u){
	        									$u->where('keputusan', 3);
	        								})
	        								->where('tipe_surat', 0)
	        								->select('*');
	        if (!isset(request()->order[0]['column'])) {
            	$records->orderBy('created_at','desc');
        	}

            if ($no_surat = $request->no_surat) {
              $records->where('no_surat','like', '%'.$no_surat.'%');
            }
            if ($no_order = $request->no_order) {
              $records->where('no_order','like', '%'.$no_order.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
              $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
              $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
              });
            }
            $records = $records->get();
    	}else{
    		$records = collect([]);
    	}

        
        //Init Sort

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('tgl_order', function ($record) use ($request) {
            	return DateToStringYear($record->tgl_order);
            })
            ->addColumn('pemesan', function ($record) use ($request) {
            	$pemesan = $record->user->pelanggans->perusahaan->nama.'</br>'.$record->user->pelanggans->nama_lengkap.'</br>'.$record->user->pelanggans->no_hp;
            	return $pemesan;
            })
            ->addColumn('layanan', function ($record) use ($request) {
            	return $record->pelayanan->nama;
            })
            ->addColumn('lingkup', function ($record) use ($request) {
            	return $record->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
            	$jenis_pengujian = '';
            	if($record->detail){
	            	$jenis_pengujian .= '<div class="ui ordered list list-more2" data-display="3">';
	            	foreach ($record->detail as $key => $value) {
	            		$kaji_ulang = KajiUlang::where('pp_id', $record->id)->first();
	            		if($kaji_ulang){
	            			foreach ($kaji_ulang->detail as $kuy => $val) {
	            				if($val->jenis_id == $value->id){
				            		$jenis_pengujian .= '<div class="item">&nbsp;&nbsp;&nbsp;&nbsp;<a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
	            				}
	            			}
	            		}else{
	            			$jenis_pengujian .= '<div class="item">&nbsp;&nbsp;&nbsp;&nbsp;<a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
	            		}
	            	}
	            	$jenis_pengujian .='</div>';
            	}
            	return $jenis_pengujian;
            })
            ->editColumn('timestamp', function ($record) {
            	$Date1 = $record->act_dispo->where('tipe', 2)->where('jenis', 1)->first()->created_at->addWeekdays(4);
            	$Date3 = $record->kaji_ulang->created_at;
            	$diff  = $Date3->diffInSeconds($Date1);
            	$s 	   = (int)$diff%60;
				$m 	   = (int)floor(($diff%3600)/60);
				$h 	   = (int)floor(($diff%86400)/3600);
				$d 	   = (int)floor(($diff%2592000)/86400);
				if($Date3 > $Date1){
	                return '<label class="ui tag top-barz colors"><b> + '.$d.' D : '.$h.' J : '.$m.' M : '.$s.' D</b></label>';
				}else{
	                return '<label class="ui tag top-barz colors"><b> - '.$d.' D : '.$h.' J : '.$m.' M : '.$s.' D</b></label>';
				}
            })
            ->editColumn('catatan', function ($record) {
            	return '<span class="ccount more" data-ccount="80">'.readMoreText(strip_tags($record->act_dispo->where('tipe', 2)->where('jenis', 1)->first()->keterangan),150).'</span>';
            })
            ->editColumn('status', function ($record) {
            	$status ='<a class="ui teal tag label">Menunggu Kaji Ulang</a>';
            	$kaji_ulang = KajiUlang::where('pp_id', $record->id)->first();
            	if($kaji_ulang){
            		if($kaji_ulang->keputusan == 1){
						$status ='<a class="ui tag label" style="background-color:#00abffcc;">DITERIMA</a>';
            		}elseif($kaji_ulang->keputusan == 2){
            			$status ='<a class="ui tag label" style="background-color:#dd0000cc;">DITOLAK</a>';
            		}else{
            			$status ='<a class="ui tag label" style="background-color:#dddd00cc;">DIDISKUSIKAN</a>';
            		}
            	}
            	return $status;
            })
            ->addColumn('briefing', function ($record) use ($link){
            	$user = auth()->user();
                $btn = '';
                if($record->pelayanan->id == 3){
			    	if($user->hasRole(['asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-tinggi','asman-dal-tegangan-rendah', 'asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-tinggi','asman-lola-tegangan-rendah'])){
			    		if(isset($record)){
			    			if(isset($record->kaji_ulang)){
			    				if($record->kaji_ulang->files->where('type', 1)->count()>0){
			    					if($record->kaji_ulang->files->where('type', 2)->count()>0){
			    						$btn .= $this->makeButton([
				    						'type' => 'url',
				    						'tooltip' => 'Download Jadwal',
				    						'class' => 'ui blue mini icon button',
				    						'label' => '<i class="download icon"></i>',
				    						'url' => url('download-briefing', $record->kaji_ulang->id).'/briefing-teknis/1'
				    					]);

				    					$btn .= $this->makeButton([
				    						'type' => 'url',
				    						'tooltip' => 'Download Notulen',
				    						'class' => 'ui teal mini icon button',
				    						'label' => '<i class="download icon"></i>',
				    						'url' => url('download-briefing', $record->kaji_ulang->id).'/briefing-teknis/2'
				    					]);
			    					}else{
			    						$btn .= $this->makeButton([
				    						'type' => 'url',
				    						'tooltip' => 'Download Jadwal',
				    						'class' => 'ui blue mini icon button',
				    						'label' => '<i class="download icon"></i>',
				    						'url' => url('download-briefing', $record->kaji_ulang->id).'/briefing-teknis/1'
				    					]);

				    					$btn .= $this->makeButton([
				    						'type' => 'modal',
				    						'class'   => 'blue icon briefing-notulen',
				    						'label'   => '<i class="handshake outline text icon"></i>',
				    						'tooltip' => 'Upload Notulen',
				    						'datas' => [
				    							'id' => $record->id
				    						],
				    						'id'   => $record->id
				    					]);
			    					}
			    				}else{
					                $btn .= $this->makeButton([
					                	'type' => 'modal',
					                	'class'   => 'orange icon briefing-jadwal',
					                	'label'   => '<i class="handshake outline text icon"></i>',
					                	'tooltip' => 'Upload Jadwal',
					                	'datas' => [
					                		'id' => $record->id
					                	],
					                	'id'   => $record->id
					                ]);
			    				}
			    			}
			    		}
			    	}else{
			    		if(isset($record)){
			    			if(isset($record->kaji_ulang)){
			    				if($record->kaji_ulang->files->where('type', 1)->count()>0){
			    					if($record->kaji_ulang->files->where('type', 2)->count()>0){
			    						$btn .= $this->makeButton([
				    						'type' => 'url',
				    						'tooltip' => 'Download Jadwal',
				    						'class' => 'ui blue mini icon button',
				    						'label' => '<i class="download icon"></i>',
				    						'url' => url('download-briefing', $record->kaji_ulang->id).'/briefing-teknis/1'
				    					]);

				    					$btn .= $this->makeButton([
				    						'type' => 'url',
				    						'tooltip' => 'Download Notulen',
				    						'class' => 'ui teal mini icon button',
				    						'label' => '<i class="download icon"></i>',
				    						'url' => url('download-briefing', $record->kaji_ulang->id).'/briefing-teknis/2'
				    					]);
			    					}else{
			    						$btn .= $this->makeButton([
				    						'type' => 'url',
				    						'tooltip' => 'Download Jadwal',
				    						'class' => 'ui blue mini icon button',
				    						'label' => '<i class="download icon"></i>',
				    						'url' => url('download-briefing', $record->kaji_ulang->id).'/briefing-teknis/1'
				    					]);
			    					}
			    				}else{
			    					$btn .= '-';
			    				}
			    			}
			    		}
			    	}
                }else{
                	$btn .= '-';
                }
                return $btn;
            })
            ->addColumn('action', function ($record) use ($link){
            	$user = auth()->user();
                $kaji_ulang = KajiUlang::where('pp_id', $record->id)->where('tipe', 0)->first();

                $btn = '';

                $btn .= $this->makeButton([
                    'type' => 'url',
                    'class'   => 'teal icon',
                    'label'   => '<i class="eye text icon"></i>',
                    'tooltip' => 'Detil Data',
                    'url'  => url($link.$record->id)
                ]);
                if($kaji_ulang){
                	if($user->hasRole(['asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-tinggi','asman-dal-tegangan-rendah', 'asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-tinggi','asman-lola-tegangan-rendah'])){
	                	$btn .= $this->makeButton([
		                	'type' => 'url',
		                	'tooltip' => 'Ubah Kaji Ulang',
		                	'class' => 'ui violet mini icon button',
		                	'label' => '<i class="edit icon"></i>',
		                	'url' => url($this->link.$record->id.'/ubah')
		                ]);
                	}
                }else{
                	if($user->hasRole(['msb-kalibrasi', 'msb-siskit', 'msb-sistgi', 'msb-tegangan-rendah', 'msb-tegangan-tinggi'])){
                		if($record->act_dispo->where('jenis', 2)->count() == 0){
                			$btn .= $this->makeButton([
	                			'type' => 'modal',
	                			'class'   => 'user blue icon dispo',
	                			'label'   => '<i class="user icon"></i>',
	                			'tooltip' => 'Disposisi',
	                			'datas' => [
	                				'id' => $record->id
	                			],
	                			'id'   => $record->id
	                		]);
                		}
                	}elseif($user->hasRole(['asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-tinggi','asman-dal-tegangan-rendah', 'asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-tinggi','asman-lola-tegangan-rendah'])){
	                	$btn .= $this->makeButton([
		                	'type' => 'url',
		                	'tooltip' => 'Buat Kaji Ulang',
		                	'class' => 'ui violet mini icon button',
		                	'label' => '<i class="edit icon"></i>',
		                	'url' => url($this->link.$record->id.'/edit')
		                ]);
                	}else{

                	}
                }
                return $btn;
            })
            ->rawColumns(['briefing','pemesan','jenis_pengujian','action','detil','catatan','status','timestamp'])
            ->make(true);
    }

    public function histori(Request $request)
    {
        $user = auth()->user();
    	if($user->hasRole(['msb-kalibrasi', 'msb-siskit', 'msb-sistgi', 'msb-tegangan-rendah', 'msb-tegangan-tinggi','admin'])){
	        $records = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->where('penerima_id', $user->id)
	                            				  ->where('jenis', 1);
	                          				})
	        								->whereIn('status', [2,3,4,5,6,7,8,9,10])
	        								->whereHas('kaji_ulang', function($e){
	        									$e->whereIn('keputusan', [1,2]);
	        								})
	        								->where('tipe_surat', 0)
	        								->select('*');
	        if (!isset(request()->order[0]['column'])) {
            	$records->orderBy('created_at','desc');
        	}

            if ($no_surat = $request->no_surat) {
              $records->where('no_surat','like', '%'.$no_surat.'%');
            }
            if ($no_order = $request->no_order) {
              $records->where('no_order','like', '%'.$no_order.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
              $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
              $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
              });
            }
            $records = $records->get();
    	}elseif($user->hasRole(['asman-dal-kalibrasi','asman-lola-kalibrasi'])){
    		$records = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->where('layanan_id', 1)
	                            				  ->where('jenis', 2);
	                          				})
	        								->whereIn('status', [2,3,4,5,6,7,8,9,10])
	        								->whereHas('kaji_ulang', function($e){
	        									$e->whereIn('keputusan', [1,2]);
	        								})
	        								->where('tipe_surat', 0)
	        								->select('*');
	        if (!isset(request()->order[0]['column'])) {
            	$records->orderBy('created_at','desc');
        	}

            if ($no_surat = $request->no_surat) {
              $records->where('no_surat','like', '%'.$no_surat.'%');
            }
            if ($no_order = $request->no_order) {
              $records->where('no_order','like', '%'.$no_order.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
              $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
              $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
              });
            }
            $records = $records->get();
    	}elseif($user->hasRole(['asman-dal-siskit','asman-lola-siskit'])){
    		$records = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->where('layanan_id', 2)
	                            				  ->where('jenis', 2);
	                          				})
	        								->whereIn('status', [2,3,4,5,6,7,8,9,10])
	        								->whereHas('kaji_ulang', function($e){
	        									$e->whereIn('keputusan', [1,2]);
	        								})
	        								->where('tipe_surat', 0)
	        								->select('*');
	        if (!isset(request()->order[0]['column'])) {
            	$records->orderBy('created_at','desc');
        	}

            if ($no_surat = $request->no_surat) {
              $records->where('no_surat','like', '%'.$no_surat.'%');
            }
            if ($no_order = $request->no_order) {
              $records->where('no_order','like', '%'.$no_order.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
              $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
              $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
              });
            }
            $records = $records->get();
    	}elseif($user->hasRole(['asman-dal-sistgi','asman-lola-sistgi'])){
    		$records = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->where('layanan_id', 3)
	                            				  ->where('jenis', 2);
	                          				})
	        								->whereIn('status', [2,3,4,5,6,7,8,9,10])
	        								->whereHas('kaji_ulang', function($e){
	        									$e->whereIn('keputusan', [1,2]);
	        								})
	        								->where('tipe_surat', 0)
	        								->select('*');
	        if (!isset(request()->order[0]['column'])) {
            	$records->orderBy('created_at','desc');
        	}

            if ($no_surat = $request->no_surat) {
              $records->where('no_surat','like', '%'.$no_surat.'%');
            }
            if ($no_order = $request->no_order) {
              $records->where('no_order','like', '%'.$no_order.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
              $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
              $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
              });
            }
            $records = $records->get();
    	}elseif($user->hasRole(['asman-dal-tegangan-rendah','asman-lola-tegangan-rendah'])){
    		$records = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->where('layanan_id', 4)
	                            				  ->where('jenis', 2);
	                          				})
	        								->whereIn('status', [2,3,4,5,6,7,8,9,10])
	        								->whereHas('kaji_ulang', function($e){
	        									$e->whereIn('keputusan', [1,2]);
	        								})
	        								->where('tipe_surat', 0)
	        								->select('*');
	        if (!isset(request()->order[0]['column'])) {
            	$records->orderBy('created_at','desc');
        	}

            if ($no_surat = $request->no_surat) {
              $records->where('no_surat','like', '%'.$no_surat.'%');
            }
            if ($no_order = $request->no_order) {
              $records->where('no_order','like', '%'.$no_order.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
              $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
              $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
              });
            }
            $records = $records->get();
    	}elseif($user->hasRole(['asman-dal-tegangan-tinggi','asman-lola-tegangan-tinggi'])){
    		$records = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->where('layanan_id', 5)
	                            				  ->where('jenis', 2);
	                          				})
	        								->whereIn('status', [2,3,4,5,6,7,8,9,10])
	        								->whereHas('kaji_ulang', function($e){
	        									$e->whereIn('keputusan', [1,2]);
	        								})
	        								->where('tipe_surat', 0)
	        								->select('*');
	        if (!isset(request()->order[0]['column'])) {
            	$records->orderBy('created_at','desc');
        	}

            if ($no_surat = $request->no_surat) {
              $records->where('no_surat','like', '%'.$no_surat.'%');
            }
            if ($no_order = $request->no_order) {
              $records->where('no_order','like', '%'.$no_order.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
              $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
              $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
              });
            }
            $records = $records->get();
    	}elseif($user->hasRole(['yan-kalibrasi'])){
    		$records = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->where('layanan_id', 1)
	                            				  ->where('jenis', 2);
	                          				})
	        								->whereIn('status', [2,3,4,5,6,7,8,9,10])
	        								->whereHas('kaji_ulang', function($e){
	        									$e->whereIn('keputusan', [1,2]);
	        								})
	        								->where('tipe_surat', 0)
	        								->select('*');
	        if (!isset(request()->order[0]['column'])) {
            	$records->orderBy('created_at','desc');
        	}

            if ($no_surat = $request->no_surat) {
              $records->where('no_surat','like', '%'.$no_surat.'%');
            }
            if ($no_order = $request->no_order) {
              $records->where('no_order','like', '%'.$no_order.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
              $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
              $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
              });
            }
            $records = $records->get();
    	}elseif($user->hasRole(['yan-uji'])){
    		$records = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->whereIn('layanan_id', [2,3,4,5])
	                            				  ->where('jenis', 2);
	                          				})
	        								->whereIn('status', [2,3,4,5,6,7,8,9,10])
	        								->whereHas('kaji_ulang', function($e){
	        									$e->whereIn('keputusan', [1,2]);
	        								})
	        								->where('tipe_surat', 0)
	        								->select('*');
	        if (!isset(request()->order[0]['column'])) {
            	$records->orderBy('created_at','desc');
        	}

            if ($no_surat = $request->no_surat) {
              $records->where('no_surat','like', '%'.$no_surat.'%');
            }
            if ($no_order = $request->no_order) {
              $records->where('no_order','like', '%'.$no_order.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
              $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
              $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
              });
            }
            $records = $records->get();
    	}else{
    		$records = collect([]);
    	}

        //Init Sort

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('tgl_order', function ($record) use ($request) {
            	return DateToStringYear($record->tgl_order);
            })
            ->addColumn('pemesan', function ($record) use ($request) {
            	$pemesan = $record->user->pelanggans->perusahaan->nama.'</br>'.$record->user->pelanggans->nama_lengkap.'</br>'.$record->user->pelanggans->no_hp;
            	return $pemesan;
            })
            ->addColumn('layanan', function ($record) use ($request) {
            	return $record->pelayanan->nama;
            })
            ->addColumn('lingkup', function ($record) use ($request) {
            	return $record->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
            	$jenis_pengujian = '';
            	if($record->detail){
	            	$jenis_pengujian .= '<div class="ui ordered list list-more3" data-display="3">';
	            	foreach ($record->detail as $key => $value) {
	            		$kaji_ulang = KajiUlang::where('pp_id', $record->id)->first();
	            		if($kaji_ulang){
	            			foreach ($kaji_ulang->detail as $kuy => $val) {
	            				if($val->jenis_id == $value->id){
				            		$jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
	            				}
	            			}
	            		}else{
	            			$jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
	            		}
	            	}
	            	$jenis_pengujian .='</div>';
            	}
            	return $jenis_pengujian;
            })
            ->editColumn('catatan', function ($record) {
            	return '<span class="ccount more" data-ccount="80">'.readMoreText(strip_tags($record->act_dispo->where('tipe', 2)->where('jenis', 1)->first()->keterangan),150).'</span>';
            })
            ->editColumn('status', function ($record) {
            	$status ='<a class="ui teal tag label">Menunggu Kaji Ulang</a>';
            	$kaji_ulang = KajiUlang::where('pp_id', $record->id)->first();
            	if($kaji_ulang){
            		if($kaji_ulang->keputusan == 1){
						$status ='<a class="ui tag label" style="background-color:#00abffcc;">DITERIMA</a>';
            		}elseif($kaji_ulang->keputusan == 2){
            			$status ='<a class="ui tag label" style="background-color:#dd0000cc;">DITOLAK</a>';
            		}else{
            			$status ='<a class="ui tag label" style="background-color:#dddd00cc;">DIDISKUSIKAN</a>';
            		}
            	}
            	return $status;
            })
            ->editColumn('status_va', function ($record) {
            	if($record->kaji_ulang->surat){
            		if($record->kaji_ulang->surat->va){
            			if($record->kaji_ulang->surat->va->status == 1){
            				$status ='<a class="ui green tag label">Aktif</a>';
            			}elseif($record->kaji_ulang->surat->va->status == 3){
            				$status ='<a class="ui green tag label">Aktif</a>';
            			}else{
            				$status ='<a class="ui red tag label">Non Aktif</a>';
            			}
            		}else{
            			$status ='<a class="ui red tag label">Non Aktif</a>';
            		}
            	}else{
	            	$status ='<a class="ui red tag label">Non Aktif</a>';
            	}
            	return $status;
            })
            ->editColumn('status_bayar', function ($record) {
            	if($record->kaji_ulang->surat){
            		if($record->kaji_ulang->surat->va){
            			if($record->kaji_ulang->surat->va->status == 3){
            				if($record->kaji_ulang->surat->va->konfirmasi){
            					if($record->kaji_ulang->surat->va->konfirmasi->status == 3){
            						$status ='<a class="ui green tag label">Lunas Dan Sudah Terbit WBS/IO</a>';
            					}elseif($record->kaji_ulang->surat->va->konfirmasi->status == 2){
            						$status ='<a class="ui blue tag label">Lebih Bayar Dan Sudah Terbit WBS/IO</a>';
            					}else{
            						$status ='<a class="ui orange tag label">Kurang Bayar Dan Sudah Terbit WBS/IO</a>';
            					}
            				}else{
            				    $status ='<a class="ui red tag label">Belum Bayar</a>';
            				}
            			}else{
            				$status ='<a class="ui red tag label">Belum Bayar</a>';
            			}
            		}else{
	            		$status ='<a class="ui red tag label">Belum Bayar</a>';
            		}
            	}else{
	            	$status ='<a class="ui red tag label">Belum Bayar</a>';
            	}
      //       	$kaji_ulang = KajiUlang::where('pp_id', $record->id)->first();
      //       	if($kaji_ulang){
      //       		if($kaji_ulang->keputusan == 1){
						// $status ='<a class="ui green tag label">Diterima</a>';
      //       		}elseif($kaji_ulang->keputusan == 2){
      //       			$status ='<a class="ui red tag label">Ditolak</a>';
      //       		}else{
      //       			$status ='<a class="ui yellow tag label">Didiskusikan</a>';
      //       		}
      //       	}
            	return $status;
            })
            ->addColumn('briefing', function ($record) use ($link){
                 $btn = '';
	                if($record->pelayanan->id == 3){
			            if(isset($record)){
							 if(isset($record->kaji_ulang)){
								if($record->kaji_ulang->files->count()>0){
									$btn .= $this->makeButton([
										'type' => 'url',
										'tooltip' => 'Download Jadwal',
										'class' => 'ui blue mini icon button',
										'label' => '<i class="download icon"></i>',
										'url' => url('download-briefing', $record->kaji_ulang->id).'/briefing-teknis/1'
									]);

									$btn .= $this->makeButton([
										'type' => 'url',
										'tooltip' => 'Download Notulen',
										'class' => 'ui teal mini icon button',
										'label' => '<i class="download icon"></i>',
										'url' => url('download-briefing', $record->kaji_ulang->id).'/briefing-teknis/2'
									]);
					            }
					        }
					    }
	                }else{
	                }
                return $btn;
            })
            ->addColumn('action', function ($record) use ($link){
                $kaji_ulang = KajiUlang::where('pp_id', $record->id)->where('tipe', 0)->first();

                $btn = '';

                $btn .= $this->makeButton([
                    'type' => 'url',
                    'class'   => 'teal icon',
                    'label'   => '<i class="eye text icon"></i>',
                    'tooltip' => 'Detil Data',
                    'url'  => url($link.$record->id)
                ]);
                if($kaji_ulang){
                	
                }else{
                	$btn .= $this->makeButton([
	                	'type' => 'url',
	                	'tooltip' => 'Ubah Data',
	                	'class' => 'ui violet mini icon button',
	                	'label' => '<i class="edit icon"></i>',
	                	'url' => url($this->link.$record->id.'/edit')
	                ]);
                }
                return $btn;
            })
            ->rawColumns(['briefing','pemesan','jenis_pengujian','action','detil','catatan','status','status_va','status_bayar'])
            ->make(true);
    }

    public function index()
    {	
    	$user = auth()->user();
    	if($user->hasRole(['msb-kalibrasi', 'msb-siskit', 'msb-sistgi', 'msb-tegangan-rendah', 'msb-tegangan-tinggi','admin'])){
	        $records = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->where('penerima_id', $user->id)
	                            				  ->where('jenis', 1);
	                          				})
	        								->whereIn('status', [2,12])
	        								->doesntHave('kaji_ulang')
	        								->where('tipe_surat', 0)
	        								->orderBy('no_order','asc')->count();
	        $diskusi = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->where('penerima_id', $user->id)
	                            				  ->where('jenis', 1);
	                          				})
	        								->where('status', 2)
	        								->whereHas('kaji_ulang', function($u){
	        									$u->where('keputusan', 3);
	        								})
	        								->where('tipe_surat', 0)
	        								->orderBy('no_order','asc')->count();
    	}elseif($user->hasRole(['asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-tinggi','asman-dal-tegangan-rendah', 'asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-tinggi','asman-lola-tegangan-rendah','admin'])){
    		$records = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->where('penerima_id', $user->id)
	                            				  ->where('jenis', 2);
	                          				})
	        								->where('status', 2)
	        								->doesntHave('kaji_ulang')
	        								->where('tipe_surat', 0)
	        								->orderBy('no_order','asc')->count();
	       	$diskusi = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->where('pengirim_id', $user->id)
	                            				  ->where('jenis', 2);
	                          				})
	        								->where('status', 2)
	        								->whereHas('kaji_ulang', function($u){
	        									$u->where('keputusan', 3);
	        								})
	        								->where('tipe_surat', 0)
	        								->orderBy('no_order','asc')->count();
    	}else{
    		$records = 0;
    		$diskusi = 0;
    	}

    	return $this->render('modules.kaji-ulang.index', [
        	'mockup' => true,
        	'structs' => $this->listStructs,
        	'data' => $records,
        	'diskusi' => $diskusi,
        ]);
    }

    public function create()
    {
        return $this->render('modules.kaji-ulang.create');
    }

    public function store(Request $request)
    {
        
    }

    public function detail($id)
    {
    	$detail = Detail::find($id);
    	$pp = PendaftaranPengujian::find($id);
        return $this->render('modules.kaji-ulang.detail-pengujian',[
        	'record' => $detail,
        	'pp' => $pp,
        ]);
    }

    public function edit($id)
    {	
    	$record = PendaftaranPengujian::find($id);
       	$kaji_ulang = KajiUlang::where('pp_id', $id)->first();
       	$this->setTitle('Buat Kaji Ulang');
       	$this->setSubtitle('No Order : '.$record->no_order);
        $this->setBreadcrumb(['Kaji Ulang' => '#', 'Buat' => '#']);
    	$record = PendaftaranPengujian::find($id);
        return $this->render('modules.kaji-ulang.edit',['record' => $record]);
    }

    public function ubah($id)
    {	
    	$record = PendaftaranPengujian::find($id);
       	$kaji_ulang = KajiUlang::where('pp_id', $id)->first();
       	$this->setTitle('Ubah Kaji Ulang');
       	$this->setSubtitle('No Order : '.$record->no_order);
        $this->setBreadcrumb(['Kaji Ulang' => '#', 'Ubah' => '#']);
    	$record = PendaftaranPengujian::find($id);
        return $this->render('modules.kaji-ulang.ubah',[
        	'record' => $record,
        	'kaji_ulang' => $kaji_ulang
    	]);
    }

    public function dispo($id)
    {
    	$record = PendaftaranPengujian::with('user.pelanggans')->find($id);
        // $users = Users::with('detail')->whereHas('detail',function($q) use($record){
        //     $q->where('jenis_pelayanan_id',$record->jenis_layanan_id);
        // })->get();
        return $this->render('modules.kaji-ulang.create-disposisi', [
            'record' => $record,
            // 'users' => $users
        ]);
    }

    public function perbaikan($id)
    {
        $record = PendaftaranPengujian::with('user.pelanggans')->find($id);
        // $users = Users::with('detail')->whereHas('detail',function($q) use($record){
        //     $q->where('jenis_pelayanan_id',$record->jenis_layanan_id);
        // })->get();
        return $this->render('modules.kaji-ulang.create-perbaikan', [
            'record' => $record,
            // 'users' => $users
        ]);
    }

    public function perbaikanMessage($id)
    {
        $record = PendaftaranPengujian::with('user.pelanggans')->find($id);
        return $this->render('modules.kaji-ulang.perbaikan-message', [
            'record' => $record->latestActivity,
        ]);
    }

    public function briefingJadwal($id)
    {
    	$record = PendaftaranPengujian::with('user.pelanggans')->find($id);
    	$kaji_ulang = KajiUlang::where('pp_id', $id)->first();
        return $this->render('modules.kaji-ulang.briefing-jadwal', [
            'record' => $kaji_ulang,
        ]);
    }

    public function briefingNotulen($id)
    {
    	$record = PendaftaranPengujian::with('user.pelanggans')->find($id);
    	$kaji_ulang = KajiUlang::where('pp_id', $id)->first();
        return $this->render('modules.kaji-ulang.briefing-notulen', [
            'record' => $kaji_ulang,
        ]);
    }

    public function saveDispo(DisposisiRequest $request, $id)
    {
    	DB::beginTransaction();
        try {
            if($request->penerima_id){
                foreach ($request->penerima_id as $k => $value) {
                    $act = new ActDisposisi;
                    $act->parent_id = $id;
                    $act->pengirim_id = $request->pengirim;
                    $act->penerima_id = $value;
                    $act->tipe = $request->tipe;
                    $act->jenis = 2;
                    $act->keterangan = $request->keterangan;
                    $act->save();
                    $user = User::find($value);
                    $pp = PendaftaranPengujian::find($id);
                    if(!is_null($user->device_id)){
                          $this->onesignal('Kaji Ulang Baru dengan nomor order '.$pp->no_order,$user->device_id,$pp->toArray(),'kaji_ulang','onprogress');
                    }
                }
            }
            DB::commit();
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true
        ]);   
    }

    public function savePerbaikan(DisposisiRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            if($request->penerima_id){
                if($request->posisi=='msb'){
                    foreach ($request->penerima_id as $k => $value) {
                        $act = new ActDisposisi;
                        $act->parent_id = $id;
                        $act->pengirim_id = $request->pengirim;
                        $act->penerima_id = $value;
                        $act->tipe = $request->tipe;
                        $act->jenis = 1;
                        $act->keterangan = $request->keterangan;
                        $act->save();

                        $pp = PendaftaranPengujian::find($id);
                        $pp->status=11;
                        $pp->save();

                        $tab = 'online';
                        $mtab = 'Masuk(Online)';
                        if($pp->tipe==2){
                            $tab = 'ams';
                            $mtab = 'Masuk(AMS)';
                        }
                        if($pp->tipe==3){
                            $tab = 'manual';
                            $mtab = 'Masuk(Manual)';
                        }

                        $user = User::find($value);
                        if(!is_null($user->device_id)){
                              $this->onesignal('Perbaikan Data Penerimaan Surat '.$mtab.' dengan nomor order '.$pp->no_order,$user->device_id,$pp->toArray(),'penerimaan-surat',$tab);
                        }
                    }
                }else{
                    foreach ($request->penerima_id as $k => $value) {
                        $act = new ActDisposisi;
                        $act->parent_id = $id;
                        $act->pengirim_id = $request->pengirim;
                        $act->penerima_id = $value;
                        $act->tipe = $request->tipe;
                        $act->jenis = 2;
                        $act->keterangan = $request->keterangan;
                        $act->save();

                        $pp = PendaftaranPengujian::find($id);
                        $pp->status=12;
                        $pp->save();

                        $user = User::find($value);
                        if(!is_null($user->device_id)){
                              $this->onesignal('Perbaikan Data Kaji Ulang dengan nomor order '.$pp->no_order,$user->device_id,$pp->toArray(),'kaji_ulang','onprogress');
                        }
                    }
                }
            }
            DB::commit();
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true
        ]);   
    }

    public function saveBriefingJadwal(Request $request, $id)
    {
    	DB::beginTransaction();
        try {
        	$kaji = KajiUlang::find($id);
        	$kaji->multipleFileUploadJadwal($request->jadwal);
        	// $kaji->save();
        	$data=[
        		'email'     => $kaji->pp->user->email,
        		'url'     => url('download-briefing', $id).'/briefing-teknis/1',
        	];
        	sendEmailBriefingJadwal($data);
            DB::commit();
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true
        ]);   
    }

    public function saveBriefingNotulen(Request $request, $id)
    {
    	DB::beginTransaction();
        try {
        	$kaji = KajiUlang::find($id);
        	$kaji->multipleFileUploadNotulen($request->notulen);
        	// $kaji->save();
        	$data=[
        		'email'     => $kaji->pp->user->email,
        		'url'     => url('download-briefing', $id).'/briefing-teknis/2',
        	];
        	sendEmailBriefingNotulen($data);
            DB::commit();
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true
        ]);   
    }

    public function show($id)
    {
       $record = PendaftaranPengujian::find($id);
       $kaji_ulang = KajiUlang::where('pp_id', $id)->first();
       $this->setTitle('Detil Kaji Ulang');
       $this->setSubtitle('No Order : '.$record->no_order);
       $this->setBreadcrumb(['Kaji Ulang' => '#', 'Detil' => '#']);
       return $this->render('modules.kaji-ulang.detail',[
       		'record' => $record, 
       		'kaji_ulang' => $kaji_ulang
   		]);
    }

    public function update(KajiUlangRequest $request, $id)
    {
    	if($request->keputusan == 1){
	    	if($request->kelas){
	    	}else{
	    		return response()->json([
	                'status' => 'false',
	                'message' => 'Data Kelas tidak boleh kosong'
	            ],402);
	    	}
	    	if($request->no_surat){
	    	}else{
	    		return response()->json([
	                'status' => 'false',
	                'message' => 'Data No Surat Permintaan tidak boleh kosong'
	            ],402);
	    	}
	    	if($request->tgl_surat){
	    	}else{
	    		return response()->json([
	                'status' => 'false',
	                'message' => 'Data Tgl Surat Permintaan tidak boleh kosong'
	            ],402);
	    	}
	    	if($request->keputusan){
	    	}else{
	    		return response()->json([
	                'status' => 'false',
	                'message' => 'Data Keputusan tidak boleh kosong'
	            ],402);
	    	}
	    	DB::beginTransaction();

	    	try {
		    	$kaji_ulang = new KajiUlang;
		    	$kaji_ulang->pp_id 		= $request->id;
			    $kaji_ulang->keputusan  = $request->keputusan;
			    $kaji_ulang->keterangan  = $request->keterangan;
		        $kaji_ulang->save();

		        $act = new ActDisposisi;
	            $act->parent_id = $request->id;
	            $act->pengirim_id = auth()->user()->id;
	            $act->penerima_id = 1;
	            $act->tipe = 3;
	            $act->jenis = 2;

	            $pp = PendaftaranPengujian::find($request->id);
	            $pp->kelas     = $request->kelas;
	            $pp->no_surat  = $request->no_surat;
	            $pp->tgl_surat = $request->tgl_surat;

	            $act->status_kaji_ulang = 1;

	            $act->save();
	            $pp->save();
		        $kaji_ulang->saveDetail($request->detail);
		        
		        DB::commit();
	        }catch (\Exception $e) {
		      DB::rollback();
		      return response([
		        'status' => 'error',
		        'message' => $e,
		        'error' => $e->getMessage(),
		      ], 500);
		    }
    	}else{
    		if($request->keputusan){
	    	}else{
	    		return response()->json([
	                'status' => 'false',
	                'message' => 'Data Keputusan tidak boleh kosong'
	            ],402);
	    	}
	    	DB::beginTransaction();
	    	try {
		    	$kaji_ulang = new KajiUlang;
		    	$kaji_ulang->pp_id 		= $request->id;
			    $kaji_ulang->keputusan  = $request->keputusan;
			    $kaji_ulang->keterangan  = $request->keterangan;
		        $kaji_ulang->save();

		        $act = new ActDisposisi;
	            $act->parent_id = $request->id;
	            $act->pengirim_id = auth()->user()->id;
	            $act->penerima_id = 1;
	            $act->tipe = 3;
	            $act->jenis = 2;

	            $pp = PendaftaranPengujian::find($request->id);
	            $pp->kelas     = $request->kelas;
	            $pp->no_surat  = $request->no_surat;
	            $pp->tgl_surat = $request->tgl_surat;

	            if($request->keputusan == 2){
	            	$pp->status    = 10;
			        $act->status_kaji_ulang = 2;
	            }elseif($request->keputusan == 3){
		            $act->status_kaji_ulang = 3;
	            }else{
	            }

	            $act->save();
	            $pp->save();
		        DB::commit();
	        }catch (\Exception $e) {
		      DB::rollback();
		      return response([
		        'status' => 'error',
		        'message' => $e,
		        'error' => $e->getMessage(),
		      ], 500);
		    }
    	}

        return response([
            'status' => true
        ]);
    }


    public function saveUbah(KajiUlangRequest $request, $id)
    {	
    	if($request->keputusan == 1){
	    	DB::beginTransaction();
	    	try {
	    		$kaji_ulang = KajiUlang::find($id);
			    $kaji_ulang->keputusan  = $request->keputusan;
			    $kaji_ulang->keterangan  = $request->keterangan;
		        $kaji_ulang->save();

		        $act = new ActDisposisi;
	            $act->parent_id = $kaji_ulang->pp_id;
	            $act->pengirim_id = auth()->user()->id;
	            $act->penerima_id = 1;
	            $act->tipe = 3;
	            $act->jenis = 2;
				$act->status_kaji_ulang = 1;

	            $act->save();
		        $kaji_ulang->saveDetail($request->detail);
		        
		        DB::commit();
	        }catch (\Exception $e) {
		      DB::rollback();
		      return response([
		        'status' => 'error',
		        'message' => $e,
		        'error' => $e->getMessage(),
		      ], 500);
		    }
    	}else{
    		if($request->keputusan){
	    	}else{
	    		return response()->json([
	                'status' => 'false',
	                'message' => 'Data Keputusan tidak boleh kosong'
	            ],402);
	    	}
	    	DB::beginTransaction();
	    	try {

	    		$kaji_ulang = KajiUlang::find($id);
	    		$kaji_ulang->keputusan  = $request->keputusan;
	    		$kaji_ulang->keterangan  = $request->keterangan;
	    		$kaji_ulang->save();

	    		$act = new ActDisposisi;
	    		$act->parent_id = $kaji_ulang->pp_id;
	    		$act->pengirim_id = auth()->user()->id;
	    		$act->penerima_id = 1;
	    		$act->tipe = 3;
	    		$act->jenis = 2;

	            $pp = PendaftaranPengujian::find($request->id);
	            if($request->keputusan == 2){
	            	$pp->status    = 10;
			        $act->status_kaji_ulang = 2;
	            }elseif($request->keputusan == 3){
		            $act->status_kaji_ulang = 3;
	            }else{
	            }

	            $act->save();
	            $pp->save();
		        DB::commit();
	        }catch (\Exception $e) {
		      DB::rollback();
		      return response([
		        'status' => 'error',
		        'message' => $e,
		        'error' => $e->getMessage(),
		      ], 500);
		    }
    	}
        return response([
            'status' => true
        ]);
    }

    public function preview($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function previewMultiple($id, $type)
    {
    	if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $c->filename,
                        ];
                    }
                }
                return $this->render('pdf-multiple', [
		        	'mockup' => true,
		        	'data' => $rename,
		        ]);
            }
        }
    }

    public function download($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

    public function loadJadwal()
    {
    	return $this->render('modules.kaji-ulang.jadwal', [
                'mockup' => false,
                'data' => $this->loadData(),
        ]);
    }

    public function loadData()
    {	
    	$data = array();
    	for($iM =1;$iM<=12;$iM++){
    		if($iM < 10){
	    		$bulan = '0'.$iM;
    		}else{
    			$bulan = $iM;
    		}
    		$bulans = date("F", strtotime("$iM/12/10"));
    		$data_bulans = formatStringMonth($bulan);
    		$data_jenis = '';
	    	$penerimaan_surat 	= PendaftaranPengujian::getPenerimaanSurat($bulan,$data_jenis);
	    	$kaji_ulang 		= PendaftaranPengujian::getKajiUlang($bulan,$data_jenis);
	    	$surat_penawaran 	= PendaftaranPengujian::getKajiUlang($bulan,$data_jenis);
	    	$konfirmasi 		= PendaftaranPengujian::getKajiUlang($bulan,$data_jenis);
	    	$data[0][$bulans] 	= $penerimaan_surat;
	    	$data[1][$bulans] 	= $kaji_ulang;
	    	$data[2][$bulans] 	= $surat_penawaran;
	    	$data[3][$bulans] 	= $konfirmasi;
	    	$data[4][$bulans] 	= $data_bulans;
    	}

    	$penerimaan_surat 	= array_values($data[0]);
    	$kaji_ulang 		= array_values($data[1]);
    	$surat_penawaran 	= array_values($data[2]);
    	$konfirmasi 		= array_values($data[3]);
    	$bulans 			= array_values($data[4]);

    	return [
    		'bulan' 			=> json_encode($bulans),
    		'penerimaan_surat' 	=> json_encode($penerimaan_surat),
    		'kaji_ulang' 		=> json_encode($kaji_ulang),
    		'surat_penawaran' 	=> json_encode($surat_penawaran),
    		'konfirmasi' 		=> json_encode($konfirmasi),
        ];
    }

    public function loadDataJadwal(Request $request)
    {
    	return $this->render('modules.kaji-ulang.jadwal-table', [
                'mockup' => false,
                'data' => $this->searchJadwal($request->bulan, $request->jenis_pengujian),
        ]);
    }

    public function searchJadwal($bulan, $jenis)
    {	
    	$bulan = substr($bulan, 5);
    	$data_bulan = (int)$bulan;
    	$data_jenis = (int)$jenis;
    	$data = array();
    	$penerimaan_surat 	= PendaftaranPengujian::getPenerimaanSurat($data_bulan,$data_jenis);
    	$kaji_ulang 		= PendaftaranPengujian::getKajiUlang($data_bulan,$data_jenis);
    	$surat_penawaran 	= PendaftaranPengujian::getKajiUlang($data_bulan,$data_jenis);
    	$konfirmasi 		= PendaftaranPengujian::getKajiUlang($data_bulan,$data_jenis);

    	$data[0][$data_bulan] 	= $penerimaan_surat;
    	$data[1][$data_bulan] 	= $kaji_ulang;
    	$data[2][$data_bulan] 	= $surat_penawaran;
    	$data[3][$data_bulan] 	= $konfirmasi;
    	$bulans = formatStringMonth($bulan);
    	$data[4][$data_bulan] 	= $bulans;

    	$penerimaan_surat 	= array_values($data[0]);
    	$kaji_ulang 		= array_values($data[1]);
    	$surat_penawaran 	= array_values($data[2]);
    	$konfirmasi 		= array_values($data[3]);
    	$bulans 			= array_values($data[4]);

    	return [
    		'bulan' 			=> json_encode($bulans),
    		'penerimaan_surat' 	=> json_encode($penerimaan_surat),
    		'kaji_ulang' 		=> json_encode($kaji_ulang),
    		'surat_penawaran' 	=> json_encode($surat_penawaran),
    		'konfirmasi' 		=> json_encode($konfirmasi),
        ];
    }

    public function grant(Request $request, $id)
    {
        $record = Role::find($id);
        $record->perms()->sync($request->check);

        return response([
            'status' => true
        ]);
    }

    public function destroy($id)
    {
        $record = Role::find($id);
        $record->delete();

        return response([
            'status' => true,
        ]);
    }
}
