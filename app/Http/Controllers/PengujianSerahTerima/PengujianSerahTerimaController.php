<?php

namespace App\Http\Controllers\PengujianSerahTerima;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Permintaan\PermintaanPengujianRequest;
use App\Http\Requests\OrderUji\UjiSerahTerimaRequest;
use App\Http\Requests\OrderUji\CekItemUjiRequest;

/* Models */
use App\Models\Authentication\Role;
use App\Models\OrderUji\UjiSerahTerima;
use App\Models\OrderUji\UjiSerahTerimaDetail;
use App\Models\Master\JenisPelayanan;
use App\Models\Master\JenisPengujian;
use App\Models\Master\Satuan;
use App\Models\Master\Lingkup;
use App\Models\Picture;
use App\Models\Files;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\FrontEnd\PendaftaranPengujianDetail;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\KajiUlang\KajiUlang;
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\SuratPenawaran\SuratPenawaranDetail;
use App\Models\VirtualAccount\KonfirmasiPembayaran;
use App\Models\Act\ActDisposisi;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Excel;
use PDF;

class PengujianSerahTerimaController extends Controller
{
    protected $link = 'pengujian-serah-terima/';
    protected $perms = 'pengujian-serah-terima';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Order Uji Serah Terima");
        $this->setPerms($this->perms);
        $this->setModalSize("tiny");
        $this->setBreadcrumb(['Order Uji Serah Terima' => '#']);

        // Header Grid Datatable
        $this->listStructs = [
            'listStruct'=>[
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'className' => "center aligned",
                    'width' => '200px',
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'className' => "center aligned",
                    'width' => '100px',
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'no_kontrak',
                    'name' => 'no_kontrak',
                    'label' => 'No Kontrak',
                    'className' => "center aligned",
                    'width' => '200px',
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'pemesan',
                    'name' => 'pemesan',
                    'label' => 'Peminta Jasa',
                    'className' => "center aligned",
                    'width' => '200px',
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'className' => "center aligned",
                    'width' => '200px',
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'className' => "center aligned",
                    'width' => '200px',
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'no_wbs',
                    'name' => 'no_wbs',
                    'label' => 'No WBS',
                    'className' => "center aligned",
                    'width' => '200px',
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'className' => "center aligned",
                    'width' => '200px',
                    'searchable' => false,
                    'sortable' => false,
                ],
                [
	                'data' => 'created_by',
	                'name' => 'created_by',
	                'label' => 'Dibuat Oleh',
	                'className' => "center aligned",
	                'width' => '200px',
	                'searchable' => false,
	                'sortable' => true,
            	],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'className' => "center aligned",
                    'width' => '100px',
                    'searchable' => false,
                    'sortable' => false,
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'className' => "center aligned",
                    'width' => '150px',
                    'searchable' => false,
                    'sortable' => true,
                ]
            ],
            'listStruct2' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'className' => "center aligned",
                    'width' => '200px',
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'className' => "center aligned",
                    'width' => '100px',
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'no_kontrak',
                    'name' => 'no_kontrak',
                    'label' => 'No Kontrak',
                    'className' => "center aligned",
                    'width' => '200px',
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'pemesan',
                    'name' => 'pemesan',
                    'label' => 'Peminta Jasa',
                    'className' => "center aligned",
                    'width' => '200px',
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'className' => "center aligned",
                    'width' => '200px',
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'className' => "center aligned",
                    'width' => '200px',
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'no_wbs',
                    'name' => 'no_wbs',
                    'label' => 'No WBS',
                    'className' => "center aligned",
                    'width' => '200px',
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'className' => "center aligned",
                    'width' => '200px',
                    'searchable' => false,
                    'sortable' => false,
                ],
                [
	                'data' => 'created_by',
	                'name' => 'created_by',
	                'label' => 'Dibuat Oleh',
	                'className' => "center aligned",
	                'width' => '200px',
	                'searchable' => false,
	                'sortable' => true,
            	],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'className' => "center aligned",
                    'width' => '100px',
                    'searchable' => false,
                    'sortable' => false,
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'className' => "center aligned",
                    'width' => '150px',
                    'searchable' => false,
                    'sortable' => true,
                ]
            ],
        ];
    }

    public function grid(Request $request)
    {	
    	if(auth()->user()->hasRole(['pelaksana-tegangan-rendah', 'yan-uji', 'msb-tegangan-rendah', 'asman-lola-tegangan-rendah', 'asman-dal-tegangan-rendah', 'adminlab-tegangan-rendah','admin'])){
    		$records = UjiSerahTerima::where('layanan_id', 4)->whereHas('pp', function($u){
    										$u->whereIn('status', [0,1,2,3,4,10]);
    									})->select('*');
    	}elseif(auth()->user()->hasRole(['pelaksana-tegangan-tinggi', 'yan-uji', 'msb-tegangan-tinggi', 'asman-lola-tegangan-tinggi', 'asman-dal-tegangan-tinggi', 'adminlab-tegangan-tinggi','admin'])){
    		$records = UjiSerahTerima::where('layanan_id', 5)->whereHas('pp', function($u){
    										$u->whereIn('status', [0,1,2,3,4,10]);
    									})->select('*');
    	}else{
    		$records = UjiSerahTerima::where('id', 0);
    	}
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at','desc');
        }

        if ($no_order = $request->no_order) {
            $records->where('no_order', 'like','%'.$no_order.'%');
        }

        if ($tanggal_order = $request->tanggal_order) {
            $records->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
        }

        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $records->where('layanan_id', $jenis_pelayanan_id);
        }

        if ($no_kontrak = $request->no_kontrak) {
            $records->where('no_kontrak', 'like','%'.$no_kontrak.'%');
        }

        if ($wbs_io = $request->wbs_io) {
            $records->where('no_wbs', 'like','%'.$wbs_io.'%');
        }

        $records = $records->get();



        return DataTables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('pemesan', function ($record) use ($request) {
            $pemesan = $record->user->pelanggans->perusahaan->nama.'</br>'.$record->user->pelanggans->nama_lengkap.'</br>'.$record->user->pelanggans->no_hp;
                return $pemesan;
        })
        ->addColumn('tgl_order', function ($record) use ($request) {
            return DateToStringYear($record->tgl_order);
        })
        ->addColumn('layanan_lingkup', function ($record) use ($request) {
            return $record->pelayanan->nama.' /</br> '.$record->lingkup->nama;
        })
        ->addColumn('created_by', function ($record) {
            return $record->creatorName();
        })
        ->addColumn('jenis_pengujian', function ($record) use ($request) {
            $jenis_pengujian = '';
            if($record->detail){
                $jenis_pengujian .= '<div class="ui ordered list list-more1" data-display="3">';
                foreach ($record->detail as $key => $value) {
                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$value->id.'">'.$value->jenis->nama.'</a></div>';
                }
                $jenis_pengujian .='</div>';
            }
            return $jenis_pengujian;
        })
        ->editColumn('detil', function($record){
            $btn = '';
            $btn .= $this->makeButton([
                'type' => 'url',
                'tooltip' => 'Detil Data',
                'class' => 'ui teal mini icon button',
                'label' => '<i class="eye icon"></i>',
                'url' => url($this->link.$record->id.'/detil')
            ]);

            return $btn;
        })
        ->editColumn('status', function($record){
            $status = '<a class="ui orange tag label">On Progress</a>';
            return $status;
        })
        ->editColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->addColumn('action', function ($record) {
            $btn = '';
            $btn .= $this->makeButton([
            	'type' => 'url',
            	'tooltip' => 'Cetak Qr Code',
            	'class' => 'ui violet mini icon button',
            	'label' => '<i class="print icon"></i>',
            	'url' => url($this->link.$record->id.'/cetakQrCode')
            ]);

            return $btn;
        })
        ->rawColumns(['status','pemesan','layanan_lingkup','action','jenis_pengujian','detil'])
        ->make(true);
    }

    public function histori(Request $request)
    {	
    	if(auth()->user()->hasRole(['pelaksana-tegangan-rendah', 'yan-uji', 'msb-tegangan-rendah', 'asman-lola-tegangan-rendah', 'asman-dal-tegangan-rendah', 'adminlab-tegangan-rendah','admin'])){
    		$records = UjiSerahTerima::where('layanan_id', 4)->whereHas('pp', function($u){
    											$u->whereIn('status', [5,6,7]);
    										})->select('*');
    	}elseif(auth()->user()->hasRole(['pelaksana-tegangan-tinggi', 'yan-uji', 'msb-tegangan-tinggi', 'asman-lola-tegangan-tinggi', 'asman-dal-tegangan-tinggi', 'adminlab-tegangan-tinggi','admin'])){
	        $records = UjiSerahTerima::where('layanan_id', 5)->whereHas('pp', function($u){
	        									$u->whereIn('status', [5,6,7]);
	        								})->select('*');
    	}else{
    		$records = UjiSerahTerima::where('id', 0);
    	}

        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at','desc');
        }

        if ($no_order = $request->no_order) {
            $records->where('no_order', 'like','%'.$no_order.'%');
        }

        if ($tanggal_order = $request->tanggal_order) {
            $records->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
        }

        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $records->where('layanan_id', $jenis_pelayanan_id);
        }

        if ($no_kontrak = $request->no_kontrak) {
            $records->where('no_kontrak', 'like','%'.$no_kontrak.'%');
        }

        if ($wbs_io = $request->wbs_io) {
            $records->where('no_wbs', 'like','%'.$wbs_io.'%');
        }

        $records = $records->get();


        return DataTables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('pemesan', function ($record) use ($request) {
            $pemesan = $record->user->pelanggans->perusahaan->nama.'</br>'.$record->user->pelanggans->nama_lengkap.'</br>'.$record->user->pelanggans->no_hp;
                return $pemesan;
        })
        ->addColumn('created_by', function ($record) {
            return $record->creatorName();
        })
        ->addColumn('tgl_order', function ($record) use ($request) {
            return DateToStringYear($record->tgl_order);
        })
        ->addColumn('layanan_lingkup', function ($record) use ($request) {
            return $record->pelayanan->nama.' /</br> '.$record->lingkup->nama;
        })
        ->addColumn('jenis_pengujian', function ($record) use ($request) {
            $jenis_pengujian = '';
            if($record->detail){
                $jenis_pengujian .= '<div class="ui ordered list list-more1" data-display="3">';
                foreach ($record->detail as $key => $value) {
                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$value->id.'">'.$value->jenis->nama.'</a></div>';
                }
                $jenis_pengujian .='</div>';
            }
            return $jenis_pengujian;
        })
        ->editColumn('detil', function($record){
            $btn = '';
            $btn .= $this->makeButton([
                'type' => 'url',
                'tooltip' => 'Detil Data',
                'class' => 'ui teal mini icon button',
                'label' => '<i class="eye icon"></i>',
                'url' => url($this->link.$record->id.'/detil')
            ]);

            return $btn;
        })
        ->editColumn('status', function($record){
            $status = '<a class="ui green tag label">Selesai</a>';
            return $status;
        })
        ->editColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->editColumn('briefing_teknis', function ($record) {
            $btn = '';
            if($record->pelayanan->id == 3){
                 if(isset($record->kaji_ulang)){
                    if($record->kaji_ulang->files->where('type', 1)->count()>0){
                        if($record->kaji_ulang->files->where('type', 2)->count()>0){
                            $btn .= $this->makeButton([
                                'type' => 'url',
                                'tooltip' => 'Download Jadwal',
                                'class' => 'ui blue mini icon button',
                                'label' => '<i class="download icon"></i>',
                                'url' => url('download-briefing', $record->kaji_ulang->id).'/briefing-teknis/1'
                            ]);

                            $btn .= $this->makeButton([
                                'type' => 'url',
                                'tooltip' => 'Download Notulen',
                                'class' => 'ui teal mini icon button',
                                'label' => '<i class="download icon"></i>',
                                'url' => url('download-briefing', $record->kaji_ulang->id).'/briefing-teknis/2'
                            ]);
                        }else{
                            $btn .= $this->makeButton([
                                'type' => 'url',
                                'tooltip' => 'Download Jadwal',
                                'class' => 'ui blue mini icon button',
                                'label' => '<i class="download icon"></i>',
                                'url' => url('download-briefing', $record->kaji_ulang->id).'/briefing-teknis/1'
                            ]);
                        }
                    }else{
                        $btn .= '-';
                    }
                }
            }else{
                $btn .= '-';
            }
            return $btn;
        })
        ->addColumn('action', function ($record) {
            $btn = '';

            $btn = $this->makeButton([
                'type' => 'url',
                'class' => 'ui blue mini icon button',
                'tooltip' => 'Cetak QR Code',
                'label' => '<i class="print icon"></i>',
                'url' => '#'
                // 'url' => url($this->link).'/print-qr-code/'.$record->id
            ]);

            return $btn;
        })
        ->rawColumns(['status','pemesan','action','briefing_teknis','layanan_lingkup','jenis_pengujian','detil'])
        ->make(true);
    }

    public function index()
    {
    	if(auth()->user()->hasRole(['pelaksana-tegangan-rendah', 'msb-tegangan-rendah', 'asman-lola-tegangan-rendah', 'asman-dal-tegangan-rendah', 'adminlab-tegangan-rendah','admin'])){
    		$records = UjiSerahTerima::where('layanan_id', 4)
			    					->whereHas('pp', function($u){
			    						$u->whereIn('status', [0,1,2,3,4,10]);
			    					})
			    					->get()->count();
    	}elseif(auth()->user()->hasRole(['pelaksana-tegangan-tinggi', 'msb-tegangan-tinggi', 'asman-lola-tegangan-tinggi', 'asman-dal-tegangan-tinggi', 'adminlab-tegangan-tinggi','admin'])){
	        $records = UjiSerahTerima::where('layanan_id', 5)
				        			->whereHas('pp', function($u){
				        				$u->whereIn('status', [0,1,2,3,4,10]);
				        			})
				        			->get()->count();
    	}elseif(auth()->user()->hasRole(['yan-uji','admin'])){
    		$records = UjiSerahTerima::whereIn('layanan_id', [4,5])
									->whereHas('pp', function($u){
        								$u->whereIn('status', [0,1,2,3,4,10]);
        							})
								 	->get()->count();
    	}else{
    		$records = 0;
    	}

        $data = [
            'mockup' => true,
            'structs' => $this->listStructs,
            'angka' => $records,
        ];
        return $this->render('modules.pengujian-serah-terima.index', $data);
    }

    public function create()
    {
        $this->setTitle('Buat Order Uji Serah Terima Baru');
        $this->setBreadcrumb(['Order Uji Serah Terima' => '#', 'Buat' => '#']);
        return $this->render('modules.pengujian-serah-terima.create');
    }

    public function buat()
    {
        return $this->render('modules.pengujian-serah-terima.partials.buat');
    }

    public function lihat($data)
    {
        $month = date('n');
        $year = date("Y");
        return $this->render('modules.pengujian-serah-terima.partials.lihat-jadwal', [
            'prioritas' => $data,
            'tahun' => $year,
            'bulan' => $month,
        ]);
    }

    public function lihatLayanan()
    {
        $record = JenisPelayanan::get();
        return $this->render('modules.pengujian-serah-terima.partials.pelayanan',['record' => $record]);
    }

    public function searchLayanan(Request $request)
    {
        $pelayanan = $request->pelayanan;
        $lingkup = $request->lingkup;
        $jenis_pengujian = $request->jenis_pengujian;

        // $record = JenisPelayanan::get();
        $record = JenisPelayanan::with('lingkup', 'pengujian');

        if ($pelayanan) {
            $record->where('id', $pelayanan);
        }

        if ($lingkup) {
             $record->whereHas('lingkup',function($q) use($lingkup){
                $q->where('id', $lingkup);
            });
        }

        if ($jenis_pengujian){
            $record->whereHas('lingkup',function($q) use($jenis_pengujian){
                $q->whereHas('pengujian',function($query) use($jenis_pengujian){
                    $query->where('nama', 'like', '%'.$jenis_pengujian.'%');
                });
            });
        }

        return view('modules.pengujian-serah-terima.partials.pelayanan-table', ['record' => $record->get()]);
    }

    public function detil($id)
    {
        $this->setTitle('Detil Order Uji Serah Terima');
        $this->setBreadcrumb(['Detil Order Uji Serah Terima' => '#']);
        $record = UjiSerahTerima::find($id);
        $this->setSubtitle('No Order : '.$record->no_order);
        return $this->render('modules.pengujian-serah-terima.partials.detil',['record' => $record]);
    }

    public function ubah()
    {
        return $this->render('modules.pengujian-serah-terima.partials.ubah');
    }

    public function store(UjiSerahTerimaRequest $request)
    {
        if(!$request->detail){
            return response()->json([
                'status' => 'false',
                'message' => 'Data Item Uji tidak boleh kosong'
            ],402);
        }

        $user = auth()->user()->id;
        $date = date('Y-m-d');
        $nomor = array();

        DB::beginTransaction();
        try {
            $no = PendaftaranPengujian::generateNomor();
            $no_order = PendaftaranPengujian::generateNoOrder($request->user_id);
            //PP
            $pp = new PendaftaranPengujian;
            $pp->no_order = $no_order;
            $pp->nomor = $no;
        	$pp->tgl_surat = $request->tgl_kontrak;
        	$pp->no_surat = $request->no_kontrak;
        	$pp->tipe = 1;
        	$pp->tipe_surat = 1;
            $pp->tgl_order = $date;
            $pp->user_id = $request->user_id;
            $pp->layanan_id = $request->jenis_pelayanan_id;
            $pp->lingkup_id = $request->lingkup_id;
            $pp->kelas = $request->kelas;
            $pp->rencana = $request->rencana;
            $pp->status = 1;
            if($request->jenis_pelayanan_id == 1){
	            $pp->jenis = 0;
            }else{
            	$pp->jenis = 1;	
            }
            $pp->catatan = $request->catatan;

            if($file = $request->surat){
        		$path = $file->store('uploads/pendaftaran', 'public');
        		$pp->filename = $file->getClientOriginalName();
        		$pp->bukti = $path;
            }
            $pp->save();
            $pp->saveDetailSerahTerima($request->detail);
            $pp->multipleFileUpload($request->lampiran);

            $act = new ActDisposisi;
            $act->parent_id = $pp->id;
            $act->pengirim_id = auth()->user()->id;
            $act->penerima_id = auth()->user()->id;
            $act->tipe = 7;
            $act->jenis = 7;
            $act->keterangan = 'Membuat <a>Order Uji Serah Terima</a> dengan No Order <a>'.$no_order.'</a>';
            $act->save();

            //SERAH TERIMA
            $pengujian = new UjiSerahTerima;
            $pengujian->pp_id = $pp->id;
            $pengujian->no_order = $no_order;
            $pengujian->nomor = $no;
            $pengujian->no_wbs = $request->no_wbs;
            $pengujian->no_kontrak = $request->no_kontrak;
            $pengujian->tgl_kontrak = $request->tgl_kontrak;
            $pengujian->tgl_order = $date;
            $pengujian->user_id = $request->user_id;
            $pengujian->layanan_id = $request->jenis_pelayanan_id;
            $pengujian->lingkup_id = $request->lingkup_id;
            $pengujian->kelas = $request->kelas;
            $pengujian->rencana = $request->rencana;
            $pengujian->status = 0;
            $pengujian->catatan = $request->catatan;

            if($file = $request->surat){
                $path = $file->store('uploads/uji-serah-terima', 'public');
                $pengujian->filename = $file->getClientOriginalName();
                $pengujian->bukti = $path;
            }
            $pengujian->save();
            $pengujian->saveDetail($request->detail);
            $pengujian->multipleFileUpload($request->lampiran);
            //KAJI ULANG
            $kaji_ulang = new KajiUlang;
	    	$kaji_ulang->pp_id 		= $pp->id;
		    $kaji_ulang->keputusan  = 1;
		    $kaji_ulang->tipe  = 1;
		    $kaji_ulang->keterangan  = 'Order Uji Serah Terima';
	        $kaji_ulang->save();

            $pp->save();
	        $kaji_ulang->saveDetailSerahTerima($request->detail, $pp->id);

	        //SURAT PENAWARAN
	        $surat = new SuratPenawaran;
    		$surat->kaji_id 		= $kaji_ulang->id;
    		$surat->no_surat 		= $request->no_kontrak;
    		$surat->tgl_surat 		= $request->tgl_kontrak;
    		$surat->adendum 		= NULL;
    		$surat->wbs_io 			= $request->no_wbs;
    		$surat->nomor 			= $no;
    		$surat->tipe 			= 1;
    		$surat->total 			= 0;
    		$surat->save();

    		//VIRTUAL ACCOUNT
    		$seri 		= $surat->kaji_ulang->pp->pelayanan->seri;
			$kode 		= $surat->kaji_ulang->pp->pelayanan->kode;
			$urut_seri 	= $seri;

			if($seri < 10){
				$seri = '00'.$seri;
			}elseif($seri > 10 && $seri < 100){
				$seri = '0'.$seri;
			}elseif($seri >= 100){
				$seri = $seri;
			}else{
				$seri = 000;
			}

    		$cari_va 			    	= VirtualAccount::where('surat_id', $surat->id)->where('tipe', 0)->first();
    		if($cari_va){
    			$va 			     	= VirtualAccount::find($cari_va->id);
    			$va->surat_id 		 	= $surat->id;
    			$va->tgl_aktif  	 	= Carbon::parse($date);
    			$va->tgl_kadaluarsa  	= Carbon::parse($date);
    			$va->nominal  			= 0;
    			$va->status  		 	= 3;
    			$va->tipe_customer  	= 1;
    			$va->tipe  				= 1;
    			$va->save();

    			$jenis_layanan 			= JenisPelayanan::find($surat->kaji_ulang->pp->pelayanan->id);
    			$jenis_layanan->seri 	= ($urut_seri+1);
    			$jenis_layanan->save();
    		}else{
    			$va 			     	= new VirtualAccount;
    			$va->surat_id 		 	= $surat->id;
    			$va->no_va 	 		 	= '89319'.sprintf("%02d",$kode).$surat->kaji_ulang->pp->user->pelanggans->perusahaan->kode.Carbon::now()->format('y').sprintf("%03d",$seri);
    			$va->tgl_aktif  	 	= Carbon::parse($date);
    			$va->tgl_kadaluarsa  	= Carbon::parse($date);
    			$va->nominal  			= 0;
    			$va->status  		 	= 3;
    			$va->tipe_customer  	= 1;
    			$va->tipe  				= 1;
    			$va->save();

    			$jenis_layanan 			= JenisPelayanan::find($surat->kaji_ulang->pp->pelayanan->id);
    			$jenis_layanan->seri 	= ($urut_seri+1);
    			$jenis_layanan->save();
    		}

    		//KONFIRMASI PEMBAYARAN
    		$konfirmasi = new KonfirmasiPembayaran;
            $konfirmasi->va_id 			= $va->id;
            $konfirmasi->status 		= 3;
            $konfirmasi->dana_masuk 	= 0;
            $konfirmasi->tipe  			= 0;
            $konfirmasi->wbs_io  		= $request->no_wbs;
            $konfirmasi->catatan 		= 'Order Serah Terima';
            $konfirmasi->save();
            $nomor[] = $no_order;
            DB::commit();

        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true,
          'message' => $nomor
        ]);
    }

    public function edit($id)
    {
        $this->setTitle('Ubah Permintaan SPM');
        $this->setBreadcrumb(['Ubah Permintaan SPM'=> '#', 'Ubah' => '#']);
        $record = UjiSerahTerima::find($id);
        return $this->render('modules.pengujian-serah-terima.edit',['record' => $record]);
    }

    public function konfirmasi($id)
    {
        $this->setTitle('Konfirmasi Pembayaran');
        $this->setBreadcrumb(['Konfirmasi Pembayaran' => '#', 'Konfirmasi Pembayaran' => '#']);
        $record = UjiSerahTerima::find($id);
        return $this->render('modules.pengujian-serah-terima.partials.konfirmasi',['record' => $record]);
    }

    public function upload($id)
    {
        return $this->render('modules.pengujian-serah-terima.partials.upload', ['lingkup_id' => $id]);
    }

    public function cekUpload(Request $request){
        $cek_id = array();
        $satuan_id = array();
        $jenis_pengujian = JenisPengujian::where('lingkup_id', $request->lingkup_id)->get();
        $satuan = Satuan::get();
        foreach ($jenis_pengujian as $key => $val) {
            $cek_id[] = $val->id;
        }
        foreach ($satuan as $key => $vl) {
            $satuan_id[] = $vl->id;
        }
        if($request->hasFile('upload_data')){
            $path = $request->file('upload_data')->getRealPath();
            $data = Excel::load($path, function($reader) {})->get();
            $insert = array();
            $i = 1;
            $y=0;
            $z=0;
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    if(!is_null($value->id_jenis_pengujian) && !is_null($value->id_satuan)){
                        if(in_array((int)$value->id_jenis_pengujian, $cek_id, TRUE)){
                            if(in_array((int)$value->id_satuan, $satuan_id, TRUE)){
                                $nama_jenis = JenisPengujian::find((int)$value->id_jenis_pengujian);
                                $nama_satuan = Satuan::find((int)$value->id_satuan);
                                if($i <= 350){
                                    $insert[] = [
                                        'id_jenis_pengujian' => (int)$value->id_jenis_pengujian,
                                        'nama_jenis' => $nama_jenis->nama,
                                        'nama_satuan' => $nama_satuan->satuan,
                                        'merk' => $value->merk,
                                        'tipe' => $value->tipe,
                                        'jumlah' => (int)$value->jumlah,
                                        'id_satuan' => (int)$value->id_satuan,
                                        'spesifikasi' => $value->spesifikasi,
                                        'tgl_mulai' => $value->tgl_mulai->toDateString(),
                                        'tgl_selesai' => $value->tgl_selesai->toDateString(),
                                    ];
                                }
                                $i++;
                            }else{
                                $y +=1;
                            }
                        }else{
                            $z +=1;
                        }
                    }
                }
                // dd($insert);
                if($z > 0){
                    return response([
                        'status' => false,
                        'message' => 'Ada '.$z.' data Jenis Pengujian yang tidak sesuai',
                        'data' => $insert,
                    ]);
                }elseif($y > 0){
                    return response([
                        'status' => false,
                        'message' => 'Ada '.$z.' data Satuan yang tidak sesuai',
                        'data' => $insert,
                    ]);
                }else{
                    return response([
                        'status' => true,
                        'message' => '',
                        'data' => $insert,
                    ]); 
                }
            }else{
                return response([
                    'status' => false,
                    'message' => 'Tidak Ada Data',
                    'data' => '',
                ]);
            }
        }else{
            return response([
                'status' => false,
                'message' => 'Inputan tidak boleh kosong',
                'data' => '',
            ]);
        }
    }

    public function exportTemplate(Request $request, $lingkup)
    {   
        $jenis_pengujian = JenisPengujian::where('lingkup_id', $lingkup)->get();
        $data_lingkup = Lingkup::find($lingkup);
        $data_satuan = Satuan::get();
         $Export = Excel::create('List Item Uji - '.$data_lingkup->pelayanan->nama, function($excel) use($jenis_pengujian, $data_lingkup, $data_satuan) {
            $excel->setTitle('List Item Uji');
            $excel->setCreator('E-Uji dan E-Kal')->setCompany('E-Pusertif');
            $excel->setDescription('Export');
            $excel->sheet('List Item Uji ', function($sheet) use($jenis_pengujian, $data_lingkup, $data_satuan){
                $sheet->row(1, array(
                    'id_jenis_pengujian',
                    'merk',
                    'tipe',
                    'jumlah',
                    'id_satuan',
                    'spesifikasi',
                    'tgl_mulai',
                    'tgl_selesai',
                ));
                $sheet->setWidth(array('J'=>6));
                $sheet->SetCellValue("J1", "ID");
                $sheet->SetCellValue("K1", "Jenis Pengujian");
                $sheet->setBorder('J1:O1', 'thin');
                $sheet->mergeCells("K1:O1");
                $sheet->cells('J1:O1', function($cells){
                        $cells->setBackground('#999999');
                        $cells->setFontColor('#FFFFFF');
                        $cells->setAlignment('center');
                });

                $row = 2;
                $no =1;
                foreach ($jenis_pengujian as $key => $value) {
                    $from="K".$row;
                    $to="O".$row;
                    $sheet->mergeCells("$from:$to");
                    $sheet->cells('J'.$row, function($cells){
                        $cells->setAlignment('center');
                    });
                    $sheet->SetCellValue('J'.$row, $value->id);
                    $sheet->SetCellValue("K".$row, $value->nama);
                    $row++;
                }

                $awal="J".($row+2);
                $akhir="K".($row+2);
                $a="K".($row+2);
                $b="M".($row+2);
                $sheet->SetCellValue('J'.($row+2), "ID");
                $sheet->SetCellValue('K'.($row+2), "Satuan");
                $sheet->setBorder("$awal:$akhir", 'thin');
                $sheet->mergeCells("$a:$b");
                $sheet->cells("$awal", function($cells){
                        $cells->setBackground('#999999');
                        $cells->setFontColor('#FFFFFF');
                        $cells->setAlignment('center');
                });
                $sheet->cells("$akhir", function($cells){
                        $cells->setBackground('#999999');
                        $cells->setFontColor('#FFFFFF');
                });

                $z = ($row+3);
                foreach ($data_satuan as $key => $val) {
                    $from="K".$z;
                    $to="M".$z;
                    $sheet->mergeCells("$from:$to");
                    $sheet->cells('J'.$z, function($cells){
                        $cells->setAlignment('center');
                    });
                    $sheet->SetCellValue('J'.$z, $val->id);
                    $sheet->SetCellValue("K".$z, $val->satuan);
                    $z++;
                }

                $sheet->setBorder('A1:H1', 'thin');
                $sheet->cells('A1:H1', function($cells){
                        $cells->setBackground('#999999');
                        $cells->setFontColor('#FFFFFF');
                        $cells->setAlignment('center');
                });
                $xx ="J".($z+2);
                $yy ="P".($z+2);
                $sheet->mergeCells("$xx:$yy");
                $sheet->SetCellValue($xx, 'Catatan : Mohon inputkan ID Pengujian dan ID Satuan Sesuai dengan List diatas.');
                $sheet->cells($xx, function($cells){
                    $cells->setFontSize(10);
                });
                $sheet->cells('A1:A300', function($cells){
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->setWidth(array('A'=>20,'B'=>30,'C'=>20, 'D'=>20, 'E'=>15, 'F'=>30, 'G'=>15, 'H'=>15));
            });
        });
        $Export ->download('xls');
    }

    public function destroy($id)
    {
        $record = UjiSerahTerima::find($id);
        unlink(public_path('storage/'.$record->bukti));
        $record->delete();
        return response([
            'status' => true,
        ]);
    }

    public function preview($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function previewMultiple($id, $type)
    {
    	if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $c->filename,
                        ];
                    }
                }
                return $this->render('pdf-multiple', [
		        	'mockup' => true,
		        	'data' => $rename,
		        ]);
            }
        }
    }

    public function download($id)
    {
        $daftar = UjiSerahTerima::find($id);
        if(file_exists(public_path('storage/'.$daftar->bukti)))
        {
            return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
        }
        return response()->view('errors.download');
    }

// BATAS BARU

    public function getDetail(Request $request){
        $recordJenisPengujian = JenisPelayanan::find($request->jenis_pelayanan);

        return $this->render('modules.pengujian-serah-terima.partials.detail',['recordJenisPengujian'=>$recordJenisPengujian]);
    }


    public function cekRequestItemUji(CekItemUjiRequest $request){
        return response([
            'status' => true,
        ]);
    }

    public function printBarcodePdf(Request $request, $id){
    	$record = UjiSerahTerima::find($id);
        $data = [
        	'records' => $record,
        ];
	    $pdf = PDF::loadView('modules.pengujian-serah-terima.cetak.cetak-barcode', $data);
        return $pdf->stream('QR Code Order Uji Serah Terima '.$record->tgl_order.'.pdf',array("Attachment" => false));
 	}
}
