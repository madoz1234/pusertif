<?php

namespace App\Http\Controllers\Reschedule;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Reschedule\Reschedule;
use App\Models\Reschedule\RescheduleDetail;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\Reschedule\RescheduleRequest;
use App\Models\FrontEnd\PendaftaranPengujian;

/* Models */
use App\Models\Authentication\Role;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use DB;

class RescheduleController extends Controller
{
    protected $link = 'reschedules/';
    protected $perms = 'reschedule';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Reschedules");
        $this->setPerms($this->perms);
        $this->setModalSize("small");
        $this->setBreadcrumb(['Reschedules' => '#']);

        // Header Grid Datatable
        $this->listStructs = [
            'listStruct' => [
	            [
	                'data' => 'num',
	                'name' => 'num',
	                'label' => '#',
	                'orderable' => false,
	                'searchable' => false,
	                'className' => "center aligned",
	                'width' => '40px',
	            ],
	            /* --------------------------- */
	            [
	                'data' => 'no_order',
	                'name' => 'no_order',
	                'label' => 'No Order',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'tgl_order',
	                'name' => 'tgl_order',
	                'label' => 'Tgl Order',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'no_surat',
	                'name' => 'no_surat',
	                'label' => 'No Surat Permintaan',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'layanan_lingkup',
	                'name' => 'layanan_lingkup',
	                'label' => 'Layanan / Lingkup',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'wbs_io',
	                'name' => 'wbs_io',
	                'label' => 'WBS/IO',
	                'searchable' => false,
	                'sortable' => true,
	            ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                ],
	            [
	            	'data' => 'action',
	            	'name' => 'action',
	            	'label' => 'Aksi',
	            	'searchable' => false,
	            	'sortable' => false,
                    'className' => "center aligned",
	            	'width' => '100px',
	            ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
            ],
            'listStruct2' => [
	            [
	                'data' => 'num',
	                'name' => 'num',
	                'label' => '#',
	                'orderable' => false,
	                'searchable' => false,
	                'className' => "center aligned",
	                'width' => '40px',
	            ],
	            /* --------------------------- */
	            [
	                'data' => 'no_order',
	                'name' => 'no_order',
	                'label' => 'No Order',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'tgl_order',
	                'name' => 'tgl_order',
	                'label' => 'Tgl Order',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'no_surat',
	                'name' => 'no_surat',
	                'label' => 'No Surat Permintaan',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'layanan_lingkup',
	                'name' => 'layanan_lingkup',
	                'label' => 'Layanan / Lingkup',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'wbs_io',
	                'name' => 'wbs_io',
	                'label' => 'WBS/IO',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	            	'data' => 'action',
	            	'name' => 'action',
	            	'label' => 'Aksi',
	            	'searchable' => false,
	            	'sortable' => false,
	            	'className' => "center aligned",
	            	'width' => '100px',
	            ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
            ],
        ];
    }

    public function grid(Request $request)
    {
    	$user = auth()->user();
    	if($user->hasRole(['asman-dal-kalibrasi','asman-lola-kalibrasi','admin'])){
	        	$records = Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
	        							$u->where('layanan_id', 1);
	        						})
	        						->where('status', 0)
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use ($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                         });
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%');
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id);
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%');
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                         });
                                    })
		                            ->select('*');

	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
            $records = $records->get();

    	}elseif($user->hasRole(['asman-dal-siskit','asman-lola-siskit','admin'])){
	        	$records = Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
	        							$u->where('layanan_id', 2);
	        						})
	        						->where('status', 0)
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use ($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                         });
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%');
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id);
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%');
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                         });
                                    })
		                            ->select('*');

	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
            $records = $records->get();

    	}elseif($user->hasRole(['asman-dal-sistgi','asman-lola-sistgi','admin'])){
	        	$records = Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
	        							$u->where('layanan_id', 3);
	        						})
	        						->where('status', 0)
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use ($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                         });
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%');
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id);
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%');
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                         });
                                    })
		                            ->select('*');

	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
            $records = $records->get();

    	}elseif($user->hasRole(['asman-dal-tegangan-rendah','asman-lola-tegangan-rendah','admin'])){
	        	$records = Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
	        							$u->where('layanan_id', 4);
	        						})
	        						->where('status', 0)
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use ($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                         });
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%');
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id);
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%');
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                         });
                                    })
		                            ->select('*');

	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
            $records = $records->get();

    	}elseif($user->hasRole(['asman-dal-tegangan-tinggi','asman-lola-tegangan-tinggi','admin'])){
	        	$records = Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
	        							$u->where('layanan_id', 5);
	        						})
	        						->where('status', 0)
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use ($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                         });
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%');
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id);
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%');
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                         });
                                    })
		                            ->select('*');

	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
            $records = $records->get();

    	}else{
    		$records = collect([]);
    	}

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
            	return DateToStringYear($record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
               return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->editColumn('wbs_io', function($record){
                 return $record->pengujian->penerimaan->konfirmasi->wbs_io;
            })
            ->editColumn('detil', function($record){
                $btn = '';
                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->id.'/detil')
                ]);
                return $btn;
            })
            ->editColumn('status', function($record){
                $status = '<label class="ui orange tag label">Menunggu Reschedule</label>';
                return $status;
            })
            ->addColumn('action', function ($record) use ($link){
            	$user = auth()->user();
                $btn = '';
				if($user->hasRole(['asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-tinggi','asman-dal-tegangan-rendah', 'asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-tinggi','asman-lola-tegangan-rendah'])){
					$btn .= $this->makeButton([
						'type' => 'url',
						'tooltip' => 'Buat Reschedule',
						'class' => 'ui violet mini icon button',
						'label' => '<i class="edit icon"></i>',
						'url' => url($this->link.$record->id.'/edit')
					]);
                }else{
                	$btn .= '-';
                }
                return $btn;
            })
            ->rawColumns(['action','status','detil','jenis_pengujian','layanan_lingkup','status','detil'])
            ->make(true);
    }

    public function historis(Request $request)
    {
    	$user = auth()->user();
        if($user->hasRole(['asman-dal-kalibrasi','asman-lola-kalibrasi','admin'])){
	        	$records = Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
	        							$u->where('layanan_id', 1);
	        						})
	        						->where('status', 1)
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use ($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                         });
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%');
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id);
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%');
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                         });
                                    })
		                            ->select('*');
	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
            $records = $records->get();
    	}elseif($user->hasRole(['asman-dal-siskit','asman-lola-siskit','admin'])){
	        	$records = Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
	        							$u->where('layanan_id', 2);
	        						})
	        						->where('status', 1)
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use ($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                         });
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%');
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id);
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%');
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                         });
                                    })
		                            ->select('*');
	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
            $records = $records->get();
    	}elseif($user->hasRole(['asman-dal-sistgi','asman-lola-sistgi','admin'])){
	        	$records = Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
	        							$u->where('layanan_id', 3);
	        						})
	        						->where('status', 1)
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use ($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                         });
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%');
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id);
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%');
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                         });
                                    })
		                            ->select('*');
	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
            $records = $records->get();
    	}elseif($user->hasRole(['asman-dal-tegangan-rendah','asman-lola-tegangan-rendah','admin'])){
	        	$records = Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
	        							$u->where('layanan_id', 4);
	        						})
	        						->where('status', 1)
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use ($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                         });
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%');
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id);
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%');
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                         });
                                    })
		                            ->select('*');
	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
            $records = $records->get();
    	}elseif($user->hasRole(['asman-dal-tegangan-tinggi','asman-lola-tegangan-tinggi','admin'])){
	        	$records = Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
	        							$u->where('layanan_id', 5);
	        						})
	        						->where('status', 1)
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use ($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                         });
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%');
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id);
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%');
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                         });
                                    })
		                            ->select('*');
	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
            $records = $records->get();
    	}else{
    		$records = collect([]);
    	}

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
            	return DateToStringYear($record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
               return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->editColumn('wbs_io', function($record){
                 return $record->pengujian->penerimaan->konfirmasi->wbs_io;
            })
            ->editColumn('status', function($record){
                $status = '<label class="ui green tag label">Jadwal Diperbarui</label>';
                return $status;
            })
            ->addColumn('action', function ($record) use ($link){
            	$user = auth()->user();
                $btn = '';
                if($user->hasRole(['asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-tinggi','asman-dal-tegangan-rendah', 'asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-tinggi','asman-lola-tegangan-rendah'])){
					$btn .= $this->makeButton([
						'type' => 'url',
						'tooltip' => 'Detail',
						'class' => 'ui teal mini icon button',
						'label' => '<i class="eye icon"></i>',
						'url' => url($this->link.$record->id.'/detil')
					]);
                }else{
                	$btn .= '-';
                }
                return $btn;
            })
            ->rawColumns(['action','status','detil','jenis_pengujian','layanan_lingkup','status'])
            ->make(true);
    }

    public function index()
    {	
    	$user = auth()->user();
    	if($user->hasRole(['asman-dal-kalibrasi','asman-lola-kalibrasi','admin'])){
	        	$satu = Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
	        							$u->where('layanan_id', 1);
	        						})
	        						->where('status', 0)->get()->count();
    	}elseif($user->hasRole(['asman-dal-siskit','asman-lola-siskit','admin'])){
	        	$satu = Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
	        							$u->where('layanan_id', 2);
	        						})
	        						->where('status', 0)->get()->count();
    	}elseif($user->hasRole(['asman-dal-siskit','asman-lola-siskit','admin'])){
	        	$satu = Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
	        							$u->where('layanan_id', 3);
	        						})
	        						->where('status', 0)->get()->count();
    	}elseif($user->hasRole(['asman-dal-tegangan-rendah','asman-lola-tegangan-rendah','admin'])){
	        	$satu = Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
	        							$u->where('layanan_id', 4);
	        						})
	        						->where('status', 0)->get()->count();
    	}elseif($user->hasRole(['asman-dal-tegangan-tinggi','asman-lola-tegangan-tinggi','admin'])){
	        	$satu = Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
	        							$u->where('layanan_id', 5);
	        						})
	        						->where('status', 0)->get()->count();
    	}else{
    		$satu = 0;
    	}
    	$data = [
            'mockup'    => true,
            'structs'   => $this->listStructs,
            'satu'   	=> $satu,
        ];
        return $this->render('modules.reschedule.index', $data);
    }

    public function create()
    {
        return $this->render('modules.reschedule.data.create');
    }

    public function store(Request $request)
    {
        // $record = new Role;
        // $record->fill($request->all());
        // $record->save();

        return response([
            'status' => true
        ]);
    }
 
    public function edit($id)
    {
        $record = Reschedule::find($id);
       	$this->setSubtitle('No WBS/IO : '.$record->pengujian->penerimaan->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Reschedule' => '#', 'Reschedule Jadwal' => '#']);
        return $this->render('modules.reschedule.edit',[
        	'record' => $record,
        	'detail' => $record->detailreschedule,
    	]);
    }

    public function detil($id)
    {
        $record = Reschedule::find($id);
       	$this->setSubtitle('No WBS/IO : '.$record->pengujian->penerimaan->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Reschedule' => '#', 'Detil Reschedule Jadwal' => '#']);
        return $this->render('modules.reschedule.detail',[
        	'record' => $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp,
            'kaji_ulang' => $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang,
            'surat' => $record->pengujian->penerimaan->konfirmasi->va->surat,
        	'detail' => $record->detailreschedule,
    	]);
    }

    public function update(RescheduleRequest $request, $id)
    {
        DB::beginTransaction();
    	try {
    		$reschedule = Reschedule::find($id);
    		$reschedule->status = 1;
    		$reschedule->save();
			$reschedule->updateDetail($request->detail);
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function download($id)
    {
        $daftar = PendaftaranPengujian::find($id);
        if(file_exists(public_path('storage/'.$daftar->bukti)))
        {
            return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
        }
        return response()->view('errors.download');
    }

    public function destroy($id)
    {
        return response([
            'status' => true,
        ]);
    }
}
