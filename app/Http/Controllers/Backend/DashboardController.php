<?php

namespace App\Http\Controllers\Backend;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;

/* Models */
use App\Models\Authentication\Role;

/* Libraries */
use DataTables;
use Carbon;
use Hash;

class DashboardController extends Controller
{
    protected $link = 'backend/dashboard/';
    protected $perms = 'dashboard';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Dashboard");
        $this->setPerms($this->perms);
    }

    public function index()
    {
        return $this->render('modules.dashboard.index', ['mockup' => true]);
    }
}
