<?php

namespace App\Http\Controllers\PenerimaanBarangUji;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\PenerimaanBarang\PenerimaanBarangRequest;
/* Models */
use App\Models\Authentication\Role;
use App\Models\Authentication\User;
use App\Models\VirtualAccount\KonfirmasiPembayaran;
use App\Models\PenerimaanBarang\PenerimaanBarang;
use App\Models\PenerimaanBarang\PenerimaanBarangDetail;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\FrontEnd\PendaftaranPengujianDetail;
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\KajiUlang\KajiUlang;
use App\Models\KajiUlang\Detail;
use App\Models\Files;
use App\Models\Act\ActDisposisi;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Excel;
use Auth;
use DB;
use PDF;

class PenerimaanBarangUjiController extends Controller
{
    protected $link = 'penerimaan-barang-uji/';
    protected $perms = 'penerimaan-barang-uji';
    protected $listStructs = [];

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Penerimaan Barang Uji");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Penerimaan Barang Uji' => '#']);

        // Header Grid Datatable
        $this->listStructs = [
            'listStruct' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '80px',
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '250px',
                ],
            ],
            'listStruct2' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'keterangan',
                    'name' => 'keterangan',
                    'label' => 'Keterangan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '80px',
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '250px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '130px',
                ],
            ],
            'listStruct3' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'keterangan',
                    'name' => 'keterangan',
                    'label' => 'Keterangan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '80px',
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '250px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '130px',
                ],
            ],
        ];
    }

    public function grid(Request $request)
    {

    	if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['adminlab-siskit']) || auth()->user()->hasRole(['adminlab-sistgi']) || auth()->user()->hasRole(['adminlab-tegangan-rendah']) || auth()->user()->hasRole(['adminlab-tegangan-tinggi'])){
        	if(auth()->user()->hasRole(['adminlab-siskit','admin'])){
		        $records = KonfirmasiPembayaran::where('status', '>', 0)
			        							 ->whereHas('va', function($z){
			        							 	$z->whereHas('surat', function($y){
			        							 		$y->whereHas('kaji_ulang', function($x){
			        							 			$x->whereHas('pp', function($m){
			        							 				$m->where('jenis', 1)
			        							 				  ->where('layanan_id', 2);
			        							 			});
			        							 		});
			        							 	});
			        							})
                                                ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                                     $q->where('wbs_io','like','%'.$no_wbs.'%');
                                                })
                                                ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                        $pp->where('no_order','like','%'.$no_order.'%');
                                                     });
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                        $pp->where('layanan_id',$jenis_pelayanan_id);
                                                     });
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                        $pp->where('no_surat','like','%'.$no_surat.'%');
                                                     });
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                     });
                                                })
				        						->doesntHave('penerimaan')
		                                 		->select('*');
		    }elseif(auth()->user()->hasRole(['adminlab-sistgi','admin'])){
		    	$records = KonfirmasiPembayaran::where('status', '>', 0)
			        							 ->whereHas('va', function($z){
			        							 	$z->whereHas('surat', function($y){
			        							 		$y->whereHas('kaji_ulang', function($x){
			        							 			$x->whereHas('pp', function($m){
			        							 				$m->where('jenis', 1)
			        							 				  ->where('layanan_id', 3);
			        							 			});
			        							 		});
			        							 	});
			        							})
                                                ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                                     $q->where('wbs_io','like','%'.$no_wbs.'%');
                                                })
                                                ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                        $pp->where('no_order','like','%'.$no_order.'%');
                                                     });
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                        $pp->where('layanan_id',$jenis_pelayanan_id);
                                                     });
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                        $pp->where('no_surat','like','%'.$no_surat.'%');
                                                     });
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                     });
                                                })
				        						->doesntHave('penerimaan')
		                                 		->select('*');
		    }elseif(auth()->user()->hasRole(['adminlab-tegangan-rendah','admin'])){
		    	$records = KonfirmasiPembayaran::where('status', '>', 0)
			        							 ->whereHas('va', function($z){
			        							 	$z->whereHas('surat', function($y){
			        							 		$y->whereHas('kaji_ulang', function($x){
			        							 			$x->whereHas('pp', function($m){
			        							 				$m->where('jenis', 1)
			        							 				  ->where('layanan_id', 4);
			        							 			});
			        							 		});
			        							 	});
			        							})
                                                ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                                     $q->where('wbs_io','like','%'.$no_wbs.'%');
                                                })
                                                ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                        $pp->where('no_order','like','%'.$no_order.'%');
                                                     });
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                        $pp->where('layanan_id',$jenis_pelayanan_id);
                                                     });
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                        $pp->where('no_surat','like','%'.$no_surat.'%');
                                                     });
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                     });
                                                })
                                                ->where('tipe', 0)
				        						->doesntHave('penerimaan')
		                                 		->select('*');
		    }elseif(auth()->user()->hasRole(['adminlab-tegangan-tinggi','admin'])){
		    	$records = KonfirmasiPembayaran::where('status', '>', 0)
			        							->whereHas('va', function($z){
			        							 	$z->whereHas('surat', function($y){
			        							 		$y->whereHas('kaji_ulang', function($x){
			        							 			$x->whereHas('pp', function($m){
			        							 				$m->where('jenis', 1)
			        							 				  ->where('layanan_id', 5);
			        							 			});
			        							 		});
			        							 	});
			        							})
                                                ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                                     $q->where('wbs_io','like','%'.$no_wbs.'%');
                                                })
                                                ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                        $pp->where('no_order','like','%'.$no_order.'%');
                                                     });
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                        $pp->where('layanan_id',$jenis_pelayanan_id);
                                                     });
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                        $pp->where('no_surat','like','%'.$no_surat.'%');
                                                     });
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                     });
                                                })
				        						->doesntHave('penerimaan')
		                                 		->select('*');
		    }else{
		    	$records = KonfirmasiPembayaran::where('status', '>', 0)
			        							->whereHas('va', function($z){
			        							 	$z->whereHas('surat', function($y){
			        							 		$y->whereHas('kaji_ulang', function($x){
			        							 			$x->whereHas('pp', function($m){
			        							 				$m->where('jenis', 1);
			        							 			});
			        							 		});
			        							 	});
			        							})
                                                ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                                     $q->where('wbs_io','like','%'.$no_wbs.'%');
                                                })
                                                ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                        $pp->where('no_order','like','%'.$no_order.'%');
                                                     });
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                        $pp->where('layanan_id',$jenis_pelayanan_id);
                                                     });
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                        $pp->where('no_surat','like','%'.$no_surat.'%');
                                                     });
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                     });
                                                })
				        						->doesntHave('penerimaan')
		                                 		->select('*');
		    }
	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
            $records = $records->get();

    	}elseif(auth()->user()->hasRole(['yan-kalibrasi']) || auth()->user()->hasRole(['adminlab-kalibrasi']) || auth()->user()->hasRole(['admin'])){
    		$records = KonfirmasiPembayaran::where('status', '>', 0)
		        							->whereHas('va', function($z){
		        							 	$z->whereHas('surat', function($y){
		        							 		$y->whereHas('kaji_ulang', function($x){
		        							 			$x->whereHas('pp', function($m){
		        							 				$m->where('jenis', 0);
		        							 			});
		        							 		});
		        							 	});
		        							})
                                            ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                                 $q->where('wbs_io','like','%'.$no_wbs.'%');
                                            })
                                            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                 $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                    $pp->where('no_order','like','%'.$no_order.'%');
                                                 });
                                            })
                                            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                 $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                    $pp->where('layanan_id',$jenis_pelayanan_id);
                                                 });
                                            })
                                            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                 $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                    $pp->where('no_surat','like','%'.$no_surat.'%');
                                                 });
                                            })
                                            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                 $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                 });
                                            })
			        						->doesntHave('penerimaan')
	                                 		->select('*');

	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
            $records = $records->get();

    	}else{
    		$records = collect([]);
    	}

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
            	return $record->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
            	$jenis_pengujian = '';
            	if($record->va->surat->kaji_ulang->pp->detail){
	            	$jenis_pengujian .= '<div class="ui ordered list list-more1" data-display="3">';
	            	foreach ($record->va->surat->kaji_ulang->pp->detail as $key => $value) {
	            		$kaji_ulang = KajiUlang::find($record->va->surat->kaji_ulang->id);
	            		if($kaji_ulang){
	            			foreach ($kaji_ulang->detail as $kuy => $val) {
	            				if($val->jenis_id == $value->id){
				            		$jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
	            				}
	            			}
	            		}else{
	            			$jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
	            		}
	            	}
	            	$jenis_pengujian .='</div>';
            	}
            	return $jenis_pengujian;
            })
            ->editColumn('wbs_io', function($record){
                return $record->wbs_io;
            })
            ->editColumn('detil', function($record){
                $btn = '';
				$btn .= $this->makeButton([
					'type' => 'url',
					'tooltip' => 'Detil Data',
					'class' => 'ui teal mini icon button',
					'label' => '<i class="eye icon"></i>',
					'url' => url($this->link.$record->va->surat_id.'/detil')
				]);

                return $btn;
            })
            ->editColumn('status', function($record){
                $status = '<a class="ui orange tag label">Menunggu Surat Penerimaan Barang</a>';
                return $status;
            })
            ->addColumn('action', function ($record) use ($link){
                $btn = '';

                $btn .= $this->makeButton([
                	'type' => 'url',
                	'tooltip' => 'Buat Surat Penerimaan Barang',
                	'class' => 'ui blue mini icon button',
                	'label' => '<i class="edit icon"></i>',
                	'url' => url($this->link.$record->id.'/buat')
                ]);
                return $btn;
            })
            ->rawColumns(['action','status','detil','jenis_pengujian','layanan_lingkup'])
            ->make(true);
    }

    public function progress(Request $request)
    {
        if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['adminlab-siskit']) || auth()->user()->hasRole(['adminlab-sistgi']) || auth()->user()->hasRole(['adminlab-tegangan-rendah']) || auth()->user()->hasRole(['adminlab-tegangan-tinggi'])){
        	if(auth()->user()->hasRole(['adminlab-siskit'])){
		        $records = PenerimaanBarang::whereHas('konfirmasi', function($u){
		        							 $u->whereHas('va', function($z){
		        							 	$z->whereHas('surat', function($y){
		        							 		$y->whereHas('kaji_ulang', function($x){
		        							 			$x->whereHas('pp', function($m){
		        							 				$m->where('jenis', 1)
		        							 				  ->where('layanan_id', 2);
		        							 			});
		        							 		});
		        							 	});
		        							 });
		        							})
									        ->doesntHave('closeorder')
                                            ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                                 $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                                    $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                                 });
                                            })
                                            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                    $pp->where('no_order','like','%'.$no_order.'%');
                                                 });
                                            })
                                            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                    $pp->where('layanan_id',$jenis_pelayanan_id);
                                                 });
                                            })
                                            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                    $pp->where('no_surat','like','%'.$no_surat.'%');
                                                 });
                                            })
                                            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                 });
                                            })
		        							->where('status', 1)
		                                 	->select('*');
		    }elseif(auth()->user()->hasRole(['adminlab-sistgi','admin'])){
		    	$records = PenerimaanBarang::whereHas('konfirmasi', function($u){
		        							 $u->whereHas('va', function($z){
		        							 	$z->whereHas('surat', function($y){
		        							 		$y->whereHas('kaji_ulang', function($x){
		        							 			$x->whereHas('pp', function($m){
		        							 				$m->where('jenis', 1)
		        							 				  ->where('layanan_id', 3);
		        							 			});
		        							 		});
		        							 	});
		        							 });
		        							})
		    								->doesntHave('closeorder')
                                            ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                                 $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                                    $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                                 });
                                            })
                                            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                    $pp->where('no_order','like','%'.$no_order.'%');
                                                 });
                                            })
                                            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                    $pp->where('layanan_id',$jenis_pelayanan_id);
                                                 });
                                            })
                                            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                    $pp->where('no_surat','like','%'.$no_surat.'%');
                                                 });
                                            })
                                            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                 });
                                            })
		        							->where('status', 1)
		                                 	->select('*');
		    }elseif(auth()->user()->hasRole(['adminlab-tegangan-rendah','admin'])){
		    	$records = PenerimaanBarang::whereHas('konfirmasi', function($u){
		        							 $u->whereHas('va', function($z){
		        							 	$z->whereHas('surat', function($y){
		        							 		$y->whereHas('kaji_ulang', function($x){
		        							 			$x->whereHas('pp', function($m){
		        							 				$m->where('jenis', 1)
		        							 				  ->where('layanan_id', 4);
		        							 			});
		        							 		});
		        							 	});
		        							 });
		        							})
		    								->doesntHave('closeorder')
                                            ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                                 $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                                    $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                                 });
                                            })
                                            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                    $pp->where('no_order','like','%'.$no_order.'%');
                                                 });
                                            })
                                            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                    $pp->where('layanan_id',$jenis_pelayanan_id);
                                                 });
                                            })
                                            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                    $pp->where('no_surat','like','%'.$no_surat.'%');
                                                 });
                                            })
                                            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                 });
                                            })
		        							->where('status', 1)
		                                 	->select('*');
		    }elseif(auth()->user()->hasRole(['adminlab-tegangan-tinggi','admin'])){
		    	$records = PenerimaanBarang::whereHas('konfirmasi', function($u){
		        							 $u->whereHas('va', function($z){
		        							 	$z->whereHas('surat', function($y){
		        							 		$y->whereHas('kaji_ulang', function($x){
		        							 			$x->whereHas('pp', function($m){
		        							 				$m->where('jenis', 1)
		        							 				  ->where('layanan_id', 5);
		        							 			});
		        							 		});
		        							 	});
		        							 });
		        							})
		    								->doesntHave('closeorder')
                                            ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                                 $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                                    $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                                 });
                                            })
                                            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                    $pp->where('no_order','like','%'.$no_order.'%');
                                                 });
                                            })
                                            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                    $pp->where('layanan_id',$jenis_pelayanan_id);
                                                 });
                                            })
                                            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                    $pp->where('no_surat','like','%'.$no_surat.'%');
                                                 });
                                            })
                                            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                 });
                                            })
		        							->where('status', 1)
		                                 	->select('*');
		    }else{
		    	$records = PenerimaanBarang::whereHas('konfirmasi', function($u){
		        							 $u->whereHas('va', function($z){
		        							 	$z->whereHas('surat', function($y){
		        							 		$y->whereHas('kaji_ulang', function($x){
		        							 			$x->whereHas('pp', function($m){
		        							 				$m->where('jenis', 1);
		        							 			});
		        							 		});
		        							 	});
		        							 });
		        							})
		    								->doesntHave('closeorder')
                                            ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                                 $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                                    $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                                 });
                                            })
                                            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                    $pp->where('no_order','like','%'.$no_order.'%');
                                                 });
                                            })
                                            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                    $pp->where('layanan_id',$jenis_pelayanan_id);
                                                 });
                                            })
                                            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                    $pp->where('no_surat','like','%'.$no_surat.'%');
                                                 });
                                            })
                                            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                 });
                                            })
		        							->where('status', 1)
		                                 	->select('*');
		    }

	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
            $records = $records->get();

    	}elseif(auth()->user()->hasRole(['yan-kalibrasi']) || auth()->user()->hasRole(['adminlab-kalibrasi']) || auth()->user()->hasRole(['admin'])){
    		$records = PenerimaanBarang::whereHas('konfirmasi', function($u){
	        							 $u->whereHas('va', function($z){
	        							 	$z->whereHas('surat', function($y){
	        							 		$y->whereHas('kaji_ulang', function($x){
	        							 			$x->whereHas('pp', function($m){
	        							 				$m->where('jenis', 0);
	        							 			});
	        							 		});
	        							 	});
	        							 });
	        							})
    									->doesntHave('closeorder')
                                        ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                             $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                                $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                             });
                                        })
                                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                $pp->where('no_order','like','%'.$no_order.'%');
                                             });
                                        })
                                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                $pp->where('layanan_id',$jenis_pelayanan_id);
                                             });
                                        })
                                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                $pp->where('no_surat','like','%'.$no_surat.'%');
                                             });
                                        })
                                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                             });
                                        })
	        							->where('status', 1)
	                                 	->select('*');

	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
            $records = $records->get();

    	}else{
    		$records = collect([]);
    	}

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
            	return $record->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
            	$jenis_pengujian = '';
            	if($record->konfirmasi->va->surat->kaji_ulang->pp->detail){
	            	$jenis_pengujian .= '<div class="ui ordered list list-more2" data-display="3">';
	            	foreach ($record->konfirmasi->va->surat->kaji_ulang->pp->detail as $key => $value) {
	            		$kaji_ulang = KajiUlang::find($record->konfirmasi->va->surat->kaji_ulang->id);
	            		if($kaji_ulang){
	            			foreach ($kaji_ulang->detail as $kuy => $val) {
	            				if($val->jenis_id == $value->id){
				            		$jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
	            				}
	            			}
	            		}else{
	            			$jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
	            		}
	            	}
	            	$jenis_pengujian .='</div>';
            	}
            	return $jenis_pengujian;
            })
            ->editColumn('wbs_io', function($record){
                return $record->konfirmasi->wbs_io;
            })
            ->editColumn('no_surat_penerimaan', function($record){
                return $record->no_kpj;
            })
            ->editColumn('keterangan', function($record){
            	$angka = 0;
				$jumlah = $record->detail_penerimaan_barang->groupBy('detail_pp_id')->count();
				$pembanding = $record->detail_penerimaan_barang->where('flag', 0)->count();

            	foreach ($record->detail_penerimaan_barang->groupBy('detail_pp_id') as $val) {
            		$angka += $val->first()->max;
            	}
            	$nilai = $angka - $pembanding;
            	$xx = $angka - $nilai;

	            return 'Sudah datang <b>'.$xx.'</b> Unit dari <b>'.$angka.'</b> Unit';
            })
            ->editColumn('detil', function($record){
                $btn = '';
				$btn .= $this->makeButton([
					'type' => 'url',
					'tooltip' => 'Detil Data',
					'class' => 'ui teal mini icon button',
					'label' => '<i class="eye icon"></i>',
					'url' => url($this->link.$record->konfirmasi->va->surat_id.'/detil')
				]);

                return $btn;
            })
            ->editColumn('status', function($record){
                $status = '<a class="ui green tag label">Barang Uji Diterima</a>';
                return $status;
            })
            ->addColumn('action', function ($record) use ($link){
                $btn = '';
                $btn .= $this->makeButton([
                	'type' => 'url',
                	'tooltip' => 'Ubah Surat Penerimaan Barang',
                	'class' => 'ui blue mini icon button',
                	'label' => '<i class="edit icon"></i>',
                	'url' => url($this->link.$record->id.'/ubah')
                ]);
                $btn .= $this->makeButton([
                	'type' => 'url',
                	'tooltip' => 'Cetak Surat Penerimaan Barang',
                	'class' => 'ui violet mini icon button',
                	'label' => '<i class="print icon"></i>',
                	'url' => url($this->link.$record->id.'/cetakSurat')
                ]);
                $btn .= $this->makeButton([
                	'type' => 'url',
                	'tooltip' => 'Cetak Kartu Gantung',
                	'class' => 'ui orange mini icon button',
                	'label' => '<i class="print icon"></i>',
                	'url' => url($this->link.$record->id.'/cetakKartu')
                ]);
                return $btn;
            })
            ->rawColumns(['action','status','detil','jenis_pengujian','layanan_lingkup','keterangan'])
            ->make(true);
    }

    public function historis(Request $request)
    {
    	if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['adminlab-siskit']) || auth()->user()->hasRole(['adminlab-sistgi']) || auth()->user()->hasRole(['adminlab-tegangan-rendah']) || auth()->user()->hasRole(['adminlab-tegangan-tinggi'])){
        	if(auth()->user()->hasRole(['adminlab-siskit'])){
		        $records = PenerimaanBarang::whereHas('closeorder', function($u){
		        								$u->whereIn('status', [0,1]);
		        							})
                                            ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                                 $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                                    $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                                 });
                                            })
                                            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                    $pp->where('no_order','like','%'.$no_order.'%');
                                                 });
                                            })
                                            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                    $pp->where('layanan_id',$jenis_pelayanan_id);
                                                 });
                                            })
                                            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                    $pp->where('no_surat','like','%'.$no_surat.'%');
                                                 });
                                            })
                                            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                 });
                                            })
		        							->where('status', 1)
		                                 	->select('*');
		    }elseif(auth()->user()->hasRole(['adminlab-sistgi','admin'])){
		    	$records = PenerimaanBarang::whereHas('closeorder', function($u){
									    		$u->whereIn('status', [0,1]);
									    	})
                                            ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                                 $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                                    $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                                 });
                                            })
                                            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                    $pp->where('no_order','like','%'.$no_order.'%');
                                                 });
                                            })
                                            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                    $pp->where('layanan_id',$jenis_pelayanan_id);
                                                 });
                                            })
                                            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                    $pp->where('no_surat','like','%'.$no_surat.'%');
                                                 });
                                            })
                                            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                 });
                                            })
		        							->where('status', 1)
		                                 	->select('*');
		    }elseif(auth()->user()->hasRole(['adminlab-tegangan-rendah','admin'])){
		    	$records = PenerimaanBarang::whereHas('closeorder', function($u){
		    									$u->whereIn('status', [0,1]);
		    								})
                                            ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                                 $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                                    $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                                 });
                                            })
                                            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                    $pp->where('no_order','like','%'.$no_order.'%');
                                                 });
                                            })
                                            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                    $pp->where('layanan_id',$jenis_pelayanan_id);
                                                 });
                                            })
                                            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                    $pp->where('no_surat','like','%'.$no_surat.'%');
                                                 });
                                            })
                                            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                 });
                                            })
		        							->where('status', 1)
		                                 	->select('*');
		    }elseif(auth()->user()->hasRole(['adminlab-tegangan-tinggi','admin'])){
		    	$records = PenerimaanBarang::whereHas('closeorder', function($u){
		    									$u->whereIn('status', [0,1]);
		    								})
                                            ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                                 $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                                    $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                                 });
                                            })
                                            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                    $pp->where('no_order','like','%'.$no_order.'%');
                                                 });
                                            })
                                            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                    $pp->where('layanan_id',$jenis_pelayanan_id);
                                                 });
                                            })
                                            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                    $pp->where('no_surat','like','%'.$no_surat.'%');
                                                 });
                                            })
                                            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                 });
                                            })
		        							->where('status', 1)
		                                 	->select('*');
		    }else{
		    	$records = PenerimaanBarang::whereHas('closeorder', function($u){
		    									$u->whereIn('status', [0,1]);
		    								})
                                            ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                                 $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                                    $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                                 });
                                            })
                                            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                    $pp->where('no_order','like','%'.$no_order.'%');
                                                 });
                                            })
                                            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                    $pp->where('layanan_id',$jenis_pelayanan_id);
                                                 });
                                            })
                                            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                    $pp->where('no_surat','like','%'.$no_surat.'%');
                                                 });
                                            })
                                            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                 });
                                            })
		        							->where('status', 1)
		                                 	->select('*');
		    }

	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
            $records = $records->get();
    	}elseif(auth()->user()->hasRole(['yan-kalibrasi']) || auth()->user()->hasRole(['adminlab-kalibrasi']) || auth()->user()->hasRole(['admin'])){
    		$records = PenerimaanBarang::whereHas('closeorder', function($u){
    										$u->whereIn('status', [0,1]);
    									})
                                        ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                             $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                                $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                             });
                                        })
                                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                $pp->where('no_order','like','%'.$no_order.'%');
                                             });
                                        })
                                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                $pp->where('layanan_id',$jenis_pelayanan_id);
                                             });
                                        })
                                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                $pp->where('no_surat','like','%'.$no_surat.'%');
                                             });
                                        })
                                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                             });
                                        })
	        							->where('status', 1)
	                                 	->select('*');

	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
            $records = $records->get();
    	}else{
    		$records = collect([]);
    	}
        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
            	return $record->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
            	$jenis_pengujian = '';
            	if($record->konfirmasi->va->surat->kaji_ulang->pp->detail){
	            	$jenis_pengujian .= '<div class="ui ordered list list-more2" data-display="3">';
	            	foreach ($record->konfirmasi->va->surat->kaji_ulang->pp->detail as $key => $value) {
	            		$kaji_ulang = KajiUlang::find($record->konfirmasi->va->surat->kaji_ulang->id);
	            		if($kaji_ulang){
	            			foreach ($kaji_ulang->detail as $kuy => $val) {
	            				if($val->jenis_id == $value->id){
				            		$jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
	            				}
	            			}
	            		}else{
	            			$jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
	            		}
	            	}
	            	$jenis_pengujian .='</div>';
            	}
            	return $jenis_pengujian;
            })
            ->editColumn('wbs_io', function($record){
                return $record->konfirmasi->wbs_io;
            })
            ->editColumn('no_surat_penerimaan', function($record){
                return $record->no_kpj;
            })
            ->editColumn('keterangan', function($record){
            	$angka = 0;
				$jumlah = $record->detail_penerimaan_barang->groupBy('detail_pp_id')->count();
				$pembanding = $record->detail_penerimaan_barang->where('flag', 0)->count();

            	foreach ($record->detail_penerimaan_barang->groupBy('detail_pp_id') as $val) {
            		$angka += $val->first()->max;
            	}
            	$nilai = $angka - $pembanding;
            	$xx = $angka - $nilai;

	            return 'Sudah datang <b>'.$xx.'</b> Unit dari <b>'.$angka.'</b> Unit';
            })
            ->editColumn('detil', function($record){
                $btn = '';
				$btn .= $this->makeButton([
					'type' => 'url',
					'tooltip' => 'Detil Data',
					'class' => 'ui teal mini icon button',
					'label' => '<i class="eye icon"></i>',
					'url' => url($this->link.$record->konfirmasi->va->surat_id.'/detil')
				]);

                return $btn;
            })
            ->editColumn('status', function($record){
                $status = '<a class="ui green tag label">Barang Uji Diterima</a>';
                return $status;
            })
            ->addColumn('action', function ($record) use ($link){
                $btn = '';
                // $btn .= $this->makeButton([
                // 	'type' => 'url',
                // 	'tooltip' => 'Ubah Surat Penerimaan Barang',
                // 	'class' => 'ui blue mini icon button',
                // 	'label' => '<i class="edit icon"></i>',
                // 	'url' => url($this->link.$record->id.'/ubah')
                // ]);
                $btn .= $this->makeButton([
                	'type' => 'url',
                	'tooltip' => 'Cetak Surat Penerimaan Barang',
                	'class' => 'ui violet mini icon button',
                	'label' => '<i class="print icon"></i>',
                	'url' => url($this->link.$record->id.'/cetakSurat')
                ]);
                $btn .= $this->makeButton([
                	'type' => 'url',
                	'tooltip' => 'Cetak Kartu Gantung',
                	'class' => 'ui orange mini icon button',
                	'label' => '<i class="print icon"></i>',
                	'url' => url($this->link.$record->id.'/cetakKartu')
                ]);
                return $btn;
            })
            ->rawColumns(['action','status','detil','jenis_pengujian','layanan_lingkup','keterangan'])
            ->make(true);
    }

    public function index()
    {	
    	if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['adminlab-siskit']) || auth()->user()->hasRole(['adminlab-sistgi']) || auth()->user()->hasRole(['adminlab-tegangan-rendah']) || auth()->user()->hasRole(['adminlab-tegangan-tinggi'])){
	        	if(auth()->user()->hasRole(['adminlab-siskit'])){
			        $records = KonfirmasiPembayaran::where('status', '>', 0)
				        							 ->whereHas('va', function($z){
				        							 	$z->whereHas('surat', function($y){
				        							 		$y->whereHas('kaji_ulang', function($x){
				        							 			$x->whereHas('pp', function($m){
				        							 				$m->where('jenis', 1)
				        							 				  ->where('layanan_id', 2);
				        							 			});
				        							 		});
				        							 	});
				        							})
					        						->doesntHave('penerimaan')
			                                 		->get()->count();
			    }elseif(auth()->user()->hasRole(['adminlab-sistgi','admin'])){
			    	$records = KonfirmasiPembayaran::where('status', '>', 0)
				        							 ->whereHas('va', function($z){
				        							 	$z->whereHas('surat', function($y){
				        							 		$y->whereHas('kaji_ulang', function($x){
				        							 			$x->whereHas('pp', function($m){
				        							 				$m->where('jenis', 1)
				        							 				  ->where('layanan_id', 3);
				        							 			});
				        							 		});
				        							 	});
				        							})
					        						->doesntHave('penerimaan')
			                                 		->get()->count();
			    }elseif(auth()->user()->hasRole(['adminlab-tegangan-rendah','admin'])){
			    	$records = KonfirmasiPembayaran::where('status', '>', 0)
				        							 ->whereHas('va', function($z){
				        							 	$z->whereHas('surat', function($y){
				        							 		$y->whereHas('kaji_ulang', function($x){
				        							 			$x->whereHas('pp', function($m){
				        							 				$m->where('jenis', 1)
				        							 				  ->where('layanan_id', 4);
				        							 			});
				        							 		});
				        							 	});
				        							})
					        						->doesntHave('penerimaan')
			                                 		->get()->count();
			    }elseif(auth()->user()->hasRole(['adminlab-tegangan-tinggi','admin'])){
			    	$records = KonfirmasiPembayaran::where('status', '>', 0)
				        							->whereHas('va', function($z){
				        							 	$z->whereHas('surat', function($y){
				        							 		$y->whereHas('kaji_ulang', function($x){
				        							 			$x->whereHas('pp', function($m){
				        							 				$m->where('jenis', 1)
				        							 				  ->where('layanan_id', 5);
				        							 			});
				        							 		});
				        							 	});
				        							})
					        						->doesntHave('penerimaan')
			                                 		->get()->count();
			    }else{
			    	$records = KonfirmasiPembayaran::where('status', '>', 0)
				        							->whereHas('va', function($z){
				        							 	$z->whereHas('surat', function($y){
				        							 		$y->whereHas('kaji_ulang', function($x){
				        							 			$x->whereHas('pp', function($m){
				        							 				$m->where('jenis', 1);
				        							 			});
				        							 		});
				        							 	});
				        							})
					        						->doesntHave('penerimaan')
			                                 		->get()->count();
			    }
	    	}elseif(auth()->user()->hasRole(['yan-kalibrasi']) || auth()->user()->hasRole(['adminlab-kalibrasi']) || auth()->user()->hasRole(['admin'])){
	    		$records = KonfirmasiPembayaran::where('status', '>', 0)
			        							->whereHas('va', function($z){
			        							 	$z->whereHas('surat', function($y){
			        							 		$y->whereHas('kaji_ulang', function($x){
			        							 			$x->whereHas('pp', function($m){
			        							 				$m->where('jenis', 0);
			        							 			});
			        							 		});
			        							 	});
			        							})
				        						->doesntHave('penerimaan')
		                                 		->get()->count();
	    	}else{
	    		$records = 0;
	    	}

	    	$data = [
	            'mockup' 	=> true,
	            'structs' 	=> $this->listStructs,
	            'records' 	=> $records,
	        ];
        return $this->render('modules.penerimaan-barang-uji.index', $data);
    }

    public function create()
    {
        return $this->render('modules.penerimaan-barang-uji.create');
    }

    public function detil($id)
    {
    	$surat = SuratPenawaran::find($id);
       	$kaji_ulang = KajiUlang::find($surat->kaji_id);
       	$record = PendaftaranPengujian::whereHas('kaji_ulang', function($u){
    										$u->whereHas('surat');
    									})->where('id', $kaji_ulang->pp_id)->get();
        $no_order = PendaftaranPengujian::whereHas('kaji_ulang', function($u){
    										$u->whereHas('surat');
    									})->where('id', $kaji_ulang->pp_id)->pluck('no_order');
        $this->setSubtitle('No Order : '.$record->first()->no_order);
	    $this->setBreadcrumb(['Penerimaan Barang Uji' => '#', 'Detil' => '#']);
       	return $this->render('modules.penerimaan-barang-uji.detail',[
       		'record' => $record->first(), 
       		'kaji_ulang' => $kaji_ulang,
        	'surat' => $surat,
        	'no_order' => $no_order,
   		]); 
    }

    public function store(PenerimaanBarangRequest $request)
    {
    	$cek =0;
    	foreach ($request->data as $key => $value) {
    		if(isset($value['detail'])){
    			$cek +=1;
    		}else{
    		
    		}

    	}
		if($cek <= 0){
			return response()->json([
           	 	'status' => 'false',
            	'message' => 'Data penerimaan tidak boleh kosong'
        	],402);
		}
        DB::beginTransaction();
        try {
        	$penerimaan 					= new PenerimaanBarang;
        	$penerimaan->konfirmasi_id 		= $request->konfirmasi_id;
        	$penerimaan->status 			= 1;
        	$penerimaan->pengirim 			= $request->pengirim;
        	$penerimaan->tanggal_terima 	= $request->tanggal_terima;
        	$penerimaan->penerima_id 		= $request->penerima_id;
        	$penerimaan->save();
        	$penerimaan->saveDetail($request->data);

        	$keterangan = 'menyelesaikan <a>Penerimaan Barang Uji</a>, saat ini surat masuk ke tahap <a>Pengujian</a>';
        	$act = new ActDisposisi;
            $act->parent_id = $penerimaan->konfirmasi->va->surat->kaji_ulang->pp->id;
            $act->pengirim_id = auth()->user()->id;
            $act->penerima_id = auth()->user()->id;
            $act->tipe = 7;
            $act->jenis = 7;
            $act->keterangan = $keterangan;
            $act->save();

            if($penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==1){
                $user = User::whereHas('roles',function($query){
                            $query->where('name','yan-kalibrasi')
                                    ->orWhere('name','msb-kalibrasi')
                                    ->orWhere('name','asman-dal-kalibrasi')
                                    ->orWhere('name','asman-lola-kalibrasi')
                                    ->orWhere('name','pelaksana-kalibrasi');
                        })->get();
            }
            if($penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==2){
                $user = User::whereHas('roles',function($query){
                            $query->where('name','yan-uji')
                                    ->orWhere('name','msb-siskit')
                                    ->orWhere('name','asman-dal-siskit')
                                    ->orWhere('name','asman-lola-siskit')
                                    ->orWhere('name','pelaksana-siskit');
                        })->get();
            }
            if($penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==3){
                $user = User::whereHas('roles',function($query){
                            $query->where('name','yan-uji')
                                    ->orWhere('name','msb-sistgi')
                                    ->orWhere('name','asman-dal-sistgi')
                                    ->orWhere('name','asman-lola-sistgi')
                                    ->orWhere('name','pelaksana-sistgi');
                        })->get();
            }
            if($penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==4){
                $user = User::whereHas('roles',function($query){
                            $query->where('name','yan-uji')
                                    ->orWhere('name','msb-tegangan-rendah')
                                    ->orWhere('name','asman-dal-tegangan-rendah')
                                    ->orWhere('name','asman-lola-tegangan-rendah')
                                    ->orWhere('name','pelaksana-tegangan-rendah');
                        })->get();
            }
            if($penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==5){
                $user = User::whereHas('roles',function($query){
                            $query->where('name','yan-uji')
                                    ->orWhere('name','msb-tegangan-tinggi')
                                    ->orWhere('name','asman-dal-tegangan-tinggi')
                                    ->orWhere('name','asman-lola-tegangan-tinggi')
                                    ->orWhere('name','pelaksana-tegangan-tinggi');
                        })->get();
            }

            foreach ($user as $row) {
                if(!is_null($row->device_id)){
                     $this->onesignal('Verifikasi Barang Uji Baru dengan nomor order '.$penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$row->device_id,$penerimaan->toArray(),'pengujian','verifikasi-barang-uji-baru');
                }
            }

            DB::commit();
	    }catch (\Exception $e) {
	      DB::rollback();
	      return response([
	        'status' => 'error',
	        'message' => 'An error occurred!',
	        'error' => $e->getMessage(),
	      ], 500);
	    }

        return response([
            'status' => true
        ]);
    }

    public function buat($id)
    {
    	$this->setTitle('Buat Penerimaan Barang Uji');
    	$this->setBreadcrumb(['Penerimaan Barang Uji' => '#', 'Buat' => '#']);
        $record = KonfirmasiPembayaran::find($id);
        return $this->render('modules.penerimaan-barang-uji.create',['record' => $record]);
    }

    public function ubah($id)
    {
        $this->setTitle('Ubah Penerimaan Barang Uji');
    	$this->setBreadcrumb(['Penerimaan Barang Uji' => '#', 'Ubah' => '#']);
        $record = PenerimaanBarang::find($id);
        return $this->render('modules.penerimaan-barang-uji.edit',['record' => $record]);
    }

    public function show($id)
    {
       return $this->render('modules.penerimaan-barang-uji.detail');
    }

    public function update(PenerimaanBarangRequest $request, $id)
    {
    	DB::beginTransaction();
        try {
        	$penerimaan = PenerimaanBarang::find($id);
        	$penerimaan->save();
        	$penerimaan->updateDetail($request->data, $request->tanggal_terima);

        	$keterangan = 'merubah <a>Penerimaan Barang Uji</a>, saat ini surat masuk ke tahap <a>Pengujian</a>';
        	$act = new ActDisposisi;
            $act->parent_id = $penerimaan->konfirmasi->va->surat->kaji_ulang->pp->id;
            $act->pengirim_id = auth()->user()->id;
            $act->penerima_id = auth()->user()->id;
            $act->tipe = 7;
            $act->jenis = 7;
            $act->keterangan = $keterangan;
            $act->save();

            DB::commit();
	    }catch (\Exception $e) {
	      DB::rollback();
	      return response([
	        'status' => 'error',
	        'message' => 'An error occurred!',
	        'error' => $e->getMessage(),
	      ], 500);
	    }
        return response([
            'status' => true
        ]);
    }

    public function grant(Request $request, $id)
    {
        $record = Role::find($id);
        $record->perms()->sync($request->check);

        return response([
            'status' => true
        ]);
    }

    public function preview($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function previewMultiple($id, $type)
    {
    	if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $c->filename,
                        ];
                    }
                }
                return $this->render('pdf-multiple', [
		        	'mockup' => true,
		        	'data' => $rename,
		        ]);
            }
        }
    }

    public function download($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

    public function destroy($id)
    {
        $record = Role::find($id);
        $record->delete();

        return response([
            'status' => true,
        ]);
    }

    public function printSuratPdf(Request $request, $id){
    	$record = PenerimaanBarang::find($id);
        $data = [
        	'records' => $record,
        ];
	    $pdf = PDF::loadView('modules.penerimaan-barang-uji.cetak.cetak', $data);
        return $pdf->stream('Penerimaan Barang Uji '.$record->tanggal.'.pdf',array("Attachment" => false));
 	}

 	public function printKartuPdf(Request $request, $id){
    	$record = PenerimaanBarang::find($id);
        $data = [
        	'records' => $record,
        ];
	    $pdf = PDF::loadView('modules.penerimaan-barang-uji.cetak.cetak-kartu', $data)->setPaper('a4', 'landscape');
        // return $this->render('modules.penerimaan-barang-uji.cetak.cetak-kartu', $data);
        return $pdf->stream('Kartu Gantung '.$record->tanggal.'.pdf',array("Attachment" => false));
 	}
}
