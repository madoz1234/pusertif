<?php

namespace App\Http\Controllers\NotaDinas;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\KajiUlang\KajiUlang;
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\KajiUlang\Detail;
use App\Models\Files;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
/* Models */
use App\Models\Authentication\Role;


/* Libraries */
use DataTables;
use Carbon;
use Hash;


class NotaDinasController extends Controller
{
    protected $link = 'nota-dinas/';
    protected $perms = 'nota-dinas';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Nota Buku");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Nota Buku' => '#']);

        // Header Grid Datatable
        $this->listStructs = [
            'listStruct'=>[
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                /* --------------------------- */
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'className' => "left aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'className' => "left aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'pemesan',
                    'name' => 'pemesan',
                    'label' => 'Peminta Jasa',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'no_skki',
                    'name' => 'no_skki',
                    'label' => 'No SKKI/SKKO/PRK',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'tgl_skki',
                    'name' => 'tgl_skki',
                    'label' => 'Tgl SKKI/SKKO/PRK',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'total',
                    'name' => 'total',
                    'label' => 'Total (Rp.)',
                    'className' => "right aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => false,
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ], 
            ],
            'listStruct2' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                /* --------------------------- */
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'className' => "left aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'className' => "left aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'pemesan',
                    'name' => 'pemesan',
                    'label' => 'Peminta Jasa',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'no_skki',
                    'name' => 'no_skki',
                    'label' => 'No SKKI/SKKO/PRK',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'tgl_skki',
                    'name' => 'tgl_skki',
                    'label' => 'Tgl SKKI/SKKO/PRK',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'total',
                    'name' => 'total',
                    'label' => 'Total (Rp.)',
                    'className' => "right aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => false,
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
            ],
        ];
    }

    public function grid(Request $request)
    {	
    	if(auth()->user()->hasRole(['keuangan','admin'])){
	        $records = VirtualAccount::whereHas('surat', function($s){
	                                    $s->whereHas('kaji_ulang', function($k){
	                                        $k->whereHas('pp', function($p){
	                                            return $p->whereNotIn('status', [6,7]); // 6:selesai 7:gagal diproses
	                                        });
	                                    });
	                                 })->where('tipe_customer', 0)->where('tipe', 0)->select('*');
	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
	    }else{
    		$records =  VirtualAccount::where('id', 0);
    	}
        
        //Init Sort

        if ($no_surat = $request->no_surat) {
            $records->whereHas('surat', function($surat) use ($no_surat){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                    $kaji->whereHas('pp', function($pp) use ($no_surat){
                        $pp->where('no_surat','like', '%'.$no_surat.'%');
                    });
                });
            });
        }
        if ($no_skki = $request->no_skki) {
            $records->whereHas('surat', function($surat) use ($no_skki){
                $surat->where('no_skki','like','%'.$no_skki.'%');
            });
        }
        if ($tanggal_skki = $request->tanggal_skki) {
            $records->whereHas('surat', function($surat) use ($tanggal_skki){
                $surat->where('tgl_skki',Carbon::createFromFormat('d/m/Y',$tanggal_skki)->format('Y-m-d'));
            });
        }
        if ($no_order = $request->no_order) {
            $records->whereHas('surat', function($surat) use ($no_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                    $kaji->whereHas('pp', function($pp) use ($no_order){
                        $pp->where('no_order','like', '%'.$no_order.'%');
                    });
                });
            });
        }
        if ($tanggal_order = $request->tanggal_order) {
            $records->whereHas('surat', function($surat) use ($tanggal_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                    $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                    });
                });
            });
        }
        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $records->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                    $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                        $pp->where('layanan_id',$jenis_pelayanan_id);
                    });
                });
            });
        }

        $records = $records->get();

        //Filters

        return Datatables::of($records)
	        ->addColumn('num', function ($record) use ($request) {
	             return $request->get('start');
	        })
	        ->editColumn('no_order', function ($record) use ($request) {
	        	return $record->surat->kaji_ulang->pp->no_order;
	        })
	        ->editColumn('tgl_order', function ($record) use ($request) {
	        	return DateToStringYear($record->surat->kaji_ulang->pp->tgl_order);
	        })
	        ->addColumn('pemesan', function ($record) use ($request) {
	        	$pemesan = $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->no_hp;
	        	return $pemesan;
	        })
	        ->editColumn('no_surat', function ($record) use ($request) {
	        	return $record->surat->kaji_ulang->pp->no_surat;
	        })
	        ->addColumn('layanan_lingkup', function ($record) use ($request) {
	        	return $record->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->surat->kaji_ulang->pp->lingkup->nama;
	        })
	        ->addColumn('jenis_pengujian', function ($record) use ($request) {
	            $jenis_pengujian = '';
                if($record->surat->kaji_ulang->pp->detail){
                    $jenis_pengujian .= '<div class="ui ordered list list-more1" data-display="3">';
                    foreach ($record->surat->kaji_ulang->pp->detail as $key => $value) {
                        $kaji_ulang = KajiUlang::find($record->surat->kaji_ulang->id);
                        if($kaji_ulang){
                            foreach ($kaji_ulang->detail as $kuy => $val) {
                                if($val->jenis_id == $value->id){
                                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
                                }
                            }
                        }else{
                            $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
                        }
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
	        })
	        ->editColumn('no_skki', function ($record) use ($request) {
	        	if($record->surat->no_skki){
		        	return $record->surat->no_skki;
	        	}else{
	        		return '-';
	        	}
	        })
	        ->editColumn('tgl_skki', function ($record) use ($request) {
	        	if($record->surat->tgl_skki){
		        	return DateToStringYear($record->surat->tgl_skki);
	        	}else{
	        		return '-';
	        	}
	        })
	        ->editColumn('status', function ($record) use ($request) {
	        	return '<a class="ui red tag label">Menunggu Pengujian</a>';
	        })
	        ->editColumn('total', function ($record) use ($request) {
	        	return formatNumber($record->nominal);
	        })
	        ->editColumn('detil', function($record){
	        	$btn = '';

	        	$btn .= $this->makeButton([
	        		'type' => 'url',
	        		'tooltip' => 'Detil Data',
	        		'class' => 'ui teal mini icon button',
	        		'label' => '<i class="eye icon"></i>',
	        		'url' => url($this->link.$record->surat_id.'/detil')
	        	]);

	        	return $btn;
	        })
	        ->rawColumns(['pemesan','layanan_lingkup','jenis_pengujian','detil','status'])
	        ->make(true);
    }

    public function histori(Request $request)
    {	
    	if(auth()->user()->hasRole(['keuangan','admin'])){
	        $records = VirtualAccount::whereHas('surat', function($s){
	                                    $s->whereHas('kaji_ulang', function($k){
	                                        $k->whereHas('pp', function($p){
	                                            return $p->whereIn('status', [6,7]); // 6:selesai 7:gagal diproses
	                                        });
	                                    });
	                                 })
	                                ->where('tipe_customer', 0)
	                                ->where('tipe', 0)
	                                ->select('*');
	    }else{
    		$records =  VirtualAccount::where('id', 0);
    	}
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }
        if ($no_surat = $request->no_surat) {
            $records->whereHas('surat', function($surat) use ($no_surat){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                    $kaji->whereHas('pp', function($pp) use ($no_surat){
                        $pp->where('no_surat','like', '%'.$no_surat.'%');
                    });
                });
            });
        }
        if ($no_skki = $request->no_skki) {
            $records->whereHas('surat', function($surat) use ($no_skki){
                $surat->where('no_skki','like','%'.$no_skki.'%');
            });
        }
        if ($tanggal_skki = $request->tanggal_skki) {
            $records->whereHas('surat', function($surat) use ($tanggal_skki){
                $surat->where('tgl_skki',Carbon::createFromFormat('d/m/Y',$tanggal_skki)->format('Y-m-d'));
            });
        }
        if ($no_order = $request->no_order) {
            $records->whereHas('surat', function($surat) use ($no_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                    $kaji->whereHas('pp', function($pp) use ($no_order){
                        $pp->where('no_order','like', '%'.$no_order.'%');
                    });
                });
            });
        }
        if ($tanggal_order = $request->tanggal_order) {
            $records->whereHas('surat', function($surat) use ($tanggal_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                    $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                    });
                });
            });
        }
        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $records->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                    $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                        $pp->where('layanan_id',$jenis_pelayanan_id);
                    });
                });
            });
        }

        $records = $records->get();
        

        //Filters

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                 return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->surat->kaji_ulang->pp->tgl_order);
            })
            ->addColumn('pemesan', function ($record) use ($request) {
                $pemesan = $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->no_hp;
                return $pemesan;
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->surat->kaji_ulang->pp->detail){
                    $jenis_pengujian .= '<div class="ui ordered list list-more2" data-display="3">';
                    foreach ($record->surat->kaji_ulang->pp->detail as $key => $value) {
                        $kaji_ulang = KajiUlang::find($record->surat->kaji_ulang->id);
                        if($kaji_ulang){
                            foreach ($kaji_ulang->detail as $kuy => $val) {
                                if($val->jenis_id == $value->id){
                                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
                                }
                            }
                        }else{
                            $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
                        }
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('no_skki', function ($record) use ($request) {
                if($record->surat->no_skki){
                    return $record->surat->no_skki;
                }else{
                    return '-';
                }
            })
            ->editColumn('tgl_skki', function ($record) use ($request) {
                if($record->surat->tgl_skki){
                    return DateToStringYear($record->surat->tgl_skki);
                }else{
                    return '-';
                }
            })
            ->editColumn('status', function ($record) use ($request) {
                return '<a class="ui red tag label">Menunggu Pengujian</a>';
            })
            ->editColumn('total', function ($record) use ($request) {
                return formatNumber($record->nominal);
            })
            ->editColumn('detil', function($record){
                $btn = '';

                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->surat_id.'/detil')
                ]);

                return $btn;
            })
            ->rawColumns(['pemesan','layanan_lingkup','jenis_pengujian','detil','status'])
            ->make(true);
    }

    public function index()
    {	
    	if(auth()->user()->hasRole(['keuangan','admin'])){
    		$on_progress = VirtualAccount::whereHas('surat', function($s){
                                    $s->whereHas('kaji_ulang', function($k){
                                        $k->whereHas('pp', function($p){
                                            return $p->whereNotIn('status', [6,7]);
                                        });
                                    });
                                 })->where('tipe_customer', 0)->where('tipe', 0)->count();
	    }else{
    		$on_progress =  0;
    	}
        return $this->render('modules.nota-dinas.index', [
        	'mockup' => true,
        	'progres' => $on_progress,
        	// 'historis' => $historis,
        	'structs' => $this->listStructs,
        ]);
    }

    public function create()
    {
        return $this->render('modules.nota-dinas.create');
    }

    public function detail($id)
    {
        $detail = Detail::find($id);
        $pp = PendaftaranPengujian::find($id);
        return $this->render('modules.nota-dinas.detail-pengujian',[
            'record' => $detail,
            'pp' => $pp,
        ]);
    }

    public function preview($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function previewMultiple($id, $type)
    {
    	if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $c->filename,
                        ];
                    }
                }
                return $this->render('pdf-multiple', [
		        	'mockup' => true,
		        	'data' => $rename,
		        ]);
            }
        }
    }

    public function download($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

    public function detil($id)
    {
       	$surat = SuratPenawaran::find($id);
       	$kaji_ulang = KajiUlang::find($surat->kaji_id);
       	$record = PendaftaranPengujian::whereHas('kaji_ulang', function($u){
    										$u->whereHas('surat');
    									})->where('id', $kaji_ulang->pp_id)->where('tipe_surat', 0)->get();
        $no_order = PendaftaranPengujian::whereHas('kaji_ulang', function($u){
    										$u->whereHas('surat');
    									})->where('id', $kaji_ulang->pp_id)->where('tipe_surat', 0)->pluck('no_order');
        $this->setSubtitle('No Order : '.$record->first()->no_order);
	    $this->setBreadcrumb(['Nota Buku' => '#', 'Detil' => '#']);
       	return $this->render('modules.nota-dinas.detail',[
       		'record' => $record->first(), 
       		'kaji_ulang' => $kaji_ulang,
        	'surat' => $surat,
        	'no_order' => $no_order,
   		]);
    }
}
