<?php

namespace App\Http\Controllers\PengembalianBarangUji;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Models\PenerimaanBarang\PenerimaanBarang;
use App\Models\PenerimaanBarang\PenerimaanBarangDetail;
use App\Models\PengembalianBarang\PengembalianBarang;
use App\Models\PengembalianBarang\PengembalianBarangDetail;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Http\Requests\PengembalianBarang\PengembalianBarangRequest;
/* Models */
use App\Models\Authentication\Role;
use App\Models\Authentication\User;
use App\Models\Files;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Zipper;
use PDF;

class PengembalianBarangUjiController extends Controller
{
    protected $link = 'pengembalian-barang-uji/';
    protected $perms = 'pengembalian-barang-uji';
    protected $listStructs = [];

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Pengembalian Barang Uji");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Pengembalian Barang Uji' => '#']);

        // Header Grid Datatable
        $this->listStructs = [
            'listStruct' => [
	            [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian dan Merk',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'no_laporan',
                    'name' => 'no_laporan',
                    'label' => 'No Laporan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_laporan',
                    'name' => 'tgl_laporan',
                    'label' => 'Tanggal Laporan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                ],
            ],
            'listStruct2' => [
	            [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian dan Merk',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'no_nota_dinas',
                    'name' => 'no_nota_dinas',
                    'label' => 'No Laporan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_nota_dinas',
                    'name' => 'tgl_nota_dinas',
                    'label' => 'Tanggal Nota Dinas',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                ],
            ],
        ];
    }

    public function grid(Request $request)
    {
    	if(auth()->user()->hasRole(['yan-kalibrasi','adminlab-kalibrasi','admin'])){
        		$records = PenerimaanBarang::whereHas('pengujian', function($u){
        								$u->where('status_data_pengujian', 1);
        							})
        							->whereHas('detail_penerimaan_barang', function($u) {
						    			  $u->whereHas('detail_pengujian', function($z){
						    			  	  $z->whereHas('detailpelaksana', function($w){
						    			  	  	$w->where('status_barang', 1);
						    			  	  });
						    			  	})
						    			    ->where('flag', 0);
						    		})
        							->byLayanan(1)
        							->where('status_pengembalian', 0)
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%')
                                            			->where('tipe', 0);
                                         });
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%')
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id)
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%')
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                                            	->where('tipe_surat', 0);
                                         });
                                    })
        							->select('*');
    	}elseif(auth()->user()->hasRole(['adminlab-siskit','admin'])){
    		$records = PenerimaanBarang::whereHas('pengujian', function($u){
        								$u->where('status_data_pengujian', 1);
        							})
						    		->whereHas('detail_penerimaan_barang', function($u) {
						    			  $u->whereHas('detail_pengujian', function($z){
						    			  	  $z->whereHas('detailpelaksana', function($w){
						    			  	  	$w->where('status_barang', 1);
						    			  	  });
						    			  	})
						    			    ->where('flag', 0);
						    		})
        							->byLayanan(2)
        							->where('status_pengembalian', 0)
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%')
                                            			->where('tipe', 0);
                                         });
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%')
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id)
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%')
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                                            	->where('tipe_surat', 0);
                                         });
                                    })
        							->select('*');
    	}elseif(auth()->user()->hasRole(['adminlab-sistgi','admin'])){
    		$records = PenerimaanBarang::whereHas('pengujian', function($u){
        								$u->where('status_data_pengujian', 1);
        							})
    								->whereHas('detail_penerimaan_barang', function($u) {
						    			  $u->whereHas('detail_pengujian', function($z){
						    			  	  $z->whereHas('detailpelaksana', function($w){
						    			  	  	$w->where('status_barang', 1);
						    			  	  });
						    			  	})
						    			    ->where('flag', 0);
						    		})
        							->byLayanan(3)
        							->where('status_pengembalian', 0)
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%')
                                            			->where('tipe', 0);
                                         });
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%')
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id)
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%')
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                                            	->where('tipe_surat', 0);
                                         });
                                    })
        							->select('*');
    	}elseif(auth()->user()->hasRole(['adminlab-tegangan-rendah'])){
    		$records = PenerimaanBarang::whereHas('pengujian', function($u){
        								$u->where('status_data_pengujian', 1);
        							})
    								->whereHas('detail_penerimaan_barang', function($u) {
						    			  $u->whereHas('detail_pengujian', function($z){
						    			  	  $z->whereHas('detailpelaksana', function($w){
						    			  	  	$w->where('status_barang', 1);
						    			  	  });
						    			  	})
						    			    ->where('flag', 0);
						    		})
        							->byLayanan(4)
        							->where('status_pengembalian', 0)
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%')
                                            			->where('tipe', 0);
                                         });
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%')
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id)
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%')
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                                            	->where('tipe_surat', 0);
                                         });
                                    })
        							->select('*');
    	}elseif(auth()->user()->hasRole(['adminlab-tegangan-tinggi','admin'])){
    		$records = PenerimaanBarang::whereHas('pengujian', function($u){
        								$u->where('status_data_pengujian', 1);
        							})
    								->whereHas('detail_penerimaan_barang', function($u) {
						    			  $u->whereHas('detail_pengujian', function($z){
						    			  	  $z->whereHas('detailpelaksana', function($w){
						    			  	  	$w->where('status_barang', 1);
						    			  	  });
						    			  	})
						    			    ->where('flag', 0);
						    		})
        							->byLayanan(5)
        							->where('status_pengembalian', 0)
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%')
                                            			->where('tipe', 0);
                                         });
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%')
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id)
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%')
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                                            	->where('tipe_surat', 0);
                                         });
                                    })
        							->select('*');
    	}elseif(auth()->user()->hasRole(['yan-uji','admin'])){
    		$records = PenerimaanBarang::whereHas('pengujian', function($u){
        								$u->where('status_data_pengujian', 1);
        							})
    								->whereHas('detail_penerimaan_barang', function($u) {
						    			  $u->whereHas('detail_pengujian', function($z){
						    			  	  $z->whereHas('detailpelaksana', function($w){
						    			  	  	$w->where('status_barang', 1);
						    			  	  });
						    			  	})
						    			    ->where('flag', 0);
						    		})
        							->byLayananV2([2,3,4,5])
        							->where('status_pengembalian', 0)
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%')
                                            			->where('tipe', 0);
                                         });
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%')
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id)
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%')
                                            	->where('tipe_surat', 0);
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                                            	->where('tipe_surat', 0);
                                         });
                                    })
        							->select('*');
    	}else{
    		$records = PenerimaanBarang::where('id', 0);
    	}
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }
        $records = $records->get();

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('no_laporan', function ($record) use ($request) {
                $no_laporan = '';
                $no_laporan .= '<div class="ui ordered list list-more1" data-display="3">';
                foreach ($record->pengujian->laporan_pengujian as $key => $vil) {
                     $no_laporan .= '<div class="item">'.$vil->no_laporan.'</div>';
                }
                $no_laporan .='</div>';
                return $no_laporan;
            })
            ->editColumn('tgl_laporan', function ($record) use ($request) {
                $tgl_laporan = '';
                $tgl_laporan .= '<div class="ui ordered list list-more2" data-display="3">';
                foreach ($record->pengujian->laporan_pengujian as $key => $vil) {
                     $tgl_laporan .= '<div class="item">'.DateToStringYear($vil->tgl_laporan).'</div>';
                }
                $tgl_laporan .='</div>';
                return $tgl_laporan;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                $jenis_pengujian .= '<div class="ui ordered list list-more3" data-display="3">';
                foreach ($record->pengujian->detailpengujian->where('flag', 0) as $key => $vil) {
                     $jenis_pengujian .= '<div class="item">'.$vil->detail_penerimaan_barang->detail_pp->jenis->nama.'-'.$vil->detail_penerimaan_barang->nama_barang.'</div>';
                }
                $jenis_pengujian .='</div>';
                return $jenis_pengujian;
            })
            ->editColumn('wbs_io', function($record){
                return $record->konfirmasi->wbs_io;
            })
            ->editColumn('detil', function($record){
                $btn = '';
                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->id.'/detil')
                ]);
                return $btn;
            })
            ->editColumn('status', function($record){
                $status = '<label class="ui orange tag label">Menunggu</label>';
                return $status;
            })
            ->addColumn('action', function ($record) use ($link){
            	$user = auth()->user();
                $btn = '';
            	if($record->konfirmasi->status == 1){
	                $btn ='<label class="ui red tag label">Menunggu Pelunasan Pembayaran</label>';
            	}else{
            		$btn = $this->makeButton([
	                	'type' => 'url',
	                	'tooltip' => 'Buat Pengembalian',
	                	'class' => 'ui violet mini icon button',
	                	'label' => '<i class="edit icon"></i>',
	                	'url' => url($this->link.$record->id.'/buat-pengembalian')
	                ]);	
            	}
                return $btn;
            })
            ->rawColumns(['action','status','detil','jenis_pengujian','layanan_lingkup','fpp_fpk','no_laporan','tgl_laporan','detil','status'])
            ->make(true);
    }

    public function historis(Request $request)
    {	
    	if(auth()->user()->hasRole(['yan-kalibrasi','adminlab-kalibrasi','admin'])){
        		$records = PengembalianBarang::when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                             $q->whereHas('penerimaan.konfirmasi', function($konfirmasi) use ($no_wbs){
                                                $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%')
                                                			->where('tipe', 0);
                                             });
                                        })
        								->whereHas('penerimaan', function($z){
        									$z->byLayanan(1);
        								})
                                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                $pp->where('no_order','like','%'.$no_order.'%')
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                $pp->where('layanan_id',$jenis_pelayanan_id)
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                $pp->where('no_surat','like','%'.$no_surat.'%')
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->select('*');
    	}elseif(auth()->user()->hasRole(['yan-uji','adminlab-siskit','admin'])){
    		$records = PengembalianBarang::when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                             $q->whereHas('penerimaan.konfirmasi', function($konfirmasi) use ($no_wbs){
                                                $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%')
                                                			->where('tipe', 0);
                                             });
                                        })
    									->whereHas('penerimaan', function($z){
        									$z->byLayanan(2);
        								})
                                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                $pp->where('no_order','like','%'.$no_order.'%')
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                $pp->where('layanan_id',$jenis_pelayanan_id)
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                $pp->where('no_surat','like','%'.$no_surat.'%')
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->select('*');
    	}elseif(auth()->user()->hasRole(['yan-uji','adminlab-sistgi','admin'])){
    		$records = PengembalianBarang::when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                             $q->whereHas('penerimaan.konfirmasi', function($konfirmasi) use ($no_wbs){
                                                $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%')
                                                			->where('tipe', 0);
                                             });
                                        })
    									->whereHas('penerimaan', function($z){
        									$z->byLayanan(3);
        								})
                                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                $pp->where('no_order','like','%'.$no_order.'%')
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                $pp->where('layanan_id',$jenis_pelayanan_id)
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                $pp->where('no_surat','like','%'.$no_surat.'%')
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->select('*');
    	}elseif(auth()->user()->hasRole(['yan-uji','adminlab-tegangan-rendah','admin'])){
    		$records = PengembalianBarang::when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                             $q->whereHas('penerimaan.konfirmasi', function($konfirmasi) use ($no_wbs){
                                                $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%')
                                                			->where('tipe', 0);
                                             });
                                        })
    									->whereHas('penerimaan', function($z){
        									$z->byLayanan(4);
        								})
                                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                $pp->where('no_order','like','%'.$no_order.'%')
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                $pp->where('layanan_id',$jenis_pelayanan_id)
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                $pp->where('no_surat','like','%'.$no_surat.'%')
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->select('*');
    	}elseif(auth()->user()->hasRole(['yan-uji','adminlab-tegangan-tinggi','admin'])){
    		$records = PengembalianBarang::when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                             $q->whereHas('penerimaan.konfirmasi', function($konfirmasi) use ($no_wbs){
                                                $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%')
                                                			->where('tipe', 0);
                                             });
                                        })
    									->whereHas('penerimaan', function($z){
        									$z->byLayanan(5);
        								})
                                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                $pp->where('no_order','like','%'.$no_order.'%')
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                $pp->where('layanan_id',$jenis_pelayanan_id)
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                $pp->where('no_surat','like','%'.$no_surat.'%')
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                             $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->select('*');
    	}else{
    		$records = PengembalianBarang::where('id', 0);
    	}
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }
        $records = $records->get();
        

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('no_nota_dinas', function ($record) use ($request) {
                 return $record->no_nota_dinas;
            })
            ->editColumn('tgl_nota_dinas', function ($record) use ($request) {
               return DateToStringYear($record->tgl_nota_dinas);
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                $jenis_pengujian .= '<div class="ui ordered list list-more4" data-display="3">';
                foreach ($record->detail_pengembalian as $key => $vil) {
                     $jenis_pengujian .= '<div class="item">'.$vil->penerimaandetail->detail_pp->jenis->nama.'-'.$vil->penerimaandetail->nama_barang.'</div>';
                }
                $jenis_pengujian .='</div>';
                return $jenis_pengujian;
            })
            ->editColumn('wbs_io', function($record){
                return $record->penerimaan->konfirmasi->wbs_io;
            })
            ->editColumn('detil', function($record){
                $btn = '';
                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->penerimaan->id.'/detil')
                ]);
                return $btn;
            })
            ->editColumn('status', function($record){
                $status = '<label class="ui green tag label">Berhasil</label>';
                return $status;
            })
            ->addColumn('action', function ($record) use ($link){
            	$user = auth()->user();
                $btn = '';
                $btn = $this->makeButton([
                	'type' => 'url',
                	'tooltip' => 'Cetak Surat Pengembalian Barang',
                	'class' => 'ui violet mini icon button',
                	'label' => '<i class="print icon"></i>',
                	'url' => url($this->link.$record->id.'/cetakSurat')
                ]);
                return $btn;
            })
            ->rawColumns(['action','status','detil','jenis_pengujian','layanan_lingkup','fpp_fpk','no_laporan','tgl_laporan','status','detil'])
            ->make(true);
    }

    public function index()
    {	
    	$user = auth()->user();
    	if(auth()->user()->hasRole(['yan-kalibrasi','adminlab-kalibrasi','admin'])){
    		$satu = PenerimaanBarang::whereHas('pengujian', function($u){
	        								$u->where('status_data_pengujian', 1);
	        							})
    									->whereHas('detail_penerimaan_barang', function($u) {
						    			  $u->whereHas('detail_pengujian', function($z){
						    			  	  $z->whereHas('detailpelaksana', function($w){
						    			  	  	$w->where('status_barang', 1);
						    			  	  });
						    			  	})
						    			    ->where('flag', 0);
							    		})
    									->byLayanan(1)
	    								->where('status_pengembalian', 0)
	    								->get()->count();	
    	}elseif(auth()->user()->hasRole(['adminlab-siskit','admin'])){
    		$satu = PenerimaanBarang::whereHas('pengujian', function($u){
	        								$u->where('status_data_pengujian', 1);
	        							})
    									->whereHas('detail_penerimaan_barang', function($u) {
						    			  $u->whereHas('detail_pengujian', function($z){
						    			  	  $z->whereHas('detailpelaksana', function($w){
						    			  	  	$w->where('status_barang', 1);
						    			  	  });
						    			  	})
						    			    ->where('flag', 0);
							    		})
    									->byLayanan(2)
	    								->where('status_pengembalian', 0)
	    								->get()->count();
    	}elseif(auth()->user()->hasRole(['adminlab-sistgi','admin'])){
    		$satu = PenerimaanBarang::whereHas('pengujian', function($u){
	        								$u->where('status_data_pengujian', 1);
	        							})
    									->whereHas('detail_penerimaan_barang', function($u) {
						    			  $u->whereHas('detail_pengujian', function($z){
						    			  	  $z->whereHas('detailpelaksana', function($w){
						    			  	  	$w->where('status_barang', 1);
						    			  	  });
						    			  	})
						    			    ->where('flag', 0);
							    		})
    									->byLayanan(3)
	    								->where('status_pengembalian', 0)
	    								->get()->count();
    	}elseif(auth()->user()->hasRole(['adminlab-tegangan-rendah','admin'])){
    		$satu = PenerimaanBarang::whereHas('pengujian', function($u){
	        								$u->where('status_data_pengujian', 1);
	        							})
    									->whereHas('detail_penerimaan_barang', function($u) {
						    			  $u->whereHas('detail_pengujian', function($z){
						    			  	  $z->whereHas('detailpelaksana', function($w){
						    			  	  	$w->where('status_barang', 1);
						    			  	  });
						    			  	})
						    			    ->where('flag', 0);
							    		})
    									->byLayanan(4)
	    								->where('status_pengembalian', 0)
	    								->get()->count();
    	}elseif(auth()->user()->hasRole(['adminlab-tegangan-tinggi','admin'])){
    		$satu = PenerimaanBarang::whereHas('pengujian', function($u){
	        								$u->where('status_data_pengujian', 1);
	        							})
    									->whereHas('detail_penerimaan_barang', function($u) {
						    			  $u->whereHas('detail_pengujian', function($z){
						    			  	  $z->whereHas('detailpelaksana', function($w){
						    			  	  	$w->where('status_barang', 1);
						    			  	  });
						    			  	})
						    			    ->where('flag', 0);
							    		})
    									->byLayanan(5)
	    								->where('status_pengembalian', 0)
	    								->get()->count();
    	}elseif(auth()->user()->hasRole(['yan-uji','admin'])){
    		$satu = PenerimaanBarang::whereHas('pengujian', function($u){
	        								$u->where('status_data_pengujian', 1);
	        							})
    									->whereHas('detail_penerimaan_barang', function($u) {
						    			  $u->whereHas('detail_pengujian', function($z){
						    			  	  $z->whereHas('detailpelaksana', function($w){
						    			  	  	$w->where('status_barang', 1);
						    			  	  });
						    			  	})
						    			    ->where('flag', 0);
							    		})
    									->byLayananV2([2,3,4,5])
	    								->where('status_pengembalian', 0)
	    								->get()->count();
    	}else{
    		$satu = 0;
    	}

    	$data = [
            'mockup'    => true,
            'structs'   => $this->listStructs,
            'satu'   	=> $satu,
        ];
        return $this->render('modules.pengembalian-barang-uji.index', $data);
    }

    public function buatPengembalian($id)
    {	
    	$record = PenerimaanBarang::find($id);
       	$this->setTitle('Pengembalian Barang Uji');
       	$this->setSubtitle('No WBS/IO : '.$record->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Pengujian' => '#', 'BuatPengembalianBarang' => '#']);
        return $this->render('modules.pengembalian-barang-uji.buat-pengembalian',[
        	'record' => $record,
    	]);
    }

    public function buatPengembalianBarangUji($arr)
    {
        return $this->render('modules.pengembalian-barang-uji.edit-pengembalian', [
            'data' => $arr,
        ]);
    }

    public function simpanPengembalian(PengembalianBarangRequest $request, $id)
    {
    	$list = explode(',', $request->data);
        DB::beginTransaction();
    	try {
    		$cari = PenerimaanBarangDetail::find($list[0]);
    		$pengembalian 					= new PengembalianBarang;
    		$pengembalian->penerimaan_id 	= $cari->penerimaan_id;
    		$pengembalian->no_nota_dinas 	= $request->nota_dinas;
    		$pengembalian->tgl_nota_dinas 	= $request->tgl_nota;
    		$pengembalian->save();
			$pengembalian->saveDetail($list);

            if($cari->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==1){
                $notif = User::whereHas('roles', function($role){
                            $role->where('name','msb-kalibrasi');
                        })->get();
            }
            if($cari->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==2){
                $notif = User::whereHas('roles', function($role){
                            $role->where('name','msb-siskit');
                        })->get();   
            }
            if($cari->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==3){
                $notif = User::whereHas('roles', function($role){
                            $role->where('name','msb-sistgi');
                        })->get();
            }
            if($cari->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==4){
                $notif = User::whereHas('roles', function($role){
                            $role->where('name','msb-tegangan-rendah');
                        })->get();   
            }
            if($cari->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==5){
                $notif = User::whereHas('roles', function($role){
                            $role->where('name','msb-tegangan-tinggi');
                        })->get();
            }

            foreach ($notif as $val) {
                if(!is_null($val->device_id)){
                     $this->onesignal('Close Order Baru dengan nomor order '.$cari->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$val->device_id,$cari->penerimaan->toArray(),'close-order','onprogress');
                }
            }

    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function printSuratPdf(Request $request, $id){
    	$record = PengembalianBarang::find($id);
        $data = [
        	'records' => $record,
        ];
	    $pdf = PDF::loadView('modules.pengembalian-barang-uji.cetak', $data);
        return $pdf->stream('Surat Pengembalian Barang '.$record->tanggal.'.pdf',array("Attachment" => false));
 	}

 	public function preview($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function previewMultiple($id, $type)
    {
    	if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $c->filename,
                        ];
                    }
                }
                return $this->render('pdf-multiple', [
		        	'mockup' => true,
		        	'data' => $rename,
		        ]);
            }
        }
    }

 	public function download($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

    public function detil($id)
    {
        $record = PenerimaanBarang::find($id);
        $this->setSubtitle('No WBS/IO : '.$record->pengujian->penerimaan->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Pengembalian Barang Uji' => '#', 'Detil Pengembalian Barang Uji' => '#']);
        $lab = $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id;
        if($lab == 1){
	        return $this->render('modules.pengembalian-barang-uji.detail-kalibrasi',[
	            'record' => $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp,
	            'kaji_ulang' => $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang,
	            'surat' => $record->pengujian->penerimaan->konfirmasi->va->surat,
	            'pengujian' => $record->pengujian,
	        ]);
        }else{
        	return $this->render('modules.pengembalian-barang-uji.detail-uji',[
	            'record' => $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp,
	            'kaji_ulang' => $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang,
	            'surat' => $record->pengujian->penerimaan->konfirmasi->va->surat,
	            'pengujian' => $record->pengujian,
	        ]);
        }
    }
}
