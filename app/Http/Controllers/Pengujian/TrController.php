<?php

namespace App\Http\Controllers\Pengujian;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Authentication\User;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\Pengujian\Uji\VerifikasiRequest;
use App\Http\Requests\Pengujian\Uji\VerifikasiPelaksanaRequest;
use App\Http\Requests\Pengujian\Uji\VerifikasiHasilPelaksanaRequest;
use App\Http\Requests\Pengujian\Uji\PelaksanaRequest;
use App\Http\Requests\Pengujian\Uji\LaporanPelaksanaRequest;
use App\Http\Requests\Pengujian\Uji\LaporanRequest;
use App\Http\Requests\Pengujian\Uji\HasilRequest;
use App\Http\Requests\Pengujian\Uji\SimpanDataRequest;

/* Models */
use App\Models\Authentication\Role;
use App\Models\PenerimaanBarang\PenerimaanBarang;
use App\Models\Pengujian\Pengujian;
use App\Models\Pengujian\PengujianDetail;
use App\Models\Pengujian\PengujianDetailPelaksana;
use App\Models\LaporanPengujian\LaporanPengujian;
use App\Models\LaporanPengujian\LaporanPengujianDetail;
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\KajiUlang\KajiUlang;
use App\Models\Act\ActDisposisi;
use App\Models\Act\ActPengujian;
use App\Models\Picture;
use App\Models\Files;
use Illuminate\Support\Facades\Storage;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Zipper;
use PDF;


class TrController extends Controller
{
    protected $link = 'pengujian/layanan-tr/';
    protected $perms = 'pengujian-tr';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Layanan Pengujian Tegangan Rendah");
        $this->setPerms($this->perms);
        $this->setModalSize("tiny");
        $this->setBreadcrumb(['Pengujian' => '#','Layanan Pengujian Tegangan Rendah' => '#']);

        // Header Grid Datatable
        $this->listStructs = [
            'listStruct' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '80px',
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '250px',
                ],
            ],
            'listStruct2' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '80px',
                ],
            ],
            'listStruct3' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '80px',
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '250px',
                ],
            ],
            'listStruct4' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '80px',
                ],
            ],
            'listStruct5' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_laporan',
                    'name' => 'no_laporan',
                    'label' => 'No Laporan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_laporan',
                    'name' => 'tgl_laporan',
                    'label' => 'Tanggal Laporan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian dan Merk',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'fpp_fpk',
                    'name' => 'fpp_fpk',
                    'label' => 'No FPP/FPK',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
            ],
        ];
    }

    public function grid(Request $request)
    {
        $user = auth()->user();
        if($user->hasRole(['yan-uji', 'msb-tegangan-rendah','asman-dal-tegangan-rendah', 'asman-lola-tegangan-rendah', 'pelaksana-tegangan-rendah','admin'])){
        		$records = PenerimaanBarang::byLayanan(4) // 4= Layanan Tegangan Rendah
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%');
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%');
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id);
                                         });
                                    })
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('konfirmasi', function($konfirmasi) use($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                         });
                                    })
                                    ->where('status', 1)
                                    ->where('status_pengujian', 0)
                                    ->select('*');
        }else{
    		$records = PenerimaanBarang::where('id', 0);
    	}
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        $records = $records->get();
        

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->konfirmasi->va->surat->kaji_ulang->pp->detail){
                    $jenis_pengujian .= '<div class="ui ordered list list-more1" data-display="2">';
                    foreach ($record->konfirmasi->va->surat->kaji_ulang->pp->detail as $key => $value) {
                        $kaji_ulang = KajiUlang::find($record->konfirmasi->va->surat->kaji_ulang->id);
                        if($kaji_ulang){
                            foreach ($kaji_ulang->detail as $kuy => $val) {
                                if($val->jenis_id == $value->id){
                                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
                                }
                            }
                        }else{
                            $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
                        }
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('wbs_io', function($record){
                return $record->konfirmasi->wbs_io;
            })
            ->editColumn('status', function($record){
                $status = '<a class="ui orange tag label">On Progress</a>';
                return $status;
            })
            ->editColumn('detil', function($record){
                $btn = '';
                // if($record->pengujian){
	                $btn .= $this->makeButton([
						'type' => 'url',
						'tooltip' => 'Detil Data',
						'class' => 'ui teal mini icon button',
						'label' => '<i class="eye icon"></i>',
						'url' => url($this->link.$record->konfirmasi->va->surat_id.'/detail')
					]);
                // }else{
                // 	$btn .= '-';
                // }

                return $btn;
            })
            ->addColumn('action', function ($record) use ($link){
            	$user = auth()->user();
                $btn = '';
                if($record->pengujian){
                	if($user->hasRole(['pelaksana-tegangan-rendah'])){
                		if($record->pengujian->detailpengujian->where('flag',0)->count() > 0){
                			if($record->pengujian->detailpengujian->where('flag',0)->where('pelaksana_id', $user->id)->count() > 0){
	                			if($record->pengujian->detailpengujian->where('flag',0)->where('verifikasi', 0)->where('pelaksana_id', $user->id)->count() > 0){
		                			$btn .= $this->makeButton([
		                				'type' => 'url',
		                				'tooltip' => 'Buat Verifikasi',
		                				'class' => 'ui violet mini icon button',
		                				'label' => '<i class="edit icon"></i>',
		                				'url' => url($this->link.$record->id.'/edit')
		                			]);
	                			}else{
	                				$btn .= $this->makeButton([
		                				'type' => 'url',
		                				'tooltip' => 'Buat Verifikasi',
		                				'class' => 'ui violet mini icon button disabled',
		                				'label' => '<i class="edit icon"></i>',
		                				'url' => url($this->link.$record->id.'/edit')
		                			]);
	                			}
                			}else{
                				$btn .='-';
                			}
                		}else{
	                		$btn .='-';
                		}
                	}elseif($user->hasRole(['asman-dal-tegangan-rendah','asman-lola-tegangan-rendah'])){
                		$user = auth()->user();
                		$penerima = $record->konfirmasi->va->surat->kaji_ulang->pp->act_dispo->where('tipe', 6)->first()->penerima_id;
		                if($user->id == $penerima){
	                		if($record->pengujian->detailpengujian->where('flag',0)->count() > 0){
	                			if($record->pengujian->detailpengujian->where('flag',0)->where('verifikasi', '>', 0)->count() > 0){
                					if($record->pengujian->detailpengujian->where('flag',0)->where('status', 2)->count() > 0){
		        						$btn .= $this->makeButton([
		        							'type' => 'url',
		        							'tooltip' => 'Disposisi Pelaksana',
		        							'class' => 'ui blue mini icon button disabled',
		        							'label' => '<i class="user icon"></i>',
		        							'url' => url($this->link.$record->id.'/buat')
		        						]);
                						$btn .= $this->makeButton([
		        							'type' => 'url',
		        							'tooltip' => 'Approval',
		        							'class' => 'ui yellow mini icon button',
		        							'label' => '<i class="check icon"></i>',
		        							'url' => url($this->link.$record->id.'/dispoAsmen')
		        						]);
                					}else{
		        						$btn .= $this->makeButton([
		        							'type' => 'url',
		        							'tooltip' => 'Disposisi Pelaksana',
		        							'class' => 'ui blue mini icon button disabled',
		        							'label' => '<i class="user icon"></i>',
		        							'url' => url($this->link.$record->id.'/buat')
		        						]);
		        						$btn .= $this->makeButton([
		        							'type' => 'url',
		        							'tooltip' => 'Approval',
		        							'class' => 'ui yellow mini icon button disabled',
		        							'label' => '<i class="check icon"></i>',
		        							'url' => url($this->link.$record->id.'/dispoAsmen')
		        						]);
                					}
	                			}else{
	                				$btn .= $this->makeButton([
	                					'type' => 'url',
	                					'tooltip' => 'Disposisi Pelaksana',
	                					'class' => 'ui blue mini icon button disabled',
	                					'label' => '<i class="user icon"></i>',
	                					'url' => url($this->link.$record->id.'/buat')
	                				]);
	                			}
	                		}else{
		                		$btn .= $this->makeButton([
									'type' => 'url',
									'tooltip' => 'Disposisi Pelaksana',
									'class' => 'ui blue mini icon button',
									'label' => '<i class="user icon"></i>',
									'url' => url($this->link.$record->id.'/buat')
								]);
	                		}
                		}else{
                			$btn .='-';
                		}
                	}elseif($user->hasRole(['msb-tegangan-rendah'])){
                		if($record->pengujian->detailpengujian->where('flag',0)->count() > 0){
							if($record->pengujian->detailpengujian->where('flag',0)->where('verifikasi', '>', 0)->count() > 0){
								if($record->pengujian->detailpengujian->where('flag',0)->where('status', 3)->count() > 0){
									$btn .= $this->makeButton([
										'type' => 'modal',
										'tooltip' => 'Disposisi',
										'class' => 'blue icon button dispoBarangUji disabled',
										'label' => '<i class="user icon"></i>',
										'data' => [
											'id' => $record->id,
										],
										'id'   => $record->id
									]);
									$btn .= $this->makeButton([
		                				'type' => 'url',
		                				'tooltip' => 'Approval',
		                				'class' => 'ui yellow mini icon button',
		                				'label' => '<i class="check icon"></i>',
		                				'url' => url($this->link.$record->id.'/dispoMsb')
	                				]);
                				}else{
                					$btn .= $this->makeButton([
                						'type' => 'modal',
                						'tooltip' => 'Disposisi',
                						'class' => 'blue icon button dispoBarangUji disabled',
                						'label' => '<i class="user icon"></i>',
                						'data' => [
                							'id' => $record->id,
                						],
                						'id'   => $record->id
                					]);
                					$btn .= $this->makeButton([
                						'type' => 'url',
                						'tooltip' => 'Approval',
                						'class' => 'ui yellow mini icon button disabled',
                						'label' => '<i class="check icon"></i>',
                						'url' => url($this->link.$record->id.'/dispoMsb')
                					]);
                				}
							}else{
								$btn .= $this->makeButton([
	                				'type' => 'modal',
	                				'tooltip' => 'Disposisi',
	                				'class' => 'blue icon button dispoBarangUji disabled',
	                				'label' => '<i class="user icon"></i>',
	                				'data' => [
	                					'id' => $record->id,
	                				],
	                				'id'   => $record->id
                				]);
								$btn .= $this->makeButton([
	                				'type' => 'url',
	                				'tooltip' => 'Approval',
	                				'class' => 'ui yellow mini icon button disabled',
	                				'label' => '<i class="check icon"></i>',
	                				'url' => url($this->link.$record->id.'/dispoMsb')
                				]);
							}
                			
                		}else{
                			$btn .= $this->makeButton([
                				'type' => 'modal',
                				'tooltip' => 'Disposisi',
                				'class' => 'blue icon button dispoBarangUji disabled',
                				'label' => '<i class="user icon"></i>',
                				'data' => [
                					'id' => $record->id,
                				],
                				'id'   => $record->id
                			]);
                		}
                	}elseif($user->hasRole(['yan-uji'])){
                		$btn .= $this->makeButton([
                				'type' => 'url',
                				'tooltip' => 'Hasil Verifikasi',
                				'class' => 'ui teal mini icon button',
                				'label' => '<i class="eye icon"></i>',
                				'url' => url($this->link.$record->id.'/dispoYan')
                			]);
                	}else{
                		$btn .='-';
                	}
                }else{
                	if($user->hasRole(['msb-tegangan-rendah'])){
                		$btn .= $this->makeButton([
                			'type' => 'modal',
                			'tooltip' => 'Disposisi',
                			'class' => 'blue icon button dispoBarangUji',
                			'label' => '<i class="user icon"></i>',
                			'data' => [
                				'id' => $record->id,
                			],
                			'id'   => $record->id
                		]);
    				}else{
    					$btn .='-';
    				}
                }
                return $btn;
            })
            ->rawColumns(['action','status','detil','jenis_pengujian','layanan_lingkup'])
            ->make(true);
    }

    public function grid2(Request $request)
    {
        $user = auth()->user();
        if($user->hasRole(['yan-uji', 'msb-tegangan-rendah','asman-dal-tegangan-rendah', 'asman-lola-tegangan-rendah', 'pelaksana-tegangan-rendah','admin'])){
        		$records = PenerimaanBarang::byLayanan(4) // 4= Layanan Tegangan Rendah
                                    ->whereHas('pengujian', function($u){
                                    	$u->where('status', 2);
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%');
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%');
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id);
                                         });
                                    })
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('konfirmasi', function($konfirmasi) use($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                         });
                                    })
                                    ->where('status', 1)
                                    ->where('status_pengujian', 1)
                                    ->select('*');
        }else{
    		$records = PenerimaanBarang::where('id', 0);
    	}
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }
        $records = $records->get();

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->konfirmasi->va->surat->kaji_ulang->pp->detail){
                    $jenis_pengujian .= '<div class="ui ordered list list-more2" data-display="2">';
                    foreach ($record->konfirmasi->va->surat->kaji_ulang->pp->detail as $key => $value) {
                        $kaji_ulang = KajiUlang::find($record->konfirmasi->va->surat->kaji_ulang->id);
                        if($kaji_ulang){
                            foreach ($kaji_ulang->detail as $kuy => $val) {
                                if($val->jenis_id == $value->id){
                                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
                                }
                            }
                        }else{
                            $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
                        }
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('wbs_io', function($record){
                return $record->konfirmasi->wbs_io;
            })
            ->editColumn('detil', function($record){
                $btn = '';
                $btn .= $this->makeButton([
					'type' => 'url',
					'tooltip' => 'Detil Data',
					'class' => 'ui teal mini icon button',
					'label' => '<i class="eye icon"></i>',
					'url' => url($this->link.$record->konfirmasi->va->surat_id.'/detail')
				]);

                return $btn;
            })
            ->rawColumns(['action','detil','jenis_pengujian','layanan_lingkup'])
            ->make(true);
    }

    public function grid3(Request $request)
    {
        $user = auth()->user();
        if($user->hasRole(['yan-uji', 'msb-tegangan-rendah','asman-dal-tegangan-rendah', 'asman-lola-tegangan-rendah', 'pelaksana-tegangan-rendah','admin'])){
        		$records = PenerimaanBarang::byLayanan(4) // 4= Layanan Tegangan Rendah
                                    ->whereHas('pengujian', function($u){
                                    	$u->where('status_data_pengujian', 0);
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%');
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%');
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id);
                                         });
                                    })
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('konfirmasi', function($konfirmasi) use($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                         });
                                    })
                                    ->where('status', 1)
                                    ->where('status_pengujian', 1)
                                    ->select('*');
        }else{
    		$records = PenerimaanBarang::where('id', 0);
    	}

        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }
        $records = $records->get();

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->konfirmasi->va->surat->kaji_ulang->pp->detail){
                    $jenis_pengujian .= '<div class="ui ordered list list-more3" data-display="2">';
                    foreach ($record->konfirmasi->va->surat->kaji_ulang->pp->detail as $key => $value) {
                        $kaji_ulang = KajiUlang::find($record->konfirmasi->va->surat->kaji_ulang->id);
                        if($kaji_ulang){
                            foreach ($kaji_ulang->detail as $kuy => $val) {
                                if($val->jenis_id == $value->id){
                                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
                                }
                            }
                        }else{
                            $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
                        }
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('wbs_io', function($record){
                return $record->konfirmasi->wbs_io;
            })
            ->editColumn('detil', function($record){
                $btn = '';
                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->konfirmasi->va->surat_id.'/detail')
                ]);

                return $btn;
            })
            ->editColumn('status', function($record){
                $status = '<a class="ui orange tag label">On Progress</a>';
                return $status;
            })
            ->editColumn('fpp_fpk', function($record){
                return $record->pengujian->fpp_fpk;
            })
            ->addColumn('action', function ($record) use ($link){
            	$user = auth()->user();
                $btn = '';
                if($record->pengujian){
                	if($record->pengujian->status == 2){
	                	if($user->hasRole(['pelaksana-tegangan-rendah'])){
	                		if($record->pengujian->detailpengujian->first()->detailpelaksana){
	                			if($record->pengujian->detailpengujian->first()->detailpelaksana->pelaksana_laporan_id == 1){
	                				if($record->pengujian->detailpengujian->first()->detailpelaksana->where('pelaksana_id',$user->id)->count() > 0){
	                					$nilai = 0;
	                					foreach ($record->pengujian->detailpengujian->where('flag', 0) as $key => $value) {
	                						if($value->detailpelaksana->status_data == 0){
                								if($value->detailpelaksana->pelaksana_id == $user->id){
	                								$nilai = $nilai+1;
                								}
	                						}
	                					}
	                					if($nilai > 0){
		                					$btn .= $this->makeButton([
				                				'type' => 'url',
				                				'tooltip' => 'Buat Pengujian',
				                				'class' => 'ui blue mini icon button',
				                				'label' => '<i class="edit icon"></i>',
				                				'url' => url($this->link.$record->id.'/pengujianPelaksana')
			                				]);
	                					}else{
		            						$btn .= $this->makeButton([
				                				'type' => 'url',
				                				'tooltip' => 'Hasil Pengujian',
				                				'class' => 'ui teal mini icon button disabled',
				                				'label' => '<i class="edit icon"></i>',
				                				'url' => url($this->link.$record->id.'/pengujianPelaksana')
			                				]);
		            					}
		            				}else{
		            					$btn .='-';
		            				}
	                			}else{
	                				if($record->pengujian->detailpengujian->first()->detailpelaksana->where('pelaksana_laporan_id', $user->id)->count() > 0){
	                					$nilai = 0;
	                					foreach ($record->pengujian->detailpengujian->where('flag', 0) as $key => $value) {
	                						if($value->detailpelaksana->status_file == 0){
	                								if($value->detailpelaksana->pelaksana_laporan_id == $user->id){
		                								$nilai = $nilai+1;
	                								}
	                						}
	                					}
	                					if($nilai > 0){
		                					$btn .= $this->makeButton([
				                				'type' => 'url',
				                				'tooltip' => 'Laporan Hasil Pengujian',
				                				'class' => 'ui teal mini icon button',
				                				'label' => '<i class="edit icon"></i>',
				                				'url' => url($this->link.$record->id.'/pengujianPelaksana')
			                				]);
	                					}else{
	                						$btn .= $this->makeButton([
				                				'type' => 'url',
				                				'tooltip' => 'Laporan Hasil Pengujian',
				                				'class' => 'ui teal mini icon button disabled',
				                				'label' => '<i class="edit icon"></i>',
				                				'url' => url($this->link.$record->id.'/pengujianPelaksana')
			                				]); 
		            						$btn .= $this->makeButton([
				                				'type' => 'url',
				                				'tooltip' => 'Hasil Pengujian',
				                				'class' => 'ui green mini icon button',
				                				'label' => '<i class="eye icon"></i>',
				                				'url' => url($this->link.$record->id.'/pengujianPelaksana')
			                				]);
		            					}
	                				}else{
	                					$btn .= $this->makeButton([
			                				'type' => 'url',
			                				'tooltip' => 'Buat Pengujian',
			                				'class' => 'ui teal mini icon button disabled',
			                				'label' => '<i class="edit icon"></i>',
			                				'url' => url($this->link.$record->id.'/pengujianPelaksana')
		                				]);
	                				}
	                			}
	                		}else{
	                			$btn .='-';
	                		}
	                	}elseif($user->hasRole(['asman-dal-tegangan-rendah','asman-lola-tegangan-rendah'])){
	                			$user = auth()->user();
		                		if($record->pengujian->detailpengujian->first()->detailpelaksana){
		                			$penerima = $record->konfirmasi->va->surat->kaji_ulang->pp->act_dispo->sortByDesc('created_at')->first()->penerima_id;
		                			if($user->id == $penerima){
	                					if($record->pengujian->status_pengujian == 3){
	                						$penerima_laporan = $record->konfirmasi->va->surat->kaji_ulang->pp->act_dispo->where('tipe', 10)->first()->penerima_id;
	                						if($penerima_laporan == $user->id){
												$btn .= $this->makeButton([
													'type' => 'url',
													'tooltip' => 'Disposisi',
													'class' => 'ui yellow mini icon button disabled',
													'label' => '<i class="user icon"></i>',
													'url' => url($this->link.$record->id.'/edit-laporan-pelaksana')
												]);
												$btn .= $this->makeButton([
													'type' => 'url',
													'tooltip' => 'Disposisi',
													'class' => 'ui blue mini icon button disabled',
													'label' => '<i class="user icon"></i>',
													'url' => url($this->link.$record->id.'/edit-pelaksana')
												]);
												$btn .= $this->makeButton([
													'type' => 'url',
													'tooltip' => 'Approval',
													'class' => 'ui yellow mini icon button disabled',
													'label' => '<i class="check icon"></i>',
													'url' => url($this->link.$record->id.'/edit-laporan-pelaksana')
												]);
												$btn .= $this->makeButton([
					                				'type' => 'url',
					                				'tooltip' => 'Buat Laporan Final',
					                				'class' => 'ui teal mini icon button',
					                				'label' => '<i class="edit icon"></i>',
					                				'url' => url($this->link.$record->id.'/edit-laporan-final')
				                				]);
	                						}else{
	                							$btn .= $this->makeButton([
					                				'type' => 'url',
					                				'tooltip' => 'Buat Laporan Final',
					                				'class' => 'ui teal mini icon button',
					                				'label' => '<i class="edit icon"></i>',
					                				'url' => url($this->link.$record->id.'/edit-laporan-final')
				                				]);
	                						}
	                					}else{
	                						if($record->pengujian->detailpengujian->first()->detailpelaksana->pelaksana_laporan_id == 1){
	                							if($record->pengujian->detailpengujian->first()->detailpelaksana->status_hasil_pengujian == 0){
			                						$btn .= $this->makeButton([
						                				'type' => 'url',
						                				'tooltip' => 'Disposisi',
						                				'class' => 'ui blue mini icon button disabled',
						                				'label' => '<i class="user icon"></i>',
						                				'url' => url($this->link.$record->id.'/edit-pelaksana')
						                			]);
			                						$btn .= $this->makeButton([
			                							'type' => 'url',
			                							'tooltip' => 'Disposisi',
			                							'class' => 'ui yellow mini icon button disabled',
			                							'label' => '<i class="user icon"></i>',
			                							'url' => url($this->link.$record->id.'/edit-laporan-pelaksana')
			                						]);
	                							}else{
	                								if($record->pengujian->detailpengujian->first()->detailpelaksana->pelaksana_laporan_id == 1){
		                								$btn .= $this->makeButton([
							                				'type' => 'url',
							                				'tooltip' => 'Disposisi',
							                				'class' => 'ui blue mini icon button disabled',
							                				'label' => '<i class="user icon"></i>',
							                				'url' => url($this->link.$record->id.'/edit-pelaksana')
							                			]);
														$btn .= $this->makeButton([
															'type' => 'url',
															'tooltip' => 'Disposisi',
															'class' => 'ui yellow mini icon button',
															'label' => '<i class="user icon"></i>',
															'url' => url($this->link.$record->id.'/edit-laporan-pelaksana')
														]);
	                								}else{
		                								$btn .= $this->makeButton([
							                				'type' => 'url',
							                				'tooltip' => 'Disposisi',
							                				'class' => 'ui blue mini icon button disabled',
							                				'label' => '<i class="user icon"></i>',
							                				'url' => url($this->link.$record->id.'/edit-pelaksana')
							                			]);
	                									$btn .= $this->makeButton([
															'type' => 'url',
															'tooltip' => 'Disposisi',
															'class' => 'ui yellow mini icon button disabled',
															'label' => '<i class="user icon"></i>',
															'url' => url($this->link.$record->id.'/edit-laporan-pelaksana')
														]);
	                								}
	                							}
	                						}else{
	                							if($record->pengujian->detailpengujian->first()->detailpelaksana->status_file == 1){
		                							$btn .= $this->makeButton([
		                								'type' => 'url',
		                								'tooltip' => 'Disposisi',
		                								'class' => 'ui blue mini icon button disabled',
		                								'label' => '<i class="user icon"></i>',
		                								'url' => url($this->link.$record->id.'/edit-pelaksana')
		                							]);
		                							$btn .= $this->makeButton([
		                								'type' => 'url',
		                								'tooltip' => 'Disposisi',
		                								'class' => 'ui yellow mini icon button disabled',
		                								'label' => '<i class="user icon"></i>',
		                								'url' => url($this->link.$record->id.'/edit-laporan-pelaksana')
		                							]);
		                							$btn .= $this->makeButton([
		                								'type' => 'url',
		                								'tooltip' => 'Approval',
		                								'class' => 'ui yellow mini icon button',
		                								'label' => '<i class="check icon"></i>',
		                								'url' => url($this->link.$record->id.'/edit-laporan-pelaksana')
		                							]);
	                							}else{
		                							$btn .= $this->makeButton([
		                								'type' => 'url',
		                								'tooltip' => 'Disposisi',
		                								'class' => 'ui blue mini icon button disabled',
		                								'label' => '<i class="user icon"></i>',
		                								'url' => url($this->link.$record->id.'/edit-pelaksana')
		                							]);
		                							$btn .= $this->makeButton([
		                								'type' => 'url',
		                								'tooltip' => 'Disposisi',
		                								'class' => 'ui yellow mini icon button disabled',
		                								'label' => '<i class="user icon"></i>',
		                								'url' => url($this->link.$record->id.'/edit-laporan-pelaksana')
		                							]);
	                								$btn .= $this->makeButton([
		                								'type' => 'url',
		                								'tooltip' => 'Approval',
		                								'class' => 'ui yellow mini icon button',
		                								'label' => '<i class="check icon"></i>',
		                								'url' => url($this->link.$record->id.'/edit-laporan-pelaksana')
		                							]);
	                							}
	                						}
	                					}
		                			}else{
		                				$penerima_laporan = $record->konfirmasi->va->surat->kaji_ulang->pp->act_dispo->where('tipe', 10)->first()->penerima_id;
                						if($penerima_laporan == $user->id){
											$btn .= $this->makeButton([
												'type' => 'url',
												'tooltip' => 'Disposisi',
												'class' => 'ui yellow mini icon button disabled',
												'label' => '<i class="user icon"></i>',
												'url' => url($this->link.$record->id.'/edit-laporan-pelaksana')
											]);
											$btn .= $this->makeButton([
												'type' => 'url',
												'tooltip' => 'Disposisi',
												'class' => 'ui blue mini icon button disabled',
												'label' => '<i class="user icon"></i>',
												'url' => url($this->link.$record->id.'/edit-pelaksana')
											]);
											$btn .= $this->makeButton([
												'type' => 'url',
												'tooltip' => 'Approval',
												'class' => 'ui yellow mini icon button disabled',
												'label' => '<i class="check icon"></i>',
												'url' => url($this->link.$record->id.'/edit-laporan-pelaksana')
											]);
                						}else{
                							$btn .='-';
                						}
		                			}
		                		}else{
		                			if($record->pengujian->status_dispo == 2){
	                					if($record->konfirmasi->va->surat->kaji_ulang->pp->act_dispo->where('penerima_id', $user->id)->where('tipe', 10)->count() > 0){
				                			$btn .= $this->makeButton([
				                				'type' => 'url',
				                				'tooltip' => 'Disposisi',
				                				'class' => 'ui blue mini icon button',
				                				'label' => '<i class="user icon"></i>',
				                				'url' => url($this->link.$record->id.'/edit-pelaksana')
				                			]);
			                			}else{
			                				$btn .='-';
			                			}
		                			}else{
		                				$btn .='-';
		                			}
		                		}
	                	}elseif($user->hasRole(['msb-tegangan-rendah'])){
	                			if($record->pengujian->detailpengujian->first()->detailpelaksana){
	                				if($record->pengujian->detailpengujian->first()->detailpelaksana->status_data < 3){
	                					if($record->pengujian->detailpengujian->first()->detailpelaksana->status_data > 0){
	                						if($record->konfirmasi->va->surat->kaji_ulang->pp->act_dispo->sortByDesc('created_at')->first()->tipe == 10){
	                							if($record->konfirmasi->va->surat->kaji_ulang->pp->act_dispo->where('tipe', 10)->count() > 1){
		                							$btn .= $this->makeButton([
		                								'type' => 'modal',
		                								'tooltip' => 'Disposisi',
		                								'class' => 'teal icon button dispoUpload',
		                								'label' => '<i class="user icon"></i>',
		                								'data' => [
		                									'id' => $record->id,
		                								],
		                								'id'   => $record->id
		                							]);
	                							}else{
	                								if($record->pengujian->detailpengujian->first()->detailpelaksana->pelaksana_laporan_id == 1){
		                								$btn .= $this->makeButton([
		                									'type' => 'modal',
		                									'tooltip' => 'Disposisi',
		                									'class' => 'blue icon button dispoPengujian disabled',
		                									'label' => '<i class="user icon"></i>',
		                									'data' => [
		                										'id' => $record->id,
		                									],
		                									'id'   => $record->id
		                								]);
		                								$btn .= $this->makeButton([
															'type' => 'url',
															'tooltip' => 'Approval',
															'class' => 'ui yellow mini icon button disabled',
															'label' => '<i class="check icon"></i>',
															'url' => url($this->link.$record->id.'/edit-laporan-msb')
														]);
	                								}else{
		                								$btn .= $this->makeButton([
		                									'type' => 'modal',
		                									'tooltip' => 'Disposisi',
		                									'class' => 'blue icon button dispoPengujian disabled',
		                									'label' => '<i class="user icon"></i>',
		                									'data' => [
		                										'id' => $record->id,
		                									],
		                									'id'   => $record->id
		                								]);
	                									$btn .= $this->makeButton([
															'type' => 'url',
															'tooltip' => 'Approval',
															'class' => 'ui yellow mini icon button',
															'label' => '<i class="check icon"></i>',
															'url' => url($this->link.$record->id.'/edit-laporan-msb')
														]);
	                								}
	                							}
	                						}elseif($record->konfirmasi->va->surat->kaji_ulang->pp->act_dispo->sortByDesc('created_at')->first()->tipe == 11){
	                							$btn .='-';
	                						}else{
				                				$btn .= $this->makeButton([
													'type' => 'url',
													'tooltip' => 'Approval',
													'class' => 'ui blue mini icon button',
													'label' => '<i class="check icon"></i>',
													'url' => url($this->link.$record->id.'/edit-laporan-msb')
												]);
	                						}
	                					}else{
	                						$btn .= $this->makeButton([
	                							'type' => 'modal',
	                							'tooltip' => 'Disposisi',
	                							'class' => 'blue icon button dispoPengujian disabled',
	                							'label' => '<i class="user icon"></i>',
	                							'data' => [
	                								'id' => $record->id,
	                							],
	                							'id'   => $record->id
	                						]);
	                						$btn .= $this->makeButton([
												'type' => 'url',
												'tooltip' => 'Approval',
												'class' => 'ui yellow mini icon button disabled',
												'label' => '<i class="check icon"></i>',
												'url' => url($this->link.$record->id.'/edit-laporan-msb')
											]);
	                					}
	                				}else{
	                					if($record->konfirmasi->va->surat->kaji_ulang->pp->act_dispo->where('tipe', 11)->count() > 0){
	                						$btn .= $this->makeButton([
	                							'type' => 'modal',
	                							'tooltip' => 'Disposisi',
	                							'class' => 'blue icon button dispoPengujian disabled',
	                							'label' => '<i class="user icon"></i>',
	                							'data' => [
	                								'id' => $record->id,
	                							],
	                							'id'   => $record->id
	                						]);
				                			$btn .= $this->makeButton([
												'type' => 'url',
												'tooltip' => 'Approval',
												'class' => 'ui yellow mini icon button disabled',
												'label' => '<i class="check icon"></i>',
												'url' => url($this->link.$record->id.'/edit-laporan-msb')
											]);
                							$btn .= $this->makeButton([
				                				'type' => 'modal',
				                				'tooltip' => 'Disposisi',
				                				'class' => 'red icon button dispoUpload disabled',
				                				'label' => '<i class="user icon"></i>',
				                				'data' => [
				                					'id' => $record->id,
				                				],
				                				'id'   => $record->id
				                			]);
	                					}else{
	                						$btn .= $this->makeButton([
	                							'type' => 'modal',
	                							'tooltip' => 'Disposisi',
	                							'class' => 'blue icon button dispoPengujian disabled',
	                							'label' => '<i class="user icon"></i>',
	                							'data' => [
	                								'id' => $record->id,
	                							],
	                							'id'   => $record->id
	                						]);
				                			$btn .= $this->makeButton([
												'type' => 'url',
												'tooltip' => 'Approval',
												'class' => 'ui yellow mini icon button disabled',
												'label' => '<i class="check icon"></i>',
												'url' => url($this->link.$record->id.'/edit-laporan-msb')
											]);
		                					$btn .= $this->makeButton([
				                				'type' => 'modal',
				                				'tooltip' => 'Disposisi',
				                				'class' => 'red icon button dispoUpload',
				                				'label' => '<i class="user icon"></i>',
				                				'data' => [
				                					'id' => $record->id,
				                				],
				                				'id'   => $record->id
				                			]);
	                					}
	                				}
	                			}else{
	                				if($record->pengujian->status_dispo == 4){
			                			$btn .= $this->makeButton([
			                				'type' => 'modal',
			                				'tooltip' => 'Disposisi',
			                				'class' => 'blue icon button dispoPengujian',
			                				'label' => '<i class="user icon"></i>',
			                				'data' => [
			                					'id' => $record->id,
			                				],
			                				'id'   => $record->id
			                			]);
	                				}else{
	                					$btn .= $this->makeButton([
	                						'type' => 'modal',
	                						'tooltip' => 'Disposisi',
	                						'class' => 'blue icon button dispoBarangUji disabled',
	                						'label' => '<i class="user icon"></i>',
	                						'data' => [
	                							'id' => $record->id,
	                						],
	                						'id'   => $record->id
	                					]);
	                				}
	                			}
	                	}else{
	                		$btn .='-';
	                	}
                	}else{
                		$btn .='-';
                	}
                }else{
                	if($user->hasRole(['msb-tegangan-rendah'])){
                		$btn .= $this->makeButton([
                			'type' => 'modal',
                			'tooltip' => 'Disposisi',
                			'class' => 'blue icon button dispoBarangUji',
                			'label' => '<i class="user icon"></i>',
                			'data' => [
                				'id' => $record->id,
                			],
                			'id'   => $record->id
                		]);
    				}else{
    					$btn .='';
    				}
                }
                return $btn;
            })
            ->rawColumns(['action','status','detil','jenis_pengujian','layanan_lingkup'])
            ->make(true);
    }

    public function grid4(Request $request)
    {
        $user = auth()->user();
        if($user->hasRole(['yan-uji', 'msb-tegangan-rendah','asman-dal-tegangan-rendah', 'asman-lola-tegangan-rendah', 'pelaksana-tegangan-rendah','admin'])){
        		$records = PenerimaanBarang::byLayanan(4) // 4= Layanan Tegangan Rendah
                                    ->whereHas('pengujian', function($u){
                                    	$u->where('status', 2);
                                    })
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%');
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%');
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id);
                                         });
                                    })
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('konfirmasi', function($konfirmasi) use($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                         });
                                    })
                                    ->where('status', 1)
                                    ->where('status_pengujian', 1)
                                    ->select('*');
        }else{
    		$records = PenerimaanBarang::where('id', 0);
    	}
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }
        $records = $records->get();

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->konfirmasi->va->surat->kaji_ulang->pp->detail){
                    $jenis_pengujian .= '<div class="ui ordered list list-more4" data-display="2">';
                    foreach ($record->konfirmasi->va->surat->kaji_ulang->pp->detail as $key => $value) {
                        $kaji_ulang = KajiUlang::find($record->konfirmasi->va->surat->kaji_ulang->id);
                        if($kaji_ulang){
                            foreach ($kaji_ulang->detail as $kuy => $val) {
                                if($val->jenis_id == $value->id){
                                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
                                }
                            }
                        }else{
                            $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
                        }
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('wbs_io', function($record){
                return $record->konfirmasi->wbs_io;
            })
            ->editColumn('detil', function($record){
                $btn = '';
                $btn .= $this->makeButton([
					'type' => 'url',
					'tooltip' => 'Detil Data',
					'class' => 'ui teal mini icon button',
					'label' => '<i class="eye icon"></i>',
					'url' => url($this->link.$record->konfirmasi->va->surat_id.'/detail')
				]);

                return $btn;
            })
            ->rawColumns(['action','detil','jenis_pengujian','layanan_lingkup'])
            ->make(true);
    }

    public function grid5(Request $request)
    {
        $user = auth()->user();
        if($user->hasRole(['yan-uji', 'msb-tegangan-rendah','asman-dal-tegangan-rendah', 'asman-lola-tegangan-rendah', 'pelaksana-tegangan-rendah','admin'])){
        		$records = LaporanPengujian::whereHas('pengujian.penerimaan', function($a){
        								$a->byLayanan(4);
        							})
                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
                                            $pp->where('no_order','like','%'.$no_order.'%');
                                         });
                                    })
                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
                                            $pp->where('no_surat','like','%'.$no_surat.'%');
                                         });
                                    })
                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                         });
                                    })
                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
                                            $pp->where('layanan_id',$jenis_pelayanan_id);
                                         });
                                    })
                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($no_wbs){
                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                         });
                                    })
        							->select('*');
      	}else{
    		$records = PenerimaanBarang::where('id', 0);
    	}
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }
        $records = $records->get();

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('no_laporan', function ($record) use ($request) {
                return $record->no_laporan;
            })
            ->editColumn('tgl_laporan', function ($record) use ($request) {
                return $record->tgl_laporan;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->detaillaporan){
                    $jenis_pengujian .= '<div class="ui ordered list list-more5" data-display="2">';
                    foreach ($record->detaillaporan as $key => $value) {
                         $jenis_pengujian .= '<div class="item">'.$value->detail_pengujian->detail_penerimaan_barang->detail_pp->jenis->nama.'-'.$value->detail_pengujian->detail_penerimaan_barang->nama_barang.'</div>';
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('wbs_io', function($record){
                return $record->pengujian->penerimaan->konfirmasi->wbs_io;
            })
            ->editColumn('fpp_fpk', function($record){
                $fpp_fpk = '';
                if($record->detaillaporan){
                    $fpp_fpk .= '<div class="ui ordered list list-more6" data-display="2">';
                    foreach ($record->detaillaporan as $key => $value) {
                         $fpp_fpk .= '<div class="item">'.$value->detail_pengujian->fpp_fpk.'</div>';
                    }
                    $fpp_fpk .='</div>';
                }
                return $fpp_fpk;
            })
            ->addColumn('action', function ($record) use ($link){
            	$user = auth()->user();
                $btn = '';
                $btn .= $this->makeButton([
                	'type' => 'url',
                	'tooltip' => 'Cetak Barcode',
                	'class' => 'ui violet mini icon button',
                	'label' => '<i class="print icon"></i>',
                	'url' => url($this->link.$record->id.'/cetakBarcode')
                ]);
                return $btn;
            })
            ->rawColumns(['action','status','detil','jenis_pengujian','layanan_lingkup','fpp_fpk'])
            ->make(true);
    }

    public function index()
    {	
    	$user = auth()->user();
		if($user->hasRole(['pelaksana-tegangan-rendah','admin'])){
			$satu =  PenerimaanBarang::byLayanan(4)
									->where('status', 1)
		                            ->where('status_pengujian', 0)
		                            ->get()->count();

		    $dua = PenerimaanBarang::byLayanan(4)
		    						->whereHas('pengujian', function($u){
                                    	$u->where('status_data_pengujian', 0);
                                    })
                                    ->where('status', 1)
                                    ->where('status_pengujian', 1)
                                    ->get()->count();
		}elseif($user->hasRole(['asman-dal-tegangan-rendah','asman-lola-tegangan-rendah','admin'])){
			$satu =  PenerimaanBarang::byLayanan(4)
									->where('status', 1)
		                            ->where('status_pengujian', 0)
		                            ->get()->count();

		    $dua = PenerimaanBarang::byLayanan(4)
		    						->whereHas('pengujian', function($u){
	                                	$u->where('status_data_pengujian', 0);
	                                })
	                                ->where('status', 1)
	                                ->where('status_pengujian', 1)
	                                ->get()->count();
		}elseif($user->hasRole(['yan-uji','msb-tegangan-rendah','admin'])){
			$satu =  PenerimaanBarang::byLayanan(4)
									->whereHas('konfirmasi', function($k){
							            $k->whereHas('va', function($v){
							                $v->whereHas('surat', function($s){
							                    $s->whereHas('kaji_ulang', function($ku){
							                        $ku->whereHas('pp', function($p){
							                            $p->where('layanan_id', 4);
							                        });
							                    });
							                });
							            });
					        		})
									->where('status', 1)
	                                ->where('status_pengujian', 0)
	                                ->get()->count();
	        // dd($satu);
		    $dua = PenerimaanBarang::byLayanan(4)
		    						->whereHas('konfirmasi', function($k){
							            $k->whereHas('va', function($v){
							                $v->whereHas('surat', function($s){
							                    $s->whereHas('kaji_ulang', function($ku){
							                        $ku->whereHas('pp', function($p){
							                            $p->where('layanan_id', 4);
							                        });
							                    });
							                });
							            });
						        	})
									->whereHas('pengujian', function($u){
                                    	$u->where('status_data_pengujian', 0);
                                    })
                                    ->where('status', 1)
                                    ->where('status_pengujian', 1)
                                    ->get()->count();
		}else{
			$satu='';
			$dua='';
		}
		// dd($satu, $dua);

        $data = [
            'mockup'    => true,
            'structs'   => $this->listStructs,
            'satu'   	=> $satu,
            'dua'   	=> $dua,
        ];
        return $this->render('modules.pengujian.tr.index', $data);
    }

    public function create()
    {
        return $this->render('modules.pengujian.tr.create');
    }

    public function store(VerifikasiPelaksanaRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$user = auth()->user();
    		$pengujian = Pengujian::find($request->pengujian_id);
    		$jenis = array();
    		foreach ($pengujian->penerimaan->detail_penerimaan_barang->groupBy('group_id') as $key => $value) {
    			$jenis[]= $value->first()->detail_pp->jenis->nama;
    		}
    		$jenis = implode(", ",$jenis);
	        $pengujian->status_dispo = 3;
        	$pengujian->save();
		    $pengujian->saveDetailUji($request->detail);

		    foreach ($request->detail as $key => $value) {
		    	$id[]				= $value['pelaksana_id'];
		    }
		    $result = array_unique($id);
		    $cek = User::whereIn('id', $result)->get();
		    $nama = array();
		    foreach ($cek as $key => $val) {
		    	$nama[]=$val->nama;
                if(!is_null($val->device_id)){
                     $this->onesignal('Verifikasi Barang Uji Baru dengan nomor order '.$pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$val->device_id,$pengujian->penerimaan->toArray(),'pengujian','verifikasi-barang-uji-baru');
                }
		    }
		    $nama = implode(", ",$nama);
		    $data_user = User::whereHas('roles', function($u){
		    						$u->where('name', 'pelaksana-tegangan-rendah');
		    				   })->first();

		    $act = new ActDisposisi;
    		$act->parent_id 		= $pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->id;
    		$act->pengirim_id 		= $user->id;
    		$act->penerima_id 		= $data_user['id'];
    		$act->tipe 				= 8;
    		$act->jenis 			= 8;
    		$act->keterangan 		= 'mendisposisikan no WBS : <a>'.$pengujian->penerimaan->konfirmasi->wbs_io.'</a>, Jenis Pengujian : <a>'.$jenis.'</a> kepada <a>'.$nama.'</a> untuk dilakukan <a>Verifikasi</a>';
    		$act->save();

    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }
    
    public function saveDispo(VerifikasiRequest $request, $id)
    {
        DB::beginTransaction();
        try {
        	$penerimaan = PenerimaanBarang::find($id);
        	if($request->pengujian_id > 0){
        		$pengujian = Pengujian::find($request->pengujian_id);
        		$pengujian->penerimaan_id = $id;
        		$pengujian->tipe = $request->layanan;
        		if($pengujian->status_data == 2 && $pengujian->status_dispo == 4){
	        		$pengujian->status_dispo = 2;
        		}elseif($pengujian->status_data == 2 && $pengujian->status_dispo == 2){
        			$pengujian->status_dispo = 1;
        		}else{
        			$pengujian->status_dispo = $request->status_dispo;
        		}
        		$pengujian->save();

	            if($request->penerima_id){
	            	$data_id = $penerimaan->konfirmasi->va->surat->kaji_ulang->pp->id;
	                foreach ($request->penerima_id as $k => $value) {
	                    $act = new ActDisposisi;
	                    $act->parent_id 		= $data_id;
	                    $act->pengirim_id 		= $request->pengirim;
	                    $act->penerima_id 		= $value;
	                    $act->tipe 				= 6;
	                    $act->jenis 			= 5;
	                    $act->keterangan 		= $request->keterangan;
	                    $act->status_pengujian 	= 1;
	                    $act->save();

                        $user = User::find($value);
                        if(!is_null($user->device_id)){
                             $this->onesignal('Verifikasi Barang Uji Baru dengan nomor order '.$pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$user->device_id,$pengujian->penerimaan->toArray(),'pengujian','verifikasi-barang-uji-baru');
                        }
	                }
	            }
        	}else{
	        	$pengujian = new Pengujian;
	        	$pengujian->penerimaan_id = $id;
        		$pengujian->tipe = $request->layanan;
        		$pengujian->status = 1;
        		$pengujian->status_dispo = $request->status_dispo;
        		$pengujian->status_data = 1;
        		$pengujian->save();

        		if($request->penerima_id){
	            	$data_id = $penerimaan->konfirmasi->va->surat->kaji_ulang->pp->id;
	                foreach ($request->penerima_id as $k => $value) {
	                    $act = new ActDisposisi;
	                    $act->parent_id 		= $data_id;
	                    $act->pengirim_id 		= $request->pengirim;
	                    $act->penerima_id 		= $value;
	                    $act->tipe 				= 6;
	                    $act->jenis 			= 5;
	                    $act->keterangan 		= $request->keterangan;
	                    $act->status_pengujian 	= 1;
	                    $act->save();

                        $user = User::find($value);
                        if(!is_null($user->device_id)){
                             $this->onesignal('Verifikasi Barang Uji Baru dengan nomor order '.$pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$user->device_id,$pengujian->penerimaan->toArray(),'pengujian','verifikasi-barang-uji-baru');
                        }
	                }
	            }
        	}
            DB::commit();
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true
        ]);   
    }

    public function saveDispoPengujian(VerifikasiRequest $request, $id)
    {
        DB::beginTransaction();
        try {
        	$user = auth()->user();
			$penerimaan = PenerimaanBarang::find($id);
			$pengujian = Pengujian::find($request->pengujian_id);
			$pengujian->penerimaan_id = $id;
			$pengujian->tipe = $request->layanan;
			if($pengujian->status_data == 2 && $pengujian->status_dispo == 4){
				$pengujian->status_dispo = 2;
			}elseif($pengujian->status_data == 2 && $pengujian->status_dispo == 2){
				$pengujian->status_dispo = 1;
			}else{
				$pengujian->fpp_fpk = $request->fpp_fpk;
				$pengujian->status_dispo = $request->status_dispo;
			}
			$pengujian->save();

			if($request->penerima_id){
			    foreach ($request->penerima_id as $k => $value) {
			    	$data_user = User::find($value);
			        $act = new ActPengujian;
			        $act->parent_id 		= $pengujian->detailpengujian->first()->id;
			        $act->pengirim_id 		= $request->pengirim;
			        $act->penerima_id 		= $value;
			        $act->tipe 				= 2;
			        $act->jenis 			= 2;
			        $act->keterangan 		= '<a>'.$user->nama.'</a> mendisposisi No WBS: <a>'.$pengujian->penerimaan->konfirmasi->wbs_io.'</a> kepada <a>'.$data_user->nama.'</a> untuk dilakukan <a>Pengujian</a>';
			        $act->save();

			        $data_user = User::find($value);
			        $act = new ActDisposisi;
			        $act->parent_id 		= $penerimaan->konfirmasi->va->surat->kaji_ulang->pp->id;
			        $act->pengirim_id 		= $request->pengirim;
			        $act->penerima_id 		= $value;
			        $act->tipe 				= 10;
			        $act->jenis 			= 10;
			        $act->keterangan 		= '-';
			        $act->save();

                    $user = User::find($value);
                    if(!is_null($user->device_id)){
                         $this->onesignal('Pengujian Baru dengan nomor order '.$pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$user->device_id,$pengujian->penerimaan->toArray(),'pengujian','pengujian-baru');
                    }
			    }
			}
            DB::commit();
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true
        ]);   
    }

    public function saveDispoUpload(VerifikasiRequest $request, $id)
    {
        DB::beginTransaction();
        try {
        	$user = auth()->user();
			$penerimaan = PenerimaanBarang::find($id);
			$pengujian = Pengujian::find($request->pengujian_id);
			$pengujian->status_pengujian = 3;
			$pengujian->save();

			if($request->penerima_id){
			    foreach ($request->penerima_id as $k => $value) {
			    	$data_user = User::find($value);
			        $act = new ActPengujian;
			        $act->parent_id 		= $pengujian->detailpengujian->first()->id;
			        $act->pengirim_id 		= $request->pengirim;
			        $act->penerima_id 		= $value;
			        $act->tipe 				= 2;
			        $act->jenis 			= 2;
			        $act->keterangan 		= '<a>'.$user->nama.'</a> mendisposisi No WBS: <a>'.$pengujian->penerimaan->konfirmasi->wbs_io.'</a> kepada <a>'.$data_user->nama.'</a> untuk dilakukan <a>Upload Laporan Pengujian</a>';
			        $act->save();

			        $data_user = User::find($value);
			        $act = new ActDisposisi;
			        $act->parent_id 		= $penerimaan->konfirmasi->va->surat->kaji_ulang->pp->id;
			        $act->pengirim_id 		= $request->pengirim;
			        $act->penerima_id 		= $value;
			        $act->tipe 				= 11;
			        $act->jenis 			= 11;
			        $act->keterangan 		= '-';
			        $act->save();

                    $notif = User::find($value);
                    if(!is_null($notif->device_id)){
                         $this->onesignal('Pengujian Baru dengan nomor order '.$pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$notif->device_id,$pengujian->penerimaan->toArray(),'pengujian','pengujian-baru');
                    }
			    }
			}
            DB::commit();
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true
        ]);   
    }

    public function dispo($id, $tipe)
    {
        $record = PenerimaanBarang::find($id);
        if ($tipe == 0) {
            return $this->render('modules.pengujian.tr.create-disposisi-barang', [
                'record' 	=> $record,
                'pengujian' => $record->pengujian,
                'users' => null
            ]);
        }elseif($tipe == 1){
            return $this->render('modules.pengujian.tr.create-disposisi-pengujian', [
                'record' => $record,
                'pengujian' => $record->pengujian,
                'users' => null
            ]);
        }else{
        	return $this->render('modules.pengujian.tr.create-disposisi-upload', [
                'record' => $record,
                'pengujian' => $record->pengujian,
                'users' => null
            ]);
        }
    }

    public function verifikasiPengujian($id)
    {
        $record = PengujianDetailPelaksana::find($id);
        return $this->render('modules.pengujian.tr.verifikasi-pengujian', [
            'data' => $record,
        ]);
    }

    public function uploadLaporanFinal($id)
    {
    	$data = Pengujian::find($id);
        return $this->render('modules.pengujian.tr.upload-final', [
            'pengujian' => $data,
        ]);
    }

    public function buat($id)
    {	
    	$record = PenerimaanBarang::find($id);
       	$this->setTitle('Verifikasi Barang Uji');
       	$this->setSubtitle('No WBS/IO : '.$record->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Pengujian' => '#', 'Buat' => '#']);
        return $this->render('modules.pengujian.tr.create',[
        	'record' => $record,
    	]);
    }

    public function editPelaksana($id)
    {	
    	$record = PenerimaanBarang::find($id);
       	$this->setTitle('Pelaksanaan Pengujian');
       	$this->setSubtitle('No WBS/IO : '.$record->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Pengujian' => '#', 'Buat Pelaksanaan Pengujian' => '#']);
        return $this->render('modules.pengujian.tr.edit-pelaksana',[
        	'record' => $record,
    	]);
    }

    public function editLaporanPelaksana($id)
    {	
    	$record = PenerimaanBarang::find($id);
       	$this->setTitle('Laporan Pengujian');
       	$this->setSubtitle('No WBS/IO : '.$record->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Pengujian' => '#', 'Buat Laporan Pengujian' => '#']);
        return $this->render('modules.pengujian.tr.edit-laporan-pelaksana',[
        	'record' => $record,
    	]);
    }

    public function editLaporanMsb($id)
    {	
    	$record = PenerimaanBarang::find($id);
       	$this->setTitle('Laporan Pengujian');
       	$this->setSubtitle('No WBS/IO : '.$record->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Pengujian' => '#', 'Buat Laporan Pengujian' => '#']);
        return $this->render('modules.pengujian.tr.edit-laporan-msb',[
        	'record' => $record,
    	]);
    }

    public function editLaporanFinal($id)
    {	
    	$record = PenerimaanBarang::find($id);
       	$this->setTitle('Laporan Pengujian');
       	$this->setSubtitle('No WBS/IO : '.$record->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Pengujian' => '#', 'Buat Laporan Pengujian' => '#']);
        return $this->render('modules.pengujian.tr.edit-laporan-final',[
        	'record' => $record,
    	]);
    }

    public function edit($id)
    {
        $penerimaan = PenerimaanBarang::find($id);
        $record = Pengujian::find($penerimaan->pengujian->id);
       	$this->setTitle('Verifikasi Barang Uji');
       	$this->setSubtitle('No WBS/IO : '.$penerimaan->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Pengujian' => '#', 'Verifikasi' => '#']);
        return $this->render('modules.pengujian.tr.edit',[
        	'record' => $record,
    	]);
    }

    public function dispoPelaksana($id)
    {
        $penerimaan = PenerimaanBarang::find($id);
        $record = Pengujian::find($penerimaan->pengujian->id);
       	$this->setTitle('Verifikasi Barang Uji');
       	$this->setSubtitle('No WBS/IO : '.$penerimaan->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Pengujian' => '#', 'Verifikasi' => '#']);
        return $this->render('modules.pengujian.tr.dispoPelaksana',[
        	'record' => $record,
    	]);
    }

    public function dispoAsmen($id)
    {
        $penerimaan = PenerimaanBarang::find($id);
        $record = Pengujian::find($penerimaan->pengujian->id);
       	$this->setTitle('Verifikasi Barang Uji');
       	$this->setSubtitle('No WBS/IO : '.$penerimaan->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Pengujian' => '#', 'Verifikasi' => '#']);
        return $this->render('modules.pengujian.tr.dispoAsmen',[
        	'record' => $record,
    	]);
    }

    public function dispoMsb($id)
    {
        $penerimaan = PenerimaanBarang::find($id);
        $record = Pengujian::find($penerimaan->pengujian->id);
       	$this->setTitle('Verifikasi Barang Uji');
       	$this->setSubtitle('No WBS/IO : '.$penerimaan->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Pengujian' => '#', 'Verifikasi' => '#']);
        return $this->render('modules.pengujian.tr.dispoMsb',[
        	'record' => $record,
    	]);
    }

    public function dispoYan($id)
    {
        $penerimaan = PenerimaanBarang::find($id);
        $record = Pengujian::find($penerimaan->pengujian->id);
       	$this->setTitle('Verifikasi Barang Uji');
       	$this->setSubtitle('No WBS/IO : '.$penerimaan->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Pengujian' => '#', 'Verifikasi' => '#']);
        return $this->render('modules.pengujian.tr.dispoYan',[
        	'record' => $record,
    	]);
    }

    public function pengujianPelaksana($id)
    {
        $penerimaan = PenerimaanBarang::find($id);
        $record = Pengujian::find($penerimaan->pengujian->id);
       	$this->setTitle('Buat Hasil Pengujian');
       	$this->setSubtitle('No WBS/IO : '.$penerimaan->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Pengujian' => '#', 'Buat Pengujian' => '#']);
        return $this->render('modules.pengujian.tr.pengujianPelaksana',[
        	'record' => $record,
    	]);
    }

    public function detil($id)
    {
    	$surat = SuratPenawaran::find($id);
       	$kaji_ulang = KajiUlang::find($surat->kaji_id);
       	$record = PendaftaranPengujian::whereHas('kaji_ulang', function($u){
    										$u->whereHas('surat');
    									})->where('id', $kaji_ulang->pp_id)->get();
        $no_order = PendaftaranPengujian::whereHas('kaji_ulang', function($u){
    										$u->whereHas('surat');
    									})->where('id', $kaji_ulang->pp_id)->pluck('no_order');
       	$penerimaan = PenerimaanBarang::find($surat->va->konfirmasi->penerimaan->id);
       	if($penerimaan->pengujian){
	        $data = Pengujian::find($penerimaan->pengujian->id);
       	}else{
       		$data = [];
       	}
        $this->setSubtitle('No Order : '.$record->first()->no_order);
	    $this->setBreadcrumb(['Pengujian' => '#', 'Detil' => '#']);
       	return $this->render('modules.pengujian.tr.detail',[
       		'record' => $record->first(), 
       		'kaji_ulang' => $kaji_ulang,
        	'surat' => $surat,
        	'no_order' => $no_order,
        	'pengujian' => $data,
        	'penerimaan' => $penerimaan,
   		]); 
    }

    public function buatLaporan($arr)
    {
        return $this->render('modules.pengujian.tr.upload-final', [
            'data' => $arr,
        ]);
    }

    public function simpanLaporanData($arr, $tipe)
    {
        return $this->render('modules.pengujian.tr.laporan', [
            'data' => $arr,
            'tipe' => $tipe,
        ]);
    }

    public function verifikasi($id)
    {
    	DB::beginTransaction();
    	try {
    		$penerimaan_barang = PenerimaanBarang::find($id);
    		$pengujian 		   = Pengujian::find($penerimaan_barang->pengujian->id);


    		$penerimaan_barang->status_pengujian = 1;
    		$penerimaan_barang->save();

    		$pengujian->status = 2;
    		$pengujian->status_dispo = 2;
    		$pengujian->status_data = 1;
    		$pengujian->save();
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function verifikasiItemUji($id, $tipe)
    {
    	$user = auth()->user();
    	DB::beginTransaction();
    	try {
    		if($tipe == 1){
	    		$pengujian_detail = PengujianDetail::find($id);
	    		$pengujian_detail->status = 2;
	    		$pengujian_detail->save();

	    		$act = new ActPengujian;
	    		$act->parent_id 		= $id;
	    		$act->pengirim_id 		= $user->id;
	    		$act->penerima_id 		= 11;
	    		$act->tipe 				= 2;
	    		$act->jenis 			= 1;
	    		$act->save();
    		}elseif($tipe == 2){
    			$pengujian_detail = PengujianDetail::find($id);
	    		$pengujian_detail->status = 3;
	    		$pengujian_detail->save();

	    		$data_user = User::whereHas('roles', function($u){
		    						$u->where('name', 'msb-tegangan-rendah');
		    				   })->first();

	    		$act = new ActPengujian;
	    		$act->parent_id 		= $id;
	    		$act->pengirim_id 		= $user->id;
	    		$act->penerima_id 		= $data_user['id'];
	    		$act->tipe 				= 2;
	    		$act->jenis 			= 1;
	    		$act->keterangan 		= '<a>'.$user->nama.'</a> Verifikasi Item <a>'.$pengujian_detail->detail_penerimaan_barang->nama_barang.'</a> kepada <a>'.$data_user->nama.'</a> untuk dilakukan <a>Pengujian</a>';
	    		$act->save();
    		}elseif($tipe == 3){
    			$pengujian_detail = PengujianDetail::find($id);
	    		$pengujian_detail->status = 5;
	    		$pengujian_detail->save();

	    		$cari = ActDisposisi::where('parent_id', $pengujian_detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->id)->orderBy('created_at', 'desc')->first();
	    		$data_user = User::find($cari->pengirim_id);

	    		$act = new ActPengujian;
	    		$act->parent_id 		= $id;
	    		$act->pengirim_id 		= $user->id;
	    		$act->penerima_id 		= $cari->pengirim_id;
	    		$act->tipe 				= 2;
	    		$act->jenis 			= 2;
	    		$act->keterangan 		= '<a>'.$user->nama.'</a> Verifikasi Item <a>'.$pengujian_detail->detail_penerimaan_barang->nama_barang.'</a> dari <a>'.$data_user->nama.'</a> untuk dilakukan <a>Pengujian</a>';
	    		$act->save();

	    		$data_count = PengujianDetail::where('pengujian_id', $pengujian_detail->pengujian_id)
	    											->where('status', '<', 5)
	    											->count();
	    		if($data_count == 0){
	    			$pengujian = Pengujian::find($pengujian_detail->pengujian_id);
		    		$pengujian->status 		= 2;
		    		$pengujian->save();

		    		$penerimaan_barang = PenerimaanBarang::find($pengujian->penerimaan_id);
		    		$penerimaan_barang->status_pengujian = 1;
		    		$penerimaan_barang->save();

	    		}
    		}else{
    		}
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function simpanDispo($list, $tipe)
    {
    	$list = explode(',', $list);
    	$user = auth()->user();
    	DB::beginTransaction();
    	try {
    		if($tipe == 1){
	    		$pengujian_detail = PengujianDetail::find($id);
	    		$pengujian_detail->status = 2;
	    		$pengujian_detail->save();

	    		$act = new ActPengujian;
	    		$act->parent_id 		= $id;
	    		$act->pengirim_id 		= $user->id;
	    		$act->penerima_id 		= 9;
	    		$act->tipe 				= 2;
	    		$act->jenis 			= 1;
	    		$act->save();
    		}elseif($tipe == 2){
    			foreach ($list as $key => $vil) {
					$cari = PengujianDetail::find($vil);
					$group = PengujianDetail::where('group_id', $cari->group_id)->get();
					foreach ($group->where('flag', 0) as $key => $value) {
		    			$pengujian_detail = PengujianDetail::find($value->id);
			    		$pengujian_detail->status = 3;
			    		$pengujian_detail->save();

			    		$data_user = User::whereHas('roles', function($u){
				    						$u->where('name', 'msb-tegangan-rendah');
				    				   })->first();

			    		$act = new ActPengujian;
			    		$act->parent_id 		= $value->id;
			    		$act->pengirim_id 		= $user->id;
			    		$act->penerima_id 		= $data_user['id'];
			    		$act->tipe 				= 2;
			    		$act->jenis 			= 1;
			    		$act->keterangan 		= '<a>'.$user->nama.'</a> menyetujui hasil verifikasi No Seri : <a>'.$pengujian_detail->detail_penerimaan_barang->no_seri.'-'.$pengujian_detail->detail_penerimaan_barang->urutan.'</a>';
			    		$act->save();
					}
    			}
    		}elseif($tipe == 3){
    			foreach ($list as $key => $vil) {
    				$cari = PengujianDetail::find($vil);
					$group = PengujianDetail::where('group_id', $cari->group_id)->get();
					foreach ($group->where('flag', 0) as $key => $value) {
		    			$pengujian_detail = PengujianDetail::find($value->id);
			    		$pengujian_detail->status = 5;
			    		$pengujian_detail->save();

			    		$cari = ActDisposisi::where('parent_id', $pengujian_detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->id)->orderBy('created_at', 'desc')->first();
			    		$data_user = User::find($cari->pengirim_id);

			    		$act = new ActPengujian;
			    		$act->parent_id 		= $value->id;
			    		$act->pengirim_id 		= $user->id;
			    		$act->penerima_id 		= $cari->pengirim_id;
			    		$act->tipe 				= 2;
			    		$act->jenis 			= 2;
			    		$act->keterangan 		= '<a>'.$user->nama.'</a> menyetujui hasil verifikasi No Seri : <a>'.$pengujian_detail->detail_penerimaan_barang->no_seri.'-'.$pengujian_detail->detail_penerimaan_barang->urutan.'</a>';
			    		$act->save();

			    		$data_count = PengujianDetail::where('pengujian_id', $pengujian_detail->pengujian_id)
			    											->where('flag', 0)
			    											->where('status', '<', 5)
			    											->count();
			    		if($data_count == 0){
			    			$pengujian = Pengujian::find($pengujian_detail->pengujian_id);
				    		$pengujian->status 		= 2;
				    		$pengujian->save();

				    		$penerimaan_barang = PenerimaanBarang::find($pengujian->penerimaan_id);
				    		$penerimaan_barang->status_pengujian = 1;
				    		$penerimaan_barang->save();

			    		}
			    	}
		    	}
    		}else{
    		}
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function verifikasiLaporanPengujian($id, $tipe, $jenis)
    {
    	$user = auth()->user();
    	DB::beginTransaction();
    	try {
    		if($jenis == 1){
	    		if($tipe == 1){
	    			$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($id);
	    			$cari = PengujianDetailPelaksana::where('group_id', $pengujian_detail_pelaksana->group_id)->get();
	    			foreach ($cari as $key => $value) {
	    				$data = PengujianDetailPelaksana::find($value->id);
	    				$data->status = 1;
	    				$data->status_data = 2;
	    				$data->save();

						$data_user = User::whereHas('roles', function($u){
		    						$u->where('name', 'msb-tegangan-rendah');
		    				   })->first();

						$act = new ActPengujian;
						$act->parent_id 		= $pengujian_detail_pelaksana->detail->id;
						$act->pengirim_id 		= $user->id;
						$act->penerima_id 		= $data_user->id;
						$act->tipe 				= 2;
						$act->jenis 			= 2;
						$act->keterangan 		= '<a>'.$user->nama.'</a> Menyetujui Hasil Laporan Pengujian Item <a>'.$pengujian_detail_pelaksana->detail->detail_penerimaan_barang->nama_barang.'</a> dan diteruskan kepada <a>'.$data_user->nama.'</a>';
						$act->save();
	    			}
	    		}else{
	    			$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($id);
	    			$cari = PengujianDetailPelaksana::where('group_id', $pengujian_detail_pelaksana->group_id)->get();
	    			foreach ($cari as $key => $value) {
	    				$data = PengujianDetailPelaksana::find($value->id);
	    				$data->status = 1;
	    				$data->status_data = 0;
	    				$data->status_file = 0;
	    				$data->save();
	    				
			    		$data_user = User::find($pengujian_detail_pelaksana->pelaksana_laporan_id);

						$act = new ActPengujian;
						$act->parent_id 		= $pengujian_detail_pelaksana->detail->id;
						$act->pengirim_id 		= $user->id;
						$act->penerima_id 		= $pengujian_detail_pelaksana->pelaksana_laporan_id;
						$act->tipe 				= 1;
						$act->jenis 			= 2;
						$act->keterangan 		= '<a>'.$user->nama.'</a> Menolak Hasil Laporan Pengujian Item <a>'.$pengujian_detail_pelaksana->detail->detail_penerimaan_barang->nama_barang.'</a> dan diteruskan kepada <a>'.$data_user->nama.'</a> untuk di review ulang.';
						$act->save();
	    			}
	    		}
    		}elseif($jenis == 2){
    			if($tipe == 1){
    				$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($id);
	    			$cari = PengujianDetailPelaksana::where('group_id', $pengujian_detail_pelaksana->group_id)->get();
	    			foreach ($cari as $key => $value) {
			    		$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($value->id);
			    		$pengujian_detail_pelaksana->status = 1;
			    		$pengujian_detail_pelaksana->status_data = 3;
			    		$pengujian_detail_pelaksana->save();

			    		$pengujian_detail = PengujianDetail::find($pengujian_detail_pelaksana->pengujian_detail_id);
			    		$pengujian_detail->status = 6;
			    		$pengujian_detail->save();

			    		$data_user = User::whereHas('roles', function($u){
		    						$u->where('name', 'asman-dal-tegangan-rendah');
		    				   })->first();

						$act = new ActPengujian;
						$act->parent_id 		= $pengujian_detail_pelaksana->detail->id;
						$act->pengirim_id 		= $user->id;
						$act->penerima_id 		= $data_user->id;
						$act->tipe 				= 2;
						$act->jenis 			= 2;
						$act->keterangan 		= '<a>'.$user->nama.'</a> Menyetujui Hasil Laporan Pengujian Item <a>'.$pengujian_detail_pelaksana->detail->detail_penerimaan_barang->nama_barang.'</a>';
						$act->save();
	    			}
	    		}else{
	    			$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($id);
	    			$cari = PengujianDetailPelaksana::where('group_id', $pengujian_detail_pelaksana->group_id)->get();
	    			foreach ($cari as $key => $value) {
		    			$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($value->id);
			    		$pengujian_detail_pelaksana->status = 1;
			    		$pengujian_detail_pelaksana->status_data = 1;
			    		$pengujian_detail_pelaksana->status_file = 1;
			    		$pengujian_detail_pelaksana->save();

			    		$data_user = User::whereHas('roles', function($u){
		    						$u->where('name', 'msb-tegangan-rendah');
		    				   })->first();

						$act = new ActPengujian;
						$act->parent_id 		= $pengujian_detail_pelaksana->detail->id;
						$act->pengirim_id 		= $user->id;
						$act->penerima_id 		= $data_user->id;
						$act->tipe 				= 2;
						$act->jenis 			= 2;
						$act->keterangan 		= '<a>'.$user->nama.'</a> Menolak Hasil Laporan Pengujian Item <a>'.$pengujian_detail_pelaksana->detail->detail_penerimaan_barang->nama_barang.'</a> dan diteruskan kepada <a>'.$data_user->nama.'</a> untuk di review ulang';
						$act->save();
	    			}

	    		}
    		}else{
    			if($tipe == 1){
	    			$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($id);
	    			$cari = PengujianDetailPelaksana::where('group_id', $pengujian_detail_pelaksana->group_id)->get();
	    			foreach ($cari as $key => $value) {
	    				$data = PengujianDetailPelaksana::find($value->id);
	    				$data->status = 1;
	    				$data->status_data = 2;
	    				$data->save();

			    		$act = new ActPengujian;
			    		$act->parent_id 		= $value->id;
			    		$act->pengirim_id 		= $user->id;
			    		$act->penerima_id 		= 11;
			    		$act->tipe 				= 2;
			    		$act->jenis 			= 2;
			    		$act->save();
	    			}
	    		}else{
	    			$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($id);
	    			$cari = PengujianDetailPelaksana::where('group_id', $pengujian_detail_pelaksana->group_id)->get();
	    			foreach ($cari as $key => $value) {
	    				$data = PengujianDetailPelaksana::find($value->id);
	    				$data->status = 1;
	    				$data->status_data = 0;
	    				$data->status_file = 0;
	    				$data->save();
	    				
			    		$act = new ActPengujian;
			    		$act->parent_id 		= $value->id;
			    		$act->pengirim_id 		= $user->id;
			    		$act->penerima_id 		= 11;
			    		$act->tipe 				= 2;
			    		$act->jenis 			= 2;
			    		$act->save();
	    			}
	    		}
    		}
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function pengujianItemUji($id, $tipe)
    {
    	$user = auth()->user();
    	DB::beginTransaction();
    	try {
    		if($tipe == 1){
	    		$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($id);
	    		$cari = PengujianDetailPelaksana::where('group_id', $pengujian_detail_pelaksana->group_id)->get();
	    		foreach ($cari as $key => $value) {
	    			$data = PengujianDetailPelaksana::find($value->id);
		    		$data->status = 1;
		    		$data->status_data = 1;
		    		$data->save();

		    		$cari = ActDisposisi::where('parent_id', $pengujian_detail_pelaksana->detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->id)->orderBy('created_at', 'desc')->first();
		    		$data_user = User::find($cari->pengirim_id);

		    		$act = new ActPengujian;
		    		$act->parent_id 		= $pengujian_detail_pelaksana->detail->id;
		    		$act->pengirim_id 		= $user->id;
		    		$act->penerima_id 		= $cari->pengirim_id;
		    		$act->tipe 				= 2;
		    		$act->jenis 			= 2;
		    		$act->keterangan 		= '<a>'.$user->nama.'</a> menyelesaikan Item <a>'.$data->detail->detail_penerimaan_barang->nama_barang.'</a> dengan No FPP/FPK : <a>'.$data->detail->fpp_fpk.'</a> dan dilaporkan kepada <a>'.$data_user->nama.'</a> untuk dilakukan <a>Verifikasi Hasil Pengujian</a>';
		    		$act->save();
	    		}
    		}elseif($tipe == 9){
    			$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($id);
	    		$cari = PengujianDetailPelaksana::where('group_id', $pengujian_detail_pelaksana->group_id)->get();
	    		foreach ($cari as $key => $value) {
	    			$data = PengujianDetailPelaksana::find($value->id);
		    		$data->status_barang = 1;
		    		$data->save();

		    		$act = new ActPengujian;
		    		$act->parent_id 		= $pengujian_detail_pelaksana->detail->id;
		    		$act->pengirim_id 		= $user->id;
		    		$act->penerima_id 		= 1;
		    		$act->tipe 				= 2;
		    		$act->jenis 			= 2;
		    		$act->keterangan 		= '<a>'.$user->nama.'</a> memutuskan Item <a>'.$data->detail->detail_penerimaan_barang->nama_barang.'</a> dengan No FPP/FPK : <a>'.$data->detail->fpp_fpk.'</a>, dikembalikan setelah pengujian selesai dilaksanakan';
		    		$act->save();

	    		}
    		}elseif($tipe == 10){
    			$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($id);
	    		$cari = PengujianDetailPelaksana::where('group_id', $pengujian_detail_pelaksana->group_id)->get();
	    		foreach ($cari as $key => $value) {
	    			$data = PengujianDetailPelaksana::find($value->id);
		    		$data->status_file = 1;
		    		$data->status_data = 1;
		    		$data->save();

		    		$cari = ActDisposisi::where('parent_id', $pengujian_detail_pelaksana->detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->id)->orderBy('created_at', 'desc')->first();
		    		$data_user = User::find($cari->pengirim_id);

		    		$act = new ActPengujian;
		    		$act->parent_id 		= $pengujian_detail_pelaksana->detail->id;
		    		$act->pengirim_id 		= $user->id;
		    		$act->penerima_id 		= $cari->pengirim_id;
		    		$act->tipe 				= 2;
		    		$act->jenis 			= 2;
		    		$act->keterangan 		= '<a>'.$user->nama.'</a> menyelesaikan Item <a>'.$data->detail->detail_penerimaan_barang->nama_barang.'</a> dengan No FPP/FPK : <a>'.$data->detail->fpp_fpk.'</a> dan dilaporkan kepada <a>'.$data_user->nama.'</a> untuk dilakukan <a>Verifikasi Laporan Hasil Pengujian</a>';
		    		$act->save();
	    		}
    		}else{

    		}

    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function saveHasilPengujian(HasilRequest $request, $id)
    {
    	$user = auth()->user();
        DB::beginTransaction();
    	try {
    		foreach($request->detail as $key => $data){
    			$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($data['detail_id']);
	    		$cari = PengujianDetailPelaksana::where('group_id', $pengujian_detail_pelaksana->group_id)->get();
	    		foreach ($cari as $key => $value) {
	    			$cek = PengujianDetailPelaksana::find($value->id);
		    		$cek->status 				 = 1;
		    		$cek->status_data 			 = 1;
		    		$cek->status_hasil_pengujian = $data['status_hasil'];
		    		$cek->tgl_selesai 			 = Carbon::parse(date('Y-m-d'));
		    		$cek->save();

		    		if($data['status_hasil'] == 1){
		    			$hasil ='Berhasil';
		    		}else{
		    			$hasil ='Gagal';
		    		}

		    		$cik = ActDisposisi::where('parent_id', $pengujian_detail_pelaksana->detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->id)->orderBy('created_at', 'desc')->first();
		    		$data_user = User::find($cik->pengirim_id);

		    		$act = new ActPengujian;
		    		$act->parent_id 		= $pengujian_detail_pelaksana->detail->id;
		    		$act->pengirim_id 		= $user->id;
		    		$act->penerima_id 		= $cik->pengirim_id;
		    		$act->tipe 				= 2;
		    		$act->jenis 			= 2;
		    		$act->keterangan 		= '<a>'.$user->nama.'</a> menyelesaikan pengujian dengan Jenis Pengujian : <a>'.$cek->detail->detail_penerimaan_barang->detail_pp->jenis->nama.'</a>, No Seri : <a>'.$cek->detail->detail_penerimaan_barang->no_seri.'-'.$cek->detail->detail_penerimaan_barang->urutan.'</a> dengan hasil <a>'.$hasil.'</a>';
		    		$act->save();

                    $notif = User::find($cik->pengirim_id);
                    if(!is_null($notif->device_id)){
                         $this->onesignal('Pengujian Baru dengan nomor order '.$pengujian_detail_pelaksana->detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$notif->device_id,$pengujian_detail_pelaksana->detail->pengujian->penerimaan->toArray(),'pengujian','pengujian-baru');
                    }
	    		}
    		}
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function update(VerifikasiHasilPelaksanaRequest $request, $id)
    {
        DB::beginTransaction();
    	try {
    		$pengujian 					= Pengujian::find($id);
    		$pengujian->status_dispo 	= 4;
    		$pengujian->status_data 	= 2;
    		$pengujian->save();
		    $pengujian->updateDetailUji($request->detail);
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function savePelaksana(PelaksanaRequest $request, $id)
    {
        DB::beginTransaction();
    	try {
    		$user = auth()->user();
    		foreach($request->detail as $key => $data){
    			$detail_pengujian 						= PengujianDetail::find($data['detail_pengujian_id']);
    			$cari 									= PengujianDetail::where('group_id', $detail_pengujian->group_id)->get();
    			foreach ($cari->where('flag', 0) as $key => $value) {
    				$data_detail_pengujian 						= PengujianDetail::find($value->id);
	    			$data_detail_pengujian->fpp_fpk 			= $data['fpp_fpk'];
	    			$data_detail_pengujian->save();

	    			$detail_pelaksana 						= new PengujianDetailPelaksana;
	    			$detail_pelaksana->pengujian_detail_id 	= $value->id;
	    			$detail_pelaksana->pelaksana_id 		= $data['pelaksana_leader'];
	    			$detail_pelaksana->pelaksana_laporan_id	= 1;
	    			$detail_pelaksana->group_id				= $value->group_id;
	    			$detail_pelaksana->save();
	    			$detail_pelaksana->saveMember($data['pelaksana_id']);

    			}

    			$data_user = User::find($data['pelaksana_leader']);
    			$act = new ActPengujian;
				$act->parent_id 		= $detail_pengujian->id;
				$act->pengirim_id 		= $user->id;
				$act->penerima_id 		= $data['pelaksana_leader'];
				$act->tipe 				= 2;
				$act->jenis 			= 2;
				$act->keterangan 		= '<a>'.$user->nama.'</a> mendisposisikan No WBS/IO : <a>'.$detail_pengujian->pengujian->penerimaan->konfirmasi->wbs_io.'</a>, Jenis Pengujian : <a>'.$detail_pengujian->detail_penerimaan_barang->detail_pp->jenis->nama.'</a> kepada <a>'.$data_user->nama.'</a> dengan No FPP/FPK : <a>'.$data['fpp_fpk'].'</a>';
				$act->save();

                $notif = User::find($data['pelaksana_leader']);
                if(!is_null($notif->device_id)){
                     $this->onesignal('Pengujian Baru dengan nomor order '.$detail_pengujian->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$notif->device_id,$detail_pengujian->pengujian->penerimaan->toArray(),'pengujian','pengujian-baru');
                }
    		}

    		$penerimaan 				= PenerimaanBarang::find($id);
    		$pengujian 					= Pengujian::find($penerimaan->pengujian->id);
    		$pengujian->status_dispo 	= 4;
    		$pengujian->status_data 	= 1;
    		$pengujian->save();
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function saveLaporanPelaksana(LaporanPelaksanaRequest $request, $id)
    {
        DB::beginTransaction();
    	try {
    		$user = auth()->user();
    		foreach($request->detail as $key => $data){
    			$detail_pelaksana 						= PengujianDetailPelaksana::find($key);
    			$cari 						= PengujianDetailPelaksana::where('group_id', $detail_pelaksana->group_id)->get();
    			foreach ($cari as $key => $value) {
    				$data_detail_pelaksana 							= PengujianDetailPelaksana::find($value->id);
	    			$data_detail_pelaksana->pelaksana_laporan_id 	= $data['pelaksana'];
	    			$data_detail_pelaksana->save();

    			}
    			
    			$data_user = User::find($data['pelaksana']);
    			$act = new ActPengujian;
				$act->parent_id 		= $detail_pelaksana->detail->id;
				$act->pengirim_id 		= $user->id;
				$act->penerima_id 		= $data['pelaksana'];
				$act->tipe 				= 2;
				$act->jenis 			= 2;
				$act->keterangan 		= '<a>'.$user->nama.'</a> mendisposisikan No WBS/IO : <a>'.$detail_pelaksana->detail->pengujian->penerimaan->konfirmasi->wbs_io.'</a>, Jenis Pengujian : <a>'.$detail_pelaksana->detail->detail_penerimaan_barang->detail_pp->jenis->nama.'</a> kepada <a>'.$data_user->nama.'</a> untuk dibuatkan <a>Laporan Pengujian</a>';
				$act->save();

                $notif = User::find($data['pelaksana']);
                if(!is_null($notif->device_id)){
                     $this->onesignal('Pengujian Baru dengan nomor order '.$detail_pelaksana->detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$notif->device_id,$detail_pelaksana->detail->pengujian->penerimaan->toArray(),'pengujian','pengujian-baru');
                }
    		}
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function simpanPengujianItemUji($list, $tipe)
    {
    	$list = explode(',', $list);
    	$user = auth()->user();
    	DB::beginTransaction();
    	try {
    		if($tipe == 1){
    			foreach ($list as $key => $value) {
    				$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($value);
    				$cari = PengujianDetailPelaksana::where('group_id', $pengujian_detail_pelaksana->group_id)->get();
    				foreach ($cari as $key => $value) {
    					$data = PengujianDetailPelaksana::find($value->id);
    					$data->status_file = 1;
    					$data->status_data = 1;
    					$data->save();

    					$cari = ActDisposisi::where('parent_id', $pengujian_detail_pelaksana->detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->id)->where('tipe', 8)->orderBy('created_at', 'desc')->first();
    					$data_user = User::find($cari->pengirim_id);

    					$act = new ActPengujian;
    					$act->parent_id 		= $pengujian_detail_pelaksana->detail->id;
    					$act->pengirim_id 		= $user->id;
    					$act->penerima_id 		= $cari->pengirim_id;
    					$act->tipe 				= 2;
    					$act->jenis 			= 2;
    					$act->keterangan 		= '<a>'.$user->nama.'</a> menyelesaikan Item: <a>'.$data->detail->detail_penerimaan_barang->nama_barang.'</a> dengan No FPP/FPK : <a>'.$data->detail->fpp_fpk.'</a> dan dilaporkan kepada <a>'.$data_user->nama.'</a> untuk dilakukan <a>Verifikasi Laporan Hasil Pengujian</a>';
    					$act->save();

                         $notif = User::find($cari->pengirim_id);
                        if(!is_null($notif->device_id)){
                             $this->onesignal('Pengujian Baru dengan nomor order '.$pengujian_detail_pelaksana->detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$notif->device_id,$pengujian_detail_pelaksana->detail->pengujian->penerimaan->toArray(),'pengujian','pengujian-baru');
                        }
    				}
		    	}
    		}else{
    		}
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function simpanData(SimpanDataRequest $request, $id)
    {
    	$list = explode(',', $request->data);
    	$user = auth()->user();
    	DB::beginTransaction();
    	try {
    		if($request->tipe == 1){
    			if($request->pilihan == 1){
    				foreach ($list as $key => $val) {
		    			$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($val);
		    			$cari = PengujianDetailPelaksana::where('group_id', $pengujian_detail_pelaksana->group_id)->get();
		    			foreach ($cari as $key => $value) {
		    				$data = PengujianDetailPelaksana::find($value->id);
		    				$data->status = 1;
		    				$data->status_data = 2;
		    				$data->save();

							$data_user = User::whereHas('roles', function($u){
			    						$u->where('name', 'msb-tegangan-rendah');
			    				   })->first();

							$act = new ActPengujian;
							$act->parent_id 		= $pengujian_detail_pelaksana->detail->id;
							$act->pengirim_id 		= $user->id;
							$act->penerima_id 		= $data_user->id;
							$act->tipe 				= 2;
							$act->jenis 			= 2;
							$act->keterangan 		= '<a>'.$user->nama.'</a> menyetujui laporan pengujian No WBS/IO : <a>'.$data->detail->pengujian->penerimaan->konfirmasi->wbs_io.'</a>, Jenis Pengujian : <a>'.$data->detail->detail_penerimaan_barang->detail_pp->jenis->nama.'</a>, No Seri : <a>'.$data->detail->detail_penerimaan_barang->no_seri.'-'.$data->detail->detail_penerimaan_barang->urutan.'</a>';
							$act->save();

                            $notif = User::find($data_user->id);
                            if(!is_null($notif->device_id)){
                                 $this->onesignal('Pengujian Baru dengan nomor order '.$pengujian_detail_pelaksana->detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$notif->device_id,$pengujian_detail_pelaksana->detail->pengujian->penerimaan->toArray(),'pengujian','pengujian-baru');
                            }
		    			}
		    		}
	    		}else{
	    			foreach ($list as $key => $val) {
		    			$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($val);
		    			$cari = PengujianDetailPelaksana::where('group_id', $pengujian_detail_pelaksana->group_id)->get();
		    			foreach ($cari as $key => $value) {
		    				$data = PengujianDetailPelaksana::find($value->id);
		    				$data->status = 1;
		    				$data->status_data = 0;
		    				$data->status_file = 0;
		    				$data->save();
		    				
				    		$data_user = User::find($pengujian_detail_pelaksana->pelaksana_laporan_id);

							$act = new ActPengujian;
							$act->parent_id 		= $pengujian_detail_pelaksana->detail->id;
							$act->pengirim_id 		= $user->id;
							$act->penerima_id 		= $pengujian_detail_pelaksana->pelaksana_laporan_id;
							$act->tipe 				= 1;
							$act->jenis 			= 2;
							$act->keterangan 		= '<a>'.$user->nama.'</a> menolak laporan pengujian No WBS/IO : <a>'.$data->detail->pengujian->penerimaan->konfirmasi->wbs_io.'</a>, Jenis Pengujian : <a>'.$data->detail->detail_penerimaan_barang->detail_pp->jenis->nama.'</a>, No Seri : <a>'.$data->detail->detail_penerimaan_barang->no_seri.'-'.$data->detail->detail_penerimaan_barang->urutan.'</a>';
							$act->save();

                            $notif = User::find($pengujian_detail_pelaksana->pelaksana_laporan_id);
                            if(!is_null($notif->device_id)){
                                 $this->onesignal('Pengujian Baru dengan nomor order '.$pengujian_detail_pelaksana->detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$notif->device_id,$pengujian_detail_pelaksana->detail->pengujian->penerimaan->toArray(),'pengujian','pengujian-baru');
                            }
		    			}
		    		}
	    		}
    		}elseif($request->tipe == 2){
    			if($request->pilihan == 1){
    				foreach ($list as $key => $val) {
						$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($val);
						$cari = PengujianDetailPelaksana::where('group_id', $pengujian_detail_pelaksana->group_id)->get();
						foreach ($cari as $key => $value) {
							$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($value->id);
							$pengujian_detail_pelaksana->status = 1;
							$pengujian_detail_pelaksana->status_data = 3;
							$pengujian_detail_pelaksana->save();

							$pengujian_detail = PengujianDetail::find($pengujian_detail_pelaksana->pengujian_detail_id);
							$pengujian_detail->status = 6;
							$pengujian_detail->save();

							$data_user = User::whereHas('roles', function($u){
								$u->where('name', 'asman-dal-tegangan-rendah');
							})->first();

							$act = new ActPengujian;
							$act->parent_id 		= $pengujian_detail_pelaksana->detail->id;
							$act->pengirim_id 		= $user->id;
							$act->penerima_id 		= $data_user->id;
							$act->tipe 				= 2;
							$act->jenis 			= 2;
							$act->keterangan 		= '<a>'.$user->nama.'</a> menyetujui laporan pengujian No WBS/IO : <a>'.$pengujian_detail_pelaksana->detail->pengujian->penerimaan->konfirmasi->wbs_io.'</a>, Jenis Pengujian : <a>'.$pengujian_detail_pelaksana->detail->detail_penerimaan_barang->detail_pp->jenis->nama.'</a>, No Seri : <a>'.$pengujian_detail_pelaksana->detail->detail_penerimaan_barang->no_seri.'-'.$pengujian_detail_pelaksana->detail->detail_penerimaan_barang->urutan.'</a>';
							$act->save();

                            $notif = User::find($data_user->id);
                            if(!is_null($notif->device_id)){
                                 $this->onesignal('Pengujian Baru dengan nomor order '.$pengujian_detail_pelaksana->detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$notif->device_id,$pengujian_detail_pelaksana->detail->pengujian->penerimaan->toArray(),'pengujian','pengujian-baru');
                            }
						}
					}
	    		}else{
	    			foreach ($list as $key => $val) {
						$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($val);
						$cari = PengujianDetailPelaksana::where('group_id', $pengujian_detail_pelaksana->group_id)->get();
						foreach ($cari as $key => $value) {
							$pengujian_detail_pelaksana = PengujianDetailPelaksana::find($value->id);
							$pengujian_detail_pelaksana->status = 1;
							$pengujian_detail_pelaksana->status_data = 1;
							$pengujian_detail_pelaksana->status_file = 1;
							$pengujian_detail_pelaksana->save();

							$data_user = User::whereHas('roles', function($u){
								$u->where('name', 'msb-tegangan-rendah');
							})->first();

							$act = new ActPengujian;
							$act->parent_id 		= $pengujian_detail_pelaksana->detail->id;
							$act->pengirim_id 		= $user->id;
							$act->penerima_id 		= $data_user->id;
							$act->tipe 				= 2;
							$act->jenis 			= 2;
							$act->keterangan 		= '<a>'.$user->nama.'</a> menolak laporan pengujian No WBS/IO : <a>'.$pengujian_detail_pelaksana->detail->pengujian->penerimaan->konfirmasi->wbs_io.'</a>, Jenis Pengujian : <a>'.$pengujian_detail_pelaksana->detail->detail_penerimaan_barang->detail_pp->jenis->nama.'</a>, No Seri : <a>'.$pengujian_detail_pelaksana->detail->detail_penerimaan_barang->no_seri.'-'.$pengujian_detail_pelaksana->detail->detail_penerimaan_barang->urutan.'</a>';
							$act->save();

                            $notif = User::find($data_user->id);
                            if(!is_null($notif->device_id)){
                                 $this->onesignal('Pengujian Baru dengan nomor order '.$pengujian_detail_pelaksana->detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$notif->device_id,$pengujian_detail_pelaksana->detail->pengujian->penerimaan->toArray(),'pengujian','pengujian-baru');
                            }
						}
					}
	    		}
    		}else{
    		}
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function uploadLaporan(LaporanRequest $request, $id)
    {
    	$list = explode(',', $request->data);
        DB::beginTransaction();
    	try {
    		$pengujian_detail 	= PengujianDetail::find($list[0]);
    		$pengujian 			= Pengujian::find($pengujian_detail->pengujian_id);
    		$cek = $pengujian->penerimaan->konfirmasi->tipe;
    		
    		$laporan_pengujian = new LaporanPengujian;
    		$laporan_pengujian->pengujian_id = $pengujian_detail->pengujian_id;
    		$laporan_pengujian->no_laporan = $request->no_laporan;
    		$laporan_pengujian->tgl_laporan = $request->tgl_laporan;

    		if($cek == 1){
    			$laporan_pengujian->status 			= 1;
    			$laporan_pengujian->nota_dinas 		= 'SERAH-TERIMA';
    			$laporan_pengujian->tgl_nota_dinas 	= Carbon::parse(date('Y-m-d'));
    		}
    		$laporan_pengujian->save();
    		$laporan_pengujian->multipleFileUploadLaporan($request->bukti);
    		$laporan_pengujian->saveDetail($list);
    		
            if($pengujian_detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->jenis==0){
                $notif = User::whereHas('roles', function($role){
                            $role->where('name','yan-kalibrasi');
                        })->get();
            }
            if($pengujian_detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->jenis==1){
                $notif = User::whereHas('roles', function($role){
                            $role->where('name','yan-uji');
                        })->get();
            }
            foreach ($notif as $val) {
                if(!is_null($val->device_id)){
                     $this->onesignal('Pengiriman Laporan Baru dengan nomor order '.$pengujian_detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$val->device_id,$laporan_pengujian->toArray(),'pengiriman-laporan','onprogress');
                }
            }

            if($laporan_pengujian->pengujian->tipe==1){
                $notif = User::whereHas('roles', function($role){
                            $role->where('name','yan-kalibrasi')
                                ->orWhere('name','srm-promskal')
                                ->orWhere('name','msb-kalibrasi')
                                ->orWhere('name','asman-dal-kalibrasi')
                                ->orWhere('name','asman-lola-kalibrasi')
                                ->orWhere('name','adminlab-kalibrasi')
                                ->orWhere('name','pelaksana-kalibrasi')
                                ->orWhere('name','admin')
                                ->orWhere('name','gm');
                        })->get();
            }
            if($laporan_pengujian->pengujian->tipe==2){
                $notif = User::whereHas('roles', function($role){
                            $role->where('name','msb-siskit')
                                ->orWhere('name','asman-dal-siskit')
                                ->orWhere('name','asman-lola-siskit')
                                ->orWhere('name','adminlab-siskit')
                                ->orWhere('name','pelaksana-siskit')
                                ->orWhere('name','yan-uji')
                                ->orWhere('name','srm-uji')
                                ->orWhere('name','admin')
                                ->orWhere('name','gm');
                        })->get();
            }
            if($laporan_pengujian->pengujian->tipe==3){
                $notif = User::whereHas('roles', function($role){
                            $role->where('name','msb-sistgi')
                                ->orWhere('name','asman-dal-sistgi')
                                ->orWhere('name','asman-lola-sistgi')
                                ->orWhere('name','adminlab-sistgi')
                                ->orWhere('name','pelaksana-sistgi')
                                ->orWhere('name','yan-uji')
                                ->orWhere('name','srm-uji')
                                ->orWhere('name','admin')
                                ->orWhere('name','gm');
                        })->get();
            }
            if($laporan_pengujian->pengujian->tipe==4){
                $notif = User::whereHas('roles', function($role){
                            $role->where('name','msb-tegangan-rendah')
                                ->orWhere('name','asman-dal-tegangan-rendah')
                                ->orWhere('name','asman-lola-tegangan-rendah')
                                ->orWhere('name','adminlab-tegangan-rendah')
                                ->orWhere('name','pelaksana-tegangan-rendah')
                                ->orWhere('name','yan-uji')
                                ->orWhere('name','srm-uji')
                                ->orWhere('name','admin')
                                ->orWhere('name','gm');
                        })->get();
            }
            if($laporan_pengujian->pengujian->tipe==5){
                $notif = User::whereHas('roles', function($role){
                            $role->where('name','msb-tegangan-tinggi')
                                ->orWhere('name','asman-dal-tegangan-tinggi')
                                ->orWhere('name','asman-lola-tegangan-tinggi')
                                ->orWhere('name','adminlab-tegangan-tinggi')
                                ->orWhere('name','pelaksana-tegangan-tinggi')
                                ->orWhere('name','yan-uji')
                                ->orWhere('name','srm-uji')
                                ->orWhere('name','admin')
                                ->orWhere('name','gm');
                        })->get();
            }

            foreach ($notif as $val) {
                if(!is_null($val->device_id)){
                     $this->onesignal('Laporan Pengujian Baru dengan nomor order '.$pengujian_detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$val->device_id,$laporan_pengujian->toArray(),'laporan-pengujian','laporan-pengujian');
                }
            }
    		
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function destroy($id)
    {
        return response([
            'status' => true,
        ]);
    }

    public function preview($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function previewMultiple($id, $type)
    {
    	if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $c->filename,
                        ];
                    }
                }
                return $this->render('pdf-multiple', [
		        	'mockup' => true,
		        	'data' => $rename,
		        ]);
            }
        }
    }

    public function download($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

    public function printBarcodePdf(Request $request, $id){
    	$record = LaporanPengujian::find($id);
        $data = [
        	'records' => $record,
        ];
	    $pdf = PDF::loadView('modules.pengujian.tr.cetak.cetak-barcode', $data);
        return $pdf->stream('Surat Penerimaan Barang '.$record->tanggal.'.pdf',array("Attachment" => false));
 	}
}
