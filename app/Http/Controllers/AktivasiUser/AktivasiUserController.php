<?php

namespace App\Http\Controllers\AktivasiUser;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\AktivasiUser\AktivasiUserRequest;
use App\Http\Requests\AktivasiUser\UserRequest;
use App\Http\Requests\AktivasiUser\AktivasiUlangUserRequest;
use App\Http\Requests\AktivasiUser\DaftarUserRequest;

/* Models */
use App\Models\Master\Pelanggan;
use App\Models\Authentication\User;
use App\Models\Master\Perusahaan;
use App\Models\Authentication\Role;
use App\Models\Files;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Mail;
use DB;
use Storage;
use Zipper;

class AktivasiUserController extends Controller
{
    protected $link = 'aktivasiuser/aktivasi-user/';
    protected $perms = 'aktivasi-user';
    protected $listStructs = [];

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Aktivasi Peminta Jasa");
        $this->setPerms($this->perms);
        $this->setModalSize("tiny");
        $this->setBreadcrumb(['Aktivasi Peminta Jasa' => '#', 'Aktivasi Peminta Jasa' => '#']);

        // Header Grid Datatable
        $this->listStructs = [
            'aktivasi' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'nama_lengkap',
                    'name' => 'nama_lengkap',
                    'label' => 'PIC',
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '200px',
                ],
                [
                    'data' => 'email',
                    'name' => 'email',
                    'label' => 'Email',
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '120px',
                ],
                [
                    'data' => 'no_hp',
                    'name' => 'no_hp',
                    'label' => 'No Hp',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '120px',
                ],
                [
                    'data' => 'perusahaan.nama',
                    'name' => 'perusahaan.nama',
                    'label' => 'Nama Perusahaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '120px',
                ],
                [
                    'data' => 'perusahaan.alamat',
                    'name' => 'perusahaan.alamat',
                    'label' => 'Alamat Perusahaan',
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '200px',
                ],
                [
                    'data' => 'no_tlp',
                    'name' => 'no_tlp',
                    'label' => 'No. Telpon Perusahaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '120px',
                ],
                [
                    'data' => 'pelanggan',
                    'name' => 'pelanggan',
                    'label' => 'Kategori',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '120px',
                ],
                [
                    'data' => 'pesan',
                    'name' => 'pesan',
                    'label' => 'Pesan',
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '200px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '120px',
                ],
                [
                    'data' => 'created_by',
                    'name' => 'created_by',
                    'label' => 'Dibuat Oleh',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '120px',
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '140px',
                ]
            ],
            'histori' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'nama_lengkap',
                    'name' => 'nama_lengkap',
                    'label' => 'PIC',
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '200px',
                ],
                [
                    'data' => 'email',
                    'name' => 'email',
                    'label' => 'Email',
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '120px',
                ],
                [
                    'data' => 'no_hp',
                    'name' => 'no_hp',
                    'label' => 'No Hp',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '120px',
                ],
                [
                    'data' => 'perusahaan.nama',
                    'name' => 'perusahaan.nama',
                    'label' => 'Nama Perusahaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '120px',
                ],
                [
                    'data' => 'perusahaan.alamat',
                    'name' => 'perusahaan.alamat',
                    'label' => 'Alamat Perusahaan',
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '200px',
                ],
                [
                    'data' => 'no_tlp',
                    'name' => 'no_tlp',
                    'label' => 'No. Telpon Perusahaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '120px',
                ],
                [
                    'data' => 'pelanggan',
                    'name' => 'pelanggan',
                    'label' => 'Kategori',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '120px',
                ],
                [
                    'data' => 'pesan',
                    'name' => 'pesan',
                    'label' => 'Pesan',
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '200px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '120px',
                ],
                [
                    'data' => 'surat_penugasan',
                    'name' => 'surat_penugasan',
                    'label' => 'Surat Penugasan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '120px',
                ],
                [
                    'data' => 'created_by',
                    'name' => 'created_by',
                    'label' => 'Dibuat Oleh',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '120px',
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '140px',
                ]
            ]
        ];
    }

    public function grid(Request $request)
    {
        $records = Pelanggan::with('perusahaan')->where('status', 1)->select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        if ($nama = $request->nama) {
            $records->where('nama_lengkap', 'like', '%'.$nama.'%');
        }

        if ($email = $request->email) {
            $records->where('email', 'like', '%'.$email.'%');
        }

        if ($nama_perusahaan = $request->nama_perusahaan) {
            $records->whereHas('perusahaan', function($q) use ($nama_perusahaan){
                return $q->where('nama', 'like', '%'.$nama_perusahaan.'%');
            });
        }
        $records = $records->get();

        
        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('pesan', function ($record) use ($request) {
            	$pesan ='-';
            	if($record->pesan){
            		$pesan = readMoreText(strip_tags($record->pesan), 80);
            	}
                return $pesan;
            })
            ->addColumn('perusahaan.alamat', function ($record) use ($request) {
            	$pesan ='-';
            	if($record->perusahaan->alamat){
            		$pesan = readMoreText(strip_tags($record->perusahaan->alamat), 80);
            	}
                return $pesan;
            })
            ->addColumn('pelanggan', function ($record) use ($request) {
                $string = '';
                if ($record->tipe_customer == 0) {
                    $string = 'PLN';
                }else if ($record->tipe_customer == 1){
                    $string = 'Non PLN';
                }else{
                    $string = 'A-PLN';
                }

                return $string;
            })
            ->editColumn('status', function ($record) {
                $string = '';
                if ($record->status == 0) {
                    $string = '<a class="ui green tag label">Di Setujui</a>';
                }else if ($record->status == 1){
                    $string = '<a class="ui yellow tag label">Menunggu</a>';
                }else{
                    $string = '<a class="ui red tag label">Di Tolak</a>';
                }

                return $string;
            })
            ->addColumn('created_by', function ($record) {
            	return $record->creatorName();
            })
            ->addColumn('action', function ($record) use ($link){
                // $disable = '';
                // if ($record->status == 0 || $record->status == 2) {
                //     $disable = 'disabled';
                // }

                $btn = '';
            
                $btn .= $this->makeButton([
                    'tooltip'  => 'Aktivasi',
                    'type' => 'edit',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'tooltip'  => 'Tolak',
                    'class'  => 'red icon reject',
                    'label'  => '<i class="close icon"></i>',
                    'type' => 'delete',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['status','action','pesan','perusahaan.alamat'])
            ->make(true);
    }

    public function grid2(Request $request)
    {
        $records = Pelanggan::with('perusahaan')->where('status', '!=',1)->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        if ($nama = $request->nama) {
            $records->where('nama_lengkap', 'like', '%'.$nama.'%');
        }

        if ($email = $request->email) {
            $records->where('email', 'like', '%'.$email.'%');
        }

        if ($nama_perusahaan = $request->nama_perusahaan) {
            $records->whereHas('perusahaan', function($q) use ($nama_perusahaan){
                return $q->where('nama', 'like', '%'.$nama_perusahaan.'%');
            });
        }
        $records = $records->get();
        

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('pesan', function ($record) use ($request) {
                $pesan ='-';
                if($record->pesan){
                    $pesan = readMoreText(strip_tags($record->pesan), 80);
                }
                return $pesan;
            })
            ->addColumn('perusahaan.alamat', function ($record) use ($request) {
                $pesan ='-';
                if($record->perusahaan->alamat){
                    $pesan = readMoreText(strip_tags($record->perusahaan->alamat), 80);
                }
                return $pesan;
            })
            ->addColumn('pelanggan', function ($record) use ($request) {
                $string = '';
                if ($record->tipe_customer == 0) {
                    $string = 'PLN';
                }else if ($record->tipe_customer == 1){
                    $string = 'Non PLN';
                }else{
                    $string = 'A-PLN';
                }

                return $string;
            })
            ->editColumn('status', function ($record) {
                $string = '';
                if ($record->status == 0) {
                    $string = '<a class="ui green tag label">Aktif</a>';
                }else{
                   $string = '<a class="ui red tag label">Non Aktif</a>';
                }

                return $string;
            })
            ->editColumn('surat_penugasan', function ($record) {
                $btn = '';
                $check = Files::where('target_id', $record->id)->where('target_type', 'aktivasi-user')->get();
                if(count($check) > 0){
	                $btn .= $this->makeButton([
			                    'tooltip'  => 'Preview Surat Penugasan',
			                    'class'  => 'teal icon lihat_data',
			                    'label'  => '<i class="file image outline icon"></i>',
			                    'type' => 'modal',
			                    'id'   => $record->id
			                ]);
	                $btn .= $this->makeButton([
	                            'type' => 'url',
	                            'tooltip' => 'Download Surat Penugasan',
	                            'class' => 'ui orange mini icon button',
	                            'label' => '<i class="download icon"></i>',
	                            'url' => url($this->link.$record->id.'/download')
	                        ]);

                }else{
                	$btn .= '-';
                }

                return $btn;
            })
            ->addColumn('created_by', function ($record) {
            	return $record->creatorName();
            })
            ->addColumn('action', function ($record) use ($link){
                // $disable = '';
                // if ($record->status == 0 || $record->status == 2) {
                //     $disable = 'disabled';
                // }

                $btn = '';
            
                $btn .= $this->makeButton([
                    'tooltip'  => 'Aktivasi Ulang',
                    'class'  => 'teal icon aktivasi-ulang',
                    'label'  => '<i class="refresh icon"></i>',
                    'type' => 'edit',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'tooltip'  => 'Non Aktif',
                    'class'  => 'red icon deactive',
                    'label'  => '<i class="shutdown icon"></i>',
                    'type' => 'delete',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['status','action','pesan','perusahaan.alamat','surat_penugasan'])
            ->make(true);
    }

    public function index()
    {
        $data = [
            'mockup' => true,
            'aktivasi' => Pelanggan::where('status', 1)->count(),
            'structs' => $this->listStructs,
        ];
        return $this->render('modules.aktivasi-user.index', $data);
    }

    public function create()
    {
        return $this->render('modules.aktivasi-user.create');
    }

    public function edit($id)
    {
        $record = Pelanggan::find($id);

        return $this->render('modules.aktivasi-user.edit', [
            'record' => $record
        ]);
    }

    public function store(DaftarUserRequest $request)
    {
    	$perusahaan = Perusahaan::find($request->perusahaan_id);
    	$password = generateRandomString();
        $role = Role::where('name', 'user')->first();

        DB::beginTransaction();
        try{
            $users 				= new User;
            $users->username 	= $request->email;
            $users->email    	= $request->email;
            $users->last_login  = Carbon::now();
    		$users->password 	= bcrypt($password);
        	$users->status 		= 1;
        	$users->nama 		= $request->nama;
            $users->save();
            $users->roles()->sync($role->id);

            $record = new Pelanggan;
            $record->user_id            	= $users->id;
            $record->perusahaan_id      	= $perusahaan->id;
            $record->tipe_customer      	= $perusahaan->kategori;
            $record->nama_lengkap       	= $request->nama;
            $record->no_hp              	= $request->no_hp;
            $record->email              	= $request->email;
            $record->no_tlp             	= $perusahaan->no_tlp;
            $record->alamat             	= $request->alamat;
            $record->nip             		= $request->nip;
            $record->status             	= 0;
            $record->save();

            $data=[
            	'username'      	=> $request->email,
            	'password'      	=> $password,
            	'email'         	=> $request->email,
            	'nama'          	=> $request->nama,
            	'no_hp'         	=> $request->no_hp,
            	'nama_perusahaan'   => $perusahaan->nama,
            	'email_perusahaan'  => $perusahaan->email,
            	'no_tlp'  			=> $perusahaan->no_tlp,
            	'alamat'  			=> $request->alamat,
            	'nip'  				=> $request->nip,
            ];

            DB::commit();
            sendEmail($data);
            return response([
                'status' => true
            ]);

        }catch(\Exception $e){
            DB::rollback();
            return response([
                'status' => true,
                'errors' => $e 
            ]);
        }
    }

    public function update(AktivasiUserRequest $request, $id)
    {
        $pln = false;
        if($request->jenis_customer==0){
            $pecah = explode('@',$request->email);
            if(strtolower($pecah[1])=='pln.co.id'){
                $pln = true;
            }
        }else{
            $pln = true;
        }

        if($pln==false){
            return response([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'email' => ['Email Harus Menggunakan Email Korporat, @pln.co.id']
                ]
            ],422);
        }else{
            $password = generateRandomString();
            DB::beginTransaction();

            try{
                $users = User::find($request->user_id);
                $users->email = $request->email;
                $users->password = bcrypt($password);
                $users->status = 1;
                if ($users->save()) {
                    $pelanggan = Pelanggan::where('user_id', $users->id)->first();
                    $pelanggan->status = 0;
                    $pelanggan->email = $request->email;
                    $pelanggan->no_hp = $request->no_hp;
                    $pelanggan->keterangan = $request->catatan;
                    $pelanggan->save();
                    $pelanggan->multipleFileUpload($request->file);

                    $perusahaan = Perusahaan::find($pelanggan->perusahaan_id);
                    $perusahaan->status = 1;
                    $perusahaan->save();
                }
                
                $data=[
                	'username'      	=> $users->email,
                    'password'      	=> $password,
                    'email'         	=> $users->email,
                	'nama'          	=> $pelanggan->nama_lengkap,
                	'no_hp'         	=> $pelanggan->no_hp,
                	'nama_perusahaan'   => $perusahaan->nama,
                	'email_perusahaan'  => $perusahaan->email,
                	'no_tlp'  			=> $perusahaan->no_tlp,
                	'alamat'  			=> $perusahaan->alamat,
                	'nip'  				=> $pelanggan->nip,
                ];

                DB::commit();
                sendEmail($data);
            }catch(\Exception $e){
                return $e;
                DB::rollback();
            }

            return response([
                'status' => true
            ]);
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $pelanggan = Pelanggan::find($id);
            $pelanggan->status = 2; // 2 = flag tolak
            $pelanggan->save();

            $users = User::find($pelanggan->user_id);
            $users->status = 1; // non aktif
            $users->save();

            DB::commit();
            return response([
              'status' => true
            ]);
        }catch (\Exception $e) {
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function ulang($id)
    {
        $record = Pelanggan::find($id);

        return $this->render('modules.aktivasi-user.edit-aktivasi-ulang', [
            'record' => $record
        ]);
    }

    public function aktivasiUlang(AktivasiUlangUserRequest $request, $id)
    {
        // dd($id);

        $pln = false;
        if($request->jenis_customer==0){
            $pecah = explode('@',$request->email);
            if(strtolower($pecah[1])=='pln.co.id'){
                $pln = true;
            }
        }else{
            $pln = true;
        }

        if($pln==false){
            return response([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'email' => ['Email Harus Menggunakan Email Korporat, @pln.co.id']
                ]
            ],422);
        }else{
            $password = generateRandomString();
            DB::beginTransaction();

            try{
                $users = User::find($request->user_id);
                $users->password = bcrypt($password);
                $users->email = $request->email;
                $users->username = $request->email;
                $users->status = 1;
                if ($users->save()) {
                    $pelanggan = Pelanggan::where('user_id', $users->id)->first();
                    $pelanggan->status = 0;
                    $pelanggan->email = $request->email;
                    $pelanggan->save();

                    $perusahaan = Perusahaan::find($pelanggan->perusahaan_id);
                    $perusahaan->status = 1;
                    $perusahaan->save();
                }
                
                $data=[
                    'username'          => $request->email,
                    'password'          => $password,
                    'email'             => $request->email,
                    'nama'              => $pelanggan->nama_lengkap,
                    'no_hp'             => $pelanggan->no_hp,
                    'nama_perusahaan'   => $perusahaan->nama,
                    'email_perusahaan'  => $perusahaan->email,
                    'no_tlp'            => $perusahaan->no_tlp,
                    'alamat'            => $perusahaan->alamat,
                    'nip'               => $pelanggan->nip,
                ];
                // dd($data);

                DB::commit();
                sendEmail($data);
            }catch(\Exception $e){
                return $e;
                DB::rollback();
            }
            
            return response([
                'status' => true
            ]);
        }
    }

    public function cari($id)
    {
    	$perusahaan = Perusahaan::find($id);
    	return $perusahaan;
    }

    public function perusahaan($id)
    {
        $perusahaan = Perusahaan::where('kategori',$id)->get();

        $return = '<option value="">Pilih Salah Satu</option>';

        foreach($perusahaan as $p)
        {
          $return .= '<option value="'.$p->id.'"> '. $p->nama .' </option>';
        }

        return $return;
    }
    public function download($id)
    {
        $check = Files::where('target_id', $id)->where('target_type', 'aktivasi-user')->get();
        $files = [];
        if($check->count() > 0)
        {
            $rename = [];
            foreach($check as $c)
            {
                if(file_exists(public_path('storage/'.$c->url)))
                {
                    $newname = '';
                    $splitname = explode("/",$c->url);
                    for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                        $newname .= $splitname[$i].'/';
                    }
                    Storage::disk('public')->move($c->url, $newname.$c->filename);
                    $files[] = public_path('storage/'.$newname.$c->filename);
                    $rename[] = [
                        'old' => $c->url,
                        'new' => $newname.$c->filename,
                    ];
                }
            }
            $now = Carbon::now()->format('Ymdhis');
            Zipper::make(public_path('storage/'.'aktivasi-user'.$now.'.zip'))->add($files)->close();
            
            for ($i=0; $i < count($rename) ; $i++) { 
                Storage::disk('public')->move($rename[$i]['new'], $rename[$i]['old']);
            }

            if(file_exists(public_path('storage/'.'aktivasi-user'.$now.'.zip')))
            {
                return response()->download(public_path('storage/'.'aktivasi-user'.$now.'.zip'));
            }
            return response()->view('errors.download');
        }

        return response()->view('errors.download');
    }

    public function lihat($id)
    {
        $check = Files::where('target_id', $id)->where('target_type', 'aktivasi-user')->get();
        $files = [];
        if($check->count() > 0)
        {
            $rename = [];
            foreach($check as $c)
            {
                if(file_exists(public_path('storage/'.$c->url)))
                {
                    $newname = '';
                    $splitname = explode("/",$c->url);
                    for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                        $newname .= $splitname[$i].'/';
                    }
                    $files[] = public_path('storage/'.$newname.$c->filename);
                    $rename[] = [
                        'old' => $c->url,
                        'new' => $c->filename,
                    ];
                }
            }
            return $this->render('pdf-multiple', [
	        	'mockup' => true,
	        	'data' => $rename,
	        ]);
        }
    }
}
