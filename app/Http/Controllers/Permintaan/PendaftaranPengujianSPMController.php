<?php

namespace App\Http\Controllers\Permintaan;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Permintaan\PermintaanPengujianRequest;
use App\Http\Requests\FrontEnd\CekItemUjiRequest;

/* Models */
use App\Models\Authentication\Role;
use App\Models\Authentication\User;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\FrontEnd\PendaftaranPengujianDetail;
use App\Models\Master\JenisPelayanan;
use App\Models\Master\JenisPengujian;
use App\Models\Master\Satuan;
use App\Models\Master\Lingkup;
use App\Models\Picture;
use App\Models\Files;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Excel;

class PendaftaranPengujianSPMController extends Controller
{
    protected $link = 'permintaan/spm/';
    protected $perms = 'permintaan-spm';
    protected $listStructs = [];

    function __construct()
    {
        if (Auth::check()){
            if(auth()->users()){
                $this->middleware('auth');
            }
        }
        $this->setLink($this->link);
        $this->setTitle("Permintaan Berjalan");
        $this->setPerms($this->perms);
        $this->setModalSize("tiny");
        $this->setBreadcrumb(['Permintaan Berjalan' => '#']);

        // Header Grid Datatable
        $this->listStructs = [
            'listStruct'=>[
              	[
	                'data' => 'num',
	                'name' => 'num',
	                'label' => '#',
	                'orderable' => false,
	                'searchable' => false,
	                'className' => "center aligned",
	                'width' => '40px',
            	],
	            [
	                'data' => 'no_order',
	                'name' => 'no_order',
	                'label' => 'No Order',
	                'className' => "center aligned",
	                'width' => '150px',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'tgl_order',
	                'name' => 'tgl_order',
	                'label' => 'Tgl Order',
	                'className' => "center aligned",
	                'width' => '150px',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'no_surat',
	                'name' => 'no_surat',
	                'label' => 'No Surat Permintaan',
	                'className' => "center aligned",
	                'width' => '200px',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'pemesan',
	                'name' => 'pemesan',
	                'label' => 'Peminta Jasa',
	                'className' => "center aligned",
	                'width' => '200px',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'layanan',
	                'name' => 'layanan',
	                'label' => 'Layanan',
	                'className' => "center aligned",
	                'width' => '200px',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'lingkup',
	                'name' => 'lingkup',
	                'label' => 'Lingkup',
	                'className' => "center aligned",
	                'width' => '200px',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'status',
	                'name' => 'status',
	                'label' => 'Status',
	                'className' => "center aligned",
	                'width' => '150px',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'created_at',
	                'name' => 'created_at',
	                'label' => 'Dibuat Pada',
	                'className' => "center aligned",
	                'width' => '120px',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'action',
	                'name' => 'action',
	                'label' => 'Aksi',
	                'className' => "center aligned",
	                'width' => '100px',
	                'searchable' => false,
	                'sortable' => false,
	            ]  
            ],
            'listStruct2' => [
                [
	                'data' => 'num',
	                'name' => 'num',
	                'label' => '#',
	                'className' => "center aligned",
	                'width' => '40px',
	                'orderable' => false,
	                'searchable' => false,
            	],
	            [
	                'data' => 'no_order',
	                'name' => 'no_order',
	                'label' => 'No Order',
	                'className' => "center aligned",
	                'width' => '150px',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'tgl_order',
	                'name' => 'tgl_order',
	                'label' => 'Tgl Order',
	                'className' => "center aligned",
	                'width' => '150px',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'no_surat',
	                'name' => 'no_surat',
	                'label' => 'No Surat Permintaan',
	                'className' => "center aligned",
	                'width' => '200px',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'pemesan',
	                'name' => 'pemesan',
	                'label' => 'Peminta Jasa',
	                'className' => "center aligned",
	                'width' => '200px',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'layanan',
	                'name' => 'layanan',
	                'label' => 'Layanan',
	                'className' => "center aligned",
	                'width' => '200px',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'lingkup',
	                'name' => 'lingkup',
	                'label' => 'Lingkup',
	                'className' => "center aligned",
	                'width' => '200px',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'briefing_teknis',
	                'name' => 'briefing_teknis',
	                'label' => 'Briefing Teknis',
	                'className' => "center aligned",
	                'width' => '120px',
	                'searchable' => false,
	                'sortable' => false,
            	],
	            [
	                'data' => 'status',
	                'name' => 'status',
	                'label' => 'Status',
	                'className' => "center aligned",
	                'width' => '150px',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'created_at',
	                'name' => 'created_at',
	                'label' => 'Dibuat Pada',
	                'className' => "center aligned",
	                'width' => '120px',
	                'searchable' => false,
	                'sortable' => true,
	            ],
	            [
	                'data' => 'action',
	                'name' => 'action',
	                'label' => 'Aksi',
	                'className' => "center aligned",
	                'width' => '100px',
	                'searchable' => false,
	                'sortable' => false,
	            ]
            ],
        ];

    }

    public function grid(Request $request)
    {
    	$user = auth()->user();
    	if($user->hasRole('user') || $user->hasRole('keuangan') || $user->hasRole('admin')){
    		$records = PendaftaranPengujian::where('id', 0);
    	}else{
	        $records = PendaftaranPengujian::with('pelayanan','lingkup')->where('created_by', auth()->user()->id)->where('tipe_surat', 0)->whereIn('status', [1,2,3,4,10])->select('*');
    	}
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at','desc');
        }

        if ($no_order = $request->no_order) {
            $records->where('no_order', 'like','%'.$no_order.'%');
        }
        if($no_surat = $request->no_surat){
		    $records->where('no_surat','like','%'.$no_surat.'%');
		}
		if($tanggal_order = $request->tanggal_order){
		    $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
		}
		if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
		    $records->where('layanan_id',$jenis_pelayanan_id);
		}
		if ($perusahaan_id = $request->perusahaan_id) {
		    $records->whereHas('user', function($user) use ($perusahaan_id){
		        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
		              $pelanggan->where('perusahaan_id',$perusahaan_id);
		        });
		    });
		}


        return DataTables::of($records->get())
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('pemesan', function ($record) use ($request) {
        	if($record->tipe == 2){
        		if($record->status_ams == 0){
        			return '-';
        		}else{
        			$pemesan = $record->user->pelanggans->perusahaan->nama.'</br>'.$record->user->pelanggans->nama_lengkap.'</br>'.$record->user->pelanggans->no_hp;
	            	return $pemesan;
        		}
        	}else{
	            $pemesan = $record->user->pelanggans->perusahaan->nama.'</br>'.$record->user->pelanggans->nama_lengkap.'</br>'.$record->user->pelanggans->no_hp;
	            	return $pemesan;
        	}
        })
        ->addColumn('tgl_order', function ($record) use ($request) {
            return DateToStringYear($record->tgl_order);
        })
        ->addColumn('layanan', function ($record) use ($request) {
            return $record->pelayanan->nama;
        })
        ->addColumn('lingkup', function ($record) use ($request) {
            return $record->lingkup->nama;
        })
        ->addColumn('total', function ($record) use ($request) {
            return 0;
        })
        ->addColumn('status', function ($record) use ($request) {
        	if($record->status == 1){
        		return '<label class="ui grey tag label">'.statusa($record->status).'</label>';
        	}elseif($record->status == 2){
        		return '<label class="ui olive tag label">'.statusa($record->status).'</label>';
        	}elseif($record->status == 3){
	            return '<a href="'.url($this->link.$record->id.'/konfirmasi').'" class="ui yellow tag label"">'.status($record->status).'</a>';
        	}elseif($record->status == 4){
        		return '<label class="ui blue tag label">'.statusa($record->status).'</label>';
        	}elseif($record->status == 5){
        		return '<label class="ui teal tag label">'.statusa($record->status).'</label>';
        	}elseif($record->status == 6){
        		return '<label class="ui violet tag label">'.statusa($record->status).'</label>';
        	}elseif($record->status == 7){
        		return '<label class="ui green tag label">'.statusa($record->status).'</label>';
        	}elseif($record->status == 8){
        		return '<label class="ui orange tag label">'.statusa($record->status).'</label>';
        	}elseif($record->status == 9){
        		return '<label class="ui red tag label">'.statusa($record->status).'</label>';
        	}else{
        		return '-';
        	}
        })
        ->editColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->addColumn('action', function ($record) {
            $btn = '';

        	$btn = $this->makeButton([
            	'type' => 'url',
            	'class' => 'ui blue mini icon button',
            	'tooltip' => 'Lihat',
            	'label' => '<i class="eye icon"></i>',
            	'url' => url($this->link.$record->id.'/detil')
            ]);

            return $btn;
        })
        ->rawColumns(['status','pemesan','action'])
        ->make(true);
    }

    public function histori(Request $request)
    {
    	$user = auth()->user();
    	if($user->hasRole('user') || $user->hasRole('keuangan') || $user->hasRole('admin')){
    		$records = PendaftaranPengujian::where('id', 0);
    	}else{
	        $records = PendaftaranPengujian::whereIn('status', [5,6,7])
        								->where('created_by', auth()->user()->id)->where('tipe_surat', 0)->select('*');
    	}
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at','desc');
        }

        if ($no_order = $request->no_order) {
            $records->where('no_order', 'like','%'.$no_order.'%');
        }
        if($no_surat = $request->no_surat){
		    $records->where('no_surat','like','%'.$no_surat.'%');
		}
		if($tanggal_order = $request->tanggal_order){
		    $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
		}
		if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
		    $records->where('layanan_id',$jenis_pelayanan_id);
		}
		if ($perusahaan_id = $request->perusahaan_id) {
		    $records->whereHas('user', function($user) use ($perusahaan_id){
		        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
		              $pelanggan->where('perusahaan_id',$perusahaan_id);
		        });
		    });
		}


        return DataTables::of($records->get())
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('pemesan', function ($record) use ($request) {
            $pemesan = $record->user->pelanggans->perusahaan->nama.'</br>'.$record->user->pelanggans->nama_lengkap.'</br>'.$record->user->pelanggans->no_hp;
                return $pemesan;
        })
        ->addColumn('tgl_order', function ($record) use ($request) {
            return DateToStringYear($record->tgl_order);
        })
        ->addColumn('layanan', function ($record) use ($request) {
            return $record->pelayanan->nama;
        })
        ->addColumn('lingkup', function ($record) use ($request) {
            return $record->lingkup->nama;
        })
        ->addColumn('total', function ($record) use ($request) {
            return 0;
        })
        ->addColumn('status', function ($record) use ($request) {
        	if($record->status == 1){
        		return '<label class="ui grey tag label">'.status($record->status).'</label>';
        	}elseif($record->status == 2){
    			if($record->kaji_ulang){
    				if($record->kaji_ulang->surat){
    					if($record->kaji_ulang->surat->va){
    						if($record->kaji_ulang->surat->va->status == 0){
    							return '<label class="ui purple tag label">Menunggu Virtual Account</label>';
    						}elseif($record->kaji_ulang->surat->va->status == 1){
    							if($record->user->pelanggans->perusahaan->kategori !== 0){
                                    $bayar = 'Konfirmasi Pembayaran';
                                    if(!is_null($record->kaji_ulang->surat->va->bukti_url)){
                                        $bayar = 'Bukti Pembayaran Terupload';
                                    }
	        						return '<a href="'.url($this->link.$record->id.'/konfirmasi').'"><button class="ui red basic button"><i class="money bill alternate outline icon"></i>'.$bayar.'</button></a>';
    							}else{
    								return '<label class="ui olive tag label">Menunggu Konfirmasi Pembayaran</label>';
    							}
    						}elseif($record->kaji_ulang->surat->va->status == 2){
    							if($record->kaji_ulang->surat->va->bukti_url){
    								return '<label class="ui pink tag label">Virtual Account Kadaluarsa</label>';
    							}else{
	    							return '<label class="ui red tag label">Virtual Account Nonaktif</label>';
    							}
    						}else{
    							if($record->user->pelanggans->perusahaan->kategori !== 0){
    								if($record->kaji_ulang->surat->va->konfirmasi){
    									if($record->kaji_ulang->surat->va->konfirmasi->status == 1){
        									if($record->kaji_ulang->surat->va->konfirmasi->penerimaan){
		    									if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian){
		    										if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->status_data_pengujian == 0){
			    										if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->reschedule){
			    											if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->reschedule->status == 0){
			    												return '<label class="ui pink tag label">Reschedule</label>';
			    											}else{
			    												return '<label class="ui brown tag label">Pengujian</label>';
			    											}
			    										}else{
			    											return '<label class="ui brown tag label">Pengujian</label>';
			    										}
		    										}else{
		    											if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->laporan_pengujian){
		    												if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->laporan_pengujian->first()->status == 0){
			    												return '<label class="ui yellow tag label">Pengiriman Laporan</label>';
		    												}else{
		    													if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->closeorder){
		    														return '<label class="ui green tag label">Close Order Selesai</label>';	
		    													}else{
		    														return '<label class="ui violet tag label">Menunggu Close Order</label>';	
		    													}
		    												}
		    											}else{
			    											return '<label class="ui brown tag label">Pengujian</label>';
		    											}
		    										}
		    									}else{
			    									return '<label class="ui teal tag label">Menunggu Pengujian</label>';
		    									}
		    								}else{
			    								return '<label class="ui violet tag label">Penyerahan Barang Uji</label>';
		    								}
        								}elseif($record->kaji_ulang->surat->va->konfirmasi->status == 2){
        									return '<label class="ui teal tag label">Pengembalian Dana</label>';
        								}elseif($record->kaji_ulang->surat->va->konfirmasi->status == 3){
		    								if($record->kaji_ulang->surat->va->konfirmasi->penerimaan){
		    									if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian){
		    										if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->status_data_pengujian == 0){
			    										if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->reschedule){
			    											if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->reschedule->status == 0){
			    												return '<label class="ui pink tag label">Reschedule</label>';
			    											}else{
			    												return '<label class="ui brown tag label">Pengujian</label>';
			    											}
			    										}else{
			    											return '<label class="ui brown tag label">Pengujian</label>';
			    										}
		    										}else{
		    											if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->laporan_pengujian){
		    												if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->laporan_pengujian->first()->status == 0){
			    												return '<label class="ui yellow tag label">Pengiriman Laporan</label>';
		    												}else{
		    													if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->closeorder){
		    														return '<label class="ui green tag label">Close Order Selesai</label>';	
		    													}else{
		    														return '<label class="ui violet tag label">Menunggu Close Order</label>';	
		    													}
		    												}
		    											}else{
			    											return '<label class="ui brown tag label">Pengujian</label>';
		    											}
		    										}
		    									}else{
			    									return '<label class="ui teal tag label">Menunggu Pengujian</label>';
		    									}
		    								}else{
			    								return '<label class="ui violet tag label">Penyerahan Barang Uji</label>';
		    								}
        								}else{
        									return '<label class="ui orange tag label">Menunggu Konfirmasi Pembayaran</label>';
        								}
	    							}else{
		    							return '<label class="ui orange tag label">Menunggu Konfirmasi Pembayaran</label>';
	    							}
	    						}else{
	    							if($record->kaji_ulang->surat->va->konfirmasi){
	    								if($record->kaji_ulang->surat->va->konfirmasi->penerimaan){
	    									if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian){
	    										return '<label class="ui brown tag label">Pengujian</label>';
	    									}else{
		    									return '<label class="ui teal tag label">Menunggu Pengujian</label>';
	    									}
	    								}else{
		    								return '<label class="ui violet tag label">Penyerahan Barang Uji</label>';
	    								}
	    							}else{
		    							return '<label class="ui green tag label">Pembayaran Berhasil</label>';
	    							}
	    						}
    						}
    					}else{
    						return '<label class="ui brown tag label">Menunggu Surat Penawaran Terkirim</label>';
    					}
    				}else{
    					if($record->kaji_ulang->keputusan == 3){
        					return '<label class="ui yellow tag label">Didiskusikan</label>';
    					}elseif($record->kaji_ulang->keputusan == 2){
    						if($record->kaji_ulang->bukti){
								return '<label class="ui red tag label">Ditolak</label>';
    						}else{
    							return '<label class="ui green tag label">Menunggu Surat Penawaran</label>';
    						}
    					}else{
    						return '<label class="ui green tag label">Menunggu Surat Penawaran</label>';
    					}
    				}
    			}else{
        			return '<label class="ui olive tag label">Menunggu Kaji Ulang</label>';
    			}
        	}elseif($record->status == 3){
        		return '-';
        	}elseif($record->status == 4){
        		return '<label class="ui blue tag label">'.status($record->status).'</label>';
        	}elseif($record->status == 5){
        		return '<label class="ui red tag label">Dibatalkan</label>';
        	}elseif($record->status == 6){
        		return '<label class="ui violet tag label">'.status($record->status).'</label>';
        	}elseif($record->status == 7){
        		return '<label class="ui green tag label">'.status($record->status).'</label>';
        	}elseif($record->status == 8){
        		return '<label class="ui orange tag label">'.status($record->status).'</label>';
        	}elseif($record->status == 9){
        		return '<label class="ui red tag label">'.status($record->status).'</label>';
        	}elseif($record->status == 10){
        		if($record->kaji_ulang->bukti){
        			return '<a href="'.url($this->link.$record->kaji_ulang->id.'/downloadPenolakan').'"><button class="ui red basic button" data-tooltip="Download Surat Penolakan" data-position="top center"><i class="download icon"></i>Ditolak</button></a>';
        		}else{
        			return '<label class="ui orange tag label">Menunggu Surat Penawaran</label>';
        		}
        		return '<label class="ui red tag label">'.status($record->status).'</label>';
        	}else{
        		return '-';
        	}
        })
        ->editColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->editColumn('briefing_teknis', function ($record) {
        	$btn = '';
            if($record->pelayanan->id == 3){
				 if(isset($record->kaji_ulang)){
				 	if($record->kaji_ulang->files->where('type', 1)->count()>0){
				 		if($record->kaji_ulang->files->where('type', 2)->count()>0){
				 			$btn .= $this->makeButton([
				 				'type' => 'url',
				 				'tooltip' => 'Download Jadwal',
				 				'class' => 'ui blue mini icon button',
				 				'label' => '<i class="download icon"></i>',
				 				'url' => url('download-briefing', $record->kaji_ulang->id).'/briefing-teknis/1'
				 			]);

				 			$btn .= $this->makeButton([
				 				'type' => 'url',
				 				'tooltip' => 'Download Notulen',
				 				'class' => 'ui teal mini icon button',
				 				'label' => '<i class="download icon"></i>',
				 				'url' => url('download-briefing', $record->kaji_ulang->id).'/briefing-teknis/2'
				 			]);
				 		}else{
				 			$btn .= $this->makeButton([
				 				'type' => 'url',
				 				'tooltip' => 'Download Jadwal',
				 				'class' => 'ui blue mini icon button',
				 				'label' => '<i class="download icon"></i>',
				 				'url' => url('download-briefing', $record->kaji_ulang->id).'/briefing-teknis/1'
				 			]);
				 		}

                        if($record->kaji_ulang->surat){
                            if($record->kaji_ulang->surat->files){
                                $btn .= $this->makeButton([
                                    'type' => 'url',
                                    'tooltip' => 'Download Surat Penawaran',
                                    'class' => 'ui orange mini icon button',
                                    'label' => '<i class="download icon"></i>',
                                    'url' => url('download-briefing', $record->kaji_ulang->surat->id).'/surat-penawaran'
                                ]);
                            }
                        }
				 	}else{
				 		if($record->kaji_ulang->surat){
                            if($record->kaji_ulang->surat->files){
                                $btn .= $this->makeButton([
                                    'type' => 'url',
                                    'tooltip' => 'Download Surat Penawaran',
                                    'class' => 'ui orange mini icon button',
                                    'label' => '<i class="download icon"></i>',
                                    'url' => url('download-briefing', $record->kaji_ulang->surat->id).'/surat-penawaran'
                                ]);
                            }
                        }
				 	}
		        }
            }else{
            	if(isset($record->kaji_ulang)){
            		if($record->kaji_ulang->surat){
            			if($record->kaji_ulang->surat->files){
            				$btn .= $this->makeButton([
            					'type' => 'url',
            					'tooltip' => 'Download Surat Penawaran',
            					'class' => 'ui orange mini icon button',
            					'label' => '<i class="download icon"></i>',
            					'url' => url('download-briefing', $record->kaji_ulang->surat->id).'/surat-penawaran'
            				]);
            			}
            		}
            	}else{
	            	$btn .= '-';
            	}
            }
        	return $btn;
        })
        ->addColumn('action', function ($record) {
            $btn = '';

            $btn = $this->makeButton([
                'type' => 'url',
                'class' => 'ui blue mini icon button',
                'tooltip' => 'Lihat',
                'label' => '<i class="eye icon"></i>',
                'url' => url($this->link.$record->id.'/detil')
            ]);

            return $btn;
        })
        ->rawColumns(['status','pemesan','action','briefing_teknis'])
        ->make(true);
    }

    public function index()
    {
    	$this->setTitle('Order');
        $this->setBreadcrumb(['Order' => '#']);
        $data = [
            'mockup' => true,
            'structs' => $this->listStructs,
        ];

        return $this->render('modules.permintaan.index', $data);
    }

    public function create()
    {
    	$this->setTitle('Buat Order Baru');
    	$this->setBreadcrumb(['Order' => '#', 'Buat' => '#']);
        return $this->render('modules.permintaan.create');
    }

    public function buat()
    {
        return $this->render('modules.permintaan.partials.buat');
    }

    public function lihat($data)
    {
    	$month = date('n');
    	$year = date("Y");
        return $this->render('modules.permintaan.partials.lihat-jadwal', [
        	'prioritas' => $data,
        	'tahun' => $year,
        	'bulan' => $month,
    	]);
    }

    public function lihatLayanan()
    {
    	$record = JenisPelayanan::get();
        return $this->render('modules.permintaan.partials.pelayanan',['record' => $record]);
    }

    public function searchLayanan(Request $request)
    {
        $pelayanan = $request->pelayanan;
        $lingkup = $request->lingkup;
        $jenis_pengujian = $request->jenis_pengujian;

        // $record = JenisPelayanan::get();
        $record = JenisPelayanan::with('lingkup', 'pengujian');

        if ($pelayanan) {
            $record->where('id', $pelayanan);
        }

        if ($lingkup) {
             $record->whereHas('lingkup',function($q) use($lingkup){
                $q->where('id', $lingkup);
            });
        }

        if ($jenis_pengujian){
            $record->whereHas('lingkup',function($q) use($jenis_pengujian){
                $q->whereHas('pengujian',function($query) use($jenis_pengujian){
                    $query->where('nama', 'like', '%'.$jenis_pengujian.'%');
                });
            });
        }

        return view('modules.permintaan.partials.pelayanan-table', ['record' => $record->get()]);
    }

    public function detil($id)
    {
        $record = PendaftaranPengujian::find($id);
    	$kaji_ulang =$record->kaji_ulang;
    	if($record->kaji_ulang){
	    	$surat = $record->kaji_ulang->surat;
    	}else{
    		$surat = [];
    	}
    	$this->setTitle('Detil Order');
        $this->setBreadcrumb(['Order' => '#','Detil Order' => '#']);
        $this->setSubtitle('No Order : '.$record->no_order);
        return $this->render('modules.permintaan.partials.detil',[
        	'record' => $record, 
			'kaji_ulang' => $kaji_ulang,
			'surat' => $surat,
        ]);
    }

    public function ubah()
    {
        return $this->render('modules.permintaan.partials.ubah');
    }

    public function store(PermintaanPengujianRequest $request)
    {
    	if($request->detail){
    	}else{
    		return response()->json([
                'status' => 'false',
                'message' => 'Data Item Uji tidak boleh kosong'
            ],402);
    	}

        $date = date('Y-m-d');
        $nomor = array();

        DB::beginTransaction();
        try {
        	$no = PendaftaranPengujian::generateNomor();
        	if($request->jenis_pelayanan_id == 3){
        		$a =0;
        		foreach ($request->detail as $key => $value) {
	        		$no_order = PendaftaranPengujian::generateNoOrder($request->user_id);
		            $pengujian = new PendaftaranPengujian;
		            $pengujian->no_order = PendaftaranPengujian::generateNoOrder($request->user_id);
		            $pengujian->nomor = $no;
		        	$pengujian->tgl_surat = $request->tgl_surat;
		        	$pengujian->no_surat = $request->no_surat;
		        	$pengujian->tipe = 3;
		            $pengujian->tgl_order = $date;
		            $pengujian->user_id = $request->user_id;
		            $pengujian->layanan_id = $request->jenis_pelayanan_id;
		            $pengujian->lingkup_id = $request->lingkup_id;
		            $pengujian->kelas = $request->kelas;
		            $pengujian->rencana = $request->rencana;
		            $pengujian->status = 1;
		            $pengujian->jenis = 1;
		            $pengujian->catatan = $request->catatan;

		            if($file = $request->surat){
		        		$path = $file->store('uploads/pendaftaran', 'public');
		        		$pengujian->filename = $file->getClientOriginalName();
		        		$pengujian->bukti = $path;
		            }
		            $pengujian->save();

		            $detail = new PendaftaranPengujianDetail;
		            $detail->pp_id = $pengujian->id;
		            $detail->jenis_id = $value['jenis_id'];
			        $detail->merk = $value['merk'];
			        $detail->tipe = $value['tipe'];
			        $detail->jumlah = $value['jumlah'];
			        $detail->satuan_id = $value['satuan_id'];
			        $detail->spesifikasi = $value['spesifikasi'];
		            $detail->save();

		            $pengujian->multipleFileUpload($request->lampiran);
		            if($a % 2 == 0){
			            $nomor[]= $no_order.'<br>';
		            }else{
			            $nomor[]= $no_order;
		            }

		            $a++;
		            if($pengujian->jenis==0){
	                    $this->pushNotifKalibrasi($pengujian);
	                }else{
	                    $this->pushNotifUji($pengujian);
	                }
        		}
        	}else{
		        $no_order = PendaftaranPengujian::generateNoOrder($request->user_id);
	            $pengujian = new PendaftaranPengujian;
	            $pengujian->no_order = PendaftaranPengujian::generateNoOrder($request->user_id);
	            $pengujian->nomor = $no;
	        	$pengujian->tgl_surat = $request->tgl_surat;
	        	$pengujian->no_surat = $request->no_surat;
	        	$pengujian->tipe = 3;
	            $pengujian->tgl_order = $date;
	            $pengujian->user_id = $request->user_id;
	            $pengujian->layanan_id = $request->jenis_pelayanan_id;
	            $pengujian->lingkup_id = $request->lingkup_id;
	            $pengujian->kelas = $request->kelas;
	            $pengujian->rencana = $request->rencana;
	            $pengujian->status = 1;
	            $pengujian->jenis = 1;
	            $pengujian->catatan = $request->catatan;
	            if($request->jenis_pelayanan_id == 1){
	            	$pengujian->jenis = 0;
	            }else{
	            	$pengujian->jenis = 1;
	            }

	            if($file = $request->surat){
	        		$path = $file->store('uploads/pendaftaran', 'public');
	        		$pengujian->filename = $file->getClientOriginalName();
	        		$pengujian->bukti = $path;
	            }
	            $pengujian->save();
	            $pengujian->saveDetail($request->detail);
	            $pengujian->multipleFileUpload($request->lampiran);

	            $nomor[] = $no_order;
	            if($pengujian->jenis==0){
                    $this->pushNotifKalibrasi($pengujian);
                }else{
                    $this->pushNotifUji($pengujian);
                }
        	}
            DB::commit();

	    }catch (\Exception $e) {
	      DB::rollback();
	      return response([
	        'status' => 'error',
	        'message' => 'An error occurred!',
	        'error' => $e->getMessage(),
	      ], 500);
	    }

	    return response([
	      'status' => true,
          'message' => $nomor
	    ]);
    }

    public function pushNotifKalibrasi($data){
        $user = User::whereHas('roles',function($query){
                    $query->where('name','yan-kalibrasi');
                })->get();
        foreach ($user as $row) {
            if(!is_null($row->device_id)){
                 $this->onesignal('Penerimaan Surat Masuk(Manual) Baru dengan nomor order '.$data->no_order,$row->device_id,$data->toArray(),'penerimaan-surat','manual');
            }
        }
    }

    public function pushNotifUji($data){
        $user = User::whereHas('roles',function($query){
                    $query->where('name','yan-uji');
                })->get();
        foreach ($user as $row) {
            if(!is_null($row->device_id)){
                 $this->onesignal('Penerimaan Surat Masuk(Manual) Baru dengan nomor order '.$data->no_order,$row->device_id,$data->toArray(),'penerimaan-surat','manual');
            }
        }
    }

    public function edit($id)
    {
    	$this->setTitle('Ubah Permintaan SPM');
    	$this->setBreadcrumb(['Ubah Permintaan SPM'=> '#', 'Ubah' => '#']);
        $record = PendaftaranPengujian::find($id);
        return $this->render('modules.permintaan.edit',['record' => $record]);
    }

    public function konfirmasi($id)
    {
    	$this->setTitle('Konfirmasi Pembayaran');
    	$this->setBreadcrumb(['Konfirmasi Pembayaran' => '#', 'Konfirmasi Pembayaran' => '#']);
        $record = PendaftaranPengujian::find($id);
        return $this->render('modules.permintaan.partials.konfirmasi',['record' => $record]);
    }

    public function upload($id)
    {
        return $this->render('modules.permintaan.partials.upload', ['lingkup_id' => $id]);
    }

    public function cekUpload(Request $request){
    	$cek_id = array();
    	$satuan_id = array();
    	$jenis_pengujian = JenisPengujian::where('lingkup_id', $request->lingkup_id)->get();
    	$satuan = Satuan::get();
    	foreach ($jenis_pengujian as $key => $val) {
    		$cek_id[] = $val->id;
    	}
    	foreach ($satuan as $key => $vl) {
    		$satuan_id[] = $vl->id;
    	}
    	if($request->hasFile('upload_data')){
    		$path = $request->file('upload_data')->getRealPath();
    		$data = Excel::load($path, function($reader) {})->get();
    		$insert = array();
    		$i = 1;
    		$y=0;
    		$z=0;
    		if(!empty($data) && $data->count()){
    			foreach ($data as $key => $value) {
    				if(!is_null($value->id_jenis_pengujian) && !is_null($value->id_satuan)){
    					if(in_array((int)$value->id_jenis_pengujian, $cek_id, TRUE)){
    						if(in_array((int)$value->id_satuan, $satuan_id, TRUE)){
    							$nama_jenis = JenisPengujian::find((int)$value->id_jenis_pengujian);
    							$nama_satuan = Satuan::find((int)$value->id_satuan);
    							if($i <= 350){
				    				$insert[] = [
				    					'id_jenis_pengujian' => (int)$value->id_jenis_pengujian,
				    					'nama_jenis' => $nama_jenis->nama,
				    					'nama_satuan' => $nama_satuan->satuan,
				    					'merk' => $value->merk,
				    					'tipe' => $value->tipe,
				    					'jumlah' => (int)$value->jumlah,
				    					'id_satuan' => (int)$value->id_satuan,
				    					'spesifikasi' => $value->spesifikasi,
				    				];
    							}
			    				$i++;
 							}else{
 								$y +=1;
 							}
 						}else{
 							$z +=1;
 						}
    				}
    			}

    			if($z > 0){
    				return response([
			            'status' => false,
			            'message' => 'Ada '.$z.' data Jenis Pengujian yang tidak sesuai',
			            'data' => $insert,
			        ]);
    			}elseif($y > 0){
    				return response([
			            'status' => false,
			            'message' => 'Ada '.$z.' data Satuan yang tidak sesuai',
			            'data' => $insert,
			        ]);
    			}else{
    				return response([
			            'status' => true,
			            'message' => '',
			            'data' => $insert,
			        ]);	
    			}
    		}else{
    			return response([
    				'status' => false,
    				'message' => 'Tidak Ada Data',
    				'data' => '',
    			]);
    		}
		}else{
			return response([
				'status' => false,
				'message' => 'Inputan tidak boleh kosong',
				'data' => '',
			]);
		}
    }

    public function exportTemplate(Request $request, $lingkup)
    {	
    	$jenis_pengujian = JenisPengujian::where('lingkup_id', $lingkup)->get();
    	$data_lingkup = Lingkup::find($lingkup);
    	$data_satuan = Satuan::get();
    	 $Export = Excel::create('List Item Uji - '.$data_lingkup->pelayanan->nama, function($excel) use($jenis_pengujian, $data_lingkup, $data_satuan) {
            $excel->setTitle('List Item Uji');
            $excel->setCreator('E-Uji dan E-Kal')->setCompany('E-Pusertif');
            $excel->setDescription('Export');
            $excel->sheet('List Item Uji ', function($sheet) use($jenis_pengujian, $data_lingkup, $data_satuan){
                $sheet->row(1, array(
	                'id_jenis_pengujian',
                    'merk',
                    'tipe',
                    'jumlah',
                    'id_satuan',
                    'spesifikasi',
                ));
                $sheet->setWidth(array('H'=>6));
                $sheet->SetCellValue("H1", "ID");
                $sheet->SetCellValue("I1", "Jenis Pengujian");
                $sheet->setBorder('H1:M1', 'thin');
                $sheet->mergeCells("I1:M1");
                $sheet->cells('H1:M1', function($cells){
                		$cells->setBackground('#999999');
                    	$cells->setFontColor('#FFFFFF');
                    	$cells->setAlignment('center');
                });

                $row = 2;
                $no =1;
                foreach ($jenis_pengujian as $key => $value) {
	                $from="I".$row;
					$to="M".$row;
	                $sheet->mergeCells("$from:$to");
	                $sheet->cells('H'.$row, function($cells){
                    	$cells->setAlignment('center');
                	});
	                $sheet->SetCellValue('H'.$row, $value->id);
	                $sheet->SetCellValue("I".$row, $value->nama);
	                $row++;
                }

                $awal="H".($row+2);
				$akhir="I".($row+2);
				$a="I".($row+2);
				$b="K".($row+2);
                $sheet->SetCellValue('H'.($row+2), "ID");
                $sheet->SetCellValue('I'.($row+2), "Satuan");
                $sheet->setBorder("$awal:$akhir", 'thin');
                $sheet->mergeCells("$a:$b");
                $sheet->cells("$awal", function($cells){
                		$cells->setBackground('#999999');
                    	$cells->setFontColor('#FFFFFF');
                    	$cells->setAlignment('center');
                });
                $sheet->cells("$akhir", function($cells){
                		$cells->setBackground('#999999');
                    	$cells->setFontColor('#FFFFFF');
                });

                $z = ($row+3);
                foreach ($data_satuan as $key => $val) {
	                $from="I".$z;
					$to="K".$z;
	                $sheet->mergeCells("$from:$to");
	                $sheet->cells('H'.$z, function($cells){
                    	$cells->setAlignment('center');
                	});
	                $sheet->SetCellValue('H'.$z, $val->id);
	                $sheet->SetCellValue("I".$z, $val->satuan);
	                $z++;
                }

                $sheet->setBorder('A1:F1', 'thin');
                $sheet->cells('A1:F1', function($cells){
                		$cells->setBackground('#999999');
                    	$cells->setFontColor('#FFFFFF');
                    	$cells->setAlignment('center');
                });
                $xx ="H".($z+2);
                $yy ="P".($z+2);
                $sheet->mergeCells("$xx:$yy");
                $sheet->SetCellValue($xx, 'Catatan : Mohon inputkan ID Pengujian dan ID Satuan Sesuai dengan List diatas.');
                $sheet->cells($xx, function($cells){
                    $cells->setFontSize(10);
                });
                $sheet->cells('A1:A300', function($cells){
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->setWidth(array('A'=>20,'B'=>40,'C'=>20, 'D'=>20, 'E'=>15, 'F'=>30));
            });
        });
        $Export ->download('xls');
    }

    public function destroy($id)
    {
        $record = PendaftaranPengujian::find($id);
        unlink(public_path('storage/'.$record->bukti));
        $record->delete();
        return response([
            'status' => true,
        ]);
    }

    public function preview($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function previewMultiple($id, $type)
    {
    	if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $c->filename,
                        ];
                    }
                }
                return $this->render('pdf-multiple', [
		        	'mockup' => true,
		        	'data' => $rename,
		        ]);
            }
        }
    }

    public function download($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

// BATAS BARU

    public function getDetail(Request $request){
        $recordJenisPengujian = JenisPelayanan::find($request->jenis_pelayanan);

        return $this->render('modules.permintaan.partials.detail',['recordJenisPengujian'=>$recordJenisPengujian]);
    }


    public function cekRequestItemUji(CekItemUjiRequest $request){
        return response([
            'status' => true,
        ]);
    }
}
