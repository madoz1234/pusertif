<?php

namespace App\Http\Controllers\KonfirmasiPembayaran;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\VirtualAccount\KonfirmasiPembayaran;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\KajiUlang\KajiUlang;
use App\Models\KajiUlang\Detail;
use App\Models\Act\ActDisposisi;

/* Validation */
use App\Http\Requests\VirtualAccount\KonfirmasiPembayaranRequest;
use App\Http\Requests\VirtualAccount\KurangBayarRequest;

/* Models */
use App\Models\Authentication\Role;
use App\Models\Authentication\User;
use App\Models\Files;

/* Libraries */
use DataTables;
use Carbon;
use Hash;

class KonfirmasiPembayaranController extends Controller
{
    protected $link = 'konfirmasi/konfirmasi-pembayaran/';
    protected $perms = 'konfirmasi-pembayaran';
    protected $listStructs = [];

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Konfirmasi Pembayaran");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Konfirmasi Pembayaran' => '#']);

        // Header Grid Datatable
        $this->listStructs = [
            'listStruct' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                /* --------------------------- */
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'className' => "center aligned",
                    'searchable' => false,
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'className' => "center aligned",
                    'searchable' => false,
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan',
                    'name' => 'layanan',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '200px',
                    'sortable' => true,
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'searchable' => false,
                    'className' => "left aligned",
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'pemesan',
                    'name' => 'pemesan',
                    'label' => 'Peminta Jasa',
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '300px',
                    'sortable' => true,
                ],
                [
                    'data' => 'va',
                    'name' => 'va',
                    'label' => 'No VA',
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'nominal',
                    'name' => 'nominal',
                    'label' => 'Nominal VA (Rp)',
                    'searchable' => false,
                    'className' => "right aligned",
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'bukti',
                    'name' => 'bukti',
                    'label' => 'Bukti Pembayaran',
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '150px',
                    'sortable' => false,
                ],
                [
                    'data' => 'timestamp',
                    'name' => 'timestamp',
                    'label' => 'Timestamp',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned timestamp",
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'batas_va',
                    'name' => 'batas_va',
                    'label' => 'Kadaluarsa VA',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '80px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
            ],
            'listStruct2' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                /* --------------------------- */
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'className' => "center aligned",
                    'searchable' => false,
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'className' => "center aligned",
                    'searchable' => false,
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan',
                    'name' => 'layanan',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '200px',
                    'sortable' => true,
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'searchable' => false,
                    'className' => "left aligned",
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'pemesan',
                    'name' => 'pemesan',
                    'label' => 'Peminta Jasa',
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '300px',
                    'sortable' => true,
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'va',
                    'name' => 'va',
                    'label' => 'No VA',
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'nominal',
                    'name' => 'nominal',
                    'label' => 'Nominal VA (Rp)',
                    'searchable' => false,
                    'className' => "right aligned",
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'dana_masuk',
                    'name' => 'dana_masuk',
                    'label' => 'Dana Masuk (Rp)',
                    'searchable' => false,
                    'className' => "right aligned",
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'bukti',
                    'name' => 'bukti',
                    'label' => 'Bukti Pembayaran',
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '150px',
                    'sortable' => false,
                ],
                [
                    'data' => 'tgl_bayar',
                    'name' => 'tgl_bayar',
                    'label' => 'Tgl Pembayaran',
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '80px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
            ],
            'listStruct3' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                /* --------------------------- */
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'className' => "center aligned",
                    'searchable' => false,
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'className' => "center aligned",
                    'searchable' => false,
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan',
                    'name' => 'layanan',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '200px',
                    'sortable' => true,
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'searchable' => false,
                    'className' => "left aligned",
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'pemesan',
                    'name' => 'pemesan',
                    'label' => 'Peminta Jasa',
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '300px',
                    'sortable' => true,
                ],
                [
                    'data' => 'va',
                    'name' => 'va',
                    'label' => 'No VA',
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'nominal',
                    'name' => 'nominal',
                    'label' => 'Nominal VA (Rp)',
                    'searchable' => false,
                    'className' => "right aligned",
                    'width' => '150px',
                    'sortable' => true,
                ],
                [
                    'data' => 'bukti',
                    'name' => 'bukti',
                    'label' => 'Bukti Pembayaran',
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '150px',
                    'sortable' => false,
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '80px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
            ],
        ];
    }

    public function belum(Request $request)
    {	
    	if(auth()->user()->hasRole(['keuangan', 'yan-uji', 'yan-kalibrasi','admin'])){
	        $records = VirtualAccount::where('status', 1)
	        						->whereIn('tipe_customer', [1,0])
	                                ->doesnthave('konfirmasi')
	                                ->where('tipe', 0)
	                                ->select('*');
	    }else{
	    	$records =  VirtualAccount::where('id', 0);
	    }
 
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        if ($no_va = $request->no_va) {
            $records->where('no_va','like','%'.$no_va.'%');
        }

        if ($no_surat = $request->no_surat) {
            $records->whereHas('surat', function($surat) use ($no_surat){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                    $kaji->whereHas('pp', function($pp) use ($no_surat){
                        $pp->where('no_surat','like', '%'.$no_surat.'%');
                    });
                });
            });
        }
        if ($no_order = $request->no_order) {
            $records->whereHas('surat', function($surat) use ($no_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                    $kaji->whereHas('pp', function($pp) use ($no_order){
                        $pp->where('no_order','like', '%'.$no_order.'%');
                    });
                });
            });
        }
        if ($tanggal_order = $request->tanggal_order) {
            $records->whereHas('surat', function($surat) use ($tanggal_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                    $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                    });
                });
            });
        }
        if ($no_wbs = $request->no_wbs) {
            $records->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
            });
        }
        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $records->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                    $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                        $pp->where('layanan_id',$jenis_pelayanan_id);
                    });
                });
            });
        }
        if ($perusahaan_id = $request->perusahaan_id) {
            $records->whereHas('surat', function($surat) use ($perusahaan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                    $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                        $pp->whereHas('user', function($user) use ($perusahaan_id){
                            $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                $pelanggan->where('perusahaan_id',$perusahaan_id);
                            });
                        });
                    });
                });
            });
        }
        if ($bukti_upload = $request->bukti_upload) {
            if($bukti_upload==1){
                $records->whereNotNull('bukti_url');
            }
            if($bukti_upload==2){
                $records->whereNull('bukti_url');
            }
        }

        $records = $records->get();

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('pemesan', function ($record) use ($request) {
                $pemesan = $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->no_hp;
                return $pemesan;
            })
            ->addColumn('layanan', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->surat->kaji_ulang->pp->detail){
                    $jenis_pengujian .= '<div class="ui ordered list list-more1" data-display="3">';
                    foreach ($record->surat->kaji_ulang->pp->detail as $key => $value) {
                        $kaji_ulang = KajiUlang::find($record->surat->kaji_ulang->id);
                        if($kaji_ulang){
                            foreach ($kaji_ulang->detail as $kuy => $val) {
                                if($val->jenis_id == $value->id){
                                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
                                }
                            }
                        }else{
                            $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
                        }
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('va', function($record){
            	if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0){
            		return '-';
            	}else{
                	return $record->no_va;
            	}
            })
            ->editColumn('nominal', function($record){
                return FormatNumber($record->nominal);
            })
            ->editColumn('bukti', function($record){
            	$btn='';
            	if($record->bukti_url){
            		$btn .= $this->makeButton([
		                    'tooltip'  => 'Preview Bukti Pembayaran',
		                    'class'  => 'teal icon lihat_data',
		                    'label'  => '<i class="file image outline icon"></i>',
		                    'type' => 'modal',
		                    'id'   => $record->id
		                ]);
            		$btn .= $this->makeButton([
            			'type' => 'url',
            			'tooltip' => 'Download Bukti Pembayaran',
            			'class' => 'ui blue mini icon button',
            			'label' => '<i class="download icon"></i>',
            			'url' => url('download-va', $record->id)
            		]);
            	}else{
            		$btn = '-';
            	}

                return $btn;
            })
            ->editColumn('detil', function($record){
                $btn = '';
                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->surat_id.'/detil')
                ]);

                return $btn;
            })
            ->editColumn('timestamp', function($record){
            	if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0){
            		return '-';
            	}else{
                	return $record->tgl_kadaluarsa;
            	}
            })
            ->editColumn('batas_va', function($record){
            	if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0){
            		return '-';
            	}else{
                	return DateToStringYear($record->tgl_kadaluarsa);
            	}
            })
            ->editColumn('status', function($record){
                $status = '<a class="ui orange tag label">Menunggu Konfirmasi</a>';
                return $status;
            })
            ->addColumn('action', function ($record) use ($link) {
                $btn = '-';
                if(auth()->user()->hasRole(['keuangan'])){
	                if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0){
	                	$btn = $this->makeButton([
		                    'type' => 'edit',
		                    'id'   => $record->id,
		                    'tooltip' => 'Buat Konfirmasi',
			            ]);
	                }else{
		                if($record->bukti_url){
			                $btn = $this->makeButton([
			                    'type' => 'edit',
			                    'id'   => $record->id,
			                    'tooltip' => 'Buat Konfirmasi',
			                ]);
		                }else{
		                	$btn = '-';
		                }
	                }
                }else{
                	$btn = '-';
                }
                return $btn;
            })
            ->rawColumns(['pemesan','jenis_pengujian','action','detil','status','layanan','bukti','tgl_bayar'])
            ->make(true);
    }

    public function kurang(Request $request)
    {	
    	if(auth()->user()->hasRole(['keuangan', 'yan-uji', 'yan-kalibrasi','admin'])){
	        $records = VirtualAccount::where('status', 3)
	                                ->whereHas('konfirmasi', function($q){
	                                    return $q->where('status', 1);
	                                })
	                                ->where('tipe', 0)
	                                ->select('*');
	    }else{
	    	$records =  VirtualAccount::where('id', 0);
	    }

        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        if ($no_va = $request->no_va) {
            $records->where('no_va','like','%'.$no_va.'%');
        }

        if ($no_surat = $request->no_surat) {
            $records->whereHas('surat', function($surat) use ($no_surat){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                    $kaji->whereHas('pp', function($pp) use ($no_surat){
                        $pp->where('no_surat','like', '%'.$no_surat.'%');
                    });
                });
            });
        }
        if ($no_order = $request->no_order) {
            $records->whereHas('surat', function($surat) use ($no_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                    $kaji->whereHas('pp', function($pp) use ($no_order){
                        $pp->where('no_order','like', '%'.$no_order.'%');
                    });
                });
            });
        }
        if ($tanggal_order = $request->tanggal_order) {
            $records->whereHas('surat', function($surat) use ($tanggal_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                    $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                    });
                });
            });
        }
        if ($no_wbs = $request->no_wbs) {
            $records->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
            });
        }
        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $records->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                    $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                        $pp->where('layanan_id',$jenis_pelayanan_id);
                    });
                });
            });
        }
        if ($perusahaan_id = $request->perusahaan_id) {
            $records->whereHas('surat', function($surat) use ($perusahaan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                    $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                        $pp->whereHas('user', function($user) use ($perusahaan_id){
                            $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                $pelanggan->where('perusahaan_id',$perusahaan_id);
                            });
                        });
                    });
                });
            });
        }
        if ($bukti_upload = $request->bukti_upload) {
            if($bukti_upload==1){
                $records->whereNotNull('bukti_url');
            }
            if($bukti_upload==2){
                $records->whereNull('bukti_url');
            }
        }

        $records = $records->get();


        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('pemesan', function ($record) use ($request) {
                $pemesan = $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->no_hp;
                return $pemesan;
            })
            ->addColumn('layanan', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->surat->kaji_ulang->pp->detail){
                    $jenis_pengujian .= '<div class="ui ordered list list-more1" data-display="3">';
                    foreach ($record->surat->kaji_ulang->pp->detail as $key => $value) {
                        $kaji_ulang = KajiUlang::find($record->surat->kaji_ulang->id);
                        if($kaji_ulang){
                            foreach ($kaji_ulang->detail as $kuy => $val) {
                                if($val->jenis_id == $value->id){
                                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
                                }
                            }
                        }else{
                            $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
                        }
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('va', function($record){
            	if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0){
            		return '-';
            	}else{
                	return $record->no_va;
            	}
            })
            ->editColumn('nominal', function($record){
                return FormatNumber($record->nominal);
            })
            ->editColumn('bukti', function($record){
            	$btn='';
            	if($record->bukti_url){
            		$btn .= $this->makeButton([
		                    'tooltip'  => 'Preview Bukti Pembayaran',
		                    'class'  => 'teal icon lihat_data',
		                    'label'  => '<i class="file image outline icon"></i>',
		                    'type' => 'modal',
		                    'id'   => $record->id
		                ]);
            		$btn .= $this->makeButton([
            			'type' => 'url',
            			'tooltip' => 'Download Bukti Pembayaran',
            			'class' => 'ui blue mini icon button',
            			'label' => '<i class="download icon"></i>',
            			'url' => url('download-va', $record->id)
            		]);
            	}else{
            		$btn = '-';
            	}

                return $btn;
            })
            ->editColumn('detil', function($record){
                $btn = '';
                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->surat_id.'/detil')
                ]);

                return $btn;
            })
            ->editColumn('status', function($record){
                $status = '<a class="ui green tag label">Terbayar</a>';
                return $status;
            })
            ->addColumn('action', function ($record) use ($link) {
                $btn = ' ';
                if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0){
                	$btn = '-';
                }else{
	                if($record->bukti_url){
	                	if(auth()->user()->hasRole(['keuangan'])){
		                	$btn .= '<button class="ui orange mini icon ubahKurang button" data-tooltip="Ubah Data" data-position="top center" data-id="'.$record->id.'"><i class="money icon"></i></button>';
		                }else{
		                	$btn = '-';
		                }
	                }else{
	                	$btn = '-';
	                }
                }

                return $btn;
            })
            ->rawColumns(['pemesan','jenis_pengujian','action','detil','status','layanan','bukti','tgl_bayar'])
            ->make(true);
    }

    public function lebih(Request $request)
    {	
    	if(auth()->user()->hasRole(['keuangan', 'yan-uji', 'yan-kalibrasi','admin'])){
	        $records = VirtualAccount::where('status', 3)
	                                ->whereHas('konfirmasi', function($q){
	                                    return $q->where('status', 2);
	                                })
	                                ->where('tipe', 0)
	                                ->select('*');
	    }else{
	    	$records =  VirtualAccount::where('id', 0);
	    }

        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        if ($no_va = $request->no_va) {
            $records->where('no_va','like','%'.$no_va.'%');
        }

        if ($no_surat = $request->no_surat) {
            $records->whereHas('surat', function($surat) use ($no_surat){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                    $kaji->whereHas('pp', function($pp) use ($no_surat){
                        $pp->where('no_surat','like', '%'.$no_surat.'%');
                    });
                });
            });
        }
        if ($no_order = $request->no_order) {
            $records->whereHas('surat', function($surat) use ($no_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                    $kaji->whereHas('pp', function($pp) use ($no_order){
                        $pp->where('no_order','like', '%'.$no_order.'%');
                    });
                });
            });
        }
        if ($tanggal_order = $request->tanggal_order) {
            $records->whereHas('surat', function($surat) use ($tanggal_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                    $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                    });
                });
            });
        }
        if ($no_wbs = $request->no_wbs) {
            $records->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
            });
        }
        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $records->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                    $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                        $pp->where('layanan_id',$jenis_pelayanan_id);
                    });
                });
            });
        }
        if ($perusahaan_id = $request->perusahaan_id) {
            $records->whereHas('surat', function($surat) use ($perusahaan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                    $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                        $pp->whereHas('user', function($user) use ($perusahaan_id){
                            $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                $pelanggan->where('perusahaan_id',$perusahaan_id);
                            });
                        });
                    });
                });
            });
        }
        if ($bukti_upload = $request->bukti_upload) {
            if($bukti_upload==1){
                $records->whereNotNull('bukti_url');
            }
            if($bukti_upload==2){
                $records->whereNull('bukti_url');
            }
        }

        $records = $records->get();


        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('pemesan', function ($record) use ($request) {
                $pemesan = $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->no_hp;
                return $pemesan;
            })
            ->addColumn('layanan', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->surat->kaji_ulang->pp->detail){
                    $jenis_pengujian .= '<div class="ui ordered list list-more3" data-display="3">';
                    foreach ($record->surat->kaji_ulang->pp->detail as $key => $value) {
                        $kaji_ulang = KajiUlang::find($record->surat->kaji_ulang->id);
                        if($kaji_ulang){
                            foreach ($kaji_ulang->detail as $kuy => $val) {
                                if($val->jenis_id == $value->id){
                                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
                                }
                            }
                        }else{
                            $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
                        }
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('va', function($record){
            	if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0){
            		return '-';
            	}else{
                	return $record->no_va;
            	}
            })
            ->editColumn('nominal', function($record){
                return FormatNumber($record->nominal);
            })
            ->editColumn('dana_masuk', function($record){
                return ($record->konfirmasi) ? FormatNumber($record->konfirmasi->dana_masuk) : '-';
            })
            ->editColumn('wbs_io', function($record){
                return $record->konfirmasi->wbs_io ? $record->konfirmasi->wbs_io : '-';
            })
            ->editColumn('bukti', function($record){
            	$btn='';
                if($record->bukti_url){
                	$btn .= $this->makeButton([
		                    'tooltip'  => 'Preview Bukti Pembayaran',
		                    'class'  => 'teal icon lihat_data',
		                    'label'  => '<i class="file image outline icon"></i>',
		                    'type' => 'modal',
		                    'id'   => $record->id
		                ]);
            		$btn .= $this->makeButton([
            			'type' => 'url',
            			'tooltip' => 'Download Bukti Pembayaran',
            			'class' => 'ui blue mini icon button',
            			'label' => '<i class="download icon"></i>',
            			'url' => url('download-va', $record->id)
            		]);
            	}else{
            		$btn = '-';
            	}

                return $btn;
            })
            ->editColumn('tgl_bayar', function ($record) use ($request) {
            	if($record->tgl_bayar){
	                // $tgl= Carbon::parse($record->tgl_bayar);
	                // return DateToStringWday($tgl).'</br>'.$tgl->format('H:i').' WIB';
                    return DateToStringYear($record->tgl_bayar);
            	}else{
            		return '-';
            	}
            })
            ->editColumn('status', function($record){
                return '-';
            })
            ->editColumn('detil', function($record){
                $btn = '';
                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->surat_id.'/detil')
                ]);

                return $btn;
            })
            ->editColumn('status', function($record){
                $status = '<a class="ui green tag label">Terbayar</a>';
                return $status;
            })
            ->editColumn('checklist', function($record){
            	if(auth()->user()->hasRole(['keuangan'])){
                	$checklist = '<div class="ui fitted checkbox">
                            <input name="checklist[]" class="va" type="checkbox" data-id="'.$record->id.'">
                            <label></label>
                          </div>';
                }else{
                	$checklist='-';
                }
                return $checklist;
            })
            ->rawColumns(['pemesan','jenis_pengujian','action','detil','bukti','status','checklist','layanan','tgl_bayar'])
            ->make(true);
    }

    public function lunas(Request $request)
    {	
    	if(auth()->user()->hasRole(['keuangan','yan-uji','yan-kalibrasi','admin'])){
	        $records = VirtualAccount::where('status', 3)
	                                ->whereHas('konfirmasi', function($q){
	                                    return $q->where('status', 3);
	                                })
	                                ->where('tipe', 0)
	                                ->select('*');
	    }else{
	    	$records =  VirtualAccount::where('id', 0);
	    }

        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        if ($no_va = $request->no_va) {
            $records->where('no_va','like','%'.$no_va.'%');
        }

        if ($no_surat = $request->no_surat) {
            $records->whereHas('surat', function($surat) use ($no_surat){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                    $kaji->whereHas('pp', function($pp) use ($no_surat){
                        $pp->where('no_surat','like', '%'.$no_surat.'%');
                    });
                });
            });
        }
        if ($no_order = $request->no_order) {
            $records->whereHas('surat', function($surat) use ($no_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                    $kaji->whereHas('pp', function($pp) use ($no_order){
                        $pp->where('no_order','like', '%'.$no_order.'%');
                    });
                });
            });
        }
        if ($tanggal_order = $request->tanggal_order) {
            $records->whereHas('surat', function($surat) use ($tanggal_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                    $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                    });
                });
            });
        }
        if ($no_wbs = $request->no_wbs) {
            $records->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
            });
        }
        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $records->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                    $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                        $pp->where('layanan_id',$jenis_pelayanan_id);
                    });
                });
            });
        }
        if ($perusahaan_id = $request->perusahaan_id) {
            $records->whereHas('surat', function($surat) use ($perusahaan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                    $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                        $pp->whereHas('user', function($user) use ($perusahaan_id){
                            $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                $pelanggan->where('perusahaan_id',$perusahaan_id);
                            });
                        });
                    });
                });
            });
        }
        if ($bukti_upload = $request->bukti_upload) {
            if($bukti_upload==1){
                $records->whereNotNull('bukti_url');
            }
            if($bukti_upload==2){
                $records->whereNull('bukti_url');
            }
        }

        $records = $records->get();


        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('pemesan', function ($record) use ($request) {
                $pemesan = $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->no_hp;
                return $pemesan;
            })
            ->addColumn('layanan', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->surat->kaji_ulang->pp->detail){
                    $jenis_pengujian .= '<div class="ui ordered list list-more4" data-display="3">';
                    foreach ($record->surat->kaji_ulang->pp->detail as $key => $value) {
                        $kaji_ulang = KajiUlang::find($record->surat->kaji_ulang->id);
                        if($kaji_ulang){
                            foreach ($kaji_ulang->detail as $kuy => $val) {
                                if($val->jenis_id == $value->id){
                                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
                                }
                            }
                        }else{
                            $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
                        }
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('va', function($record){
            	if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0){
            		return '-';
            	}else{
                	return $record->no_va;
            	}
            })
            ->editColumn('nominal', function($record){
                return FormatNumber($record->nominal);
            })
            ->editColumn('dana_masuk', function($record){
                return ($record->konfirmasi) ? FormatNumber($record->konfirmasi->dana_masuk) : '-';
            })
            ->editColumn('wbs_io', function($record){
                return $record->konfirmasi->wbs_io ? $record->konfirmasi->wbs_io : '-';
            })
            ->editColumn('bukti', function($record){
            	$btn='';
                if($record->bukti_url){
                	$btn .= $this->makeButton([
		                    'tooltip'  => 'Preview Bukti Pembayaran',
		                    'class'  => 'teal icon lihat_data',
		                    'label'  => '<i class="file image outline icon"></i>',
		                    'type' => 'modal',
		                    'id'   => $record->id
		                ]);
            		$btn .= $this->makeButton([
            			'type' => 'url',
            			'tooltip' => 'Download Bukti Pembayaran',
            			'class' => 'ui blue mini icon button',
            			'label' => '<i class="download icon"></i>',
            			'url' => url('download-va', $record->id)
            		]);
            	}else{
            		$btn = '-';
            	}

                return $btn;
            })
            ->editColumn('tgl_bayar', function ($record) use ($request) {
            	if($record->tgl_bayar){
                    return DateToStringYear($record->tgl_bayar);
            	}else{
            		return '-';
            	}
            })
            ->editColumn('status', function($record){
                return '-';
            })
            ->editColumn('detil', function($record){
                $btn = '';
                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->surat_id.'/detil')
                ]);

                return $btn;
            })
            ->editColumn('status', function($record){
                $status = '<a class="ui green tag label">Terbayar</a>';
                return $status;
            })
            ->editColumn('checklist', function($record){
            	if(auth()->user()->hasRole(['keuangan'])){
                	$checklist = '<div class="ui fitted checkbox">
                            <input name="checklist[]" class="va" type="checkbox" data-id="'.$record->id.'">
                            <label></label>
                          </div>';
                }else{
                	$checklist='-';
                }
                return $checklist;
            })
            ->rawColumns(['pemesan','jenis_pengujian','action','detil','bukti','status','checklist','layanan','tgl_bayar'])
            ->make(true);
    }

    public function terbit(Request $request)
    {	
    	if(auth()->user()->hasRole(['keuangan', 'yan-uji', 'yan-kalibrasi','admin'])){
	        $records = VirtualAccount::where('status', 3)
	        						->where('tipe', 0)
	                                ->select('*');
	    }else{
	    	$records =  VirtualAccount::where('id', 0);
	    }

        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        if ($no_va = $request->no_va) {
            $records->where('no_va','like','%'.$no_va.'%');
        }

        if ($no_surat = $request->no_surat) {
            $records->whereHas('surat', function($surat) use ($no_surat){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                    $kaji->whereHas('pp', function($pp) use ($no_surat){
                        $pp->where('no_surat','like', '%'.$no_surat.'%');
                    });
                });
            });
        }
        if ($no_order = $request->no_order) {
            $records->whereHas('surat', function($surat) use ($no_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                    $kaji->whereHas('pp', function($pp) use ($no_order){
                        $pp->where('no_order','like', '%'.$no_order.'%');
                    });
                });
            });
        }
        if ($tanggal_order = $request->tanggal_order) {
            $records->whereHas('surat', function($surat) use ($tanggal_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                    $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                    });
                });
            });
        }
        if ($no_wbs = $request->no_wbs) {
            $records->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
            });
        }
        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $records->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                    $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                        $pp->where('layanan_id',$jenis_pelayanan_id);
                    });
                });
            });
        }
        if ($perusahaan_id = $request->perusahaan_id) {
            $records->whereHas('surat', function($surat) use ($perusahaan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                    $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                        $pp->whereHas('user', function($user) use ($perusahaan_id){
                            $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                $pelanggan->where('perusahaan_id',$perusahaan_id);
                            });
                        });
                    });
                });
            });
        }
        if ($bukti_upload = $request->bukti_upload) {
            if($bukti_upload==1){
                $records->whereNotNull('bukti_url');
            }
            if($bukti_upload==2){
                $records->whereNull('bukti_url');
            }
        }

        $records = $records->get();


        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->surat->kaji_ulang->pp->tgl_order);
            })
            ->addColumn('pemesan', function ($record) use ($request) {
                $pemesan = $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->no_hp;
                return $pemesan;
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->surat->kaji_ulang->pp->detail){
                    $jenis_pengujian .= '<div class="ui ordered list list-more5" data-display="3">';
                    foreach ($record->surat->kaji_ulang->pp->detail as $key => $value) {
                        $kaji_ulang = KajiUlang::find($record->surat->kaji_ulang->id);
                        if($kaji_ulang){
                            foreach ($kaji_ulang->detail as $kuy => $val) {
                                if($val->jenis_id == $value->id){
                                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
                                }
                            }
                        }else{
                            $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
                        }
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('va', function($record){
            	if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0){
            		return '-';
            	}else{
                	return $record->no_va;
            	}
            })
            ->editColumn('nominal', function($record){
                return FormatNumber($record->nominal);
            })
            ->editColumn('dana_masuk', function($record){
                return ($record->konfirmasi) ? FormatNumber($record->konfirmasi->dana_masuk) : '-';
            })
            ->editColumn('wbs_io', function($record){
                return $record->konfirmasi->wbs_io ? $record->konfirmasi->wbs_io : '-';
            })
            ->editColumn('bukti', function($record){
            	$btn='';
                if($record->bukti_url){
                	$btn .= $this->makeButton([
		                    'tooltip'  => 'Preview Bukti Pembayaran',
		                    'class'  => 'teal icon lihat_data',
		                    'label'  => '<i class="file image outline icon"></i>',
		                    'type' => 'modal',
		                    'id'   => $record->id
		                ]);
            		$btn .= $this->makeButton([
            			'type' => 'url',
            			'tooltip' => 'Download Bukti Pembayaran',
            			'class' => 'ui blue mini icon button',
            			'label' => '<i class="download icon"></i>',
            			'url' => url('download-va', $record->id)
            		]);
            	}else{
            		$btn = '-';
            	}

                return $btn;
            })
            ->editColumn('tgl_bayar', function ($record) use ($request) {
            	if($record->tgl_bayar){
	                // $tgl= Carbon::parse($record->tgl_bayar);
	                // return DateToStringWday($tgl).'</br>'.$tgl->format('H:i').' WIB';
                    return DateToStringYear($record->tgl_bayar);
            	}else{
            		return '-';
            	}
            })
            ->editColumn('detil', function($record){
                $btn = '';
                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->surat_id.'/detil')
                ]);

                return $btn;
            })
            ->editColumn('status', function($record){
            	if($record->konfirmasi){
            		if($record->konfirmasi->status == 1){
                		$status = '<a class="ui orange tag label">Kurang Bayar</a>';
            		}elseif ($record->konfirmasi->status == 2) {
                		$status = '<a class="ui blue tag label">Lebih Bayar</a>';
            		}elseif ($record->konfirmasi->status == 3) {
                		$status = '<a class="ui green tag label">Lunas</a>';
            		}else{
	            		$status = '-';
            		}
            	}else{
            		$status = '-';
            	}
                return $status;
            })
            ->editColumn('checklist', function($record){
            	if(auth()->user()->hasRole(['keuangan'])){
                	$checklist = '<div class="ui fitted checkbox">
                            <input name="checklist[]" class="va" type="checkbox" data-id="'.$record->id.'">
                            <label></label>
                          </div>';
                }else{
                	$checklist='-';
                }
                return $checklist;
            })
            ->rawColumns(['pemesan','jenis_pengujian','action','detil','bukti','status','checklist','layanan','tgl_bayar'])
            ->make(true);
    }

    public function histori(Request $request)
    {	
    	if(auth()->user()->hasRole(['keuangan', 'yan-uji', 'yan-kalibrasi','admin'])){
	        $records = VirtualAccount::where('status', 3)
	                                    ->whereHas('surat', function($q){
	                                        $q->whereHas('kaji_ulang',function($x){
	                                            $x->whereHas('pp',function($y){
	                                                return $y->where('status',6);
	                                            });
	                                        });
	                                    })
	                                    ->where('tipe', 0)
	                                    ->select('*');
	    }else{
	    	$records =  VirtualAccount::where('id', 0);
	    }
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        if ($no_va = $request->no_va) {
            $records->where('no_va','like','%'.$no_va.'%');
        }

        if ($no_surat = $request->no_surat) {
            $records->whereHas('surat', function($surat) use ($no_surat){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                    $kaji->whereHas('pp', function($pp) use ($no_surat){
                        $pp->where('no_surat','like', '%'.$no_surat.'%');
                    });
                });
            });
        }
        if ($no_order = $request->no_order) {
            $records->whereHas('surat', function($surat) use ($no_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                    $kaji->whereHas('pp', function($pp) use ($no_order){
                        $pp->where('no_order','like', '%'.$no_order.'%');
                    });
                });
            });
        }
        if ($tanggal_order = $request->tanggal_order) {
            $records->whereHas('surat', function($surat) use ($tanggal_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                    $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                    });
                });
            });
        }
        if ($no_wbs = $request->no_wbs) {
            $records->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
            });
        }
        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $records->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                    $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                        $pp->where('layanan_id',$jenis_pelayanan_id);
                    });
                });
            });
        }
        if ($perusahaan_id = $request->perusahaan_id) {
            $records->whereHas('surat', function($surat) use ($perusahaan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                    $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                        $pp->whereHas('user', function($user) use ($perusahaan_id){
                            $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                $pelanggan->where('perusahaan_id',$perusahaan_id);
                            });
                        });
                    });
                });
            });
        }
        if ($bukti_upload = $request->bukti_upload) {
            if($bukti_upload==1){
                $records->whereNotNull('bukti_url');
            }
            if($bukti_upload==2){
                $records->whereNull('bukti_url');
            }
        }

        $records = $records->get();
        

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->surat->kaji_ulang->pp->tgl_order);
            })
            ->addColumn('pemesan', function ($record) use ($request) {
                $pemesan = $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->no_hp;
                return $pemesan;
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->surat->kaji_ulang->pp->detail){
                    $jenis_pengujian .= '<div class="ui ordered list list-more6" data-display="3">';
                    foreach ($record->surat->kaji_ulang->pp->detail as $key => $value) {
                        $kaji_ulang = KajiUlang::find($record->surat->kaji_ulang->id);
                        if($kaji_ulang){
                            foreach ($kaji_ulang->detail as $kuy => $val) {
                                if($val->jenis_id == $value->id){
                                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
                                }
                            }
                        }else{
                            $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
                        }
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('va', function($record){
            	if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0){
            		return '-';
            	}else{
                	return $record->no_va;
            	}
            })
            ->editColumn('nominal', function($record){
                return FormatNumber($record->nominal);
            })
            ->editColumn('dana_masuk', function($record){
                return ($record->konfirmasi) ? FormatNumber($record->konfirmasi->dana_masuk) : '-';
            })
            ->editColumn('wbs_io', function($record){
                return $record->konfirmasi->wbs_io ? $record->konfirmasi->wbs_io : '-';
            })
            ->editColumn('bukti', function($record){
            	$btn='';
                if($record->bukti_url){
                	$btn .= $this->makeButton([
		                    'tooltip'  => 'Preview Bukti Pembayaran',
		                    'class'  => 'teal icon lihat_data',
		                    'label'  => '<i class="file image outline icon"></i>',
		                    'type' => 'modal',
		                    'id'   => $record->id
		                ]);
                    $btn .= $this->makeButton([
                        'type' => 'url',
                        'tooltip' => 'Download Bukti Pembayaran',
                        'class' => 'ui blue mini icon button',
                        'label' => '<i class="download icon"></i>',
                        'url' => url('download-va', $record->id)
                    ]);
                }else{
                    $btn = '-';
                }

                return $btn;
            })
            ->editColumn('tgl_bayar', function ($record) use ($request) {
                if($record->tgl_bayar){
                    // $tgl= Carbon::parse($record->tgl_bayar);
                    // return DateToStringWday($tgl).'</br>'.$tgl->format('H:i').' WIB';
                    return DateToStringYear($record->tgl_bayar);
                }else{
                    return '-';
                }
            })
            ->editColumn('detil', function($record){
                $btn = '';
                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->surat_id.'/detil')
                ]);

                return $btn;
            })
            ->editColumn('status', function($record){
                if($record->surat->kaji_ulang->pp){
                    if($record->surat->kaji_ulang->pp->status == 6){
                        $status = '<a class="ui blue tag label">Close Order</a>';
                    }else{
                        $status = '-';
                    }
                }else{
                    $status = '-';
                }
                return $status;
            })
            ->editColumn('checklist', function($record){
            	if(auth()->user()->hasRole(['keuangan'])){
                	$checklist = '<div class="ui fitted checkbox">
                            <input name="checklist[]" class="va" type="checkbox" data-id="'.$record->id.'">
                            <label></label>
                          </div>';
                }else{
                	$checklist='-';
                }
                return $checklist;
            })
            ->rawColumns(['pemesan','jenis_pengujian','action','detil','bukti','status','checklist','layanan','tgl_bayar'])
            ->make(true);
    }

    public function index()
    {
    	if($auth = auth()->user()->hasRole(['keuangan', 'yan-uji', 'yan-kalibrasi','admin'])){
	    	$satu = VirtualAccount::where('status', 1)
							        ->whereIn('tipe_customer', [0,1,2])
	                                ->doesnthave('konfirmasi')
	                                ->where('tipe', 0)
	                                ->get()->count();
	        $dua = VirtualAccount::where('status', 3)
	        						->whereIn('tipe_customer', [0,1,2])
	                                ->whereHas('konfirmasi', function($q){
	                                    return $q->where('status', 1);
	                                })
	                                ->where('tipe', 0)
	                                ->get()->count();
	        $tiga = VirtualAccount::where('status', 3)
	        						->whereIn('tipe_customer', [0,1,2])
	                                ->whereHas('konfirmasi', function($q){
	                                    return $q->where('status', 2);
	                                })
	                                ->where('tipe', 0)
	                                ->get()->count();
	        $empat = VirtualAccount::where('status', 3)
	        						->whereIn('tipe_customer', [0,1,2])
	                                ->whereHas('konfirmasi', function($q){
	                                    return $q->where('status', 3);
	                                })
	                                ->where('tipe', 0)
	                                ->get()->count();
	        $lima = VirtualAccount::where('status', 3)
	        						->whereIn('tipe_customer', [0,1,2])
	        						->where('tipe', 0)
	                                ->get()->count();
            $enam = VirtualAccount::where('status', 3)
                                    ->whereIn('tipe_customer', [0,1,2])
                                    ->whereHas('surat', function($q){
                                        $q->whereHas('kaji_ulang',function($x){
                                            $x->whereHas('pp',function($y){
                                                return $y->where('status',6);
                                            });
                                        });
                                    })
                                    ->where('tipe', 0)
                                    ->get()->count();
        }else{
	    	$satu 	= 0;
	        $dua 	= 0;
	        $tiga 	= 0;
	        $empat 	= 0;
	        $lima 	= 0;
            $enam   = 0;
	    }
        $data = [
            'mockup' 	=> true,
            'structs' 	=> $this->listStructs,
            'satu' 		=> $satu,
            'dua' 		=> $dua,
            'tiga' 		=> $tiga,
            'empat' 	=> $empat,
            'lima' 		=> $lima,
            'enam'      => $enam,
        ];
        return $this->render('modules.konfirmasi-pembayaran.index', $data);
    }

    public function create()
    {
        return $this->render('modules.konfirmasi-pembayaran.create');
    }

    public function store(Request $request)
    {
        $entry = array_filter($request['entry'], function($item){
            return !is_null($item['id_va']) && !is_null($item['tgl_bayar']);
        });

        foreach ($entry as $key => $row) {
            $record = VirtualAccount::find($row['id_va']);
            $record->tgl_bayar = $row['tgl_bayar'];
            if(!is_null($row['bukti'])){
                $path = $row['bukti']->store('uploads/konfirmasi-pembayaran', 'public');

                $record->bukti_url = $path;
                $record->bukti_filename = $row['bukti']->getClientOriginalName();
            }
            $record->status = 3; // 3 sudah bayar
            $record->save();
        }

        return response([
            'status' => true
        ]);
    }

    public function edit($id)
    {
        $record = VirtualAccount::find($id);

        return $this->render('modules.konfirmasi-pembayaran.edit', [
            'record' => $record
        ]);
    }

    public function ubahKurang($data)
    {
        $record = VirtualAccount::find($data);
        return $this->render('modules.konfirmasi-pembayaran.ubahKurang', [
            'record' => $record
        ]);
    }

    public function show($id)
    {
       return $this->render('modules.konfirmasi-pembayaran.detail');
    }

    public function update(KonfirmasiPembayaranRequest $request, $id)
    {
    	if($request->status == 0){
    		return response([
            'status' => true
        	]);
    	}else{
	        $record = VirtualAccount::find($id);
	        if ($record) {
	            $konfirmasi = new KonfirmasiPembayaran;
	            $konfirmasi->fill($request->all());
                $konfirmasi->dana_masuk = str_replace('.', '', $request->dana_masuk);
	            $record->konfirmasi()->save($konfirmasi);

	            $record->status = 3;
	            $record->save();

                $statusbayar = 'Lunas';
                if($record->status==1){
                    $statusbayar = 'Kurang Bayar';
                }else{
                    if($record->status==2){
                        $statusbayar = 'Lebih Bayar';
                    }
                }
                $keterangan = '';
                if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0){
                	$keterangan = 'menyelesaikan <a>Konfirmasi Pembayaran</a> dengan status <a>'.$statusbayar.'</a>, saat ini surat masuk ke tahap <a>Penerimaan Barang Uji</a>';
                }else{
                	$keterangan = 'menyelesaikan <a>Konfirmasi Pembayaran</a> No Va <a>'.$record->no_va.'</a> dengan status <a>'.$statusbayar.'</a>, saat ini surat masuk ke tahap <a>Penerimaan Barang Uji</a>';
                }
                $act = new ActDisposisi;
                $act->parent_id = $record->surat->kaji_ulang->pp->id;
                $act->pengirim_id = auth()->user()->id;
                $act->penerima_id = auth()->user()->id;
                $act->tipe = 7;
                $act->jenis = 7;
                $act->keterangan = $keterangan;
                $act->save();

                $user = [];

                if($record->surat->kaji_ulang->pp->jenis==0){
                    $user = User::whereHas('roles',function($query){
                        $query->where('name','yan-kalibrasi')
                                ->orWhere('name','adminlab-kalibrasi');
                    })->get();
                }else{
                    if($record->surat->kaji_ulang->pp->layanan_id==2){
                        $user = User::whereHas('roles',function($query){
                                    $query->where('name','yan-uji')
                                            ->orWhere('name','adminlab-siskit');
                                })->get();
                    }
                    if($record->surat->kaji_ulang->pp->layanan_id==3){
                        $user = User::whereHas('roles',function($query){
                                    $query->where('name','yan-uji')
                                            ->orWhere('name','adminlab-sistgi');
                                })->get();
                    }
                    if($record->surat->kaji_ulang->pp->layanan_id==4){
                        $user = User::whereHas('roles',function($query){
                                    $query->where('name','yan-uji')
                                            ->orWhere('name','adminlab-tegangan-rendah');
                                })->get();
                    }
                    if($record->surat->kaji_ulang->pp->layanan_id==5){
                        $user = User::whereHas('roles',function($query){
                                    $query->where('name','yan-uji')
                                            ->orWhere('name','adminlab-tegangan-tinggi');
                                })->get();
                    }
                }
                foreach ($user as $row) {
                    if(!is_null($row->device_id)){
                         $this->onesignal('Penerimaan Barang Uji Baru dengan nomor order '.$record->surat->kaji_ulang->pp->no_order,$row->device_id,$record->konfirmasi->toArray(),'penerimaan-barang-uji','baru');
                    }
                }

                if($record->konfirmasi){
                    if($record->konfirmasi->status==3){
                        $user = User::whereHas('roles',function($query){
                                    $query->where('name','keuangan');
                                })->get();

                        foreach ($user as $row) {
                            if(!is_null($row->device_id)){
                                 $this->onesignal('Realisasi Biaya Baru dengan nomor order '.$record->surat->kaji_ulang->pp->no_order,$row->device_id,$record->konfirmasi->toArray(),'realisasi-biaya','onprogress');
                            }
                        }
                    }

                    if($record->konfirmasi->status==2){
                        $user = User::whereHas('roles',function($query){
                                    $query->where('name','keuangan');
                                })->get();

                        foreach ($user as $row) {
                            if(!is_null($row->device_id)){
                                 $this->onesignal('Pengembalian Dana Baru dengan nomor order '.$record->surat->kaji_ulang->pp->no_order,$row->device_id,$record->konfirmasi->toArray(),'pengembalian-dana','lebih-bayar');
                            }
                        }
                    }
                }
	        }
    	}

        return response([
            'status' => true
        ]);
    }

    public function saveKurang(KurangBayarRequest $request, $id)
    {
    	if($request->status == 0){
    		return response([
            'status' => true
        	]);
    	}else{
    		$record = VirtualAccount::find($id);
	        if ($record) {
	            $konfirmasi = KonfirmasiPembayaran::find($record->konfirmasi->id);
	            $konfirmasi->dana_kurang = str_replace('.', '', $request->dana_kurang);
	            $konfirmasi->catatan     = $request->catatan;
	            $konfirmasi->status = 3;
	            $record->konfirmasi()->save($konfirmasi);

	            $record->status = 3;
	            $record->save();

	            $keterangan = '';

                if($record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kategori == 0){
                	$keterangan = 'menyelesaikan <a>Konfirmasi Pembayaran</a> dari status <a>Kurang Bayar</a> menjadi <a>Lunas</a>';
                }else{
                	$keterangan = 'menyelesaikan <a>Konfirmasi Pembayaran</a> No Va <a>'.$konfirmasi->va->no_va.'</a> dari status <a>Kurang Bayar</a> menjadi <a>Lunas</a>';
                }

                $act = new ActDisposisi;
	            $act->parent_id = $konfirmasi->va->surat->kaji_ulang->pp->id;
	            $act->pengirim_id = auth()->user()->id;
	            $act->penerima_id = auth()->user()->id;
	            $act->tipe = 7;
	            $act->jenis = 7;
	            $act->keterangan = $keterangan;
	            $act->save();
	        }
    	}

        return response([
            'status' => true
        ]);
    }

    public function buat($arr)
    {
        $list = explode(',', $arr);
        $records = VirtualAccount::whereIn('id', $list)->get();

        $this->setTitle("Buat Konfirmasi Pembayaran");
        $this->setBreadcrumb(['Buat' => '#']);
        return $this->render('modules.konfirmasi-pembayaran.create', [
            'data' => $records,
        ]);
    }

    public function detail($id)
    {
        $detail = Detail::find($id);
        $pp = PendaftaranPengujian::find($id);
        return $this->render('modules.konfirmasi-pembayaran.detail-pengujian',[
            'record' => $detail,
            'pp' => $pp,
        ]);
    }

    public function detil($id)
    {
    	$surat = SuratPenawaran::find($id);
       	$kaji_ulang = KajiUlang::find($surat->kaji_id);
       	$record = PendaftaranPengujian::whereHas('kaji_ulang', function($u){
    										$u->whereHas('surat');
    									})->where('id', $kaji_ulang->pp_id)->where('tipe_surat', 0)->get();
        $no_order = PendaftaranPengujian::whereHas('kaji_ulang', function($u){
    										$u->whereHas('surat');
    									})->where('id', $kaji_ulang->pp_id)->where('tipe_surat', 0)->pluck('no_order');
        $this->setTitle('Detil Konfirmasi Pembayaran');
        $this->setSubtitle('No Order : '.$record->first()->no_order);
	    $this->setBreadcrumb(['Konfirmasi Pembayaran' => '#', 'Detil' => '#']);
       	return $this->render('modules.konfirmasi-pembayaran.detail',[
       		'record' => $record->first(), 
       		'kaji_ulang' => $kaji_ulang,
        	'surat' => $surat,
        	'no_order' => $no_order,
   		]);
    }

    public function preview($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function lihat($id)
    {
    	$va 	= VirtualAccount::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$va->bukti_url)))
    	{
    		$link = asset('/storage/'.$va->bukti_url);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function previewMultiple($id, $type)
    {
    	if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $c->filename,
                        ];
                    }
                }
                return $this->render('pdf-multiple', [
		        	'mockup' => true,
		        	'data' => $rename,
		        ]);
            }
        }
    }

    public function download($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

    public function destroy($id)
    {
        $record = Role::find($id);
        $record->delete();

        return response([
            'status' => true,
        ]);
    }
}
