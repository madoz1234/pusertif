<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\Master\Contact;

use Hash;
use Auth;
use Session;
use Carbon;
use Lang;
use App\Models\Authentication\User;
use App\Models\Translators;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/backend/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        $jum = Contact::where('status', 1)->first();
        return $this->render('modules.authentication.login', [
            'jum' => $jum
        ]);
    }

    // custom login
    public function login(Request $request)
    {
        // Check validation
        $this->validate($request, [
            'username' => 'required',            
            'password' => 'required',            
        ]);

        $user = User::where('username', $request->username)
        ->orWhere('email', $request->username)->first();   

        // check user
        if ($user) {
            // check status user
            if ($user->status == 1) {
                // check password
                if (Hash::check($request->password, $user->password)) {
                    // set session login
                    $user->last_login = Carbon::now();
                    $user->save();
                    Auth::login($user);

                    if ($user->hasRole('user')) {
                        //set Translator for new user
                        $translator = Translators::where('created_by',$user->id)->first();
                        if (is_null($translator)) {
                            $default = new Translators;
                            $default->translator = 'id';
                            $default->created_by = $user->id;
                            $default->save();
                        }

                        return redirect(url('/frontend/pendaftaran-pengujian'));
                    } else {
                        return redirect(url('/backend'));
                    }
                }else{
                    Session::flash('message', trans('translator.Password Salah'));
                    return \Redirect::back();
                }
            } else {
                Session::flash('message', trans('translator.Username belum aktif'));
                return redirect()->back();
            }
        }

        // user not exist
        Session::flash('message', trans('translator.Username atau Password yang anda masukan tidak sesuai'));
        return redirect()->back();
    }

    public function logout()
    {
        if(Auth::check() && auth()->user()->id == true)
        {
            $default = Translators::where('created_by',auth()->user()->id)->first();
            if ($default) {
                $default->translator = 'id';
                $default->save();
            }
        }

        Auth::logout();
        return redirect('/');
    }

    public function username()
    {
        return 'username';
    }

    public function inject($user)
    {
        $user = User::where('username', $user)
                    ->first();
        if ($user) {
            Auth::login($user);
            // redirect
            return redirect(url('/backend'));
            
        } else {
            return 'T_T';
        }
    }
}
