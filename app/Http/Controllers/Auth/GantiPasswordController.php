<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Auth\ResetsPasswords;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GantiPasswordController extends Controller
{
    /*
     * Ensure the user is signed in to access this page
     */
    public function __construct() {

        $this->middleware('auth');

    }

    /**
     * Update the password for the user.
     *
     * @param  Request  $request
     * @return Response
     */
    // public function getgantipassword() {
    //     return view('auth.reset');
    // }
    public function getgantipassword(Request $request)
    {
        return view('auth.index');
    }
    
    public function postgantipassword(Request $request)
    {

        $oldPassword = $request->oldPassword;
        $newPassword = $request->newPassword;

        if(!Hash::check($oldPassword, Auth::User()->password)){
            // echo "password tidak benar";
            return redirect('/frontend/ganti-password')
                ->with('gagal', 'Password Lama Tidak Sama Dengan Password Sebelumnya');
            
        }else{
            $request->User()->fill(['password' => Hash::make($newPassword)])->save();
            // echo "done";
            return redirect('/frontend/ganti-password')
                ->with('success', 'Password Berhasil Diganti');
            // return redirect(url('/logout'));
        }
    }
}