<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Files;
use App\Models\Picture;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\Aduan;
use App\Models\Pengujian\Pengujian;


use Zipper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class DownloadController extends Controller
{

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index($id, $type)
    {
        // $check = Files::find($id);
        // if(file_exists(public_path('storage/'.$check->url)))
        // {
        //     return response()->download(storage_path('app/public/'.$check->url), $check->filename);
        // }

        // return response()->view('errors.download');
    }

    public function picture($id)
    {
        $check = Picture::find($id);
        if(file_exists(public_path('storage/'.$check->url)))
        {
            return response()->download(public_path('storage/'.$check->url), $check->filename);
        }

        return response()->view('errors.download');
    }

    public function multiple($id, $type)
    {
        if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        Storage::disk('public')->move($c->url, $newname.$c->filename);
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $newname.$c->filename,
                        ];
                    }
                }
                $now = Carbon::now()->format('Ymdhis');
                Zipper::make(public_path('storage/'.$type.$now.'.zip'))->add($files)->close();
                
                for ($i=0; $i < count($rename) ; $i++) { 
                    Storage::disk('public')->move($rename[$i]['new'], $rename[$i]['old']);
                }

                if(file_exists(public_path('storage/'.$type.$now.'.zip')))
                {
                    return response()->download(public_path('storage/'.$type.$now.'.zip'));
                }
                return response()->view('errors.download');
            }

            return response()->view('errors.download');
        }
    }

    public function multipleBriefing($id, $type, $jenis=null)
    {
        if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->where('type', $jenis)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        Storage::disk('public')->move($c->url, $newname.$c->filename);
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $newname.$c->filename,
                        ];
                    }
                }
                $now = Carbon::now()->format('Ymdhis');
                Zipper::make(public_path('storage/'.$type.$now.'.zip'))->add($files)->close();

                for ($i=0; $i < count($rename) ; $i++) { 
                    Storage::disk('public')->move($rename[$i]['new'], $rename[$i]['old']);
                }
                
                if(file_exists(public_path('storage/'.$type.$now.'.zip')))
                {
                    return response()->download(public_path('storage/'.$type.$now.'.zip'));
                }
                return response()->view('errors.download');
            }

            return response()->view('errors.download');
        }
    }

    public function laporanPengujian($id){
        $pengujian = Pengujian::find($id);
        $menu = 'laporan_pengujian_'.$pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order;
        $now = Carbon::now()->format('Ymdhis');
        Zipper::make(public_path('storage/'.$menu.'_'.$now.'.zip'))->close();
        foreach ($pengujian->laporan_pengujian as $laporan) {
            $pendukung = Files::where('target_id', $laporan->id)->where('target_type', 'laporan-pengujian')->where('type', 2)->get();
            $files = [];
            if($pendukung->count() > 0)
            {
                $rename = [];
                foreach($pendukung as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        Storage::disk('public')->move($c->url, $newname.$c->filename);
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $newname.$c->filename,
                        ];
                    }
                }
                $zipper2 = new \Chumper\Zipper\Zipper;
                $zipper2->zip(public_path('storage/'.$menu.'_'.$now.'.zip'))->folder($laporan->no_laporan)->add($files);
                $zipper2->close();

                for ($i=0; $i < count($rename) ; $i++) { 
                    Storage::disk('public')->move($rename[$i]['new'], $rename[$i]['old']);
                }
            }
        }

        if(file_exists(public_path('storage/'.$menu.'_'.$now.'.zip')))
        {
            return response()->download(public_path('storage/'.$menu.'_'.$now.'.zip'));
        }
        return response()->view('errors.download');

    }

    public function delete($id)
    {
        $file = Files::find($id);
        $file->delete();

        return $this->render('partials.file-tab.exist-file.lampiran', ['record' => $file->target]);
    }

    public function va($id)
    {
        $check = VirtualAccount::find($id);
        $files = [];
        $rename = [];        
        if($check->count() > 0)
        {
            if(file_exists(public_path('storage/'.$check->bukti_url)))
            {
                // $files[] = public_path('storage/'.$check->bukti_url);
                $newname = '';
                $splitname = explode("/",$check->bukti_url);
                for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                    $newname .= $splitname[$i].'/';
                }
                Storage::disk('public')->move($check->bukti_url, $newname.$check->bukti_filename);
                $files[] = public_path('storage/'.$newname.$check->bukti_filename);
                $rename[] = [
                    'old' => $check->bukti_url,
                    'new' => $newname.$check->bukti_filename,
                ];
            }

            $now = Carbon::now()->format('Ymdhis');

            Zipper::make(public_path('storage/'.$check->id.$now.'.zip'))->add($files)->close();
            
            for ($i=0; $i < count($rename) ; $i++) { 
                Storage::disk('public')->move($rename[$i]['new'], $rename[$i]['old']);
            }

            if(file_exists(public_path('storage/'.$check->id.$now.'.zip')))
            {
                return response()->download(public_path('storage/'.$check->id.$now.'.zip'));
            }
            return response()->view('errors.download');
        }
        return response()->view('errors.download');
    }

    public function tanggapan($id)
    {
        $check = Aduan::find($id);
        $files = [];
        $rename = [];        
        if($check->count() > 0)
        {
	    	if(file_exists(public_path('storage/'.$check->path)))
	    	{
	    		return response()->download(public_path('storage/'.$check->path), $check->tanggapan);
	    	}
	    	return response()->view('errors.download');
        }
        return response()->view('errors.download');
    }

    // public function deleteDaftarHadir($id)
    // {
    //     $file = DaftarHadir::find($id);
    //     $file->delete();

    //     return $this->render('partials.file-tab.exist-file.daftar-hadir', ['record' => $file->target]);
    // }

    // public function deletePicture($id)
    // {
    //     $file = Picture::find($id);
    //     $file->delete();

    //     return $this->render('partials.file-tab.exist-file.foto-rapat', ['record' => $file->target]);
    // }

    // public function deleteBeritaAcara($id)
    // {
    //     $data = RapatBeritaAcara::find($id);
    //     $data->delete();
    //     $file = FIles::where('target_id',$data->id)->where('target_type','rapat_berita_acara');
    //     $file->delete();

    //     // return $this->render('partials.file-tab.exist-file.foto-rapat', ['record' => $data->target]);
    // }
}
