<?php

namespace App\Http\Controllers\RealisasiBiaya;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\VirtualAccount\KonfirmasiPembayaran;
use App\Models\VirtualAccount\RealisasiBiaya;
use App\Models\VirtualAccount\RealisasiDetail;
use App\Models\KajiUlang\KajiUlang;
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\KajiUlang\Detail;
use App\Models\Act\ActDisposisi;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\Realisasi\RealisasiRequest;
/* Models */
use App\Models\Authentication\Role;
use App\Models\Picture;
use App\Models\Files;


/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Zipper;
use Excel;

class RealisasiBiayaController extends Controller
{
    protected $link = 'realisasi/realisasi-biaya/';
    protected $perms = 'realisasi-biaya';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Realisasi Biaya");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Realisasi Biaya' => '#']);

        // Header Grid Datatable
        $this->listStructs = [
		    'listStruct' => [
		        [
		            'data' => 'num',
		            'name' => 'num',
		            'label' => '#',
		            'orderable' => false,
		            'searchable' => false,
		            'className' => "center aligned",
		            'width' => '40px',
		        ],
		        [
		            'data' => 'no_order',
		            'name' => 'no_order',
		            'label' => 'No Order',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		            'width' => '100px',
		        ],
		        [
		            'data' => 'tgl_order',
		            'name' => 'tgl_order',
		            'label' => 'Tgl Order',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		            'width' => '100px',
		        ],
		        [
		            'data' => 'no_surat',
		            'name' => 'no_surat',
		            'label' => 'No Surat Permintaan',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		            'width' => '100px',
		        ],
		        [
		            'data' => 'layanan_lingkup',
		            'name' => 'layanan_lingkup',
		            'label' => 'Layanan / Lingkup',
		            'className' => "left aligned",
		            'searchable' => false,
		            'sortable' => true,
		            'width' => '200px',
		        ],
		        [
		            'data' => 'pemesan',
		            'name' => 'pemesan',
		            'label' => 'Peminta Jasa',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		            'width' => '200px',
		        ],
		        [
		            'data' => 'wbs_io',
		            'name' => 'wbs_io',
		            'label' => 'No WBS / IO',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		        ],
		        [
		            'data' => 'nominal',
		            'name' => 'nominal',
		            'label' => 'Nominal Pembayaran (Rp)',
		            'className' => "right aligned",
		            'searchable' => false,
		            'sortable' => true,
		        ],
		        [
		            'data' => 'tgl_bayar',
		            'name' => 'tgl_bayar',
		            'label' => 'Tgl Pembayaran',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		        ],
		        [
		            'data' => 'realisasi',
		            'name' => 'realisasi',
		            'label' => 'Realisasi Biaya Berjalan',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		        ],
		        [
		            'data' => 'periode',
		            'name' => 'periode',
		            'label' => 'Periode Update',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		        ],
		        [
		            'data' => 'detil',
		            'name' => 'detil',
		            'label' => 'Detil',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => false,
		        ],
		        // [
          //           'data' => 'action',
          //           'name' => 'action',
          //           'label' => 'Aksi',
          //           'searchable' => false,
          //           'sortable' => false,
          //           'className' => "center aligned",
          //           'width' => '50px',
          //       ],
		        [
		            'data' => 'status',
		            'name' => 'status',
		            'label' => 'Status',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		        ],
            ],
            'listStruct2' => [
				[
		            'data' => 'num',
		            'name' => 'num',
		            'label' => '#',
		            'orderable' => false,
		            'searchable' => false,
		            'className' => "center aligned",
		            'width' => '40px',
		        ],
		        [
		            'data' => 'no_order',
		            'name' => 'no_order',
		            'label' => 'No Order',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		            'width' => '100px',
		        ],
		        [
		            'data' => 'tgl_order',
		            'name' => 'tgl_order',
		            'label' => 'Tgl Order',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		            'width' => '100px',
		        ],
		        [
		            'data' => 'no_surat',
		            'name' => 'no_surat',
		            'label' => 'No Surat Permintaan',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		            'width' => '100px',
		        ],
		        [
		            'data' => 'layanan_lingkup',
		            'name' => 'layanan_lingkup',
		            'label' => 'Layanan / Lingkup',
		            'className' => "left aligned",
		            'searchable' => false,
		            'sortable' => true,
		            'width' => '200px',
		        ],
		        [
		            'data' => 'pemesan',
		            'name' => 'pemesan',
		            'label' => 'Peminta Jasa',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		            'width' => '200px',
		        ],
		        [
		            'data' => 'wbs_io',
		            'name' => 'wbs_io',
		            'label' => 'No WBS / IO',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		        ],
		        [
		            'data' => 'nominal',
		            'name' => 'nominal',
		            'label' => 'Nominal Pembayaran (Rp)',
		            'className' => "right aligned",
		            'searchable' => false,
		            'sortable' => true,
		        ],
		        [
		            'data' => 'tgl_bayar',
		            'name' => 'tgl_bayar',
		            'label' => 'Tgl Pembayaran',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		        ],
		        [
		            'data' => 'realisasi',
		            'name' => 'realisasi',
		            'label' => 'Realisasi Biaya',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		        ],
		        [
		            'data' => 'periode',
		            'name' => 'periode',
		            'label' => 'Periode Update',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		        ],
		        [
		            'data' => 'detil',
		            'name' => 'detil',
		            'label' => 'Detil',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => false,
		        ],
		        // [
          //           'data' => 'action',
          //           'name' => 'action',
          //           'label' => 'Aksi',
          //           'searchable' => false,
          //           'sortable' => false,
          //           'className' => "center aligned",
          //           'width' => '50px',
          //       ],
		        [
		            'data' => 'status',
		            'name' => 'status',
		            'label' => 'Status',
		            'className' => "center aligned",
		            'searchable' => false,
		            'sortable' => true,
		        ],
            ],
        ];
    }

    public function grid(Request $request)
    {
    	if(auth()->user()->hasRole(['keuangan','admin'])){
	        $records = KonfirmasiPembayaran::where('status', 3)
	        						->whereHas('penerimaan', function($penerimaan){
	        							$penerimaan->doesnthave('closeorder');
	        						})
	        						->where('tipe', 0)
	        						->orDoesntHave('penerimaan')
	        						->select('*');
	  	}else{
	  		$records = KonfirmasiPembayaran::where('id', 0);
	  	}
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        if($no_order = $request->no_order){
        	$records->whereHas('va', function($va) use ($no_order){
        		$va->whereHas('surat', function($surat) use ($no_order){
        			$surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
        				$kaji->whereHas('pp', function($pp) use($no_order){
        					$pp->where('no_order','like','%'.$no_order.'%');
        				});
        			});
        		});
        	});
        }

        if($no_surat = $request->no_surat){
        	$records->whereHas('va', function($va) use ($no_surat){
        		$va->whereHas('surat', function($surat) use ($no_surat){
        			$surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
        				$kaji->whereHas('pp', function($pp) use($no_surat){
        					$pp->where('no_surat','like','%'.$no_surat.'%');
        				});
        			});
        		});
        	});
        }

        if($tanggal_order = $request->tanggal_order){
        	$records->whereHas('va', function($va) use ($tanggal_order){
        		$va->whereHas('surat', function($surat) use ($tanggal_order){
        			$surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
        				$kaji->whereHas('pp', function($pp) use($tanggal_order){
        					$pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
        				});
        			});
        		});
        	});
        }

        if($jenis_pelayanan_id = $request->jenis_pelayanan_id){
        	$records->whereHas('va', function($va) use ($jenis_pelayanan_id){
        		$va->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
        			$surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
        				$kaji->whereHas('pp', function($pp) use($jenis_pelayanan_id){
        					$pp->where('layanan_id',$jenis_pelayanan_id);
        				});
        			});
        		});
        	});
        }

        if($perusahaan_id = $request->perusahaan_id){
        	$records->whereHas('va', function($va) use ($perusahaan_id){
        		$va->whereHas('surat', function($surat) use ($perusahaan_id){
        			$surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
        				$kaji->whereHas('pp', function($pp) use($perusahaan_id){
        					$pp->whereHas('user', function($user) use ($perusahaan_id){
			                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
			                          $pelanggan->where('perusahaan_id',$perusahaan_id);
			                    });
				            });
        				});
        			});
        		});
        	});
        }

        if($wbs_io = $request->wbs_io){
        	$records->where("wbs_io",'like','%'.$wbs_io.'%');
        }

        $records = $records->get();

        //Filters
        $link = $this->link;
        return Datatables::of($records)
	        ->addColumn('num', function ($record) use ($request) {
	             return $request->get('start');
	        })
	        ->editColumn('no_order', function ($record) use ($request) {
	        	return $record->va->surat->kaji_ulang->pp->no_order;
	        })
	        ->editColumn('tgl_order', function ($record) use ($request) {
	        	return DateToStringYear($record->va->surat->kaji_ulang->pp->tgl_order);
	        })
	        ->addColumn('pemesan', function ($record) use ($request) {
	        	$pemesan = $record->va->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$record->va->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$record->va->surat->kaji_ulang->pp->user->pelanggans->no_hp;
	        	return $pemesan;
	        })
	        ->editColumn('no_surat', function ($record) use ($request) {
	        	return $record->va->surat->kaji_ulang->pp->no_surat;
	        })
	        ->editColumn('realisasi', function ($record) use ($request) {
	        	$nilai =0;
	        	if($record->realisasi){
	        		foreach ($record->realisasi->realisasi_detail as $key => $value) {
	        			$nilai += $value->jumlah;
	        		}
	        	}else{
					return $nilai;
	        	}
	        	return FormatNumber($nilai);
	        })
	        ->addColumn('layanan_lingkup', function ($record) use ($request) {
	        	return $record->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->va->surat->kaji_ulang->pp->lingkup->nama;
	        })
	        ->editColumn('wbs_io', function ($record) use ($request) {
	        	return $record->wbs_io;
	        })
	        ->editColumn('no_va', function ($record) use ($request) {
	        	return $record->va->no_va;
	        })
	        ->editColumn('nominal', function ($record) use ($request) {
	        	return formatNumber($record->va->nominal);
	        })
	        ->editColumn('periode', function ($record) use ($request) {
	        	if($record->realisasi){
	        		if($record->realisasi->realisasi_detail){
	        			if($record->realisasi->realisasi_detail->last()){
			        		return BulanToString($record->realisasi->realisasi_detail->last()->periode);
	        			}else{
	        				return '-';
	        			}
	        		}else{
						return '-';
	        		}
	        	}else{
					return '-';
	        	}
	        })
	        ->editColumn('tgl_bayar', function ($record) use ($request) {
	        	return DateToStringYear($record->created_at);
	        })
	        ->editColumn('status', function ($record) use ($request) {
	        	return '-';
	        })
	        ->editColumn('detil', function($record){
	        	$btn = '';

	        	$btn .= $this->makeButton([
	        		'type' => 'url',
	        		'tooltip' => 'Detil Data',
	        		'class' => 'ui teal mini icon button',
	        		'label' => '<i class="eye icon"></i>',
	        		'url' => url($this->link.$record->va->surat_id.'/detil')
	        	]);

	        	return $btn;
	        })
	        ->addColumn('action', function ($record) use ($link){
	        	$btn = $this->makeButton([
	        		'type' => 'modal',
	        		'class'   => 'blue icon upload-file',
	        		'label'   => '<i class="upload text icon"></i>',
	        		'tooltip' => 'Buat Realisasi Biaya',
	        		'datas' => [
	        			'id' => $record->id
	        		],
	        		'id'   => $record->id
	        	]);
	        	return $btn;
            })
	        ->rawColumns(['pemesan','layanan_lingkup','jenis_pengujian','detil','action'])
	        ->make(true);
    }

    public function histori(Request $request)
    {	
    	if(auth()->user()->hasRole(['keuangan','admin'])){
	        $records = KonfirmasiPembayaran::where('status', 3)
	        								->whereHas('penerimaan',function($penerimaan){
	        									$penerimaan->has('closeorder');
	        								})
	        								->where('tipe', 0)
	        								->select('*');
	  	}else{
	  		$records = KonfirmasiPembayaran::where('id', 0);
	  	}
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        if($no_order = $request->no_order){
        	$records->whereHas('va', function($va) use ($no_order){
        		$va->whereHas('surat', function($surat) use ($no_order){
        			$surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
        				$kaji->whereHas('pp', function($pp) use($no_order){
        					$pp->where('no_order','like','%'.$no_order.'%');
        				});
        			});
        		});
        	});
        }

        if($no_surat = $request->no_surat){
        	$records->whereHas('va', function($va) use ($no_surat){
        		$va->whereHas('surat', function($surat) use ($no_surat){
        			$surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
        				$kaji->whereHas('pp', function($pp) use($no_surat){
        					$pp->where('no_surat','like','%'.$no_surat.'%');
        				});
        			});
        		});
        	});
        }

        if($tanggal_order = $request->tanggal_order){
        	$records->whereHas('va', function($va) use ($tanggal_order){
        		$va->whereHas('surat', function($surat) use ($tanggal_order){
        			$surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
        				$kaji->whereHas('pp', function($pp) use($tanggal_order){
        					$pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
        				});
        			});
        		});
        	});
        }

        if($jenis_pelayanan_id = $request->jenis_pelayanan_id){
        	$records->whereHas('va', function($va) use ($jenis_pelayanan_id){
        		$va->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
        			$surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
        				$kaji->whereHas('pp', function($pp) use($jenis_pelayanan_id){
        					$pp->where('layanan_id',$jenis_pelayanan_id);
        				});
        			});
        		});
        	});
        }

        if($perusahaan_id = $request->perusahaan_id){
        	$records->whereHas('va', function($va) use ($perusahaan_id){
        		$va->whereHas('surat', function($surat) use ($perusahaan_id){
        			$surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
        				$kaji->whereHas('pp', function($pp) use($perusahaan_id){
        					$pp->whereHas('user', function($user) use ($perusahaan_id){
			                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
			                          $pelanggan->where('perusahaan_id',$perusahaan_id);
			                    });
				            });
        				});
        			});
        		});
        	});
        }

        if($wbs_io = $request->wbs_io){
        	$records->where("wbs_io",'like','%'.$wbs_io.'%');
        }

        $records = $records->get();


        // $records =[];
        //Filters
        $link = $this->link;
        return Datatables::of($records)
	        ->addColumn('num', function ($record) use ($request) {
	             return $request->get('start');
	        })
	        ->editColumn('no_order', function ($record) use ($request) {
	        	return $record->va->surat->kaji_ulang->pp->no_order;
	        })
	        ->editColumn('tgl_order', function ($record) use ($request) {
	        	return DateToStringYear($record->va->surat->kaji_ulang->pp->tgl_order);
	        })
	        ->addColumn('pemesan', function ($record) use ($request) {
	        	$pemesan = $record->va->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$record->va->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$record->va->surat->kaji_ulang->pp->user->pelanggans->no_hp;
	        	return $pemesan;
	        })
	        ->editColumn('no_surat', function ($record) use ($request) {
	        	return $record->va->surat->kaji_ulang->pp->no_surat;
	        })
	        ->addColumn('layanan_lingkup', function ($record) use ($request) {
	        	return $record->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->va->surat->kaji_ulang->pp->lingkup->nama;
	        })
	        ->editColumn('wbs_io', function ($record) use ($request) {
	        	return $record->wbs_io;
	        })
	        ->editColumn('realisasi', function ($record) use ($request) {
	        	$nilai =0;
	        	if($record->realisasi){
	        		foreach ($record->realisasi->realisasi_detail as $key => $value) {
	        			$nilai += $value->jumlah;
	        		}
	        	}else{
					return $nilai;
	        	}
	        	return FormatNumber($nilai);
	        })
	        ->editColumn('nominal', function ($record) use ($request) {
	        	return formatNumber($record->va->nominal);
	        })
	        ->editColumn('periode', function ($record) use ($request) {
	        	if($record->realisasi){
	        		if($record->realisasi->realisasi_detail){
	        			if($record->realisasi->realisasi_detail->last()){
			        		return BulanToString($record->realisasi->realisasi_detail->last()->periode);
	        			}else{
	        				return '-';
	        			}
	        		}else{
						return '-';
	        		}
	        	}else{
					return '-';
	        	}
	        })
	        ->editColumn('tgl_bayar', function ($record) use ($request) {
	        	return DateToStringYear($record->created_at);
	        })
	        ->editColumn('status', function ($record) use ($request) {
	        	return '-';
	        })
	        ->editColumn('detil', function($record){
	        	$btn = '';

	        	$btn .= $this->makeButton([
	        		'type' => 'url',
	        		'tooltip' => 'Detil Data',
	        		'class' => 'ui teal mini icon button',
	        		'label' => '<i class="eye icon"></i>',
	        		'url' => url($this->link.$record->va->surat_id.'/detil')
	        	]);

	        	return $btn;
	        })
	        ->addColumn('action', function ($record) use ($link){
	        	$btn = '';
	        	if($record->realisasi){
	                $btn = $this->makeButton([
		        		'type' => 'modal',
		        		'class'   => 'blue icon upload-ulang',
		        		'label'   => '<i class="upload text icon"></i>',
		        		'tooltip' => 'Buat Ulang Realisasi Biaya',
		        		'datas' => [
		        			'id' => $record->realisasi->id
		        		],
		        		'id'   => $record->realisasi->id
		        	]);
	        	}
	        	return $btn;
            })
	        ->rawColumns(['pemesan','layanan_lingkup','jenis_pengujian','detil','action'])
	        ->make(true);
    }

    public function index()
    {	
    	if(auth()->user()->hasRole(['keuangan','admin'])){
	    	$satu = KonfirmasiPembayaran::where('status', 3)
	        						->whereHas('penerimaan', function($penerimaan){
	        							$penerimaan->doesnthave('closeorder');
	        						})
	        						->where('tipe', 0)
	        						->orDoesntHave('penerimaan')
	        						->get()->count();
	  	}else{
	  		$satu = KonfirmasiPembayaran::where('id', 0)->get()->count();
	  	}
     	// $dua  = KonfirmasiPembayaran::where('status', 3)
      //   								->whereHas('realisasi')
      //   								->get()->count();
        // $dua  = 0;
    	$data = [
            'mockup' 	=> true,
            'structs' 	=> $this->listStructs,
            'satu' 		=> $satu,
            // 'dua' 		=> $dua,
        ];
        return $this->render('modules.realisasi-biaya.index', $data);
    }

    // public function detail($id)
    // {
    //     $detail = Detail::find($id);
    //     $pp = PendaftaranPengujian::find($id);
    //     return $this->render('modules.realisasi-biaya.detail-pengujian',[
    //         'record' => $detail,
    //         'pp' => $pp,
    //     ]);
    // }

    public function detil($id)
    {
       	$surat = SuratPenawaran::find($id);
       	$kaji_ulang = KajiUlang::find($surat->kaji_id);
       	$record = PendaftaranPengujian::whereHas('kaji_ulang', function($u){
    										$u->whereHas('surat');
    									})->where('id', $kaji_ulang->pp_id)->get();
        $no_order = PendaftaranPengujian::whereHas('kaji_ulang', function($u){
    										$u->whereHas('surat');
    									})->where('id', $kaji_ulang->pp_id)->pluck('no_order');
        $this->setTitle('Detil Realisasi Biaya');
        $this->setSubtitle('No Order : '.$record->first()->no_order);
	    $this->setBreadcrumb(['Realisasi Biaya' => '#', 'Detil' => '#']);
       	return $this->render('modules.realisasi-biaya.detail',[
       		'record' => $record->first(), 
       		'kaji_ulang' => $kaji_ulang,
        	'surat' => $surat,
        	'no_order' => $no_order,
   		]);
    }

    public function upload($id)
    {
        return $this->render('modules.realisasi-biaya.upload', [
            'id' => $id,
        ]);
    }

    public function uploadTemplate()
    {
        return $this->render('modules.realisasi-biaya.upload-template');
    }

    public function uploadTemplateSave(Request $request)
    {
    	$request->validate([
		    'data_upload' => 'required',
		    'periode' => 'required',
		]);

		$failupload = 0;
    	if($request->hasFile('data_upload'))
    	{
    		$path = $request->file('data_upload')->getRealPath();
    		$data = Excel::load($path, function($reader) {})->get();
			$periode = Carbon::createFromFormat('Y-m',$request->periode)->format('Y-m');
			foreach ($data as $key => $value) {
				if($value->start!='' || $value->end!='' || $value->biaya!='' || $value->no_wbs!=''){
				$simpan = false;
				$ftgl = false;
				$start = '';
				$end = '';
				try {
					$start = Carbon::createFromFormat('dmY',$value->start)->format('Y-m-d');
					$end = Carbon::createFromFormat('dmY',$value->end)->format('Y-m-d');
					$ftgl = true;
				} catch (\Exception $e) {
					$ftgl = false;
				}

				if($ftgl==true){
					if((Carbon::parse($start)->format('Y-m') == $periode) && (Carbon::parse($end)->format('Y-m') == $periode)){
						$konfirmasi = KonfirmasiPembayaran::where('wbs_io',$value->no_wbs)->where('tipe', 0)->first();
						if($konfirmasi){
							if(!$konfirmasi->realisasi){
								$row = $request->all();
								$row['start'] = $start;
								$row['end'] = $end;

								$pathr = null;
								$filenamer = null;

								if($filer = $request->data_upload){
					        		$pathr = $filer->store('realisasi-biaya', 'public');
					        		$filenamer = $filer->getClientOriginalName();
					            }

								$realisasi = new RealisasiBiaya;
								$realisasi->konfirmasi_id = $konfirmasi->id;
								$realisasi->status = 1;
								$realisasi->save();
								
								$realisasidetail = new RealisasiDetail;
								$realisasidetail->realisasi_id = $realisasi->id;
					    		$realisasidetail->periode 		= $row['periode'];
					            $realisasidetail->start        = $row['start'];
					            $realisasidetail->end          = $row['end'];
						        $realisasidetail->jumlah 		= $value->biaya;
					    		$realisasidetail->file_upload 	= $filenamer;
					    		$realisasidetail->path 		= $pathr;
						        $realisasidetail->tgl_upload 	= Carbon::now()->format('Y-m-d');
						        $realisasidetail->save();

						        $simpan = true;

						        $logperiode = explode(' ',DateToStringYear($periode.'-01'));

						        $act = new ActDisposisi;
					    		$act->parent_id = $konfirmasi->va->surat->kaji_ulang->pp->id;
					    		$act->pengirim_id = auth()->user()->id;
					    		$act->penerima_id = auth()->user()->id;
					    		$act->tipe = 9;
					    		$act->jenis = 9;
					    		$act->keterangan = 'membuat <a>Realisasi Biaya</a> Periode <a>'.$logperiode[1].' '.$logperiode[2].'</a>';
					    		$act->save();

								// $realisasi->saveDetail($row, $value->biaya);
							}else{
								$realisasi = RealisasiBiaya::where('konfirmasi_id',$konfirmasi->id)->first();
								if($realisasi){
									$row = $request->all();
									$row['start'] = $start;
									$row['end'] = $end;

									$pathr = null;
									$filenamer = null;

									if($filer = $request->data_upload){
						        		$pathr = $filer->store('realisasi-biaya', 'public');
						        		$filenamer = $filer->getClientOriginalName();
						            }

									$realisasidetail = new RealisasiDetail;
									$realisasidetail->realisasi_id = $realisasi->id;
						    		$realisasidetail->periode 		= $row['periode'];
						            $realisasidetail->start        = $row['start'];
						            $realisasidetail->end          = $row['end'];
							        $realisasidetail->jumlah 		= $value->biaya;
						    		$realisasidetail->file_upload 	= $filenamer;
						    		$realisasidetail->path 		= $pathr;
							        $realisasidetail->tgl_upload 	= Carbon::now()->format('Y-m-d');
							        $realisasidetail->save();

							        $simpan = true;

							        $logperiode = explode(' ',DateToStringYear($periode.'-01'));

							        $act = new ActDisposisi;
						    		$act->parent_id = $konfirmasi->va->surat->kaji_ulang->pp->id;
						    		$act->pengirim_id = auth()->user()->id;
						    		$act->penerima_id = auth()->user()->id;
						    		$act->tipe = 9;
						    		$act->jenis = 9;
						    		$act->keterangan = 'membuat <a>Realisasi Biaya</a> Periode <a>'.$logperiode[1].' '.$logperiode[2].'</a>';
						    		$act->save();
								}
							}
						}
					}
				}

				if($simpan==false){
					$failupload = $failupload+1;
				}
				}
			}
    	}

		return response([
          'status' => true,
          'fail' => $failupload
        ]);
    }

    public function saveUpload(RealisasiRequest $request, $id)
    {
    	if($request->hasFile('data_upload')){
    		$path = $request->file('data_upload')->getRealPath();
    		$data = Excel::load($path, function($reader) {})->get();
    		$insert = array();
    		$y=0;
    		$z=0;
    		$biaya =0;
    		if(!empty($data) && $data->count()){
    			foreach ($data as $key => $value) {
    				$cek = is_string($value->biaya) ? 'true' : 'false';
    				$konfirmasi = KonfirmasiPembayaran::find($id);
    				if(!is_null($value->no_wbs) && !is_null($value->biaya)){
    					if($konfirmasi->wbs_io == $value->no_wbs){
    						if($cek == 'false'){
    							$biaya +=$value->biaya;
 							}else{
 								$y +=1;
 							}
 						}else{
 							$z +=1;
 						}
    				}
    			}
    			if($biaya > 0){
					DB::beginTransaction();
					try {
						$cari_realisasi = RealisasiBiaya::where('konfirmasi_id', $id)->first();
						if($cari_realisasi){
							$realisasi = RealisasiBiaya::find($cari_realisasi->id);
							$realisasi->konfirmasi_id = $id;
							$realisasi->status = 1;
							$realisasi->save();
							$realisasi->saveDetail($request->all(), $biaya);
						}else{
							$realisasi = new RealisasiBiaya;
							$realisasi->konfirmasi_id = $id;
							$realisasi->status = 1;
							$realisasi->save();
							$realisasi->saveDetail($request->all(), $biaya);
						}
						DB::commit();
					}catch (\Exception $e) {
						DB::rollback();
						return response([
							'status' => 'error',
							'message' => 'An error occurred!',
							'error' => $e->getMessage(),
						], 500);
					}
    			}else{
    				return response([
    					'status' => false,
    					'message' => 'Data Tidak Sesuai',
    					'data' => '',
    				]);
    			}

    			if($z > 0){
    				return response([
			            'status' => false,
			            'message' => 'Ada '.$z.' data WBS/IO yang tidak sesuai',
			        ]);
    			}elseif($y > 0){
    				return response([
			            'status' => false,
			            'message' => 'Ada '.$z.' data Satuan yang tidak sesuai',
			        ]);
    			}else{
    				return response([
			            'status' => true,
			            'message' => '',
			        ]);	
    			}
    		}else{
    			return response([
    				'status' => false,
    				'message' => 'Tidak Ada Data',
    				'data' => '',
    			]);
    		}
		}else{
			return response([
				'status' => false,
				'message' => 'Inputan tidak boleh kosong',
				'data' => '',
			]);
		}
    }

    public function uploadUlang($id)
    {
        return $this->render('modules.realisasi-biaya.upload-ulang', [
            'id' => $id,
        ]);
    }

    public function saveUploadUlang(Request $request, $id)
    {
    	DB::beginTransaction();
        try {
        	$realisasi = RealisasiBiaya::find($id);
        	$realisasi->status = 3;
        	$realisasi->save();
        	$realisasi->multipleFileUpload($request->data_upload);
        	// $realisasi->save();
            DB::commit();
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true
        ]);   
    }

    public function preview($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function previewMultiple($id, $type)
    {
    	if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $c->filename,
                        ];
                    }
                }
                return $this->render('pdf-multiple', [
		        	'mockup' => true,
		        	'data' => $rename,
		        ]);
            }
        }
    }

    public function download($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

    public function exportTemplate(Request $request)
    {	
    	$Export = Excel::create('Realisasi Biaya', function($excel){
            $excel->setTitle('Realisasi Biaya');
            $excel->setCreator('E-Uji dan E-Kal')->setCompany('E-Pusertif');
            $excel->setDescription('Export');
            $excel->sheet('List Realisasi Biaya ', function($sheet){
                $sheet->row(1, array(
                	'START',
	                'END',
                    'No WBS',
                    'BIAYA',
                ));
                $sheet->setBorder('A1:D1', 'thin');
                $sheet->cells('A1:D1', function($cells){
                		$cells->setBackground('#999999');
                    	$cells->setFontColor('#FFFFFF');
                    	$cells->setAlignment('center');
                });
                // $sheet->mergeCells("F1:H1");
                $sheet->SetCellValue("F1", 'Catatan :');
                $sheet->cells("F1", function($cells){
                    $cells->setFontSize(10);
                });
                $sheet->SetCellValue("F2", 'Mohon No WBS sesuai dengan data yang dipilih');
                $sheet->cells("F2", function($cells){
                    $cells->setFontSize(10);
                });
                $sheet->SetCellValue("F3", 'Format START dan END "DDMMYYYY", example : 19072019');
                $sheet->cells("F3", function($cells){
                    $cells->setFontSize(10);
                });
                $sheet->cells('C1:C300', function($cells){
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });

                $sheet->setColumnFormat(array(
				    'A' => '@',
				    'B' => '@',
				    'D' => '0',
				));
                $sheet->setWidth(array('A'=>20,'B'=>20,'C'=>20,'D'=>40,'F'=>40,'G'=>40,'H'=>40));
            });
        });
        $Export ->download('xls');
    }
}
