<?php

namespace App\Http\Controllers\PengembalianDana;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\VirtualAccount\KonfirmasiPembayaran;
use App\Models\VirtualAccount\RealisasiBiaya;
use App\Models\VirtualAccount\PengembalianDana;
use App\Models\KajiUlang\KajiUlang;
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\PenerimaanBarang\PenerimaanBarang;
use App\Models\CloseOrder\CloseOrder;
use App\Models\KajiUlang\Detail;
use App\Models\PengembalianDanaBarang\PengembalianDanaBarang;
use App\Models\Act\ActPengujian;

/* Validation */
// use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\VirtualAccount\PengembalianDanaRequest;
use App\Http\Requests\VirtualAccount\DanaBarangRequest;
/* Models */
use App\Models\Authentication\Role;
use App\Models\Files;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Zipper;

class PengembalianDanaController extends Controller
{
    protected $link = 'pengembalian-dana/';
    protected $perms = 'pengembalian-dana';
    protected $listStructs = [];

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Pengembalian Dana");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Pengembalian Dana' => '#']);

        // Header Grid Datatable
        $this->listStructs = [
            'listStruct' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '200px',
                ],
                [
                    'data' => 'pemesan',
                    'name' => 'pemesan',
                    'label' => 'Peminta Jasa',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '200px',
                ],
                [
                    'data' => 'no_va',
                    'name' => 'no_va',
                    'label' => 'No VA',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'nominal',
                    'name' => 'nominal',
                    'label' => 'Nominal VA (Rp)',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'dana_masuk',
                    'name' => 'dana_masuk',
                    'label' => 'Dana Masuk (Rp)',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'tgl_bayar',
                    'name' => 'tgl_bayar',
                    'label' => 'Tgl Dana Masuk',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => false,
                ],
                [
                    'data' => 'timestamp',
                    'name' => 'timestamp',
                    'label' => 'Time Stamp',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '150px',
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '50px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
            ],
            'listStruct2' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '200px',
                ],
                [
                    'data' => 'pemesan',
                    'name' => 'pemesan',
                    'label' => 'Peminta Jasa',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                    'width' => '200px',
                ],
                [
                    'data' => 'no_va',
                    'name' => 'no_va',
                    'label' => 'No VA',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'nominal',
                    'name' => 'nominal',
                    'label' => 'Nominal VA (Rp)',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'dana_masuk',
                    'name' => 'dana_masuk',
                    'label' => 'Dana Masuk (Rp)',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'tgl_bayar',
                    'name' => 'tgl_bayar',
                    'label' => 'Tgl Dana Masuk',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => false,
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '50px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'className' => "center aligned",
                    'searchable' => false,
                    'sortable' => true,
                ],
            ],
            'listStruct3' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian dan Merk',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
            ],
            'listStruct4' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian dan Merk',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
            ],
        ];
    }

    public function grid(Request $request)
    {
    	$user = auth()->user();
    	if($user->hasRole(['keuangan','admin'])){
	        $records = VirtualAccount::where('status', 3)
	                                ->whereHas('konfirmasi',function($q) {
	                                    $q->where('status',2);
	                                })
	                                ->where('tipe', 0)
	                                ->select('*');
	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
	   }else{
	   		$records= VirtualAccount::where('id',0)->select('*');
	   }
        //Init Sort

        if ($no_va = $request->no_va) {
            $records->where('no_va','like','%'.$no_va.'%');
        }

        if ($no_surat = $request->no_surat) {
            $records->whereHas('surat', function($surat) use ($no_surat){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                    $kaji->whereHas('pp', function($pp) use ($no_surat){
                        $pp->where('no_surat','like', '%'.$no_surat.'%');
                    });
                });
            });
        }
        if ($no_order = $request->no_order) {
            $records->whereHas('surat', function($surat) use ($no_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                    $kaji->whereHas('pp', function($pp) use ($no_order){
                        $pp->where('no_order','like', '%'.$no_order.'%');
                    });
                });
            });
        }
        if ($tanggal_order = $request->tanggal_order) {
            $records->whereHas('surat', function($surat) use ($tanggal_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                    $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                    });
                });
            });
        }
        if ($no_wbs = $request->no_wbs) {
            $records->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
            });
        }
        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $records->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                    $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                        $pp->where('layanan_id',$jenis_pelayanan_id);
                    });
                });
            });
        }
        if ($perusahaan_id = $request->perusahaan_id) {
            $records->whereHas('surat', function($surat) use ($perusahaan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                    $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                        $pp->whereHas('user', function($user) use ($perusahaan_id){
                            $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                $pelanggan->where('perusahaan_id',$perusahaan_id);
                            });
                        });
                    });
                });
            });
        }

        $records = $records->get();


        //Filters
        $link = $this->link;
        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                 return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('pemesan', function ($record) use ($request) {
                $pemesan = $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->no_hp;
                return $pemesan;
            })
            ->editColumn('no_va', function ($record) use ($request) {
                return $record->no_va;
            })
            ->editColumn('wbs_io', function ($record) use ($request) {
                return ($record->konfirmasi) ? $record->konfirmasi->wbs_io : '-';
            })
            ->editColumn('nominal', function ($record) use ($request) {
                return formatNumber($record->nominal);
            })
            ->editColumn('dana_masuk', function ($record) use ($request) {
                return ($record->konfirmasi) ? FormatNumber($record->konfirmasi->dana_masuk) : '-';
            })
            ->editColumn('tgl_bayar', function ($record) use ($request) {
            	if($record->tgl_bayar){
	                $tgl = Carbon::createFromFormat('Y-m-d H:i:s', $record->tgl_bayar);
	                return DateToStringYear($tgl->format('Y-m-d'));
            	}else{
            		return '-';
            	}
            })
            ->editColumn('detil', function($record){
                $btn = '';

                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->surat_id.'/detil')
                ]);

                return $btn;
            })
            ->editColumn('timestamp', function ($record) use ($request) {
                return $record->tgl_bayar;
            })
           ->addColumn('action', function ($record) use ($link){
                $btn='-';
                if(auth()->user()->hasRole(['keuangan'])){
                    if($record->konfirmasi->pengembalian){
                        if($record->konfirmasi->pengembalian->tgl_kembali){
                            $btn='-';
                        }else{
                            $btn = $this->makeButton([
                                'type' => 'modal',
                                'class'   => 'blue icon pengembalian',
                                'label'   => '<i class="edit text icon"></i>',
                                'tooltip' => 'Buat Pengembalian Dana',
                                'datas' => [
                                    'id' => $record->id
                                ],
                                'id'   => $record->id
                            ]);
                        }
                    }else{
                       $btn='-'; 
                    }
                }elseif(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi'])){
                    if ($record->konfirmasi->pengembalian) {
                        $btn ='-';
                    }else{
                        $btn = $this->makeButton([
                            'type' => 'modal',
                            'class'   => 'blue icon pengembalian',
                            'label'   => '<i class="edit text icon"></i>',
                            'tooltip' => 'Buat Pengembalian Dana',
                            'datas' => [
                                'id' => $record->id
                            ],
                            'id'   => $record->id
                        ]);
                    }
                }else{
                    $btn ='-';
                }
                return $btn;
            })
            ->editColumn('status', function ($record) use ($request) {
                return '-';
            })
            ->rawColumns(['pemesan','layanan_lingkup','jenis_pengujian','detil','action','timestamp'])
            ->make(true);
    }

    public function histori(Request $request)
    {
    	$user = auth()->user();
		if($user->hasRole(['keuangan','admin'])){
		   	$records = VirtualAccount::where('status', 3)
                                ->whereHas('konfirmasi',function($q) {
                                    $q->whereHas('pengembalian',function($x) {
                                            $x->whereNotNull('tgl_kembali');
                                        });
                                })
                                ->where('tipe', 0)
                                ->select('*');
		    if (!isset(request()->order[0]['column'])) {
		        $records->orderBy('created_at', 'desc');
		    }
		}else{
            $records= VirtualAccount::where('id',0)->select('*');

		}

        if ($no_va = $request->no_va) {
            $records->where('no_va','like','%'.$no_va.'%');
        }

        if ($no_surat = $request->no_surat) {
            $records->whereHas('surat', function($surat) use ($no_surat){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                    $kaji->whereHas('pp', function($pp) use ($no_surat){
                        $pp->where('no_surat','like', '%'.$no_surat.'%');
                    });
                });
            });
        }
        if ($no_order = $request->no_order) {
            $records->whereHas('surat', function($surat) use ($no_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                    $kaji->whereHas('pp', function($pp) use ($no_order){
                        $pp->where('no_order','like', '%'.$no_order.'%');
                    });
                });
            });
        }
        if ($tanggal_order = $request->tanggal_order) {
            $records->whereHas('surat', function($surat) use ($tanggal_order){
                $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                    $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                    });
                });
            });
        }
        if ($no_wbs = $request->no_wbs) {
            $records->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
            });
        }
        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $records->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                    $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                        $pp->where('layanan_id',$jenis_pelayanan_id);
                    });
                });
            });
        }
        if ($perusahaan_id = $request->perusahaan_id) {
            $records->whereHas('surat', function($surat) use ($perusahaan_id){
                $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                    $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                        $pp->whereHas('user', function($user) use ($perusahaan_id){
                            $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                $pelanggan->where('perusahaan_id',$perusahaan_id);
                            });
                        });
                    });
                });
            });
        }
        $records = $records->get();
         //Filters
        $link = $this->link;
        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                 return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->surat->kaji_ulang->pp->tgl_order);
            })
            ->addColumn('pemesan', function ($record) use ($request) {
                $pemesan = $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->no_hp;
                return $pemesan;
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->editColumn('wbs_io', function ($record) use ($request) {
                return ($record->konfirmasi) ? $record->konfirmasi->wbs_io : '-';
            })
            ->editColumn('no_va', function ($record) use ($request) {
                return $record->no_va;
            })
            ->editColumn('nominal', function ($record) use ($request) {
                return formatNumber($record->nominal);
            })
            ->editColumn('dana_masuk', function ($record) use ($request) {
                return ($record->konfirmasi) ? FormatNumber($record->konfirmasi->dana_masuk) : '-';
            })
            ->editColumn('tgl_bayar', function ($record) use ($request) {
            	if($record->tgl_bayar){
	                $tgl = Carbon::createFromFormat('Y-m-d H:i:s', $record->tgl_bayar);
	                return DateToStringYear($tgl->format('Y-m-d'));
            	}else{
            		return '-';
            	}
            })
            ->editColumn('status', function ($record) use ($request) {
                $status = '<a class="ui green tag label">Refund</a>';
                return $status;
            })
            ->editColumn('detil', function($record){
                $btn = '';

                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->surat_id.'/detil')
                ]);

                return $btn;
            })
            ->addColumn('action', function ($record) use ($link){
                $btn = $this->makeButton([
                    'type' => 'modal',
                    'class'   => 'blue icon detailPengembalian',
                    'label'   => '<i class="eye text icon"></i>',
                    'tooltip' => 'Lihat Pengembalian Dana',
                    'datas' => [
                        'id' => $record->id
                    ],
                    'id'   => $record->id
                ]);
                return $btn;
            })
            ->rawColumns(['pemesan','layanan_lingkup','jenis_pengujian','detil','action', 'status'])
            ->make(true);
    }

    public function kembali(Request $request)
    {
    	$user = auth()->user();
    	if($user->hasRole(['keuangan','admin'])){
		    	$records = CloseOrder::whereHas('penerimaan.pengujian.detailpengujian', function($u){
		    								return $u->where('verifikasi', '!=', 1);
		    						  })
		    						  ->doesntHave('pengembaliandana')
		    						  ->select('*');
		        if (!isset(request()->order[0]['column'])) {
		            $records->orderBy('created_at', 'desc');
		        }
		}else{
            $records= CloseOrder::where('id',0)->select('*');

		}

        if ($no_va = $request->no_va) {
            $records->whereHas('penerimaan', function($penerimaan) use($no_va){
                $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($no_va){
                    $konfirmasi->whereHas('va', function($va) use ($no_va){
                        $va->where('no_va','like','%'.$no_va.'%');
                    });
                });
            });
        }

        if ($no_surat = $request->no_surat) {
            $records->whereHas('penerimaan', function($penerimaan) use($no_surat){
                $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($no_surat){
                    $konfirmasi->whereHas('va', function($va) use ($no_surat){
                        $va->whereHas('surat', function($surat) use ($no_surat){
                            $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                                $kaji->whereHas('pp', function($pp) use ($no_surat){
                                    $pp->where('no_surat','like', '%'.$no_surat.'%');
                                });
                            });
                        });
                    });
                });
            });
        }
        if ($no_order = $request->no_order) {
            $records->whereHas('penerimaan', function($penerimaan) use($no_order){
                $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($no_order){
                    $konfirmasi->whereHas('va', function($va) use ($no_order){
                        $va->whereHas('surat', function($surat) use ($no_order){
                            $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                                $kaji->whereHas('pp', function($pp) use ($no_order){
                                    $pp->where('no_order','like', '%'.$no_order.'%');
                                });
                            });
                        });
                    });
                });
            });
        }
        if ($tanggal_order = $request->tanggal_order) {
            $records->whereHas('penerimaan', function($penerimaan) use($tanggal_order){
                $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($tanggal_order){
                    $konfirmasi->whereHas('va', function($va) use ($tanggal_order){
                        $va->whereHas('surat', function($surat) use ($tanggal_order){
                            $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                                $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                });
                            });
                        });
                    });
                });
            });
        }
        if ($no_wbs = $request->no_wbs) {
            $records->whereHas('penerimaan', function($penerimaan) use($no_wbs){
                $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                    $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                });
            });
        }
        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $records->whereHas('penerimaan', function($penerimaan) use($jenis_pelayanan_id){
                $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($jenis_pelayanan_id){
                    $konfirmasi->whereHas('va', function($va) use ($jenis_pelayanan_id){
                        $va->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                            $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                                $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                                    $pp->where('layanan_id',$jenis_pelayanan_id);
                                });
                            });
                        });
                    });
                });
            });
        }
        if ($perusahaan_id = $request->perusahaan_id) {
            $records->whereHas('penerimaan', function($penerimaan) use($perusahaan_id){
                $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($perusahaan_id){
                    $konfirmasi->whereHas('va', function($va) use ($perusahaan_id){
                        $va->whereHas('surat', function($surat) use ($perusahaan_id){
                            $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                                $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                                    $pp->whereHas('user', function($user) use ($perusahaan_id){
                                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                            $pelanggan->where('perusahaan_id',$perusahaan_id);
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        }
        $records = $records->get();

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->detail){
                    $jenis_pengujian .= '<div class="ui ordered list list-more4" data-display="3">';
                    foreach ($record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->detail as $key => $value) {
                        $kaji_ulang = KajiUlang::find($record->penerimaan->konfirmasi->va->surat->kaji_ulang->id);
                        if($kaji_ulang){
                            foreach ($kaji_ulang->detail as $kuy => $val) {
                                if($val->jenis_id == $value->id){
                                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
                                }
                            }
                        }else{
                            $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-id="0">'.$value->jenis->nama.'</a></div>';
                        }
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('wbs_io', function($record){
                return $record->penerimaan->konfirmasi->wbs_io;
            })
            ->editColumn('detil', function($record){
                $btn = '';
        		$btn .= $this->makeButton([
        			'type' => 'url',
        			'tooltip' => 'Detil',
        			'class' => 'teal icon button detil',
        			'label' => '<i class="eye icon"></i>',
        			'url' => url($this->link.$record->penerimaan->id.'/detail-data')
        		]);
                return $btn;
            })
            ->addColumn('action', function ($record) use ($link){
            	$btn = '';
        		$btn .= $this->makeButton([
        			'type' => 'modal',
        			'tooltip' => 'Buat Pengembalian Dana',
        			'class' => 'blue icon button buat-pengembalian',
        			'label' => '<i class="edit icon"></i>',
        			'data' => [
        				'id' => $record->id,
        			],
        			'id'   => $record->id
        		]);
                return $btn;
            })
            ->rawColumns(['action','status','detil','jenis_pengujian','layanan_lingkup','penerimaan','pengujian'])
            ->make(true);
    }

    public function historiKembali(Request $request)
    {
    	$user = auth()->user();
    	if($user->hasRole(['keuangan','admin'])){
	    	$records = CloseOrder::whereHas('penerimaan.pengujian.detailpengujian', function($u){
	    								return $u->where('verifikasi', '!=', 1);
	    						  })
	    						  ->has('pengembaliandana')
	    						  ->select('*');
	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }
	     }else{
            $records= CloseOrder::where('id',0)->select('*');
	     }

        if ($no_va = $request->no_va) {
            $records->whereHas('penerimaan', function($penerimaan) use($no_va){
                $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($no_va){
                    $konfirmasi->whereHas('va', function($va) use ($no_va){
                        $va->where('no_va','like','%'.$no_va.'%');
                    });
                });
            });
        }

        if ($no_surat = $request->no_surat) {
            $records->whereHas('penerimaan', function($penerimaan) use($no_surat){
                $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($no_surat){
                    $konfirmasi->whereHas('va', function($va) use ($no_surat){
                        $va->whereHas('surat', function($surat) use ($no_surat){
                            $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                                $kaji->whereHas('pp', function($pp) use ($no_surat){
                                    $pp->where('no_surat','like', '%'.$no_surat.'%');
                                });
                            });
                        });
                    });
                });
            });
        }
        if ($no_order = $request->no_order) {
            $records->whereHas('penerimaan', function($penerimaan) use($no_order){
                $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($no_order){
                    $konfirmasi->whereHas('va', function($va) use ($no_order){
                        $va->whereHas('surat', function($surat) use ($no_order){
                            $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                                $kaji->whereHas('pp', function($pp) use ($no_order){
                                    $pp->where('no_order','like', '%'.$no_order.'%');
                                });
                            });
                        });
                    });
                });
            });
        }
        if ($tanggal_order = $request->tanggal_order) {
            $records->whereHas('penerimaan', function($penerimaan) use($tanggal_order){
                $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($tanggal_order){
                    $konfirmasi->whereHas('va', function($va) use ($tanggal_order){
                        $va->whereHas('surat', function($surat) use ($tanggal_order){
                            $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                                $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                });
                            });
                        });
                    });
                });
            });
        }
        if ($no_wbs = $request->no_wbs) {
            $records->whereHas('penerimaan', function($penerimaan) use($no_wbs){
                $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($no_wbs){
                    $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                });
            });
        }
        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $records->whereHas('penerimaan', function($penerimaan) use($jenis_pelayanan_id){
                $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($jenis_pelayanan_id){
                    $konfirmasi->whereHas('va', function($va) use ($jenis_pelayanan_id){
                        $va->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                            $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                                $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                                    $pp->where('layanan_id',$jenis_pelayanan_id);
                                });
                            });
                        });
                    });
                });
            });
        }
        if ($perusahaan_id = $request->perusahaan_id) {
            $records->whereHas('penerimaan', function($penerimaan) use($perusahaan_id){
                $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($perusahaan_id){
                    $konfirmasi->whereHas('va', function($va) use ($perusahaan_id){
                        $va->whereHas('surat', function($surat) use ($perusahaan_id){
                            $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                                $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                                    $pp->whereHas('user', function($user) use ($perusahaan_id){
                                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                            $pelanggan->where('perusahaan_id',$perusahaan_id);
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        }
        $records = $records->get();

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->detail){
                    $jenis_pengujian .= '<div class="ui ordered list list-more4" data-display="3">';
                    foreach ($record->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->detail as $key => $value) {
                        $kaji_ulang = KajiUlang::find($record->penerimaan->konfirmasi->va->surat->kaji_ulang->id);
                        if($kaji_ulang){
                            foreach ($kaji_ulang->detail as $kuy => $val) {
                                if($val->jenis_id == $value->id){
                                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
                                }
                            }
                        }else{
                            $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-id="0">'.$value->jenis->nama.'</a></div>';
                        }
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('wbs_io', function($record){
                return $record->penerimaan->konfirmasi->wbs_io;
            })
            ->editColumn('detil', function($record){
                $btn = '';
        		$btn .= $this->makeButton([
        			'type' => 'url',
        			'tooltip' => 'Detil',
        			'class' => 'teal icon button detil',
        			'label' => '<i class="eye icon"></i>',
        			'url' => url($this->link.$record->penerimaan->id.'/detail-data')
        		]);
                return $btn;
            })
            ->addColumn('action', function ($record) use ($link){
            	$btn = '';
        		$btn .= $this->makeButton([
        			'type' => 'modal',
        			'tooltip' => 'Buat Pengembalian Dana',
        			'class' => 'blue icon button buat-pengembalian',
        			'label' => '<i class="edit icon"></i>',
        			'data' => [
        				'id' => $record->id,
        			],
        			'id'   => $record->id
        		]);
                return $btn;
            })
            ->rawColumns(['action','status','detil','jenis_pengujian','layanan_lingkup','penerimaan','pengujian'])
            ->make(true);
    }

    public function index()
    {
    	$user = auth()->user();
		if($user->hasRole(['keuangan','admin'])){
	        $satu = VirtualAccount::where('status', 3)
	                                ->whereHas('konfirmasi',function($q) {
	                                    $q->whereHas('pengembalian',function($x) {
	                                            $x->whereNull('tgl_kembali');
	                                        })->where('status',2);
	                                })
	                                ->where('tipe', 0)
	                                ->get()->count();

	        $dua =	CloseOrder::whereHas('penerimaan.pengujian.detailpengujian', function($u){
										return $u->where('verifikasi', '!=', 1);
									})
									->doesntHave('pengembaliandana')
									->get()->count();
		}else{
			$satu ='';
			$dua ='';
		}
        $data = [
            'mockup'    => true,
            'structs'   => $this->listStructs,
            'satu'      => $satu,
            'dua'       => $dua,
        ];
        return $this->render('modules.pengembalian-dana.index', $data);
    }

    public function create()
    {
        return $this->render('modules.pengembalian-dana.create');
    }

    public function store(RolesRequest $request)
    {
        $record = new Role;
        $record->fill($request->all());
        $record->save();

        return response([
            'status' => true
        ]);
    }

    public function edit($id)
    {
        return $this->render('modules.pengembalian-dana.edit');
    }

    public function show($id)
    {
       return $this->render('modules.pengembalian-dana.detail');
    }

    public function update(PengembalianDanaRequest $request, $id)
    {
        $record = new PengembalianDana;
        $record->konfirmasi_id = $id;
        $record->fill($request->all());
        $record->save();

        return response([
            'status' => true
        ]);
    }
    public function detil($id)
    {
        $surat = SuratPenawaran::find($id);
        $kaji_ulang = KajiUlang::find($surat->kaji_id);
        $record = PendaftaranPengujian::whereHas('kaji_ulang', function($u){
                                            $u->whereHas('surat');
                                        })->where('id', $kaji_ulang->pp_id)->where('tipe_surat', 0)->get();
        $no_order = PendaftaranPengujian::whereHas('kaji_ulang', function($u){
                                            $u->whereHas('surat');
                                        })->where('id', $kaji_ulang->pp_id)->where('tipe_surat', 0)->pluck('no_order');
        $this->setTitle('Detil Pengembalian Dana');
        $this->setSubtitle('No Order : '.$record->first()->no_order);
        $this->setBreadcrumb(['Pengembalian Dana' => '#', 'Detil' => '#']);
        return $this->render('modules.pengembalian-dana.detail',[
            'record' => $record->first(), 
            'kaji_ulang' => $kaji_ulang,
            'surat' => $surat,
            'no_order' => $no_order,
        ]);
    }
    public function pengembalian($id)
    {
        $record = KonfirmasiPembayaran::where('va_id', $id)->first();
        return $this->render('modules.pengembalian-dana.pengembalian', [
            'record' => $record
        ]);
    }

    public function buatPengembalian($id)
    {
    	$data = CloseOrder::find($id);
        return $this->render('modules.pengembalian-dana.pengembalian-dana-barang', [
            'record' => $data,
        ]);
    }

    public function detailPengembalian($id)
    {
        $record = KonfirmasiPembayaran::where('va_id', $id)->first();
        return $this->render('modules.pengembalian-dana.detail-pengembalian', [
            'record' => $record
        ]);
    }

    public function detail($id)
    {
        $penerimaan = PenerimaanBarang::find($id);
        $layanan = $penerimaan->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama;
        if($penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id == 1){
	       	$this->setTitle('Detail Close Order Kalibrasi');
	       	$this->setSubtitle('No WBS/IO : '.$penerimaan->konfirmasi->wbs_io);
	        $this->setBreadcrumb(['Close Order' => '#', 'Close Order Detail' => '#']);
	        return $this->render('modules.pengembalian-dana.detail-kalibrasi',[
	        	'record' => $penerimaan,
	    	]);
        }else{
        	$this->setTitle('Detail Close Order '.$layanan);
	       	$this->setSubtitle('No WBS/IO : '.$penerimaan->konfirmasi->wbs_io);
	        $this->setBreadcrumb(['Close Order' => '#', 'Close Order Detail' => '#']);
	        return $this->render('modules.pengembalian-dana.detail-uji',[
	        	'record' => $penerimaan,
	    	]);
        }
    }

    public function savePengembalian(PengembalianDanaRequest $request, $id)
    {   
    	$user = auth()->user();
        $pengembalian = PengembalianDana::where('konfirmasi_id', $id)->first();
        if($pengembalian){
            $pengembalian->fill($request->all());
            if ($request->jumlah_dana) {
                $pengembalian->jumlah_dana = str_replace('.', '', $request->jumlah_dana);
            }
            if ($pengembalian->save()) {
                if ($pengembalian->tgl_kembali != null) {
                    $konfirmasi = KonfirmasiPembayaran::find($pengembalian->konfirmasi_id);
                    $konfirmasi->status = 3;
                    $konfirmasi->save();
                }
            }

            $act = new ActPengujian;
    		$act->parent_id = $pengembalian->konfirmasi->va->surat->kaji_ulang->pp->id;
    		$act->pengirim_id = auth()->user()->id;
    		$act->penerima_id = 1;
    		$act->tipe = 2;
    		$act->jenis = 2;
    		$act->keterangan = '<a>'.$user->roles[0]->display_name.'</a> mengembalikan Dana : <a>'.$request->jumlah_dana.'</a>, No Wbs : <a>'.$pengembalian->konfirmasi->wbs_io.'</a>';
    		$act->save();
        }else{
            $pengembalian_dana = new PengembalianDana;
            $pengembalian_dana->konfirmasi_id = $id;
            $pengembalian_dana->fill($request->all());
            if ($request->jumlah_dana) {
                $pengembalian_dana->jumlah_dana = str_replace('.', '', $request->jumlah_dana);
            }
            $pengembalian_dana->save();

            $act = new ActPengujian;
    		$act->parent_id = $pengembalian_dana->konfirmasi->va->surat->kaji_ulang->pp->id;
    		$act->pengirim_id = auth()->user()->id;
    		$act->penerima_id = 1;
    		$act->tipe = 2;
    		$act->jenis = 2;
    		$act->keterangan = '<a>'.$user->roles[0]->display_name.'</a> mengembalikan Dana : <a>'.$request->jumlah_dana.'</a>, No Wbs : <a>'.$pengembalian_dana->konfirmasi->wbs_io.'</a>';
    		$act->save();
        }

        return response([
          'status' => true
        ]);   
    }

    public function preview($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function previewMultiple($id, $type)
    {
    	if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $c->filename,
                        ];
                    }
                }
                return $this->render('pdf-multiple', [
		        	'mockup' => true,
		        	'data' => $rename,
		        ]);
            }
        }
    }

    public function download($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

    public function savePengembalianDanaBarang(DanaBarangRequest $request, $id)
    {
    	DB::beginTransaction();
    	try {
    		$dana_barang = new PengembalianDanaBarang;
    		$dana_barang->close_id = $id;
    		$dana_barang->no_nota = $request->no_nota;
    		$dana_barang->tgl_nota = $request->tgl_nota;
    		$dana_barang->dana_kembali = str_replace('.', '', $request->dana_kembali);
    		$dana_barang->save();
    		$dana_barang->saveDetail($id);
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }
}
