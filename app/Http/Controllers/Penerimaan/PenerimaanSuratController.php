<?php

namespace App\Http\Controllers\Penerimaan;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\Penerimaan\DisposisiRequest;
use App\Http\Requests\Penerimaan\PenerimaanRequest;
use App\Http\Requests\FrontEnd\CekItemUjiRequest;
use App\Http\Requests\Permintaan\PermintaanPengujianRequest;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;


/* Models */
use App\Models\Authentication\Role;
use App\Models\Authentication\User;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\FrontEnd\PendaftaranPengujianDetail;
use App\Models\Master\Pelanggan;
use App\Models\KajiUlang\KajiUlang;
// use App\Models\PenerimaanSurat\PenerimaanSurat;
use App\Models\Act\ActDisposisi;
use App\Models\Users;
use App\Models\Picture;
use App\Models\Files;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;

class PenerimaanSuratController extends Controller
{
    protected $link = 'penerimaan/surat/';
    protected $perms = 'penerimaan-surat';
    protected $listStructs = [];

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Penerimaan Surat");
        $this->setPerms($this->perms);
        $this->setModalSize("tiny");
        $this->setBreadcrumb(['Penerimaan Surat' => '#']);

        // Header Grid Datatable
        $this->data = [
            'pageUrl' => $this->link,
        ];

        $this->listStructs = [
            'listStruct' => [
            	[
            		'data' => 'num',
            		'name' => 'num',
            		'label' => '#',
            		'orderable' => false,
            		'searchable' => false,
            		'className' => "center aligned",
            		'width' => '40px',
            	],
            	/* --------------------------- */
            	[
            		'data' => 'no_order',
            		'name' => 'no_order',
            		'label' => 'No Order',
            		'className' => "center aligned",
            		'searchable' => false,
            		'sortable' => true,
            		'width' => '150px',
            	],
            	[
            		'data' => 'tgl_order',
            		'name' => 'tgl_order',
            		'label' => 'Tgl Order',
            		'className' => "center aligned",
            		'searchable' => false,
            		'sortable' => true,
            		'width' => '150px',
            	],
            	[
            		'data' => 'no_surat',
            		'name' => 'no_surat',
            		'label' => 'No Surat Permintaan',
            		'className' => "center aligned",
            		'searchable' => false,
            		'sortable' => true,
            		'width' => '150px',
            	],
            	[
            		'data' => 'layanan',
            		'name' => 'layanan',
            		'label' => 'Layanan',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '200px',
            	],
            	[
            		'data' => 'lingkup',
            		'name' => 'lingkup',
            		'label' => 'Lingkup',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '200px',
            	],
            	[
            		'data' => 'jenis_pengujian',
            		'name' => 'jenis_pengujian',
            		'label' => 'Jenis Pengujian',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "left aligned",
            		'width' => '200px',
            	],
            	[
            		'data' => 'pemesan',
            		'name' => 'pemesan',
            		'label' => 'Peminta Jasa',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '300px',
            	],
            	[
        			'data' => 'timestamp',
        			'name' => 'timestamp',
        			'label' => 'Time Stamp',
        			'searchable' => false,
        			'sortable' => true,
        			'className' => "center aligned timestamp",
        			'width' => '120px',
        		],
            	[
            		'data' => 'detil',
            		'name' => 'detil',
            		'label' => 'Detil',
            		'searchable' => false,
            		'sortable' => false,
            		'className' => "center aligned",
            		'width' => '50px',
            	],
            	[
            		'data' => 'action',
            		'name' => 'action',
            		'label' => 'Aksi',
            		'searchable' => false,
            		'sortable' => false,
            		'className' => "center aligned",
            		'width' => '150px',
            	],
            	[
            		'data' => 'status',
            		'name' => 'status',
            		'label' => 'Status',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '150px',
            	],
            ],
            'listStruct2' => [
            	[
            		'data' => 'num',
            		'name' => 'num',
            		'label' => '#',
            		'orderable' => false,
            		'searchable' => false,
            		'className' => "center aligned",
            		'width' => '40px',
            	],
            	/* --------------------------- */
            	[
            		'data' => 'no_order',
            		'name' => 'no_order',
            		'label' => 'No Order',
            		'className' => "center aligned",
            		'searchable' => false,
            		'sortable' => true,
            		'width' => '150px',
            	],
            	[
            		'data' => 'tgl_order',
            		'name' => 'tgl_order',
            		'label' => 'Tgl Order',
            		'className' => "center aligned",
            		'searchable' => false,
            		'sortable' => true,
            		'width' => '150px',
            	],
            	[
            		'data' => 'no_surat',
            		'name' => 'no_surat',
            		'label' => 'No Surat Permintaan',
            		'className' => "center aligned",
            		'searchable' => false,
            		'sortable' => true,
            		'width' => '150px',
            	],
            	[
            		'data' => 'layanan',
            		'name' => 'layanan',
            		'label' => 'Layanan',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '200px',
            	],
            	[
            		'data' => 'lingkup',
            		'name' => 'lingkup',
            		'label' => 'Lingkup',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '200px',
            	],
            	[
            		'data' => 'jenis_pengujian',
            		'name' => 'jenis_pengujian',
            		'label' => 'Jenis Pengujian',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "left aligned",
            		'width' => '200px',
            	],
            	[
            		'data' => 'pemesan',
            		'name' => 'pemesan',
            		'label' => 'Peminta Jasa',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '300px',
            	],
            	[
            		'data' => 'detil',
            		'name' => 'detil',
            		'label' => 'Detil',
            		'searchable' => false,
            		'sortable' => false,
            		'className' => "center aligned",
            		'width' => '50px',
            	],
            	[
            		'data' => 'action',
            		'name' => 'action',
            		'label' => 'Aksi',
            		'searchable' => false,
            		'sortable' => false,
            		'className' => "center aligned",
            		'width' => '150px',
            	],
            	[
            		'data' => 'status',
            		'name' => 'status',
            		'label' => 'Status',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '150px',
            	],
            ],
            'listStruct3' => [
            	[
            		'data' => 'num',
            		'name' => 'num',
            		'label' => '#',
            		'orderable' => false,
            		'searchable' => false,
            		'className' => "center aligned",
            		'width' => '40px',
            	],
            	/* --------------------------- */
            	[
            		'data' => 'no_order',
            		'name' => 'no_order',
            		'label' => 'No Order',
            		'className' => "center aligned",
            		'searchable' => false,
            		'sortable' => true,
            		'width' => '150px',
            	],
            	[
            		'data' => 'tgl_order',
            		'name' => 'tgl_order',
            		'label' => 'Tgl Order',
            		'className' => "center aligned",
            		'searchable' => false,
            		'sortable' => true,
            		'width' => '150px',
            	],
            	[
            		'data' => 'no_surat',
            		'name' => 'no_surat',
            		'label' => 'No Surat Permintaan',
            		'className' => "center aligned",
            		'searchable' => false,
            		'sortable' => true,
            		'width' => '150px',
            	],
            	[
            		'data' => 'layanan',
            		'name' => 'layanan',
            		'label' => 'Layanan',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '200px',
            	],
            	[
            		'data' => 'lingkup',
            		'name' => 'lingkup',
            		'label' => 'Lingkup',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '200px',
            	],
            	[
            		'data' => 'jenis_pengujian',
            		'name' => 'jenis_pengujian',
            		'label' => 'Jenis Pengujian',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "left aligned",
            		'width' => '200px',
            	],
            	[
            		'data' => 'pemesan',
            		'name' => 'pemesan',
            		'label' => 'Peminta Jasa',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '300px',
            	],
            	[
            		'data' => 'detil',
            		'name' => 'detil',
            		'label' => 'Detil',
            		'searchable' => false,
            		'sortable' => false,
            		'className' => "center aligned",
            		'width' => '50px',
            	],
            	// [
            	// 	'data' => 'action',
            	// 	'name' => 'action',
            	// 	'label' => 'Aksi',
            	// 	'searchable' => false,
            	// 	'sortable' => false,
            	// 	'className' => "center aligned",
            	// 	'width' => '50px',
            	// ],
            	[
            		'data' => 'status',
            		'name' => 'status',
            		'label' => 'Status',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '150px',
            	],
            ],
            'listStruct4' => [
            	[
            		'data' => 'num',
            		'name' => 'num',
            		'label' => '#',
            		'orderable' => false,
            		'searchable' => false,
            		'className' => "center aligned",
            		'width' => '40px',
            	],
            	/* --------------------------- */
            	[
            		'data' => 'no_order',
            		'name' => 'no_order',
            		'label' => 'No Order',
            		'className' => "center aligned",
            		'searchable' => false,
            		'sortable' => true,
            		'width' => '150px',
            	],
            	[
            		'data' => 'tgl_order',
            		'name' => 'tgl_order',
            		'label' => 'Tgl Order',
            		'className' => "center aligned",
            		'searchable' => false,
            		'sortable' => true,
            		'width' => '150px',
            	],
            	[
            		'data' => 'no_surat',
            		'name' => 'no_surat',
            		'label' => 'No Surat Permintaan',
            		'className' => "center aligned",
            		'searchable' => false,
            		'sortable' => true,
            		'width' => '150px',
            	],
            	[
            		'data' => 'layanan',
            		'name' => 'layanan',
            		'label' => 'Layanan',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '200px',
            	],
            	[
            		'data' => 'lingkup',
            		'name' => 'lingkup',
            		'label' => 'Lingkup',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '200px',
            	],
            	[
            		'data' => 'jenis_pengujian',
            		'name' => 'jenis_pengujian',
            		'label' => 'Jenis Pengujian',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "left aligned",
            		'width' => '200px',
            	],
            	[
            		'data' => 'pemesan',
            		'name' => 'pemesan',
            		'label' => 'Peminta Jasa',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '300px',
            	],
            	[
        			'data' => 'timestamp',
        			'name' => 'timestamp',
        			'label' => 'Time Stamp',
        			'searchable' => false,
        			'sortable' => true,
        			'className' => "center aligned timestamp",
        			'width' => '120px',
        		],
            	[
            		'data' => 'detil',
            		'name' => 'detil',
            		'label' => 'Detil',
            		'searchable' => false,
            		'sortable' => false,
            		'className' => "center aligned",
            		'width' => '50px',
            	],
            	[
            		'data' => 'action',
            		'name' => 'action',
            		'label' => 'Aksi',
            		'searchable' => false,
            		'sortable' => false,
            		'className' => "center aligned",
            		'width' => '150px',
            	],
            	[
            		'data' => 'status',
            		'name' => 'status',
            		'label' => 'Status',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '150px',
            	],
            ],
            'listStruct5' => [
            	[
            		'data' => 'num',
            		'name' => 'num',
            		'label' => '#',
            		'orderable' => false,
            		'searchable' => false,
            		'className' => "center aligned",
            		'width' => '40px',
            	],
            	/* --------------------------- */
            	[
            		'data' => 'no_order',
            		'name' => 'no_order',
            		'label' => 'No Order',
            		'className' => "center aligned",
            		'searchable' => false,
            		'sortable' => true,
            		'width' => '150px',
            	],
            	[
            		'data' => 'tgl_order',
            		'name' => 'tgl_order',
            		'label' => 'Tgl Order',
            		'className' => "center aligned",
            		'searchable' => false,
            		'sortable' => true,
            		'width' => '150px',
            	],
            	[
            		'data' => 'no_surat',
            		'name' => 'no_surat',
            		'label' => 'No Surat Permintaan',
            		'className' => "center aligned",
            		'searchable' => false,
            		'sortable' => true,
            		'width' => '150px',
            	],
            	[
            		'data' => 'layanan',
            		'name' => 'layanan',
            		'label' => 'Layanan',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '200px',
            	],
            	[
            		'data' => 'lingkup',
            		'name' => 'lingkup',
            		'label' => 'Lingkup',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '200px',
            	],
            	[
            		'data' => 'jenis_pengujian',
            		'name' => 'jenis_pengujian',
            		'label' => 'Jenis Pengujian',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "left aligned",
            		'width' => '200px',
            	],
            	[
            		'data' => 'pemesan',
            		'name' => 'pemesan',
            		'label' => 'Peminta Jasa',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '300px',
            	],
            	[
            		'data' => 'detil',
            		'name' => 'detil',
            		'label' => 'Detil',
            		'searchable' => false,
            		'sortable' => false,
            		'className' => "center aligned",
            		'width' => '50px',
            	],
            	// [
            	// 	'data' => 'action',
            	// 	'name' => 'action',
            	// 	'label' => 'Aksi',
            	// 	'searchable' => false,
            	// 	'sortable' => false,
            	// 	'className' => "center aligned",
            	// 	'width' => '50px',
            	// ],
            	[
            		'data' => 'status',
            		'name' => 'status',
            		'label' => 'Status',
            		'searchable' => false,
            		'sortable' => true,
            		'className' => "center aligned",
            		'width' => '150px',
            	],
            ],
        ];
    }

    public function grid(Request $request)
    {	
    	$user = auth()->user();
    	$records = PendaftaranPengujian::where('id', 0)->select('*');

    	if($user->hasRole(['yan-kalibrasi','admin'])){
            $records = PendaftaranPengujian::whereIn('status', [1,11,12])->where('tipe', 1)->where('jenis', 0)->where('tipe_surat', 0)->select('*');
            if (!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($no_order = $request->no_order) {
                  $records->where('no_order', 'like','%'.$no_order.'%');
            }
            if($no_surat = $request->no_surat){
                  $records->where('no_surat','like','%'.$no_surat.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                  $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                  $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
                  });
            }
    	}elseif($user->hasRole(['yan-uji','admin'])){
    		$records = PendaftaranPengujian::whereIn('status', [1,11,12])->where('tipe', 1)->where('jenis', 1)->where('tipe_surat', 0)->select('*');
            if (!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($no_order = $request->no_order) {
                  $records->where('no_order', 'like','%'.$no_order.'%');
            }
            if($no_surat = $request->no_surat){
                  $records->where('no_surat','like','%'.$no_surat.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                  $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                  $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
                  });
            }
    	}else{
    		$records = PendaftaranPengujian::where('id', 0)->select('*');
    	}
		//Init Sort

        $link = $this->link;
        return DataTables::of($records->get())
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('pemesan', function ($record) use ($request) {
            $pemesan = $record->user->pelanggans->perusahaan->nama.'</br>'.$record->user->pelanggans->nama_lengkap.'</br>'.$record->user->pelanggans->no_hp;
            return $pemesan;
        })
        ->addColumn('layanan', function ($record) use ($request) {
            return $record->pelayanan->nama;
        })
        ->addColumn('lingkup', function ($record) use ($request) {
            return $record->lingkup->nama;
        })
        ->addColumn('jenis_pengujian', function ($record) use ($request) {
        	$jenis_pengujian = '';
            	if($record->detail){
	            	$jenis_pengujian .= '<div class="ui list list-more1" data-display="3">';
			        	foreach ($record->detail as $key => $value) {
			        		$jenis_pengujian .= '<div class="item">'.($key+1).'. '.$value->jenis->nama.'</div>';
			        	}
			        $jenis_pengujian .='</div>';
			    }
            return $jenis_pengujian;
        })
        ->addColumn('tgl_order', function ($record) use ($request) {
            return DateToStringYear($record->tgl_order);
        })
        ->addColumn('total', function ($record) use ($request) {
            return 0;
        })
        ->editColumn('timestamp', function ($record) {
            $time = $record->created_at;
        	if(!is_null($record->updated_at)){
        		$time =  $record->updated_at;
        	}

            $time = Carbon::parse($time)->addWeekdays(getHariKerja(url($this->link),$record->layanan_id));

            return $time->format('Y-m-d H:i:s');
        })
        ->addColumn('detil', function ($record) use ($link){
                $btn = '';

				$btn .= $this->makeButton([
					'type' => 'url',
					'tooltip' => 'Detil Data',
					'class' => 'ui teal mini icon button',
					'label' => '<i class="eye icon"></i>',
					'url' => url($this->link.$record->id.'/detil')
				]);

                return $btn;
        })
        ->addColumn('status', function ($record) use ($request) {
            $stat = '';
            if($record->status==11){
              $stat ='<a class="ui orange tag label perbaikan-message" data-id="'.$record->id.'">Perbaikan</a>';
            }else{
                  $stat = statusa($record->status);
            }
            return $stat;
        })
        ->addColumn('action', function ($record) use ($link){
                $btn = '';
                if($record->pelayanan->id !== 3){
	                $btn .= $this->makeButton([
						'type' => 'url',
						'tooltip' => 'Ubah Data',
						'class' => 'ui violet mini icon button',
						'label' => '<i class="edit icon"></i>',
						'url' => url($this->link.$record->id.'/ubah')
					]);
                }
                
                $btn .= $this->makeButton([
                   	'type' => 'modal',
                    'tooltip' => 'Disposisi',
                    'label'   => '<i class="user icon"></i>',
                    'data' => [
                        'id' => $record->id
                    ],
                    'id'   => $record->id
                ]);

                $btn .= $this->makeButton([
                   	'type' => 'modal',
                    'tooltip' => 'Tolak',
                    'label'   => '<i class="close icon"></i>',
                    'class' => 'ui red mini icon tolak button',
                    'data' => [
                        'id' => $record->id
                    ],
                    'id'   => $record->id
                ]);
                return $btn;
        })
        ->rawColumns(['pemesan','action','detil','jenis_pengujian','status'])
        ->make(true);
    }

    public function ams(Request $request)
    {	
      $user = auth()->user();
      
    	// $records = [];
      if($user->hasRole(['yan-kalibrasi'])){
            $records = PendaftaranPengujian::whereIn('status', [1,11,12])->where('tipe', 2)->where('jenis', 0)->where('status_ams',1)->where('tipe_surat', 0)->select('*');
            if (!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($no_order = $request->no_order) {
                  $records->where('no_order', 'like','%'.$no_order.'%');
            }
            if($no_surat = $request->no_surat){
                  $records->where('no_surat','like','%'.$no_surat.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                  $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                  $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
                  });
            }
      }elseif($user->hasRole(['yan-uji'])){
            $records = PendaftaranPengujian::whereIn('status', [1,11,12])->where('tipe', 2)->where('jenis', 1)->where('status_ams',1)->where('tipe_surat', 0)->select('*');
            if (!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($no_order = $request->no_order) {
                  $records->where('no_order', 'like','%'.$no_order.'%');
            }
            if($no_surat = $request->no_surat){
                  $records->where('no_surat','like','%'.$no_surat.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                  $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                  $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
                  });
            }
      }elseif($user->hasRole(['admin'])){
            $records = PendaftaranPengujian::whereIn('status', [1,11,12])->where('tipe', 2)->whereIn('jenis', [0,1])->where('status_ams', 0)->where('tipe_surat', 0)->select('*');
            if (!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($no_order = $request->no_order) {
                  $records->where('no_order', 'like','%'.$no_order.'%');
            }
            if($no_surat = $request->no_surat){
                  $records->where('no_surat','like','%'.$no_surat.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                  $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                  $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
                  });
            }
      }else{
            $records = PendaftaranPengujian::where('id', 0)->select('*');
      }
      if ($no_surat = $request->no_surat) {
            $records->where('no_surat','like', '%'.$no_surat.'%');
      }
      if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $records->where('layanan_id',$jenis_pelayanan_id);
      }
      if ($perusahaan_id = $request->perusahaan_id) {
            $records->whereHas('user', function($user) use ($perusahaan_id){
                  $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                        $pelanggan->where('perusahaan_id',$perusahaan_id);
                  });
            });
      }

      $link = $this->link;
        return DataTables::of($records->get())
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('pemesan', function ($record) use ($request) {
            $pemesan = '-';
            if($record->user->pelanggans){
                  $pemesan = $record->user->pelanggans->perusahaan->nama.'</br>'.$record->user->pelanggans->nama_lengkap.'</br>'.$record->user->pelanggans->no_hp;
            }
            return $pemesan;
        })
        ->addColumn('no_order', function ($record) use ($request) {
        	if($record->status_ams == 0){
	            return '-';
        	}else{
				return $record->no_order;
        	}
        })
        ->addColumn('layanan', function ($record) use ($request) {
        	if($record->status_ams == 0){
	            return '-';
        	}else{
				return $record->pelayanan->nama;
        	}
        })
        ->addColumn('lingkup', function ($record) use ($request) {
        	if($record->status_ams == 0){
	            return '-';
        	}else{
				return $record->lingkup->nama;
        	}
        })
        ->addColumn('jenis_pengujian', function ($record) use ($request) {
        	if($record->status_ams == 0){
	            return '-';
        	}else{
				$jenis_pengujian = '';
                  if($record->detail){
                        $jenis_pengujian .= '<div class="ui list list-more2" data-display="3">';
                              foreach ($record->detail as $key => $value) {
                                    $jenis_pengujian .= '<div class="item">'.($key+1).'. '.$value->jenis->nama.'</div>';
                              }
                          $jenis_pengujian .='</div>';
                      }
            	return $jenis_pengujian;
        	}
        })
        ->addColumn('tgl_order', function ($record) use ($request) {
            return DateToStringYear($record->tgl_order);
        })
        ->addColumn('total', function ($record) use ($request) {
            return 0;
        })
        ->addColumn('detil', function ($record) use ($link){
                $btn = '';
                $disabled = '';
                if($record->status_ams==0){
                  $disabled = 'disabled';
                }
                  $btn .= $this->makeButton([
                        'type' => 'url',
                        'tooltip' => 'Detil Data',
                        'class' => 'ui teal mini icon button '.$disabled,
                        'label' => '<i class="eye icon"></i>',
                        'url' => url($this->link.$record->id.'/detil')
                  ]);

                return $btn;
        })
        ->addColumn('status', function ($record) use ($request) {
            $status = 'Data Belum Lengkap';
            if($record->status_ams==1){
                  $status = statusa($record->status);
            }
            return $status;
        })
        ->editColumn('timestamp', function ($record) {
            $time = $record->created_at;
            if(!is_null($record->updated_at)){
                  $time =  $record->updated_at;
            }

            $time = Carbon::parse($time)->addWeekdays(getHariKerja(url($this->link),$record->layanan_id));

            return $time->format('Y-m-d H:i:s');
        })
        ->addColumn('action', function ($record) use ($link){
                $btn = '';

                if($record->status_ams==1){
                      $btn .= $this->makeButton([
                                    'type' => 'url',
                                    'tooltip' => 'Ubah Data',
                                    'class' => 'ui violet mini icon button',
                                    'label' => '<i class="edit icon"></i>',
                                    'url' => url($this->link.$record->id.'/ubah')
                              ]);
                      
                      $btn .= $this->makeButton([
                              'type' => 'modal',
                          'tooltip' => 'Disposisi',
                          'label'   => '<i class="user icon"></i>',
                          'data' => [
                              'id' => $record->id
                          ],
                          'id'   => $record->id
                      ]);

                      $btn .= $this->makeButton([
                              'type' => 'modal',
                          'tooltip' => 'Tolak',
                          'label'   => '<i class="close icon"></i>',
                          'class' => 'ui red mini icon tolak button',
                          'data' => [
                              'id' => $record->id
                          ],
                          'id'   => $record->id
                      ]);
                }else{
                  $btn .= $this->makeButton([
                        'type' => 'url',
                        'tooltip' => 'Lengkapi Data',
                        'class' => 'ui violet mini icon button',
                        'label' => '<i class="edit icon"></i>',
                        'url' => url($this->link.$record->id.'/ubah-ams')
                  ]);
                }
                return $btn;
        })
        ->rawColumns(['pemesan','action','detil','jenis_pengujian'])
        ->make(true);
    }

    public function spm(Request $request)
    {	
    	$user = auth()->user();
    	$records = PendaftaranPengujian::where('id', 0)->select('*');

    	if($user->hasRole(['yan-kalibrasi','admin'])){
	     	$records = PendaftaranPengujian::whereIn('status', [1,11,12])->where('jenis', 0)->where('tipe', 3)->where('tipe_surat', 0)->select('*');
	      if (!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($no_order = $request->no_order) {
                  $records->where('no_order', 'like','%'.$no_order.'%');
            }
            if($no_surat = $request->no_surat){
                  $records->where('no_surat','like','%'.$no_surat.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                  $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                  $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
                  });
            }
    	}elseif($user->hasRole(['yan-uji','admin'])){
    		$records = PendaftaranPengujian::whereIn('status', [1,11,12])->where('jenis', 1)->where('tipe', 3)->where('tipe_surat', 0)->select('*');
	     if (!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($no_order = $request->no_order) {
                  $records->where('no_order', 'like','%'.$no_order.'%');
            }
            if($no_surat = $request->no_surat){
                  $records->where('no_surat','like','%'.$no_surat.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                  $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                  $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
                  });
            }
    	}else{
    		$records = PendaftaranPengujian::where('id', 0)->select('*');
    	}
		//Init Sort

        $link = $this->link;
        return DataTables::of($records->get())
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('pemesan', function ($record) use ($request) {
            $pemesan = $record->user->pelanggans->perusahaan->nama.'</br>'.$record->user->pelanggans->nama_lengkap.'</br>'.$record->user->pelanggans->no_hp;
            return $pemesan;
        })
        ->addColumn('layanan', function ($record) use ($request) {
            return $record->pelayanan->nama;
        })
        ->addColumn('lingkup', function ($record) use ($request) {
            return $record->lingkup->nama;
        })
        ->addColumn('jenis_pengujian', function ($record) use ($request) {
        	$jenis_pengujian = '';
            	if($record->detail){
	            	$jenis_pengujian .= '<div class="ui list list-more3" data-display="3">';
			        	foreach ($record->detail as $key => $value) {
			        		$jenis_pengujian .= '<div class="item">'.($key+1).'. '.$value->jenis->nama.'</div>';
			        	}
			        $jenis_pengujian .='</div>';
			    }
            return $jenis_pengujian;
        })
        ->addColumn('tgl_order', function ($record) use ($request) {
            return DateToStringYear($record->tgl_order);
        })
        ->addColumn('total', function ($record) use ($request) {
            return 0;
        })
        ->addColumn('detil', function ($record) use ($link){
                $btn = '';

				$btn .= $this->makeButton([
					'type' => 'url',
					'tooltip' => 'Detil Data',
					'class' => 'ui teal mini icon button',
					'label' => '<i class="eye icon"></i>',
					'url' => url($this->link.$record->id.'/detil')
				]);

                return $btn;
        })
        ->addColumn('status', function ($record) use ($request) {
            return statusa($record->status);
        })
        ->editColumn('timestamp', function ($record) {
        	$time = $record->created_at;
            if(!is_null($record->updated_at)){
                  $time =  $record->updated_at;
            }

            $time = Carbon::parse($time)->addWeekdays(getHariKerja(url($this->link),$record->layanan_id));

            return $time->format('Y-m-d H:i:s');
        })
        ->addColumn('action', function ($record) use ($link){
                $btn = '';

                $btn .= $this->makeButton([
					'type' => 'url',
					'tooltip' => 'Ubah Data',
					'class' => 'ui violet mini icon button',
					'label' => '<i class="edit icon"></i>',
					'url' => url($this->link.$record->id.'/ubah')
				]);
                
                $btn .= $this->makeButton([
                   	'type' => 'modal',
                    'tooltip' => 'Disposisi',
                    'label'   => '<i class="user icon"></i>',
                    'data' => [
                        'id' => $record->id
                    ],
                    'id'   => $record->id
                ]);

                $btn .= $this->makeButton([
                   	'type' => 'modal',
                    'tooltip' => 'Tolak',
                    'label'   => '<i class="close icon"></i>',
                    'class' => 'ui red mini icon tolak button',
                    'data' => [
                        'id' => $record->id
                    ],
                    'id'   => $record->id
                ]);
                return $btn;
        })
        ->rawColumns(['pemesan','action','detil','jenis_pengujian'])
        ->make(true);
    }

    public function histori(Request $request)
    {	
    	$user = auth()->user();
    	$records = PendaftaranPengujian::where('id', 0)->select('*');

    	if($user->hasRole(['yan-kalibrasi','admin'])){
	        $records = PendaftaranPengujian::whereIn('status', [2,3,4,5,6,7])
	        								->where('jenis', 0)
	        								->where('tipe_surat', 0)
	        	                            ->select('*');
	        	                            
	      	if (!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($no_order = $request->no_order) {
                  $records->where('no_order', 'like','%'.$no_order.'%');
            }
            if($no_surat = $request->no_surat){
                  $records->where('no_surat','like','%'.$no_surat.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                  $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                  $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
                  });
            }
    	}elseif($user->hasRole(['yan-uji','admin'])){
    		$records = PendaftaranPengujian::whereIn('status', [2,3,4,5,6,7])
	        								->where('jenis', 1)
	        								->where('tipe_surat', 0)
	        	                            ->select('*');
	        	                            
	      	if (!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($no_order = $request->no_order) {
                  $records->where('no_order', 'like','%'.$no_order.'%');
            }
            if($no_surat = $request->no_surat){
                  $records->where('no_surat','like','%'.$no_surat.'%');
            }
            if($tanggal_order = $request->tanggal_order){
                  $records->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $records->where('layanan_id',$jenis_pelayanan_id);
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                  $records->whereHas('user', function($user) use ($perusahaan_id){
                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                    });
                  });
            }
    	}else{
    		$records = PendaftaranPengujian::where('id', 0)->select('*');
    	}

        $link = $this->link;
        return DataTables::of($records->get())
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('pemesan', function ($record) use ($request) {
            $pemesan = $record->user->pelanggans->perusahaan->nama.'</br>'.$record->user->pelanggans->nama_lengkap.'</br>'.$record->user->pelanggans->no_tlp;
            return $pemesan;
        })
        ->addColumn('layanan', function ($record) use ($request) {
        	return $record->pelayanan->nama;
        })
        ->addColumn('lingkup', function ($record) use ($request) {
            return $record->lingkup->nama;
        })
        ->addColumn('jenis_pengujian', function ($record) use ($request) {
        	$jenis_pengujian = '';
            	if($record->detail){
	            	$jenis_pengujian .= '<div class="ui list list-more4" data-display="3">';
			        	foreach ($record->detail as $key => $value) {
			        		$jenis_pengujian .= '<div class="item">'.($key+1).'. '.$value->jenis->nama.'</div>';
			        	}
			        $jenis_pengujian .='</div>';
			    }
            return $jenis_pengujian;
        })
        ->addColumn('tgl_order', function ($record) use ($request) {
            return DateToStringYear($record->tgl_order);
        })
        ->addColumn('total', function ($record) use ($request) {
            return 0;
        })
        ->addColumn('detil', function ($record) use ($link){
                $btn = '';

				$btn .= $this->makeButton([
					'type' => 'url',
					'tooltip' => 'Detil Data',
					'class' => 'ui teal mini icon button',
					'label' => '<i class="eye icon"></i>',
					'url' => url($this->link.$record->id.'/detil-terkirim')
				]);

                return $btn;
        })
        ->addColumn('status', function ($record) use ($request) {
        	$status = '-';
        	if($record->act_dispo){
	        	if($record->act_dispo->sortByDesc('jenis')->first()){
		        	$jenis = $record->act_dispo->sortByDesc('jenis')->first()->jenis;
		        	$tipe = $record->act_dispo->sortByDesc('jenis')->first()->tipe;

		        	if($jenis == 2){
		        		if($tipe == 2){
		        			$status = 'Menunggu Kaji Ulang';
		        		}elseif($tipe == 3){
		        			$status = 'Kaji Ulang Selesai';
		        		}else{
		        			$status = '-';
		        		}
		        	}elseif($jenis == 3){
		        		$status = 'Virtual Account';
		        	}elseif($jenis == 4){
		        		$status = 'Surat Penawaran';
		        	}else{
		        		$status = 'Penerimaan Surat';
		        	}
	        	}else{
	        		$status = 'Penerimaan Surat';
	        	}
        	}else{
        		$status = 'Penerimaan Surat';
        	}
            return $status;
        })
        ->rawColumns(['pemesan','detil','jenis_pengujian'])
        ->make(true);
    }

    public function index()
    {
    	$user = auth()->user();
    	$online =0;
    	$spm =0;
    	$ams =0;
    	$terkirim =0;
    	if($user->hasRole(['yan-kalibrasi','admin'])){
    		$online = PendaftaranPengujian::whereIn('status', [1,11,12])->where('jenis', 0)->where('tipe', 1)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
    		$spm = PendaftaranPengujian::whereIn('status', [1,11,12])->where('jenis', 0)->where('tipe', 3)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
    		$terkirim = PendaftaranPengujian::where('status', 2)->where('jenis', 0)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
            $ams = PendaftaranPengujian::where('status', 1)->where('jenis', 0)->where('tipe', 2)->where('status_ams',1)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
    	}elseif($user->hasRole(['yan-uji','admin'])){
    		$online = PendaftaranPengujian::whereIn('status', [1,11,12])->where('jenis', 1)->where('tipe', 1)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
    		$spm = PendaftaranPengujian::whereIn('status', [1,11,12])->where('jenis', 1)->where('tipe', 3)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
    		$terkirim = PendaftaranPengujian::where('status', 2)->where('jenis', 1)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
            $ams = PendaftaranPengujian::where('status', 1)->where('jenis', 1)->where('tipe', 2)->where('status_ams',1)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
    	}elseif($user->hasRole(['admin'])){
            $ams = PendaftaranPengujian::where('status', 1)->whereIn('jenis', [1,0])->where('tipe', 2)->where('status_ams', 0)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
      	}else{
    		$online =0;
    		$spm =0;
    		$ams =0;
    		$terkirim =0;
    	}
        return $this->render('modules.penerimaan.index', [
        	'mockup' => true,
        	'online' => $online,
        	'spm' => $spm,
        	'ams' => $ams,
        	'terkirim' => $terkirim,
        	'structs' => $this->listStructs,
        ]);
    }

    public function create()
    {
        return $this->render('modules.penerimaan.create');
    }

    public function perbaikanMessage($id)
    {
        $record = PendaftaranPengujian::find($id);
        return $this->render('modules.penerimaan.perbaikan-message', [
            'record' => $record->latestActivity,
        ]);
    }

    public function ubah($id)
    {
    	$this->setTitle('Ubah Penerimaan Surat');
    	$this->setBreadcrumb(['Penerimaan Surat' => '#', 'Ubah' => '#']);
        $record = PendaftaranPengujian::find($id);
        return $this->render('modules.penerimaan.edit',['record' => $record]);
    }

    public function ubahAms($id)
    {
      $this->setTitle('Ubah Data AMS');
      $this->setBreadcrumb(['Penerimaan Surat' => '#', 'Ubah' => '#']);
        $record = PendaftaranPengujian::find($id);
        return $this->render('modules.penerimaan.ams.edit-ams',['record' => $record]);
    }

    public function store(RolesRequest $request)
    {
        $record = new Role;
        $record->fill($request->all());
        $record->save();

        return response([
            'status' => true
        ]);
    }

    public function detil($id)
    {
        $record = PendaftaranPengujian::find($id);
        $kaji_ulang = KajiUlang::where('pp_id', $id)->first();
        $this->setTitle('Detil Penerimaan Surat');
        $this->setSubtitle('No Order : '.$record->no_order);
        $this->setBreadcrumb(['Penerimaan Surat' => '#', 'Detil' => '#']);
        return $this->render('modules.penerimaan.detil',[
        	'record' => $record,
        	'kaji_ulang' => $kaji_ulang,
    	]);
    }

    public function detilTerkirim($id)
    {
        $record = PendaftaranPengujian::find($id);
        $kaji_ulang = KajiUlang::where('pp_id', $id)->first();
        $this->setTitle('Detil Penerimaan Surat');
        $this->setSubtitle('No Order : '.$record->no_order);
        $this->setBreadcrumb(['Penerimaan Surat' => '#', 'Detil' => '#']);
        return $this->render('modules.penerimaan.detil',[
        	'record' => $record,
        	'kaji_ulang' => $kaji_ulang,
    	]);
    }

    public function detilTolak($id)
    {
        $record = PendaftaranPengujian::find($id);
        $kaji_ulang = KajiUlang::where('pp_id', $id)->first();
        return $this->render('modules.penerimaan.detil',[
        	'record' => $record,
        	'kaji_ulang' => $kaji_ulang,
    	]);
    }

    public function detail()
    {
        return $this->render('modules.penerimaan.detil-ams');
    }

    public function edit($id)
    {
        $record = PendaftaranPengujian::with('user.pelanggans')->find($id);
        // $users = Users::with('detail')->whereHas('detail',function($q) use($record){
        //     $q->where('jenis_pelayanan_id',$record->jenis_layanan_id);
        // })->get();
        return $this->render('modules.penerimaan.create-disposisi', [
            'record' => $record,
            // 'users' => $users
        ]);
    }

    public function dispo()
    {
        return $this->render('modules.penerimaan.create-disposisi-ams');
    }

    public function tolakOnline(Request $request, $id)
    {
    	DB::beginTransaction();
        try {

            $pengujian = PendaftaranPengujian::find($id);
            $pengujian->status = 5;
            $pengujian->save();

            DB::commit();
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true
        ]); 
    }

    public function saveUbah(Request $request, $id)
    {
    	$user = auth()->user();
    	if($request->detail){
    	}else{
    		return response()->json([
                'status' => 'false',
                'message' => 'Data Item Uji tidak boleh kosong'
            ],402);
    	}

        DB::beginTransaction();
        try {
            $pengujian = PendaftaranPengujian::find($id);
            $pengujian->layanan_id = $request->jenis_pelayanan_id;
            $pengujian->lingkup_id = $request->lingkup_id;
            $pengujian->status=1;
            $pengujian->save();
            if($request->detail){
            	$hapus = PendaftaranPengujianDetail::where('pp_id', $id)
	                        						->delete();
            }

            $act = new ActDisposisi;
            $act->parent_id = $pengujian->id;
            $act->pengirim_id = auth()->user()->id;
            $act->penerima_id = 1;
            $act->tipe = 0;
            $act->jenis = 1;
            $act->save();

            $pengujian->saveDetail($request->detail);
            DB::commit();

	    }catch (\Exception $e) {
	      DB::rollback();
	      return response([
	        'status' => 'error',
	        'message' => 'An error occurred!',
	        'error' => $e->getMessage(),
	      ], 500);
	    }

	    return response([
	      'status' => true,
	    ]);
    }

    public function update(DisposisiRequest $request, $id)
    {
    	DB::beginTransaction();
        try {

        	if($request->tipe == 1){
        		$pengujian = PendaftaranPengujian::find($id);
        		if($pengujian->layanan_id == 1){
        			$pengujian->jenis = 0;
        		}else{
		            $pengujian->jenis = 1;
        		}
	            $pengujian->save();
                  $tab = 'online';
                  $mtab = 'Masuk(Online)';
                  if($pengujian->tipe==2){
                        $tab = 'ams';
                        $mtab = 'Masuk(AMS)';
                  }
                  if($pengujian->tipe==3){
                        $tab = 'manual';
                        $mtab = 'Masuk(Manual)';
                  }
        		if($request->penerima_id){
        			foreach ($request->penerima_id as $k => $value) {
        				$act = new ActDisposisi;
        				$act->parent_id = $id;
        				$act->pengirim_id = $request->pengirim;
        				$act->penerima_id = $value;
        				$act->tipe = $request->tipe;
        				$act->jenis = 0;
        				$act->keterangan = $request->keterangan;
        				$act->save();
                              $user = User::find($value);
                              if(!is_null($user->device_id)){
                                    $this->onesignal('Penerimaan Surat '.$mtab.' Baru dengan nomor order '.$pengujian->no_order.' status Dialihkan',$user->device_id,$pengujian->toArray(),'penerimaan-surat',$tab);
                              }
        			}
        		}
        	}else{
	            $pengujian = PendaftaranPengujian::find($id);
	            $pengujian->status = 2;
	            $pengujian->save();

	            if($request->penerima_id){
	                foreach ($request->penerima_id as $k => $value) {
	                    $act = new ActDisposisi;
	                    $act->parent_id = $id;
	                    $act->pengirim_id = $request->pengirim;
	                    $act->penerima_id = $value;
	                    $act->tipe = $request->tipe;
	                    $act->jenis = 1;
	                    $act->keterangan = $request->keterangan;
	                    $act->save();

                          $user = User::find($value);
                          if(!is_null($user->device_id)){
                              $this->onesignal('Kaji Ulang Baru dengan nomor order '.$pengujian->no_order,$user->device_id,$pengujian->toArray(),'kaji_ulang','onprogress');
                          }
	                }
	            }
        	}
            DB::commit();
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true
        ]);   
    }

    public function buat()
    {
        return $this->render('modules.penerimaan.buat');
    }

    public function lihat($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function lihatMultiple($id, $type)
    {
    	if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $c->filename,
                        ];
                    }
                }
                return $this->render('pdf-multiple', [
		        	'mockup' => true,
		        	'data' => $rename,
		        ]);
            }
        }
    }

    public function download($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

    public function grant(Request $request, $id)
    {
        $record = Role::find($id);
        $record->perms()->sync($request->check);

        return response([
            'status' => true
        ]);
    }

    public function cekRequestItemUji(CekItemUjiRequest $request){
        return response([
            'status' => true,
        ]);
    }

    public function destroy($id)
    {
        $record = Role::find($id);
        $record->delete();

        return response([
            'status' => true,
        ]);
    }

    public function saveAms(PermintaanPengujianRequest $request)
    {
      if($request->detail){
      }else{
            return response()->json([
                'status' => 'false',
                'message' => 'Data Item Uji tidak boleh kosong'
            ],402);
      }

        $date = date('Y-m-d');
        $nomor = array();

        DB::beginTransaction();
        try {
            $no = PendaftaranPengujian::generateNomor();
            if($request->jenis_pelayanan_id == 3){
                  $a =0;
                  foreach ($request->detail as $key => $value) {
                        $no_order = PendaftaranPengujian::generateNoOrder($request->user_id);
                        $pengujian = PendaftaranPengujian::find($request->pp_id);
                        $pengujian->no_order = PendaftaranPengujian::generateNoOrder($request->user_id);
                        $pengujian->nomor = $no;
                        $pengujian->tgl_surat = $request->tgl_surat;
                        $pengujian->no_surat = $request->no_surat;
                        $pengujian->tgl_order = $date;
                        $pengujian->user_id = $request->user_id;
                        $pengujian->layanan_id = $request->jenis_pelayanan_id;
                        $pengujian->lingkup_id = $request->lingkup_id;
                        $pengujian->kelas = $request->kelas;
                        $pengujian->rencana = $request->rencana;
                        $pengujian->status = 1;
                        $pengujian->jenis = 1;
                        $pengujian->catatan = $request->catatan;
                        $pengujian->status_ams = 1;


                        if($file = $request->surat){
                              $path = $file->store('uploads/pendaftaran', 'public');
                              $pengujian->filename = $file->getClientOriginalName();
                              $pengujian->bukti = $path;
                        }
                        $pengujian->save();

                        $detail = new PendaftaranPengujianDetail;
                        $detail->pp_id = $pengujian->id;
                        $detail->jenis_id = $value['jenis_id'];
                          $detail->merk = $value['merk'];
                          $detail->tipe = $value['tipe'];
                          $detail->jumlah = $value['jumlah'];
                          $detail->satuan_id = $value['satuan_id'];
                          $detail->spesifikasi = $value['spesifikasi'];
                        $detail->save();

                        $pengujian->multipleFileUpload($request->lampiran);
                        if($a % 2 == 0){
                              $nomor[]= $no_order.'<br>';
                        }else{
                              $nomor[]= $no_order;
                        }

                        $a++;
                        if($pengujian->jenis==0){
                          $this->pushNotifKalibrasi($pengujian);
                        }else{
                          $this->pushNotifUji($pengujian);
                        }
                  }
            }else{
                   $no_order = PendaftaranPengujian::generateNoOrder($request->user_id);
                  $pengujian = PendaftaranPengujian::find($request->pp_id);
                  $pengujian->no_order = PendaftaranPengujian::generateNoOrder($request->user_id);
                  $pengujian->nomor = $no;
                  $pengujian->tgl_surat = $request->tgl_surat;
                  $pengujian->no_surat = $request->no_surat;
                  $pengujian->tgl_order = $date;
                  $pengujian->user_id = $request->user_id;
                  $pengujian->layanan_id = $request->jenis_pelayanan_id;
                  $pengujian->lingkup_id = $request->lingkup_id;
                  $pengujian->kelas = $request->kelas;
                  $pengujian->rencana = $request->rencana;
                  $pengujian->status = 1;
                  $pengujian->jenis = 1;
                  $pengujian->catatan = $request->catatan;
                  $pengujian->status_ams = 1;

                  if($request->jenis_pelayanan_id == 1){
                        $pengujian->jenis = 0;
                  }else{
                        $pengujian->jenis = 1;
                  }

                  if($file = $request->surat){
                        $path = $file->store('uploads/pendaftaran', 'public');
                        $pengujian->filename = $file->getClientOriginalName();
                        $pengujian->bukti = $path;
                  }
                  $pengujian->save();
                  $pengujian->saveDetail($request->detail);
                  $pengujian->multipleFileUpload($request->lampiran);

                  $nomor[] = $no_order;
                  if($pengujian->jenis==0){
                        $this->pushNotifKalibrasi($pengujian);
                  }else{
                        $this->pushNotifUji($pengujian);
                  }
            }
            DB::commit();

          }catch (\Exception $e) {
            DB::rollback();
            return response([
              'status' => 'error',
              'message' => 'An error occurred!',
              'error' => $e->getMessage(),
            ], 500);
          }

          return response([
            'status' => true,
          'message' => $nomor
          ]);
    }

    public function pushNotifKalibrasi($data){
        $user = User::whereHas('roles',function($query){
                    $query->where('name','yan-kalibrasi');
                })->get();
        foreach ($user as $row) {
            if(!is_null($row->device_id)){
                 $this->onesignal('Penerimaan Surat Masuk(AMS) Baru dengan nomor order '.$data->no_order,$row->device_id,$data->toArray(),'penerimaan-surat','ams');
            }
        }
    }

    public function pushNotifUji($data){
        $user = User::whereHas('roles',function($query){
                    $query->where('name','yan-uji');
                })->get();
        foreach ($user as $row) {
            if(!is_null($row->device_id)){
                 $this->onesignal('Penerimaan Surat Masuk(AMS) Baru dengan nomor order '.$data->no_order,$row->device_id,$data->toArray(),'penerimaan-surat','ams');
            }
        }
    }

    public function importAms(){
      try {
            $client = new Client(['base_uri' => 'http://202.162.216.68:9000/']);
              $res = $client->request('GET', 'api_ams/');
              $data = json_decode($res->getBody()->getContents());
              foreach ($data->data as $ams) {
                  $cek = PendaftaranPengujian::where('no_surat',$ams->no_surat)->first();
                  if(!$cek){
                      $pp = new PendaftaranPengujian;
                      $pp->no_order = $ams->no_surat;
                      $pp->tgl_order = $ams->tgl_terima;
                      $pp->user_id = 1;
                      $pp->layanan_id = 1;
                      $pp->lingkup_id = 1;
                      $pp->status = 1;
                      $pp->bukti = 'null';
                      $pp->filename = 'null';
                      $pp->no_surat = $ams->no_surat;
                      $pp->tgl_surat = $ams->tgl_surat;
                      $pp->tipe = 2;
                      $pp->catatan = $ams->perihal;
                      $pp->kelas = 1;
                      $pp->rencana = Carbon::now()->addMonths(5)->format('Y-m');
                      $pp->nomor = 0;
                      $pp->status_ams = 0;
                      $pp->save();
                  }
              }
      } catch (\Exception $e) {
            
      }

      return response([
      'status' => true,
      ]);
    }
}
