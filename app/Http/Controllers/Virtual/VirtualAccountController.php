<?php

namespace App\Http\Controllers\Virtual;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\VirtualAccount\VirtualAccountRequest;
use App\Http\Requests\VirtualAccount\AktivasiUlangRequest;
/* Models */
use App\Models\Authentication\Role;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\KajiUlang\KajiUlang;
use App\Models\KajiUlang\Detail;
use App\Models\Act\ActDisposisi;
use App\Models\Files;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Excel;
use Auth;
use DB;

class VirtualAccountController extends Controller
{
    protected $link = 'virtual-account/';
    protected $perms = 'virtual-account';
    protected $listStructs = [];

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Virtual Account (VA)");
        $this->setPerms($this->perms);
        $this->setModalSize("large");
        $this->setBreadcrumb(['Virtual Account (VA)' => '#']);

        // Header Grid Datatable
        $this->listStructs = [
            'listStruct' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                /* --------------------------- */
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'pemesan',
                    'name' => 'pemesan',
                    'label' => 'Peminta Jasa',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'no_va',
                    'name' => 'no_va',
                    'label' => 'No VA',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'nominal_va',
                    'name' => 'nominal_va',
                    'label' => 'Nominal VA (Rp)',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "right aligned",
                    'width' => '100px',
                ],
                [
                	'data' => 'timestamp',
                	'name' => 'timestamp',
                	'label' => 'Time Stamp',
                	'searchable' => false,
                	'sortable' => true,
                	'className' => "center aligned timestamp",
                	'width' => '150px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '80px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '130px',
                ],
                [
                    'data' => 'checklist',
                    'name' => 'checklist',
                    'label' => 'Checklist',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '100px',
                ]
            ],
            'listStruct2' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                /* --------------------------- */
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan',
                    'name' => 'layanan',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'pemesan',
                    'name' => 'pemesan',
                    'label' => 'Peminta Jasa',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'no_va',
                    'name' => 'no_va',
                    'label' => 'No VA',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'nominal_va',
                    'name' => 'nominal_va',
                    'label' => 'Nominal VA (Rp)',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "right aligned",
                    'width' => '100px',
                ],
                [
                    'data' => 'tgl_download',
                    'name' => 'tgl_download',
                    'label' => 'Tgl Download',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '80px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '130px',
                ]
            ],
            'listStruct3' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                /* --------------------------- */
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan',
                    'name' => 'layanan',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'pemesan',
                    'name' => 'pemesan',
                    'label' => 'Peminta Jasa',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'no_va',
                    'name' => 'no_va',
                    'label' => 'No VA',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'nominal_va',
                    'name' => 'nominal_va',
                    'label' => 'Nominal VA (Rp)',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "right aligned",
                    'width' => '100px',
                ],
                [
                    'data' => 'tgl_aktif',
                    'name' => 'tgl_aktif',
                    'label' => 'Tgl Aktivasi',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
                [
                    'data' => 'tgl_kadaluarsa',
                    'name' => 'tgl_kadaluarsa',
                    'label' => 'Tgl Kadaluarsa',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
                 [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '80px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '130px',
                ]
            ],
            'listStruct4' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                /* --------------------------- */
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan',
                    'name' => 'layanan',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'pemesan',
                    'name' => 'pemesan',
                    'label' => 'Peminta Jasa',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'no_va',
                    'name' => 'no_va',
                    'label' => 'No VA',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'nominal_va',
                    'name' => 'nominal_va',
                    'label' => 'Nominal VA (Rp)',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "right aligned",
                    'width' => '100px',
                ],
                [
                    'data' => 'tgl_aktif',
                    'name' => 'tgl_aktif',
                    'label' => 'Tgl Aktivasi',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
                [
                    'data' => 'tgl_kadaluarsa',
                    'name' => 'tgl_kadaluarsa',
                    'label' => 'Tgl Kadaluarsa',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '80px',
                ],
                // [
                //     'data' => 'keterangan',
                //     'name' => 'keterangan',
                //     'label' => 'Keterangan',
                //     'searchable' => false,
                //     'sortable' => false,
                //     'className' => "center aligned",
                //     'width' => '200px',
                // ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '130px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '130px',
                ]
            ],
            'listStruct5' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                /* --------------------------- */
                [
                    'data' => 'tgl_download',
                    'name' => 'tgl_download',
                    'label' => 'Tgl Download',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
                [
                    'data' => 'no_va',
                    'name' => 'no_va',
                    'label' => 'No VA',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'nominal_va',
                    'name' => 'nominal_va',
                    'label' => 'Nominal VA (Rp)',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "right aligned",
                    'width' => '100px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Penawaran',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_surat',
                    'name' => 'tgl_surat',
                    'label' => 'Tanggal Surat Penawaran',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'pemesan',
                    'name' => 'pemesan',
                    'label' => 'Peminta Jasa',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
            ],
        ];
    }

    public function grid(Request $request)
    {
    	if(auth()->user()->hasRole(['keuangan','admin'])){
	        $records = VirtualAccount::where('status', 0)
	                                 ->whereNull('download_date')
	                                 ->where('tipe_customer', '!=', 0)
	                                 ->where('tipe', 0)
	                                 ->select('*');

	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }

            if ($no_va = $request->no_va) {
                $records->where('no_va','like','%'.$no_va.'%');
            }

            if ($no_surat = $request->no_surat) {
                $records->whereHas('surat', function($surat) use ($no_surat){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                        $kaji->whereHas('pp', function($pp) use ($no_surat){
                            $pp->where('no_surat','like', '%'.$no_surat.'%');
                        });
                    });
                });
            }
            if ($no_order = $request->no_order) {
                $records->whereHas('surat', function($surat) use ($no_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                        $kaji->whereHas('pp', function($pp) use ($no_order){
                            $pp->where('no_order','like', '%'.$no_order.'%');
                        });
                    });
                });
            }
            if ($tanggal_order = $request->tanggal_order) {
                $records->whereHas('surat', function($surat) use ($tanggal_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                        $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                        });
                    });
                });
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                $records->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                        $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                            $pp->where('layanan_id',$jenis_pelayanan_id);
                        });
                    });
                });
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                $records->whereHas('surat', function($surat) use ($perusahaan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                        $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                            $pp->whereHas('user', function($user) use ($perusahaan_id){
                                $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                                });
                            });
                        });
                    });
                });
            }
            $records = $records->get();
    	}else{
    		$records =  collect([]);
    	}

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('pemesan', function ($record) use ($request) {
            	$pemesan = $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->no_hp;
            	return $pemesan;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
            	return $record->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
            	$jenis_pengujian = '';
            	if($record->surat->kaji_ulang->pp->detail){
	            	$jenis_pengujian .= '<div class="ui ordered list list-more1" data-display="3">';
	            	foreach ($record->surat->kaji_ulang->pp->detail as $key => $value) {
	            		$kaji_ulang = KajiUlang::find($record->surat->kaji_ulang->id);
	            		if($kaji_ulang){
	            			foreach ($kaji_ulang->detail as $kuy => $val) {
	            				if($val->jenis_id == $value->id){
				            		$jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
	            				}
	            			}
	            		}else{
	            			$jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
	            		}
	            	}
	            	$jenis_pengujian .='</div>';
            	}
            	return $jenis_pengujian;
            })
            ->editColumn('no_va', function($record){
                return $record->no_va;
            })
            ->editColumn('nominal_va', function($record){
                return FormatNumber($record->nominal);
            })
            ->editColumn('tgl_aktif', function($record){
                return DateToStringYear($record->tgl_aktif);
            })
            ->editColumn('tgl_kadaluarsa', function($record){
                return DateToStringYear($record->tgl_kadaluarsa);
            })
            ->editColumn('timestamp', function ($record) {
                $time = $record->tgl_aktif;
                $time = Carbon::parse($time)->addDays(getHariKerja(url($this->link),$record->surat->kaji_ulang->pp->layanan_id));
                return $time->format('Y-m-d H:i:s');
            })
            ->editColumn('detil', function($record){
                $btn = '';
				$btn .= $this->makeButton([
					'type' => 'url',
					'tooltip' => 'Detil Data',
					'class' => 'ui teal mini icon button',
					'label' => '<i class="eye icon"></i>',
					'url' => url($this->link.$record->surat_id.'/detil')
				]);

                return $btn;
            })
            ->editColumn('status', function($record){
                $status ='';
                switch ($record->status) {
                    case '0':
                        $status = '<a class="ui orange tag label">Menunggu Aktivasi</a>';
                        break;
                    case '1':
                        $status = '<a class="ui green tag label">Aktif</a>';
                        break;
                    case '2':
                        $status = '<a class="ui red tag label">Expired</a>'; //tidak aktif sama dengan expired
                        break;
                    case '3':
                        $status = '<a class="ui teal tag label">Terbayar</a>';
                        break;
                    case '4':
                        $status = '<a class="ui blue tag label">Downloaded</a>';
                        break;
                }

                return $status;
            })
            ->editColumn('checklist', function($record){
            	if(auth()->user()->hasRole(['keuangan'])){
	                $checklist = '<div class="ui fitted checkbox">
	                            <input name="checklist[]" class="va" type="checkbox" data-id="'.$record->id.'">
	                            <label></label>
	                          </div>';

            	}else{
            		$checklist = '-';
            	}
                return $checklist;
            })
            ->rawColumns(['pemesan','jenis_pengujian','action','timestamp','detil','status','checklist','layanan_lingkup'])
            ->make(true);
    }

    public function menunggu(Request $request)
    {
    	if(auth()->user()->hasRole(['keuangan','admin'])){
	        $records = VirtualAccount::whereNotNull('download_by')
	        						  ->where('status', 0)
	        						  ->where('tipe_customer', '!=', 0)
	        						  ->where('tipe', 0)
	                              	  ->select('*');

	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }

            if ($no_va = $request->no_va) {
                $records->where('no_va','like','%'.$no_va.'%');
            }

            if ($no_surat = $request->no_surat) {
                $records->whereHas('surat', function($surat) use ($no_surat){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                        $kaji->whereHas('pp', function($pp) use ($no_surat){
                            $pp->where('no_surat','like', '%'.$no_surat.'%');
                        });
                    });
                });
            }
            if ($no_order = $request->no_order) {
                $records->whereHas('surat', function($surat) use ($no_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                        $kaji->whereHas('pp', function($pp) use ($no_order){
                            $pp->where('no_order','like', '%'.$no_order.'%');
                        });
                    });
                });
            }
            if ($tanggal_order = $request->tanggal_order) {
                $records->whereHas('surat', function($surat) use ($tanggal_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                        $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                        });
                    });
                });
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                $records->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                        $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                            $pp->where('layanan_id',$jenis_pelayanan_id);
                        });
                    });
                });
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                $records->whereHas('surat', function($surat) use ($perusahaan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                        $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                            $pp->whereHas('user', function($user) use ($perusahaan_id){
                                $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                                });
                            });
                        });
                    });
                });
            }

            $records = $records->get();
    	}else{
    		$records = collect([]);
        }

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->surat->kaji_ulang->pp->tgl_order);
            })
            ->addColumn('pemesan', function ($record) use ($request) {
            	$pemesan = $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->no_hp;
            	return $pemesan;
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan', function ($record) use ($request) {
            	return $record->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('lingkup', function ($record) use ($request) {
            	return $record->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
            	$jenis_pengujian = '';
            	if($record->surat->kaji_ulang->pp->detail){
	            	$jenis_pengujian .= '<div class="ui ordered list list-more2" data-display="3">';
	            	foreach ($record->surat->kaji_ulang->pp->detail as $key => $value) {
	            		$kaji_ulang = KajiUlang::find($record->surat->kaji_ulang->id);
	            		if($kaji_ulang){
	            			foreach ($kaji_ulang->detail as $kuy => $val) {
	            				if($val->jenis_id == $value->id){
				            		$jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
	            				}
	            			}
	            		}else{
	            			$jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
	            		}
	            	}
	            	$jenis_pengujian .='</div>';
            	}
            	return $jenis_pengujian;
            })
            ->editColumn('no_va', function($record){
                return $record->no_va;
            })
            ->editColumn('nominal_va', function($record){
                return FormatNumber($record->nominal);
            })
            ->editColumn('tgl_aktif', function($record){
                return DateToStringYear($record->tgl_aktif);
            })
            ->editColumn('tgl_download', function($record){
                $tgl = Carbon::createFromFormat('Y-m-d H:i:s', $record->download_date);
                return DateToStringYear($tgl->format('Y-m-d')).' '.$tgl->format('H:i').' WIB';
            })
            ->editColumn('detil', function($record){
                $btn = '';

				$btn .= $this->makeButton([
					'type' => 'url',
					'tooltip' => 'Detil Data',
					'class' => 'ui teal mini icon button',
					'label' => '<i class="eye icon"></i>',
					'url' => url($this->link.$record->surat_id.'/detil')
				]);

                return $btn;
            })
            ->editColumn('status', function($record){
            	$status ='<a class="ui orange tag label">Menunggu Teraktivasi</a>';
                return $status;
            })
            ->rawColumns(['pemesan','jenis_pengujian','detil','status','layanan','tgl_download'])
            ->make(true);
    }

    public function submit(Request $request)
    {	
    	if(auth()->user()->hasRole(['keuangan','admin'])){
	        $records = VirtualAccount::where('status', 1)
	        						  ->where('tipe_customer', '!=', 0)
	        						  ->where('tipe', 0)
	                                  ->select('*');

	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }

            if ($no_va = $request->no_va) {
                $records->where('no_va','like','%'.$no_va.'%');
            }

            if ($no_surat = $request->no_surat) {
                $records->whereHas('surat', function($surat) use ($no_surat){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                        $kaji->whereHas('pp', function($pp) use ($no_surat){
                            $pp->where('no_surat','like', '%'.$no_surat.'%');
                        });
                    });
                });
            }
            if ($no_order = $request->no_order) {
                $records->whereHas('surat', function($surat) use ($no_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                        $kaji->whereHas('pp', function($pp) use ($no_order){
                            $pp->where('no_order','like', '%'.$no_order.'%');
                        });
                    });
                });
            }
            if ($tanggal_order = $request->tanggal_order) {
                $records->whereHas('surat', function($surat) use ($tanggal_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                        $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                        });
                    });
                });
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                $records->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                        $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                            $pp->where('layanan_id',$jenis_pelayanan_id);
                        });
                    });
                });
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                $records->whereHas('surat', function($surat) use ($perusahaan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                        $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                            $pp->whereHas('user', function($user) use ($perusahaan_id){
                                $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                                });
                            });
                        });
                    });
                });
            }
            $records = $records->get();
    	}else{
    		$records = collect([]);
        }

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->surat->kaji_ulang->pp->tgl_order);
            })
            ->addColumn('pemesan', function ($record) use ($request) {
                $pemesan = $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->no_hp;
                return $pemesan;
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('lingkup', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->surat->kaji_ulang->pp->detail){
                    $jenis_pengujian .= '<div class="ui ordered list list-more3" data-display="3">';
                    foreach ($record->surat->kaji_ulang->pp->detail as $key => $value) {
                        $kaji_ulang = KajiUlang::find($record->surat->kaji_ulang->id);
                        if($kaji_ulang){
                            foreach ($kaji_ulang->detail as $kuy => $val) {
                                if($val->jenis_id == $value->id){
                                    $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
                                }
                            }
                        }else{
                            $jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
                        }
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('no_va', function($record){
                return $record->no_va;
            })
            ->editColumn('nominal_va', function($record){
                return FormatNumber($record->nominal);
            })
            ->editColumn('wbs_io', function($record){
                return $record->wbs_io ? $record->wbs_io : '';
            })
            ->editColumn('tgl_aktif', function($record){
                return DateToStringYear($record->tgl_aktif);
            })
            ->editColumn('tgl_kadaluarsa', function($record){
                return DateToStringYear($record->tgl_kadaluarsa);
            })
            ->editColumn('detil', function($record){
                $btn = '';

                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->surat_id.'/detil')
                ]);

                return $btn;
            })
            ->editColumn('status', function($record){
                $status ='';
                if($record->status == 1){
                    $status = '<a class="ui green tag label">Aktif</a>';
                }else{
                    $status = '<a class="ui red tag label">Tidak Aktif</a>';
                }
                return $status;
            })
            ->rawColumns(['pemesan','jenis_pengujian','action','detil','status','layanan'])
            ->make(true);
    }

    public function nonaktif(Request $request)
    {
    	if(auth()->user()->hasRole(['keuangan','admin'])){
	        $records = VirtualAccount::whereIn('status', [2,3])
	        						  ->where('tipe_customer', '!=', 0)
	        						  ->where('tipe', 0)
	                              	  ->select('*');

	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }

            if ($no_va = $request->no_va) {
                $records->where('no_va','like','%'.$no_va.'%');
            }

            if ($no_surat = $request->no_surat) {
                $records->whereHas('surat', function($surat) use ($no_surat){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                        $kaji->whereHas('pp', function($pp) use ($no_surat){
                            $pp->where('no_surat','like', '%'.$no_surat.'%');
                        });
                    });
                });
            }
            if ($no_order = $request->no_order) {
                $records->whereHas('surat', function($surat) use ($no_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                        $kaji->whereHas('pp', function($pp) use ($no_order){
                            $pp->where('no_order','like', '%'.$no_order.'%');
                        });
                    });
                });
            }
            if ($tanggal_order = $request->tanggal_order) {
                $records->whereHas('surat', function($surat) use ($tanggal_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                        $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                        });
                    });
                });
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                $records->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                        $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                            $pp->where('layanan_id',$jenis_pelayanan_id);
                        });
                    });
                });
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                $records->whereHas('surat', function($surat) use ($perusahaan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                        $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                            $pp->whereHas('user', function($user) use ($perusahaan_id){
                                $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                                });
                            });
                        });
                    });
                });
            }
            $records = $records->get();
    	}else{
    		$records = collect([]);
    	}

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->surat->kaji_ulang->pp->tgl_order);
            })
            ->addColumn('pemesan', function ($record) use ($request) {
            	$pemesan = $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->no_hp;
            	return $pemesan;
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan', function ($record) use ($request) {
            	return $record->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('lingkup', function ($record) use ($request) {
            	return $record->surat->kaji_ulang->pp->lingkup->nama;
            })
            // ->addColumn('keterangan', function ($record) use ($request) {
            // 	$keterangan ='';
            // 	if($record->bukti_url){
            // 		$keterangan ='-';
            // 	}else{
            // 		$keterangan ='<a class="ui orange tag label">Bukti Pembayaran Belum Terupload</a>';
            // 	}
            // 	return $keterangan;
            // })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
            	$jenis_pengujian = '';
            	if($record->surat->kaji_ulang->pp->detail){
	            	$jenis_pengujian .= '<div class="ui ordered list list-more4" data-display="3">';
	            	foreach ($record->surat->kaji_ulang->pp->detail as $key => $value) {
	            		$kaji_ulang = KajiUlang::find($record->surat->kaji_ulang->id);
	            		if($kaji_ulang){
	            			foreach ($kaji_ulang->detail as $kuy => $val) {
	            				if($val->jenis_id == $value->id){
				            		$jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="'.$val->id.'">'.$value->jenis->nama.'</a></div>';
	            				}
	            			}
	            		}else{
	            			$jenis_pengujian .= '<div class="item"><a style="color:black;" class="ui teal mini icon eye pengujian" data-tooltip="Lihat Data" data-position="top center" data-id="0">'.$value->jenis->nama.'</a></div>';
	            		}
	            	}
	            	$jenis_pengujian .='</div>';
            	}
            	return $jenis_pengujian;
            })
            ->editColumn('no_va', function($record){
                return $record->no_va;
            })
            ->editColumn('nominal_va', function($record){
                return FormatNumber($record->nominal);
            })
            ->editColumn('tgl_aktif', function($record){
                return DateToStringYear($record->tgl_aktif);
            })
            ->editColumn('tgl_kadaluarsa', function($record){
                return DateToStringYear($record->tgl_kadaluarsa);
            })
            ->editColumn('detil', function($record){
                $btn = '';

				$btn .= $this->makeButton([
					'type' => 'url',
					'tooltip' => 'Detil Data',
					'class' => 'ui teal mini icon button',
					'label' => '<i class="eye icon"></i>',
					'url' => url($this->link.$record->surat_id.'/detil')
				]);

                return $btn;
            })
            ->addColumn('action', function ($record) use ($link) {
                $btn = '';
            	$btn .= $this->makeButton([
            		'type' => 'modal',
            		'class'   => 'undo outline orange icon aktivasi',
            		'label'   => '<i class="undo outline icon"></i>',
            		'tooltip' => 'Aktivasi Ulang',
            		'datas' => [
            			'id' => $record->id,
            			'modalsize' => 'mini',
            		],
            		'id'   => $record->id,
            		'modalsize' => 'mini',
            	]);
                return $btn;
            })
            ->editColumn('status', function($record){
            	$status ='';
            	if($record->status == 1){
            		$status = '<a class="ui green tag label">Aktif</a>';
            	}else{
            		$status = '<a class="ui red tag label">Tidak Aktif</a>';
            	}
                return $status;
            })
            ->rawColumns(['pemesan','jenis_pengujian','action','detil','status','layanan','keterangan'])
            ->make(true);
    }

    public function download(Request $request)
    {
    	if(auth()->user()->hasRole(['keuangan','admin'])){
	        $records = VirtualAccount::whereNotNull('download_by')
	        						  ->where('tipe_customer', '!=', 0)
	        						  ->where('tipe', 0)
                                      ->whereNull('parent_id')
	                                  ->select('*');

	        if (!isset(request()->order[0]['column'])) {
	            $records->orderBy('created_at', 'desc');
	        }

            if ($no_va = $request->no_va) {
                $records->where('no_va','like','%'.$no_va.'%')
                        ->orWhereHas('groups', function($query) use ($no_va) {
                            $query->where('no_va','like','%'.$no_va.'%');
                        });
            }

            if ($no_surat = $request->no_surat) {
                $records->whereHas('surat', function($surat) use ($no_surat){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                        $kaji->whereHas('pp', function($pp) use ($no_surat){
                            $pp->where('no_surat','like', '%'.$no_surat.'%');
                        });
                    });
                })->orWhereHas('groups', function($group) use ($no_surat){
                    $group->whereHas('surat', function($surat) use ($no_surat){
                        $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                            $kaji->whereHas('pp', function($pp) use ($no_surat){
                                $pp->where('no_surat','like', '%'.$no_surat.'%');
                            });
                        });
                    });
                });
            }
            if ($no_order = $request->no_order) {
                $records->whereHas('surat', function($surat) use ($no_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                        $kaji->whereHas('pp', function($pp) use ($no_order){
                            $pp->where('no_order','like', '%'.$no_order.'%');
                        });
                    });
                })->orWhereHas('groups', function($group) use ($no_order){
                    $group->whereHas('surat', function($surat) use ($no_order){
                        $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                            $kaji->whereHas('pp', function($pp) use ($no_order){
                                $pp->where('no_order','like', '%'.$no_order.'%');
                            });
                        });
                    });
                });
            }
            if ($tanggal_order = $request->tanggal_order) {
                $records->whereHas('surat', function($surat) use ($tanggal_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                        $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                        });
                    });
                })->orWhereHas('groups', function($group) use ($tanggal_order){
                    $group->whereHas('surat', function($surat) use ($tanggal_order){
                        $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                            $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                            });
                        });
                    });
                });
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                $records->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                        $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                            $pp->where('layanan_id',$jenis_pelayanan_id);
                        });
                    });
                })->orWhereHas('groups', function($group) use ($jenis_pelayanan_id){
                    $group->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                        $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                            $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                                $pp->where('layanan_id',$jenis_pelayanan_id);
                            });
                        });
                    });
                });
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                $records->whereHas('surat', function($surat) use ($perusahaan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                        $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                            $pp->whereHas('user', function($user) use ($perusahaan_id){
                                $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                                });
                            });
                        });
                    });
                })->orWhereHas('groups', function($group) use ($perusahaan_id){
                    $group->whereHas('surat', function($surat) use ($perusahaan_id){
                        $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                            $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                                $pp->whereHas('user', function($user) use ($perusahaan_id){
                                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                        $pelanggan->where('perusahaan_id',$perusahaan_id);
                                    });
                                });
                            });
                        });
                    });
                });
            }
            $records = $records->get();
    	}else{
    		$records = collect([]);
    	}

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->surat->kaji_ulang->pp->tgl_order);
            })
            ->addColumn('pemesan', function ($record) use ($request) {
                $pemesan = $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.'</br>'.$record->surat->kaji_ulang->pp->user->pelanggans->no_hp;
                return $pemesan;
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->surat->no_surat;
            })
            ->editColumn('tgl_surat', function($record){
                return DateToStringYear($record->surat->tgl_surat);
            })
            ->editColumn('no_va', function($record){
                return $record->no_va;
            })
            ->editColumn('nominal_va', function($record){
                return FormatNumber($record->nominal);
            })
            ->editColumn('action', function($record){
                $btn = '';

                $arr = '';
                
                $btn .= '
                <div class="ui mini icon riwayat-va button" data-id="'.$record->id.'"><i class="eye icon"></i>Detail</div>
                <div class="ui riwayat-va popup top center transition hidden">
                  <div class="ui one column grid">
                    <div class="column">
                    No Va<br>';
                    if($record->groups){
                        $btn .= $record->no_va.'<br>';
                        $arr .= $record->id.',';
                        foreach ($record->groups as $value) {
                          $btn .= $value->no_va.'<br>';
                            $arr .= $value->id.',';
                        }
                    }else{
                        $btn .= $record->no_va.'<br>';
                        $arr .= $record->id.',';
                    }
                $btn .='
                    </div>
                  </div>
                </div>
                ';

                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Download',
                    'class' => 'ui blue mini icon button',
                    'label' => '<i class="download icon"></i>',
                    'url' => url($this->link).'/download/'.$arr
                ]);
                return $btn;
            })
            ->editColumn('tgl_download', function($record){
                $tgl = Carbon::createFromFormat('Y-m-d H:i:s', $record->download_date);
                return DateToStringYear($tgl->format('Y-m-d')).' '.$tgl->format('H:i').' WIB';
            })
            ->rawColumns(['pemesan','action'])
            ->make(true);
    }

    public function index()
    {	
    	if(auth()->user()->hasRole(['keuangan','admin'])){
	    	$satu 	= VirtualAccount::where('status', 0)
	    							 ->where('tipe_customer', '!=', 0)
	                                 ->whereNull('download_date')
	                                 ->where('tipe', 0)
	                                 ->get()->count();
	        $dua 	= VirtualAccount::whereNotNull('download_by')
	        						  ->where('status', 0)
	        						  ->where('tipe_customer', '!=', 0)
	        						  ->where('tipe', 0)
	                              	  ->get()->count();
	        $tiga 	= VirtualAccount::where('status', 1)
							         ->where('tipe_customer', '!=', 0)
							         ->where('tipe', 0)
	                                 ->get()->count();
	        $empat 	= VirtualAccount::whereIn('status', [2,3])
	        						 ->where('tipe_customer', '!=', 0)
	        						 ->where('tipe', 0)
	                              	 ->get()->count();
	    }else{
	    	$satu 	= 0;
	        $dua 	= 0;
	        $tiga 	= 0;
	        $empat 	= 0;
	    }
	    
        $data = [
            'mockup' 	=> true,
            'structs' 	=> $this->listStructs,
            'satu' 		=> $satu,
            'dua' 		=> $dua,
            'tiga' 		=> $tiga,
            'empat' 	=> $empat,
        ];
        return $this->render('modules.virtual.account.index', $data);
    }

    public function create()
    {
        return $this->render('modules.virtual.account.create');
    }

    public function preview($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function previewMultiple($id, $type)
    {
    	if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $c->filename,
                        ];
                    }
                }
                return $this->render('pdf-multiple', [
		        	'mockup' => true,
		        	'data' => $rename,
		        ]);
            }
        }
    }

    public function detailGroup($data)
    {
        $structs = [
            'listStruct5' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                /* --------------------------- */
                [
                    'data' => 'no_va',
                    'name' => 'no_va',
                    'label' => 'No VA',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'nominal_va',
                    'name' => 'nominal_va',
                    'label' => 'Nominal VA (Rp)',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "right aligned",
                    'width' => '100px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Penawaran',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_surat',
                    'name' => 'tgl_surat',
                    'label' => 'Tanggal Surat Penawaran',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'pemesan',
                    'name' => 'pemesan',
                    'label' => 'Peminta Jasa',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
            ],
        ];
        $record = VirtualAccount::find($data);
        return $this->render('modules.virtual.account.modals.detail-group',['record' => $record,'structs' => $structs]);
    }
    
    public function detail($id)
    {
        $detail = Detail::find($id);
        $pp = PendaftaranPengujian::find($id);
        return $this->render('modules.virtual.account.detail-pengujian',[
            'record' => $detail,
            'pp' => $pp,
        ]);
    }

    public function detil($id)
    {
    	$surat = SuratPenawaran::find($id);
       	$kaji_ulang = KajiUlang::find($surat->kaji_id);
       	$record = PendaftaranPengujian::whereHas('kaji_ulang', function($u){
    										$u->whereHas('surat');
    									})->where('id', $kaji_ulang->pp_id)->where('tipe_surat', 0)->get();
        $no_order = PendaftaranPengujian::whereHas('kaji_ulang', function($u){
    										$u->whereHas('surat');
    									})->where('id', $kaji_ulang->pp_id)->where('tipe_surat', 0)->pluck('no_order');
        $this->setSubtitle('No Order : '.$record->first()->no_order);
	    $this->setBreadcrumb(['Virtual Account' => '#', 'Detil' => '#']);
       	return $this->render('modules.virtual.account.detail',[
       		'record' => $record->first(), 
       		'kaji_ulang' => $kaji_ulang,
        	'surat' => $surat,
        	'no_order' => $no_order,
   		]); 
    }

    public function aktivasi($id)
    {
        $record = VirtualAccount::find($id);
        $this->setModalSize("mini");
        return $this->render('modules.virtual.account.aktivasi', [
            'record' => $record
        ]);
    }

    public function destroy($id)
    {
        return response([
            'status' => true,
        ]);
    }

    public function saveAktivasi(AktivasiUlangRequest $request, $id)
    {
    	$data_id = array();
    	DB::beginTransaction();
    	try {
    		$va 				= VirtualAccount::find($id);
    		$va->tgl_aktif 		= Carbon::parse($request->tgl_aktif)->format('Y-m-d H:i:s');
    		$va->tgl_kadaluarsa = Carbon::parse($request->tgl_kadaluarsa)->format('Y-m-d H:i:s');
    		$va->status 		= 1;
	        $va->save();

    		$act = new ActDisposisi;
            $act->parent_id = $va->surat->kaji_ulang->pp->id;
            $act->pengirim_id = auth()->user()->id;
            $act->penerima_id = auth()->user()->id;
            $act->tipe = 7;
            $act->jenis = 7;
            $act->keterangan = 'mengaktivasi Ulang <a>Virtual Account</a> : <a>'.$va->no_va.'</a>';
            $act->save();

	        $data_id[]=$id;
	        DB::commit();
        }catch (\Exception $e) {
	      DB::rollback();
	      return response([
	        'status' => 'error',
	        'message' => $e,
	        'error' => $e->getMessage(),
	      ], 500);
	    }
        return response([
            'status' => true,
            'message' => $data_id,
        ]);
    }

    public function downloadExcel($arr)
    {
        $list = explode(',', $arr);
        $records = VirtualAccount::whereIn('id', $list)->where('tipe', 0)->get();

        $time = Carbon::now()->hour.':'.Carbon::now()->minute.':'.Carbon::now()->second;
        $reports =  Excel::load(public_path('template-xls/template-va.xlsx'), function($reader) use($time, $records) {
            $reader->sheet('Sheet', function ($sheet) use ($time, $records) {

                $sheet->setWidth(array('A'=>6,'B'=>15,'C'=>15,'D'=>60,'E'=>20,'F'=>45,'G'=>20,'H'=>20,'I'=>20,'J'=>20));
                //supaya bisa di read ketika di upload
                $arrayTitle = ['no', 'no_order', 'tgl_order', 'layanan', 'jenis_pengujian', 'peminta_jasa', 'no_wbs_io', 'no_va', 'nominal_va', 'status'];
                $sheet->row(1,$arrayTitle);
                $sheet->getRowDimension(1)->setVisible(false);

                $downloadOFF = ['Tanggal Download : '.Carbon::now()->format('d-m-Y').' '.$time.' -  Di Download Oleh : '.auth()->user()->nama];
                $sheet->row(5,$downloadOFF);
                $sheet->mergeCells('A5:J5');
                $sheet->mergeCells('A6:J6');

                $arrayTitle = ['No.', 'No Order', 'Tgl Order', 'Layanan/Lingkup', 'Jenis Pengujian', 'Peminta Jasa', 'No WBS/IO', 'No VA', 'Nominal VA (RP)', 'Status'];
                $sheet->row(7,$arrayTitle);

                $row = 8;
                $no =1;
                foreach ($records as $record) {
                    $no_order = $record->surat->kaji_ulang->pp->no_order;

                    $tgl_order = DateToStringYear($record->surat->kaji_ulang->pp->tgl_order);

                    $layanan = $record->surat->kaji_ulang->pp->pelayanan->nama.' / '.$record->surat->kaji_ulang->pp->lingkup->nama;

                    $jenis_pengujian = '';
                    if($record->surat->kaji_ulang->pp->detail){
                        foreach ($record->surat->kaji_ulang->pp->detail as $key => $value) {
                            $kaji_ulang = KajiUlang::find($record->surat->kaji_ulang->id);
                            if($kaji_ulang){
                                foreach ($kaji_ulang->detail as $kuy => $val) {
                                    if($val->jenis_id == $value->id){
                                        $jenis_pengujian .= $value->jenis->nama;
                                    }
                                }
                            }else{
                                $jenis_pengujian .= $value->jenis->nama;
                            }
                        }
                    }

                    $pemesan = $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama.' - '.$record->surat->kaji_ulang->pp->user->pelanggans->nama_lengkap.' - '.$record->surat->kaji_ulang->pp->user->pelanggans->no_hp;

                    $sheet->row($row, array(
                        $no,
                        $no_order,
                        $tgl_order,
                        $layanan,
                        $jenis_pengujian,
                        $pemesan,
                        $record->wbs_io ? $record->wbs_io : '-',
                        $record->no_va,
                        number_format($record->nominal),
                        'Menunggu Aktivasi',
                        ));
                    $row=$row;
                    $row++;$no++;
                }

                $sheet->getStyle('I8:I'.$row)->getAlignment()->applyFromArray(
                    array('horizontal' => 'right')
                );
            });
        });

        //set status downloaded
        foreach ($records as $key => $data) {
            VirtualAccount::setDownloaded($data->id);
        }
        
        $reports->setFileName("Rekap-VA")->download('xls');
        return response([
            'status' => true,
        ]);
    }

    public function downloadExcelNewFormat($arr)
    {
        $list = explode(',', $arr);
        $records = VirtualAccount::whereIn('id', $list)->where('tipe', 0)->get();

        $format = '89319_'.Carbon::now()->format('Ymd');
        $reports = Excel::create($format, function($excel) use ($format,$records) {
            $excel->sheet($format, function($sheet) use ($records) {
                $header = ['No Virtual','KEY1','KEY2','CCY','Nama Perusahaan','Nomor Surat Penawaran','Nominal','Nomor VA','Nama Pekerjaan'];
                for ($i=6; $i <=25 ; $i++) { 
                    $nom = sprintf("%02d", $i);
                    array_push($header,"B_INFO".$nom);
                }
                array_push($header,"PERIOD_OPEN","PERIOD_CLOSE");
                for ($i=1; $i <=25 ; $i++) { 
                    $nom = sprintf("%02d", $i);
                    array_push($header,"SUBBILL_".$nom);
                }
                array_push($header,"END_RECORD");

                $sheet->row(1, $header);
                $sheet->setWidth(array('B'=>6,'C'=>6,'D'=>5,'E'=>16,'F'=>22,'I'=>16,'J'=>3,'K'=>3,'L'=>3,'M'=>3,'N'=>3,'O'=>3,'P'=>3,'Q'=>3,'R'=>3,'S'=>3,'T'=>3,'U'=>3,'V'=>3,'W'=>3,'X'=>3,'Y'=>3,'Z'=>3,'AA'=>3,'AB'=>3,'AC'=>3));

                $sheet->setAutoSize(array(
                    'A', 'G','H'
                ));

                $sheet->cells('A1', function($cells) {
                    $cells->setBackground('#FFFF00');
                });
                $sheet->cells('E1', function($cells) {
                    $cells->setBackground('#FFFF00');
                });
                $sheet->cells('AD1:BE1', function($cells) {
                    $cells->setBackground('#FFFF00');
                });

                $sheet->setColumnFormat(array(
                    'A' => '@',
                    // 'G' => '0',
                    'H' => '@',
                ));

                $row = 2;
                foreach ($records as $record) {
                	$seri = ($record->surat->kaji_ulang->pp->pelayanan->seri -1);
                	if($seri < 10){
                		$seri = '00'.$seri;
                	}elseif($seri > 10 && $seri < 100){
                		$seri = '0'.$seri;
                	}elseif($seri >= 100){
                		$seri = $seri;
                	}else{
                		$seri = 000;
                	}


                    $jenis_pengujian = '';
                    if($record->surat->kaji_ulang->pp->detail){
                        foreach ($record->surat->kaji_ulang->pp->detail as $key => $value) {
                            $kaji_ulang = KajiUlang::find($record->surat->kaji_ulang->id);
                            if($kaji_ulang){
                                foreach ($kaji_ulang->detail as $kuy => $val) {
                                    if($val->jenis_id == $value->id){
                                        $jenis_pengujian .= $value->jenis->nama;
                                    }
                                }
                            }else{
                                $jenis_pengujian .= $value->jenis->nama;
                            }
                        }
                    }
                    $angka = str_replace(',00', '', number_format($record->surat->total, 2, ',', ''));
                    $nomor = "'".$record->no_va;
                    $isi = [str_replace("/'",".",$nomor),
                            '',
                            '',
                            'IDR',
                            $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama,
                            str_replace("/",".",$record->surat->no_surat),
                            str_replace(array('\'', '"'), '', $angka),
                            str_replace("/'",".",$nomor),
                            $jenis_pengujian
                        ];
                    for ($i=6; $i <=25 ; $i++) { 
                        array_push($isi,"");
                    }
                    array_push($isi,Carbon::parse($record->tgl_aktif)->format('Ymd'));
                    array_push($isi,Carbon::parse($record->tgl_kadaluarsa)->format('Ymd'));
                    $string = '01\TOTAL\TOTAL\1';
                    array_push($isi,$string);
                    for ($i=2; $i <=25 ; $i++) { 
                        array_push($isi,'\\\\\\');
                    }
                    array_push($isi,'~');


                    $sheet->row($row, $isi);
                    $sheet->cells('A'.$row, function($cells) {
                        $cells->setBackground('#FFFF00');
                    });

                    $sheet->cells('E'.$row, function($cells) {
                        $cells->setBackground('#FFFF00');
                    });

                    $sheet->cells('AD'.$row.':BE'.$row, function($cells) {
                        $cells->setBackground('#FFFF00');
                    });
                    $row++;
                }

            });
        });

        //set status downloaded
        $parent = null;
        $count = 0;
        foreach ($records as $key => $data) {
            VirtualAccount::setDownloaded($data->id);
            if($count>0){
                $va = VirtualAccount::find($data->id);
                $va->parent_id = $parent;
                $va->save();
            }else{
                $parent = $data->id;
            }
            $va = VirtualAccount::find($data->id);
            $act = new ActDisposisi;
            $act->parent_id = $va->surat->kaji_ulang->pp->id;
            $act->pengirim_id = auth()->user()->id;
            $act->penerima_id = auth()->user()->id;
            $act->tipe = 7;
            $act->jenis = 7;
            $act->keterangan = 'mengaktivasi <a>Virtual Account</a> : <a>'.$va->no_va.'</a>, saat ini surat masuk ke tahap <a> Konfirmasi Pembayaran</a>';
            $act->save();
            
            $count++;
        }
        
        $reports->download('xls');
        return response([
            'status' => true,
        ]);
    }

    public function downloadExcelNewFormatUlang($arr)
    {
        $list = explode(',', $arr);
        $records = VirtualAccount::whereIn('id', $list)->where('tipe', 0)->get();

        $format = '89319_'.Carbon::now()->format('Ymd');
        $reports = Excel::create($format, function($excel) use ($format,$records) {
            $excel->sheet($format, function($sheet) use ($records) {
                $header = ['No Virtual','KEY1','KEY2','CCY','Nama Perusahaan','Nomor Surat Penawaran','Nominal','Nomor VA','Nama Pekerjaan'];
                for ($i=6; $i <=25 ; $i++) { 
                    $nom = sprintf("%02d", $i);
                    array_push($header,"B_INFO".$nom);
                }
                array_push($header,"PERIOD_OPEN","PERIOD_CLOSE");
                for ($i=1; $i <=25 ; $i++) { 
                    $nom = sprintf("%02d", $i);
                    array_push($header,"SUBBILL_".$nom);
                }
                array_push($header,"END_RECORD");

                $sheet->row(1, $header);
                $sheet->setWidth(array('B'=>6,'C'=>6,'D'=>5,'E'=>16,'F'=>22,'I'=>16,'J'=>3,'K'=>3,'L'=>3,'M'=>3,'N'=>3,'O'=>3,'P'=>3,'Q'=>3,'R'=>3,'S'=>3,'T'=>3,'U'=>3,'V'=>3,'W'=>3,'X'=>3,'Y'=>3,'Z'=>3,'AA'=>3,'AB'=>3,'AC'=>3));

                $sheet->setAutoSize(array(
                    'A', 'G','H'
                ));

                $sheet->cells('A1', function($cells) {
                    $cells->setBackground('#FFFF00');
                });
                $sheet->cells('E1', function($cells) {
                    $cells->setBackground('#FFFF00');
                });
                $sheet->cells('AD1:BE1', function($cells) {
                    $cells->setBackground('#FFFF00');
                });

                $sheet->setColumnFormat(array(
                    'A' => '0',
                    // 'G' => '0',
                    'H' => '0',
                ));

                $row = 2;
                foreach ($records as $record) {
                    $no_virtual = "'89319".sprintf("%02d",$record->surat->kaji_ulang->pp->pelayanan->id).$record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kode.Carbon::now()->format('y').sprintf("%03d",$record->id);

                    $jenis_pengujian = '';
                    if($record->surat->kaji_ulang->pp->detail){
                        foreach ($record->surat->kaji_ulang->pp->detail as $key => $value) {
                            $kaji_ulang = KajiUlang::find($record->surat->kaji_ulang->id);
                            if($kaji_ulang){
                                foreach ($kaji_ulang->detail as $kuy => $val) {
                                    if($val->jenis_id == $value->id){
                                        $jenis_pengujian .= $value->jenis->nama;
                                    }
                                }
                            }else{
                                $jenis_pengujian .= $value->jenis->nama;
                            }
                        }
                    }
                    $angka = str_replace(',00', '', number_format($record->surat->total, 2, ',', ''));

                    $isi = [str_replace(array('\'', '"'), '', $record->no_va),
                            '',
                            '',
                            'IDR',
                            $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama,
                            str_replace("/",".",$record->surat->no_surat),
                            str_replace(array('\'', '"'), '', $angka),
                            str_replace(array('\'', '"'), '', $record->no_va),
                            $jenis_pengujian
                        ];

                    for ($i=6; $i <=25 ; $i++) { 
                        array_push($isi,"");
                    }
                    array_push($isi,Carbon::parse($record->tgl_aktif)->format('Ymd'));
                    array_push($isi,Carbon::parse($record->tgl_kadaluarsa)->format('Ymd'));
                    $string = '01\TOTAL\TOTAL\1';
                    array_push($isi,$string);
                    for ($i=2; $i <=25 ; $i++) { 
                        array_push($isi,'\\\\\\');
                    }
                    array_push($isi,'~');


                    $sheet->row($row, $isi);
                    $sheet->cells('A'.$row, function($cells) {
                        $cells->setBackground('#FFFF00');
                    });

                    $sheet->cells('E'.$row, function($cells) {
                        $cells->setBackground('#FFFF00');
                    });

                    $sheet->cells('AD'.$row.':BE'.$row, function($cells) {
                        $cells->setBackground('#FFFF00');
                    });
                    $row++;
                }

            });
        });

        //set status downloaded
        $parent = null;
        $count = 0;
        foreach ($records as $key => $data) {
            VirtualAccount::setDownloaded($data->id);
            if($count>0){
                $va = VirtualAccount::find($data->id);
                $va->parent_id = $parent;
                $va->save();
            }else{
                $parent = $data->id;
            }
            $count++;
        }
        
        $reports->download('xls');
        return response([
            'status' => true,
        ]);
    }

    public function show()
    {
        return $this->render('modules.virtual.account.upload');
    }

    public function updateVa()
    {
    	DB::beginTransaction();
    	try {
    		$due = Carbon::now()->format("Y-m-d");
    		$vas = VirtualAccount::where(\DB::raw('cast(tgl_kadaluarsa as date)'),'<=', $due)
                             ->where('status', 1);
            if($vas->count() > 0){
           		$vas->update(['status' => 2, 'updated_at' => Carbon::now()]);
        	}else{
        	}
	        DB::commit();
        }catch (\Exception $e) {
	      DB::rollback();
	      return response([
	        'status' => 'error',
	        'message' => $e,
	        'error' => $e->getMessage(),
	      ], 500);
	    }
        return response([
            'status' => true,
        ]);
    }
}
