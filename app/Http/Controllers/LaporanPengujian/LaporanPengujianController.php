<?php

namespace App\Http\Controllers\LaporanPengujian;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Models\LaporanPengujian\LaporanPengujian;
use App\Models\LaporanPengujian\LaporanPengujianDetail;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\Files;

/* Models */
use App\Models\Authentication\Role;

/* Libraries */
use DataTables;
use Carbon;
use Hash;

class LaporanPengujianController extends Controller
{
    protected $link = 'laporan-pengujian/';
    protected $perms = 'laporan-pengujian';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan Pengujian");
        $this->setPerms($this->perms);
        $this->setModalSize("large");
        $this->setBreadcrumb(['Laporan Pengujian' => '#', 'Laporan Pengujian' => '#']);

        // Header Grid Datatable
        $this->listStructs = [
            'listStruct' => [
	            [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian dan Merk',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'fpp_fpk',
                    'name' => 'fpp_fpk',
                    'label' => 'No FPP/FPK',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'no_laporan',
                    'name' => 'no_laporan',
                    'label' => 'No Laporan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_laporan',
                    'name' => 'tgl_laporan',
                    'label' => 'Tanggal Laporan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '100px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                ],
            ],
        ];
    }

    public function grid(Request $request)
    {
    	$user = auth()->user();
    	if($user->hasRole(['srm-prosmkal','yan-kalibrasi', 'msb-kalibrasi', 'adminlab-kalibrasi', 'asman-dal-kalibrasi', 'asman-lola-kalibrasi', 'pelaksana-kalibrasi','admin'])){
	        $records = LaporanPengujian::whereHas('pengujian', function($u){
	        								$u->where('tipe', 1);
	        							})
	        							->when($no_order = $request->no_order, function($q) use ($no_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
	                                            $pp->where('no_order','like','%'.$no_order.'%');
	                                         });
	                                    })
	                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
	                                            $pp->where('no_surat','like','%'.$no_surat.'%');
	                                         });
	                                    })
	                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
	                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
	                                         });
	                                    })
	                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
	                                            $pp->where('layanan_id',$jenis_pelayanan_id);
	                                         });
	                                    })
	                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($no_wbs){
	                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
	                                         });
	                                    })->select('*');
    	}elseif($user->hasRole(['msb-siskit', 'adminlab-siskit', 'asman-dal-siskit', 'asman-lola-siskit', 'pelaksana-siskit','admin'])){
    		$records = LaporanPengujian::whereHas('pengujian', function($u){
	        								$u->where('tipe', 2);
	        							})
	        							->when($no_order = $request->no_order, function($q) use ($no_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
	                                            $pp->where('no_order','like','%'.$no_order.'%');
	                                         });
	                                    })
	                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
	                                            $pp->where('no_surat','like','%'.$no_surat.'%');
	                                         });
	                                    })
	                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
	                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
	                                         });
	                                    })
	                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
	                                            $pp->where('layanan_id',$jenis_pelayanan_id);
	                                         });
	                                    })
	                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($no_wbs){
	                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
	                                         });
	                                    })->select('*');
    	}elseif($user->hasRole(['msb-sistgi', 'adminlab-sistgi', 'asman-dal-sistgi', 'asman-lola-sistgi', 'pelaksana-sistgi','admin'])){
    		$records = LaporanPengujian::whereHas('pengujian', function($u){
	        								$u->where('tipe', 3);
	        							})
	        							->when($no_order = $request->no_order, function($q) use ($no_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
	                                            $pp->where('no_order','like','%'.$no_order.'%');
	                                         });
	                                    })
	                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
	                                            $pp->where('no_surat','like','%'.$no_surat.'%');
	                                         });
	                                    })
	                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
	                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
	                                         });
	                                    })
	                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
	                                            $pp->where('layanan_id',$jenis_pelayanan_id);
	                                         });
	                                    })
	                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($no_wbs){
	                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
	                                         });
	                                    })->select('*');
    	}elseif($user->hasRole(['msb-tegangan-rendah', 'adminlab-tegangan-rendah', 'asman-dal-tegangan-rendah', 'asman-lola-tegangan-rendah', 'pelaksana-tegangan-rendah','admin'])){
    		$records = LaporanPengujian::whereHas('pengujian', function($u){
	        								$u->where('tipe', 4);
	        							})
	        							->when($no_order = $request->no_order, function($q) use ($no_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
	                                            $pp->where('no_order','like','%'.$no_order.'%');
	                                         });
	                                    })
	                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
	                                            $pp->where('no_surat','like','%'.$no_surat.'%');
	                                         });
	                                    })
	                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
	                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
	                                         });
	                                    })
	                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
	                                            $pp->where('layanan_id',$jenis_pelayanan_id);
	                                         });
	                                    })
	                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($no_wbs){
	                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
	                                         });
	                                    })->select('*');
    	}elseif($user->hasRole(['msb-tegangan-tinggi', 'adminlab-tegangan-tinggi', 'asman-dal-tegangan-tinggi', 'asman-lola-tegangan-tinggi', 'pelaksana-tegangan-tinggi','admin'])){
    		$records = LaporanPengujian::whereHas('pengujian', function($u){
	        								$u->where('tipe', 5);
	        							})
	        							->when($no_order = $request->no_order, function($q) use ($no_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
	                                            $pp->where('no_order','like','%'.$no_order.'%');
	                                         });
	                                    })
	                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
	                                            $pp->where('no_surat','like','%'.$no_surat.'%');
	                                         });
	                                    })
	                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
	                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
	                                         });
	                                    })
	                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
	                                            $pp->where('layanan_id',$jenis_pelayanan_id);
	                                         });
	                                    })
	                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($no_wbs){
	                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
	                                         });
	                                    })->select('*');
    	}elseif($user->hasRole(['yan-uji', 'srm-uji','admin'])){
    		$records = LaporanPengujian::whereHas('pengujian', function($u){
	        								$u->whereIn('tipe', [2,3,4,5]);
	        							})
	        							->when($no_order = $request->no_order, function($q) use ($no_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
	                                            $pp->where('no_order','like','%'.$no_order.'%');
	                                         });
	                                    })
	                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
	                                            $pp->where('no_surat','like','%'.$no_surat.'%');
	                                         });
	                                    })
	                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
	                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
	                                         });
	                                    })
	                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
	                                            $pp->where('layanan_id',$jenis_pelayanan_id);
	                                         });
	                                    })
	                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($no_wbs){
	                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
	                                         });
	                                    })->select('*');
    	}elseif($user->hasRole(['gm','admin'])){
    		$records = LaporanPengujian::when($no_order = $request->no_order, function($q) use ($no_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
	                                            $pp->where('no_order','like','%'.$no_order.'%');
	                                         });
	                                    })
	                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
	                                            $pp->where('no_surat','like','%'.$no_surat.'%');
	                                         });
	                                    })
	                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
	                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
	                                         });
	                                    })
	                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
	                                            $pp->where('layanan_id',$jenis_pelayanan_id);
	                                         });
	                                    })
	                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($no_wbs){
	                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
	                                         });
	                                    })->select('*');
    	}else{
    		$records = LaporanPengujian::where('id', 0);
    	}
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }
        $records = $records->get();
        

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('no_laporan', function ($record) use ($request) {
                return $record->no_laporan;
            })
            ->editColumn('tgl_laporan', function ($record) use ($request) {
                return DateToStringYear($record->tgl_laporan);
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->detaillaporan){
                    $jenis_pengujian .= '<div class="ui ordered list list-more1" data-display="3">';
                    foreach ($record->detaillaporan as $key => $value) {
                         $jenis_pengujian .= '<div class="item">'.$value->detail_pengujian->detail_penerimaan_barang->detail_pp->jenis->nama.'-'.$value->detail_pengujian->detail_penerimaan_barang->nama_barang.'</div>';
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('wbs_io', function($record){
                return $record->pengujian->penerimaan->konfirmasi->wbs_io;
            })
            ->editColumn('fpp_fpk', function($record){
                $fpp_fpk = '';
                if($record->detaillaporan){
                    $fpp_fpk .= '<div class="ui ordered list list-more2" data-display="3">';
                    foreach ($record->detaillaporan as $key => $value) {
                         $fpp_fpk .= '<div class="item">'.$value->detail_pengujian->fpp_fpk.'</div>';
                    }
                    $fpp_fpk .='</div>';
                }
                return $fpp_fpk;
            })
            ->editColumn('detil', function($record){
                $btn = '';
                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->id.'/detil')
                ]);
                return $btn;
            })
            ->editColumn('status', function($record){
                $status = '<label class="ui orange tag label">Menunggu Dikirim</label>';
                if($record->status==1){
                    $status = '<label class="ui green tag label">Terkirim</label>';
                }
                return $status;
            })
            ->addColumn('action', function ($record) use ($link){
            	$user = auth()->user();
                $btn = '';
                if(isset($record->files)){
    				$btn .= $this->makeButton([
    					'type' => 'url',
    					'tooltip' => 'Download Laporan',
    					'class' => 'ui orange mini icon button',
    					'label' => '<i class="download icon"></i>',
    					'url' => url('download', $record->id).'/laporan-pengujian'
    				]);
            	}else{
	            	$btn .= '-';
            	}
                return $btn;
            })
            ->rawColumns(['action','status','detil','jenis_pengujian','layanan_lingkup','fpp_fpk'])
            ->make(true);
    }

    public function index()
    {
	    $user = auth()->user();
    	if($user->hasRole(['srm-prosmkal','yan-kalibrasi', 'msb-kalibrasi', 'adminlab-kalibrasi', 'asman-dal-kalibrasi', 'asman-lola-kalibrasi', 'pelaksana-kalibrasi','admin'])){
	        $satu = LaporanPengujian::whereHas('pengujian', function($u){
	        								$u->where('tipe', 1);
	        							})->get()->count();
    	}elseif($user->hasRole(['msb-siskit', 'adminlab-siskit', 'asman-dal-siskit', 'asman-lola-siskit', 'pelaksana-siskit','admin'])){
    		$satu = LaporanPengujian::whereHas('pengujian', function($u){
	        								$u->where('tipe', 2);
	        							})->get()->count();
    	}elseif($user->hasRole(['msb-sistgi', 'adminlab-sistgi', 'asman-dal-sistgi', 'asman-lola-sistgi', 'pelaksana-sistgi','admin'])){
    		$satu = LaporanPengujian::whereHas('pengujian', function($u){
	        								$u->where('tipe', 3);
	        							})->get()->count();
    	}elseif($user->hasRole(['msb-tegangan-rendah', 'adminlab-tegangan-rendah', 'asman-dal-tegangan-rendah', 'asman-lola-tegangan-rendah', 'pelaksana-tegangan-rendah','admin'])){
    		$satu = LaporanPengujian::whereHas('pengujian', function($u){
	        								$u->where('tipe', 4);
	        							})->get()->count();
    	}elseif($user->hasRole(['msb-tegangan-tinggi', 'adminlab-tegangan-tinggi', 'asman-dal-tegangan-tinggi', 'asman-lola-tegangan-tinggi', 'pelaksana-tegangan-tinggi','admin'])){
    		$satu = LaporanPengujian::whereHas('pengujian', function($u){
	        								$u->where('tipe', 5);
	        							})->get()->count();
    	}elseif($user->hasRole(['yan-uji', 'srm-uji','admin'])){
    		$satu = LaporanPengujian::whereHas('pengujian', function($u){
	        								$u->whereIn('tipe', [2,3,4,5]);
	        							})->get()->count();
    	}elseif($user->hasRole(['gm','admin'])){
    		$satu = LaporanPengujian::get()->count();
    	}else{
    		$satu = LaporanPengujian::where('id', 0)->get()->count();;
    	}

    	$data = [
            'mockup'    => true,
            'structs'   => $this->listStructs,
            'satu'   	=> $satu,
        ];
        return $this->render('modules.laporan-pengujian.index', $data);
    }

    public function create()
    {
        return $this->render('modules.laporan-pengujian.create');
    }

    public function store(Request $request)
    {
        //
    }

    public function edit($id)
    {
        return $this->render('modules.laporan-pengujian.edit');
    }

    public function show($id)
    {
        return $this->render('modules.laporan-pengujian.detail');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function preview($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function previewMultiple($id, $type)
    {
    	if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $c->filename,
                        ];
                    }
                }
                return $this->render('pdf-multiple', [
		        	'mockup' => true,
		        	'data' => $rename,
		        ]);
            }
        }
    }

    public function download($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

    public function detil($id)
    {
        $record = LaporanPengujian::find($id);
        $this->setSubtitle('No WBS/IO : '.$record->pengujian->penerimaan->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Pengiriman Laporan' => '#', 'Detil Pengiriman Laporan' => '#']);
        $lab = $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id;
        if($lab == 1){
	        return $this->render('modules.laporan-pengujian.detail-kalibrasi',[
	            'record' => $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp,
	            'kaji_ulang' => $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang,
	            'surat' => $record->pengujian->penerimaan->konfirmasi->va->surat,
	            'pengujian' => $record->pengujian,
	        ]);
        }else{
        	return $this->render('modules.laporan-pengujian.detail-uji',[
	            'record' => $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp,
	            'kaji_ulang' => $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang,
	            'surat' => $record->pengujian->penerimaan->konfirmasi->va->surat,
	            'pengujian' => $record->pengujian,
	        ]);
        }
    }
}
