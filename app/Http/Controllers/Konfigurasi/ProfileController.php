<?php

namespace App\Http\Controllers\Konfigurasi;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Konfigurasi\ProfileRequest;

/* Models */
use App\Models\User;
use App\Models\UsersDetail;
// use App\Models\WorkingPermit\WorkingPermit;
 
/* Libraries */
use Datatables;
use Entrust;
use Carbon\Carbon;
use Hash;

class ProfileController extends Controller
{
    protected $link = '/backend/profile/';
    // protected $perms = 'konfigurasi-profile';

    function __construct()
    {
        $this->setLink($this->link);
        // $this->setPerms($this->perms);
        $this->setTitle("Profil");
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Profile Pengguna' => '#']);
    }

    public function index()
    {
        $user = auth()->user();
        // $ongoing   = WorkingPermit::relatedTo($user)
        //                           ->onGoing()
        //                           ->get();
        // $done      = WorkingPermit::relatedTo($user)
        //                           ->done()
        //                           ->get();

        return $this->render('modules.konfigurasi.profile.index', [
            'user'   => $user, 
            // 'ongoing'   => $ongoing,
            // 'done'      => $done
        ]);
    }

    public function store(ProfileRequest $request)
    {
        // Data User
        $user = auth()->user();
        $user->username = $request->username;
        $user->nama = $request->nama_lengkap;
        $user->email = $request->email;
        if($file = $request->photo){
            $photo = $file->store('uploads/user', 'public');
            $user->photo = $photo;
        }
        if($request->old_password && !Hash::check($request->old_password, $user->password)){
            return response([
                'message' => 'Password Lama tidak sesuai',
                'errors' => [
                    'old_password' => ['Password Lama tidak sesuai']
                ]
            ], 422);
        }elseif($request->password && $request->password == $request->confirm_password){
            $user->password = bcrypt($request->password);
        }
            
        // $user->password = bcrypt($request->password);
        // $user->last_activity = Carbon::now();
        $user->save();

        $detail = auth()->user()->detail;
        if(is_null($detail)){
            $detail = new UsersDetail;
            $detail->user_id = $user->id;
        }
        $detail->nama_lengkap = $request->nama_lengkap;
        $detail->nip = $request->nip;
        $detail->jenis_pelayanan_id = $request->jenis_pelayanan_id;
        $detail->jabatan = $request->jabatan;
        $detail->alamat = $request->alamat;
        $detail->no_tlp = $request->no_tlp;
        $detail->save();
        
        return response([
            'status' => true
        ]);
    }

    public function reads()
    {
        auth()->user()->unreadNotifications->markAsRead();

        return response([
            'status' => true
        ]);
    }
}
