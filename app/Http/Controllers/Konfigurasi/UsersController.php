<?php

namespace App\Http\Controllers\Konfigurasi;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Konfigurasi\UsersRequest;

/* Models */
use App\Models\Authentication\User;
use App\Models\Master\Perusahaan;
use App\Models\Master\Pelanggan;
use App\Models\Users;
use App\Models\UsersDetail;

/* Libraries */
use DataTables;
use Entrust;
use Carbon;
use Hash;
use DB;

class UsersController extends Controller
{
    protected $link = 'konfigurasi/users/';
    protected $perms = 'konfigurasi-users';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Manajemen Pengguna");
        $this->setModalSize("small");
        $this->setBreadcrumb(['Konfigurasi' => '#', 'Manajemen Pengguna' => '#']);

        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'username',
                'name' => 'username',
                'label' => 'Username',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'email',
                'name' => 'email',
                'label' => 'Email',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'roles',
                'name' => 'roles',
                'label' => 'Hak Akses',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'last_login',
                'name' => 'last_login',
                'label' => 'Login Terakhir',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '150px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = User::doesnthave('pelanggan')->select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($username = $request->username) {
            $records->where('username', 'like', '%' . $username . '%');
        }
        
        if ($email = $request->email) {
            $records->where('email', 'like', '%' . $email . '%');
        }

        if ($role = $request->role) {
            $records->whereHas('roles', function($query) use ($role){
                $query->where('id',$role);
            });
        }

        $records = $records->get();

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('roles', function ($record) {
                $roles = '';
                foreach ($record->roles as $i => $role) {
                    $roles .= $role->display_name;
                    if($i < $record->roles->count() - 1){
                        $roles .= ', ';
                    }
                }
                return $roles;
            })
            ->editColumn('last_login', function ($record) {
                // return $record->last_login->formatLocalized("%d %B");
                return $record->last_login->diffForHumans();
            })
            ->editColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) use ($link){
                $btn = '';
                
                $btn .= $this->makeButton([
                    'type' => 'modal',
                    'datas' => [
                        'id' => $record->id
                    ],
                    'disabled' => isset($this->perms) && $this->perms != '' && !Entrust::can($this->perms.'-edit'),
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id,
                    'url'   => url($link.$record->id)
                ]);

                return $btn;
            })
            ->make(true);
    }

    public function index()
    {
        return $this->render('modules.konfigurasi.users.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.konfigurasi.users.create');
    }

    public function store(UsersRequest $request)
    {
    	DB::beginTransaction();
        try {
	        $recordUser = new User;
	        $recordUser->fill($request->all());
	        $recordUser->password = bcrypt($request->password);
	        $recordUser->last_login = Carbon::now();
	        $recordUser->nama = $request->nama_lengkap;
	        $recordUser->save();
	        $recordUser->roles()->sync($request->roles);

	        $request['user_id'] = $recordUser->id;
	        $newRecord = UsersDetail::saveData($request);

            DB::commit();
            return response([
              'status' => true
            ]);
        }catch (\Exception $e) {
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function edit($id)
    {
        $record = User::with('detail')->find($id);
        return $this->render('modules.konfigurasi.users.edit', [
            'record' => $record
        ]);
    }

    public function update(UsersRequest $request, $id)
    {
        $record = User::find($id);
        $record->username = $request->username;
        $record->email = $request->email;
        $record->fill($request->all());
        $record->nama = $request->nama_lengkap;
        if($request->password_lama && !Hash::check($request->password_lama, $record->password)){
            return response([
                'message' => 'Password Lama tidak sesuai',
                'errors' => [
                    'password_lama' => ['Password Lama tidak sesuai']
                ]
            ], 422);
        }elseif($pass = $request->password && $request->password == $request->confirm_password){
            $record->password = bcrypt($pass);
        }
        $record->save();

        $record->roles()->sync($request->roles);

        $rec = UsersDetail::find($request->id_detail);
        if(!$rec){
            $rec = new UsersDetail;
        }
        // dd($request->all());
        $request['user_id'] = $record->id;
        $rec->fill($request->all());

        // $rec->jenis_pelayanan_id = $request->jenis_pelayanan_id;
        // $rec->nama_lengkap = $request->nama_lengkap;
        // $rec->nip = $request->nip;
        // $rec->jabatan = $request->jabatan;
        // $rec->alamat = $request->alamat;
        // $rec->no_tlp = $request->no_tlp;

        $rec->save();


        return response([
            'status' => true
        ]);
    }

    public function destroy($id)
    {
        $record = User::find($id);
        $record->detail->delete();
        $record->delete();

        return response([
            'status' => true,
        ]);
    }
}
