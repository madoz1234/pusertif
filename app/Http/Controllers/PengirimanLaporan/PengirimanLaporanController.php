<?php

namespace App\Http\Controllers\PengirimanLaporan;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;

/* Models */
use App\Models\LaporanPengujian\LaporanPengujian;
use App\Models\LaporanPengujian\LaporanPengujianDetail;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\Act\ActPengujian;
use App\Models\Authentication\Role;
use App\Models\Authentication\User;
use App\Models\Picture;
use App\Models\Files;
use Illuminate\Support\Facades\Storage;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Zipper;
class PengirimanLaporanController extends Controller
{
    protected $link = 'pengiriman-laporan/';
    protected $perms = 'pengiriman-laporan';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Pengiriman Laporan");
        $this->setPerms($this->perms);
        $this->setModalSize("tiny");
        $this->setBreadcrumb(['Pengiriman Laporan' => '#']);

        // Header Grid Datatable
        $this->listStructs = [
            'listStruct' => [
	            [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian dan Merk',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'fpp_fpk',
                    'name' => 'fpp_fpk',
                    'label' => 'No FPP/FPK',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'no_laporan',
                    'name' => 'no_laporan',
                    'label' => 'No Laporan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_laporan',
                    'name' => 'tgl_laporan',
                    'label' => 'Tanggal Laporan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                ],
            ],
            'listStruct2' => [
	            [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                [
                    'data' => 'no_order',
                    'name' => 'no_order',
                    'label' => 'No Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_order',
                    'name' => 'tgl_order',
                    'label' => 'Tgl Order',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'no_surat',
                    'name' => 'no_surat',
                    'label' => 'No Surat Permintaan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'layanan_lingkup',
                    'name' => 'layanan_lingkup',
                    'label' => 'Layanan / Lingkup',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '350px',
                ],
                [
                    'data' => 'jenis_pengujian',
                    'name' => 'jenis_pengujian',
                    'label' => 'Jenis Pengujian dan Merk',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '200px',
                ],
                [
                    'data' => 'wbs_io',
                    'name' => 'wbs_io',
                    'label' => 'No WBS / IO',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'fpp_fpk',
                    'name' => 'fpp_fpk',
                    'label' => 'No FPP/FPK',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "left aligned",
                    'width' => '300px',
                ],
                [
                    'data' => 'no_laporan',
                    'name' => 'no_laporan',
                    'label' => 'No Laporan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_laporan',
                    'name' => 'tgl_laporan',
                    'label' => 'Tanggal Laporan',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'nota_dinas',
                    'name' => 'nota_dinas',
                    'label' => 'No Nota Dinas',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'tgl_nota_dinas',
                    'name' => 'tgl_nota_dinas',
                    'label' => 'Tanggal Nota Dinas',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                    'width' => '150px',
                ],
                [
                    'data' => 'detil',
                    'name' => 'detil',
                    'label' => 'Detil',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                ],
            ],
        ];
    }

    public function grid(Request $request)
    {
    	$user = auth()->user();
    	if(auth()->user()->hasRole(['yan-kalibrasi','admin'])){
	        $records = LaporanPengujian::where('status', 0)
	        							->whereHas('pengujian.penerimaan', function($u){
	        								$u->byLayanan(1);
	        							})
	                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
	                                            $pp->where('no_order','like','%'.$no_order.'%');
	                                         });
	                                    })
	                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
	                                            $pp->where('no_surat','like','%'.$no_surat.'%');
	                                         });
	                                    })
	                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
	                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
	                                         });
	                                    })
	                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
	                                            $pp->where('layanan_id',$jenis_pelayanan_id);
	                                         });
	                                    })
	                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($no_wbs){
	                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
	                                         });
	                                    })
	                                    ->select('*');
								        if (!isset(request()->order[0]['column'])) {
								            $records->orderBy('created_at', 'desc');
								        }
                                        $records = $records->get();
    	}elseif(auth()->user()->hasRole(['yan-uji','admin'])){
    		 $records = LaporanPengujian::where('status', 0)
	        							->whereHas('pengujian.penerimaan', function($u){
	        								$u->byLayananV2([2,3,4,5]);
	        							})
	                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
	                                            $pp->where('no_order','like','%'.$no_order.'%');
	                                         });
	                                    })
	                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
	                                            $pp->where('no_surat','like','%'.$no_surat.'%');
	                                         });
	                                    })
	                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
	                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
	                                         });
	                                    })
	                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
	                                            $pp->where('layanan_id',$jenis_pelayanan_id);
	                                         });
	                                    })
	                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($no_wbs){
	                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
	                                         });
	                                    })
	                                    ->select('*');
								        if (!isset(request()->order[0]['column'])) {
								            $records->orderBy('created_at', 'desc');
								        }
                                        $records = $records->get();

    	}else{
    		$records = collect([]);
    	}

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('no_laporan', function ($record) use ($request) {
                return $record->no_laporan;
            })
            ->editColumn('tgl_laporan', function ($record) use ($request) {
                return DateToStringYear($record->tgl_laporan);
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->detaillaporan){
                    $jenis_pengujian .= '<div class="ui ordered list list-more1" data-display="3">';
                    foreach ($record->detaillaporan as $key => $value) {
                         $jenis_pengujian .= '<div class="item">'.$value->detail_pengujian->detail_penerimaan_barang->detail_pp->jenis->nama.'-'.$value->detail_pengujian->detail_penerimaan_barang->nama_barang.'</div>';
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('wbs_io', function($record){
                return $record->pengujian->penerimaan->konfirmasi->wbs_io;
            })
            ->editColumn('fpp_fpk', function($record){
                $fpp_fpk = '';
                if($record->detaillaporan){
                    $fpp_fpk .= '<div class="ui ordered list list-more2" data-display="3">';
                    foreach ($record->detaillaporan as $key => $value) {
                         $fpp_fpk .= '<div class="item">'.$value->detail_pengujian->fpp_fpk.'</div>';
                    }
                    $fpp_fpk .='</div>';
                }
                return $fpp_fpk;
            })
            ->editColumn('detil', function($record){
                $btn = '';
                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->id.'/detil')
                ]);
                return $btn;
            })
            ->editColumn('status', function($record){
                $status = '<label class="ui orange tag label">Menunggu Dikirim</label>';
                return $status;
            })
            ->addColumn('action', function ($record) use ($link){
            	$user = auth()->user();
                $checklist = '';
                if(auth()->user()->hasRole(['yan-uji', 'yan-kalibrasi'])){
                	 if($record->pengujian->penerimaan->konfirmasi->status == 1){
                	 	$checklist ='<label class="ui red tag label">Menunggu Pelunasan Pembayaran</label>';
                	 }else{
		                $checklist = '<div class="ui fitted checkbox">
		                            <input name="checklist[]" class="check" type="checkbox" data-id="'.$record->id.'">
		                            <label></label>
		                          </div>';
                	 }

            	}else{
            		$checklist = '-';
            	}
                return $checklist;
            })
            ->rawColumns(['action','status','detil','jenis_pengujian','layanan_lingkup','fpp_fpk','detil','status'])
            ->make(true);
    }

    public function historis(Request $request)
    {
    	$user = auth()->user();
    	if(auth()->user()->hasRole(['yan-kalibrasi','admin'])){
	        $records = LaporanPengujian::where('status', 1)
	        							->whereHas('pengujian.penerimaan', function($u){
	        								$u->byLayanan(1);
	        							})
	                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
	                                            $pp->where('no_order','like','%'.$no_order.'%');
	                                         });
	                                    })
	                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
	                                            $pp->where('no_surat','like','%'.$no_surat.'%');
	                                         });
	                                    })
	                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
	                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
	                                         });
	                                    })
	                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
	                                            $pp->where('layanan_id',$jenis_pelayanan_id);
	                                         });
	                                    })
	                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($no_wbs){
	                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
	                                         });
	                                    })
	                                    ->select('*');
								        if (!isset(request()->order[0]['column'])) {
								            $records->orderBy('created_at', 'desc');
								        }
                                        $records = $records->get();

    	}elseif(auth()->user()->hasRole(['yan-uji','admin'])){
    		 $records = LaporanPengujian::where('status', 1)
	        							->whereHas('pengujian.penerimaan', function($u){
	        								$u->byLayananV2([2,3,4,5]);
	        							})
	                                    ->when($no_order = $request->no_order, function($q) use ($no_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
	                                            $pp->where('no_order','like','%'.$no_order.'%');
	                                         });
	                                    })
	                                    ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
	                                            $pp->where('no_surat','like','%'.$no_surat.'%');
	                                         });
	                                    })
	                                    ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
	                                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
	                                         });
	                                    })
	                                    ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
	                                            $pp->where('layanan_id',$jenis_pelayanan_id);
	                                         });
	                                    })
	                                    ->when($no_wbs = $request->no_wbs, function($q) use ($no_wbs) {
	                                         $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($no_wbs){
	                                            $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
	                                         });
	                                    })
	                                    ->select('*');
								        if (!isset(request()->order[0]['column'])) {
								            $records->orderBy('created_at', 'desc');
								        }
                                        $records = $records->get();

    	}else{
    		$records = collect([]);
    	}

        $link = $this->link;
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('no_order', function ($record) use ($request) {
                return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order;
            })
            ->editColumn('no_laporan', function ($record) use ($request) {
                return $record->no_laporan;
            })
            ->editColumn('tgl_laporan', function ($record) use ($request) {
                return DateToStringYear($record->tgl_laporan);
            })
            ->editColumn('nota_dinas', function ($record) use ($request) {
                return $record->nota_dinas;
            })
            ->editColumn('tgl_nota_dinas', function ($record) use ($request) {
                return DateToStringYear($record->tgl_nota_dinas);
            })
            ->editColumn('tgl_order', function ($record) use ($request) {
                return DateToStringYear($record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->tgl_order);
            })
            ->editColumn('no_surat', function ($record) use ($request) {
                return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_surat;
            })
            ->addColumn('layanan_lingkup', function ($record) use ($request) {
                return $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->pelayanan->nama.' </br> '.$record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->lingkup->nama;
            })
            ->addColumn('jenis_pengujian', function ($record) use ($request) {
                $jenis_pengujian = '';
                if($record->detaillaporan){
                    $jenis_pengujian .= '<div class="ui ordered list list-more3" data-display="3">';
                    foreach ($record->detaillaporan as $key => $value) {
                         $jenis_pengujian .= '<div class="item">'.$value->detail_pengujian->detail_penerimaan_barang->detail_pp->jenis->nama.'-'.$value->detail_pengujian->detail_penerimaan_barang->nama_barang.'</div>';
                    }
                    $jenis_pengujian .='</div>';
                }
                return $jenis_pengujian;
            })
            ->editColumn('wbs_io', function($record){
                return $record->pengujian->penerimaan->konfirmasi->wbs_io;
            })
            ->editColumn('fpp_fpk', function($record){
                $fpp_fpk = '';
                if($record->detaillaporan){
                    $fpp_fpk .= '<div class="ui ordered list list-more4" data-display="3">';
                    foreach ($record->detaillaporan as $key => $value) {
                         $fpp_fpk .= '<div class="item">'.$value->detail_pengujian->fpp_fpk.'</div>';
                    }
                    $fpp_fpk .='</div>';
                }
                return $fpp_fpk;
            })
            ->editColumn('detil', function($record){
                $btn = '';
                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Detil Data',
                    'class' => 'ui teal mini icon button',
                    'label' => '<i class="eye icon"></i>',
                    'url' => url($this->link.$record->id.'/detil')
                ]);
                return $btn;
            })
            ->editColumn('status', function($record){
                $status = '<label class="ui green tag label">Terkirim</label>';
                return $status;
            })
            ->rawColumns(['status','detil','jenis_pengujian','layanan_lingkup','fpp_fpk','status','detil'])
            ->make(true);
    }

    public function index()
    {	
    	$user = auth()->user();
    	if($user->hasRole(['yan-kalibrasi','admin'])){
	    	$satu = LaporanPengujian::where('status', 0)
	    							->whereHas('pengujian.penerimaan', function($u){
	        								$u->byLayanan(1);
	        						})
	    							->get()->count();
    	}elseif($user->hasRole(['yan-uji','admin'])){
    		$satu = LaporanPengujian::where('status', 0)
	    							->whereHas('pengujian.penerimaan', function($u){
	        								$u->byLayananV2([2,3,4,5]);
	        						})
	    							->get()->count();
    	}else{
    		$satu ='';
    	}
    	$data = [
            'mockup'    => true,
            'structs'   => $this->listStructs,
            'satu'   	=> $satu,
        ];
        return $this->render('modules.pengiriman-laporan.index', $data);
    }

    public function create()
    {
        return $this->render('modules.reschedule.data.create');
    }

    public function store(Request $request)
    {
        // $record = new Role;
        // $record->fill($request->all());
        // $record->save();

        return response([
            'status' => true
        ]);
    }
 
    public function edit($id)
    {
        $record = Reschedule::find($id);
       	$this->setSubtitle('No WBS/IO : '.$record->pengujian->penerimaan->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Reschedule' => '#', 'Reschedule Jadwal' => '#']);
        return $this->render('modules.reschedule.edit',[
        	'record' => $record,
        	'detail' => $record->detailreschedule,
    	]);
    }

    public function show($id)
    {
        // $record = Role::find($id);

        return $this->render('modules.reschedule.data.show');
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
    	try {
    		$reschedule = Reschedule::find($id);
    		$reschedule->status = 1;
    		$reschedule->save();
			$reschedule->updateDetail($request->detail);
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function destroy($id)
    {
        return response([
            'status' => true,
        ]);
    }

    public function preview($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}

    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);

    }

    public function previewMultiple($id, $type)
    {
    	if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $c->filename,
                        ];
                    }
                }
                return $this->render('pdf-multiple', [
		        	'mockup' => true,
		        	'data' => $rename,
		        ]);
            }
        }
    }

    public function download($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

    public function pengirimanLaporan($arr)
    {
        return $this->render('modules.pengiriman-laporan.kirim-laporan', [
            'data' => $arr,
        ]);
    }

    public function saveKirimLaporan(Request $request, $id)
    {	
    	$user = auth()->user();
    	$list = explode(',', $request->data);
        DB::beginTransaction();
    	try {
    		foreach ($list as $key => $value) {
	    		$pengujian_detail = LaporanPengujian::find($value);
	    		$pengujian_detail->status = 1;
	    		$pengujian_detail->nota_dinas = $request->nota_dinas;
	    		$pengujian_detail->tgl_nota_dinas = $request->tgl_nota;
	    		$pengujian_detail->save();

	    		$nama_pt = $pengujian_detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama;

	    		$act = new ActPengujian;
		        $act->parent_id 		= $pengujian_detail->pengujian->detailpengujian->first()->id;
		        $act->pengirim_id 		= $user->id;
		        $act->penerima_id 		= 1;
		        $act->tipe 				= 3;
		        $act->jenis 			= 3;
		        $act->keterangan 		= '<a>'.$user->nama.'</a> mengirim Laporan Hasil Pengujian Kepada <a>'.$nama_pt.'</a> dengan No Laporan : <a>'.$pengujian_detail->no_laporan.'</a>';
		        $act->save();


                if($pengujian_detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==1){
                     $notif = User::whereHas('roles', function($role){
                            $role->where('name','yan-kalibrasi')
                            	->orWhere('name','adminlab-kalibrasi');
                        })->get();
                }
                if($pengujian_detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==2){
                    $notif = User::whereHas('roles', function($role){
                            $role->where('name','yan-uji')
                            	 ->orWhere('name','adminlab-siskit');
                        })->get();
                }
                if($pengujian_detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==3){
                    $notif = User::whereHas('roles', function($role){
                            $role->where('name','yan-uji')
                            	 ->orWhere('name','adminlab-sistgi');
                        })->get();
                }
                if($pengujian_detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==4){
                    $notif = User::whereHas('roles', function($role){
                            $role->where('name','yan-uji')
                            	 ->orWhere('name','adminlab-tegangan-rendah');
                        })->get();
                }
                if($pengujian_detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id==5){
                    $notif = User::whereHas('roles', function($role){
                            $role->where('name','yan-uji')
                            	 ->orWhere('name','adminlab-tegangan-tinggi');
                        })->get();
                }

                foreach ($notif as $val) {
                    if(!is_null($val->device_id)){
                         $this->onesignal('Pengembalian Barang Uji Baru dengan nomor order '.$pengujian_detail->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->no_order,$val->device_id,$pengujian_detail->pengujian->penerimaan->toArray(),'pengembalian-barang-uji','onprogress');
                    }
                }
    		}
    		DB::commit();
    	}catch (\Exception $e) {
    		DB::rollback();
    		return response([
    			'status' => 'error',
    			'message' => 'An error occurred!',
    			'error' => $e->getMessage(),
    		], 500);
    	}
    }

    public function detil($id)
    {
        $record = LaporanPengujian::find($id);
        $this->setSubtitle('No WBS/IO : '.$record->pengujian->penerimaan->konfirmasi->wbs_io);
        $this->setBreadcrumb(['Pengiriman Laporan' => '#', 'Detil Pengiriman Laporan' => '#']);
        $lab = $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp->layanan_id;
        if($lab == 1){
	        return $this->render('modules.pengiriman-laporan.detail-kalibrasi',[
	            'record' => $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp,
	            'kaji_ulang' => $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang,
	            'surat' => $record->pengujian->penerimaan->konfirmasi->va->surat,
	            'pengujian' => $record->pengujian,
	        ]);
        }else{
        	return $this->render('modules.pengiriman-laporan.detail-uji',[
	            'record' => $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang->pp,
	            'kaji_ulang' => $record->pengujian->penerimaan->konfirmasi->va->surat->kaji_ulang,
	            'surat' => $record->pengujian->penerimaan->konfirmasi->va->surat,
	            'pengujian' => $record->pengujian,
	        ]);
        }
    }
}
