<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FrontendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render('frontend.user.index');
    }

    public function tracking()
    {
        return $this->render('frontend.tracking.index');
    }

    public function pendaftaranPengujian()
    {
        return $this->render('frontend.pengujian.index');
    }
    
    public function daftarOrder()
    {
        return $this->render('frontend.list.index');
    }

    public function pembayaran()
    {
        return $this->render('frontend.pembayaran.index');
    }

    public function getgantipassword(Request $request)
    {
        return view('frontend.auth.index');
    }
}
