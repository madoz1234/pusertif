<?php

namespace App\Http\Controllers\API\Dashboard;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Unlu\Laravel\Api\QueryBuilder;

/* Validation */


/* Models */
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\Authentication\User;
use App\Models\LaporanPengujian\LaporanPengujian;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Zipper;


class DashboardController extends Controller
{
  public function graph(){
    $kaji = $this->graphKaji();
    $pengujian = $this->graphPengujian();
    $laporan = $this->graphLaporan();

    $graph = [
      'kaji_ulang' => $kaji,
      'pengujian' => $pengujian,
      'laporan' => $laporan,
    ];
    
    return response()->json([
        'data' => $graph,
    ]);
  }

  public function getBuble(){
    $buble = [
      'penerimaan_surat' => $this->buble('penerimaanSurat'),
      'kaji_ulang' => $this->buble('kajiUlang'),
      'surat_penawaran' => $this->buble('suratPenawaran'),
      'virtual_account' => $this->buble('virtualAccount(VA)'),
      'nota_buku' => $this->buble('notaBuku'),
      'konfirmasi_pembayaran' => $this->buble('konfirmasiPembayaran'),
      'pengembalian_dana' => $this->buble('pengembalianDana'),
      'realisasi_biaya' => $this->buble('realisasiBiaya'),
      'order_uji_serah_terima' => $this->buble('orderUjiSerahTerima'),
      'penerimaan_barang_uji' => $this->buble('penerimaanBarangUji'),
      'reschedule' => $this->buble('reschedule'),
      'pengiriman_laporan' => $this->buble('pengirimanLaporan'),
      'close_order' => $this->buble('closeOrder'),
      'pengaduan' => $this->buble('pengaduan'),
      'aktivasi_user' => $this->buble('aktivasiPemintaJasa'),
      'pengembalian_barang_uji' => $this->buble('pengembalianBarangUji'),
      'laporan_pengujian' => $this->buble('laporanPengujian'),
      'historikal_data' => $this->buble('historikalData'),
      'pengujian' => $this->bublepengujian('pengujian'),
    ];
    return response([
      'buble' => $buble
    ]);
  }

  public function buble($menu){
    $user = auth()->user();
      $online='';$ams='';$spm='';$jumlah='';$kaji='';$diskusi='';$surat='';$on_progress='';$va='';$konfirmasi='';$realisasi='';$angka='';$angka_kaji='';$angka_surat='';$angka_va='';$angka_konfirmasi='';$angka_realisasi='';$angka_nota='';$nota='';$pengembalian_dana='';$angka_dana='';

      if($menu == 'penerimaanSurat'){
        if($user->hasRole(['yan-kalibrasi'])){
          $online   = \App\Models\FrontEnd\PendaftaranPengujian::whereIn('status', [1,11,12])->where('jenis', 0)->where('tipe', 1)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
          $spm    = \App\Models\FrontEnd\PendaftaranPengujian::whereIn('status', [1,11,12])->where('jenis', 0)->where('tipe', 3)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
          $ams        = \App\Models\FrontEnd\PendaftaranPengujian::whereIn('status', [1,11,12])->where('tipe', 2)->where('status_ams', 1)->where('layanan_id', 1)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
          $jumlah   = $online + $spm + $ams; 

        $records  = \App\Models\FrontEnd\PendaftaranPengujian::isDoneKal();
        $kirim    = \App\Models\FrontEnd\PendaftaranPengujian::isKirimKal();
        $histori  = \App\Models\FrontEnd\PendaftaranPengujian::isHistoriKal();

        $surat    = count($records) + count($kirim);
        }elseif($user->hasRole(['yan-uji'])){
          $online   = \App\Models\FrontEnd\PendaftaranPengujian::whereIn('status', [1,11,12])->where('jenis', 1)->where('tipe', 1)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
          $spm    = \App\Models\FrontEnd\PendaftaranPengujian::whereIn('status', [1,11,12])->where('jenis', 1)->where('tipe', 3)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
          $ams        = \App\Models\FrontEnd\PendaftaranPengujian::whereIn('status', [1,11,12])->where('tipe', 2)->where('status_ams', 1)->whereIn('layanan_id', [2,3,4,5])->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
          $jumlah   = $online + $spm + $ams;

          $records  = \App\Models\FrontEnd\PendaftaranPengujian::isDoneUji();
        $kirim    = \App\Models\FrontEnd\PendaftaranPengujian::isKirimUji();
        $histori  = \App\Models\FrontEnd\PendaftaranPengujian::isHistoriUji();

        $surat    = count($records) + count($kirim);
        }elseif($user->hasRole(['admin'])){
                $ams        = \App\Models\FrontEnd\PendaftaranPengujian::whereIn('status', [1,11,12])->where('tipe', 2)->where('status_ams', 0)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
          $jumlah   = $ams; 
        }else{
          $jumlah   ='';
        }
        if($jumlah > 0){
          return $jumlah;
        }
      }elseif($menu == 'kajiUlang'){
        if($user->hasRole(['msb-kalibrasi', 'msb-siskit', 'msb-sistgi', 'msb-tegangan-rendah', 'msb-tegangan-tinggi'])){
          $on_progress = \App\Models\FrontEnd\PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
                            $q->where('penerima_id', $user->id)
                              ->where('jenis', 1);
                          })
                          ->whereIn('status', [2,12])
                          ->doesntHave('kaji_ulang')
                          ->where('tipe_surat', 0)
                          ->orderBy('no_order','asc')->count();
          $diskusi = \App\Models\FrontEnd\PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
                                      $q->where('penerima_id', $user->id)
                                        ->where('jenis', 1);
                                      })
                            ->where('status', 2)
                            ->whereHas('kaji_ulang', function($u){
                              $u->where('keputusan', 3);
                            })
                            ->where('tipe_surat', 0)
                            ->orderBy('no_order','asc')->count();
            $kaji     = $on_progress + $diskusi;
        }elseif($user->hasRole(['asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-tinggi','asman-dal-tegangan-rendah', 'asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-tinggi','asman-lola-tegangan-rendah'])){
          $on_progress = \App\Models\FrontEnd\PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
                            $q->where('penerima_id', $user->id)
                              ->where('jenis', 2);
                          })
                          ->where('status', 2)
                          ->doesntHave('kaji_ulang')
                          ->where('tipe_surat', 0)
                          ->orderBy('no_order','asc')->count();
          $diskusi  = \App\Models\FrontEnd\PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
                                      $q->where('pengirim_id', $user->id)
                                        ->where('jenis', 2);
                                      })
                            ->where('status', 2)
                            ->whereHas('kaji_ulang', function($u){
                              $u->where('keputusan', 3);
                            })
                            ->where('tipe_surat', 0)
                            ->orderBy('no_order','asc')->count();
            $kaji     = $on_progress + $diskusi;
        }else{
          $kaji     = '';
        }

        if($kaji > 0){
          return $kaji;
        }
      }elseif($menu == 'suratPenawaran'){
        if($user->hasRole(['yan-kalibrasi'])){
        $records  = \App\Models\FrontEnd\PendaftaranPengujian::isDoneKal();
        $kirim    = \App\Models\FrontEnd\PendaftaranPengujian::isKirimKal();
        $tolak    = \App\Models\FrontEnd\PendaftaranPengujian::isTolakKal();
        $surat    = count($records) + count($kirim) + count($tolak);
        }elseif($user->hasRole(['yan-uji'])){
          $records  = \App\Models\FrontEnd\PendaftaranPengujian::isDoneUji();
        $kirim    = \App\Models\FrontEnd\PendaftaranPengujian::isKirimUji();
        $tolak    = \App\Models\FrontEnd\PendaftaranPengujian::isTolakUji();
        $surat    = count($records) + count($kirim) + count($tolak);
        }else{
          $surat  =0;
        }
        if($surat > 0){
          return $surat;
        }
      }elseif($menu == 'virtualAccount(VA)'){
        if($user->hasRole(['keuangan'])){
          $satu   = \App\Models\VirtualAccount\VirtualAccount::where('status', 0)
                     ->whereIn('tipe_customer', [1,2])
                                   ->whereNull('download_date')
                                   ->where('tipe', 0)
                                   ->get()->count();
            $dua  = \App\Models\VirtualAccount\VirtualAccount::whereNotNull('download_by')
                          ->where('status', 0)
                          ->where('tipe_customer', '!=', 0)
                          ->where('tipe', 0)
                                      ->get()->count();
            $tiga   = \App\Models\VirtualAccount\VirtualAccount::where('status', 1)
                         ->whereIn('tipe_customer', [1,2])
                         ->where('tipe', 0)
                                     ->get()->count();
            $empat  = \App\Models\VirtualAccount\VirtualAccount::whereIn('status', [2,3])
                         ->whereIn('tipe_customer', [1,2])
                         ->where('tipe', 0)
                                     ->get()->count();
            $va   = $satu + $dua + $tiga + $empat;
        }else{
          $va='';
        }

        if($va > 0){
          return $va;
        }
      }elseif($menu == 'notaBuku'){
        if($user->hasRole(['keuangan'])){
          $nota = \App\Models\VirtualAccount\VirtualAccount::whereHas('surat', function($s){
                                    $s->whereHas('kaji_ulang', function($k){
                                        $k->whereHas('pp', function($p){
                                            return $p->whereNotIn('status', [6,7]);
                                        });
                                    });
                                 })->where('tipe_customer', 0)->where('tipe', 0)->count();
        }else{
          $nota='';
        }

        if($nota > 0){
          return $nota;
        }
      }elseif($menu == 'konfirmasiPembayaran'){
        if($user->hasRole(['keuangan'])){
          $first = \App\Models\VirtualAccount\VirtualAccount::where('status', 1)
                      ->whereIn('tipe_customer', [0,1,2])
                                  ->doesnthave('konfirmasi')
                                  ->where('tipe', 0)
                                  ->get()->count();
            $second = \App\Models\VirtualAccount\VirtualAccount::where('status', 3)
                        ->whereIn('tipe_customer', [0,1,2])
                                    ->whereHas('konfirmasi', function($q){
                                        return $q->where('status', 1);
                                    })
                                    ->where('tipe', 0)
                                    ->get()->count();
            $third = \App\Models\VirtualAccount\VirtualAccount::where('status', 3)
                        ->whereIn('tipe_customer', [0,1,2])
                                    ->whereHas('konfirmasi', function($q){
                                        return $q->where('status', 2);
                                    })
                                    ->where('tipe', 0)
                                    ->get()->count();
            $four = \App\Models\VirtualAccount\VirtualAccount::where('status', 3)
                        ->whereIn('tipe_customer', [0,1,2])
                                    ->whereHas('konfirmasi', function($q){
                                        return $q->where('status', 3);
                                    })
                                    ->where('tipe', 0)
                                    ->get()->count();
            $five = \App\Models\VirtualAccount\VirtualAccount::where('status', 3)
                        ->whereIn('tipe_customer', [0,1,2])
                        ->where('tipe', 0)
                                    ->get()->count();
                $six = \App\Models\VirtualAccount\VirtualAccount::where('status', 3)
                                        ->whereIn('tipe_customer', [0,1,2])
                                        ->whereHas('surat', function($q){
                                        $q->whereHas('kaji_ulang',function($x){
                                            $x->whereHas('pp',function($y){
                                                return $y->where('status',6);
                                            });
                                        });
                                    })
                                    ->where('tipe', 0)
                                    ->get()->count();

            $konfirmasi   = $first + $second + $third + $four + $five;
        }else{
          $konfirmasi='';
        }

        if($konfirmasi > 0){
          return $konfirmasi;
        }
      }elseif($menu == 'pengembalianDana'){
        if($user->hasRole(['keuangan'])){
          $satu = \App\Models\VirtualAccount\VirtualAccount::where('status', 3)
                                                      ->whereHas('konfirmasi',function($q) {
                                                          $q->whereHas('pengembalian',function($x) {
                                                                  $x->whereNull('tgl_kembali');
                                                              })->where('status',2);
                                                      })->where('tipe', 0)
                                                      ->get()->count();
        $dua = \App\Models\CloseOrder\CloseOrder::whereHas('penerimaan.pengujian.detailpengujian', function($u){
                                    return $u->where('verifikasi', '!=', 1);
                                  })
                                  ->doesntHave('pengembaliandana')
                                  ->get()->count();
          $pengembalian_dana= $satu + $dua;
        }else{
          $pengembalian_dana='';
        }

        if($pengembalian_dana > 0){
          return $pengembalian_dana;
        }
      }elseif($menu == 'realisasiBiaya'){
        if($user->hasRole(['keuangan'])){
          $one  = \App\Models\VirtualAccount\KonfirmasiPembayaran::where('status', 3)
                    ->whereHas('penerimaan', function($penerimaan){
                                    $penerimaan->doesnthave('closeorder');
                                })
                                ->orDoesntHave('penerimaan')
                                ->where('tipe', 0)
                    ->get()->count();
        }else{
          $one='';
        }

        if($one > 0){
          return $one;
        }
      }elseif($menu == 'orderUjiSerahTerima'){
        if($user->hasRole(['pelaksana-tegangan-rendah', 'yan-uji', 'msb-tegangan-rendah', 'asman-lola-tegangan-rendah', 'asman-dal-tegangan-rendah', 'adminlab-tegangan-rendah'])){
          $records = \App\Models\OrderUji\UjiSerahTerima::where('layanan_id', 4)
                                  ->whereHas('pp', function($u){
                                    $u->whereIn('status', [0,1,2,3,4,10]);
                                  })->get()->count();
        }elseif($user->hasRole(['pelaksana-tegangan-tinggi', 'yan-uji', 'msb-tegangan-tinggi', 'asman-lola-tegangan-tinggi', 'asman-dal-tegangan-tinggi', 'adminlab-tegangan-tinggi'])){
            $records = \App\Models\OrderUji\UjiSerahTerima::where('layanan_id', 5)
                            ->whereHas('pp', function($u){
                            $u->whereIn('status', [0,1,2,3,4,10]);
                          })->get()->count();
        }else{
          $records = 0;
        }

        if($records > 0){
          return $records;
        }
      }elseif($menu == 'penerimaanBarangUji'){
        if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['adminlab-siskit']) || auth()->user()->hasRole(['adminlab-sistgi']) || auth()->user()->hasRole(['adminlab-tegangan-rendah']) || auth()->user()->hasRole(['adminlab-tegangan-tinggi'])){
            if(auth()->user()->hasRole(['adminlab-siskit'])){
              $records = \App\Models\VirtualAccount\KonfirmasiPembayaran::where('status', '>', 0)
                               ->whereHas('va', function($z){
                                $z->whereHas('surat', function($y){
                                  $y->whereHas('kaji_ulang', function($x){
                                    $x->whereHas('pp', function($m){
                                      $m->where('jenis', 1)
                                        ->where('layanan_id', 2);
                                    });
                                  });
                                });
                              })
                              ->where('tipe', 0)
                              ->doesntHave('penerimaan')
                                          ->get()->count();
          }elseif(auth()->user()->hasRole(['adminlab-sistgi'])){
            $records = \App\Models\VirtualAccount\KonfirmasiPembayaran::where('status', '>', 0)
                               ->whereHas('va', function($z){
                                $z->whereHas('surat', function($y){
                                  $y->whereHas('kaji_ulang', function($x){
                                    $x->whereHas('pp', function($m){
                                      $m->where('jenis', 1)
                                        ->where('layanan_id', 3);
                                    });
                                  });
                                });
                              })
                              ->where('tipe', 0)
                              ->doesntHave('penerimaan')
                                          ->get()->count();
          }elseif(auth()->user()->hasRole(['adminlab-tegangan-rendah'])){
            $records = \App\Models\VirtualAccount\KonfirmasiPembayaran::where('status', '>', 0)
                               ->whereHas('va', function($z){
                                $z->whereHas('surat', function($y){
                                  $y->whereHas('kaji_ulang', function($x){
                                    $x->whereHas('pp', function($m){
                                      $m->where('jenis', 1)
                                        ->where('layanan_id', 4);
                                    });
                                  });
                                });
                              })
                              ->where('tipe', 0)
                              ->doesntHave('penerimaan')
                                          ->get()->count();
          }elseif(auth()->user()->hasRole(['adminlab-tegangan-tinggi'])){
            $records = \App\Models\VirtualAccount\KonfirmasiPembayaran::where('status', '>', 0)
                              ->whereHas('va', function($z){
                                $z->whereHas('surat', function($y){
                                  $y->whereHas('kaji_ulang', function($x){
                                    $x->whereHas('pp', function($m){
                                      $m->where('jenis', 1)
                                        ->where('layanan_id', 5);
                                    });
                                  });
                                });
                              })
                            ->where('tipe', 0)
                              ->doesntHave('penerimaan')
                                          ->get()->count();
          }else{
            $records = \App\Models\VirtualAccount\KonfirmasiPembayaran::where('status', '>', 0)
                              ->whereHas('va', function($z){
                                $z->whereHas('surat', function($y){
                                  $y->whereHas('kaji_ulang', function($x){
                                    $x->whereHas('pp', function($m){
                                      $m->where('jenis', 1);
                                    });
                                  });
                                });
                              })
                            ->where('tipe', 0)
                              ->doesntHave('penerimaan')
                                          ->get()->count();
          }
        }elseif(auth()->user()->hasRole(['yan-kalibrasi']) || auth()->user()->hasRole(['adminlab-kalibrasi'])){
          $records = \App\Models\VirtualAccount\KonfirmasiPembayaran::where('status', '>', 0)
                            ->whereHas('va', function($z){
                              $z->whereHas('surat', function($y){
                                $y->whereHas('kaji_ulang', function($x){
                                  $x->whereHas('pp', function($m){
                                    $m->where('jenis', 0);
                                  });
                                });
                              });
                            })
                          ->where('tipe', 0)
                            ->doesntHave('penerimaan')
                                        ->get()->count();
        }else{
          $records ='';
        }

        if($records > 0){
          return $records;
        }
      }elseif($menu == 'reschedule'){
        if($user->hasRole(['asman-dal-kalibrasi','asman-lola-kalibrasi'])){
              $satu = \App\Models\Reschedule\Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
                        $u->where('layanan_id', 1);
                      })
                      ->where('status', 0)->get()->count();
        }elseif($user->hasRole(['asman-dal-siskit','asman-lola-siskit'])){
              $satu = \App\Models\Reschedule\Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
                          $u->where('layanan_id', 2);
                        })
                        ->where('status', 0)->get()->count();
        }elseif($user->hasRole(['asman-dal-siskit','asman-lola-siskit'])){
              $satu = \App\Models\Reschedule\Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
                          $u->where('layanan_id', 3);
                        })
                        ->where('status', 0)->get()->count();
        }elseif($user->hasRole(['asman-dal-tegangan-rendah','asman-lola-tegangan-rendah'])){
              $satu = \App\Models\Reschedule\Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
                          $u->where('layanan_id', 4);
                        })
                        ->where('status', 0)->get()->count();
        }elseif($user->hasRole(['asman-dal-tegangan-tinggi','asman-lola-tegangan-tinggi'])){
              $satu = \App\Models\Reschedule\Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
                          $u->where('layanan_id', 5);
                        })
                        ->where('status', 0)->get()->count();
        }else{
          $satu = 0;
        }

        if($satu > 0){
          return $satu;
        }
      }elseif($menu == 'pengirimanLaporan'){
      if($user->hasRole(['yan-kalibrasi'])){
        $records = \App\Models\LaporanPengujian\LaporanPengujian::where('status', 0)
                  ->whereHas('pengujian.penerimaan', function($u){
                      $u->byLayanan(1);
                  })
                  ->get()->count();
      }elseif($user->hasRole(['yan-uji'])){
        $records = \App\Models\LaporanPengujian\LaporanPengujian::where('status', 0)
                  ->whereHas('pengujian.penerimaan', function($u){
                      $u->byLayananV2([2,3,4,5]);
                  })
                  ->get()->count();
      }else{
        $records ='';
      }

        if($records > 0){
          return $records;
        }
      }elseif($menu == 'closeOrder'){
        if($user->hasRole(['msb-kalibrasi'])){
              $satu = \App\Models\PenerimaanBarang\PenerimaanBarang::doesntHave('closeorder')->byLayanan(1)->get()->count();
          }elseif($user->hasRole(['msb-siskit'])){
            $satu = \App\Models\PenerimaanBarang\PenerimaanBarang::doesntHave('closeorder')->byLayanan(2)->get()->count();
          }elseif($user->hasRole(['msb-sistgi'])){
            $satu = \App\Models\PenerimaanBarang\PenerimaanBarang::doesntHave('closeorder')->byLayanan(3)->get()->count();
          }elseif($user->hasRole(['msb-tegangan-rendah'])){
            $satu = \App\Models\PenerimaanBarang\PenerimaanBarang::doesntHave('closeorder')->byLayanan(4)->get()->count();
          }elseif($user->hasRole(['msb-tegangan-tinggi'])){
            $satu = \App\Models\PenerimaanBarang\PenerimaanBarang::doesntHave('closeorder')->byLayanan(5)->get()->count();
          }else{
            $satu = 0;
          }

        if($satu > 0){
          return$satu;
        }
      }elseif($menu == 'pengaduan'){
        $records =\App\Models\Aduan::orderBy('created_at','asc')->get()->count();
        
        if($records > 0){
          return $records;
        }
      }elseif($menu == 'aktivasiPemintaJasa'){
        $active = \App\Models\Master\Pelanggan::where('status', 1)->count();
          if($active>0){
            return $active;
          }
      }elseif($menu == 'pengembalianBarangUji'){
        if(auth()->user()->hasRole(['yan-kalibrasi','adminlab-kalibrasi'])){
          $satu = \App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('pengujian', function($u){
                          $u->where('status_data_pengujian', 1);
                        })
                      ->byLayanan(1)
                      ->where('status_pengembalian', 0)
                      ->get()->count(); 
        }elseif(auth()->user()->hasRole(['yan-uji','adminlab-siskit'])){
          $satu = \App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('pengujian', function($u){
                            $u->where('status_data_pengujian', 1);
                          })
                        ->byLayanan(2)
                        ->where('status_pengembalian', 0)
                        ->get()->count();
        }elseif(auth()->user()->hasRole(['yan-uji','adminlab-sistgi'])){
          $satu = \App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('pengujian', function($u){
                            $u->where('status_data_pengujian', 1);
                          })
                        ->byLayanan(3)
                        ->where('status_pengembalian', 0)
                        ->get()->count();
        }elseif(auth()->user()->hasRole(['yan-uji','adminlab-tegangan-rendah'])){
          $satu = \App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('pengujian', function($u){
                            $u->where('status_data_pengujian', 1);
                          })
                        ->byLayanan(4)
                        ->where('status_pengembalian', 0)
                        ->get()->count();
        }elseif(auth()->user()->hasRole(['yan-uji','adminlab-tegangan-tinggi'])){
          $satu = \App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('pengujian', function($u){
                            $u->where('status_data_pengujian', 1);
                          })
                        ->byLayanan(5)
                        ->where('status_pengembalian', 0)
                        ->get()->count();
        }else{
          $satu = 0;
        }

          if($satu>0){
            return $satu;
          }
      }elseif($menu == 'laporanPengujian'){
        if($user->hasRole(['srm-prosmkal','yan-kalibrasi', 'msb-kalibrasi', 'adminlab-kalibrasi', 'asman-dal-kalibrasi', 'asman-lola-kalibrasi', 'pelaksana-kalibrasi'])){
            $satu = \App\Models\LaporanPengujian\LaporanPengujian::whereHas('pengujian', function($u){
                            $u->where('tipe', 1);
                          })->get()->count();
        }elseif($user->hasRole(['msb-siskit', 'adminlab-siskit', 'asman-dal-siskit', 'asman-lola-siskit', 'pelaksana-siskit'])){
          $satu = \App\Models\LaporanPengujian\LaporanPengujian::whereHas('pengujian', function($u){
                            $u->where('tipe', 2);
                          })->get()->count();
        }elseif($user->hasRole(['msb-sistgi', 'adminlab-sistgi', 'asman-dal-sistgi', 'asman-lola-sistgi', 'pelaksana-sistgi'])){
          $satu = \App\Models\LaporanPengujian\LaporanPengujian::whereHas('pengujian', function($u){
                            $u->where('tipe', 3);
                          })->get()->count();
        }elseif($user->hasRole(['msb-tegangan-rendah', 'adminlab-tegangan-rendah', 'asman-dal-tegangan-rendah', 'asman-lola-tegangan-rendah', 'pelaksana-tegangan-rendah'])){
          $satu = \App\Models\LaporanPengujian\LaporanPengujian::whereHas('pengujian', function($u){
                            $u->where('tipe', 4);
                          })->get()->count();
        }elseif($user->hasRole(['msb-tegangan-tinggi', 'adminlab-tegangan-tinggi', 'asman-dal-tegangan-tinggi', 'asman-lola-tegangan-tinggi', 'pelaksana-tegangan-tinggi'])){
          $satu = \App\Models\LaporanPengujian\LaporanPengujian::whereHas('pengujian', function($u){
                            $u->where('tipe', 5);
                          })->get()->count();
        }elseif($user->hasRole(['yan-uji', 'srm-uji'])){
          $satu = \App\Models\LaporanPengujian\LaporanPengujian::whereHas('pengujian', function($u){
                            $u->whereIn('tipe', [2,3,4,5]);
                          })->get()->count();
        }elseif($user->hasRole(['gm','admin'])){
          $satu = \App\Models\LaporanPengujian\LaporanPengujian::get()->count();
        }else{
          $satu = \App\Models\LaporanPengujian\LaporanPengujian::where('id', 0)->get()->count();;
        }

          if($satu>0){
            return $satu;
          }
        }elseif($menu == 'historikalData'){

        }else{

      }
  }

  public function bublepengujian($menu){
    $user = auth()->user();
    if($menu == 'pengujian'){
      if($user->hasRole(['pelaksana-kalibrasi'])){
        $satu =  \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(1)
                                  ->where('status', 1)
                                                ->where('status_pengujian', 0)
                                                ->get()->count();

          $dua = \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(1)
                                    ->whereHas('pengujian', function($u){
                                                  $u->where('status_data_pengujian', 0);
                                                })
                                                ->where('status', 1)
                                                ->where('status_pengujian', 1)
                                                ->get()->count();
          $records = $satu + $dua;
      }elseif($user->hasRole(['pelaksana-siskit'])){
        $satu =  \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(2)
                                  ->where('status', 1)
                                                ->where('status_pengujian', 0)
                                                ->get()->count();

          $dua = \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(2)
                                    ->whereHas('pengujian', function($u){
                                                  $u->where('status_data_pengujian', 0);
                                                })
                                                ->where('status', 1)
                                                ->where('status_pengujian', 1)
                                                ->get()->count();
          $records = $satu + $dua;
      }elseif($user->hasRole(['pelaksana-sistgi'])){
        $satu =  \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(3)
                                  ->where('status', 1)
                                                ->where('status_pengujian', 0)
                                                ->get()->count();

          $dua = \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(3)
                                    ->whereHas('pengujian', function($u){
                                                  $u->where('status_data_pengujian', 0);
                                                })
                                                ->where('status', 1)
                                                ->where('status_pengujian', 1)
                                                ->get()->count();
          $records = $satu + $dua;
      }elseif($user->hasRole(['pelaksana-tegangan-rendah'])){
        $satu =  \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(4)
                                  ->where('status', 1)
                                                ->where('status_pengujian', 0)
                                                ->get()->count();

          $dua = \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(4)
                                    ->whereHas('pengujian', function($u){
                                                  $u->where('status_data_pengujian', 0);
                                                })
                                                ->where('status', 1)
                                                ->where('status_pengujian', 1)
                                                ->get()->count();
          $records = $satu + $dua;
      }elseif($user->hasRole(['pelaksana-tegangan-tinggi'])){
        $satu =  \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(5)
                                  ->where('status', 1)
                                                ->where('status_pengujian', 0)
                                                ->get()->count();

          $dua = \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(5)
                                    ->whereHas('pengujian', function($u){
                                                  $u->where('status_data_pengujian', 0);
                                                })
                                                ->where('status', 1)
                                                ->where('status_pengujian', 1)
                                                ->get()->count();
          $records = $satu + $dua;
      }elseif($user->hasRole(['asman-dal-kalibrasi', 'asman-lola-kalibrasi'])){
        $satu =  \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(1)
                                  ->where('status', 1)
                                                ->where('status_pengujian', 0)
                                                ->get()->count();

          $dua = \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(1)
                                    ->whereHas('pengujian', function($u){
                                                  $u->where('status_data_pengujian', 0);
                                                })
                                                ->where('status', 1)
                                                ->where('status_pengujian', 1)
                                                ->get()->count();
          $records = $satu + $dua;
      }elseif($user->hasRole(['asman-dal-siskit', 'asman-lola-siskit'])){
        $satu =  \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(2)
                                  ->where('status', 1)
                                                ->where('status_pengujian', 0)
                                                ->get()->count();

          $dua = \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(2)
                                    ->whereHas('pengujian', function($u){
                                                  $u->where('status_data_pengujian', 0);
                                                })
                                                ->where('status', 1)
                                                ->where('status_pengujian', 1)
                                                ->get()->count();
          $records = $satu + $dua;
      }elseif($user->hasRole(['asman-dal-sistgi', 'asman-lola-sistgi'])){
        $satu =  \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(3)
                                  ->where('status', 1)
                                                ->where('status_pengujian', 0)
                                                ->get()->count();

          $dua = \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(3)
                                    ->whereHas('pengujian', function($u){
                                                  $u->where('status_data_pengujian', 0);
                                                })
                                                ->where('status', 1)
                                                ->where('status_pengujian', 1)
                                                ->get()->count();
          $records = $satu + $dua;
      }elseif($user->hasRole(['asman-dal-tegangan-rendah', 'asman-lola-tegangan-rendah'])){
        $satu =  \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(4)
                                  ->where('status', 1)
                                                ->where('status_pengujian', 0)
                                                ->get()->count();

          $dua = \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(4)
                                    ->whereHas('pengujian', function($u){
                                                  $u->where('status_data_pengujian', 0);
                                                })
                                                ->where('status', 1)
                                                ->where('status_pengujian', 1)
                                                ->get()->count();
          $records = $satu + $dua;
      }elseif($user->hasRole(['asman-dal-tegangan-tinggi', 'asman-lola-tegangan-tinggi'])){
        $satu =  \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(5)
                                  ->where('status', 1)
                                                ->where('status_pengujian', 0)
                                                ->get()->count();

          $dua = \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(5)
                                    ->whereHas('pengujian', function($u){
                                                  $u->where('status_data_pengujian', 0);
                                                })
                                                ->where('status', 1)
                                                ->where('status_pengujian', 1)
                                                ->get()->count();
          $records = $satu + $dua;
      }elseif($user->hasRole(['yan-kalibrasi','msb-kalibrasi'])){
        $satu =  \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(1)
                                  ->where('status', 1)
                                                ->where('status_pengujian', 0)
                                                ->get()->count();

          $dua = \App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(1)
                                    ->whereHas('pengujian', function($u){
                                                  $u->where('status_data_pengujian', 0);
                                                })
                                                ->where('status', 1)
                                                ->where('status_pengujian', 1)
                                                ->get()->count();
          $records = $satu + $dua;
      }elseif($user->hasRole(['yan-uji'])){
        $satu =  \App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
                                        $k->whereHas('va', function($v){
                                            $v->whereHas('surat', function($s){
                                                $s->whereHas('kaji_ulang', function($ku){
                                                    $ku->whereHas('pp', function($p){
                                                        $p->whereIn('layanan_id', [2,3,4,5]);
                                                    });
                                                });
                                            });
                                        })->where('tipe', 0);
                                    })
                                    ->where('status', 1)
                                                  ->where('status_pengujian', 0)
                                                  ->get()->count();

          $dua = \App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
                                      $k->whereHas('va', function($v){
                                          $v->whereHas('surat', function($s){
                                              $s->whereHas('kaji_ulang', function($ku){
                                                  $ku->whereHas('pp', function($p){
                                                      $p->whereIn('layanan_id', [2,3,4,5]);
                                                  });
                                              });
                                          });
                                      })->where('tipe', 0);
                                  })
                                    ->whereHas('pengujian', function($u){
                                                  $u->where('status_data_pengujian', 0);
                                                })
                                                ->where('status', 1)
                                                ->where('status_pengujian', 1)
                                                ->get()->count();
          $records = $satu + $dua;
      }elseif($user->hasRole(['msb-siskit'])){
        $satu =  \App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
                                        $k->whereHas('va', function($v){
                                            $v->whereHas('surat', function($s){
                                                $s->whereHas('kaji_ulang', function($ku){
                                                    $ku->whereHas('pp', function($p){
                                                        $p->where('layanan_id', 2);
                                                    });
                                                });
                                            });
                                        })->where('tipe', 0);
                                    })
                                    ->where('status', 1)
                                                  ->where('status_pengujian', 0)
                                                  ->get()->count();

          $dua = \App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
                                      $k->whereHas('va', function($v){
                                          $v->whereHas('surat', function($s){
                                              $s->whereHas('kaji_ulang', function($ku){
                                                  $ku->whereHas('pp', function($p){
                                                      $p->where('layanan_id', 2);
                                                  });
                                              });
                                          });
                                      })->where('tipe', 0);
                                  })
                                    ->whereHas('pengujian', function($u){
                                                  $u->where('status_data_pengujian', 0);
                                                })
                                                ->where('status', 1)
                                                ->where('status_pengujian', 1)
                                                ->get()->count();
          $records = $satu + $dua;
      }elseif($user->hasRole(['msb-sistgi'])){
        $satu =  \App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
                                        $k->whereHas('va', function($v){
                                            $v->whereHas('surat', function($s){
                                                $s->whereHas('kaji_ulang', function($ku){
                                                    $ku->whereHas('pp', function($p){
                                                        $p->where('layanan_id', 3);
                                                    });
                                                });
                                            });
                                        })->where('tipe', 0);
                                    })
                                    ->where('status', 1)
                                                  ->where('status_pengujian', 0)
                                                  ->get()->count();

          $dua = \App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
                                      $k->whereHas('va', function($v){
                                          $v->whereHas('surat', function($s){
                                              $s->whereHas('kaji_ulang', function($ku){
                                                  $ku->whereHas('pp', function($p){
                                                      $p->where('layanan_id', 3);
                                                  });
                                              });
                                          });
                                      })->where('tipe', 0);
                                  })
                                    ->whereHas('pengujian', function($u){
                                                  $u->where('status_data_pengujian', 0);
                                                })
                                                ->where('status', 1)
                                                ->where('status_pengujian', 1)
                                                ->get()->count();
          $records = $satu + $dua;
      }elseif($user->hasRole(['msb-tegangan-rendah'])){
        $satu =  \App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
                                        $k->whereHas('va', function($v){
                                            $v->whereHas('surat', function($s){
                                                $s->whereHas('kaji_ulang', function($ku){
                                                    $ku->whereHas('pp', function($p){
                                                        $p->where('layanan_id', 4);
                                                    });
                                                });
                                            });
                                        })->where('tipe', 0);
                                    })
                                    ->where('status', 1)
                                                  ->where('status_pengujian', 0)
                                                  ->get()->count();

          $dua = \App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
                                      $k->whereHas('va', function($v){
                                          $v->whereHas('surat', function($s){
                                              $s->whereHas('kaji_ulang', function($ku){
                                                  $ku->whereHas('pp', function($p){
                                                      $p->where('layanan_id', 4);
                                                  });
                                              });
                                          })->where('tipe', 0);
                                      });
                                  })
                                    ->whereHas('pengujian', function($u){
                                                  $u->where('status_data_pengujian', 0);
                                                })
                                                ->where('status', 1)
                                                ->where('status_pengujian', 1)
                                                ->get()->count();
          $records = $satu + $dua;
      }elseif($user->hasRole(['msb-tegangan-tinggi'])){
        $satu =  \App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
                                        $k->whereHas('va', function($v){
                                            $v->whereHas('surat', function($s){
                                                $s->whereHas('kaji_ulang', function($ku){
                                                    $ku->whereHas('pp', function($p){
                                                        $p->where('layanan_id', 5);
                                                    });
                                                });
                                            });
                                        })->where('tipe', 0);
                                    })
                                    ->where('status', 1)
                                                  ->where('status_pengujian', 0)
                                                  ->get()->count();

          $dua = \App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
                                      $k->whereHas('va', function($v){
                                          $v->whereHas('surat', function($s){
                                              $s->whereHas('kaji_ulang', function($ku){
                                                  $ku->whereHas('pp', function($p){
                                                      $p->where('layanan_id', 5);
                                                  });
                                              });
                                          });
                                      })->where('tipe', 0);
                                  })
                                    ->whereHas('pengujian', function($u){
                                                  $u->where('status_data_pengujian', 0);
                                                })
                                                ->where('status', 1)
                                                ->where('status_pengujian', 1)
                                                ->get()->count();
          $records = $satu + $dua;
      }else{
        $records='';
      }

      if($records>0){
        return $records;
      }
    }else{
    }
  }

  public function graphKaji(){
    $diskusi = 0;
    $progres = 0;
    $layanan_id = $this->getLayananUser();

    $user = auth()->user();
    if($user->hasRole(['msb-kalibrasi', 'msb-siskit', 'msb-sistgi', 'msb-tegangan-rendah', 'msb-tegangan-tinggi'])){
        $diskusi = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
                                            $q->where('penerima_id', $user->id)
                                              ->where('jenis', 1);
                                        })
                                        ->where('status', 2)
                                        ->whereHas('kaji_ulang', function($u){
                                            $u->where('keputusan', 3);
                                        })
                                        ->where('tipe_surat', 0)
                                        ->count();

        $progres = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
                                            $q->where('penerima_id', $user->id)
                                              ->where('jenis', 1);
                                        })
								        ->where('tipe_surat', 0)
                                        ->whereIn('status', [2,12])
                                        ->doesntHave('kaji_ulang')
                                        ->count();
    }elseif($user->hasRole(['asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-tinggi','asman-dal-tegangan-rendah', 'asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-tinggi','asman-lola-tegangan-rendah'])){
        $diskusi = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
                                            $q->where('penerima_id', $user->id)
                                              ->where('jenis', 2);
                                        })
                                        ->where('status', 2)
                                        ->whereHas('kaji_ulang', function($u){
                                            $u->where('keputusan', 3);
                                        })
                                        ->where('tipe_surat', 0)
                                        ->count();

        $progres = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
                                            $q->where('penerima_id', $user->id)
                                              ->where('jenis', 2);
                                        })
                                        ->where('status', 2)
                                        ->doesntHave('kaji_ulang')
                                        ->where('tipe_surat', 0)
                                        ->count();
    }else{
        $user = User::get();
        foreach ($user as $u) {
            if($u->hasRole(['msb-kalibrasi', 'msb-siskit', 'msb-sistgi', 'msb-tegangan-rendah', 'msb-tegangan-tinggi'])){
                $diskusi += PendaftaranPengujian::whereHas('act_dispo', function($q) use ($u){
                                            $q->where('penerima_id', $u->id)
                                              ->where('jenis', 1);
                                        })
                                        ->where('status', 2)
                                        ->whereHas('kaji_ulang', function($u){
                                            $u->where('keputusan', 3);
                                        })
                                        ->where('tipe_surat', 0)
                                        ->count();

                $progres += PendaftaranPengujian::whereHas('act_dispo', function($q) use ($u){
                                                    $q->where('penerima_id', $u->id)
                                                      ->where('jenis', 1);
                                                })
                                                ->whereIn('status', [2,12])
                                                ->doesntHave('kaji_ulang')
                                                ->where('tipe_surat', 0)
                                                ->count();
            }
        }
    }

    $selesai = PendaftaranPengujian::whereIn('layanan_id',$layanan_id)
                                    ->whereHas('kaji_ulang', function($query){
                                        $query->where('keputusan','!=',3);
                                    })
                                    ->whereHas('act_dispo', function($query){
                                        $query->where('jenis',2);
                                    })
                                    ->where('tipe_surat', 0)
                                    ->count();
    $graph = [
        'selesai' => $selesai,
        'belum' => $diskusi+$progres
    ];

    return $graph;
  }

  public function graphPengujian(){
      $layanan_id = $this->getLayananUser();

      $belum = PendaftaranPengujian::whereIn('layanan_id',$layanan_id)
                                      ->whereHas('kaji_ulang', function($query){
                                          $query->whereHas('surat', function($surat){
                                              $surat->whereHas('va', function($va){
                                                  $va->whereHas('konfirmasi', function($konfirmasi){
                                                      $konfirmasi->whereHas('penerimaan', function($penerimaan){
                                                          $penerimaan->whereHas('pengujian', function($pengujian){
                                                                          $pengujian->where('status_data_pengujian',0);
                                                                      })
                                                                      ->orWhere(function($condition){
                                                                          $condition->where('status_pengujian',0);
                                                                      });
                                                      });
                                                  });
                                              });
                                          });
                                      })
                                      ->count();

      $selesai = PendaftaranPengujian::whereIn('layanan_id',$layanan_id)
                                      ->whereHas('kaji_ulang', function($query){
                                          $query->whereHas('surat', function($surat){
                                              $surat->whereHas('va', function($va){
                                                  $va->whereHas('konfirmasi', function($konfirmasi){
                                                      $konfirmasi->whereHas('penerimaan', function($penerimaan){
                                                          // $penerimaan->where('status_pengujian',1);
                                                          $penerimaan->whereHas('pengujian', function($pengujian){
                                                              $pengujian->where('status_data_pengujian',1);
                                                          });
                                                      });
                                                  });
                                              });
                                          });
                                      })
                                      ->count();
      
      $graph = [
          'selesai' => $selesai,
          'belum' => $belum
      ];

      return $graph;
  }

  public function graphLaporan(){
    $layanan_id = $this->getLayananUser();

    $belum = LaporanPengujian::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($layanan_id){
        $pp->whereIn('layanan_id',$layanan_id)
        ->where('tipe_surat', 0);
    })
                                    ->where('status',0)
                                    ->count();

    $selesai = LaporanPengujian::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($layanan_id){
        $pp->whereIn('layanan_id',$layanan_id)
        ->where('tipe_surat', 0);
    })
                                    ->where('status',1)
                                    ->count();
      $graph = [
          'selesai' => $selesai,
          'belum' => $belum
      ];

      return $graph;
  }

  public function getLayananUser(){
      $layanan_id = [1,2,3,4,5];
        if(auth()->user()->hasRole(['asman-dal-kalibrasi']) || auth()->user()->hasRole(['asman-lola-kalibrasi']) || auth()->user()->hasRole(['msb-kalibrasi']) || auth()->user()->hasRole(['adminlab-kalibrasi']) || auth()->user()->hasRole(['pelaksana-kalibrasi'])){
            $layanan_id = [1];
        }
        if(auth()->user()->hasRole(['asman-dal-siskit']) || auth()->user()->hasRole(['asman-lola-siskit']) || auth()->user()->hasRole(['msb-siskit']) || auth()->user()->hasRole(['adminlab-siskit']) || auth()->user()->hasRole(['pelaksana-siskit'])){
            $layanan_id = [2];
        }
        if(auth()->user()->hasRole(['asman-dal-sistgi']) || auth()->user()->hasRole(['asman-lola-sistgi']) || auth()->user()->hasRole(['msb-sistgi']) || auth()->user()->hasRole(['adminlab-sistgi']) || auth()->user()->hasRole(['pelaksana-sistgi'])){
            $layanan_id = [3];
        }
        if(auth()->user()->hasRole(['asman-dal-tegangan-rendah']) || auth()->user()->hasRole(['asman-lola-tegangan-rendah']) || auth()->user()->hasRole(['msb-tegangan-rendah']) || auth()->user()->hasRole(['adminlab-tegangan-rendah']) || auth()->user()->hasRole(['pelaksana-tegangan-rendah'])){
            $layanan_id = [4];
        }
        if(auth()->user()->hasRole(['asman-dal-tegangan-tinggi']) || auth()->user()->hasRole(['asman-lola-tegangan-tinggi']) || auth()->user()->hasRole(['msb-tegangan-tinggi']) || auth()->user()->hasRole(['adminlab-tegangan-tinggi']) || auth()->user()->hasRole(['pelaksana-tegangan-tinggi'])){
            $layanan_id = [5];
        }
        if(auth()->user()->hasRole(['yan-kalibrasi'])){
            $layanan_id = [1];
        }
        if(auth()->user()->hasRole(['yan-uji'])){
            $layanan_id = [2,3,4,5];
        }

        return $layanan_id;
  }

  public function updateUser(Request $request){
    try {
      $u = auth()->user();
      $user = User::find($u->id);
      $user->device_id = $request->device_id;
      $user->save();
    } catch (\Exception $e) {
      
    }
    return response([
      'status' => true
    ]);
  }
}
