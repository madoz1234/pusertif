<?php

namespace App\Http\Controllers\API\Penerimaan;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Unlu\Laravel\Api\QueryBuilder;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\Penerimaan\DisposisiRequest;
use App\Http\Requests\Penerimaan\PenerimaanRequest;
use App\Http\Requests\FrontEnd\CekItemUjiRequest;

/* Models */
use App\Models\Authentication\Role;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\FrontEnd\PendaftaranPengujianDetail;
use App\Models\Master\Pelanggan;
use App\Models\KajiUlang\KajiUlang;
// use App\Models\PenerimaanSurat\PenerimaanSurat;
use App\Models\Act\ActDisposisi;
use App\Models\Users;
use App\Models\Picture;
use App\Models\Files;
// use Spatie\Fractal\Fractal;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;

class PenerimaanSuratController extends Controller
{
      public function index(Request $request)
      {
            $data = new QueryBuilder(new PendaftaranPengujian, $request);
            if($page=$request->page){
                  return $data->build()->paginate();
            }

            return response()->json([
                  'status' => true,
                  'data' => $data->build()->get()
            ]);
      }

      public function online(Request $request)
      {
        // dd($request->sort);
            $user = auth()->user();
            $data =[];

            if($user->hasRole(['yan-kalibrasi'])){
                    $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','files')->whereIn('status', [1,11,12])->where('tipe', 1)->where('tipe_surat', 0)->where('jenis', 0)->select('*');

                    if ($no_order = $request->no_order) {
                          $data->where('no_order', 'like','%'.$no_order.'%');
                    }
                    if($no_surat = $request->no_surat){
                          $data->where('no_surat','like','%'.$no_surat.'%');
                    }
                    if($tanggal_order = $request->tanggal_order){
                          $data->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                    }
                    if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                          $data->where('layanan_id',$jenis_pelayanan_id);
                    }
                    if ($perusahaan_id = $request->perusahaan_id) {
                          $data->whereHas('user', function($user) use ($perusahaan_id){
                            $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                  $pelanggan->where('perusahaan_id',$perusahaan_id);
                            });
                          });
                    }
                    if($sort = $request->sort){
                        if($sort == 'order'){
                          $data->orderBy('tgl_order','asc');
                        }
                        if($sort == 'surat'){
                          $data->orderBy('tgl_surat','asc');
                        }
                    }else{
                          $data->orderBy('created_at','desc');
                    }
            }elseif($user->hasRole(['yan-uji'])){
                  $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','files')->whereIn('status', [1,11,12])->where('tipe', 1)->where('tipe_surat', 0)->where('jenis', 1)->select('*');
                    if ($no_order = $request->no_order) {
                          $data->where('no_order', 'like','%'.$no_order.'%');
                    }
                    if($no_surat = $request->no_surat){
                          $data->where('no_surat','like','%'.$no_surat.'%');
                    }
                    if($tanggal_order = $request->tanggal_order){
                          $data->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                    }
                    if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                          $data->where('layanan_id',$jenis_pelayanan_id);
                    }
                    if ($perusahaan_id = $request->perusahaan_id) {
                          $data->whereHas('user', function($user) use ($perusahaan_id){
                            $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                  $pelanggan->where('perusahaan_id',$perusahaan_id);
                            });
                          });
                    }
                    if($sort = $request->sort){
                        if($sort == 'order'){
                          $data->orderBy('tgl_order','asc');
                        }
                        if($sort == 'surat'){
                          $data->orderBy('tgl_surat','asc');
                        }
                    }else{
                          $data->orderBy('created_at','desc');
                    }
            }else{
              return response()->json([
                  'status' => false,
                  'message' => 'page not found'
              ],404);
            }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

            return response()->json($page);
      }

      public function ams(Request $request)
      {
        $user = auth()->user();
        $data =[];

        if($user->hasRole(['yan-kalibrasi'])){
          $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','files')->whereIn('status', [1,11,12])->where('tipe', 2)->where('jenis',0)->where('tipe_surat', 0)->where('status_ams',1)->select('*');
          if ($no_order = $request->no_order) {
                $data->where('no_order', 'like','%'.$no_order.'%');
          }
          if($no_surat = $request->no_surat){
                $data->where('no_surat','like','%'.$no_surat.'%');
          }
          if($tanggal_order = $request->tanggal_order){
                $data->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
          }
          if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                $data->where('layanan_id',$jenis_pelayanan_id);
          }
          if ($perusahaan_id = $request->perusahaan_id) {
                $data->whereHas('user', function($user) use ($perusahaan_id){
                  $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                        $pelanggan->where('perusahaan_id',$perusahaan_id);
                  });
                });
          }
          if($sort = $request->sort){
              if($sort == 'order'){
                $data->orderBy('tgl_order','asc');
              }
              if($sort == 'surat'){
                $data->orderBy('tgl_surat','asc');
              }
          }else{
                $data->orderBy('created_at','desc');
          }
        }elseif($user->hasRole(['yan-uji'])){
          $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','files')->whereIn('status', [1,11,12])->where('tipe', 2)->where('jenis',1)->where('tipe_surat', 0)->where('status_ams',1)->select('*');
          if ($no_order = $request->no_order) {
                $data->where('no_order', 'like','%'.$no_order.'%');
          }
          if($no_surat = $request->no_surat){
                $data->where('no_surat','like','%'.$no_surat.'%');
          }
          if($tanggal_order = $request->tanggal_order){
                $data->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
          }
          if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                $data->where('layanan_id',$jenis_pelayanan_id);
          }
          if ($perusahaan_id = $request->perusahaan_id) {
                $data->whereHas('user', function($user) use ($perusahaan_id){
                  $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                        $pelanggan->where('perusahaan_id',$perusahaan_id);
                  });
                });
          }
          if($sort = $request->sort){
              if($sort == 'order'){
                $data->orderBy('tgl_order','asc');
              }
              if($sort == 'surat'){
                $data->orderBy('tgl_surat','asc');
              }
          }else{
                $data->orderBy('created_at','desc');
          }
        }elseif($user->hasRole(['admin'])){
          $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','files')->whereIn('status', [1,11,12])->where('tipe', 2)->whereIn('jenis',[1,0])->where('status_ams',0)->where('tipe_surat', 0)->select('*');

          if ($no_order = $request->no_order) {
                $data->where('no_order', 'like','%'.$no_order.'%');
          }
          if($no_surat = $request->no_surat){
                $data->where('no_surat','like','%'.$no_surat.'%');
          }
          if($tanggal_order = $request->tanggal_order){
                $data->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
          }
          if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                $data->where('layanan_id',$jenis_pelayanan_id);
          }
          if ($perusahaan_id = $request->perusahaan_id) {
                $data->whereHas('user', function($user) use ($perusahaan_id){
                  $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                        $pelanggan->where('perusahaan_id',$perusahaan_id);
                  });
                });
          }
          if($sort = $request->sort){
              if($sort == 'order'){
                $data->orderBy('tgl_order','asc');
              }
              if($sort == 'surat'){
                $data->orderBy('tgl_surat','asc');
              }
          }else{
                $data->orderBy('created_at','desc');
          }
        }else{
          return response()->json([
              'status' => false,
              'message' => 'page not found'
          ],404);
        }
        $page = $data->paginate(10);
        // dd($page[0]);
        if ($page[0] == null) {
          return response()->json([
            'status' => false,
            'message' => 'page not found'
          ],404);
        }

        return response()->json($page);
      }
      
      public function spm(Request $request)
      {
            $user = auth()->user();
            $data =[];

            if($user->hasRole(['yan-kalibrasi'])){
                    $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','files')->whereIn('status', [1,11,12])->where('jenis', 0)->where('tipe', 3)->where('tipe_surat', 0)->select('*');

                    if ($no_order = $request->no_order) {
                          $data->where('no_order', 'like','%'.$no_order.'%');
                    }
                    if($no_surat = $request->no_surat){
                          $data->where('no_surat','like','%'.$no_surat.'%');
                    }
                    if($tanggal_order = $request->tanggal_order){
                          $data->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                    }
                    if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                          $data->where('layanan_id',$jenis_pelayanan_id);
                    }
                    if ($perusahaan_id = $request->perusahaan_id) {
                          $data->whereHas('user', function($user) use ($perusahaan_id){
                            $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                  $pelanggan->where('perusahaan_id',$perusahaan_id);
                            });
                          });
                    }
                    if($sort = $request->sort){
                        if($sort == 'order'){
                          $data->orderBy('tgl_order','asc');
                        }
                        if($sort == 'surat'){
                          $data->orderBy('tgl_surat','asc');
                        }
                    }else{
                          $data->orderBy('created_at','desc');
                    }
            }elseif($user->hasRole(['yan-uji'])){
                  $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','files')->whereIn('status', [1,11,12])->where('jenis', 1)->where('tipe', 3)->where('tipe_surat', 0)->select('*');
                    if ($no_order = $request->no_order) {
                          $data->where('no_order', 'like','%'.$no_order.'%');
                    }
                    if($no_surat = $request->no_surat){
                          $data->where('no_surat','like','%'.$no_surat.'%');
                    }
                    if($tanggal_order = $request->tanggal_order){
                          $data->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                    }
                    if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                          $data->where('layanan_id',$jenis_pelayanan_id);
                    }
                    if ($perusahaan_id = $request->perusahaan_id) {
                          $data->whereHas('user', function($user) use ($perusahaan_id){
                            $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                  $pelanggan->where('perusahaan_id',$perusahaan_id);
                            });
                          });
                    }
                    if($sort = $request->sort){
                        if($sort == 'order'){
                          $data->orderBy('tgl_order','asc');
                        }
                        if($sort == 'surat'){
                          $data->orderBy('tgl_surat','asc');
                        }
                    }else{
                          $data->orderBy('created_at','desc');
                    }
            }else{
              return response()->json([
                  'status' => false,
                  'message' => 'page not found'
              ],404);
            }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

            return response()->json($page);
      }

      public function histori(Request $request)
      {
        $user = auth()->user();
        $data =[];

        if($user->hasRole(['yan-kalibrasi'])){
          $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','files')->whereIn('status', [2,3,4,5,6,7])->where('jenis',0)->where('tipe_surat', 0)->select('*');

          if ($no_order = $request->no_order) {
                $data->where('no_order', 'like','%'.$no_order.'%');
          }
          if($no_surat = $request->no_surat){
                $data->where('no_surat','like','%'.$no_surat.'%');
          }
          if($tanggal_order = $request->tanggal_order){
                $data->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
          }
          if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                $data->where('layanan_id',$jenis_pelayanan_id);
          }
          if ($perusahaan_id = $request->perusahaan_id) {
                $data->whereHas('user', function($user) use ($perusahaan_id){
                  $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                        $pelanggan->where('perusahaan_id',$perusahaan_id);
                  });
                });
          }
          if($sort = $request->sort){
              if($sort == 'order'){
                $data->orderBy('tgl_order','asc');
              }
              if($sort == 'surat'){
                $data->orderBy('tgl_surat','asc');
              }
          }else{
                $data->orderBy('created_at','desc');
          }

        }elseif($user->hasRole(['yan-uji'])){
          $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','files')->whereIn('status', [2,3,4,5,6,7])->where('jenis',1)->where('tipe_surat', 0)->select('*');

          if ($no_order = $request->no_order) {
                $data->where('no_order', 'like','%'.$no_order.'%');
          }
          if($no_surat = $request->no_surat){
                $data->where('no_surat','like','%'.$no_surat.'%');
          }
          if($tanggal_order = $request->tanggal_order){
                $data->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
          }
          if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                $data->where('layanan_id',$jenis_pelayanan_id);
          }
          if ($perusahaan_id = $request->perusahaan_id) {
                $data->whereHas('user', function($user) use ($perusahaan_id){
                  $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                        $pelanggan->where('perusahaan_id',$perusahaan_id);
                  });
                });
          }
          if($sort = $request->sort){
              if($sort == 'order'){
                $data->orderBy('tgl_order','asc');
              }
              if($sort == 'surat'){
                $data->orderBy('tgl_surat','asc');
              }
          }else{
                $data->orderBy('created_at','desc');
          }
        }else{
          return response()->json([
              'status' => false,
              'message' => 'page not found'
          ],404);
        }
        $page = $data->paginate(10);
        // dd($page[0]);
        if ($page[0] == null) {
          return response()->json([
            'status' => false,
            'message' => 'page not found'
          ],404);
        }

        return response()->json($page);
      }

      public function download($id)
      {
        $daftar = PendaftaranPengujian::find($id);
        if(file_exists(public_path('storage/'.$daftar->bukti)))
        {
          return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
        }
        return response([
          'status' => 'error'
        ],500);
      }
}
