<?php

namespace App\Http\Controllers\API\KajiUlang;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Unlu\Laravel\Api\QueryBuilder;

/* Models */
use App\Models\Authentication\Role;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\KajiUlang\KajiUlang;
use App\Models\KajiUlang\Detail;
use App\Models\Master\Pelanggan;
use App\Models\Users;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\KajiUlang\KajiUlangRequest;
use App\Http\Requests\KajiUlang\DisposisiRequest;
use App\Models\Act\ActDisposisi;
use App\Models\Picture;
use App\Models\Files;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Zipper;

class KajiUlangController extends Controller
{
      public function index(Request $request)
      {
            $data = new QueryBuilder(new KajiUlang, $request);
            if($page=$request->page){
                  return $data->build()->paginate();
            }

            return response()->json([
                  'status' => true,
                  'data' => $data->build()->get()
            ]);
      }
      public function grid(Request $request)
      {
            $user = auth()->user();
            if($user->hasRole(['msb-kalibrasi', 'msb-siskit', 'msb-sistgi', 'msb-tegangan-rendah', 'msb-tegangan-tinggi'])){
                $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','kaji_ulang.surat.va.konfirmasi','kaji_ulang.detail.mata_uji','kaji_ulang.detail.lokasi','files')->whereHas('act_dispo', function($q) use ($user){
                                                    $q->where('penerima_id', $user->id)
                                                    ->where('jenis', 1);
                                                    })
                                                  ->whereIn('status', [2,12])
                                                  ->doesntHave('kaji_ulang')
                                                  ->where('tipe_surat', 0)
                                                  ->select('*');

                if ($no_surat = $request->no_surat) {
                  $data->where('no_surat','like', '%'.$no_surat.'%');
                }
                if ($no_order = $request->no_order) {
                  $data->where('no_order','like', '%'.$no_order.'%');
                }
                if($tanggal_order = $request->tanggal_order){
                    $data->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                }
                if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $data->where('layanan_id',$jenis_pelayanan_id);
                }
                if ($perusahaan_id = $request->perusahaan_id) {
                  $data->whereHas('user', function($user) use ($perusahaan_id){
                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                              $pelanggan->where('perusahaan_id',$perusahaan_id);
                        });
                  });
                }
                if($sort = $request->sort){
                    if($sort == 'order'){
                      $data->orderBy('tgl_order','asc');
                    }
                    if($sort == 'surat'){
                      $data->orderBy('tgl_surat','asc');
                    }
                }else{
                      $data->orderBy('created_at','desc');
                }
            }elseif($user->hasRole(['asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-tinggi','asman-dal-tegangan-rendah', 'asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-tinggi','asman-lola-tegangan-rendah'])){
              $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','kaji_ulang.surat.va.konfirmasi','kaji_ulang.detail.mata_uji','kaji_ulang.detail.lokasi','files')->whereHas('act_dispo', function($q) use ($user){
                                      $q->where('penerima_id', $user->id)
                                        ->where('jenis', 2);
                                                })
                                      ->where('status', 2)
                                      ->doesntHave('kaji_ulang')
                                      ->where('tipe_surat', 0)
                                      ->select('*');

                if ($no_surat = $request->no_surat) {
                  $data->where('no_surat','like', '%'.$no_surat.'%');
                }
                if ($no_order = $request->no_order) {
                  $data->where('no_order','like', '%'.$no_order.'%');
                }
                if($tanggal_order = $request->tanggal_order){
                    $data->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                }
                if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $data->where('layanan_id',$jenis_pelayanan_id);
                }
                if ($perusahaan_id = $request->perusahaan_id) {
                  $data->whereHas('user', function($user) use ($perusahaan_id){
                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                              $pelanggan->where('perusahaan_id',$perusahaan_id);
                        });
                  });
                }
                if($sort = $request->sort){
                    if($sort == 'order'){
                      $data->orderBy('tgl_order','asc');
                    }
                    if($sort == 'surat'){
                      $data->orderBy('tgl_surat','asc');
                    }
                }else{
                      $data->orderBy('created_at','desc');
                }
            }else{
              // $data = [];
              return response()->json([
                  'status' => false,
                  'message' => 'page not found'
              ],404);
            }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

            return response()->json($page);
      }
      public function diskusi(Request $request)
      {
            $user = auth()->user();
            if($user->hasRole(['msb-kalibrasi', 'msb-siskit', 'msb-sistgi', 'msb-tegangan-rendah', 'msb-tegangan-tinggi'])){
                $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','kaji_ulang.surat.va.konfirmasi','kaji_ulang.detail.mata_uji','kaji_ulang.detail.lokasi','files')->whereHas('act_dispo', function($q) use ($user){
                                      $q->where('penerima_id', $user->id)
                                        ->where('jenis', 1);
                                                  })
                                        ->where('status', 2)
                                        ->whereHas('kaji_ulang', function($u){
                                          $u->where('keputusan', 3);
                                        })
                                        ->where('tipe_surat', 0)
                                        ->select('*');
                
                if ($no_surat = $request->no_surat) {
                  $data->where('no_surat','like', '%'.$no_surat.'%');
                }
                if ($no_order = $request->no_order) {
                  $data->where('no_order','like', '%'.$no_order.'%');
                }
                if($tanggal_order = $request->tanggal_order){
                    $data->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                }
                if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $data->where('layanan_id',$jenis_pelayanan_id);
                }
                if ($perusahaan_id = $request->perusahaan_id) {
                  $data->whereHas('user', function($user) use ($perusahaan_id){
                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                              $pelanggan->where('perusahaan_id',$perusahaan_id);
                        });
                  });
                }
                if($sort = $request->sort){
                    if($sort == 'order'){
                      $data->orderBy('tgl_order','asc');
                    }
                    if($sort == 'surat'){
                      $data->orderBy('tgl_surat','asc');
                    }
                }else{
                      $data->orderBy('created_at','desc');
                }
            }elseif($user->hasRole(['asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-tinggi','asman-dal-tegangan-rendah', 'asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-tinggi','asman-lola-tegangan-rendah'])){
              $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','kaji_ulang.surat.va.konfirmasi','kaji_ulang.detail.mata_uji','kaji_ulang.detail.lokasi','files')->whereHas('act_dispo', function($q) use ($user){
                                      $q->where('penerima_id', $user->id)
                                        ->where('jenis', 2);
                                                  })
                                        ->where('status', 2)
                                        ->whereHas('kaji_ulang', function($u){
                                          $u->where('keputusan', 3);
                                        })
                                        ->where('tipe_surat', 0)
                                        ->select('*');
                
                if ($no_surat = $request->no_surat) {
                  $data->where('no_surat','like', '%'.$no_surat.'%');
                }
                if ($no_order = $request->no_order) {
                  $data->where('no_order','like', '%'.$no_order.'%');
                }
                if($tanggal_order = $request->tanggal_order){
                    $data->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                }
                if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $data->where('layanan_id',$jenis_pelayanan_id);
                }
                if ($perusahaan_id = $request->perusahaan_id) {
                  $data->whereHas('user', function($user) use ($perusahaan_id){
                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                              $pelanggan->where('perusahaan_id',$perusahaan_id);
                        });
                  });
                }
                if($sort = $request->sort){
                    if($sort == 'order'){
                      $data->orderBy('tgl_order','asc');
                    }
                    if($sort == 'surat'){
                      $data->orderBy('tgl_surat','asc');
                    }
                }else{
                      $data->orderBy('created_at','desc');
                }
            }else{
              // $data = [];
              return response()->json([
                  'status' => false,
                  'message' => 'page not found'
              ],404);
            }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

            return response()->json($page);
      }

      public function histori(Request $request)
      {
            $user = auth()->user();
            if($user->hasRole(['msb-kalibrasi', 'msb-siskit', 'msb-sistgi', 'msb-tegangan-rendah', 'msb-tegangan-tinggi'])){
                $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','kaji_ulang.surat.va.konfirmasi','kaji_ulang.detail.mata_uji','kaji_ulang.detail.lokasi','files')->whereHas('act_dispo', function($q) use ($user){
                                      $q->where('penerima_id', $user->id)
                                        ->where('jenis', 1);
                                                  })
                                        ->whereIn('status', [2,3,4,5,6,7,8,9,10])
                                        ->whereHas('kaji_ulang', function($e){
                                          $e->whereIn('keputusan', [1,2]);
                                        })
                                        ->where('tipe_surat', 0)
                                        ->select('*');
                
                if ($no_surat = $request->no_surat) {
                  $data->where('no_surat','like', '%'.$no_surat.'%');
                }
                if ($no_order = $request->no_order) {
                  $data->where('no_order','like', '%'.$no_order.'%');
                }
                if($tanggal_order = $request->tanggal_order){
                    $data->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                }
                if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $data->where('layanan_id',$jenis_pelayanan_id);
                }
                if ($perusahaan_id = $request->perusahaan_id) {
                  $data->whereHas('user', function($user) use ($perusahaan_id){
                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                              $pelanggan->where('perusahaan_id',$perusahaan_id);
                        });
                  });
                }
                if($sort = $request->sort){
                    if($sort == 'order'){
                      $data->orderBy('tgl_order','asc');
                    }
                    if($sort == 'surat'){
                      $data->orderBy('tgl_surat','asc');
                    }
                }else{
                      $data->orderBy('created_at','desc');
                }
            }elseif($user->hasRole(['asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-tinggi','asman-dal-tegangan-rendah', 'asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-tinggi','asman-lola-tegangan-rendah'])){
              $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','kaji_ulang.surat.va.konfirmasi','kaji_ulang.detail.mata_uji','kaji_ulang.detail.lokasi','files')->whereHas('act_dispo', function($q) use ($user){
                                      $q->where('penerima_id', $user->id)
                                        ->where('jenis', 2);
                                                  })
                                        ->whereIn('status', [2,3,4,5,6,7,8,9,10])
                                        ->whereHas('kaji_ulang', function($e){
                                          $e->whereIn('keputusan', [1,2]);
                                        })
                                        ->where('tipe_surat', 0)
                                        ->select('*');
                
                if ($no_surat = $request->no_surat) {
                  $data->where('no_surat','like', '%'.$no_surat.'%');
                }
                if ($no_order = $request->no_order) {
                  $data->where('no_order','like', '%'.$no_order.'%');
                }
                if($tanggal_order = $request->tanggal_order){
                    $data->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                }
                if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $data->where('layanan_id',$jenis_pelayanan_id);
                }
                if ($perusahaan_id = $request->perusahaan_id) {
                  $data->whereHas('user', function($user) use ($perusahaan_id){
                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                              $pelanggan->where('perusahaan_id',$perusahaan_id);
                        });
                  });
                }
                if($sort = $request->sort){
                    if($sort == 'order'){
                      $data->orderBy('tgl_order','asc');
                    }
                    if($sort == 'surat'){
                      $data->orderBy('tgl_surat','asc');
                    }
                }else{
                      $data->orderBy('created_at','desc');
                }
            }else{
              // $data = [];
              return response()->json([
                  'status' => false,
                  'message' => 'page not found'
              ],404);
            }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

            return response()->json($page);
      }

      public function download($id)
      {
        $daftar = PendaftaranPengujian::find($id);
        if(file_exists(public_path('storage/'.$daftar->bukti)))
        {
          return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
        }
        return response([
          'status' => 'error'
        ],500);
      }
}
