<?php

namespace App\Http\Controllers\API\Download;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Files;
use App\Models\Picture;
use App\Models\VirtualAccount\VirtualAccount;

use Zipper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class DownloadController extends Controller
{
    public function multiple($id, $type)
    {
        if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        Storage::disk('public')->move($c->url, $newname.$c->filename);
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $newname.$c->filename,
                        ];
                    }
                }
                $now = Carbon::now()->format('Ymdhis');
                Zipper::make(public_path('storage/'.$type.$now.'.zip'))->add($files)->close();
                
                for ($i=0; $i < count($rename) ; $i++) { 
                    Storage::disk('public')->move($rename[$i]['new'], $rename[$i]['old']);
                }

                if(file_exists(public_path('storage/'.$type.$now.'.zip')))
                {
                    return response()->download(public_path('storage/'.$type.$now.'.zip'));
                }
                return response([
                    'status' => 'error'
                ],500);
            }
            return response([
                'status' => 'error'
            ],500);
        }
    }

    public function va($id)
    {
        $check = VirtualAccount::find($id);
        $files = [];
        $rename = [];        
        if($check->count() > 0)
        {
            if(file_exists(public_path('storage/'.$check->bukti_url)))
            {
                // $files[] = public_path('storage/'.$check->bukti_url);
                $newname = '';
                $splitname = explode("/",$check->bukti_url);
                for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                    $newname .= $splitname[$i].'/';
                }
                Storage::disk('public')->move($check->bukti_url, $newname.$check->bukti_filename);
                $files[] = public_path('storage/'.$newname.$check->bukti_filename);
                $rename[] = [
                    'old' => $check->bukti_url,
                    'new' => $newname.$check->bukti_filename,
                ];
            }

            $now = Carbon::now()->format('Ymdhis');

            Zipper::make(public_path('storage/'.$check->id.$now.'.zip'))->add($files)->close();
            
            for ($i=0; $i < count($rename) ; $i++) { 
                Storage::disk('public')->move($rename[$i]['new'], $rename[$i]['old']);
            }

            if(file_exists(public_path('storage/'.$check->id.$now.'.zip')))
            {
                return response()->download(public_path('storage/'.$check->id.$now.'.zip'));
            }
            return response([
                'status' => 'error'
            ],500);
        }
        return response([
            'status' => 'error'
        ],500);
    }
}
