<?php

namespace App\Http\Controllers\API\PengirimanLaporan;

/* Base App */
use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;
use App\Http\Controllers\Controller;

/* Validation */

/* Models */
use App\Models\LaporanPengujian\LaporanPengujian;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\Authentication\Role;
use App\Models\Picture;
use App\Models\Files;


/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Zipper;
use Excel;

class PengirimanLaporanController extends Controller
{
    public function grid(Request $request)
    {
        $data = [];
        if(auth()->user()->hasRole(['yan-kalibrasi'])){
            $data = LaporanPengujian::with('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','pengujian.penerimaan.konfirmasi.va.surat.detail','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detaillaporan.detail_pengujian.detail_penerimaan_barang.detail_pp.jenis','detaillaporan.detail_pengujian.pelaksana','detaillaporan.detail_pengujian.detailpelaksana.pelaksanalaporan')
            ->join('trans_pengujian','trans_laporan_pengujian.pengujian_id','=','trans_pengujian.id')
            ->join('trans_penerimaan_barang','trans_pengujian.penerimaan_id','=','trans_penerimaan_barang.id')
            ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
            ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
            ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
           	->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
           	->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
            ->where('trans_laporan_pengujian.status', 0)
            ->whereHas('pengujian.penerimaan', function($u){
                $u->byLayanan(1);
            })
            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
                    $pp->where('no_order','like','%'.$no_order.'%');
                 });
            })
            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
                    $pp->where('no_surat','like','%'.$no_surat.'%');
                 });
            })
            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                 });
            })
            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
                    $pp->where('layanan_id',$jenis_pelayanan_id);
                 });
            })
            ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($wbs_io){
                    $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
                 });
            })
            ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                 });
            })
            ->select('trans_laporan_pengujian.*');
            // if (!isset(request()->order[0]['column'])) {
            //     $data->orderBy('created_at', 'desc');
            // }
            if($sort = $request->sort){
                if($sort == 'order'){
                  $data->orderBy('trans_pp.tgl_order','asc');
                }
                if($sort == 'surat'){
                  $data->orderBy('trans_pp.tgl_surat','asc');
                }
            }else{
              $data->orderBy('trans_laporan_pengujian.created_at','desc');
            }
        }elseif(auth()->user()->hasRole(['yan-uji'])){
             $data = LaporanPengujian::with('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','pengujian.penerimaan.konfirmasi.va.surat.detail','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detaillaporan.detail_pengujian.detail_penerimaan_barang.detail_pp.jenis','detaillaporan.detail_pengujian.pelaksana','detaillaporan.detail_pengujian.detailpelaksana.pelaksanalaporan')
             ->join('trans_pengujian','trans_laporan_pengujian.pengujian_id','=','trans_pengujian.id')
            ->join('trans_penerimaan_barang','trans_pengujian.penerimaan_id','=','trans_penerimaan_barang.id')
            ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
            ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
            ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
           ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
           ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
            ->where('trans_laporan_pengujian.status', 0)
            ->whereHas('pengujian.penerimaan', function($u){
                $u->byLayananV2([2,3,4,5]);
            })
            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
                    $pp->where('no_order','like','%'.$no_order.'%');
                 });
            })
            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
                    $pp->where('no_surat','like','%'.$no_surat.'%');
                 });
            })
            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                 });
            })
            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
                    $pp->where('layanan_id',$jenis_pelayanan_id);
                 });
            })
            ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($wbs_io){
                    $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
                 });
            })
            ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                 });
            })
            ->select('trans_laporan_pengujian.*');
            // if (!isset(request()->order[0]['column'])) {
            //     $data->orderBy('created_at', 'desc');
            // }
            if($sort = $request->sort){
                if($sort == 'order'){
                  $data->orderBy('trans_pp.tgl_order','asc');
                }
                if($sort == 'surat'){
                  $data->orderBy('trans_pp.tgl_surat','asc');
                }
            }else{
              $data->orderBy('trans_laporan_pengujian.created_at','desc');
            }
        }else{
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }
        
        //Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     $data->orderBy('created_at', 'desc');
        // }else{
        //     $data =[];
        // }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function histori(Request $request)
    {
        if(auth()->user()->hasRole(['yan-kalibrasi'])){
            $data = LaporanPengujian::with('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','pengujian.penerimaan.konfirmasi.va.surat.detail','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detaillaporan.detail_pengujian.detail_penerimaan_barang.detail_pp.jenis','detaillaporan.detail_pengujian.pelaksana','detaillaporan.detail_pengujian.detailpelaksana.pelaksanalaporan')
            ->join('trans_pengujian','trans_laporan_pengujian.pengujian_id','=','trans_pengujian.id')
            ->join('trans_penerimaan_barang','trans_pengujian.penerimaan_id','=','trans_penerimaan_barang.id')
            ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
            ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
            ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
           ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
           ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
            ->where('trans_laporan_pengujian.status', 1)
            ->whereHas('pengujian.penerimaan', function($u){
                $u->byLayanan(1);
            })
            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
                    $pp->where('no_order','like','%'.$no_order.'%');
                 });
            })
            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
                    $pp->where('no_surat','like','%'.$no_surat.'%');
                 });
            })
            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                 });
            })
            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
                    $pp->where('layanan_id',$jenis_pelayanan_id);
                 });
            })
            ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($wbs_io){
                    $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
                 });
            })
            ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                 });
            })
            ->select('trans_laporan_pengujian.*');
            if($sort = $request->sort){
                if($sort == 'order'){
                  $data->orderBy('trans_pp.tgl_order','asc');
                }
                if($sort == 'surat'){
                  $data->orderBy('trans_pp.tgl_surat','asc');
                }
            }else{
              $data->orderBy('trans_laporan_pengujian.created_at','desc');
            }
            // if (!isset(request()->order[0]['column'])) {
            //     $data->orderBy('created_at', 'desc');
            // }
        }elseif(auth()->user()->hasRole(['yan-uji'])){
             $data = LaporanPengujian::with('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','pengujian.penerimaan.konfirmasi.va.surat.detail','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detaillaporan.detail_pengujian.detail_penerimaan_barang.detail_pp.jenis','detaillaporan.detail_pengujian.pelaksana','detaillaporan.detail_pengujian.detailpelaksana.pelaksanalaporan')
             ->join('trans_pengujian','trans_laporan_pengujian.pengujian_id','=','trans_pengujian.id')
            ->join('trans_penerimaan_barang','trans_pengujian.penerimaan_id','=','trans_penerimaan_barang.id')
            ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
            ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
            ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
           ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
           ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
            ->where('trans_laporan_pengujian.status', 1)
            ->whereHas('pengujian.penerimaan', function($u){
                $u->byLayananV2([2,3,4,5]);
            })
            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
                    $pp->where('no_order','like','%'.$no_order.'%');
                 });
            })
            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
                    $pp->where('no_surat','like','%'.$no_surat.'%');
                 });
            })
            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                 });
            })
            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
                    $pp->where('layanan_id',$jenis_pelayanan_id);
                 });
            })
            ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($wbs_io){
                    $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
                 });
            })
            ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                 $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                 });
            })
            ->select('trans_laporan_pengujian.*');
            if($sort = $request->sort){
                if($sort == 'order'){
                  $data->orderBy('trans_pp.tgl_order','asc');
                }
                if($sort == 'surat'){
                  $data->orderBy('trans_pp.tgl_surat','asc');
                }
            }else{
              $data->orderBy('trans_laporan_pengujian.created_at','desc');
            }
            // if (!isset(request()->order[0]['column'])) {
            //     $data->orderBy('created_at', 'desc');
            // }
        }else{
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }
        
        //Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     $data->orderBy('created_at', 'desc');
        // }else{
        //     $data =[];
        // }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function download($id)
    {
        $daftar = PendaftaranPengujian::find($id);
        if(file_exists(public_path('storage/'.$daftar->bukti)))
        {
            return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
        }

        return response([
            'status' => 'error'
        ],500);
    }
}
