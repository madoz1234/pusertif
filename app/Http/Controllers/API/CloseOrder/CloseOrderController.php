<?php

namespace App\Http\Controllers\API\CloseOrder;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Unlu\Laravel\Api\QueryBuilder;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\CloseOrder\CloseOrderRequest;

/* Models */
use App\Models\KajiUlang\KajiUlang;
use App\Models\CloseOrder\CloseOrder;
use App\Models\CloseOrder\CloseOrderDetail;
use App\Models\PenerimaanBarang\PenerimaanBarang;
use App\Models\PenerimaanBarang\PenerimaanBarangDetail;
use App\Models\VirtualAccount\KonfirmasiPembayaran;
use App\Models\LaporanPengujian\LaporanPengujian;
use App\Models\LaporanPengujian\LaporanPengujianDetail;
use App\Models\Pengujian\Pengujian;
use App\Models\Pengujian\PengujianDetail;
use App\Models\Authentication\Role;
use App\Models\Picture;
use App\Models\Files;
use Illuminate\Support\Facades\Storage;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Zipper;


class CloseOrderController extends Controller
{
    public function grid(Request $request)
    {
        $user = auth()->user();
        $layanan_id = [];
        if($user->hasRole(['msb-kalibrasi'])){
          $layanan_id = [1];
        }elseif($user->hasRole(['msb-siskit'])){
          $layanan_id = [2];
        }elseif($user->hasRole(['msb-sistgi'])){
          $layanan_id = [3];
        }elseif($user->hasRole(['msb-tegangan-rendah'])){
          $layanan_id = [4];
        }elseif($user->hasRole(['msb-tegangan-tinggi'])){
          $layanan_id = [5];
        }else{
          return response()->json([
            'status' => false,
            'message' => 'page not found'
          ],404);
        }
        // $data =[];

        // if($user->hasRole(['yan-kalibrasi']) || $user->hasRole(['yan-uji'])){
          $data = PenerimaanBarang::with('pengujian.penerimaan','pengujian.detailpengujian','detail_penerimaan_barang.penerimaan','konfirmasi.va.surat.kaji_ulang.pp.detail.jenis','penerima','konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp','closeorder.detailclose','detail_penerimaan_barang.detail_pp','detail_penerimaan_barang.detail_pengujian.detaillaporan.laporanpengujian','detail_penerimaan_barang.detail_pp.jenis')
                  ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
                  ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                  ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                 ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                 ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                  ->doesntHave('closeorder')
                  ->byLayananV2($layanan_id)
                  ->select('trans_penerimaan_barang.*');


          if($no_order = $request->no_order){
              $data->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                  $pp->where('no_order','like','%'.$no_order.'%')
	                 ->where('tipe_surat', 0);
              });
          }
          if($perusahaan_id = $request->perusahaan_id){
              $data->whereHas('konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use ($perusahaan_id){
                  $pelanggan->where('perusahaan_id',$perusahaan_id)
                   ->where('tipe_surat', 0);
              });
          }
          if($no_surat = $request->no_surat){
              $data->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                  $pp->where('no_surat','like','%'.$no_surat.'%')
	                 ->where('tipe_surat', 0);
              });
          }
          if($tanggal_order = $request->tanggal_order){
              $data->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                  $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
	                 ->where('tipe_surat', 0);
              });
          }
          if($jenis_pelayanan_id = $request->jenis_pelayanan_id){
              $data->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                  $pp->where('layanan_id',$jenis_pelayanan_id)
	                 ->where('tipe_surat', 0);
              });
          }
          if($wbs_io = $request->wbs_io){
              $data->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                  $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                  			 ->where('tipe', 0);
              });
          }

          if($sort = $request->sort){
              if($sort == 'order'){
                $data->orderBy('trans_pp.tgl_order','asc');
              }
              if($sort == 'surat'){
                $data->orderBy('trans_pp.tgl_surat','asc');
              }
          }else{
            $data->orderBy('trans_penerimaan_barang.created_at','desc');
          }
        $page = $data->paginate(10);
        // dd($page[0]);
        if ($page[0] == null) {
          return response()->json([
            'status' => false,
            'message' => 'page not found'
          ],404);
        }

        return response()->json($page);
    }

    public function historis(Request $request)
    {
      $user = auth()->user();
      $layanan_id = [];
      if($user->hasRole(['msb-kalibrasi'])){
        $layanan_id = [1];
      }elseif($user->hasRole(['msb-siskit'])){
        $layanan_id = [2];
      }elseif($user->hasRole(['msb-sistgi'])){
        $layanan_id = [3];
      }elseif($user->hasRole(['msb-tegangan-rendah'])){
        $layanan_id = [4];
      }elseif($user->hasRole(['msb-tegangan-tinggi'])){
        $layanan_id = [5];
      }else{
        return response()->json([
          'status' => false,
          'message' => 'page not found'
        ],404);
      }
        // $data =[];

        // if($user->hasRole(['yan-kalibrasi']) || $user->hasRole(['yan-uji'])){
          $data = PenerimaanBarang::with('pengujian.penerimaan','pengujian.detailpengujian','detail_penerimaan_barang.penerimaan','konfirmasi.va.surat.kaji_ulang.pp.detail.jenis','penerima','konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp','closeorder.detailclose','detail_penerimaan_barang.detail_pp','detail_penerimaan_barang.detail_pengujian.detaillaporan.laporanpengujian')
                  ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
                    ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                    ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                   ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                   ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                   ->has('closeorder')
                   ->byLayananV2($layanan_id)
                   ->select('trans_penerimaan_barang.*');

          // if (!isset(request()->order[0]['column'])) {
          //   $data->orderBy('created_at','desc');
          // }
          if($no_order = $request->no_order){
              $data->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                  $pp->where('no_order','like','%'.$no_order.'%')
                  ->where('tipe_surat', 0);
              });
          }
          if($perusahaan_id = $request->perusahaan_id){
              $data->whereHas('konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use ($perusahaan_id){
                  $pelanggan->where('perusahaan_id',$perusahaan_id)
                   ->where('tipe_surat', 0);
              });
          }
          if($no_surat = $request->no_surat){
              $data->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                  $pp->where('no_surat','like','%'.$no_surat.'%')
                  ->where('tipe_surat', 0);
              });
          }
          if($tanggal_order = $request->tanggal_order){
              $data->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                  $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                  ->where('tipe_surat', 0);
              });
          }
          if($jenis_pelayanan_id = $request->jenis_pelayanan_id){
              $data->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                  $pp->where('layanan_id',$jenis_pelayanan_id)
                  ->where('tipe_surat', 0);
              });
          }
          if($wbs_io = $request->wbs_io){
              $data->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                  $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                  			->where('tipe', 0);
              });
          }
          if($sort = $request->sort){
              if($sort == 'order'){
                $data->orderBy('trans_pp.tgl_order','asc');
              }
              if($sort == 'surat'){
                $data->orderBy('trans_pp.tgl_surat','asc');
              }
          }else{
            $data->orderBy('trans_penerimaan_barang.created_at','desc');
          }

        $page = $data->paginate(10);
        // dd($page[0]);
        if ($page[0] == null) {
          return response()->json([
            'status' => false,
            'message' => 'page not found'
          ],404);
        }

        return response()->json($page);
    }

    public function download($id)
    {
      $daftar = PendaftaranPengujian::find($id);
      if(file_exists(public_path('storage/'.$daftar->bukti)))
      {
        return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
      }

      return response([
        'status' => 'error'
      ],500);
    }
}
