<?php

namespace App\Http\Controllers\API\PenerimaanBarangUji;

/* Base App */
use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\PenerimaanBarang\PenerimaanBarangRequest;
/* Models */
use App\Models\Authentication\Role;
use App\Models\VirtualAccount\KonfirmasiPembayaran;
use App\Models\PenerimaanBarang\PenerimaanBarang;
use App\Models\PenerimaanBarang\PenerimaanBarangDetail;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\FrontEnd\PendaftaranPengujianDetail;
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\KajiUlang\KajiUlang;
use App\Models\KajiUlang\Detail;
// use Barryvdh\DomPDF\PDF;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Excel;
use Auth;
use DB;
use PDF;

class PenerimaanBarangUjiBUController extends Controller
{
    // public function grid(Request $request)
    // {
    //     if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi'])){
	   //      $data = KonfirmasiPembayaran::with('va.surat.kaji_ulang.pp','va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','va.surat.kaji_ulang.pp.pelayanan','va.surat.kaji_ulang.pp.lingkup','va.surat.kaji_ulang.pp.detail.jenis','va.surat.kaji_ulang.surat.detail','va.surat.kaji_ulang.detail.lokasi','va.surat.kaji_ulang.detail.mata_uji','va.surat.kaji_ulang.detail.detail_pendaftaran','va.surat.files','realisasi.realisasi_detail')->where('status', 3)
			 //        						->doesntHave('penerimaan')
	   //                               		->select('*');

	   //      if (!isset(request()->order[0]['column'])) {
	   //          $data->orderBy('created_at', 'desc');
	   //      }
    // 	}else{
    // 		$data =[];
    // 	}
    //     $page = $data->paginate(10);
    //             // dd($page[0]);
    //     if ($page[0] == null) {
    //         return response()->json([
    //             'status' => false,
    //             'message' => 'page not found'
    //         ],404);
    //     }

    //     return response()->json($page);
    // }

    public function grid(Request $Request){
        if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['adminlab-siskit']) || auth()->user()->hasRole(['adminlab-sistgi']) || auth()->user()->hasRole(['adminlab-tegangan-rendah']) || auth()->user()->hasRole(['adminlab-tegangan-tinggi'])){
            if(auth()->user()->hasRole(['adminlab-siskit'])){
                $data = KonfirmasiPembayaran::with('va.surat.kaji_ulang.pp','va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','va.surat.kaji_ulang.pp.pelayanan','va.surat.kaji_ulang.pp.lingkup','va.surat.kaji_ulang.pp.detail.jenis','va.surat.kaji_ulang.surat.detail','va.surat.kaji_ulang.detail.lokasi','va.surat.kaji_ulang.detail.mata_uji','va.surat.kaji_ulang.detail.detail_pendaftaran','va.surat.files','realisasi.realisasi_detail')
                                                ->where('status', '>', 0)
                                                 ->whereHas('va', function($z){
                                                    $z->whereHas('surat', function($y){
                                                        $y->whereHas('kaji_ulang', function($x){
                                                            $x->whereHas('pp', function($m){
                                                                $m->where('jenis', 1)
                                                                  ->where('layanan_id', 2);
                                                            });
                                                        });
                                                    });
                                                })
                                                ->doesntHave('penerimaan')
                                                ->select('*');
            }elseif(auth()->user()->hasRole(['adminlab-sistgi'])){
                $data = KonfirmasiPembayaran::with('va.surat.kaji_ulang.pp','va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','va.surat.kaji_ulang.pp.pelayanan','va.surat.kaji_ulang.pp.lingkup','va.surat.kaji_ulang.pp.detail.jenis','va.surat.kaji_ulang.surat.detail','va.surat.kaji_ulang.detail.lokasi','va.surat.kaji_ulang.detail.mata_uji','va.surat.kaji_ulang.detail.detail_pendaftaran','va.surat.files','realisasi.realisasi_detail')
                                                ->where('status', '>', 0)
                                                 ->whereHas('va', function($z){
                                                    $z->whereHas('surat', function($y){
                                                        $y->whereHas('kaji_ulang', function($x){
                                                            $x->whereHas('pp', function($m){
                                                                $m->where('jenis', 1)
                                                                  ->where('layanan_id', 3);
                                                            });
                                                        });
                                                    });
                                                })
                                                ->doesntHave('penerimaan')
                                                ->select('*');
            }elseif(auth()->user()->hasRole(['adminlab-tegangan-rendah'])){
                $data = KonfirmasiPembayaran::with('va.surat.kaji_ulang.pp','va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','va.surat.kaji_ulang.pp.pelayanan','va.surat.kaji_ulang.pp.lingkup','va.surat.kaji_ulang.pp.detail.jenis','va.surat.kaji_ulang.surat.detail','va.surat.kaji_ulang.detail.lokasi','va.surat.kaji_ulang.detail.mata_uji','va.surat.kaji_ulang.detail.detail_pendaftaran','va.surat.files','realisasi.realisasi_detail')
                                                ->where('status', '>', 0)
                                                 ->whereHas('va', function($z){
                                                    $z->whereHas('surat', function($y){
                                                        $y->whereHas('kaji_ulang', function($x){
                                                            $x->whereHas('pp', function($m){
                                                                $m->where('jenis', 1)
                                                                  ->where('layanan_id', 4);
                                                            });
                                                        });
                                                    });
                                                })
                                                ->doesntHave('penerimaan')
                                                ->select('*');
            }elseif(auth()->user()->hasRole(['adminlab-tegangan-tinggi'])){
                $data = KonfirmasiPembayaran::with('va.surat.kaji_ulang.pp','va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','va.surat.kaji_ulang.pp.pelayanan','va.surat.kaji_ulang.pp.lingkup','va.surat.kaji_ulang.pp.detail.jenis','va.surat.kaji_ulang.surat.detail','va.surat.kaji_ulang.detail.lokasi','va.surat.kaji_ulang.detail.mata_uji','va.surat.kaji_ulang.detail.detail_pendaftaran','va.surat.files','realisasi.realisasi_detail')
                                                ->where('status', '>', 0)
                                                ->whereHas('va', function($z){
                                                    $z->whereHas('surat', function($y){
                                                        $y->whereHas('kaji_ulang', function($x){
                                                            $x->whereHas('pp', function($m){
                                                                $m->where('jenis', 1)
                                                                  ->where('layanan_id', 5);
                                                            });
                                                        });
                                                    });
                                                })
                                                ->doesntHave('penerimaan')
                                                ->select('*');
            }else{
                $data = KonfirmasiPembayaran::with('va.surat.kaji_ulang.pp','va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','va.surat.kaji_ulang.pp.pelayanan','va.surat.kaji_ulang.pp.lingkup','va.surat.kaji_ulang.pp.detail.jenis','va.surat.kaji_ulang.surat.detail','va.surat.kaji_ulang.detail.lokasi','va.surat.kaji_ulang.detail.mata_uji','va.surat.kaji_ulang.detail.detail_pendaftaran','va.surat.files','realisasi.realisasi_detail')
                                                ->where('status', '>', 0)
                                                ->whereHas('va', function($z){
                                                    $z->whereHas('surat', function($y){
                                                        $y->whereHas('kaji_ulang', function($x){
                                                            $x->whereHas('pp', function($m){
                                                                $m->where('jenis', 1);
                                                            });
                                                        });
                                                    });
                                                })
                                                ->doesntHave('penerimaan')
                                                ->select('*');
            }
            if (!isset(request()->order[0]['column'])) {
                $data->orderBy('created_at', 'desc');
            }
        }elseif(auth()->user()->hasRole(['yan-kalibrasi']) || auth()->user()->hasRole(['adminlab-kalibrasi'])){
            $data = KonfirmasiPembayaran::with('va.surat.kaji_ulang.pp','va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','va.surat.kaji_ulang.pp.pelayanan','va.surat.kaji_ulang.pp.lingkup','va.surat.kaji_ulang.pp.detail.jenis','va.surat.kaji_ulang.surat.detail','va.surat.kaji_ulang.detail.lokasi','va.surat.kaji_ulang.detail.mata_uji','va.surat.kaji_ulang.detail.detail_pendaftaran','va.surat.files','realisasi.realisasi_detail')
                                            ->where('status', '>', 0)
                                            ->whereHas('va', function($z){
                                                $z->whereHas('surat', function($y){
                                                    $y->whereHas('kaji_ulang', function($x){
                                                        $x->whereHas('pp', function($m){
                                                            $m->where('jenis', 0);
                                                        });
                                                    });
                                                });
                                            })
                                            ->doesntHave('penerimaan')
                                            ->select('*');

            if (!isset(request()->order[0]['column'])) {
                $data->orderBy('created_at', 'desc');
            }
        }else{
            $data =[];
        }

        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function progress(Request $request)
    {
        if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi'])){
	        $data = PenerimaanBarang::with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.detail.jenis','konfirmasi.va.surat.kaji_ulang.surat.detail','konfirmasi.va.surat.kaji_ulang.detail.lokasi','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran','konfirmasi.va.surat.files','konfirmasi.realisasi.realisasi_detail','detail_penerimaan_barang','penerima')->where('status', 1)
	                                 	->select('*');

	        if (!isset(request()->order[0]['column'])) {
	            $data->orderBy('created_at', 'desc');
	        }
    	}else{
    		$data =[];
    	}
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function historis(Request $request)
    {
        // if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi'])){
        //     $data = [];

            // if (!isset(request()->order[0]['column'])) {
            //     $data->orderBy('created_at', 'desc');
            // }
        // }
        // else{
            // $data =[];
        // }
        // $page = $data->paginate(10);
        //         // dd($page[0]);
        // if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        // }

        // return response()->json($page);
    }
    public function printSuratPdf(Request $request, $id){
        $record = PenerimaanBarang::find($id);
        $data = [
            'records' => $record,
        ];
        $pdf = PDF::loadView('modules.penerimaan-barang-uji.cetak.cetak', $data);
        return $pdf->stream('Surat Penerimaan Barang '.$record->tanggal.'.pdf',array("Attachment" => false));
    }

    public function printKartuPdf(Request $request, $id){
        $record = PenerimaanBarang::find($id);
        $data = [
            'records' => $record,
        ];
        $pdf = PDF::loadView('modules.penerimaan-barang-uji.cetak.cetak-kartu', $data);
        return $pdf->stream('Kartu Gantung '.$record->tanggal.'.pdf',array("Attachment" => false));
    }

    public function download($id)
    {
        $daftar = PendaftaranPengujian::find($id);
        if(file_exists(public_path('storage/'.$daftar->bukti)))
        {
            return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
        }

        return response([
            'status' => 'error'
        ],500);
    }
}
