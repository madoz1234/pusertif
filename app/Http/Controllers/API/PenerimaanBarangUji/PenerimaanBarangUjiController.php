<?php

namespace App\Http\Controllers\API\PenerimaanBarangUji;

/* Base App */
use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\PenerimaanBarang\PenerimaanBarangRequest;
/* Models */
use App\Models\Authentication\Role;
use App\Models\VirtualAccount\KonfirmasiPembayaran;
use App\Models\PenerimaanBarang\PenerimaanBarang;
use App\Models\PenerimaanBarang\PenerimaanBarangDetail;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\FrontEnd\PendaftaranPengujianDetail;
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\KajiUlang\KajiUlang;
use App\Models\KajiUlang\Detail;
// use Barryvdh\DomPDF\PDF;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Excel;
use Auth;
use DB;
use PDF;

class PenerimaanBarangUjiController extends Controller
{
    public function grid(Request $request){
        if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['adminlab-siskit']) || auth()->user()->hasRole(['adminlab-sistgi']) || auth()->user()->hasRole(['adminlab-tegangan-rendah']) || auth()->user()->hasRole(['adminlab-tegangan-tinggi'])){
            if(auth()->user()->hasRole(['adminlab-siskit'])){
                $data = KonfirmasiPembayaran::with('va.surat.kaji_ulang.pp','va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','va.surat.kaji_ulang.pp.pelayanan','va.surat.kaji_ulang.pp.lingkup','va.surat.kaji_ulang.pp.detail.jenis','va.surat.kaji_ulang.surat.detail','va.surat.kaji_ulang.detail.lokasi','va.surat.kaji_ulang.detail.mata_uji','va.surat.kaji_ulang.detail.detail_pendaftaran','va.surat.files','realisasi.realisasi_detail')
                                                ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                                ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                               ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                               ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                                ->where('trans_konfrimasi_pembayaran.status', '>', 0)
                                                 ->whereHas('va', function($z){
                                                    $z->whereHas('surat', function($y){
                                                        $y->whereHas('kaji_ulang', function($x){
                                                            $x->whereHas('pp', function($m){
                                                                $m->where('jenis', 1)
                                                                  ->where('layanan_id', 2);
                                                            });
                                                        });
                                                    });
                                                })
                                                 ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                                                     $q->where('trans_konfirmasi_pembayaran.wbs_io','like','%'.$wbs_io.'%');
                                                })
                                                ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                        $pp->where('no_order','like','%'.$no_order.'%');
                                                     });
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                        $pp->where('layanan_id',$jenis_pelayanan_id);
                                                     });
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                        $pp->where('no_surat','like','%'.$no_surat.'%');
                                                     });
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                     });
                                                })
                                                ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use ($perusahaan_id){
                                                        $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                     });
                                                })
                                                ->doesntHave('penerimaan')
                                                ->select('trans_konfirmasi_pembayaran.*');
            }elseif(auth()->user()->hasRole(['adminlab-sistgi'])){
                $data = KonfirmasiPembayaran::with('va.surat.kaji_ulang.pp','va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','va.surat.kaji_ulang.pp.pelayanan','va.surat.kaji_ulang.pp.lingkup','va.surat.kaji_ulang.pp.detail.jenis','va.surat.kaji_ulang.surat.detail','va.surat.kaji_ulang.detail.lokasi','va.surat.kaji_ulang.detail.mata_uji','va.surat.kaji_ulang.detail.detail_pendaftaran','va.surat.files','realisasi.realisasi_detail')
                                                ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                                ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                               ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                               ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                                ->where('trans_konfirmasi_pembayaran.status', '>', 0)
                                                 ->whereHas('va', function($z){
                                                    $z->whereHas('surat', function($y){
                                                        $y->whereHas('kaji_ulang', function($x){
                                                            $x->whereHas('pp', function($m){
                                                                $m->where('jenis', 1)
                                                                  ->where('layanan_id', 3);
                                                            });
                                                        });
                                                    });
                                                })
                                                 ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                                                     $q->where('trans_konfirmasi_pembayaran.wbs_io','like','%'.$wbs_io.'%');
                                                })
                                                ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                        $pp->where('no_order','like','%'.$no_order.'%');
                                                     });
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                        $pp->where('layanan_id',$jenis_pelayanan_id);
                                                     });
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                        $pp->where('no_surat','like','%'.$no_surat.'%');
                                                     });
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                     });
                                                })
                                                ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use ($perusahaan_id){
                                                        $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                     });
                                                })
                                                ->doesntHave('penerimaan')
                                                ->select('trans_konfirmasi_pembayaran.*');
            }elseif(auth()->user()->hasRole(['adminlab-tegangan-rendah'])){
                $data = KonfirmasiPembayaran::with('va.surat.kaji_ulang.pp','va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','va.surat.kaji_ulang.pp.pelayanan','va.surat.kaji_ulang.pp.lingkup','va.surat.kaji_ulang.pp.detail.jenis','va.surat.kaji_ulang.surat.detail','va.surat.kaji_ulang.detail.lokasi','va.surat.kaji_ulang.detail.mata_uji','va.surat.kaji_ulang.detail.detail_pendaftaran','va.surat.files','realisasi.realisasi_detail')
                                                ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                                ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                               ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                               ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                                ->where('trans_konfirmasi_pembayaran.status', '>', 0)
                                                 ->whereHas('va', function($z){
                                                    $z->whereHas('surat', function($y){
                                                        $y->whereHas('kaji_ulang', function($x){
                                                            $x->whereHas('pp', function($m){
                                                                $m->where('jenis', 1)
                                                                  ->where('layanan_id', 4);
                                                            });
                                                        });
                                                    });
                                                })
                                                 ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                                                     $q->where('trans_konfirmasi_pembayaran.wbs_io','like','%'.$wbs_io.'%');
                                                })
                                                ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                        $pp->where('no_order','like','%'.$no_order.'%');
                                                     });
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                        $pp->where('layanan_id',$jenis_pelayanan_id);
                                                     });
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                        $pp->where('no_surat','like','%'.$no_surat.'%');
                                                     });
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                     });
                                                })
                                                ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use ($perusahaan_id){
                                                        $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                     });
                                                })
                                                ->doesntHave('penerimaan')
                                                ->select('trans_konfirmasi_pembayaran.*');
            }elseif(auth()->user()->hasRole(['adminlab-tegangan-tinggi'])){
                $data = KonfirmasiPembayaran::with('va.surat.kaji_ulang.pp','va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','va.surat.kaji_ulang.pp.pelayanan','va.surat.kaji_ulang.pp.lingkup','va.surat.kaji_ulang.pp.detail.jenis','va.surat.kaji_ulang.surat.detail','va.surat.kaji_ulang.detail.lokasi','va.surat.kaji_ulang.detail.mata_uji','va.surat.kaji_ulang.detail.detail_pendaftaran','va.surat.files','realisasi.realisasi_detail')
                                                ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                                ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                               ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                               ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                                ->where('trans_konfirmasi_pembayaran.status', '>', 0)
                                                ->whereHas('va', function($z){
                                                    $z->whereHas('surat', function($y){
                                                        $y->whereHas('kaji_ulang', function($x){
                                                            $x->whereHas('pp', function($m){
                                                                $m->where('jenis', 1)
                                                                  ->where('layanan_id', 5);
                                                            });
                                                        });
                                                    });
                                                })
                                                ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                                                     $q->where('trans_konfirmasi_pembayaran.wbs_io','like','%'.$wbs_io.'%');
                                                })
                                                ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                        $pp->where('no_order','like','%'.$no_order.'%');
                                                     });
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                        $pp->where('layanan_id',$jenis_pelayanan_id);
                                                     });
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                        $pp->where('no_surat','like','%'.$no_surat.'%');
                                                     });
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                     });
                                                })
                                                ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use ($perusahaan_id){
                                                        $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                     });
                                                })
                                                ->doesntHave('penerimaan')
                                                ->select('trans_konfirmasi_pembayaran.*');
            }else{
                $data = KonfirmasiPembayaran::with('va.surat.kaji_ulang.pp','va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','va.surat.kaji_ulang.pp.pelayanan','va.surat.kaji_ulang.pp.lingkup','va.surat.kaji_ulang.pp.detail.jenis','va.surat.kaji_ulang.surat.detail','va.surat.kaji_ulang.detail.lokasi','va.surat.kaji_ulang.detail.mata_uji','va.surat.kaji_ulang.detail.detail_pendaftaran','va.surat.files','realisasi.realisasi_detail')
                                                ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                                ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                               ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                               ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                                ->where('trans_konfirmasi_pembayaran.status', '>', 0)
                                                ->whereHas('va', function($z){
                                                    $z->whereHas('surat', function($y){
                                                        $y->whereHas('kaji_ulang', function($x){
                                                            $x->whereHas('pp', function($m){
                                                                $m->where('jenis', 1);
                                                            });
                                                        });
                                                    });
                                                })
                                                ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                                                     $q->where('trans_konfirmasi_pembayaran.wbs_io','like','%'.$wbs_io.'%');
                                                })
                                                ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                        $pp->where('no_order','like','%'.$no_order.'%');
                                                     });
                                                })
                                                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                        $pp->where('layanan_id',$jenis_pelayanan_id);
                                                     });
                                                })
                                                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                        $pp->where('no_surat','like','%'.$no_surat.'%');
                                                     });
                                                })
                                                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                     });
                                                })
                                                ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                     $q->whereHas('va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use ($perusahaan_id){
                                                        $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                     });
                                                })
                                                ->doesntHave('penerimaan')
                                                ->select('trans_konfirmasi_pembayaran.*');
            }
            // if (!isset(request()->order[0]['column'])) {
            //     $data->orderBy('created_at', 'desc');
            // }
            if($sort = $request->sort){
                if($sort == 'order'){
                  $data->orderBy('trans_pp.tgl_order','asc');
                }
                if($sort == 'surat'){
                  $data->orderBy('trans_pp.tgl_surat','asc');
                }
            }else{
              $data->orderBy('trans_konfirmasi_pembayaran.created_at','desc');
            }
        }elseif(auth()->user()->hasRole(['yan-kalibrasi']) || auth()->user()->hasRole(['adminlab-kalibrasi'])){
            $data = KonfirmasiPembayaran::with('va.surat.kaji_ulang.pp','va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','va.surat.kaji_ulang.pp.pelayanan','va.surat.kaji_ulang.pp.lingkup','va.surat.kaji_ulang.pp.detail.jenis','va.surat.kaji_ulang.surat.detail','va.surat.kaji_ulang.detail.lokasi','va.surat.kaji_ulang.detail.mata_uji','va.surat.kaji_ulang.detail.detail_pendaftaran','va.surat.files','realisasi.realisasi_detail')
                                            ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                            ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                           ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                           ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                            ->where('trans_konfirmasi_pembayaran.status', '>', 0)
                                            ->whereHas('va', function($z){
                                                $z->whereHas('surat', function($y){
                                                    $y->whereHas('kaji_ulang', function($x){
                                                        $x->whereHas('pp', function($m){
                                                            $m->where('jenis', 0);
                                                        });
                                                    });
                                                });
                                            })
                                            ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                                                 $q->where('trans_konfirmasi_pembayaran.wbs_io','like','%'.$wbs_io.'%');
                                            })
                                            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                 $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                    $pp->where('no_order','like','%'.$no_order.'%');
                                                 });
                                            })
                                            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                 $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                    $pp->where('layanan_id',$jenis_pelayanan_id);
                                                 });
                                            })
                                            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                 $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                    $pp->where('no_surat','like','%'.$no_surat.'%');
                                                 });
                                            })
                                            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                 $q->whereHas('va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                                 });
                                            })
                                            ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                 $q->whereHas('va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use ($perusahaan_id){
                                                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                 });
                                            })
                                            ->doesntHave('penerimaan')
                                            ->select('trans_konfirmasi_pembayaran.*');

            // if (!isset(request()->order[0]['column'])) {
            //     $data->orderBy('created_at', 'desc');
            // }
            if($sort = $request->sort){
                if($sort == 'order'){
                  $data->orderBy('trans_pp.tgl_order','asc');
                }
                if($sort == 'surat'){
                  $data->orderBy('trans_pp.tgl_surat','asc');
                }
            }else{
              $data->orderBy('trans_konfirmasi_pembayaran.created_at','desc');
            }
        }else{
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function progress(Request $request){
        if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['adminlab-siskit']) || auth()->user()->hasRole(['adminlab-sistgi']) || auth()->user()->hasRole(['adminlab-tegangan-rendah']) || auth()->user()->hasRole(['adminlab-tegangan-tinggi'])){
            if(auth()->user()->hasRole(['adminlab-siskit'])){
                $data = PenerimaanBarang::with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.detail.jenis','konfirmasi.va.surat.kaji_ulang.surat.detail','konfirmasi.va.surat.kaji_ulang.detail.lokasi','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran','konfirmasi.va.surat.files','konfirmasi.realisasi.realisasi_detail','detail_penerimaan_barang','penerima')
                                            ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
                                            ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                            ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                           ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                           ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                            ->whereHas('konfirmasi', function($u){
                                             $u->whereHas('va', function($z){
                                                $z->whereHas('surat', function($y){
                                                    $y->whereHas('kaji_ulang', function($x){
                                                        $x->whereHas('pp', function($m){
                                                            $m->where('jenis', 1)
                                                              ->where('layanan_id', 2)
                                                              ->where('tipe_surat', 0);
                                                        });
                                                    });
                                                });
                                             });
                                            })
                                            ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                                                 $q->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                                                    $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                                                    			->where('tipe', 0);
                                                 });
                                            })
                                            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                    $pp->where('no_order','like','%'.$no_order.'%')
                                                    	->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                    $pp->where('layanan_id',$jenis_pelayanan_id)
                                                    	->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                    $pp->where('no_surat','like','%'.$no_surat.'%')
                                                    	->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                                                    	->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use ($perusahaan_id){
                                                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                 });
                                            })
                                            ->where('trans_penerimaan_barang.status', 1)
                                            ->select('trans_penerimaan_barang.*');
            }elseif(auth()->user()->hasRole(['adminlab-sistgi'])){
                $data = PenerimaanBarang::with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.detail.jenis','konfirmasi.va.surat.kaji_ulang.surat.detail','konfirmasi.va.surat.kaji_ulang.detail.lokasi','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran','konfirmasi.va.surat.files','konfirmasi.realisasi.realisasi_detail','detail_penerimaan_barang','penerima')
                                            ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
                                            ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                            ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                           ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                           ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                            ->whereHas('konfirmasi', function($u){
                                             $u->whereHas('va', function($z){
                                                $z->whereHas('surat', function($y){
                                                    $y->whereHas('kaji_ulang', function($x){
                                                        $x->whereHas('pp', function($m){
                                                            $m->where('jenis', 1)
                                                              ->where('layanan_id', 3)
                                                              ->where('tipe_surat', 0);
                                                        });
                                                    });
                                                });
                                             });
                                            })
                                            ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                                                 $q->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                                                    $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                                                    			->where('tipe', 0);
                                                 });
                                            })
                                            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                    $pp->where('no_order','like','%'.$no_order.'%')
                                                    	->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                    $pp->where('layanan_id',$jenis_pelayanan_id)
                                                    	->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                    $pp->where('no_surat','like','%'.$no_surat.'%')
                                                    	->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                                                    	->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use ($perusahaan_id){
                                                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                 });
                                            })
                                            ->where('trans_penerimaan_barang.status', 1)
                                            ->select('trans_penerimaan_barang.*');
            }elseif(auth()->user()->hasRole(['adminlab-tegangan-rendah'])){
                $data = PenerimaanBarang::with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.detail.jenis','konfirmasi.va.surat.kaji_ulang.surat.detail','konfirmasi.va.surat.kaji_ulang.detail.lokasi','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran','konfirmasi.va.surat.files','konfirmasi.realisasi.realisasi_detail','detail_penerimaan_barang','penerima')
                                            ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
                                            ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                            ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                           ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                           ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                            ->whereHas('konfirmasi', function($u){
                                             $u->whereHas('va', function($z){
                                                $z->whereHas('surat', function($y){
                                                    $y->whereHas('kaji_ulang', function($x){
                                                        $x->whereHas('pp', function($m){
                                                            $m->where('jenis', 1)
                                                              ->where('layanan_id', 4)
                                                              ->where('tipe_surat', 0);
                                                        });
                                                    });
                                                });
                                             });
                                            })
                                            ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                                                 $q->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                                                    $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                                                    			->where('tipe', 0);
                                                 });
                                            })
                                            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                    $pp->where('no_order','like','%'.$no_order.'%')
	                                                    ->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                    $pp->where('layanan_id',$jenis_pelayanan_id)
	                                                    ->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                    $pp->where('no_surat','like','%'.$no_surat.'%')
	                                                    ->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
	                                                    ->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use ($perusahaan_id){
                                                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                 });
                                            })
                                            ->where('trans_penerimaan_barang.status', 1)
                                            ->select('trans_penerimaan_barang.*');
            }elseif(auth()->user()->hasRole(['adminlab-tegangan-tinggi'])){
                $data = PenerimaanBarang::with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.detail.jenis','konfirmasi.va.surat.kaji_ulang.surat.detail','konfirmasi.va.surat.kaji_ulang.detail.lokasi','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran','konfirmasi.va.surat.files','konfirmasi.realisasi.realisasi_detail','detail_penerimaan_barang','penerima')
                                            ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
                                            ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                            ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                           ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                           ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                            ->whereHas('konfirmasi', function($u){
                                             $u->whereHas('va', function($z){
                                                $z->whereHas('surat', function($y){
                                                    $y->whereHas('kaji_ulang', function($x){
                                                        $x->whereHas('pp', function($m){
                                                            $m->where('jenis', 1)
                                                              ->where('layanan_id', 5)
                                                              ->where('tipe_surat', 0);
                                                        });
                                                    });
                                                });
                                             });
                                            })
                                            ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                                                 $q->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                                                    $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                                                    			->where('tipe', 0);
                                                 });
                                            })
                                            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                    $pp->where('no_order','like','%'.$no_order.'%')
                                                    	->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                    $pp->where('layanan_id',$jenis_pelayanan_id)
                                                    	->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                    $pp->where('no_surat','like','%'.$no_surat.'%')
                                                    	->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                                                    	->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use ($perusahaan_id){
                                                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                 });
                                            })
                                            ->where('trans_penerimaan_barang.status', 1)
                                            ->select('trans_penerimaan_barang.*');
            }else{
                $data = PenerimaanBarang::with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.detail.jenis','konfirmasi.va.surat.kaji_ulang.surat.detail','konfirmasi.va.surat.kaji_ulang.detail.lokasi','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran','konfirmasi.va.surat.files','konfirmasi.realisasi.realisasi_detail','detail_penerimaan_barang','penerima')
                                            ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
                                            ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                            ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                           ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                           ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                            ->whereHas('konfirmasi', function($u){
                                             $u->whereHas('va', function($z){
                                                $z->whereHas('surat', function($y){
                                                    $y->whereHas('kaji_ulang', function($x){
                                                        $x->whereHas('pp', function($m){
                                                            $m->where('jenis', 1)
                                                            	->where('tipe_surat', 0);
                                                        });
                                                    });
                                                });
                                             });
                                            })
                                            ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                                                 $q->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                                                    $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                                                    			->where('tipe', 0);
                                                 });
                                            })
                                            ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                    $pp->where('no_order','like','%'.$no_order.'%')
                                                    	->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                    $pp->where('layanan_id',$jenis_pelayanan_id)
                                                    	->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                    $pp->where('no_surat','like','%'.$no_surat.'%')
                                                    	->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                    $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                                                    	->where('tipe_surat', 0);
                                                 });
                                            })
                                            ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                                 $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use ($perusahaan_id){
                                                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                                                 });
                                            })
                                            ->where('trans_penerimaan_barang.status', 1)
                                            ->select('trans_penerimaan_barang.*');
            }

            // if (!isset(request()->order[0]['column'])) {
            //     $data->orderBy('created_at', 'desc');
            // }
            if($sort = $request->sort){
                if($sort == 'order'){
                  $data->orderBy('trans_pp.tgl_order','asc');
                }
                if($sort == 'surat'){
                  $data->orderBy('trans_pp.tgl_surat','asc');
                }
            }else{
              $data->orderBy('trans_penerimaan_barang.created_at','desc');
            }
        }elseif(auth()->user()->hasRole(['yan-kalibrasi']) || auth()->user()->hasRole(['adminlab-kalibrasi'])){
            $data = PenerimaanBarang::with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.detail.jenis','konfirmasi.va.surat.kaji_ulang.surat.detail','konfirmasi.va.surat.kaji_ulang.detail.lokasi','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran','konfirmasi.va.surat.files','konfirmasi.realisasi.realisasi_detail','detail_penerimaan_barang','penerima')
                                        ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
                                            ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                            ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                           ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                           ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                        ->whereHas('konfirmasi', function($u){
                                         $u->whereHas('va', function($z){
                                            $z->whereHas('surat', function($y){
                                                $y->whereHas('kaji_ulang', function($x){
                                                    $x->whereHas('pp', function($m){
                                                        $m->where('jenis', 0)
                                                        	->where('tipe_surat', 0);
                                                    });
                                                });
                                            });
                                         });
                                        })
                                        ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                                             $q->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                                                $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                                                			->where('tipe', 0);
                                             });
                                        })
                                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                                                $pp->where('no_order','like','%'.$no_order.'%')
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                                                $pp->where('layanan_id',$jenis_pelayanan_id)
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                                                $pp->where('no_surat','like','%'.$no_surat.'%')
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                                                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                                                	->where('tipe_surat', 0);
                                             });
                                        })
                                        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use ($perusahaan_id){
                                                $pelanggan->where('perusahaan_id',$perusahaan_id);
                                             });
                                        })
                                        ->where('trans_penerimaan_barang.status', 1)
                                        ->select('trans_penerimaan_barang.*');

            // if (!isset(request()->order[0]['column'])) {
            //     $data->orderBy('created_at', 'desc');
            // }
            if($sort = $request->sort){
                if($sort == 'order'){
                  $data->orderBy('trans_pp.tgl_order','asc');
                }
                if($sort == 'surat'){
                  $data->orderBy('trans_pp.tgl_surat','asc');
                }
            }else{
              $data->orderBy('trans_penerimaan_barang.created_at','desc');
            }
        }else{
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }



    public function historis(Request $request)
    {
        // if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi'])){
        //     $data = [];

            // if (!isset(request()->order[0]['column'])) {
            //     $data->orderBy('created_at', 'desc');
            // }
        // }
        // else{
            // $data =[];
        // }
        // $page = $data->paginate(10);
        //         // dd($page[0]);
        // if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        // }

        // return response()->json($page);
    }
    public function printSuratPdf(Request $request, $id){
        $record = PenerimaanBarang::find($id);
        $data = [
            'records' => $record,
        ];
        $pdf = PDF::loadView('modules.penerimaan-barang-uji.cetak.cetak', $data);
        return $pdf->stream('Surat Penerimaan Barang '.$record->tanggal.'.pdf',array("Attachment" => false));
    }

    public function printKartuPdf(Request $request, $id){
        $record = PenerimaanBarang::find($id);
        $data = [
            'records' => $record,
        ];
        $pdf = PDF::loadView('modules.penerimaan-barang-uji.cetak.cetak-kartu', $data);
        return $pdf->stream('Kartu Gantung '.$record->tanggal.'.pdf',array("Attachment" => false));
    }

    public function download($id)
    {
        $daftar = PendaftaranPengujian::find($id);
        if(file_exists(public_path('storage/'.$daftar->bukti)))
        {
            return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
        }

        return response([
            'status' => 'error'
        ],500);
    }
}
