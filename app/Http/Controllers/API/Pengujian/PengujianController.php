<?php

namespace App\Http\Controllers\API\Pengujian;

/* Base App */
use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;
use App\Http\Controllers\Controller;

/* Validation */

/* Models */
use App\Models\PenerimaanBarang\PenerimaanBarang;
use App\Models\LaporanPengujian\LaporanPengujian;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\Authentication\Role;
use App\Models\Picture;
use App\Models\Files;


/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Zipper;
use Excel;
use PDF;

class PengujianController extends Controller
{
    public function verifikasi(Request $request)
    {
        $user = auth()->user();
        $layanan = [];
        $data = [];
        if($user->hasRole(['yan-kalibrasi','msb-kalibrasi','asman-dal-kalibrasi','asman-lola-kalibrasi'])){
            $layanan = [1];
        }
        if($user->hasRole(['yan-uji'])){
            $layanan = [2,3,4,5];
        }
        if($user->hasRole(['msb-siskit','asman-dal-siskit','asman-lola-siskit'])){
            $layanan = [2];
        }
        if($user->hasRole(['msb-sistgi','asman-dal-sistgi','asman-lola-sistgi'])){
            $layanan = [3];
        }
        if($user->hasRole(['msb-tegangan-rendah','asman-dal-tegangan-rendah','asman-lola-tegangan-rendah'])){
            $layanan = [4];
        }
        if($user->hasRole(['msb-tegangan-tinggi','asman-dal-tegangan-tinggi','asman-lola-tegangan-tinggi'])){
            $layanan = [5];
        }
        if($user->hasRole(['pelaksana-kalibrasi'])){
            $layanan = [1];
        }
        if($user->hasRole(['pelaksana-siskit'])){
            $layanan = [2];
        }
        if($user->hasRole(['pelaksana-sistgi'])){
            $layanan = [3];
        }
        if($user->hasRole(['pelaksana-tegangan-rendah'])){
            $layanan = [4];
        }
        if($user->hasRole(['pelaksana-tegangan-tinggi'])){
            $layanan = [5];
        }
        if(count($layanan)>0){
            $data = PenerimaanBarang::byLayananV2($layanan)
                                        ->with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.detail','konfirmasi.va.surat.kaji_ulang.detail','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.lokasi','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','konfirmasi.va.surat.kaji_ulang.pp.detail','konfirmasi','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.user','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detail_penerimaan_barang.detail_pp.jenis','detail_penerimaan_barang.detail_pp.satuan','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.satuan','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.satuan',
                                            'detail_penerimaan_barang.detail_pengujian.pelaksana','detail_penerimaan_barang.detail_pengujian.detailpelaksana.pelaksana')
                                        ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
                                          ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                          ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                         ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                         ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
                                                $pp->where('no_order','like','%'.$no_order.'%');
                                             });
                                        })
                                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
                                                $pp->where('no_surat','like','%'.$no_surat.'%');
                                             });
                                        })
                                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
                                                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                             });
                                        })
                                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
                                                $pp->where('layanan_id',$jenis_pelayanan_id);
                                             });
                                        })
                                        ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                                             $q->whereHas('konfirmasi', function($konfirmasi) use($wbs_io){
                                                $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
                                             });
                                        })
                                        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
                                                $pelanggan->where('perusahaan_id',$perusahaan_id);
                                             });
                                        })
                                        ->where('trans_penerimaan_barang.status', 1)
                                        ->where('trans_penerimaan_barang.status_pengujian', 0)
                                        ->select('trans_penerimaan_barang.*');
            if($sort = $request->sort){
              if($sort == 'order'){
                $data->orderBy('trans_pp.tgl_order','asc');
              }
              if($sort == 'surat'){
                $data->orderBy('trans_pp.tgl_surat','asc');
              }
            }else{
                $data->orderBy('trans_penerimaan_barang.created_at','desc');
            }
        }
        
        //Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     $data->orderBy('created_at', 'desc');
        // }else{
        //     return response()->json([
        //         'status' => false,
        //         'message' => 'page not found'
        //     ],404);
        // }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function verifikasiHistori(Request $request)
    {
        $user = auth()->user();
        $layanan = [];
        $data = [];
        if($user->hasRole(['yan-kalibrasi','msb-kalibrasi','asman-dal-kalibrasi','asman-lola-kalibrasi'])){
            $layanan = [1];
        }
        if($user->hasRole(['yan-uji'])){
            $layanan = [2,3,4,5];
        }
        if($user->hasRole(['msb-siskit','asman-dal-siskit','asman-lola-siskit'])){
            $layanan = [2];
        }
        if($user->hasRole(['msb-sistgi','asman-dal-sistgi','asman-lola-sistgi'])){
            $layanan = [3];
        }
        if($user->hasRole(['msb-tegangan-rendah','asman-dal-tegangan-rendah','asman-lola-tegangan-rendah'])){
            $layanan = [4];
        }
        if($user->hasRole(['msb-tegangan-tinggi','asman-dal-tegangan-tinggi','asman-lola-tegangan-tinggi'])){
            $layanan = [5];
        }
        if($user->hasRole(['pelaksana-kalibrasi'])){
            $layanan = [1];
        }
        if($user->hasRole(['pelaksana-siskit'])){
            $layanan = [2];
        }
        if($user->hasRole(['pelaksana-sistgi'])){
            $layanan = [3];
        }
        if($user->hasRole(['pelaksana-tegangan-rendah'])){
            $layanan = [4];
        }
        if($user->hasRole(['pelaksana-tegangan-tinggi'])){
            $layanan = [5];
        }
        if(count($layanan)>0){
            $data = PenerimaanBarang::byLayananV2($layanan)
                                        ->with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.detail','konfirmasi.va.surat.kaji_ulang.detail','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.lokasi','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','konfirmasi.va.surat.kaji_ulang.pp.detail','konfirmasi','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.user','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detail_penerimaan_barang.detail_pp.jenis','detail_penerimaan_barang.detail_pp.satuan','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.satuan',
                                            'detail_penerimaan_barang.detail_pengujian.pelaksana','detail_penerimaan_barang.detail_pengujian.detailpelaksana.pelaksana')
                                        ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
                                          ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                          ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                         ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                         ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                        ->whereHas('pengujian', function($u){
                                            $u->where('status', 2);
                                        })
                                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
                                                $pp->where('no_order','like','%'.$no_order.'%');
                                             });
                                        })
                                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
                                                $pp->where('no_surat','like','%'.$no_surat.'%');
                                             });
                                        })
                                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
                                                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                             });
                                        })
                                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
                                                $pp->where('layanan_id',$jenis_pelayanan_id);
                                             });
                                        })
                                        ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                                             $q->whereHas('konfirmasi', function($konfirmasi) use($wbs_io){
                                                $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
                                             });
                                        })
                                        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
                                                $pelanggan->where('perusahaan_id',$perusahaan_id);
                                             });
                                        })
                                        ->where('trans_penerimaan_barang.status', 1)
                                        ->where('trans_penerimaan_barang.status_pengujian', 1)
                                        ->select('trans_penerimaan_barang.*');
            if($sort = $request->sort){
              if($sort == 'order'){
                $data->orderBy('trans_pp.tgl_order','asc');
              }
              if($sort == 'surat'){
                $data->orderBy('trans_pp.tgl_surat','asc');
              }
            }else{
                $data->orderBy('trans_penerimaan_barang.created_at','desc');
            }
        }
        
        //Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     $data->orderBy('created_at', 'desc');
        // }else{
        //     return response()->json([
        //         'status' => false,
        //         'message' => 'page not found'
        //     ],404);
        // }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function pengujian(Request $request)
    {
        $user = auth()->user();
        $layanan = [];
        $data = [];
        if($user->hasRole(['yan-kalibrasi','msb-kalibrasi','asman-dal-kalibrasi','asman-lola-kalibrasi'])){
            $layanan = [1];
        }
        if($user->hasRole(['yan-uji'])){
            $layanan = [2,3,4,5];
        }
        if($user->hasRole(['msb-siskit','asman-dal-siskit','asman-lola-siskit'])){
            $layanan = [2];
        }
        if($user->hasRole(['msb-sistgi','asman-dal-sistgi','asman-lola-sistgi'])){
            $layanan = [3];
        }
        if($user->hasRole(['msb-tegangan-rendah','asman-dal-tegangan-rendah','asman-lola-tegangan-rendah'])){
            $layanan = [4];
        }
        if($user->hasRole(['msb-tegangan-tinggi','asman-dal-tegangan-tinggi','asman-lola-tegangan-tinggi'])){
            $layanan = [5];
        }
        if($user->hasRole(['pelaksana-kalibrasi'])){
            $layanan = [1];
        }
        if($user->hasRole(['pelaksana-siskit'])){
            $layanan = [2];
        }
        if($user->hasRole(['pelaksana-sistgi'])){
            $layanan = [3];
        }
        if($user->hasRole(['pelaksana-tegangan-rendah'])){
            $layanan = [4];
        }
        if($user->hasRole(['pelaksana-tegangan-tinggi'])){
            $layanan = [5];
        }
        if(count($layanan)>0){
            $data = PenerimaanBarang::byLayananV2($layanan)
                                        ->with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.detail','konfirmasi.va.surat.kaji_ulang.detail','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.lokasi','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','konfirmasi.va.surat.kaji_ulang.pp.detail','konfirmasi','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.user','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detail_penerimaan_barang.detail_pp.jenis','detail_penerimaan_barang.detail_pp.satuan','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.satuan',
                                            'detail_penerimaan_barang.detail_pengujian.pelaksana','detail_penerimaan_barang.detail_pengujian.detailpelaksana.pelaksana')
                                        ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
                                          ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                          ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                         ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                         ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                        ->whereHas('pengujian', function($u){
                                            $u->where('status_data_pengujian', 0);
                                        })
                                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
                                                $pp->where('no_order','like','%'.$no_order.'%');
                                             });
                                        })
                                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
                                                $pp->where('no_surat','like','%'.$no_surat.'%');
                                             });
                                        })
                                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
                                                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                             });
                                        })
                                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
                                                $pp->where('layanan_id',$jenis_pelayanan_id);
                                             });
                                        })
                                        ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                                             $q->whereHas('konfirmasi', function($konfirmasi) use($wbs_io){
                                                $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
                                             });
                                        })
                                        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
                                                $pelanggan->where('perusahaan_id',$perusahaan_id);
                                             });
                                        })
                                        ->where('trans_penerimaan_barang.status', 1)
                                        ->where('trans_penerimaan_barang.status_pengujian', 1)
                                        ->select('trans_penerimaan_barang.*');
            if($sort = $request->sort){
              if($sort == 'order'){
                $data->orderBy('trans_pp.tgl_order','asc');
              }
              if($sort == 'surat'){
                $data->orderBy('trans_pp.tgl_surat','asc');
              }
            }else{
                $data->orderBy('trans_penerimaan_barang.created_at','desc');
            }
        }
        
        //Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     $data->orderBy('created_at', 'desc');
        // }else{
        //     return response()->json([
        //         'status' => false,
        //         'message' => 'page not found'
        //     ],404);
        // }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function pengujianHistori(Request $request)
    {
        $user = auth()->user();
        $layanan = [];
        $data = [];
        if($user->hasRole(['yan-kalibrasi','msb-kalibrasi','asman-dal-kalibrasi','asman-lola-kalibrasi'])){
            $layanan = [1];
        }
        if($user->hasRole(['yan-uji'])){
            $layanan = [2,3,4,5];
        }
        if($user->hasRole(['msb-siskit','asman-dal-siskit','asman-lola-siskit'])){
            $layanan = [2];
        }
        if($user->hasRole(['msb-sistgi','asman-dal-sistgi','asman-lola-sistgi'])){
            $layanan = [3];
        }
        if($user->hasRole(['msb-tegangan-rendah','asman-dal-tegangan-rendah','asman-lola-tegangan-rendah'])){
            $layanan = [4];
        }
        if($user->hasRole(['msb-tegangan-tinggi','asman-dal-tegangan-tinggi','asman-lola-tegangan-tinggi'])){
            $layanan = [5];
        }
        if($user->hasRole(['pelaksana-kalibrasi'])){
            $layanan = [1];
        }
        if($user->hasRole(['pelaksana-siskit'])){
            $layanan = [2];
        }
        if($user->hasRole(['pelaksana-sistgi'])){
            $layanan = [3];
        }
        if($user->hasRole(['pelaksana-tegangan-rendah'])){
            $layanan = [4];
        }
        if($user->hasRole(['pelaksana-tegangan-tinggi'])){
            $layanan = [5];
        }
        if(count($layanan)>0){
            $data = PenerimaanBarang::byLayananV2($layanan)
                                        ->with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.detail','konfirmasi.va.surat.kaji_ulang.detail','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.lokasi','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','konfirmasi.va.surat.kaji_ulang.pp.detail','konfirmasi','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.user','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detail_penerimaan_barang.detail_pp.jenis','detail_penerimaan_barang.detail_pp.satuan','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.satuan',
                                            'detail_penerimaan_barang.detail_pengujian.pelaksana','detail_penerimaan_barang.detail_pengujian.detailpelaksana.pelaksana')
                                        ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
                                          ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                          ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                         ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                         ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                        ->whereHas('pengujian', function($u){
                                            $u->where('status', 2);
                                        })
                                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
                                                $pp->where('no_order','like','%'.$no_order.'%');
                                             });
                                        })
                                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
                                                $pp->where('no_surat','like','%'.$no_surat.'%');
                                             });
                                        })
                                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
                                                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                             });
                                        })
                                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
                                                $pp->where('layanan_id',$jenis_pelayanan_id);
                                             });
                                        })
                                        ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                                             $q->whereHas('konfirmasi', function($konfirmasi) use($wbs_io){
                                                $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
                                             });
                                        })
                                        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                             $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
                                                $pelanggan->where('perusahaan_id',$perusahaan_id);
                                             });
                                        })
                                        ->where('trans_penerimaan_barang.status', 1)
                                        ->where('trans_penerimaan_barang.status_pengujian', 1)
                                        ->select('trans_penerimaan_barang.*');
            if($sort = $request->sort){
              if($sort == 'order'){
                $data->orderBy('trans_pp.tgl_order','asc');
              }
              if($sort == 'surat'){
                $data->orderBy('trans_pp.tgl_surat','asc');
              }
            }else{
                $data->orderBy('trans_penerimaan_barang.created_at','desc');
            }
        }
        
        //Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     $data->orderBy('created_at', 'desc');
        // }else{
        //     return response()->json([
        //         'status' => false,
        //         'message' => 'page not found'
        //     ],404);
        // }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function qrCode(Request $request)
    {
        $user = auth()->user();
        $layanan = [];
        $data = [];
        if($user->hasRole(['yan-kalibrasi','msb-kalibrasi','asman-dal-kalibrasi','asman-lola-kalibrasi'])){
            $layanan = [1];
        }
        if($user->hasRole(['yan-uji'])){
            $layanan = [2,3,4,5];
        }
        if($user->hasRole(['msb-siskit','asman-dal-siskit','asman-lola-siskit'])){
            $layanan = [2];
        }
        if($user->hasRole(['msb-sistgi','asman-dal-sistgi','asman-lola-sistgi'])){
            $layanan = [3];
        }
        if($user->hasRole(['msb-tegangan-rendah','asman-dal-tegangan-rendah','asman-lola-tegangan-rendah'])){
            $layanan = [4];
        }
        if($user->hasRole(['msb-tegangan-tinggi','asman-dal-tegangan-tinggi','asman-lola-tegangan-tinggi'])){
            $layanan = [5];
        }
        if($user->hasRole(['pelaksana-kalibrasi'])){
            $layanan = [1];
        }
        if($user->hasRole(['pelaksana-siskit'])){
            $layanan = [2];
        }
        if($user->hasRole(['pelaksana-sistgi'])){
            $layanan = [3];
        }
        if($user->hasRole(['pelaksana-tegangan-rendah'])){
            $layanan = [4];
        }
        if($user->hasRole(['pelaksana-tegangan-tinggi'])){
            $layanan = [5];
        }
        if(count($layanan)>0){
            $data = LaporanPengujian::with('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp','pengujian.penerimaan.konfirmasi.va.surat.detail','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.detail','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','pengujian.penerimaan.konfirmasi','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','pengujian.penerimaan.detail_penerimaan_barang.detail_pp.jenis','pengujian.penerimaan.detail_penerimaan_barang.detail_pp.satuan','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.satuan',
                'pengujian.penerimaan.detail_penerimaan_barang.detail_pengujian.pelaksana','pengujian.penerimaan.detail_penerimaan_barang.detail_pengujian.detailpelaksana.pelaksana')
                                        ->join('trans_pengujian','trans_laporan_pengujian.pengujian_id','=','trans_pengujian.id')
                                        ->join('trans_penerimaan_barang','trans_pengujian.penerimaan_id','=','trans_penerimaan_barang.id')
                                        ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
                                        ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                        ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                       ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                       ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                        ->whereHas('pengujian.penerimaan', function($a) use ($layanan){
                                            $a->byLayananV2($layanan);
                                        })
                                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                                             $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
                                                $pp->where('no_order','like','%'.$no_order.'%');
                                             });
                                        })
                                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                             $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
                                                $pp->where('no_surat','like','%'.$no_surat.'%');
                                             });
                                        })
                                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                                             $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
                                                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                             });
                                        })
                                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                                             $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
                                                $pp->where('layanan_id',$jenis_pelayanan_id);
                                             });
                                        })
                                        ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                                             $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($wbs_io){
                                                $konfirmasi->where('wbs_io','like','%'.$no_wbs.'%');
                                             });
                                        })
                                        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                                             $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
                                                $pelanggan->where('perusahaan_id',$perusahaan_id);
                                             });
                                        })
                                        ->select('trans_laporan_pengujian.*');
            if($sort = $request->sort){
                if($sort == 'order'){
                  $data->orderBy('trans_pp.tgl_order','asc');
                }
                if($sort == 'surat'){
                  $data->orderBy('trans_pp.tgl_surat','asc');
                }
            }else{
              $data->orderBy('trans_laporan_pengujian.created_at','desc');
            }
        }
        
        //Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     $data->orderBy('created_at', 'desc');
        // }else{
        //     return response()->json([
        //         'status' => false,
        //         'message' => 'page not found'
        //     ],404);
        // }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function downloadBarcode(Request $request, $id){
        $record = LaporanPengujian::find($id);
        $data = [
            'records' => $record,
        ];
        $pdf = PDF::loadView('modules.pengujian.kal.cetak.cetak-barcode', $data);
        return $pdf->stream('Surat Penerimaan Barang '.$record->tanggal.'.pdf',array("Attachment" => false));
    }

    public function download($id)
    {
        $daftar = PendaftaranPengujian::find($id);
        if(file_exists(public_path('storage/'.$daftar->bukti)))
        {
            return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
        }
        return response([
            'status' => 'error'
        ],500);
    }
}
