<?php

namespace App\Http\Controllers\API\Pengujian;

/* Base App */
use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;
use App\Http\Controllers\Controller;

/* Validation */

/* Models */
use App\Models\PenerimaanBarang\PenerimaanBarang;
use App\Models\LaporanPengujian\LaporanPengujian;
use App\Models\Authentication\Role;
use App\Models\Picture;
use App\Models\Files;


/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Zipper;
use Excel;

class KalibrasiController extends Controller
{
    public function verifikasi(Request $request)
    {
        $data = PenerimaanBarang::byLayanan(1)
                                    ->with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.detail','konfirmasi.va.surat.kaji_ulang.detail','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.lokasi','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','konfirmasi.va.surat.kaji_ulang.pp.detail','konfirmasi','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.user','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detail_penerimaan_barang.detail_pp.jenis','detail_penerimaan_barang.detail_pp.satuan','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.satuan','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.satuan')
                                    ->where('status', 1)
                                    ->where('status_pengujian', 0)
                                    ->select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $data->orderBy('created_at', 'desc');
        }else{
            $data =[];
        }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function verifikasiHistori(Request $request)
    {
        $data = PenerimaanBarang::byLayanan(1)
                                    ->with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.detail','konfirmasi.va.surat.kaji_ulang.detail','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.lokasi','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','konfirmasi.va.surat.kaji_ulang.pp.detail','konfirmasi','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.user','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detail_penerimaan_barang.detail_pp.jenis','detail_penerimaan_barang.detail_pp.satuan','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.satuan')
                                    ->whereHas('pengujian', function($u){
                                        $u->where('status', 2);
                                    })
                                    ->where('status', 1)
                                    ->where('status_pengujian', 1)
                                    ->select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $data->orderBy('created_at', 'desc');
        }else{
            $data =[];
        }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function pengujian(Request $request)
    {
        $data = PenerimaanBarang::byLayanan(1)
                                    ->with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.detail','konfirmasi.va.surat.kaji_ulang.detail','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.lokasi','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','konfirmasi.va.surat.kaji_ulang.pp.detail','konfirmasi','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.user','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detail_penerimaan_barang.detail_pp.jenis','detail_penerimaan_barang.detail_pp.satuan','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.satuan')
                                    ->whereHas('pengujian', function($u){
                                        $u->where('status_data_pengujian', 0);
                                    })
                                    ->where('status', 1)
                                    ->where('status_pengujian', 1)
                                    ->select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $data->orderBy('created_at', 'desc');
        }else{
            $data =[];
        }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function pengujianHistori(Request $request)
    {
        $data = PenerimaanBarang::byLayanan(1)
                                    ->with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.detail','konfirmasi.va.surat.kaji_ulang.detail','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.lokasi','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','konfirmasi.va.surat.kaji_ulang.pp.detail','konfirmasi','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.user','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detail_penerimaan_barang.detail_pp.jenis','detail_penerimaan_barang.detail_pp.satuan','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.satuan')
                                    ->whereHas('pengujian', function($u){
                                        $u->where('status', 2);
                                    })
                                    ->where('status', 1)
                                    ->where('status_pengujian', 1)
                                    ->select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $data->orderBy('created_at', 'desc');
        }else{
            $data =[];
        }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function qrCode(Request $request)
    {
        $data = LaporanPengujian::with('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp','pengujian.penerimaan.konfirmasi.va.surat.detail','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.detail','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','pengujian.penerimaan.konfirmasi','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','pengujian.penerimaan.detail_penerimaan_barang.detail_pp.jenis','pengujian.penerimaan.detail_penerimaan_barang.detail_pp.satuan','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.satuan')
                                    ->whereHas('pengujian.penerimaan', function($a){
                                        $a->byLayanan(1);
                                    })
                                    ->select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $data->orderBy('created_at', 'desc');
        }else{
            $data =[];
        }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }
}
