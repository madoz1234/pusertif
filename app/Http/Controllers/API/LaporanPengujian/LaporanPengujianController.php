<?php

namespace App\Http\Controllers\API\LaporanPengujian;

/* Base App */
use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;
use App\Http\Controllers\Controller;

/* Validation */

/* Models */
use App\Models\LaporanPengujian\LaporanPengujian;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\Authentication\Role;
use App\Models\Picture;
use App\Models\Files;


/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Zipper;
use Excel;

class LaporanPengujianController extends Controller
{
    public function grid(Request $request)
    {
      $user = auth()->user();
      if($user->hasRole(['srm-prosmkal','yan-kalibrasi', 'msb-kalibrasi', 'adminlab-kalibrasi', 'asman-dal-kalibrasi', 'asman-lola-kalibrasi', 'pelaksana-kalibrasi'])){
          $data = LaporanPengujian::with('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detaillaporan.detail_pengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.penerimaan.konfirmasi.va.surat.detail','detaillaporan.detail_pengujian.pelaksana','detaillaporan.detail_pengujian.detailpelaksana.pelaksanalaporan')
        ->join('trans_pengujian','trans_laporan_pengujian.pengujian_id','=','trans_pengujian.id')
        ->join('trans_penerimaan_barang','trans_pengujian.penerimaan_id','=','trans_penerimaan_barang.id')
        ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
        ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
        ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
        ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
        ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
        ->whereHas('pengujian', function($u){
          $u->where('tipe', 1);
        })
        ->when($no_order = $request->no_order, function($q) use ($no_order) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
              $pp->where('no_order','like','%'.$no_order.'%');
           });
        })
        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
              $pp->where('no_surat','like','%'.$no_surat.'%');
           });
        })
        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
              $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
           });
        })
        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
              $pp->where('layanan_id',$jenis_pelayanan_id);
           });
        })
        ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
           $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($wbs_io){
              $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
           });
        })
        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
              $pelanggan->where('perusahaan_id',$perusahaan_id);
           });
        })
        ->select('trans_laporan_pengujian.*');
      }elseif($user->hasRole(['msb-siskit', 'adminlab-siskit', 'asman-dal-siskit', 'asman-lola-siskit', 'pelaksana-siskit'])){
        $data = LaporanPengujian::with('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detaillaporan.detail_pengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.penerimaan.konfirmasi.va.surat.detail','detaillaporan.detail_pengujian.pelaksana','detaillaporan.detail_pengujian.detailpelaksana.pelaksanalaporan')
        ->join('trans_pengujian','trans_laporan_pengujian.pengujian_id','=','trans_pengujian.id')
        ->join('trans_penerimaan_barang','trans_pengujian.penerimaan_id','=','trans_penerimaan_barang.id')
        ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
        ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
        ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
        ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
        ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
        ->whereHas('pengujian', function($u){
          $u->where('tipe', 2);
        })
        ->when($no_order = $request->no_order, function($q) use ($no_order) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
              $pp->where('no_order','like','%'.$no_order.'%');
           });
        })
        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
              $pp->where('no_surat','like','%'.$no_surat.'%');
           });
        })
        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
              $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
           });
        })
        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
              $pp->where('layanan_id',$jenis_pelayanan_id);
           });
        })
        ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
           $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($wbs_io){
              $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
           });
        })
        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
              $pelanggan->where('perusahaan_id',$perusahaan_id);
           });
        })
        ->select('trans_laporan_pengujian.*');
      }elseif($user->hasRole(['msb-sistgi', 'adminlab-sistgi', 'asman-dal-sistgi', 'asman-lola-sistgi', 'pelaksana-sistgi'])){
        $data = LaporanPengujian::with('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detaillaporan.detail_pengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.penerimaan.konfirmasi.va.surat.detail','detaillaporan.detail_pengujian.pelaksana','detaillaporan.detail_pengujian.detailpelaksana.pelaksanalaporan')
        ->join('trans_pengujian','trans_laporan_pengujian.pengujian_id','=','trans_pengujian.id')
        ->join('trans_penerimaan_barang','trans_pengujian.penerimaan_id','=','trans_penerimaan_barang.id')
        ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
        ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
        ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
        ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
        ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
        ->whereHas('pengujian', function($u){
          $u->where('tipe', 3);
        })
        ->when($no_order = $request->no_order, function($q) use ($no_order) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
              $pp->where('no_order','like','%'.$no_order.'%');
           });
        })
        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
              $pp->where('no_surat','like','%'.$no_surat.'%');
           });
        })
        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
              $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
           });
        })
        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
              $pp->where('layanan_id',$jenis_pelayanan_id);
           });
        })
        ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
           $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($wbs_io){
              $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
           });
        })
        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
              $pelanggan->where('perusahaan_id',$perusahaan_id);
           });
        })
        ->select('trans_laporan_pengujian.*');
      }elseif($user->hasRole(['msb-tegangan-rendah', 'adminlab-tegangan-rendah', 'asman-dal-tegangan-rendah', 'asman-lola-tegangan-rendah', 'pelaksana-tegangan-rendah'])){
        $data = LaporanPengujian::with('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detaillaporan.detail_pengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.penerimaan.konfirmasi.va.surat.detail','detaillaporan.detail_pengujian.pelaksana','detaillaporan.detail_pengujian.detailpelaksana.pelaksanalaporan')
        ->join('trans_pengujian','trans_laporan_pengujian.pengujian_id','=','trans_pengujian.id')
        ->join('trans_penerimaan_barang','trans_pengujian.penerimaan_id','=','trans_penerimaan_barang.id')
        ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
        ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
        ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
        ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
        ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
        ->whereHas('pengujian', function($u){
          $u->where('tipe', 4);
        })
        ->when($no_order = $request->no_order, function($q) use ($no_order) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
              $pp->where('no_order','like','%'.$no_order.'%');
           });
        })
        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
              $pp->where('no_surat','like','%'.$no_surat.'%');
           });
        })
        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
              $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
           });
        })
        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
              $pp->where('layanan_id',$jenis_pelayanan_id);
           });
        })
        ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
           $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($wbs_io){
              $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
           });
        })
        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
              $pelanggan->where('perusahaan_id',$perusahaan_id);
           });
        })
        ->select('trans_laporan_pengujian.*');
      }elseif($user->hasRole(['msb-tegangan-tinggi', 'adminlab-tegangan-tinggi', 'asman-dal-tegangan-tinggi', 'asman-lola-tegangan-tinggi', 'pelaksana-tegangan-tinggi'])){
        $data = LaporanPengujian::with('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detaillaporan.detail_pengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.penerimaan.konfirmasi.va.surat.detail','detaillaporan.detail_pengujian.pelaksana','detaillaporan.detail_pengujian.detailpelaksana.pelaksanalaporan')
        ->join('trans_pengujian','trans_laporan_pengujian.pengujian_id','=','trans_pengujian.id')
        ->join('trans_penerimaan_barang','trans_pengujian.penerimaan_id','=','trans_penerimaan_barang.id')
        ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
        ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
        ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
        ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
        ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
        ->whereHas('pengujian', function($u){
          $u->where('tipe', 5);
        })
        ->when($no_order = $request->no_order, function($q) use ($no_order) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
              $pp->where('no_order','like','%'.$no_order.'%');
           });
        })
        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
              $pp->where('no_surat','like','%'.$no_surat.'%');
           });
        })
        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
              $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
           });
        })
        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
              $pp->where('layanan_id',$jenis_pelayanan_id);
           });
        })
        ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
           $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($wbs_io){
              $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
           });
        })
        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
              $pelanggan->where('perusahaan_id',$perusahaan_id);
           });
        })
        ->select('trans_laporan_pengujian.*');
      }elseif($user->hasRole(['yan-uji', 'srm-uji'])){
        $data = LaporanPengujian::with('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detaillaporan.detail_pengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.penerimaan.konfirmasi.va.surat.detail','detaillaporan.detail_pengujian.pelaksana','detaillaporan.detail_pengujian.detailpelaksana.pelaksanalaporan')
        ->join('trans_pengujian','trans_laporan_pengujian.pengujian_id','=','trans_pengujian.id')
        ->join('trans_penerimaan_barang','trans_pengujian.penerimaan_id','=','trans_penerimaan_barang.id')
        ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
        ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
        ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
        ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
        ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
        ->whereHas('pengujian', function($u){
          $u->whereIn('tipe', [2,3,4,5]);
        })
        ->when($no_order = $request->no_order, function($q) use ($no_order) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
              $pp->where('no_order','like','%'.$no_order.'%');
           });
        })
        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
              $pp->where('no_surat','like','%'.$no_surat.'%');
           });
        })
        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
              $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
           });
        })
        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
              $pp->where('layanan_id',$jenis_pelayanan_id);
           });
        })
        ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
           $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($wbs_io){
              $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
           });
        })
        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
              $pelanggan->where('perusahaan_id',$perusahaan_id);
           });
        })
        ->select('trans_laporan_pengujian.*');
      }elseif($user->hasRole(['gm','admin'])){
        $data = LaporanPengujian::with('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detaillaporan.detail_pengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.penerimaan.konfirmasi.va.surat.detail','detaillaporan.detail_pengujian.pelaksana','detaillaporan.detail_pengujian.detailpelaksana.pelaksanalaporan')
        ->join('trans_pengujian','trans_laporan_pengujian.pengujian_id','=','trans_pengujian.id')
        ->join('trans_penerimaan_barang','trans_pengujian.penerimaan_id','=','trans_penerimaan_barang.id')
        ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
        ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
        ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
        ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
        ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
        ->when($no_order = $request->no_order, function($q) use ($no_order) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_order){
              $pp->where('no_order','like','%'.$no_order.'%');
           });
        })
        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($no_surat){
              $pp->where('no_surat','like','%'.$no_surat.'%');
           });
        })
        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($tanggal_order){
              $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
           });
        })
        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use($jenis_pelayanan_id){
              $pp->where('layanan_id',$jenis_pelayanan_id);
           });
        })
        ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
           $q->whereHas('pengujian.penerimaan.konfirmasi', function($konfirmasi) use($wbs_io){
              $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
           });
        })
        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
           $q->whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
              $pelanggan->where('perusahaan_id',$perusahaan_id);
           });
        })
        ->select('trans_laporan_pengujian.*');
      }else{
        return response()->json([
            'status' => false,
            'message' => 'page not found'
        ],404);
      }
        
        if($sort = $request->sort){
            if($sort == 'order'){
              $data->orderBy('trans_pp.tgl_order','asc');
            }
            if($sort == 'surat'){
              $data->orderBy('trans_pp.tgl_surat','asc');
            }
        }else{
          $data->orderBy('trans_laporan_pengujian.created_at','desc');
        }
            
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function download($id)
    {
        $daftar = PendaftaranPengujian::find($id);
        if(file_exists(public_path('storage/'.$daftar->bukti)))
        {
            return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
        }

        return response([
            'status' => 'error'
        ],500);
    }
}
