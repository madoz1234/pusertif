<?php

namespace App\Http\Controllers\API\Surat;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Unlu\Laravel\Api\QueryBuilder;
/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\SuratPenawaran\SuratPenawaranRequest;
use App\Http\Requests\SuratPenawaran\AdendumRequest;
use App\Http\Requests\SuratPenawaran\SuratPenawaranWBSRequest;
/* Models */
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\SuratPenawaran\SuratPenawaranDetail;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\Act\ActDisposisi;
use App\Models\KajiUlang\KajiUlang;
use App\Models\KajiUlang\Detail;
use App\Models\Authentication\Role;
use App\Models\Users;
use App\Models\Picture;
use App\Models\Files;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Zipper; 

class SuratPenawaranController extends Controller
{
      public function index(Request $request)
      {
            $data = new QueryBuilder(new PendaftaranPengujian, $request);
            if($page=$request->page){
                  return $data->build()->paginate();
            }

            return response()->json([
                  'status' => true,
                  'data' => $data->build()->get()
            ]);
      }
      public function done(Request $request)
      {
            $user = auth()->user();
            if($user->hasRole(['yan-uji'])){
                $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','kaji_ulang.detail.mata_uji','kaji_ulang.detail.lokasi','kaji_ulang.surat','files')
                        ->whereHas('kaji_ulang', function($u){
                            $u->doesntHave('surat')->where('keputusan', 1);
                              })
                        ->where('layanan_id', '!=', 1)
                        ->where('tipe_surat', 0)
                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                             $q->where('no_order', 'like','%'.$no_order.'%');
                        })
                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                             $q->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                        })
                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                             $q->where('no_surat', 'like','%'.$no_surat.'%');
                        })
                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                             $q->where('layanan_id',$jenis_pelayanan_id);
                        })
                        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                            $q->whereHas('user', function($user) use ($perusahaan_id){
                                $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                      $pelanggan->where('perusahaan_id',$perusahaan_id);
                                });
                            });
                        })
                        ->select('*');
                if($sort = $request->sort){
                    if($sort == 'order'){
                      $data->orderBy('tgl_order','asc')->get()->groupBy(['nomor']);
                    }
                    if($sort == 'surat'){
                      $data->orderBy('tgl_surat','asc')->get()->groupBy(['nomor']);
                    }
                }else{
                      $data->orderBy('tgl_surat','desc')->get()->groupBy(['nomor']);
                }
            }elseif($user->hasRole(['yan-kalibrasi'])){
              $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','kaji_ulang.detail.mata_uji','kaji_ulang.detail.lokasi','kaji_ulang.surat','files')
                        ->whereHas('kaji_ulang', function($u){
                            $u->doesntHave('surat')->where('keputusan', 1);
                              })
                        ->where('layanan_id', 1)
                        ->where('tipe_surat', 0)
                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                             $q->where('no_order', 'like','%'.$no_order.'%');
                        })
                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                             $q->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                        })
                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                             $q->where('no_surat', 'like','%'.$no_surat.'%');
                        })
                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                             $q->where('layanan_id',$jenis_pelayanan_id);
                        })
                        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                            $q->whereHas('user', function($user) use ($perusahaan_id){
                                $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                      $pelanggan->where('perusahaan_id',$perusahaan_id);
                                });
                            });
                        })
                        ->select('*');
                if($sort = $request->sort){
                    if($sort == 'order'){
                      $data->orderBy('tgl_order','asc')->get()->groupBy(['nomor']);
                    }
                    if($sort == 'surat'){
                      $data->orderBy('tgl_surat','asc')->get()->groupBy(['nomor']);
                    }
                }else{
                      $data->orderBy('tgl_surat','desc')->get()->groupBy(['nomor']);
                }
            }
            else{
              return response()->json([
                  'status' => false,
                  'message' => 'page not found'
              ],404);
            }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

            return response()->json($page);
      }
      public function kirim(Request $request)
      {
            $user = auth()->user();
            if($user->hasRole(['yan-uji'])){
                $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','kaji_ulang.detail.mata_uji','kaji_ulang.detail.lokasi','kaji_ulang.surat.detail','kaji_ulang.surat.va','files')
                        ->whereHas('kaji_ulang', function($u){
                          $u->whereHas('surat', function($z){
                            $z->where('surat_status', 0);
                          })->where('keputusan', 1);
                            })
                        ->where('layanan_id', '!=', 1)
                        ->where('tipe_surat', 0)
                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                             $q->where('no_order', 'like','%'.$no_order.'%');
                        })
                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                             $q->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                        })
                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                             $q->where('no_surat', 'like','%'.$no_surat.'%');
                        })
                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                             $q->where('layanan_id',$jenis_pelayanan_id);
                        })
                        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                            $q->whereHas('user', function($user) use ($perusahaan_id){
                                $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                      $pelanggan->where('perusahaan_id',$perusahaan_id);
                                });
                            });
                        })
                        ->select('*');
                 if($sort = $request->sort){
                      if($sort == 'order'){
                        $data->orderBy('tgl_order','asc')->get()->groupBy(['nomor']);
                      }
                      if($sort == 'surat'){
                        $data->orderBy('tgl_surat','asc')->get()->groupBy(['nomor']);
                      }
                  }else{
                        $data->orderBy('tgl_surat','desc')->get()->groupBy(['nomor']);
                  }
            }elseif($user->hasRole(['yan-kalibrasi'])){
              $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','kaji_ulang.detail.mata_uji','kaji_ulang.detail.lokasi','kaji_ulang.surat.detail','kaji_ulang.surat.va','files')
                        ->whereHas('kaji_ulang', function($u){
                          $u->whereHas('surat', function($z){
                            $z->where('surat_status', 0);
                          })->where('keputusan', 1);
                            })
                        ->where('layanan_id', 1)
                        ->where('tipe_surat', 0)
                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                             $q->where('no_order', 'like','%'.$no_order.'%');
                        })
                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                             $q->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                        })
                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                             $q->where('no_surat', 'like','%'.$no_surat.'%');
                        })
                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                             $q->where('layanan_id',$jenis_pelayanan_id);
                        })
                        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                            $q->whereHas('user', function($user) use ($perusahaan_id){
                                $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                      $pelanggan->where('perusahaan_id',$perusahaan_id);
                                });
                            });
                        })
                        ->select('*');
                if($sort = $request->sort){
                    if($sort == 'order'){
                      $data->orderBy('tgl_order','asc')->get()->groupBy(['nomor']);
                    }
                    if($sort == 'surat'){
                      $data->orderBy('tgl_surat','asc')->get()->groupBy(['nomor']);
                    }
                }else{
                      $data->orderBy('tgl_surat','desc')->get()->groupBy(['nomor']);
                }
            }
            else{
              return response()->json([
                  'status' => false,
                  'message' => 'page not found'
              ],404);
            }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

            return response()->json($page);
      }

      public function histori(Request $request)
      {
            $user = auth()->user();
            if($user->hasRole(['yan-uji'])){
                $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','kaji_ulang.detail.mata_uji','kaji_ulang.detail.lokasi','kaji_ulang.surat.detail','kaji_ulang.surat.va','files')
                        ->whereHas('kaji_ulang', function($u){
                            $u->whereHas('surat', function($z){
                              $z->where('surat_status', 1);
                          })->where('keputusan', 1);
                        })
                        ->where('layanan_id', '!=', 1)
                        ->where('tipe_surat', 0)
                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                             $q->where('no_order', 'like','%'.$no_order.'%');
                        })
                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                             $q->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                        })
                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                             $q->where('no_surat', 'like','%'.$no_surat.'%');
                        })
                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                             $q->where('layanan_id',$jenis_pelayanan_id);
                        })
                        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                            $q->whereHas('user', function($user) use ($perusahaan_id){
                                $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                      $pelanggan->where('perusahaan_id',$perusahaan_id);
                                });
                            });
                        })
                        ->select('*');
                if($sort = $request->sort){
                    if($sort == 'order'){
                      $data->orderBy('tgl_order','asc')->get()->groupBy(['nomor']);
                    }
                    if($sort == 'surat'){
                      $data->orderBy('tgl_surat','asc')->get()->groupBy(['nomor']);
                    }
                }else{
                      $data->orderBy('tgl_surat','desc')->get()->groupBy(['nomor']);
                }
            }elseif($user->hasRole(['yan-kalibrasi'])){
              $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','kaji_ulang.detail.mata_uji','kaji_ulang.detail.lokasi','kaji_ulang.surat.detail','kaji_ulang.surat.va','files')
                        ->whereHas('kaji_ulang', function($u){
                            $u->whereHas('surat', function($z){
                              $z->where('surat_status', 1);
                          })->where('keputusan', 1);
                        })
                        ->where('layanan_id', 1)
                        ->where('tipe_surat', 0)
                        ->when($no_order = $request->no_order, function($q) use ($no_order) {
                             $q->where('no_order', 'like','%'.$no_order.'%');
                        })
                        ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                             $q->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                        })
                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                             $q->where('no_surat', 'like','%'.$no_surat.'%');
                        })
                        ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                             $q->where('layanan_id',$jenis_pelayanan_id);
                        })
                        ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                            $q->whereHas('user', function($user) use ($perusahaan_id){
                                $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                      $pelanggan->where('perusahaan_id',$perusahaan_id);
                                });
                            });
                        })
                        ->select('*');
                if($sort = $request->sort){
                    if($sort == 'order'){
                      $data->orderBy('tgl_order','asc')->get()->groupBy(['nomor']);
                    }
                    if($sort == 'surat'){
                      $data->orderBy('tgl_surat','asc')->get()->groupBy(['nomor']);
                    }
                }else{
                      $data->orderBy('tgl_surat','desc')->get()->groupBy(['nomor']);
                }
            }
            else{
              return response()->json([
                  'status' => false,
                  'message' => 'page not found'
              ],404);
            }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

            return response()->json($page);
      }
      public function tolak(Request $request)
      {
       $user = auth()->user();
       if($user->hasRole(['yan-uji'])){
        $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','kaji_ulang.detail.mata_uji','kaji_ulang.detail.lokasi','kaji_ulang.surat.detail','kaji_ulang.surat.va','files')
                ->whereHas('kaji_ulang', function($u){
                  $u->where('keputusan', 2);
                    })
                ->where('layanan_id', '!=', 1)
                ->where('tipe_surat', 0)
                ->when($no_order = $request->no_order, function($q) use ($no_order) {
                     $q->where('no_order', 'like','%'.$no_order.'%');
                })
                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                     $q->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                })
                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                     $q->where('no_surat', 'like','%'.$no_surat.'%');
                })
                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                     $q->where('layanan_id',$jenis_pelayanan_id);
                })
                ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                    $q->whereHas('user', function($user) use ($perusahaan_id){
                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                              $pelanggan->where('perusahaan_id',$perusahaan_id);
                        });
                    });
                })
                ->select('*');
              if($sort = $request->sort){
                  if($sort == 'order'){
                    $data->orderBy('tgl_order','asc')->get()->groupBy(['nomor']);
                  }
                  if($sort == 'surat'){
                    $data->orderBy('tgl_surat','asc')->get()->groupBy(['nomor']);
                  }
              }else{
                    $data->orderBy('tgl_surat','desc')->get()->groupBy(['nomor']);
              }
      }elseif($user->hasRole(['yan-kalibrasi'])){
        $data = PendaftaranPengujian::with('detail.jenis.lingkup.pelayanan.pengujian','user.pelanggans.perusahaan','kaji_ulang.detail.mata_uji','kaji_ulang.detail.lokasi','kaji_ulang.surat.detail','kaji_ulang.surat.va','files')
                ->whereHas('kaji_ulang', function($u){
                  $u->where('keputusan', 2);
                    })
                ->where('layanan_id', 1)
                ->where('tipe_surat', 0)
                ->when($no_order = $request->no_order, function($q) use ($no_order) {
                     $q->where('no_order', 'like','%'.$no_order.'%');
                })
                ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                     $q->where('tgl_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                })
                ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                     $q->where('no_surat', 'like','%'.$no_surat.'%');
                })
                ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                     $q->where('layanan_id',$jenis_pelayanan_id);
                })
                ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                    $q->whereHas('user', function($user) use ($perusahaan_id){
                        $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                              $pelanggan->where('perusahaan_id',$perusahaan_id);
                        });
                    });
                })
                ->select('*');
          if($sort = $request->sort){
              if($sort == 'order'){
                $data->orderBy('tgl_order','asc')->get()->groupBy(['nomor']);
              }
              if($sort == 'surat'){
                $data->orderBy('tgl_surat','asc')->get()->groupBy(['nomor']);
              }
          }else{
                $data->orderBy('tgl_surat','desc')->get()->groupBy(['nomor']);
          }
      }
      else{
        return response()->json([
              'status' => false,
              'message' => 'page not found'
          ],404);
      }
      $page = $data->paginate(10);
            // dd($page[0]);  
      if ($page[0] == null) {
        return response()->json([
          'status' => false,
          'message' => 'page not found'
        ],404);
      }

      return response()->json($page); 
      }

      public function download($id)
      {
        $daftar = PendaftaranPengujian::find($id);
        if(file_exists(public_path('storage/'.$daftar->bukti)))
        {
          return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
        }

        return response([
          'sattus' => 'error'
        ],500);
      }
}
