<?php

namespace App\Http\Controllers\API\HistoryData;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Unlu\Laravel\Api\QueryBuilder;
use App\Models\Authentication\User;
use App\Models\Master\JenisPelayanan;


/* Validation */


/* Models */
use App\Models\FrontEnd\PendaftaranPengujian;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Zipper;
use Excel;


class HistoryDataController extends Controller
{
  public function grid(Request $request){
    $historikal = [];
    if($request->jenis_pelayanan_id){
      $records = PendaftaranPengujian::where('layanan_id', $request->jenis_pelayanan_id)->get();
      $historikal = $this->historyData($records);
    }else{
      return response()->json([
          'status' => false,
          'message' => 'page not found'
      ],404);
    }

    return response([
      'data' => $historikal
    ]);
  }

  public function historyData($data){
    $histori = [];

    foreach ($data as $cek) {
      if(isset($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian)){
        if($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->detailpengujian->count() > 0){
          if($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->detailpengujian->first()->detailpelaksana){
              $pengujian = [];

              foreach($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->detail_penerimaan_barang->where('flag', 0)->groupBy('group_id') as $key => $row){
                foreach($row as $kuy => $data){
                  $hasil_verifikasi = '-';
                  if($data->detail_pengujian->verifikasi == 1){
                    $hasil_verifikasi = 'DITERIMA';
                  }elseif($data->detail_pengujian->verifikasi == 1){
                    $hasil_verifikasi = 'DITOLAK';
                  }

                  $tgl_verifikasi_barang_uji = '-';
                  if($data->detail_pengujian){
                    if($data->detail_pengujian->tgl_selesai){
                      $tgl_verifikasi_barang_uji =  DateToStringYear(Carbon\Carbon::parse($data->detail_pengujian->tgl_selesai)->format('Y-m-d'));
                    }
                  }

                  $pelaksana_pengujian = '-';
                  if($data->detail_pengujian->detailpelaksana){
                    $pelaksana_pengujian = $data->detail_pengujian->detailpelaksana->pelaksana->nama;
                  }

                  $tgl_selesai_pengujian = '-';
                  if($data->detail_pengujian->detailpelaksana){
                    if($data->detail_pengujian->detailpelaksana->tgl_selesai){
                      $tgl_selesai_pengujian = DateToStringYear(Carbon\Carbon::parse($data->detail_pengujian->detailpelaksana->tgl_selesai)->format('Y-m-d'));
                    }
                  }

                  $hasil_pengujian = '-';
                  if($data->detail_pengujian->detailpelaksana){
                    if($data->detail_pengujian->detailpelaksana->status_hasil_pengujian == 1){
                      $hasil_pengujian = 'BERHASIL';
                    }elseif($data->detail_pengujian->detailpelaksana->status_hasil_pengujian == 2){
                      $hasil_pengujian = 'GAGAL';
                    }else{
                      $hasil_pengujian = 'Menunggu Hasil Pengujian';
                    }
                  }

                  $detil_pelaksana_member = [];
                  foreach($data->detail_pengujian->detailpelaksana->detailmember as $member){
                    $detil_pelaksana_member[] = [
                      'nama' => $member->pelaksana->nama
                    ];
                  }

                  $pembuat_laporan = '-';
                  if($data->detail_pengujian->detailpelaksana){
                    if($data->detail_pengujian->detailpelaksana->status == 0){
                      $pembuat_laporan = 'Menunggu Verifikasi Hasil Pengujian';
                    }else{
                      if($data->detail_pengujian->detailpelaksana->pelaksana_laporan_id !== 1){
                        $pembuat_laporan =  $data->detail_pengujian->detailpelaksana->pelaksanalaporan->nama;
                      }else{
                        $pembuat_laporan = 'Menunggu Verifikasi Asman';
                      }
                    }
                  }

                  $barang_uji_kembali = '-';
                  if($data->detail_pengujian->detailpelaksana){
                    if($data->detail_pengujian->detailpelaksana->status_barang == 0){
                      $barang_uji_kembali = 'Barang tidak dikembalikan';
                    }else{
                      $barang_uji_kembali = 'Barang dikembalikan';
                    }
                  }

                  $pengujian[] = [
                    'jenis_pengujian'             => $data->detail_pp->jenis->nama, 
                    'nama_barang'                 => $data->nama_barang. '['.$data->urutan.'/'.$data->max.']',
                    'merk'                        => $data->detail_pp->merk,
                    'no_seri'                     => $data->no_seri,
                    'tgl_penerimaan_barang_uji'   => DateToStringYear($data->tanggal_terima),
                    'pelaksana_verifikasi'        => $data->detail_pengujian->pelaksana->nama,
                    'hasil_verifikasi'            => $hasil_verifikasi,
                    'tgl_verifikasi_barang_uji'   => $tgl_verifikasi_barang_uji,
                    'catatan'                     => $data->catatan,
                    'no_fpp_fpk'                  => $data->detail_pengujian->fpp_fpk,
                    'pelaksana_pengujian'         => $pelaksana_pengujian,
                    'tgl_selesai_pengujian'       => $tgl_selesai_pengujian,
                    'hasil_pengujian'             => $hasil_pengujian,
                    'detil_pelaksana_member'      => $detil_pelaksana_member,
                    'pembuat_laporan'             => $pembuat_laporan,
                    'barang_uji_kembali'          => $barang_uji_kembali,
                  ];
                }
              }

              $histori[] = [
                'no_order'              => $cek->no_order,
                'peminta_jasa'          => $cek->user->pelanggans->perusahaan->nama,
                'tgl_order'             => DateToStringYear($cek->tgl_order),
                'no_surat_permintaan'   => $cek->no_surat,
                'wbs_io'                => $cek->kaji_ulang->surat->va->konfirmasi->wbs_io,
                'no_surat_penawaran'    => $cek->kaji_ulang->surat->no_surat,
                'layanan_lingkup'       => $cek->pelayanan->nama.' - '.$cek->lingkup->nama,
                'pengujian'             => $pengujian
              ];
          }
        }
      }
    }
    return $histori;
  }

  public function print($id, $tipe){
      if($tipe == 1){
        $records = PendaftaranPengujian::where('layanan_id', $id)->get();
        $pelayanan = JenisPelayanan::find($id);
          $reports =  Excel::load(public_path('template-xls/template-historis.xlsx'), function($reader) use ($records){
              $reader->sheet('Sheet', function ($sheet) use ($records){
                  $awal = 7;
                  $no =1;
                  $i=1;
          $angka =0;
          $jumlah=0;
          $aa=0;
          $status ='-';
          $tglawal ='-';
          $tglakhir ='-';
          $pelaksana ='-';
          $hasil ='-';
          $pelaksanalaporan = '-';
          $status_barang = '-';
          $data_pembanding=0;
          $row_awal =7;
          $row_akhir=0;
          $a=0;$b=0;$c=0;$d=0;$e=0;$f=0;$g=0;$h=0;$i=0;$j=0;$k=0;$l=0;$m=0;$n=0;
          $aa=0;$bb=0;$cc=0;$dd=0;$ee=0;$ff=0;$gg=0;$hh=0;$ii=0;$jj=0;$kk=0;$ll=0;$mm=0;$nn=0;$oo=0;$pp=0;$qq=0;$rr=0;$ss=0;$tt=0;$uu=0;$vv=0;$ww=0;$xx=0;$yy=0;$zz=0;
          $merge = 0;
          $row_kecil=7;
                  foreach($records as $cek){
                    if($cek->kaji_ulang){
              if($cek->kaji_ulang->surat){
                if($cek->kaji_ulang->surat->va){
                  if($cek->kaji_ulang->surat->va->konfirmasi){
                    if($cek->kaji_ulang->surat->va->konfirmasi->penerimaan){
                      if($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian){
                        if($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->detailpengujian->count() > 0){
                          if($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->detailpengujian->first()->detailpelaksana){
                            foreach($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->detail_penerimaan_barang->where('flag', 0)->groupBy('group_id') as $pembanding){
                              $data_pembanding += $pembanding->count();
                            }
                            $u =0;
                            $sheet->setMergeColumn(array(
                              'columns' => array('A','B','C','D','E','F','G'),
                              'rows' => array(
                                array($row_awal,($row_awal + ($data_pembanding-1))),
                                array($row_awal,($row_awal + ($data_pembanding-1))),
                                array($row_awal,($row_awal + ($data_pembanding-1))),
                                array($row_awal,($row_awal + ($data_pembanding-1))),
                                array($row_awal,($row_awal + ($data_pembanding-1))),
                                array($row_awal,($row_awal + ($data_pembanding-1))),
                                array($row_awal,($row_awal + ($data_pembanding-1))),
                              )
                            ));
                            $a = "A".$row_awal;
                            $b = "A".($row_awal + ($data_pembanding-1));
                            $c = "B".$row_awal;
                            $d = "B".($row_awal + ($data_pembanding-1));
                            $e = "C".$row_awal;
                            $f = "C".($row_awal + ($data_pembanding-1));
                            $g = "D".$row_awal;
                            $h = "D".($row_awal + ($data_pembanding-1));
                            $i = "E".$row_awal;
                            $j = "E".($row_awal + ($data_pembanding-1));
                            $k = "F".$row_awal;
                            $l = "F".($row_awal + ($data_pembanding-1));
                            $m = "G".$row_awal;
                            $n = "G".($row_awal + ($data_pembanding-1));

                            $style = array(
                              'alignment' => array(
                                'vertical' => 'center',
                              )
                            );

                            $sheet->getStyle("$a:$b")->applyFromArray($style);
                            $sheet->getStyle("$c:$d")->applyFromArray($style);
                            $sheet->getStyle("$e:$f")->applyFromArray($style);
                            $sheet->getStyle("$g:$h")->applyFromArray($style);
                            $sheet->getStyle("$i:$j")->applyFromArray($style);
                            $sheet->getStyle("$k:$l")->applyFromArray($style);
                            $sheet->getStyle("$m:$n")->applyFromArray($style);

                            $sheet->cells('A:V', function($cells) {
                               $cells->setFontFamily('Times New Roman');
                            });
                            $row_awal = $row_awal + ($data_pembanding);
                            foreach($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->detail_penerimaan_barang->where('flag', 0)->groupBy('group_id') as $key => $row){
                              foreach($row as $kuy => $data){
                                $sheet->setMergeColumn(array(
                                  'columns' => array('H','J','L','M','N','O','P','Q','R','S','T','U','V'),
                                  'rows' => array(
                                    array($row_kecil,($row_kecil + ($row->count()-1))),
                                    array($row_kecil,($row_kecil + ($row->count()-1))),
                                    array($row_kecil,($row_kecil + ($row->count()-1))),
                                    array($row_kecil,($row_kecil + ($row->count()-1))),
                                    array($row_kecil,($row_kecil + ($row->count()-1))),
                                    array($row_kecil,($row_kecil + ($row->count()-1))),
                                    array($row_kecil,($row_kecil + ($row->count()-1))),
                                    array($row_kecil,($row_kecil + ($row->count()-1))),
                                    array($row_kecil,($row_kecil + ($row->count()-1))),
                                    array($row_kecil,($row_kecil + ($row->count()-1))),
                                    array($row_kecil,($row_kecil + ($row->count()-1))),
                                    array($row_kecil,($row_kecil + ($row->count()-1))),
                                    array($row_kecil,($row_kecil + ($row->count()-1))),
                                  )
                                ));

                                $aa = "H".$row_kecil;
                                $bb = "H".($row_kecil + ($row->count()-1));
                                $cc = "J".$row_kecil;
                                $dd = "J".($row_kecil + ($row->count()-1));
                                $ee = "L".$row_kecil;
                                $ff = "L".($row_kecil + ($row->count()-1));
                                $gg = "M".$row_kecil;
                                $hh = "M".($row_kecil + ($row->count()-1));
                                $ii = "N".$row_kecil;
                                $jj = "N".($row_kecil + ($row->count()-1));
                                $kk = "O".$row_kecil;
                                $ll = "O".($row_kecil + ($row->count()-1));
                                $mm = "P".$row_kecil;
                                $nn = "P".($row_kecil + ($row->count()-1));
                                $oo = "Q".$row_kecil;
                                $pp = "Q".($row_kecil + ($row->count()-1));
                                $qq = "R".$row_kecil;
                                $rr = "R".($row_kecil + ($row->count()-1));
                                $ss = "S".$row_kecil;
                                $tt = "S".($row_kecil + ($row->count()-1));
                                $uu = "T".$row_kecil;
                                $vv = "T".($row_kecil + ($row->count()-1));
                                $ww = "U".$row_kecil;
                                $xx = "U".($row_kecil + ($row->count()-1));
                                $yy = "V".$row_kecil;
                                $zz = "V".($row_kecil + ($row->count()-1));

                                $style = array(
                                  'alignment' => array(
                                    'vertical' => 'center',
                                  )
                                );

                                $sheet->getStyle("$aa:$bb")->applyFromArray($style);
                                $sheet->getStyle("$cc:$dd")->applyFromArray($style);
                                $sheet->getStyle("$ee:$ff")->applyFromArray($style);
                                $sheet->getStyle("$gg:$hh")->applyFromArray($style);
                                $sheet->getStyle("$ii:$jj")->applyFromArray($style);
                                $sheet->getStyle("$kk:$ll")->applyFromArray($style);
                                $sheet->getStyle("$mm:$nn")->applyFromArray($style);
                                $sheet->getStyle("$oo:$pp")->applyFromArray($style);
                                $sheet->getStyle("$qq:$rr")->applyFromArray($style);
                                $sheet->getStyle("$ss:$tt")->applyFromArray($style);
                                $sheet->getStyle("$uu:$vv")->applyFromArray($style);
                                $sheet->getStyle("$ww:$xx")->applyFromArray($style);
                                $sheet->getStyle("$yy:$zz")->applyFromArray($style);

                                if($data->detail_pengujian->verifikasi == 1){
                                  $status = 'DITERIMA';
                                }else{
                                  $status = 'DITOLAK';
                                }
                                if($data->detail_pengujian){
                                  if($data->detail_pengujian->tgl_selesai){
                                    $tglawal = DateToStringYear(Carbon\Carbon::parse($data->detail_pengujian->tgl_selesai)->format('Y-m-d'));
                                  }
                                }

                                if($data->detail_pengujian->detailpelaksana){
                                  $pelaksana = $data->detail_pengujian->detailpelaksana->pelaksana->nama;
                                }

                                if($data->detail_pengujian->detailpelaksana){
                                  if($data->detail_pengujian->detailpelaksana->tgl_selesai){
                                    $tglakhir = DateToStringYear(Carbon\Carbon::parse($data->detail_pengujian->detailpelaksana->tgl_selesai)->format('Y-m-d'));
                                  }
                                }

                                if($data->detail_pengujian->detailpelaksana){
                                  if($data->detail_pengujian->detailpelaksana->status_hasil_pengujian == 1){
                                    $hasil = 'BERHASIL';
                                  }elseif($data->detail_pengujian->detailpelaksana->status_hasil_pengujian == 2){
                                    $hasil = 'GAGAL';
                                  }else{
                                    $hasil ='-';
                                  }
                                }

                                if($data->detail_pengujian->detailpelaksana){
                                  if($data->detail_pengujian->detailpelaksana->status == 0){
                                    $pelaksanalaporan = '-';
                                  }else{
                                    if($data->detail_pengujian->detailpelaksana->pelaksana_laporan_id !== 1){
                                      $pelaksanalaporan = $data->detail_pengujian->detailpelaksana->pelaksanalaporan->nama;
                                    }
                                  }
                                }

                                if($data->detail_pengujian->detailpelaksana){
                                  if($data->detail_pengujian->detailpelaksana->status_barang == 0){
                                    $status_barang = 'Barang Tidak Dikembalikan';
                                  }else{
                                    $status_barang = 'Barang Dikembalikan';
                                  }
                                }
                                foreach($data->detail_pengujian->detailpelaksana->detailmember as $member){
                                  $data_member[] = $member->pelaksana->nama; 
                                }
                                $member = implode(', ', $data_member);

                                if($kuy == 0){
                                  $sheet->row($awal, array(
                                      '',
                                                  '',
                                                  '',
                                                  '',
                                                  '',
                                                  '',
                                                  '',
                                                  $data->detail_pp->jenis->nama,
                                                  $data->nama_barang.' ['.$data->urutan.'/'.$data->max.']',
                                                  $data->detail_pp->merk,
                                                  $data->no_seri,
                                                  DateToStringYear($data->tanggal_terima),
                                                  $data->detail_pengujian->pelaksana->nama,
                                                  $status,
                                                  $tglawal,
                                                  $data->detail_pengujian->fpp_fpk,
                                                  $pelaksana,
                                                  $tglakhir,
                                                  $hasil,
                                                  $member,
                                                  $pelaksanalaporan,
                                                  $status_barang,
                                                  ));
                                  if($u == 0){
                                   $sheet->row($awal, array(
                                                  $cek->no_order,
                                                  $cek->user->pelanggans->perusahaan->nama,
                                                  DateToStringYear($cek->tgl_order),
                                                  $cek->no_surat,
                                                  $cek->kaji_ulang->surat->va->konfirmasi->wbs_io,
                                                  $cek->kaji_ulang->surat->no_surat,
                                                  $cek->pelayanan->nama.'/'.$cek->lingkup->nama,
                                                  ));
                                  }
                                }else{
                                  $sheet->row($awal, array(
                                      '',
                                                  '',
                                                  '',
                                                  '',
                                                  '',
                                                  '',
                                                  '',
                                                  $data->detail_pp->jenis->nama,
                                                  $data->nama_barang.' ['.$data->urutan.'/'.$data->max.']',
                                                  $data->detail_pp->merk,
                                                  $data->no_seri,
                                                  DateToStringYear($data->tanggal_terima),
                                                  $data->detail_pengujian->pelaksana->nama,
                                                  $status,
                                                  $tglawal,
                                                  $data->detail_pengujian->fpp_fpk,
                                                  $pelaksana,
                                                  $tglakhir,
                                                  $hasil,
                                                  $member,
                                                  $pelaksanalaporan,
                                                  $status_barang,
                                                  ));
                                }
                                $data_member= [];
                                          $awal=$awal;
                                          $awal++;$no++;
                              }
                                        $row_kecil = $row_kecil + ($row->count());
                              $u++;
                              $i++;
                            }
                            $data_pembanding=0;
                          }
                        }
                      }
                    }
                  }
                }
              }
                    }
                  }
              });
          });

          $reports->setFileName("History Data - ".$pelayanan->nama)->download('xlsx');
          return response([
              'status' => true,
          ]);
      }else{
        $records = PendaftaranPengujian::where('layanan_id', $id)->get();
        $pelayanan = JenisPelayanan::find($id);
          $reports =  Excel::load(public_path('template-xls/template-historis.xlsx'), function($reader) use ($records){
              $reader->sheet('Sheet', function ($sheet) use ($records){
                  $awal = 7;
                  $no =1;
                  $i=1;
          $angka =0;
          $jumlah=0;
          $aa=0;
          $status ='-';
          $tglawal ='-';
          $tglakhir ='-';
          $pelaksana ='-';
          $hasil ='-';
          $pelaksanalaporan = '-';
          $status_barang = '-';
          $data_pembanding=0;
          $row_awal =7;
          $row_akhir=0;
          $a=0;$b=0;$c=0;$d=0;$e=0;$f=0;$g=0;$h=0;$i=0;$j=0;$k=0;$l=0;$m=0;$n=0;
          $merge = 0;
          $row_kecil=7;
                  foreach($records as $cek){
                    if($cek->kaji_ulang){
              if($cek->kaji_ulang->surat){
                if($cek->kaji_ulang->surat->va){
                  if($cek->kaji_ulang->surat->va->konfirmasi){
                    if($cek->kaji_ulang->surat->va->konfirmasi->penerimaan){
                      if($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian){
                        if($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->detailpengujian->count() > 0){
                          if($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->detailpengujian->first()->detailpelaksana){
                            foreach($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->detail_penerimaan_barang->where('flag', 0)->groupBy('group_id') as $pembanding){
                              $data_pembanding += $pembanding->count();
                            }
                            $u =0;
                            $sheet->setMergeColumn(array(
                              'columns' => array('A','B','C','D','E','F','G'),
                              'rows' => array(
                                array($row_awal,($row_awal + ($data_pembanding-1))),
                                array($row_awal,($row_awal + ($data_pembanding-1))),
                                array($row_awal,($row_awal + ($data_pembanding-1))),
                                array($row_awal,($row_awal + ($data_pembanding-1))),
                                array($row_awal,($row_awal + ($data_pembanding-1))),
                                array($row_awal,($row_awal + ($data_pembanding-1))),
                                array($row_awal,($row_awal + ($data_pembanding-1))),
                              )
                            ));
                            $a = "A".$row_awal;
                            $b = "A".($row_awal + ($data_pembanding-1));
                            $c = "B".$row_awal;
                            $d = "B".($row_awal + ($data_pembanding-1));
                            $e = "C".$row_awal;
                            $f = "C".($row_awal + ($data_pembanding-1));
                            $g = "D".$row_awal;
                            $h = "D".($row_awal + ($data_pembanding-1));
                            $i = "E".$row_awal;
                            $j = "E".($row_awal + ($data_pembanding-1));
                            $k = "F".$row_awal;
                            $l = "F".($row_awal + ($data_pembanding-1));
                            $m = "G".$row_awal;
                            $n = "G".($row_awal + ($data_pembanding-1));

                            $style = array(
                              'alignment' => array(
                                'vertical' => 'center',
                              )
                            );

                            $sheet->getStyle("$a:$b")->applyFromArray($style);
                            $sheet->getStyle("$c:$d")->applyFromArray($style);
                            $sheet->getStyle("$e:$f")->applyFromArray($style);
                            $sheet->getStyle("$g:$h")->applyFromArray($style);
                            $sheet->getStyle("$i:$j")->applyFromArray($style);
                            $sheet->getStyle("$k:$l")->applyFromArray($style);
                            $sheet->getStyle("$m:$n")->applyFromArray($style);
                            $sheet->cells('A:V', function($cells) {
                               $cells->setFontFamily('Times New Roman');
                            });
                            $row_awal = $row_awal + ($data_pembanding);
                            foreach($cek->kaji_ulang->surat->va->konfirmasi->penerimaan->detail_penerimaan_barang->where('flag', 0)->groupBy('group_id') as $key => $row){
                              foreach($row as $kuy => $data){
                                foreach($data->detail_pengujian->detailpelaksana->detailmember as $member){
                                    $data_member[] = $member->pelaksana->nama; 
                                }
                                $member = implode(', ', $data_member);

                                if($data->detail_pengujian->verifikasi == 1){
                                  $status = 'DITERIMA';
                                }else{
                                  $status = 'DITOLAK';
                                }
                                if($data->detail_pengujian){
                                  if($data->detail_pengujian->tgl_selesai){
                                    $tglawal = DateToStringYear(Carbon\Carbon::parse($data->detail_pengujian->tgl_selesai)->format('Y-m-d'));
                                  }
                                }

                                if($data->detail_pengujian->detailpelaksana){
                                  $pelaksana = $data->detail_pengujian->detailpelaksana->pelaksana->nama;
                                }

                                if($data->detail_pengujian->detailpelaksana){
                                  if($data->detail_pengujian->detailpelaksana->tgl_selesai){
                                    $tglakhir = DateToStringYear(Carbon\Carbon::parse($data->detail_pengujian->detailpelaksana->tgl_selesai)->format('Y-m-d'));
                                  }
                                }

                                if($data->detail_pengujian->detailpelaksana){
                                  if($data->detail_pengujian->detailpelaksana->status_hasil_pengujian == 1){
                                    $hasil = 'BERHASIL';
                                  }elseif($data->detail_pengujian->detailpelaksana->status_hasil_pengujian == 2){
                                    $hasil = 'GAGAL';
                                  }else{
                                    $hasil ='-';
                                  }
                                }

                                if($data->detail_pengujian->detailpelaksana){
                                  if($data->detail_pengujian->detailpelaksana->status == 0){
                                    $pelaksanalaporan = '-';
                                  }else{
                                    if($data->detail_pengujian->detailpelaksana->pelaksana_laporan_id !== 1){
                                      $pelaksanalaporan = $data->detail_pengujian->detailpelaksana->pelaksanalaporan->nama;
                                    }
                                  }
                                }

                                if($data->detail_pengujian->detailpelaksana){
                                  if($data->detail_pengujian->detailpelaksana->status_barang == 0){
                                    $status_barang = 'Barang Tidak Dikembalikan';
                                  }else{
                                    $status_barang = 'Barang Dikembalikan';
                                  }
                                }

                                if($kuy == 0){
                                  $sheet->row($awal, array(
                                      '',
                                                  '',
                                                  '',
                                                  '',
                                                  '',
                                                  '',
                                                  '',
                                                  $data->detail_pp->jenis->nama,
                                                  $data->nama_barang.' ['.$data->urutan.'/'.$data->max.']',
                                                  $data->detail_pp->merk,
                                                  $data->no_seri,
                                                  DateToStringYear($data->tanggal_terima),
                                                  $data->detail_pengujian->pelaksana->nama,
                                                  $status,
                                                  $tglawal,
                                                  $data->detail_pengujian->fpp_fpk,
                                                  $pelaksana,
                                                  $tglakhir,
                                                  $hasil,
                                                  $member,
                                                  $pelaksanalaporan,
                                                  $status_barang,
                                                  ));
                                  if($u == 0){
                                   $sheet->row($awal, array(
                                                  $cek->no_order,
                                                  $cek->user->pelanggans->perusahaan->nama,
                                                  DateToStringYear($cek->tgl_order),
                                                  $cek->no_surat,
                                                  $cek->kaji_ulang->surat->va->konfirmasi->wbs_io,
                                                  $cek->kaji_ulang->surat->no_surat,
                                                  $cek->pelayanan->nama.'/'.$cek->lingkup->nama,
                                                  ));
                                  }
                                }else{
                                  $sheet->row($awal, array(
                                      '',
                                                  '',
                                                  '',
                                                  '',
                                                  '',
                                                  '',
                                                  '',
                                                  $data->detail_pp->jenis->nama,
                                                  $data->nama_barang.' ['.$data->urutan.'/'.$data->max.']',
                                                  $data->detail_pp->merk,
                                                  $data->no_seri,
                                                  DateToStringYear($data->tanggal_terima),
                                                  $data->detail_pengujian->pelaksana->nama,
                                                  $status,
                                                  $tglawal,
                                                  $data->detail_pengujian->fpp_fpk,
                                                  $pelaksana,
                                                  $tglakhir,
                                                  $hasil,
                                                  $member,
                                                  $pelaksanalaporan,
                                                  $status_barang,
                                                  ));
                                }
                                $data_member =[];
                                          $awal=$awal;
                                          $awal++;$no++;
                              }
                                        $row_kecil = $row_kecil + ($row->count());
                              $u++;
                              $i++;
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
                    }
                  }
              });
          });

          $reports->setFileName("History Data - ".$pelayanan->nama)->download('xlsx');
          return response([
              'status' => true,
          ]);
      }
    }
}
