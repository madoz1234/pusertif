<?php

namespace App\Http\Controllers\API\HistoryData;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Unlu\Laravel\Api\QueryBuilder;

/* Validation */


/* Models */
use App\Models\FrontEnd\PendaftaranPengujian;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Zipper;


class HistoryDataBUController extends Controller
{
  public function grid(Request $request)
  {
    $pengujian = [];
    $data = PendaftaranPengujian::with('pelayanan','lingkup','user.pelanggans','user.pelanggans.perusahaan')->where('tipe_surat', 0)->select('*')->get();
    if(auth()->user()->hasRole(['yan-uji'])){
      $data = PendaftaranPengujian::with('pelayanan','lingkup','user.pelanggans','user.pelanggans.perusahaan')->where('tipe_surat', 0)->select('*')->where('jenis',1)->get();
    }elseif(auth()->user()->hasRole(['yan-kalibrasi'])){
      $data = PendaftaranPengujian::with('pelayanan','lingkup','user.pelanggans','user.pelanggans.perusahaan')->where('tipe_surat', 0)->select('*')->where('jenis',0)->get();
    }

    if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi'])){
      $pengujian = $data->map(function ($item, $key){
                      $download = [
                        'order' => null,
                        'surat_penawaran' => null,
                        'virtual_account' => null,
                        'konfirmasi_pembayaran' => null,
                        'laporan_pengujian' => null,
                      ];
                      if($item->tipe==2){
                        if($item->status_ams==1){
                          $download['order'] = url('history-data/download/order/'.$item->id);
                        }
                      }else{
                          $download['order'] = url('history-data/download/order/'.$item->id);
                      }

                      if($item->kaji_ulang){
                          if($item->kaji_ulang->surat){
                              if($item->kaji_ulang->surat->surat_status==1){
                                $download['surat_penawaran'] = url('history-data/download/surat-penawaran/'.$item->id);
                              }
                          }
                      }

                      if($item->kaji_ulang){
                          if($item->kaji_ulang->surat){
                              if($item->kaji_ulang->surat->va){
                                  if(!is_null($item->kaji_ulang->surat->va->download_date)){
                                    $download['virtual_account'] = url('virtual-account/download/'.$item->kaji_ulang->surat->va->id);
                                  }
                              }
                          }
                      }

                      if($item->kaji_ulang){
                          if($item->kaji_ulang->surat){
                              if($item->kaji_ulang->surat->va){
                                  if($item->kaji_ulang->surat->va->bukti_url){
                                      $download['konfirmasi_pembayaran'] = url('history-data/download/konfirmasi-pembayaran/'.$item->id);
                                  }
                              }
                          }
                      }

                      if($item->kaji_ulang){
                          if($item->kaji_ulang->surat){
                              if($item->kaji_ulang->surat->va){
                                  if($item->kaji_ulang->surat->va){
                                      if($item->kaji_ulang->surat->va->konfirmasi){
                                          if($item->kaji_ulang->surat->va->konfirmasi->penerimaan){
                                              if($item->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian){
                                                  if($item->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->laporan_pengujian){
                                                      $download['laporan_pengujian'] = url('history-data/download/laporan-pengujian/'.$item->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->id);
                                                  }
                                              }
                                          }
                                      }
                                  }
                              }
                          }
                      }



                      $array = [
                          'id' => $item->id,
                          'no_order' => $item->no_order,
                          'tgl_order' => $item->tgl_order,
                          'no_surat' => $item->no_surat,
                          'perusahaan' => isset($item->user->pelanggans->perusahaan->nama) ? $item->user->pelanggans->perusahaan->nama : '-',
                          'pelanggan' => isset($item->user->pelanggans->nama_lengkap) ? $item->user->pelanggans->nama_lengkap : '-',
                          'layanan' => $item->pelayanan->nama,
                          'lingkup' => $item->lingkup->nama,
                          'download' => $download,
                      ];
                      return $array;
                  });
    }

    return response([
        'data' => $pengujian,
    ]);

  }


  public function download($menu,$id){
        $type = 'pendaftaran-pengujian';
        $now = Carbon::now()->format('Ymdhis');
        Zipper::make(public_path('storage/'.$menu.'_'.$now.'.zip'))->close();
        switch ($menu) {
            case 'order':
                $type = 'pendaftaran-pengujian';
                $surat = PendaftaranPengujian::find($id);
                $pendukung = Files::where('target_id', $id)->where('target_type', $type)->where('type', null)->get();

                if(file_exists(public_path('storage/'.$surat->bukti)))
                {
                    $newname = '';
                    $splitname = explode("/",$surat->bukti);
                    for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                        $newname .= $splitname[$i].'/';
                    }
                    Storage::disk('public')->move($surat->bukti, $newname.$surat->filename);
                    $zipper = new \Chumper\Zipper\Zipper;
                    $zipper->zip(public_path('storage/'.$menu.'_'.$now.'.zip'))->folder('surat permintaan')->add(public_path('storage/'.$newname.$surat->filename));
                    $zipper->close();
                    Storage::disk('public')->move($newname.$surat->filename, $surat->bukti);
                }

                $files = [];
                if($pendukung->count() > 0)
                {
                    $rename = [];
                    foreach($pendukung as $c)
                    {
                        if(file_exists(public_path('storage/'.$c->url)))
                        {
                            $newname = '';
                            $splitname = explode("/",$c->url);
                            for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                                $newname .= $splitname[$i].'/';
                            }
                            Storage::disk('public')->move($c->url, $newname.$c->filename);
                            $files[] = public_path('storage/'.$newname.$c->filename);
                            $rename[] = [
                                'old' => $c->url,
                                'new' => $newname.$c->filename,
                            ];
                        }
                    }
                    $zipper2 = new \Chumper\Zipper\Zipper;
                    $zipper2->zip(public_path('storage/'.$menu.'_'.$now.'.zip'))->folder('dokumen pendukung')->add($files);
                    $zipper2->close();

                    for ($i=0; $i < count($rename) ; $i++) { 
                        Storage::disk('public')->move($rename[$i]['new'], $rename[$i]['old']);
                    }
                }
                break;

            case 'surat-penawaran':
                $type = 'surat-penawaran';
                $pendukung = Files::where('target_id', $id)->where('target_type', $type)->where('type', null)->get();

                $files = [];
                if($pendukung->count() > 0)
                {
                    $rename = [];
                    foreach($pendukung as $c)
                    {
                        if(file_exists(public_path('storage/'.$c->url)))
                        {
                            $newname = '';
                            $splitname = explode("/",$c->url);
                            for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                                $newname .= $splitname[$i].'/';
                            }
                            Storage::disk('public')->move($c->url, $newname.$c->filename);
                            $files[] = public_path('storage/'.$newname.$c->filename);
                            $rename[] = [
                                'old' => $c->url,
                                'new' => $newname.$c->filename,
                            ];
                        }
                    }
                    $zipper = new \Chumper\Zipper\Zipper;
                    $zipper->zip(public_path('storage/'.$menu.'_'.$now.'.zip'))->folder('surat penawaran')->add($files);
                    $zipper->close();

                    for ($i=0; $i < count($rename) ; $i++) { 
                        Storage::disk('public')->move($rename[$i]['new'], $rename[$i]['old']);
                    }
                }
                break;
            case 'konfirmasi-pembayaran':
                $pp = PendaftaranPengujian::find($id);
                $file = $pp->kaji_ulang->surat->va->bukti_url;
                $file_name = $pp->kaji_ulang->surat->va->bukti_filename;
                if(file_exists(public_path('storage/'.$file)))
                {
                    $newname = '';
                    $splitname = explode("/",$file);
                    for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                        $newname .= $splitname[$i].'/';
                    }
                    Storage::disk('public')->move($file, $newname.$file_name);
                    $zipper = new \Chumper\Zipper\Zipper;
                    $zipper->zip(public_path('storage/'.$menu.'_'.$now.'.zip'))->folder('bukti pembayaran')->add(public_path('storage/'.$newname.$file_name));
                    $zipper->close();
                    Storage::disk('public')->move($newname.$file_name, $file);
                }
                break;
            case 'laporan-pengujian':
                $type = 'laporan-pengujian';
                $laporan = LaporanPengujian::where('pengujian_id',$id)->get();
                foreach ($laporan as $lap) {
                    $pendukung = Files::where('target_id', $lap->id)->where('target_type', $type)->where('type', 2)->get();

                    $files = [];
                    if($pendukung->count() > 0)
                    {
                        $rename = [];
                        foreach($pendukung as $c)
                        {
                            if(file_exists(public_path('storage/'.$c->url)))
                            {
                                $newname = '';
                                $splitname = explode("/",$c->url);
                                for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                                    $newname .= $splitname[$i].'/';
                                }
                                Storage::disk('public')->move($c->url, $newname.$c->filename);
                                $files[] = public_path('storage/'.$newname.$c->filename);
                                $rename[] = [
                                    'old' => $c->url,
                                    'new' => $newname.$c->filename,
                                ];
                            }
                        }
                        $zipper = new \Chumper\Zipper\Zipper;
                        $zipper->zip(public_path('storage/'.$menu.'_'.$now.'.zip'))->folder('laporan pengujian')->add($files);
                        $zipper->close();

                        for ($i=0; $i < count($rename) ; $i++) { 
                            Storage::disk('public')->move($rename[$i]['new'], $rename[$i]['old']);
                        }
                    }
                }
                break;
            
            default:
                # code...
                break;
        }
        if(file_exists(public_path('storage/'.$menu.'_'.$now.'.zip')))
        {
            return response()->download(public_path('storage/'.$menu.'_'.$now.'.zip'));
        }
        return response([
          'status' => 'error'
        ],500);


    }
}
