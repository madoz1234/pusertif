<?php

namespace App\Http\Controllers\API\KonfirmasiPembayaran;

/* Base App */
use Unlu\Laravel\Api\QueryBuilder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\VirtualAccount\KonfirmasiPembayaran;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\KajiUlang\KajiUlang;
use App\Models\KajiUlang\Detail;
use App\Models\Authentication\Role;

/* Validation */
use App\Http\Requests\VirtualAccount\KonfirmasiPembayaranRequest;
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\VirtualAccount\VirtualAccountRequest;

/* Models */
 
/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Excel;
use Auth;
use DB;

class KonfirmasiPembayaranController extends Controller
{
    public function index(Request $request)
    {
        $data = new QueryBuilder(new VirtualAccount, $request);
        if($page=$request->page){
              return $data->build()->paginate();
        }

        return response()->json([
              'status' => true,
              'data' => $data->build()->get()
        ]);
    }
    public function belum(Request $request)
    {
        if(auth()->user()->hasRole(['keuangan'])){
            $data = VirtualAccount::with('surat.kaji_ulang.pp','surat.kaji_ulang.pp.user.pelanggans.perusahaan','surat.kaji_ulang.pp.pelayanan','surat.kaji_ulang.pp.lingkup','surat.kaji_ulang.pp.detail.jenis','surat.kaji_ulang.surat.detail','surat.kaji_ulang.detail.lokasi','surat.kaji_ulang.detail.mata_uji','surat.kaji_ulang.detail.detail_pendaftaran','surat.files')
                                  ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                 ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                 ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                  ->where('trans_va.status', 1)
                                  ->whereIn('trans_va.tipe_customer', [1,0])
                                  ->doesnthave('konfirmasi')
                                  ->where('trans_va.tipe', 0)
                                  ->select('trans_va.*');

            if ($no_va = $request->no_va) {
                $data->where('no_va','like','%'.$no_va.'%');
            }

            if ($no_surat = $request->no_surat) {
                $data->whereHas('surat', function($surat) use ($no_surat){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                        $kaji->whereHas('pp', function($pp) use ($no_surat){
                            $pp->where('no_surat','like', '%'.$no_surat.'%');
                        });
                    });
                });
            }
            if ($no_order = $request->no_order) {
                $data->whereHas('surat', function($surat) use ($no_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                        $kaji->whereHas('pp', function($pp) use ($no_order){
                            $pp->where('no_order','like', '%'.$no_order.'%');
                        });
                    });
                });
            }
            if ($tanggal_order = $request->tanggal_order) {
                $data->whereHas('surat', function($surat) use ($tanggal_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                        $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                        });
                    });
                });
            }
            if ($wbs_io = $request->wbs_io) {
                $data->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                    $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
                });
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                $data->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                        $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                            $pp->where('layanan_id',$jenis_pelayanan_id);
                        });
                    });
                });
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                $data->whereHas('surat', function($surat) use ($perusahaan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                        $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                            $pp->whereHas('user', function($user) use ($perusahaan_id){
                                $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                                });
                            });
                        });
                    });
                });
            }
            if($sort = $request->sort){
              if($sort == 'order'){
                $data->orderBy('trans_pp.tgl_order','asc');
              }
              if($sort == 'surat'){
                $data->orderBy('trans_pp.tgl_surat','asc');
              }
            }else{
                $data->orderBy('trans_va.created_at','desc');
            }
          }else{
            // $data =[];
              return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
          }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

        return response()->json($page);   
    }

    public function kurang(Request $request)
    {
        if(auth()->user()->hasRole(['keuangan'])){
          $data = VirtualAccount::with('surat.kaji_ulang.pp','surat.kaji_ulang.pp.user.pelanggans.perusahaan','surat.kaji_ulang.pp.pelayanan','surat.kaji_ulang.pp.lingkup','surat.kaji_ulang.pp.detail.jenis','surat.kaji_ulang.surat.detail','surat.kaji_ulang.detail.lokasi','surat.kaji_ulang.detail.mata_uji','surat.kaji_ulang.detail.detail_pendaftaran','surat.files')
                                ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                 ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                 ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                ->where('trans_va.status', 3)
                                ->whereHas('konfirmasi', function($q){
                                    return $q->where('status', 1);
                                })
                                ->where('trans_va.tipe', 0)
                                ->select('trans_va.*');

          
          if ($no_va = $request->no_va) {
              $data->where('no_va','like','%'.$no_va.'%');
          }

          if ($no_surat = $request->no_surat) {
              $data->whereHas('surat', function($surat) use ($no_surat){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                      $kaji->whereHas('pp', function($pp) use ($no_surat){
                          $pp->where('no_surat','like', '%'.$no_surat.'%');
                      });
                  });
              });
          }
          if ($no_order = $request->no_order) {
              $data->whereHas('surat', function($surat) use ($no_order){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                      $kaji->whereHas('pp', function($pp) use ($no_order){
                          $pp->where('no_order','like', '%'.$no_order.'%');
                      });
                  });
              });
          }
          if ($tanggal_order = $request->tanggal_order) {
              $data->whereHas('surat', function($surat) use ($tanggal_order){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                      $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                          $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                      });
                  });
              });
          }
          if ($wbs_io = $request->wbs_io) {
              $data->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                  $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
              });
          }
          if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
              $data->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                      $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                          $pp->where('layanan_id',$jenis_pelayanan_id);
                      });
                  });
              });
          }
          if ($perusahaan_id = $request->perusahaan_id) {
              $data->whereHas('surat', function($surat) use ($perusahaan_id){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                      $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                          $pp->whereHas('user', function($user) use ($perusahaan_id){
                              $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                  $pelanggan->where('perusahaan_id',$perusahaan_id);
                              });
                          });
                      });
                  });
              });
          }
          if($sort = $request->sort){
            if($sort == 'order'){
              $data->orderBy('trans_pp.tgl_order','asc');
            }
            if($sort == 'surat'){
              $data->orderBy('trans_pp.tgl_surat','asc');
            }
          }else{
              $data->orderBy('trans_va.created_at','desc');
          }
        }else{
            // $data =[];
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
          }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

        return response()->json($page);   
    }

    public function lebih(Request $request)
    {
        if(auth()->user()->hasRole(['keuangan'])){
          $data = VirtualAccount::with('surat.kaji_ulang.pp','surat.kaji_ulang.pp.user.pelanggans.perusahaan','surat.kaji_ulang.pp.pelayanan','surat.kaji_ulang.pp.lingkup','surat.kaji_ulang.pp.detail.jenis','surat.kaji_ulang.surat.detail','surat.kaji_ulang.detail.lokasi','surat.kaji_ulang.detail.mata_uji','surat.kaji_ulang.detail.detail_pendaftaran','surat.files')
                                ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                 ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                 ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                ->where('trans_va.status', 3)
                                ->whereHas('konfirmasi', function($q){
                                    return $q->where('status', 2);
                                })
                                ->where('trans_va.tipe', 0)
                                ->select('trans_va.*');

          if ($no_va = $request->no_va) {
              $data->where('no_va','like','%'.$no_va.'%');
          }

          if ($no_surat = $request->no_surat) {
              $data->whereHas('surat', function($surat) use ($no_surat){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                      $kaji->whereHas('pp', function($pp) use ($no_surat){
                          $pp->where('no_surat','like', '%'.$no_surat.'%');
                      });
                  });
              });
          }
          if ($no_order = $request->no_order) {
              $data->whereHas('surat', function($surat) use ($no_order){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                      $kaji->whereHas('pp', function($pp) use ($no_order){
                          $pp->where('no_order','like', '%'.$no_order.'%');
                      });
                  });
              });
          }
          if ($tanggal_order = $request->tanggal_order) {
              $data->whereHas('surat', function($surat) use ($tanggal_order){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                      $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                          $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                      });
                  });
              });
          }
          if ($wbs_io = $request->wbs_io) {
              $data->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                  $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
              });
          }
          if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
              $data->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                      $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                          $pp->where('layanan_id',$jenis_pelayanan_id);
                      });
                  });
              });
          }
          if ($perusahaan_id = $request->perusahaan_id) {
              $data->whereHas('surat', function($surat) use ($perusahaan_id){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                      $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                          $pp->whereHas('user', function($user) use ($perusahaan_id){
                              $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                  $pelanggan->where('perusahaan_id',$perusahaan_id);
                              });
                          });
                      });
                  });
              });
          }
          if($sort = $request->sort){
            if($sort == 'order'){
              $data->orderBy('trans_pp.tgl_order','asc');
            }
            if($sort == 'surat'){
              $data->orderBy('trans_pp.tgl_surat','asc');
            }
          }else{
              $data->orderBy('trans_va.created_at','desc');
          }
        }else{
            // $data =[];
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
          }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

        return response()->json($page);
    }

    public function lunas(Request $request)
    {
       if(auth()->user()->hasRole(['keuangan'])){
          $data = VirtualAccount::with('surat.kaji_ulang.pp','surat.kaji_ulang.pp.user.pelanggans.perusahaan','surat.kaji_ulang.pp.pelayanan','surat.kaji_ulang.pp.lingkup','surat.kaji_ulang.pp.detail.jenis','surat.kaji_ulang.surat.detail','surat.kaji_ulang.detail.lokasi','surat.kaji_ulang.detail.mata_uji','surat.kaji_ulang.detail.detail_pendaftaran','surat.files')
                                ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                 ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                 ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                ->where('trans_va.status', 3)
                                ->whereHas('konfirmasi', function($q){
                                    return $q->where('status', 3);
                                })
                                ->where('trans_va.tipe', 0)
                                ->select('trans_va.*');

          if ($no_va = $request->no_va) {
              $data->where('no_va','like','%'.$no_va.'%');
          }

          if ($no_surat = $request->no_surat) {
              $data->whereHas('surat', function($surat) use ($no_surat){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                      $kaji->whereHas('pp', function($pp) use ($no_surat){
                          $pp->where('no_surat','like', '%'.$no_surat.'%');
                      });
                  });
              });
          }
          if ($no_order = $request->no_order) {
              $data->whereHas('surat', function($surat) use ($no_order){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                      $kaji->whereHas('pp', function($pp) use ($no_order){
                          $pp->where('no_order','like', '%'.$no_order.'%');
                      });
                  });
              });
          }
          if ($tanggal_order = $request->tanggal_order) {
              $data->whereHas('surat', function($surat) use ($tanggal_order){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                      $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                          $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                      });
                  });
              });
          }
          if ($wbs_io = $request->wbs_io) {
              $data->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                  $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
              });
          }
          if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
              $data->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                      $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                          $pp->where('layanan_id',$jenis_pelayanan_id);
                      });
                  });
              });
          }
          if ($perusahaan_id = $request->perusahaan_id) {
              $data->whereHas('surat', function($surat) use ($perusahaan_id){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                      $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                          $pp->whereHas('user', function($user) use ($perusahaan_id){
                              $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                  $pelanggan->where('perusahaan_id',$perusahaan_id);
                              });
                          });
                      });
                  });
              });
          }
          if($sort = $request->sort){
            if($sort == 'order'){
              $data->orderBy('trans_pp.tgl_order','asc');
            }
            if($sort == 'surat'){
              $data->orderBy('trans_pp.tgl_surat','asc');
            }
          }else{
              $data->orderBy('trans_va.created_at','desc');
          }
        }else{
            // $data =[];
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
          }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

        return response()->json($page);   
    }

    public function terbit(Request $request)
    {
        if(auth()->user()->hasRole(['keuangan'])){
          $data = VirtualAccount::with('surat.kaji_ulang.pp','surat.kaji_ulang.pp.user.pelanggans.perusahaan','surat.kaji_ulang.pp.pelayanan','surat.kaji_ulang.pp.lingkup','surat.kaji_ulang.pp.detail.jenis','surat.kaji_ulang.surat.detail','surat.kaji_ulang.detail.lokasi','surat.kaji_ulang.detail.mata_uji','surat.kaji_ulang.detail.detail_pendaftaran','surat.files')
                      ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                       ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                       ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                      ->where('trans_va.status', 3)
          						->where('trans_va.tipe', 0)
                      ->select('trans_va.*');

          if ($no_va = $request->no_va) {
              $data->where('no_va','like','%'.$no_va.'%');
          }

          if ($no_surat = $request->no_surat) {
              $data->whereHas('surat', function($surat) use ($no_surat){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                      $kaji->whereHas('pp', function($pp) use ($no_surat){
                          $pp->where('no_surat','like', '%'.$no_surat.'%');
                      });
                  });
              });
          }
          if ($no_order = $request->no_order) {
              $data->whereHas('surat', function($surat) use ($no_order){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                      $kaji->whereHas('pp', function($pp) use ($no_order){
                          $pp->where('no_order','like', '%'.$no_order.'%');
                      });
                  });
              });
          }
          if ($tanggal_order = $request->tanggal_order) {
              $data->whereHas('surat', function($surat) use ($tanggal_order){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                      $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                          $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                      });
                  });
              });
          }
          if ($wbs_io = $request->wbs_io) {
              $data->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                  $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
              });
          }
          if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
              $data->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                      $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                          $pp->where('layanan_id',$jenis_pelayanan_id);
                      });
                  });
              });
          }
          if ($perusahaan_id = $request->perusahaan_id) {
              $data->whereHas('surat', function($surat) use ($perusahaan_id){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                      $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                          $pp->whereHas('user', function($user) use ($perusahaan_id){
                              $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                  $pelanggan->where('perusahaan_id',$perusahaan_id);
                              });
                          });
                      });
                  });
              });
          }
          if($sort = $request->sort){
            if($sort == 'order'){
              $data->orderBy('trans_pp.tgl_order','asc');
            }
            if($sort == 'surat'){
              $data->orderBy('trans_pp.tgl_surat','asc');
            }
          }else{
              $data->orderBy('trans_va.created_at','desc');
          }
        }else{
            // $data =[];
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
          }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

        return response()->json($page);
    }

    public function histori(Request $request)
    {
      if (auth()->user()->hasRole(['keuangan'])){
        $data = VirtualAccount::with('surat.kaji_ulang.pp','surat.kaji_ulang.pp.user.pelanggans.perusahaan','surat.kaji_ulang.pp.pelayanan','surat.kaji_ulang.pp.lingkup','surat.kaji_ulang.pp.detail.jenis','surat.kaji_ulang.surat.detail','surat.kaji_ulang.detail.lokasi','surat.kaji_ulang.detail.mata_uji','surat.kaji_ulang.detail.detail_pendaftaran','surat.files')
          ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
           ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
           ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
          ->where('trans_va.status', 3)
          ->whereHas('surat', function($q){
              $q->whereHas('kaji_ulang',function($x){
                  $x->whereHas('pp',function($y){
                      return $y->where('status',6);
                  });
              });
          })
          ->where('trans_va.tipe', 0)
          ->select('trans_va.*');

          if ($no_va = $request->no_va) {
              $data->where('no_va','like','%'.$no_va.'%');
          }

          if ($no_surat = $request->no_surat) {
              $data->whereHas('surat', function($surat) use ($no_surat){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                      $kaji->whereHas('pp', function($pp) use ($no_surat){
                          $pp->where('no_surat','like', '%'.$no_surat.'%');
                      });
                  });
              });
          }
          if ($no_order = $request->no_order) {
              $data->whereHas('surat', function($surat) use ($no_order){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                      $kaji->whereHas('pp', function($pp) use ($no_order){
                          $pp->where('no_order','like', '%'.$no_order.'%');
                      });
                  });
              });
          }
          if ($tanggal_order = $request->tanggal_order) {
              $data->whereHas('surat', function($surat) use ($tanggal_order){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                      $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                          $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                      });
                  });
              });
          }
          if ($wbs_io = $request->wbs_io) {
              $data->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                  $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
              });
          }
          if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
              $data->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                      $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                          $pp->where('layanan_id',$jenis_pelayanan_id);
                      });
                  });
              });
          }
          if ($perusahaan_id = $request->perusahaan_id) {
              $data->whereHas('surat', function($surat) use ($perusahaan_id){
                  $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                      $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                          $pp->whereHas('user', function($user) use ($perusahaan_id){
                              $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                  $pelanggan->where('perusahaan_id',$perusahaan_id);
                              });
                          });
                      });
                  });
              });
          }
          if($sort = $request->sort){
            if($sort == 'order'){
              $data->orderBy('trans_pp.tgl_order','asc');
            }
            if($sort == 'surat'){
              $data->orderBy('trans_pp.tgl_surat','asc');
            }
          }else{
              $data->orderBy('trans_va.created_at','desc');
          }
      }else{
        return response()->json([
            'status' => false,
            'message' => 'page not found'
        ],404);
      }
      $page = $data->paginate(10);
      if ($page[0] == null){
        return response()->json([
          'status' => false,
          'message' => 'page not found'
        ],404);
      }
      return response()->json($page);
    }

    public function download($id)
    {
      $daftar = PendaftaranPengujian::find($id);
      if(file_exists(public_path('storage/'.$daftar->bukti)))
      {
        return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
      }

      return response([
        'status' => 'error'
      ],500);
    }
}