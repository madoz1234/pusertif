<?php

namespace App\Http\Controllers\API;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Unlu\Laravel\Api\QueryBuilder;

/* Validation */
use App\Http\Requests\API\ProfileRequest;
use App\Http\Requests\API\ChangePasswordRequest;

/* Models */
use App\Models\User;
/* Libraries */
use Datatables;
use Entrust;
use Carbon\Carbon;
use Hash;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [
            'user' => $user = auth()->user()
        ];

        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(ProfileRequest $request)
    {
        // Data Pegawai
        $pegawai = auth()->user()->pegawai;
        if($pegawai){
            $pegawai->nama = $request->pegawai['nama'];
            $pegawai->nip = $request->pegawai['nip'];
            $pegawai->telepon = $request->pegawai['telepon'];
            $pegawai->save();
        }

        $user = auth()->user();
        $user->username = $request->user['username'];
        $user->username = $request->user['email'];
        if(isset($request->user['photo'])){
            $file = $request->user['photo'];

            $photo = $file->store('uploads/user', 'public');
            $user->photo = $photo;
        }
        $user->save();

        return response([
            'status' => true
        ]);
    }

    public function changePhoto(Request $request)
    {
        $record = auth()->user();
        $record->username = $request->username;
        $record->email = $request->email;
        if(isset($request->photo)){
            $file = $request->photo;

            $photo = $file->store('uploads/user', 'public');
            $record->photo = $photo;
        }
        $record->save();

        return response([
            'status' => true
        ]);  
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = auth()->user();
        if($request->old_password && !Hash::check($request->old_password, $user->password)){
            return response([
                'old_password' => ['Password Lama tidak sesuai']
            ], 422);
        }elseif($request->password && $request->password == $request->password_confirmation){
            $user->password = bcrypt($request->password);
        }
            
        // $user->password = bcrypt($request->password);
        $user->last_login = Carbon::now();
        $user->save();

        return response([
            'status' => true
        ]);
    }

    public function androidToken(Request $request)
    {
        $user = auth()->user();
        if ($user) {
            $user->android_token = $request->token;
            $user->last_activity = Carbon::now();
            $user->save();
        }

        return response([
            'status' => true
        ]);
    }
}
