<?php

namespace App\Http\Controllers\API;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Unlu\Laravel\Api\QueryBuilder;

/* Validation */


/* Models */
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\Act\ActPengujian;
use App\Models\Act\ActDisposisi;
/* Libraries */
use Datatables;
use Entrust;
use Carbon\Carbon;
use Hash;

class TrackingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tracking(Request $request)
    {
        $tracking = [];
        $row = PendaftaranPengujian::when($no_order = $request->no_order, function($q) use ($no_order) {
                                             $q->where('no_order',$no_order);
                                        })
                                        ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                                             $q->where('no_surat',$no_surat);
                                        })
                                        ->where('tipe_surat', 0)->first();
        if($row){
            $string = '';
            if($row->status == 1){
                $string = status($row->status);
            }elseif($row->status == 2){
                if($row->kaji_ulang){
                    if($row->kaji_ulang->surat){
                        if($row->kaji_ulang->surat->va){
                            if($row->kaji_ulang->surat->va->status == 0){
                                $string = "Menunggu Virtual Account";
                            }elseif($row->kaji_ulang->surat->va->status == 1){
                                if($row->user->pelanggans->perusahaan->kategori !== 0){
                                    $string = "Konfirmasi Pembayaran";
                                }else{
                                    $string = "Menunggu Konfirmasi Pembayaran";
                                }
                            }elseif($row->kaji_ulang->surat->va->status == 2){
                                if($row->kaji_ulang->surat->va->bukti_url){
                                    $string = "Virtual Account Kadaluarsa";
                                }else{
                                    $string = "Virtual Account Nonaktif";
                                }
                            }else{
                                if($row->user->pelanggans->perusahaan->kategori !== 0){
                                    if($row->kaji_ulang->surat->va->konfirmasi){
                                        if($row->kaji_ulang->surat->va->konfirmasi->status == 1){
                                            $string = "Kurang Bayar";
                                        }elseif($row->kaji_ulang->surat->va->konfirmasi->status == 2){
                                            $string = "Pengembalian Dana";
                                        }elseif($row->kaji_ulang->surat->va->konfirmasi->status == 3){
                                            if($row->kaji_ulang->surat->va->konfirmasi->penerimaan){
                                                if($row->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian){
                                                    $string = "Pengujian";
                                                }else{
                                                    $string = "Menunggu Pengujian";
                                                }
                                            }else{
                                                $string = "Penyerahan Barang Uji";
                                            }
                                        }else{
                                            $string = "Menunggu Konfirmasi Pembayaran";
                                        }
                                    }else{
                                        $string = "Menunggu Konfirmasi Pembayaran";
                                    }
                                }else{
                                    if($row->kaji_ulang->surat->va->konfirmasi){
                                        if($row->kaji_ulang->surat->va->konfirmasi->penerimaan){
                                            if($row->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian){
                                                $string = "Pengujian";
                                            }else{
                                                $string = "Menunggu Pengujian";
                                            }
                                        }else{
                                            $string = "Penyerahan Barang Uji";
                                        }
                                    }else{
                                        $string = "Pembayaran Berhasil";
                                    }
                                }
                            }
                        }else{
                            $string = "Menunggu Surat Penawaran Terkirim";
                        }
                    }else{
                        if($row->kaji_ulang->keputusan == 3){
                            $string = "Didiskusikan";
                        }elseif($row->kaji_ulang->keputusan == 2){
                            if($row->kaji_ulang->bukti){
                                $string = "Ditolak";
                            }else{
                                $string = "Menunggu Surat Penawaran";
                            }
                        }else{
                            $string = "Menunggu Surat Penawaran";
                        }
                    }
                }else{
                    return '<label class="ui olive tag label">Menunggu Kaji Ulang</label>';
                }
            }elseif($row->status == 3){
                $string ='-';
            }elseif($row->status == 4){
                $string = status($row->status);
            }elseif($row->status == 5){
                $string = status($row->status);
            }elseif($row->status == 6){
                $string = status($row->status);
            }elseif($row->status == 7){
                $string = status($row->status);
            }elseif($row->status == 8){
                $string = status($row->status);
            }elseif($row->status == 9){
                $string = status($row->status);
            }elseif($row->status == 10){
                if($row->kaji_ulang->bukti){
                    $string = "Ditolak";
                }else{
                    $string = "Menunggu Surat Penawaran";
                }
                $string = status($row->status);
            }else{
                $string ='-';
            }

            $tracking = [
                'no_permintaan'  => $row->no_order,
                'layanan'  => $row->pelayanan->nama,
                'perusahaan'  => $row->user->pelanggans->perusahaan->nama,
                'pic'  => $row->user->pelanggans->nama_lengkap,
                'status'  => str_replace('translator.','',$string),
            ];
        }else{
            return response([
                'status' => 'error'
            ],500);
        }

        return response([
            'data' => $tracking
        ]);
    }

    public function riwayat(Request $request)
    {
        $tracking = [];
        $data = PendaftaranPengujian::where('no_order',$request->no_order)->where('tipe_surat', 0)->first();
        if($data){
            if(isset($data->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian)){
                $detail = $data->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->detailpengujian->pluck('id');
                if($detail){
                    $dispo = ActPengujian::whereIn('parent_id', $detail)->orWhere('parent_id', $data->kaji_ulang->pp->id)->orderBy('created_at','desc')->get();
                }else{
                    $dispo = ActPengujian::where('parent_id', $data->kaji_ulang->pp->id)->orderBy('created_at','desc')->get();
                }
                foreach ($dispo as $key => $cek) {
                    $text = '';
                    switch ($cek->tipe) {
                        case 1:
                            if($cek->jenis==1){
                                $text = $cek->keterangan;
                            }else{
                                if($cek->jenis==2){
                                    $text = $cek->keterangan;
                                }
                            }
                            break;
                        case 2:
                            if($cek->jenis==1){
                                $text = $cek->keterangan;
                            }else{
                                if($cek->jenis==2){
                                    $text = $cek->keterangan;
                                }
                            }
                            break;
                    }
                    $tracking[] = [
                        'time'      => $cek->created_at->diffForHumans(),
                        'message'   => strip_tags($text)
                    ];
                }
            }

            if($data->act_dispo()->count()>0){
                $dispo = $data->act_dispo->sortByDesc('created_at');
                $prev = $data->logsyan()->first(); 
                foreach ($dispo as $key => $cek) {
                    $Date1  = $cek->created_at;
                    $prev   = $cek->prev();
                    $text = '';
                    switch ($cek->tipe) {
                        case 0:
                            $text = '<a> '.$cek->pengirim->nama .' </a> mengubah data Layanan, Lingkup dan Jenis Pengujian';
                            break;
                        case 1:
                            $text = '<a> '. $cek->pengirim->nama .' </a>  Mengalihkan surat <a> '. $data->no_order .'</a> kepada <a> '. $cek->penerima->nama .' </a> '.(($cek->keterangan) ? '<i>dengan catatan : '. $cek->keterangan .' </i>' : '');
                            break;
                        case 2:
                            $text = '<a> '. $cek->pengirim->nama .' </a>  Mendisposisikan surat <a> '. $data->no_order .' </a> kepada <a> '. $cek->penerima->nama .' </a> '. (($cek->keterangan) ? '<i>dengan catatan : '.  $cek->keterangan .'</i>' : '' ) .'
                                    , dalam waktu &nbsp;&nbsp; '. timestampReturn($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name).' ';
                            break;
                        case 3:
                            $status_kaji = '';
                            if($cek->status_kaji_ulang == 1){
                                $status_kaji = 'Diterima';
                            }elseif($cek->status_kaji_ulang == 2){
                                $status_kaji = 'Ditolak';
                            }elseif($cek->status_kaji_ulang == 3){
                                $status_kaji = 'Didiskusikan';
                            }
                            $text = '<a> '. $cek->pengirim->nama .' </a> menyelesaikan <a>Kaji Ulang</a> dengan status <a> '.$status_kaji.' </a>, dalam waktu &nbsp;&nbsp; '. timestampReturn($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name);
                            break;
                        case 4:
                            $text = '<a> '. $cek->pengirim->nama .' </a> menyelesaikan <a>Surat Penawaran,</a> No Order <a> '. $data->no_order .' </a> '.(($cek->status_surat_penawaran > 0) ? ' dengan <a> Adendum '. $cek->status_surat_penawaran.' </a>' : '');
                            break;
                        case 5:
	                        if($data->user->pelanggans->perusahaan->kategori == 0){
	                   			$text = '<a> '. $cek->pengirim->nama .'</a> mengirim email ke <a> '.($data->user->pelanggans->perusahaan->nama) .' </a> dengan No Order <a> '. $data->no_order .' </a>';
	                    	}else{
		                        $text = '<a> '. $cek->pengirim->nama .' </a> menyelesaikan <a> Virtual Account,</a> No Order <a> '. $data->no_order .' </a> '.(($cek->status_surat_penawaran > 0) ? ' dengan <a> Adendum '. $cek->status_surat_penawaran .' </a>' : '').' dan mengirim email ke <a> '.($data->user->pelanggans->perusahaan->nama) .' </a>';
	                    	}
                            break;
                        case 6:
                            if($cek->jenis == 5){
                                $text = '<a> '. $cek->pengirim->nama .' </a>  Mendisposisikan surat <a> '. $data->no_order .' </a> kepada <a> '. $cek->penerima->nama .' </a> untuk dilakukan tahap <a>Verifikasi</a> ';
                            }else{
                                $text = '<a> '. $cek->pengirim->nama .' </a>  Mendisposisikan surat <a> '. $data->no_order .' </a> kepada <a> '. $cek->penerima->nama .' </a> untuk dilakukan tahap <a>Pengujian</a> ';
                            }
                            break;
                        case 7:
                            if($cek->jenis == 6){
                    			$text = $cek->keterangan;
	                    	}else{
		                        $text = '<a> '. $cek->pengirim->nama .' </a> '.$cek->keterangan;
	                    	}
	                        break;
                        case 8:
                            $text = '<a> '. $cek->pengirim->nama .' </a> '.$cek->keterangan;
                            break;
                        case 9:
                            $text = '<a> '. $cek->pengirim->nama .' </a> '.$cek->keterangan;
                            break;
                    }
                    if($cek->tipe != 10 AND $cek->tipe != 11){
                        $tracking[] = [
                            'time'      => $cek->created_at->diffForHumans(),
                            'message'   => strip_tags($text)
                        ];
                    }
                }
                $tracking[] = [
                    'time'      => $data->created_at->diffForHumans(),
                    'message'   => 'Penerimaan Surat'
                ];
            }
        }else{
            return response([
                'status' => 'error'
            ],500);
        }
        return response([
            'data' => $tracking
        ]);
    }
}
