<?php

namespace App\Http\Controllers\API\PengembalianDana;

/* Base App */
use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;
use App\Http\Controllers\Controller;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\VirtualAccount\KonfirmasiPembayaran;
use App\Models\VirtualAccount\RealisasiBiaya;
use App\Models\VirtualAccount\PengembalianDana;
use App\Models\KajiUlang\KajiUlang;
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\KajiUlang\Detail;
use App\Models\CloseOrder\CloseOrder;

/* Validation */
// use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\VirtualAccount\PengembalianDanaRequest;
/* Models */
use App\Models\Authentication\Role;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Zipper;

class PengembalianDanaController extends Controller
{
    public function grid(Request $request)
    {
        if(auth()->user()->hasRole(['keuangan']) || auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi'])){
            $data = VirtualAccount::with('surat.kaji_ulang.pp','surat.kaji_ulang.pp.user.pelanggans.perusahaan','surat.kaji_ulang.pp.pelayanan','surat.kaji_ulang.pp.lingkup','surat.kaji_ulang.pp.detail.jenis','surat.kaji_ulang.surat.detail','surat.kaji_ulang.detail.lokasi','surat.kaji_ulang.detail.mata_uji','surat.kaji_ulang.detail.detail_pendaftaran','surat.files')
                    ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                   ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                   ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                    ->where('trans_va.status', 3)
                      ->whereHas('konfirmasi',function($q) {
                          $q->where('status',2);
                      })
                      ->where('trans_va.tipe', 0)
                      ->select('trans_va.*');

            if ($no_va = $request->no_va) {
                $data->where('no_va','like','%'.$no_va.'%');
            }

            if ($no_surat = $request->no_surat) {
                $data->whereHas('surat', function($surat) use ($no_surat){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                        $kaji->whereHas('pp', function($pp) use ($no_surat){
                            $pp->where('no_surat','like', '%'.$no_surat.'%');
                        });
                    });
                });
            }
            if ($no_order = $request->no_order) {
                $data->whereHas('surat', function($surat) use ($no_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                        $kaji->whereHas('pp', function($pp) use ($no_order){
                            $pp->where('no_order','like', '%'.$no_order.'%');
                        });
                    });
                });
            }
            if ($tanggal_order = $request->tanggal_order) {
                $data->whereHas('surat', function($surat) use ($tanggal_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                        $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                        });
                    });
                });
            }
            if ($wbs_io = $request->wbs_io) {
                $data->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                    $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
                });
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                $data->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                        $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                            $pp->where('layanan_id',$jenis_pelayanan_id);
                        });
                    });
                });
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                $data->whereHas('surat', function($surat) use ($perusahaan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                        $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                            $pp->whereHas('user', function($user) use ($perusahaan_id){
                                $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                                });
                            });
                        });
                    });
                });
            }
            if($sort = $request->sort){
                if($sort == 'order'){
                  $data->orderBy('trans_pp.tgl_order','asc');
                }
                if($sort == 'surat'){
                  $data->orderBy('trans_pp.tgl_surat','asc');
                }
              }else{
                  $data->orderBy('trans_va.created_at','desc');
              }
          }else{
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
          }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

            return response()->json($page);
    }

    public function histori(Request $request)
    {
        if(auth()->user()->hasRole(['keuangan']) || auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi'])){
            $data = VirtualAccount::with('surat.kaji_ulang.pp','surat.kaji_ulang.pp.user.pelanggans.perusahaan','surat.kaji_ulang.pp.pelayanan','surat.kaji_ulang.pp.lingkup','surat.kaji_ulang.pp.detail.jenis','surat.kaji_ulang.surat.detail','surat.kaji_ulang.detail.lokasi','surat.kaji_ulang.detail.mata_uji','surat.kaji_ulang.detail.detail_pendaftaran','surat.files','konfirmasi.pengembalian')
                                ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                               ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                               ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                ->where('trans_va.status', 3)
                                  ->whereHas('konfirmasi',function($q) {
                                      $q->whereHas('pengembalian',function($x) {
                                              $x->whereNotNull('tgl_kembali');
                                          });
                                  })
                                  ->where('trans_va.tipe', 0)
                                  ->select('trans_va.*');

            if ($no_va = $request->no_va) {
                $data->where('no_va','like','%'.$no_va.'%');
            }

            if ($no_surat = $request->no_surat) {
                $data->whereHas('surat', function($surat) use ($no_surat){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                        $kaji->whereHas('pp', function($pp) use ($no_surat){
                            $pp->where('no_surat','like', '%'.$no_surat.'%');
                        });
                    });
                });
            }
            if ($no_order = $request->no_order) {
                $data->whereHas('surat', function($surat) use ($no_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                        $kaji->whereHas('pp', function($pp) use ($no_order){
                            $pp->where('no_order','like', '%'.$no_order.'%');
                        });
                    });
                });
            }
            if ($tanggal_order = $request->tanggal_order) {
                $data->whereHas('surat', function($surat) use ($tanggal_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                        $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                        });
                    });
                });
            }
            if ($wbs_io = $request->wbs_io) {
                $data->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                    $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
                });
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                $data->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                        $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                            $pp->where('layanan_id',$jenis_pelayanan_id);
                        });
                    });
                });
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                $data->whereHas('surat', function($surat) use ($perusahaan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                        $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                            $pp->whereHas('user', function($user) use ($perusahaan_id){
                                $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                                });
                            });
                        });
                    });
                });
            }
            if($sort = $request->sort){
                if($sort == 'order'){
                  $data->orderBy('trans_pp.tgl_order','asc');
                }
                if($sort == 'surat'){
                  $data->orderBy('trans_pp.tgl_surat','asc');
                }
              }else{
                  $data->orderBy('trans_va.created_at','desc');
              }
          }else{
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
          }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

            return response()->json($page);
    }

    public function dana(Request $request)
    {
        if(auth()->user()->hasRole(['keuangan']) || auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi'])){
            $data = CloseOrder::with('penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.detail.jenis','penerimaan.konfirmasi.va.surat.kaji_ulang.surat.detail','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran')
                        ->join('trans_penerimaan_barang','trans_penerimaan_barang.id','=','trans_close_order.penerimaan_id')
                        ->join('trans_konfirmasi_pembayaran','trans_konfirmasi_pembayaran.id','=','trans_penerimaan_barang.konfirmasi_id')
                        ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                        ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                       ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                       ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                        ->whereHas('penerimaan.pengujian.detailpengujian', function($u){
                          return $u->where('verifikasi', '!=', 1);
                        })
                        ->doesntHave('pengembaliandana')
                        ->select('trans_close_order.*');

            if ($no_va = $request->no_va) {
                $data->whereHas('penerimaan', function($penerimaan) use($no_va){
                    $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($no_va){
                        $konfirmasi->whereHas('va', function($va) use ($no_va){
                            $va->where('no_va','like','%'.$no_va.'%');
                        });
                    });
                });
            }

            if ($no_surat = $request->no_surat) {
                $data->whereHas('penerimaan', function($penerimaan) use($no_surat){
                    $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($no_surat){
                        $konfirmasi->whereHas('va', function($va) use ($no_surat){
                            $va->whereHas('surat', function($surat) use ($no_surat){
                                $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                                    $kaji->whereHas('pp', function($pp) use ($no_surat){
                                        $pp->where('no_surat','like', '%'.$no_surat.'%');
                                    });
                                });
                            });
                        });
                    });
                });
            }
            if ($no_order = $request->no_order) {
                $data->whereHas('penerimaan', function($penerimaan) use($no_order){
                    $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($no_order){
                        $konfirmasi->whereHas('va', function($va) use ($no_order){
                            $va->whereHas('surat', function($surat) use ($no_order){
                                $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                                    $kaji->whereHas('pp', function($pp) use ($no_order){
                                        $pp->where('no_order','like', '%'.$no_order.'%');
                                    });
                                });
                            });
                        });
                    });
                });
            }
            if ($tanggal_order = $request->tanggal_order) {
                $data->whereHas('penerimaan', function($penerimaan) use($tanggal_order){
                    $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($tanggal_order){
                        $konfirmasi->whereHas('va', function($va) use ($tanggal_order){
                            $va->whereHas('surat', function($surat) use ($tanggal_order){
                                $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                                    $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                    });
                                });
                            });
                        });
                    });
                });
            }
            if ($wbs_io = $request->wbs_io) {
                $data->whereHas('penerimaan', function($penerimaan) use($wbs_io){
                    $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                        $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
                    });
                });
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                $data->whereHas('penerimaan', function($penerimaan) use($jenis_pelayanan_id){
                    $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($jenis_pelayanan_id){
                        $konfirmasi->whereHas('va', function($va) use ($jenis_pelayanan_id){
                            $va->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                                $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                                    $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                                        $pp->where('layanan_id',$jenis_pelayanan_id);
                                    });
                                });
                            });
                        });
                    });
                });
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                $data->whereHas('penerimaan', function($penerimaan) use($perusahaan_id){
                    $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($perusahaan_id){
                        $konfirmasi->whereHas('va', function($va) use ($perusahaan_id){
                            $va->whereHas('surat', function($surat) use ($perusahaan_id){
                                $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                                    $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                                        $pp->whereHas('user', function($user) use ($perusahaan_id){
                                            $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                                $pelanggan->where('perusahaan_id',$perusahaan_id);
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            }
            if($sort = $request->sort){
                if($sort == 'order'){
                  $data->orderBy('trans_pp.tgl_order','asc');
                }
                if($sort == 'surat'){
                  $data->orderBy('trans_pp.tgl_surat','asc');
                }
              }else{
                  $data->orderBy('trans_close_order.created_at','desc');
              }
          }else{
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
          }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

            return response()->json($page);
    }

    public function danaHistori(Request $request)
    {
        if(auth()->user()->hasRole(['keuangan']) || auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi'])){
            $data = CloseOrder::with('penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.detail.jenis','penerimaan.konfirmasi.va.surat.kaji_ulang.surat.detail','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran')
                        ->join('trans_penerimaan_barang','trans_penerimaan_barang.id','=','trans_close_order.penerimaan_id')
                        ->join('trans_konfirmasi_pembayaran','trans_konfirmasi_pembayaran.id','=','trans_penerimaan_barang.konfirmasi_id')
                        ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                        ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                       ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                       ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                      ->whereHas('penerimaan.pengujian.detailpengujian', function($u){
                        return $u->where('verifikasi', '!=', 1);
                      })
                      ->has('pengembaliandana')
                      ->select('trans_close_order.*');

            if ($no_va = $request->no_va) {
                $data->whereHas('penerimaan', function($penerimaan) use($no_va){
                    $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($no_va){
                        $konfirmasi->whereHas('va', function($va) use ($no_va){
                            $va->where('no_va','like','%'.$no_va.'%');
                        });
                    });
                });
            }

            if ($no_surat = $request->no_surat) {
                $data->whereHas('penerimaan', function($penerimaan) use($no_surat){
                    $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($no_surat){
                        $konfirmasi->whereHas('va', function($va) use ($no_surat){
                            $va->whereHas('surat', function($surat) use ($no_surat){
                                $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                                    $kaji->whereHas('pp', function($pp) use ($no_surat){
                                        $pp->where('no_surat','like', '%'.$no_surat.'%');
                                    });
                                });
                            });
                        });
                    });
                });
            }
            if ($no_order = $request->no_order) {
                $data->whereHas('penerimaan', function($penerimaan) use($no_order){
                    $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($no_order){
                        $konfirmasi->whereHas('va', function($va) use ($no_order){
                            $va->whereHas('surat', function($surat) use ($no_order){
                                $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                                    $kaji->whereHas('pp', function($pp) use ($no_order){
                                        $pp->where('no_order','like', '%'.$no_order.'%');
                                    });
                                });
                            });
                        });
                    });
                });
            }
            if ($tanggal_order = $request->tanggal_order) {
                $data->whereHas('penerimaan', function($penerimaan) use($tanggal_order){
                    $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($tanggal_order){
                        $konfirmasi->whereHas('va', function($va) use ($tanggal_order){
                            $va->whereHas('surat', function($surat) use ($tanggal_order){
                                $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                                    $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                                    });
                                });
                            });
                        });
                    });
                });
            }
            if ($wbs_io = $request->wbs_io) {
                $data->whereHas('penerimaan', function($penerimaan) use($wbs_io){
                    $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                        $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%');
                    });
                });
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                $data->whereHas('penerimaan', function($penerimaan) use($jenis_pelayanan_id){
                    $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($jenis_pelayanan_id){
                        $konfirmasi->whereHas('va', function($va) use ($jenis_pelayanan_id){
                            $va->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                                $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                                    $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                                        $pp->where('layanan_id',$jenis_pelayanan_id);
                                    });
                                });
                            });
                        });
                    });
                });
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                $data->whereHas('penerimaan', function($penerimaan) use($perusahaan_id){
                    $penerimaan->whereHas('konfirmasi', function($konfirmasi) use ($perusahaan_id){
                        $konfirmasi->whereHas('va', function($va) use ($perusahaan_id){
                            $va->whereHas('surat', function($surat) use ($perusahaan_id){
                                $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                                    $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                                        $pp->whereHas('user', function($user) use ($perusahaan_id){
                                            $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                                $pelanggan->where('perusahaan_id',$perusahaan_id);
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            }
            if($sort = $request->sort){
                if($sort == 'order'){
                  $data->orderBy('trans_pp.tgl_order','asc');
                }
                if($sort == 'surat'){
                  $data->orderBy('trans_pp.tgl_surat','asc');
                }
              }else{
                  $data->orderBy('trans_close_order.created_at','desc');
              }
          }else{
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
          }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

            return response()->json($page);
    }

    public function download($id)
    {
      $daftar = PendaftaranPengujian::find($id);
      if(file_exists(public_path('storage/'.$daftar->bukti)))
      {
        return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
      }
      return response([
        'status' => 'error'
      ],500);
    }
}
