<?php

namespace App\Http\Controllers\API\Virtual;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Unlu\Laravel\Api\QueryBuilder;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\VirtualAccount\VirtualAccountRequest;
/* Models */
use App\Models\Authentication\Role;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\KajiUlang\KajiUlang;
use App\Models\KajiUlang\Detail;
use App\Models\Act\ActDisposisi;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Excel;
use Auth;
use DB;

class VirtualAccountController extends Controller
{
      public function index(Request $request)
      {
            $data = new QueryBuilder(new VirtualAccount, $request);
            if($page=$request->page){
                  return $data->build()->paginate();
            }

            return response()->json([
                  'status' => true,
                  'data' => $data->build()->get()
            ]);
      }
      public function grid(Request $request)
      {
          if(auth()->user()->hasRole(['keuangan']) || auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi'])){
            $data = VirtualAccount::with('surat.kaji_ulang.pp','surat.kaji_ulang.pp.user.pelanggans.perusahaan','surat.kaji_ulang.pp.pelayanan','surat.kaji_ulang.pp.lingkup','surat.kaji_ulang.pp.detail.jenis','surat.kaji_ulang.surat.detail','surat.kaji_ulang.detail.lokasi','surat.kaji_ulang.detail.mata_uji','surat.kaji_ulang.detail.detail_pendaftaran','surat.files')
                                     ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                     ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                     ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                     ->whereNull('trans_va.download_date')
                                     ->where('trans_va.tipe_customer', '!=', 0)
                                     ->where('trans_va.tipe', 0)
                                     ->where('trans_va.status', 0)
                                     ->select('*');

            if ($no_va = $request->no_va) {
                $data->where('no_va','like','%'.$no_va.'%');
            }

            if ($no_surat = $request->no_surat) {
                $data->whereHas('surat', function($surat) use ($no_surat){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                        $kaji->whereHas('pp', function($pp) use ($no_surat){
                            $pp->where('no_surat','like', '%'.$no_surat.'%');
                        });
                    });
                });
            }
            if ($no_order = $request->no_order) {
                $data->whereHas('surat', function($surat) use ($no_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                        $kaji->whereHas('pp', function($pp) use ($no_order){
                            $pp->where('no_order','like', '%'.$no_order.'%');
                        });
                    });
                });
            }
            if ($tanggal_order = $request->tanggal_order) {
                $data->whereHas('surat', function($surat) use ($tanggal_order){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                        $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                            $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                        });
                    });
                });
            }
            if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                $data->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                        $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                            $pp->where('layanan_id',$jenis_pelayanan_id);
                        });
                    });
                });
            }
            if ($perusahaan_id = $request->perusahaan_id) {
                $data->whereHas('surat', function($surat) use ($perusahaan_id){
                    $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                        $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                            $pp->whereHas('user', function($user) use ($perusahaan_id){
                                $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                    $pelanggan->where('perusahaan_id',$perusahaan_id);
                                });
                            });
                        });
                    });
                });
            }
            if($sort = $request->sort){
                if($sort == 'order'){
                  $data->orderBy('trans_pp.tgl_order','asc');
                }
                if($sort == 'surat'){
                  $data->orderBy('trans_pp.tgl_surat','asc');
                }
            }else{
                  $data->orderBy('trans_va.created_at','desc');
            }
          }else{
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
          }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

            return response()->json($page);
      }
      
      public function menunggu(Request $request)
      {
            if(auth()->user()->hasRole(['keuangan']) || auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi'])){
              $data = VirtualAccount::with('surat.kaji_ulang.pp','surat.kaji_ulang.pp.user.pelanggans.perusahaan','surat.kaji_ulang.pp.pelayanan','surat.kaji_ulang.pp.lingkup','surat.kaji_ulang.pp.detail.jenis','surat.kaji_ulang.surat.detail','surat.kaji_ulang.detail.lokasi','surat.kaji_ulang.detail.mata_uji','surat.kaji_ulang.detail.detail_pendaftaran','surat.files')
                                  ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                     ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                     ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                     ->whereNotNull('trans_va.download_by')
                                     ->where('trans_va.tipe_customer', '!=', 0)
                                     ->where('trans_va.tipe', 0)
                                     ->where('trans_va.status', 0)
                                  ->select('*');


              if ($no_va = $request->no_va) {
                  $data->where('no_va','like','%'.$no_va.'%');
              }

              if ($no_surat = $request->no_surat) {
                  $data->whereHas('surat', function($surat) use ($no_surat){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                          $kaji->whereHas('pp', function($pp) use ($no_surat){
                              $pp->where('no_surat','like', '%'.$no_surat.'%');
                          });
                      });
                  });
              }
              if ($no_order = $request->no_order) {
                  $data->whereHas('surat', function($surat) use ($no_order){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                          $kaji->whereHas('pp', function($pp) use ($no_order){
                              $pp->where('no_order','like', '%'.$no_order.'%');
                          });
                      });
                  });
              }
              if ($tanggal_order = $request->tanggal_order) {
                  $data->whereHas('surat', function($surat) use ($tanggal_order){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                          $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                              $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                          });
                      });
                  });
              }
              if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $data->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                          $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                              $pp->where('layanan_id',$jenis_pelayanan_id);
                          });
                      });
                  });
              }
              if ($perusahaan_id = $request->perusahaan_id) {
                  $data->whereHas('surat', function($surat) use ($perusahaan_id){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                          $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                              $pp->whereHas('user', function($user) use ($perusahaan_id){
                                  $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                      $pelanggan->where('perusahaan_id',$perusahaan_id);
                                  });
                              });
                          });
                      });
                  });
              }
              if($sort = $request->sort){
                  if($sort == 'order'){
                    $data->orderBy('trans_pp.tgl_order','asc');
                  }
                  if($sort == 'surat'){
                    $data->orderBy('trans_pp.tgl_surat','asc');
                  }
              }else{
                    $data->orderBy('trans_va.created_at','desc');
              }
            }else{
              return response()->json([
                  'status' => false,
                  'message' => 'page not found'
              ],404);
            }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

            return response()->json($page);
      }

      public function submit(Request $request)
      {
            if(auth()->user()->hasRole(['keuangan']) || auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi'])){
              $data = VirtualAccount::with('surat.kaji_ulang.pp','surat.kaji_ulang.pp.user.pelanggans.perusahaan','surat.kaji_ulang.pp.pelayanan','surat.kaji_ulang.pp.lingkup','surat.kaji_ulang.pp.detail.jenis','surat.kaji_ulang.surat.detail','surat.kaji_ulang.detail.lokasi','surat.kaji_ulang.detail.mata_uji','surat.kaji_ulang.detail.detail_pendaftaran','surat.files')
                                      ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                     ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                     ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                     ->where('trans_va.tipe_customer', '!=', 0)
                                     ->where('trans_va.tipe', 0)
                                     ->where('trans_va.status', 1)
                                      ->select('*');


              if ($no_va = $request->no_va) {
                  $data->where('no_va','like','%'.$no_va.'%');
              }

              if ($no_surat = $request->no_surat) {
                  $data->whereHas('surat', function($surat) use ($no_surat){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                          $kaji->whereHas('pp', function($pp) use ($no_surat){
                              $pp->where('no_surat','like', '%'.$no_surat.'%');
                          });
                      });
                  });
              }
              if ($no_order = $request->no_order) {
                  $data->whereHas('surat', function($surat) use ($no_order){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                          $kaji->whereHas('pp', function($pp) use ($no_order){
                              $pp->where('no_order','like', '%'.$no_order.'%');
                          });
                      });
                  });
              }
              if ($tanggal_order = $request->tanggal_order) {
                  $data->whereHas('surat', function($surat) use ($tanggal_order){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                          $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                              $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                          });
                      });
                  });
              }
              if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $data->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                          $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                              $pp->where('layanan_id',$jenis_pelayanan_id);
                          });
                      });
                  });
              }
              if ($perusahaan_id = $request->perusahaan_id) {
                  $data->whereHas('surat', function($surat) use ($perusahaan_id){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                          $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                              $pp->whereHas('user', function($user) use ($perusahaan_id){
                                  $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                      $pelanggan->where('perusahaan_id',$perusahaan_id);
                                  });
                              });
                          });
                      });
                  });
              }
              if($sort = $request->sort){
                  if($sort == 'order'){
                    $data->orderBy('trans_pp.tgl_order','asc');
                  }
                  if($sort == 'surat'){
                    $data->orderBy('trans_pp.tgl_surat','asc');
                  }
              }else{
                    $data->orderBy('trans_va.created_at','desc');
              }
            }else{
              return response()->json([
                  'status' => false,
                  'message' => 'page not found'
              ],404);
            }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

            return response()->json($page);
      }

      public function nonaktif(Request $request)
      {
            if(auth()->user()->hasRole(['keuangan']) || auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi'])){
              $data = VirtualAccount::with('surat.kaji_ulang.pp','surat.kaji_ulang.pp.user.pelanggans.perusahaan','surat.kaji_ulang.pp.pelayanan','surat.kaji_ulang.pp.lingkup','surat.kaji_ulang.pp.detail.jenis','surat.kaji_ulang.surat.detail','surat.kaji_ulang.detail.lokasi','surat.kaji_ulang.detail.mata_uji','surat.kaji_ulang.detail.detail_pendaftaran','surat.files')
                                      ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                     ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                     ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                     ->where('trans_va.tipe_customer', '!=', 0)
                                     ->where('trans_va.tipe', 0)
                                     ->whereIn('trans_va.status', [2,3])
                                      ->select('trans_va.*');


              if ($no_va = $request->no_va) {
                  $data->where('no_va','like','%'.$no_va.'%');
              }

              if ($no_surat = $request->no_surat) {
                  $data->whereHas('surat', function($surat) use ($no_surat){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                          $kaji->whereHas('pp', function($pp) use ($no_surat){
                              $pp->where('no_surat','like', '%'.$no_surat.'%');
                          });
                      });
                  });
              }
              if ($no_order = $request->no_order) {
                  $data->whereHas('surat', function($surat) use ($no_order){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                          $kaji->whereHas('pp', function($pp) use ($no_order){
                              $pp->where('no_order','like', '%'.$no_order.'%');
                          });
                      });
                  });
              }
              if ($tanggal_order = $request->tanggal_order) {
                  $data->whereHas('surat', function($surat) use ($tanggal_order){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                          $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                              $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                          });
                      });
                  });
              }
              if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $data->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                          $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                              $pp->where('layanan_id',$jenis_pelayanan_id);
                          });
                      });
                  });
              }
              if ($perusahaan_id = $request->perusahaan_id) {
                  $data->whereHas('surat', function($surat) use ($perusahaan_id){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                          $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                              $pp->whereHas('user', function($user) use ($perusahaan_id){
                                  $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                      $pelanggan->where('perusahaan_id',$perusahaan_id);
                                  });
                              });
                          });
                      });
                  });
              }
              if($sort = $request->sort){
                  if($sort == 'order'){
                    $data->orderBy('trans_pp.tgl_order','asc');
                  }
                  if($sort == 'surat'){
                    $data->orderBy('trans_pp.tgl_surat','asc');
                  }
              }else{
                    $data->orderBy('trans_va.created_at','desc');
              }
            }else{
              return response()->json([
                  'status' => false,
                  'message' => 'page not found'
              ],404);
            }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

            return response()->json($page);
      }
      public function download(Request $request)
      {
            if(auth()->user()->hasRole(['keuangan']) || auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi'])){
              $data = VirtualAccount::with('surat.kaji_ulang.pp','surat.kaji_ulang.pp.user.pelanggans.perusahaan','surat.kaji_ulang.pp.pelayanan','surat.kaji_ulang.pp.lingkup','surat.kaji_ulang.pp.detail.jenis','surat.kaji_ulang.surat.detail','surat.kaji_ulang.detail.lokasi','surat.kaji_ulang.detail.mata_uji','surat.kaji_ulang.detail.detail_pendaftaran','surat.files',
                'groups.surat.kaji_ulang.pp','groups.surat.kaji_ulang.pp.user.pelanggans.perusahaan','groups.surat.kaji_ulang.pp.pelayanan','groups.surat.kaji_ulang.pp.lingkup','groups.surat.kaji_ulang.pp.detail.jenis','groups.surat.kaji_ulang.surat.detail','groups.surat.kaji_ulang.detail.lokasi','groups.surat.kaji_ulang.detail.mata_uji','groups.surat.kaji_ulang.detail.detail_pendaftaran','groups.surat.files')
                                      ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                     ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                     ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                     ->where('trans_va.tipe_customer', '!=', 0)
                                     ->where('trans_va.tipe', 0)
                                     ->whereNotNull('trans_va.download_by')
                                     ->whereNull('trans_va.parent_id')
                                      ->select('*');

              if ($no_va = $request->no_va) {
                  $data->where('no_va','like','%'.$no_va.'%')
                          ->orWhereHas('groups', function($query) use ($no_va) {
                              $query->where('no_va','like','%'.$no_va.'%');
                          });
              }

              if ($no_surat = $request->no_surat) {
                  $data->whereHas('surat', function($surat) use ($no_surat){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                          $kaji->whereHas('pp', function($pp) use ($no_surat){
                              $pp->where('no_surat','like', '%'.$no_surat.'%');
                          });
                      });
                  })->orWhereHas('groups', function($group) use ($no_surat){
                      $group->whereHas('surat', function($surat) use ($no_surat){
                          $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                              $kaji->whereHas('pp', function($pp) use ($no_surat){
                                  $pp->where('no_surat','like', '%'.$no_surat.'%');
                              });
                          });
                      });
                  });
              }
              if ($no_order = $request->no_order) {
                  $data->whereHas('surat', function($surat) use ($no_order){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                          $kaji->whereHas('pp', function($pp) use ($no_order){
                              $pp->where('no_order','like', '%'.$no_order.'%');
                          });
                      });
                  })->orWhereHas('groups', function($group) use ($no_order){
                      $group->whereHas('surat', function($surat) use ($no_order){
                          $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                              $kaji->whereHas('pp', function($pp) use ($no_order){
                                  $pp->where('no_order','like', '%'.$no_order.'%');
                              });
                          });
                      });
                  });
              }
              if ($tanggal_order = $request->tanggal_order) {
                  $data->whereHas('surat', function($surat) use ($tanggal_order){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                          $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                              $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                          });
                      });
                  })->orWhereHas('groups', function($group) use ($tanggal_order){
                      $group->whereHas('surat', function($surat) use ($tanggal_order){
                          $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                              $kaji->whereHas('pp', function($pp) use ($tanggal_order){
                                  $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                              });
                          });
                      });
                  });
              }
              if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
                  $data->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                          $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                              $pp->where('layanan_id',$jenis_pelayanan_id);
                          });
                      });
                  })->orWhereHas('groups', function($group) use ($jenis_pelayanan_id){
                      $group->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                          $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                              $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
                                  $pp->where('layanan_id',$jenis_pelayanan_id);
                              });
                          });
                      });
                  });
              }
              if ($perusahaan_id = $request->perusahaan_id) {
                  $data->whereHas('surat', function($surat) use ($perusahaan_id){
                      $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                          $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                              $pp->whereHas('user', function($user) use ($perusahaan_id){
                                  $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                      $pelanggan->where('perusahaan_id',$perusahaan_id);
                                  });
                              });
                          });
                      });
                  })->orWhereHas('groups', function($group) use ($perusahaan_id){
                      $group->whereHas('surat', function($surat) use ($perusahaan_id){
                          $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                              $kaji->whereHas('pp', function($pp) use ($perusahaan_id){
                                  $pp->whereHas('user', function($user) use ($perusahaan_id){
                                      $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                                      });
                                  });
                              });
                          });
                      });
                  });
              }
              if($sort = $request->sort){
                if($sort == 'order'){
                    $data->orderBy('trans_pp.tgl_order','asc');
                  }
                  if($sort == 'surat'){
                    $data->orderBy('trans_pp.tgl_surat','asc');
                  }
              }else{
                    $data->orderBy('trans_va.created_at','desc');
              }
            }else{
              return response()->json([
                  'status' => false,
                  'message' => 'page not found'
              ],404);
            }
            $page = $data->paginate(10);
            // dd($page[0]);
            if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
            }

            return response()->json($page);
      }

      public function downloadVA($arr)
      {
          $list = explode(',', $arr);
          $records = VirtualAccount::whereIn('id', $list)->where('tipe', 0)->get();

          $format = '89319_'.Carbon::now()->format('Ymd');
          $reports = Excel::create($format, function($excel) use ($format,$records) {
              $excel->sheet($format, function($sheet) use ($records) {
                  $header = ['No Virtual','KEY1','KEY2','CCY','Nama Perusahaan','Nomor Surat Penawaran','Nominal','Nomor VA','Nama Pekerjaan'];
                  for ($i=6; $i <=25 ; $i++) { 
                      $nom = sprintf("%02d", $i);
                      array_push($header,"B_INFO".$nom);
                  }
                  array_push($header,"PERIOD_OPEN","PERIOD_CLOSE");
                  for ($i=1; $i <=25 ; $i++) { 
                      $nom = sprintf("%02d", $i);
                      array_push($header,"SUBBILL_".$nom);
                  }
                  array_push($header,"END_RECORD");

                  $sheet->row(1, $header);
                  $sheet->setWidth(array('B'=>6,'C'=>6,'D'=>5,'E'=>16,'F'=>22,'I'=>16,'J'=>3,'K'=>3,'L'=>3,'M'=>3,'N'=>3,'O'=>3,'P'=>3,'Q'=>3,'R'=>3,'S'=>3,'T'=>3,'U'=>3,'V'=>3,'W'=>3,'X'=>3,'Y'=>3,'Z'=>3,'AA'=>3,'AB'=>3,'AC'=>3));

                  $sheet->setAutoSize(array(
                      'A', 'G','H'
                  ));

                  $sheet->cells('A1', function($cells) {
                      $cells->setBackground('#FFFF00');
                  });
                  $sheet->cells('E1', function($cells) {
                      $cells->setBackground('#FFFF00');
                  });
                  $sheet->cells('AD1:BE1', function($cells) {
                      $cells->setBackground('#FFFF00');
                  });

                  $sheet->setColumnFormat(array(
                      'A' => '0',
                      // 'G' => '0',
                      'H' => '0',
                  ));

                  $row = 2;
                  foreach ($records as $record) {
                      $no_virtual = "'89319".sprintf("%02d",$record->surat->kaji_ulang->pp->pelayanan->id).$record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->kode.Carbon::now()->format('Y').sprintf("%03d",$record->id);

                      $jenis_pengujian = '';
                      if($record->surat->kaji_ulang->pp->detail){
                          foreach ($record->surat->kaji_ulang->pp->detail as $key => $value) {
                              $kaji_ulang = KajiUlang::find($record->surat->kaji_ulang->id);
                              if($kaji_ulang){
                                  foreach ($kaji_ulang->detail as $kuy => $val) {
                                      if($val->jenis_id == $value->id){
                                          $jenis_pengujian .= $value->jenis->nama;
                                      }
                                  }
                              }else{
                                  $jenis_pengujian .= $value->jenis->nama;
                              }
                          }
                      }


                      $isi = [$no_virtual,
                              '',
                              '',
                              'IDR',
                              $record->surat->kaji_ulang->pp->user->pelanggans->perusahaan->nama,
                              str_replace("/",".",$record->surat->no_surat),
                              "'".$record->surat->total,
                              "'".$record->no_va,
                              $jenis_pengujian
                          ];

                      for ($i=6; $i <=25 ; $i++) { 
                          array_push($isi,"");
                      }
                      array_push($isi,Carbon::parse($record->tgl_aktif)->format('Ymd'));
                      array_push($isi,Carbon::parse($record->tgl_kadaluarsa)->format('Ymd'));
                      $string = '01\TOTAL\TOTAL\1';
                      array_push($isi,$string);
                      for ($i=2; $i <=25 ; $i++) { 
                          array_push($isi,'\\\\\\');
                      }
                      array_push($isi,'~');


                      $sheet->row($row, $isi);
                      $sheet->cells('A'.$row, function($cells) {
                          $cells->setBackground('#FFFF00');
                      });

                      $sheet->cells('E'.$row, function($cells) {
                          $cells->setBackground('#FFFF00');
                      });

                      $sheet->cells('AD'.$row.':BE'.$row, function($cells) {
                          $cells->setBackground('#FFFF00');
                      });
                      $row++;
                  }

              });
          });

          //set status downloaded
          $parent = null;
          $count = 0;
          foreach ($records as $key => $data) {
              VirtualAccount::setDownloaded($data->id);
              if($count>0){
                  $va = VirtualAccount::find($data->id);
                  $va->parent_id = $parent;
                  $va->save();
              }else{
                  $parent = $data->id;
              }
              $count++;
          }
          
          $reports->download('xls');
          return response([
              'status' => true,
          ]);
      }
}
