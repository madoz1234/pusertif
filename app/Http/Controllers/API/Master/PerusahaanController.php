<?php

namespace App\Http\Controllers\API\Master;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Unlu\Laravel\Api\QueryBuilder;

/* Validation */

/* Models */
use App\Models\Master\Perusahaan;
// use Spatie\Fractal\Fractal;

/* Libraries */
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;

class PerusahaanController extends Controller
{
      public function index(Request $request)
      {
            $data = Perusahaan::when($id = $request->id, function($q) use ($id) {
                                         $q->where('id',$id);
                                    })
                                    ->when($nama = $request->nama, function($q) use ($nama) {
                                         $q->where('nama', 'like','%'.$nama.'%');
                                    })
                                    ->where('status',1)
                                    ->get();

            return response([
              'data' => $data
            ]);
      }
}
