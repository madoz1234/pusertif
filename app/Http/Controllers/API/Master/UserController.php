<?php

namespace App\Http\Controllers\API\Master;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Unlu\Laravel\Api\QueryBuilder;

/* Validation */

/* Models */
use App\Models\Master\Perusahaan;
use App\Models\Authentication\User;
// use Spatie\Fractal\Fractal;

/* Libraries */
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;

class UserController extends Controller
{
      public function historidata(Request $request)
      {
            $data = User::whereHas('roles', function($a){
                            $a->whereIn('name', ['keuangan','pelaksana-kalibrasi','pelaksana-siskit','pelaksana-sistgi','pelaksana-tegangan-rendah','pelaksana-tegangan-tinggi','yan-kalibrasi','yan-uji','msb-kalibrasi','msb-siskit','msb-sistgi','msb-tegangan-rendah','msb-tegangan-tinggi','adminlab-kalibrasi','adminlab-siskit','adminlab-sistgi','adminlab-tegangan-rendah','adminlab-tegangan-tinggi','asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-rendah','asman-dal-tegangan-tinggi','asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-rendah','asman-lola-tegangan-tinggi']);
                          })->orderBy('id','asc')->get();

            return response([
              'data' => $data
            ]);
      }
}
