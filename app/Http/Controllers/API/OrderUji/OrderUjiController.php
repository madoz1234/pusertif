<?php

namespace App\Http\Controllers\API\OrderUji;

/* Base App */
use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;
use App\Http\Controllers\Controller;

/* Validation */

/* Models */
use App\Models\OrderUji\UjiSerahTerima;
use App\Models\Authentication\Role;
use App\Models\Picture;
use App\Models\Files;


/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Zipper;
use Excel;
use PDF;

class OrderUjiController extends Controller
{
    public function grid(Request $request)
    {

        if(auth()->user()->hasRole(['pelaksana-tegangan-rendah', 'yan-uji', 'msb-tegangan-rendah', 'asman-lola-tegangan-rendah', 'asman-dal-tegangan-rendah', 'adminlab-tegangan-rendah'])){
            $data = UjiSerahTerima::with('pelayanan','lingkup','user','user.pelanggans','user.pelanggans.perusahaan','detail','detail.jenis','detail.satuan')->where('layanan_id', 4)->select('*');
        }elseif(auth()->user()->hasRole(['pelaksana-tegangan-tinggi', 'yan-uji', 'msb-tegangan-tinggi', 'asman-lola-tegangan-tinggi', 'asman-dal-tegangan-tinggi', 'adminlab-tegangan-tinggi'])){
            $data = UjiSerahTerima::with('pelayanan','lingkup','user','user.pelanggans','user.pelanggans.perusahaan','detail','detail.jenis','detail.satuan')->where('layanan_id', 5)->select('*');
        }else{
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }
        
        
        if ($no_order = $request->no_order) {
            $data->where('no_order', 'like','%'.$no_order.'%');
        }

        if ($tanggal_order = $request->tanggal_order) {
            $data->where('tanggal_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
        }

        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $data->where('layanan_id', $jenis_pelayanan_id);
        }

        if ($no_kontrak = $request->no_kontrak) {
            $data->where('no_kontrak', 'like','%'.$no_kontrak.'%');
        }

        if ($wbs_io = $request->wbs_io) {
            $data->where('no_wbs', 'like','%'.$wbs_io.'%');
        }
        if ($perusahaan_id = $request->perusahaan_id) {
          $data->whereHas('user.pelanggans', function($pelanggan) use ($perusahaan_id){
              $pelanggan->where('perusahaan_id',$perusahaan_id);
          });
        }
        if($sort = $request->sort){
            if($sort == 'order'){
              $data->orderBy('tgl_order','asc');
            }
            if($sort == 'surat'){
              $data->orderBy('tgl_kontrak','asc');
            }
          }else{
              $data->orderBy('created_at','desc');
          }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function histori(Request $request)
    {
        if(auth()->user()->hasRole(['pelaksana-tegangan-rendah', 'yan-uji', 'msb-tegangan-rendah', 'asman-lola-tegangan-rendah', 'asman-dal-tegangan-rendah', 'adminlab-tegangan-rendah'])){
            $records = UjiSerahTerima::with('pelayanan','lingkup','user','user.pelanggans','user.pelanggans.perusahaan','detail','detail.jenis','detail.satuan')->where('layanan_id', 4)->whereIn('status', [2,3,4,5,6,7,10])
                                            ->where('created_by', auth()->user()->id)->select('*');
        }elseif(auth()->user()->hasRole(['pelaksana-tegangan-tinggi', 'yan-uji', 'msb-tegangan-tinggi', 'asman-lola-tegangan-tinggi', 'asman-dal-tegangan-tinggi', 'adminlab-tegangan-tinggi'])){
            $records = UjiSerahTerima::with('pelayanan','lingkup','user','user.pelanggans','user.pelanggans.perusahaan','detail','detail.jenis','detail.satuan')->where('layanan_id', 5)->whereIn('status', [2,3,4,5,6,7,10])
                                            ->where('created_by', auth()->user()->id)->select('*');
        }else{
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }
        
        //Init Sort
        if ($no_order = $request->no_order) {
            $data->where('no_order', 'like','%'.$no_order.'%');
        }

        if ($tanggal_order = $request->tanggal_order) {
            $data->where('tanggal_order', Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
        }

        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $data->where('layanan_id', $jenis_pelayanan_id);
        }

        if ($no_kontrak = $request->no_kontrak) {
            $data->where('no_kontrak', 'like','%'.$no_kontrak.'%');
        }

        if ($wbs_io = $request->wbs_io) {
            $data->where('no_wbs', 'like','%'.$wbs_io.'%');
        }
        if ($perusahaan_id = $request->perusahaan_id) {
          $data->whereHas('user.pelanggans', function($pelanggan) use ($perusahaan_id){
              $pelanggan->where('perusahaan_id',$perusahaan_id);
          });
        }
        if($sort = $request->sort){
            if($sort == 'order'){
              $data->orderBy('tgl_order','asc');
            }
            if($sort == 'surat'){
              $data->orderBy('tgl_kontrak','asc');
            }
          }else{
              $data->orderBy('created_at','desc');
          }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function download($id)
    {
        $daftar = UjiSerahTerima::find($id);
        if(file_exists(public_path('storage/'.$daftar->bukti)))
        {
            return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
        }

        return response([
            'status' => 'error'
        ],500);
    }

    public function printBarcodePdf(Request $request, $id){
        $record = UjiSerahTerima::find($id);
        $data = [
            'records' => $record,
        ];
        $pdf = PDF::loadView('modules.pengujian-serah-terima.cetak.cetak-barcode', $data);
        return $pdf->stream('QR Code Order Uji Serah Terima '.$record->tgl_order.'.pdf',array("Attachment" => false));
    }
}
