<?php

namespace App\Http\Controllers\API\NotaDinas;

/* Base App */
use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;
use App\Http\Controllers\Controller;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\KajiUlang\KajiUlang;
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\KajiUlang\Detail;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
/* Models */
use App\Models\Authentication\Role;


/* Libraries */
use DataTables;
use Carbon;
use Hash;


class NotaDinasController extends Controller
{
    public function grid(Request $request)
    {
    	if(auth()->user()->hasRole(['keuangan'])){
	        $data = VirtualAccount::with('surat.kaji_ulang.pp','surat.kaji_ulang.pp.user.pelanggans.perusahaan','surat.kaji_ulang.pp.pelayanan','surat.kaji_ulang.pp.lingkup','surat.kaji_ulang.pp.detail.jenis','surat.kaji_ulang.surat.detail','surat.kaji_ulang.detail.lokasi','surat.kaji_ulang.detail.mata_uji','surat.kaji_ulang.detail.detail_pendaftaran','surat.files')
	                                ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
	                                 ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
	                                 ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
	                                ->whereHas('surat', function($s){
	                                    $s->whereHas('kaji_ulang', function($k){
	                                        $k->whereHas('pp', function($p){
	                                            return $p->whereNotIn('status', [6,7]); // 6:selesai 7:gagal diproses
	                                        });
	                                    });
	                                 })->where('trans_va.tipe_customer', 0)->where('trans_va.tipe', 0)->select('trans_va.*');
	        //Init Sort
	        if ($no_surat = $request->no_surat) {
	            $data->whereHas('surat', function($surat) use ($no_surat){
	                $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
	                    $kaji->whereHas('pp', function($pp) use ($no_surat){
	                        $pp->where('no_surat','like', '%'.$no_surat.'%');
	                    });
	                });
	            });
	        }
	        if ($no_skki = $request->no_skki) {
	            $data->whereHas('surat', function($surat) use ($no_skki){
	                $surat->where('no_skki','like','%'.$no_skki.'%');
	            });
	        }
	        if ($tanggal_skki = $request->tanggal_skki) {
	            $data->whereHas('surat', function($surat) use ($tanggal_skki){
	                $surat->where('tgl_skki',Carbon::createFromFormat('d/m/Y',$tanggal_skki)->format('Y-m-d'));
	            });
	        }
	        if ($no_order = $request->no_order) {
	            $data->whereHas('surat', function($surat) use ($no_order){
	                $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
	                    $kaji->whereHas('pp', function($pp) use ($no_order){
	                        $pp->where('no_order','like', '%'.$no_order.'%');
	                    });
	                });
	            });
	        }
	        if ($tanggal_order = $request->tanggal_order) {
	            $data->whereHas('surat', function($surat) use ($tanggal_order){
	                $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
	                    $kaji->whereHas('pp', function($pp) use ($tanggal_order){
	                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
	                    });
	                });
	            });
	        }
	        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
	            $data->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
	                $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
	                    $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
	                        $pp->where('layanan_id',$jenis_pelayanan_id);
	                    });
	                });
	            });
	        }
	        if ($perusahaan_id = $request->perusahaan_id) {
	          $data->whereHas('surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use ($perusahaan_id){
	              $pelanggan->where('perusahaan_id',$perusahaan_id);
	          });
	        }
	        if($sort = $request->sort){
	          if($sort == 'order'){
	            $data->orderBy('trans_pp.tgl_order','asc');
	          }
	          if($sort == 'surat'){
	            $data->orderBy('trans_pp.tgl_surat','asc');
	          }
	        }else{
	            $data->orderBy('trans_va.created_at','desc');
	        }
	    }else{
	    	return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
	    }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function histori(Request $request)
    {
    	if(auth()->user()->hasRole(['keuangan'])){
	        $data = VirtualAccount::with('surat.kaji_ulang.pp','surat.kaji_ulang.pp.user.pelanggans.perusahaan','surat.kaji_ulang.pp.pelayanan','surat.kaji_ulang.pp.lingkup','surat.kaji_ulang.pp.detail.jenis','surat.kaji_ulang.surat.detail','surat.kaji_ulang.detail.lokasi','surat.kaji_ulang.detail.mata_uji','surat.kaji_ulang.detail.detail_pendaftaran','surat.files')
	                                ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
	                                 ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
	                                 ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
	                                ->whereHas('surat', function($s){
	                                    $s->whereHas('kaji_ulang', function($k){
	                                        $k->whereHas('pp', function($p){
	                                            return $p->whereIn('status', [6,7]); // 6:selesai 7:gagal diproses
	                                        });
	                                    });
	                                 })
	                                ->where('trans_va.tipe_customer', 0)
	                                ->where('trans_va.tipe', 0)
	                                ->select('trans_va.*');
	        
	        //Init Sort
	        if ($no_surat = $request->no_surat) {
	            $data->whereHas('surat', function($surat) use ($no_surat){
	                $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
	                    $kaji->whereHas('pp', function($pp) use ($no_surat){
	                        $pp->where('no_surat','like', '%'.$no_surat.'%');
	                    });
	                });
	            });
	        }
	        if ($no_skki = $request->no_skki) {
	            $data->whereHas('surat', function($surat) use ($no_skki){
	                $surat->where('no_skki','like','%'.$no_skki.'%');
	            });
	        }
	        if ($tanggal_skki = $request->tanggal_skki) {
	            $data->whereHas('surat', function($surat) use ($tanggal_skki){
	                $surat->where('tgl_skki',Carbon::createFromFormat('d/m/Y',$tanggal_skki)->format('Y-m-d'));
	            });
	        }
	        if ($no_order = $request->no_order) {
	            $data->whereHas('surat', function($surat) use ($no_order){
	                $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
	                    $kaji->whereHas('pp', function($pp) use ($no_order){
	                        $pp->where('no_order','like', '%'.$no_order.'%');
	                    });
	                });
	            });
	        }
	        if ($tanggal_order = $request->tanggal_order) {
	            $data->whereHas('surat', function($surat) use ($tanggal_order){
	                $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
	                    $kaji->whereHas('pp', function($pp) use ($tanggal_order){
	                        $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
	                    });
	                });
	            });
	        }
	        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
	            $data->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
	                $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
	                    $kaji->whereHas('pp', function($pp) use ($jenis_pelayanan_id){
	                        $pp->where('layanan_id',$jenis_pelayanan_id);
	                    });
	                });
	            });
	        }
	        if ($perusahaan_id = $request->perusahaan_id) {
	          $data->whereHas('surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use ($perusahaan_id){
	              $pelanggan->where('perusahaan_id',$perusahaan_id);
	          });
	        }
	        if($sort = $request->sort){
	          if($sort == 'order'){
	            $data->orderBy('trans_pp.tgl_order','asc');
	          }
	          if($sort == 'surat'){
	            $data->orderBy('trans_pp.tgl_surat','asc');
	          }
	        }else{
	            $data->orderBy('trans_va.created_at','desc');
	        }
	    }else{
	    	return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
	    }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function download($id)
    {
        $daftar = PendaftaranPengujian::find($id);
        if(file_exists(public_path('storage/'.$daftar->bukti)))
        {
            return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
        }
        return response([
            'status' => 'error'
        ],500);
    }
}
