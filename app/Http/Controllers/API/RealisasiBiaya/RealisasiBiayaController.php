<?php

namespace App\Http\Controllers\API\RealisasiBiaya;

/* Base App */
use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;
use App\Http\Controllers\Controller;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\VirtualAccount\KonfirmasiPembayaran;
use App\Models\VirtualAccount\RealisasiBiaya;
use App\Models\KajiUlang\KajiUlang;
use App\Models\SuratPenawaran\SuratPenawaran;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\KajiUlang\Detail;

/* Validation */
use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\Realisasi\RealisasiRequest;
/* Models */
use App\Models\Authentication\Role;
use App\Models\Picture;
use App\Models\Files;


/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Zipper;
use Excel;

class RealisasiBiayaController extends Controller
{
    public function grid(Request $request)
    {
        if(auth()->user()->hasRole(['keuangan'])){
            $data = KonfirmasiPembayaran::with('va.surat.kaji_ulang.pp','va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','va.surat.kaji_ulang.pp.pelayanan','va.surat.kaji_ulang.pp.lingkup','va.surat.kaji_ulang.pp.detail.jenis','va.surat.kaji_ulang.surat.detail','va.surat.kaji_ulang.detail.lokasi','va.surat.kaji_ulang.detail.mata_uji','va.surat.kaji_ulang.detail.detail_pendaftaran','va.surat.files','realisasi.realisasi_detail')
                                    ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                    ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                   ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                   ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                    ->where('trans_konfirmasi_pembayaran.status', 3)
                                    ->whereHas('penerimaan', function($penerimaan){
                                        $penerimaan->doesnthave('closeorder');
                                    })
                                    ->where('trans_konfirmasi_pembayaran.tipe', 0)
                                    ->orDoesntHave('penerimaan')
                                    ->select('trans_konfirmasi_pembayaran.*');
            

            if($no_order = $request->no_order){
                $data->whereHas('va', function($va) use ($no_order){
                    $va->whereHas('surat', function($surat) use ($no_order){
                        $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                            $kaji->whereHas('pp', function($pp) use($no_order){
                                $pp->where('no_order','like','%'.$no_order.'%');
                            });
                        });
                    });
                });
            }

            if($no_surat = $request->no_surat){
                $data->whereHas('va', function($va) use ($no_surat){
                    $va->whereHas('surat', function($surat) use ($no_surat){
                        $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                            $kaji->whereHas('pp', function($pp) use($no_surat){
                                $pp->where('no_surat','like','%'.$no_surat.'%');
                            });
                        });
                    });
                });
            }

            if($tanggal_order = $request->tanggal_order){
                $data->whereHas('va', function($va) use ($tanggal_order){
                    $va->whereHas('surat', function($surat) use ($tanggal_order){
                        $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                            $kaji->whereHas('pp', function($pp) use($tanggal_order){
                                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                            });
                        });
                    });
                });
            }

            if($jenis_pelayanan_id = $request->jenis_pelayanan_id){
                $data->whereHas('va', function($va) use ($jenis_pelayanan_id){
                    $va->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                        $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                            $kaji->whereHas('pp', function($pp) use($jenis_pelayanan_id){
                                $pp->where('layanan_id',$jenis_pelayanan_id);
                            });
                        });
                    });
                });
            }

            if($perusahaan_id = $request->perusahaan_id){
                $data->whereHas('va', function($va) use ($perusahaan_id){
                    $va->whereHas('surat', function($surat) use ($perusahaan_id){
                        $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                            $kaji->whereHas('pp', function($pp) use($perusahaan_id){
                                $pp->whereHas('user', function($user) use ($perusahaan_id){
                                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                                    });
                                });
                            });
                        });
                    });
                });
            }

            if($wbs_io = $request->wbs_io){
                $data->where("wbs_io",'like','%'.$wbs_io.'%');
            }
            if($sort = $request->sort){
                if($sort == 'order'){
                  $data->orderBy('trans_pp.tgl_order','asc');
                }
                if($sort == 'surat'){
                  $data->orderBy('trans_pp.tgl_surat','asc');
                }
            }else{
              $data->orderBy('trans_konfirmasi_pembayaran.created_at','desc');
            }
        }else{
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function histori(Request $request)
    {
        if(auth()->user()->hasRole(['keuangan'])){
            $data = KonfirmasiPembayaran::with('va.surat.kaji_ulang.pp','va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','va.surat.kaji_ulang.pp.pelayanan','va.surat.kaji_ulang.pp.lingkup','va.surat.kaji_ulang.pp.detail.jenis','va.surat.kaji_ulang.surat.detail','va.surat.kaji_ulang.detail.lokasi','va.surat.kaji_ulang.detail.mata_uji','va.surat.kaji_ulang.detail.detail_pendaftaran','va.surat.files','realisasi.realisasi_detail')
                                            ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
                                            ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
                                           ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
                                           ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
                                            ->where('trans_konfirmasi_pembayaran.status', 3)
                                            ->whereHas('penerimaan',function($penerimaan){
                                                $penerimaan->has('closeorder');
                                            })
                                            ->where('trans_konfirmasi_pembayaran.tipe', 0)
                                            ->select('trans_konfirmasi_pembayaran.*');
            
            //Init Sort
            if($no_order = $request->no_order){
                $data->whereHas('va', function($va) use ($no_order){
                    $va->whereHas('surat', function($surat) use ($no_order){
                        $surat->whereHas('kaji_ulang', function($kaji) use ($no_order){
                            $kaji->whereHas('pp', function($pp) use($no_order){
                                $pp->where('no_order','like','%'.$no_order.'%');
                            });
                        });
                    });
                });
            }

            if($no_surat = $request->no_surat){
                $data->whereHas('va', function($va) use ($no_surat){
                    $va->whereHas('surat', function($surat) use ($no_surat){
                        $surat->whereHas('kaji_ulang', function($kaji) use ($no_surat){
                            $kaji->whereHas('pp', function($pp) use($no_surat){
                                $pp->where('no_surat','like','%'.$no_surat.'%');
                            });
                        });
                    });
                });
            }

            if($tanggal_order = $request->tanggal_order){
                $data->whereHas('va', function($va) use ($tanggal_order){
                    $va->whereHas('surat', function($surat) use ($tanggal_order){
                        $surat->whereHas('kaji_ulang', function($kaji) use ($tanggal_order){
                            $kaji->whereHas('pp', function($pp) use($tanggal_order){
                                $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'));
                            });
                        });
                    });
                });
            }

            if($jenis_pelayanan_id = $request->jenis_pelayanan_id){
                $data->whereHas('va', function($va) use ($jenis_pelayanan_id){
                    $va->whereHas('surat', function($surat) use ($jenis_pelayanan_id){
                        $surat->whereHas('kaji_ulang', function($kaji) use ($jenis_pelayanan_id){
                            $kaji->whereHas('pp', function($pp) use($jenis_pelayanan_id){
                                $pp->where('layanan_id',$jenis_pelayanan_id);
                            });
                        });
                    });
                });
            }

            if($perusahaan_id = $request->perusahaan_id){
                $data->whereHas('va', function($va) use ($perusahaan_id){
                    $va->whereHas('surat', function($surat) use ($perusahaan_id){
                        $surat->whereHas('kaji_ulang', function($kaji) use ($perusahaan_id){
                            $kaji->whereHas('pp', function($pp) use($perusahaan_id){
                                $pp->whereHas('user', function($user) use ($perusahaan_id){
                                    $user->whereHas('pelanggans', function($pelanggan) use ($perusahaan_id){
                                          $pelanggan->where('perusahaan_id',$perusahaan_id);
                                    });
                                });
                            });
                        });
                    });
                });
            }

            if($wbs_io = $request->wbs_io){
                $data->where("wbs_io",'like','%'.$wbs_io.'%');
            }
            if($sort = $request->sort){
                if($sort == 'order'){
                  $data->orderBy('trans_pp.tgl_order','asc');
                }
                if($sort == 'surat'){
                  $data->orderBy('trans_pp.tgl_surat','asc');
                }
            }else{
              $data->orderBy('trans_konfirmasi_pembayaran.created_at','desc');
            }
        }else{
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }
        return response()->json($page);
    }

    public function download($id)
    {
        $daftar = PendaftaranPengujian::find($id);
        if(file_exists(public_path('storage/'.$daftar->bukti)))
        {
            return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
        }
        return response([
            'status' => 'error'
        ],500);
    }
}
