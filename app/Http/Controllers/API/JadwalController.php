<?php

namespace App\Http\Controllers\API;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Unlu\Laravel\Api\QueryBuilder;

/* Validation */


/* Models */
use App\Models\Master\JenisPengujian;
use App\Models\Master\Lingkup;
use App\Models\KajiUlang\KajiUlang;
use App\Models\KajiUlang\Detail;
/* Libraries */
use Datatables;
use Entrust;
use Carbon\Carbon;
use Hash;

class JadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function jadwal()
    {
        $layanan_id = null;
        if(auth()->user()->hasRole(['asman-dal-kalibrasi']) || auth()->user()->hasRole(['asman-lola-kalibrasi']) || auth()->user()->hasRole(['msb-kalibrasi'])){
            $layanan_id = 1;
        }
        if(auth()->user()->hasRole(['asman-dal-siskit']) || auth()->user()->hasRole(['asman-lola-siskit']) || auth()->user()->hasRole(['msb-siskit'])){
            $layanan_id = 2;
        }
        if(auth()->user()->hasRole(['asman-dal-sistgi']) || auth()->user()->hasRole(['asman-lola-sistgi']) || auth()->user()->hasRole(['msb-sistgi'])){
            $layanan_id = 3;
        }
        if(auth()->user()->hasRole(['asman-dal-tegangan-rendah']) || auth()->user()->hasRole(['asman-lola-tegangan-rendah']) || auth()->user()->hasRole(['msb-tegangan-rendah'])){
            $layanan_id = 4;
        }
        if(auth()->user()->hasRole(['asman-dal-tegangan-tinggi']) || auth()->user()->hasRole(['asman-lola-tegangan-tinggi']) || auth()->user()->hasRole(['msb-tegangan-tinggi'])){
            $layanan_id = 5;
        }

        $jadwal = [];
        if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi'])){
            if(auth()->user()->hasRole(['yan-kalibrasi'])){
                $jenis = JenisPengujian::select('*')
                                    ->whereHas('lingkup', function($query) use ($layanan_id){
                                        $query->where('jenis_pelayanan_id',1);
                                    })
                                    ->get();
            }else{
                $jenis = JenisPengujian::select('*')
                                    ->whereHas('lingkup', function($query) use ($layanan_id){
                                        $query->where('jenis_pelayanan_id','!=',1);
                                    })
                                    ->get();
            }
        }else{
            $jenis = JenisPengujian::select('*')
                                    ->whereHas('lingkup', function($query) use ($layanan_id){
                                        $query->where('jenis_pelayanan_id',$layanan_id);
                                    })
                                    ->get();
        }

        foreach ($jenis as $j) {
            $kaji = Detail::with('detail_pendaftaran.pp')->whereHas('detail_pendaftaran', function ($detail) use ($j) {
                                $detail->where('jenis_id',$j->id);
                            })
                            ->whereHas('kajiulang', function ($kajiulang) {
                                $kajiulang->whereHas('surat', function ($surat) {
                                    $surat->whereHas('va', function ($va) {
                                        $va->whereHas('konfirmasi', function ($konfirmasi) {
                                            $konfirmasi->whereNotNull('wbs_io');
                                        });
                                    });
                                });
                            })->get();

            $hasil = $kaji->map(function ($item, $key){
                $array = [
                    'no_order' => $item->detail_pendaftaran->pp->no_order,
                    'start' => $item->tentative_start,
                    'end' => $item->tentative_end,
                ];
                return $array;
            });

            $jadwal[] = [
                'jenis_pengujian'   => $j->nama,
                'jadwal'            => $hasil
            ];

        }


        return response([
            'data' => $jadwal,
        ]);
    }
}
