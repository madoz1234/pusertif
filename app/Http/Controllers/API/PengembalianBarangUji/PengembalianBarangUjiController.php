<?php

namespace App\Http\Controllers\API\PengembalianBarangUji;

/* Base App */
use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;
use App\Http\Controllers\Controller;

/* Validation */
// use App\Http\Requests\Konfigurasi\RolesRequest;
/* Models */
use App\Models\Authentication\Role;
use App\Models\PenerimaanBarang\PenerimaanBarang;
use App\Models\PengembalianBarang\PengembalianBarang;
use App\Models\FrontEnd\PendaftaranPengujian;


/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Zipper;

class PengembalianBarangUjiController extends Controller
{
    public function grid(Request $request)
    {
      if(auth()->user()->hasRole(['yan-kalibrasi','adminlab-kalibrasi'])){
            $data = PenerimaanBarang::with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detail_penerimaan_barang.detail_pengujian.detailpelaksana','detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.laporan_pengujian','konfirmasi.va.surat.detail','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.pelaksana','pengujian.detailpengujian.detailpelaksana.pelaksanalaporan')
              ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
              ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
              ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
              ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
              ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
              ->whereHas('pengujian', function($u){
                $u->where('status_data_pengujian', 1);
              })
              ->byLayananV2([1])
              ->where('trans_penerimaan_barang.status_pengembalian', 0)
              ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                   $q->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                      $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                            ->where('tipe', 0);
                   });
              })
              ->when($no_order = $request->no_order, function($q) use ($no_order) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                      $pp->where('no_order','like','%'.$no_order.'%')
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                      $pp->where('layanan_id',$jenis_pelayanan_id)
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                      $pp->where('no_surat','like','%'.$no_surat.'%')
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                      $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
                      $pelanggan->where('perusahaan_id',$perusahaan_id);
                   });
              })
              ->select('trans_penerimaan_barang.*');
      }elseif(auth()->user()->hasRole(['yan-uji','adminlab-siskit'])){
        $data = PenerimaanBarang::with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detail_penerimaan_barang.detail_pengujian.detailpelaksana','detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.laporan_pengujian','konfirmasi.va.surat.detail','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.pelaksana','pengujian.detailpengujian.detailpelaksana.pelaksanalaporan')
              ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
              ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
              ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
              ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
              ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
              ->whereHas('pengujian', function($u){
                $u->where('status_data_pengujian', 1);
              })
              ->byLayananV2([2])
              ->where('trans_penerimaan_barang.status_pengembalian', 0)
              ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                   $q->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                      $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                            ->where('tipe', 0);
                   });
              })
              ->when($no_order = $request->no_order, function($q) use ($no_order) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                      $pp->where('no_order','like','%'.$no_order.'%')
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                      $pp->where('layanan_id',$jenis_pelayanan_id)
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                      $pp->where('no_surat','like','%'.$no_surat.'%')
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                      $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
                      $pelanggan->where('perusahaan_id',$perusahaan_id);
                   });
              })
              ->select('trans_penerimaan_barang.*');
      }elseif(auth()->user()->hasRole(['yan-uji','adminlab-sistgi'])){
        $data = PenerimaanBarang::with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detail_penerimaan_barang.detail_pengujian.detailpelaksana','detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.laporan_pengujian','konfirmasi.va.surat.detail','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.pelaksana','pengujian.detailpengujian.detailpelaksana.pelaksanalaporan')
              ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
              ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
              ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
              ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
              ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
              ->whereHas('pengujian', function($u){
                $u->where('status_data_pengujian', 1);
              })
              ->byLayananV2([3])
              ->where('trans_penerimaan_barang.status_pengembalian', 0)
              ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                   $q->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                      $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                            ->where('tipe', 0);
                   });
              })
              ->when($no_order = $request->no_order, function($q) use ($no_order) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                      $pp->where('no_order','like','%'.$no_order.'%')
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                      $pp->where('layanan_id',$jenis_pelayanan_id)
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                      $pp->where('no_surat','like','%'.$no_surat.'%')
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                      $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
                      $pelanggan->where('perusahaan_id',$perusahaan_id);
                   });
              })
              ->select('trans_penerimaan_barang.*');
      }elseif(auth()->user()->hasRole(['yan-uji','adminlab-tegangan-rendah'])){
        $data = PenerimaanBarang::with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detail_penerimaan_barang.detail_pengujian.detailpelaksana','detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.laporan_pengujian','konfirmasi.va.surat.detail','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.pelaksana','pengujian.detailpengujian.detailpelaksana.pelaksanalaporan')
              ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
              ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
              ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
              ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
              ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
              ->whereHas('pengujian', function($u){
                $u->where('status_data_pengujian', 1);
              })
              ->byLayananV2([4])
              ->where('trans_penerimaan_barang.status_pengembalian', 0)
              ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                   $q->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                      $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                            ->where('tipe', 0);
                   });
              })
              ->when($no_order = $request->no_order, function($q) use ($no_order) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                      $pp->where('no_order','like','%'.$no_order.'%')
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                      $pp->where('layanan_id',$jenis_pelayanan_id)
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                      $pp->where('no_surat','like','%'.$no_surat.'%')
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                      $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
                      $pelanggan->where('perusahaan_id',$perusahaan_id);
                   });
              })
              ->select('trans_penerimaan_barang.*');
      }elseif(auth()->user()->hasRole(['yan-uji','adminlab-tegangan-tinggi'])){
        $data = PenerimaanBarang::with('konfirmasi.va.surat.kaji_ulang.pp','konfirmasi.va.surat.kaji_ulang.pp.lingkup','konfirmasi.va.surat.kaji_ulang.pp.pelayanan','konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','detail_penerimaan_barang.detail_pengujian.detailpelaksana','detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.laporan_pengujian','konfirmasi.va.surat.detail','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','konfirmasi.va.surat.kaji_ulang.detail.mata_uji','konfirmasi.va.surat.kaji_ulang.detail.lokasi','pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','pengujian.detailpengujian.pelaksana','pengujian.detailpengujian.detailpelaksana.pelaksanalaporan')
              ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
              ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
              ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
              ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
              ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
              ->whereHas('pengujian', function($u){
                $u->where('status_data_pengujian', 1);
              })
              ->byLayananV2([5])
              ->where('trans_penerimaan_barang.status_pengembalian', 0)
              ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
                   $q->whereHas('konfirmasi', function($konfirmasi) use ($wbs_io){
                      $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                            ->where('tipe', 0);
                   });
              })
              ->when($no_order = $request->no_order, function($q) use ($no_order) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
                      $pp->where('no_order','like','%'.$no_order.'%')
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
                      $pp->where('layanan_id',$jenis_pelayanan_id)
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
                      $pp->where('no_surat','like','%'.$no_surat.'%')
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
                      $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                        ->where('tipe_surat', 0);
                   });
              })
              ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
                   $q->whereHas('konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
                      $pelanggan->where('perusahaan_id',$perusahaan_id);
                   });
              })
              ->select('trans_penerimaan_barang.*');
      }else{
        return response()->json([
            'status' => false,
            'message' => 'page not found'
        ],404);
      }
        if($sort = $request->sort){
            if($sort == 'order'){
              $data->orderBy('trans_pp.tgl_order','asc');
            }
            if($sort == 'surat'){
              $data->orderBy('trans_pp.tgl_surat','asc');
            }
        }else{
          $data->orderBy('trans_penerimaan_barang.created_at','desc');
        }
        
        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function histori(Request $request)
    {
      if(auth()->user()->hasRole(['yan-kalibrasi','adminlab-kalibrasi'])){
        $data = PengembalianBarang::with('penerimaan.konfirmasi.va.surat.kaji_ulang.pp','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','penerimaan.detail_penerimaan_barang.detail_pengujian.detailpelaksana','penerimaan.detail_penerimaan_barang.detail_pp.jenis','penerimaan.pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','penerimaan.pengujian.laporan_pengujian','penerimaan.konfirmasi.va.surat.detail','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','penerimaan.pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','penerimaan.pengujian.detailpengujian.pelaksana','penerimaan.pengujian.detailpengujian.detailpelaksana.pelaksanalaporan')
          ->join('trans_penerimaan_barang','trans_pengembalian_barang.penerimaan_id','=','trans_penerimaan_barang.id')
          ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
          ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
          ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
          ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
          ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
          ->whereHas('penerimaan', function($z){
            $z->byLayananV2([1]);
          })
          ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
           $q->whereHas('penerimaan.konfirmasi', function($konfirmasi) use ($wbs_io){
              $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                    ->where('tipe', 0);
           });
          })
          ->when($no_order = $request->no_order, function($q) use ($no_order) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
              $pp->where('no_order','like','%'.$no_order.'%')
                ->where('tipe_surat', 0);
           });
          })
          ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
              $pp->where('layanan_id',$jenis_pelayanan_id)
                ->where('tipe_surat', 0);
           });
          })
          ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
              $pp->where('no_surat','like','%'.$no_surat.'%')
                ->where('tipe_surat', 0);
           });
          })
          ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
              $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                ->where('tipe_surat', 0);
           });
          })
          ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
              $pelanggan->where('perusahaan_id',$perusahaan_id);
           });
          })
          ->select('trans_pengembalian_barang.*');
      }elseif(auth()->user()->hasRole(['yan-uji','adminlab-siskit'])){
        $data = PengembalianBarang::with('penerimaan.konfirmasi.va.surat.kaji_ulang.pp','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','penerimaan.detail_penerimaan_barang.detail_pengujian.detailpelaksana','penerimaan.detail_penerimaan_barang.detail_pp.jenis','penerimaan.pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','penerimaan.pengujian.laporan_pengujian','penerimaan.konfirmasi.va.surat.detail','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','penerimaan.pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','penerimaan.pengujian.detailpengujian.pelaksana','penerimaan.pengujian.detailpengujian.detailpelaksana.pelaksanalaporan')
          ->join('trans_penerimaan_barang','trans_pengembalian_barang.penerimaan_id','=','trans_penerimaan_barang.id')
          ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
          ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
          ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
          ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
          ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
          ->whereHas('penerimaan', function($z){
            $z->byLayananV2([2]);
          })
          ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
           $q->whereHas('penerimaan.konfirmasi', function($konfirmasi) use ($wbs_io){
              $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                    ->where('tipe', 0);
           });
          })
          ->when($no_order = $request->no_order, function($q) use ($no_order) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
              $pp->where('no_order','like','%'.$no_order.'%')
                ->where('tipe_surat', 0);
           });
          })
          ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
              $pp->where('layanan_id',$jenis_pelayanan_id)
                ->where('tipe_surat', 0);
           });
          })
          ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
              $pp->where('no_surat','like','%'.$no_surat.'%')
                ->where('tipe_surat', 0);
           });
          })
          ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
              $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                ->where('tipe_surat', 0);
           });
          })
          ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
              $pelanggan->where('perusahaan_id',$perusahaan_id);
           });
          })
          ->select('trans_pengembalian_barang.*');
      }elseif(auth()->user()->hasRole(['yan-uji','adminlab-sistgi'])){
       $data = PengembalianBarang::with('penerimaan.konfirmasi.va.surat.kaji_ulang.pp','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','penerimaan.detail_penerimaan_barang.detail_pengujian.detailpelaksana','penerimaan.detail_penerimaan_barang.detail_pp.jenis','penerimaan.pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','penerimaan.pengujian.laporan_pengujian','penerimaan.konfirmasi.va.surat.detail','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','penerimaan.pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','penerimaan.pengujian.detailpengujian.pelaksana','penerimaan.pengujian.detailpengujian.detailpelaksana.pelaksanalaporan')
          ->join('trans_penerimaan_barang','trans_pengembalian_barang.penerimaan_id','=','trans_penerimaan_barang.id')
          ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
          ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
          ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
          ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
          ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
          ->whereHas('penerimaan', function($z){
            $z->byLayananV2([3]);
          })
          ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
           $q->whereHas('penerimaan.konfirmasi', function($konfirmasi) use ($wbs_io){
              $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                    ->where('tipe', 0);
           });
          })
          ->when($no_order = $request->no_order, function($q) use ($no_order) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
              $pp->where('no_order','like','%'.$no_order.'%')
                ->where('tipe_surat', 0);
           });
          })
          ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
              $pp->where('layanan_id',$jenis_pelayanan_id)
                ->where('tipe_surat', 0);
           });
          })
          ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
              $pp->where('no_surat','like','%'.$no_surat.'%')
                ->where('tipe_surat', 0);
           });
          })
          ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
              $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                ->where('tipe_surat', 0);
           });
          })
          ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
              $pelanggan->where('perusahaan_id',$perusahaan_id);
           });
          })
          ->select('trans_pengembalian_barang.*');
      }elseif(auth()->user()->hasRole(['yan-uji','adminlab-tegangan-rendah'])){
        $data = PengembalianBarang::with('penerimaan.konfirmasi.va.surat.kaji_ulang.pp','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','penerimaan.detail_penerimaan_barang.detail_pengujian.detailpelaksana','penerimaan.detail_penerimaan_barang.detail_pp.jenis','penerimaan.pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','penerimaan.pengujian.laporan_pengujian','penerimaan.konfirmasi.va.surat.detail','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','penerimaan.pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','penerimaan.pengujian.detailpengujian.pelaksana','penerimaan.pengujian.detailpengujian.detailpelaksana.pelaksanalaporan')
          ->join('trans_penerimaan_barang','trans_pengembalian_barang.penerimaan_id','=','trans_penerimaan_barang.id')
          ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
          ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
          ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
          ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
          ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
          ->whereHas('penerimaan', function($z){
            $z->byLayananV2([4]);
          })
          ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
           $q->whereHas('penerimaan.konfirmasi', function($konfirmasi) use ($wbs_io){
              $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                    ->where('tipe', 0);
           });
          })
          ->when($no_order = $request->no_order, function($q) use ($no_order) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
              $pp->where('no_order','like','%'.$no_order.'%')
                ->where('tipe_surat', 0);
           });
          })
          ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
              $pp->where('layanan_id',$jenis_pelayanan_id)
                ->where('tipe_surat', 0);
           });
          })
          ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
              $pp->where('no_surat','like','%'.$no_surat.'%')
                ->where('tipe_surat', 0);
           });
          })
          ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
              $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                ->where('tipe_surat', 0);
           });
          })
          ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
              $pelanggan->where('perusahaan_id',$perusahaan_id);
           });
          })
          ->select('trans_pengembalian_barang.*');
      }elseif(auth()->user()->hasRole(['yan-uji','adminlab-tegangan-tinggi'])){
        $data = PengembalianBarang::with('penerimaan.konfirmasi.va.surat.kaji_ulang.pp','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.lingkup','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.pelayanan','penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans.perusahaan','penerimaan.detail_penerimaan_barang.detail_pengujian.detailpelaksana','penerimaan.detail_penerimaan_barang.detail_pp.jenis','penerimaan.pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','penerimaan.pengujian.laporan_pengujian','penerimaan.konfirmasi.va.surat.detail','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.jenis','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.detail_pendaftaran.satuan','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.mata_uji','penerimaan.konfirmasi.va.surat.kaji_ulang.detail.lokasi','penerimaan.pengujian.detailpengujian.detail_penerimaan_barang.detail_pp.jenis','penerimaan.pengujian.detailpengujian.pelaksana','penerimaan.pengujian.detailpengujian.detailpelaksana.pelaksanalaporan')
          ->join('trans_penerimaan_barang','trans_pengembalian_barang.penerimaan_id','=','trans_penerimaan_barang.id')
          ->join('trans_konfirmasi_pembayaran','trans_penerimaan_barang.konfirmasi_id','=','trans_konfirmasi_pembayaran.id')
          ->join('trans_va','trans_va.id','=','trans_konfirmasi_pembayaran.va_id')
          ->join('trans_surat_penawaran','trans_surat_penawaran.id','=','trans_va.surat_id')
          ->join('trans_kaji_ulang','trans_surat_penawaran.kaji_id','=','trans_kaji_ulang.id')
          ->join('trans_pp','trans_kaji_ulang.pp_id','=','trans_pp.id')
          ->whereHas('penerimaan', function($z){
            $z->byLayananV2([5]);
          })
          ->when($wbs_io = $request->wbs_io, function($q) use ($wbs_io) {
           $q->whereHas('penerimaan.konfirmasi', function($konfirmasi) use ($wbs_io){
              $konfirmasi->where('wbs_io','like','%'.$wbs_io.'%')
                    ->where('tipe', 0);
           });
          })
          ->when($no_order = $request->no_order, function($q) use ($no_order) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_order){
              $pp->where('no_order','like','%'.$no_order.'%')
                ->where('tipe_surat', 0);
           });
          })
          ->when($jenis_pelayanan_id = $request->jenis_pelayanan_id, function($q) use ($jenis_pelayanan_id) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($jenis_pelayanan_id){
              $pp->where('layanan_id',$jenis_pelayanan_id)
                ->where('tipe_surat', 0);
           });
          })
          ->when($no_surat = $request->no_surat, function($q) use ($no_surat) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($no_surat){
              $pp->where('no_surat','like','%'.$no_surat.'%')
                ->where('tipe_surat', 0);
           });
          })
          ->when($tanggal_order = $request->tanggal_order, function($q) use ($tanggal_order) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($tanggal_order){
              $pp->where('tgl_order',Carbon::createFromFormat('d/m/Y',$tanggal_order)->format('Y-m-d'))
                ->where('tipe_surat', 0);
           });
          })
          ->when($perusahaan_id = $request->perusahaan_id, function($q) use ($perusahaan_id) {
           $q->whereHas('penerimaan.konfirmasi.va.surat.kaji_ulang.pp.user.pelanggans', function($pelanggan) use($perusahaan_id){
              $pelanggan->where('perusahaan_id',$perusahaan_id);
           });
          })
          ->select('trans_pengembalian_barang.*');
      }else{
        return response()->json([
            'status' => false,
            'message' => 'page not found'
        ],404);
      }
        
        if($sort = $request->sort){
            if($sort == 'order'){
              $data->orderBy('trans_pp.tgl_order','asc');
            }
            if($sort == 'surat'){
              $data->orderBy('trans_pp.tgl_surat','asc');
            }
        }else{
          $data->orderBy('trans_pengembalian_barang.created_at','desc');
        }

        $page = $data->paginate(10);
                // dd($page[0]);
        if ($page[0] == null) {
            return response()->json([
                'status' => false,
                'message' => 'page not found'
            ],404);
        }

        return response()->json($page);
    }

    public function download($id)
    {
        $daftar = PendaftaranPengujian::find($id);
        if(file_exists(public_path('storage/'.$daftar->bukti)))
        {
            return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
        }

        return response([
            'status' => 'error'
        ],500);
    }

    public function printSuratPdf(Request $request, $id){
      $record = PengembalianBarang::find($id);
        $data = [
          'data' => $record,
        ];
      $pdf = PDF::loadView('modules.pengembalian-barang-uji.cetak', $data);
        return $pdf->stream('Surat Pengembalian Barang '.$record->tanggal.'.pdf',array("Attachment" => false));
  }
}
