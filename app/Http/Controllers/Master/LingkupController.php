<?php

namespace App\Http\Controllers\Master;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Master\LingkupRequest;

/* Models */
use App\Models\Model;
use App\Models\Authentication\Role;
use App\Models\Master\Lingkup;
use App\Models\Master\JenisPengujian;

/* Libraries */
use DataTables;

class LingkupController extends Controller
{
    protected $link = 'master/lingkup/';
    protected $perms = 'master-lingkup';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Lingkup");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Master' => '#', 'Lingkup' => '#']);

        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'jenis_pelayanan',
                'name' => 'jenis_pelayanan',
                'label' => 'Jenis Pelayanan',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'searchable' => false,
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '100px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Lingkup::select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'like', '%' . $nama . '%');
        }

        if ($jenis_pelayanan_id = $request->jenis_pelayanan_id) {
            $records->where('jenis_pelayanan_id',$jenis_pelayanan_id);
        }

        $records = $records->get();
        

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('jenis_pelayanan', function ($record) use ($request) {
            return $record->pelayanan->nama;
        })
        ->addColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->addColumn('created_by', function ($record) {
            return $record->creatorName();
        })
       
        ->addColumn('action', function ($record) {
            $btn = '';
            
            // $btn .= $this->makeButton([
            //     'type' => 'edit',
            //     'id'   => $record->id
            // ]);
            // Delete
            if(auth()->user()->hasRole(['admin'])){
	            $btn .= $this->makeButton([
	                'type' => 'edit',
	                'id'   => $record->id
	            ]);
	            // Delete
	            $btn .= $this->makeButton([
	                'type' => 'delete',
	                'id'   => $record->id
	            ]);
	        }else{
	        	$btn = '-';
	        }

            return $btn;
        })
        ->rawColumns(['action','attachment','deskripsi'])
        ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.lingkup.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.master.lingkup.create');
    }

    public function store(LingkupRequest $request)
    {
        
        $record = Lingkup::saveData($request);
        return response([
            'status' => true
        ]);
    }

    public function edit($id)
    {
        $record = Lingkup::find($id);

        return $this->render('modules.master.lingkup.edit', [
            'record' => $record
        ]);
    }

    public function show($id)
    {
        // $record = Role::find($id);

        // return $this->render('modules.master.lingkup.detail', [
        //     'record' => $record
        // ]);
    }

    public function update(LingkupRequest $request, $id)
    {
        $lingkup = Lingkup::find($id);
        $lingkup->fill($request->all());
        $lingkup->save();

        return response([
            'status' => true
        ]);
    }

    public function destroy($id)
    {
        $lingkup = Lingkup::find($id);
        if($lingkup){
        	if($lingkup->pengujian->count() > 0){
        		return response([
                	'status' => true,
            	],500);
        	}else{
        		$lingkup->delete();
        		return response([
                	'status' => true,
            	],200);
        	}
        }else{
            return response([
                'status' => true,
            ],500);
        }
    }
}
