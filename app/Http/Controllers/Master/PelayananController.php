<?php

namespace App\Http\Controllers\Master;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Master\JenisLayananRequest;

/* Models */
use App\Models\Model;
use App\Models\Authentication\Role;
use App\Models\Master\JenisPelayanan;

/* Libraries */
use DataTables;

class PelayananController extends Controller
{
    protected $link = 'master/jenis-layanan/';
    protected $perms = 'master-jenis-pelayanan';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Jenis Layanan");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Master' => '#', 'Jenis Layanan' => '#']);

        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama Jenis Layanan',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'searchable' => false,
                'sortable' => true,
                'width' => '120px',
            ],
            // [
            //     'data' => 'action',
            //     'name' => 'action',
            //     'label' => 'Aksi',
            //     'searchable' => false,
            //     'sortable' => false,
            //     'className' => "center aligned",
            //     'width' => '100px',
            // ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = JenisPelayanan::select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'like', '%' . $nama . '%');
        }

        $records = $records->get();
        

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->addColumn('created_by', function ($record) {
            return $record->creatorName();
        })
        // ->addColumn('action', function ($record) {
        //     $btn = '';
            
        //     $btn .= $this->makeButton([
        //         'type' => 'edit',
        //         'id'   => $record->id
        //     ]);
        //     // Delete
        //     $btn .= $this->makeButton([
        //         'type' => 'delete',
        //         'id'   => $record->id
        //     ]);

        //     return $btn;
        // })
        ->rawColumns(['action','attachment','deskripsi'])
        ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.jenis-layanan.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.master.jenis-layanan.create');
    }

    public function store(JenisLayananRequest $request)
    {
        $record = JenisPelayanan::saveData($request);
        return response([
            'status' => true
        ]);
    }

    public function edit($id)
    {
        $record = JenisPelayanan::find($id);

        return $this->render('modules.master.jenis-layanan.edit', [
            'record' => $record
        ]);
    }

    public function show($id)
    {
        // $record = Role::find($id);

        // return $this->render('modules.master.jenis-layanan.detail', [
        //     'record' => $record
        // ]);
    }

    public function update(JenisLayananRequest $request, $id)
    {
        $record = JenisPelayanan::saveData($request);

        return response([
            'status' => true
        ]);
    }

    public function destroy($id)
    {
        $pelayanan = JenisPelayanan::find($id);
        $pelayanan->delete();

        return response([
            'status' => true,
        ]);
    }
}
