<?php

namespace App\Http\Controllers\Master;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Master\HariKerjaRequest;
 
/* Models */
use App\Models\Model;
use App\Models\Authentication\Role;
use App\Models\Master\HariKerja;

/* Libraries */
use DataTables;

class HariKerjaController extends Controller
{
    protected $link = 'master/hari-kerja/';
    protected $perms = 'master-hari-kerja';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Hari Kerja");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Master' => '#', 'Hari Kerja' => '#']);

        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            // [
            //     'data' => 'nama',
            //     'name' => 'nama',
            //     'label' => 'Nama',
            //     'className' => "center aligned",
            //     'searchable' => false,
            //     'sortable' => true,
            // ],
            [
                'data' => 'jenis_pelayanan_id',
                'name' => 'jenis_pelayanan_id',
                'label' => 'Jenis Pelayanan',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'display_menu',
                'name' => 'display_menu',
                'label' => 'Menu',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'description',
                'name' => 'description',
                'label' => 'Description',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'hk',
                'name' => 'hk',
                'label' => 'Hari Kerja',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'searchable' => false,
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '100px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = HariKerja::select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'like', '%' . $nama . '%');
        }

        $records = $records->get();
        

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->addColumn('created_by', function ($record) {
            return $record->creatorName();
        })
        ->editColumn('jenis_pelayanan_id', function ($record) {
            return $record->pelayanan->nama;
        })
        ->addColumn('action', function ($record) {
            $btn = '';
            
            if(auth()->user()->hasRole(['admin'])){
	            $btn .= $this->makeButton([
	                'type' => 'edit',
	                'id'   => $record->id
	            ]);
	            // Delete
	            $btn .= $this->makeButton([
	                'type' => 'delete',
	                'id'   => $record->id
	            ]);
	        }else{
	        	$btn = '-';
	        }

            return $btn;
        })
        ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.hari-kerja.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.master.hari-kerja.create');
    }

    public function store(HariKerjaRequest $request)
    {
    	// dd($request);
        $record = new HariKerja;
        $record->fill($request->all());
        $record->save();

        return response([
            'status' => true
        ]);
    }

    public function edit($id)
    {
        $record = HariKerja::find($id);

        return $this->render('modules.master.hari-kerja.edit', [
            'record' => $record
        ]);
    }

    public function show($id)
    {
        // $record = Role::find($id);

        // return $this->render('modules.master.hari-kerja.detail', [
        //     'record' => $record
        // ]);
    }

    public function update(HariKerjaRequest $request, $id)
    {
        $hari_kerja = HariKerja::find($id);
        $hari_kerja->fill($request->all());
        $hari_kerja->save();

        return response([
            'status' => true
        ]);
    }

    public function destroy($id)
    {
        $hari_kerja = HariKerja::find($id);
        $hari_kerja->delete();

        return response([
            'status' => true,
        ]);
    }
}
