<?php

namespace App\Http\Controllers\Master;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Master\ContactRequest;

/* Models */
use App\Models\Model;
use App\Models\Master\Contact;

/* Libraries */
use DataTables;
use DB;

class ContactController extends Controller
{
    protected $link = 'master/contact/';
    protected $perms = 'master-contact';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Contact");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Master' => '#', 'Contact' => '#']);

        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'no_tlp',
                'name' => 'no_tlp',
                'label' => 'No Telepon',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'wa',
                'name' => 'wa',
                'label' => 'WhatsApp',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'searchable' => false,
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '100px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Contact::select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'like', '%' . $nama . '%');
        }

        $records = $records->get();
        

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('status', function ($record) use ($request) {
        	$status = 'Tidak Aktif';
        	if($record->status == 1){
        		$status = 'Aktif';
        	}
            return $status;
        })
        ->addColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->addColumn('created_by', function ($record) {
            return $record->creatorName();
        })
        ->addColumn('action', function ($record) {
            $btn = '';
            
            if(auth()->user()->hasRole(['admin'])){
	            $btn .= $this->makeButton([
	                'type' => 'edit',
	                'id'   => $record->id
	            ]);
	            // Delete
	            $btn .= $this->makeButton([
	                'type' => 'delete',
	                'id'   => $record->id
	            ]);
	        }else{
	        	$btn = '-';
	        }
            return $btn;
        })
        ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.contact.index', ['mockup' => false]);
    }

    public function create()
    {
    	$jum = Contact::where('status', 1)->count();
        return $this->render('modules.master.contact.create', [
            'jum' => $jum
        ]);
    }

    public function store (ContactRequest $request)
    {
    	DB::beginTransaction();
        try{

        	$record = new Contact;
        	$record->no_tlp = $request->no_tlp;
        	$record->wa = $request->wa;
        	if(is_null($request->status)){
        		$record->status = 0;
        	}else{
        		$record->status = 1;
        	}
        	$record->save();

            DB::commit();
            return response([
                'status' => true
            ]);

        }catch(\Exception $e){
        	return $e;
            DB::rollback();
            return response([
                'status' => true,
                'errors' => $e 
            ]);
        }
    }

    public function edit($id)
    {
        $record = Contact::find($id);
        $jum = Contact::where('status', 1)
        				->where('id', '!=' , $id)
        				->count();
        return $this->render('modules.master.contact.edit', [
            'record' => $record,
            'jum' => $jum
        ]);
    }

    public function show($id)
    {
        // $record = Role::find($id);

        // return $this->render('modules.master.contact.detail', [
        //     'record' => $record
        // ]);
    }

    public function update (ContactRequest $request, $id)
    {
        DB::beginTransaction();
        try{
        	$record = Contact::find($id);
        	$record->no_tlp = $request->no_tlp;
        	$record->wa = $request->wa;
        	if(is_null($request->status)){
        		$record->status = 0;
        	}else{
        		$record->status = 1;
        	}
        	$record->save();

            DB::commit();
            return response([
                'status' => true
            ]);

        }catch(\Exception $e){
        	return $e;
            DB::rollback();
            return response([
                'status' => true,
                'errors' => $e 
            ]);
        }
    }

    public function destroy($id)
    {
        $contact = Contact::find($id);
        $contact->delete();

        return response([
            'status' => true,
        ]);
    }
}
