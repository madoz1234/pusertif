<?php

namespace App\Http\Controllers\Master;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Master\MataUjiRequest;

/* Models */
use App\Models\Model;
use App\Models\Authentication\Role;
use App\Models\Master\MataUji;

/* Libraries */
use DataTables;

class MataUjiController extends Controller
{
    protected $link = 'master/mata-uji/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Mata Uji");
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Master' => '#', 'Mata Uji' => '#']);

        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama_mata_uji',
                'name' => 'nama_mata_uji',
                'label' => 'Mata Uji',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'searchable' => false,
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '100px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = MataUji::select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama_mata_uji', 'like', '%' . $nama . '%');
        }

        $records = $records->get();
        

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->addColumn('created_by', function ($record) {
            return $record->creatorName();
        })
        ->addColumn('action', function ($record) {
            $btn = '';
            
            if(auth()->user()->hasRole(['admin'])){
	            $btn .= $this->makeButton([
	                'type' => 'edit',
	                'id'   => $record->id
	            ]);
	            // Delete
	            $btn .= $this->makeButton([
	                'type' => 'delete',
	                'id'   => $record->id
	            ]);
	        }else{
	        	$btn = '-';
	        }

            return $btn;
        })
        ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.mata-uji.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.master.mata-uji.create');
    }

    public function store(MataUjiRequest $request)
    {
        $record = new MataUji;
        $record->fill($request->all());
        $record->save();

        return response([
            'status' => true
        ]);
    }

    public function edit($id)
    {
        $record = MataUji::find($id);

        return $this->render('modules.master.mata-uji.edit', [
            'record' => $record
        ]);
    }

    public function show($id)
    {
        // $record = Role::find($id);

        // return $this->render('modules.master.mata-uji.detail', [
        //     'record' => $record
        // ]);
    }

    public function update(MataUjiRequest $request, $id)
    {
        $pengujian = MataUji::find($id);
        $pengujian->fill($request->all());
        $pengujian->save();

        return response([
            'status' => true
        ]);
    }

    public function destroy($id)
    {
        $pengujian = MataUji::find($id);
        $pengujian->delete();

        return response([
            'status' => true,
        ]);
    }
}
