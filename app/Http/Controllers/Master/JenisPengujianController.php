<?php

namespace App\Http\Controllers\Master;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Master\JenisPengujianRequest;

/* Models */
use App\Models\Model;
use App\Models\Authentication\Role;
use App\Models\Master\JenisPengujian;

/* Libraries */
use DataTables;

class JenisPengujianController extends Controller
{
    protected $link = 'master/jenis-pengujian/';
    protected $perms = 'master-jenis-pengujian';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Jenis Pengujian");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Master' => '#', 'Jenis Pengujian' => '#']);

        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'pelayanan',
                'name' => 'pelayanan',
                'label' => 'Pelayanan',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'lingkup',
                'name' => 'lingkup',
                'label' => 'Lingkup',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Jenis Pengujian',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'searchable' => false,
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '100px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = JenisPengujian::select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'like', '%' . $nama . '%');
        }

        $records = $records->get();
        

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('pelayanan', function ($record) use ($request) {
            return $record->lingkup->pelayanan->nama;
        })
        ->addColumn('lingkup', function ($record) use ($request) {
            return $record->lingkup->nama;
        })
        ->addColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->addColumn('created_by', function ($record) {
            return $record->creatorName();
        })
        ->addColumn('action', function ($record) {
            $btn = '';
            if(auth()->user()->hasRole(['admin'])){
	            $btn .= $this->makeButton([
	                'type' => 'edit',
	                'id'   => $record->id
	            ]);
	            // Delete
	            $btn .= $this->makeButton([
	                'type' => 'delete',
	                'id'   => $record->id
	            ]);
	        }else{
	        	$btn = '-';
	        }

            return $btn;
        })
        ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.jenis-pengujian.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.master.jenis-pengujian.create');
    }

    public function store(JenisPengujianRequest $request)
    {
        $record = new JenisPengujian;
        $record->fill($request->all());
        $record->save();

        return response([
            'status' => true
        ]);
    }

    public function edit($id)
    {
        $record = JenisPengujian::find($id);

        return $this->render('modules.master.jenis-pengujian.edit', [
            'record' => $record
        ]);
    }

    public function show($id)
    {
        // $record = Role::find($id);

        // return $this->render('modules.master.jenis-pengujian.detail', [
        //     'record' => $record
        // ]);
    }

    public function update(JenisPengujianRequest $request, $id)
    {
        $pengujian = JenisPengujian::find($id);
        $pengujian->nama = $request->nama;
        $pengujian->save();

        return response([
            'status' => true
        ]);
    }

    public function destroy($id)
    {
        $pengujian = JenisPengujian::find($id);
        if($pengujian->ppdetail->count() > 0){
        	return response([
		            'status' => true,
		    ], 500);
        }else{
        	if($pengujian->ujiserahterimadetail->count() > 0){
        		return response([
		            'status' => true,
		        ], 500);
        	}else{
        		$pengujian->delete();
        		return response([
		            'status' => true,
		        ], 200);
        	}
        }
    }
}
