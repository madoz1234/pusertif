<?php

namespace App\Http\Controllers\Master;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Master\SurveiRequest;

/* Models */
use App\Models\Model;
use App\Models\Master\Survei;
use App\Models\Master\SurveiDetail;
use App\Models\FrontEnd\TransSurvei;

/* Libraries */
use DataTables;
use DB;

class SurveiController extends Controller
{
    protected $link = 'master/survei/';
    protected $perms = 'master-survei';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Survei");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Master' => '#', 'Survei' => '#']);

        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama Survei',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'deskripsi',
                'name' => 'deskripsi',
                'label' => 'Deskripsi',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'searchable' => false,
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '100px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Survei::select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'like', '%' . $nama . '%');
        }

        $records = $records->get();
        

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('status', function ($record) use ($request) {
            return ($record->status == 1) ? 'Aktif' : 'Tidak Aktif';
        })
        ->addColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->addColumn('created_by', function ($record) {
            return $record->creatorName();
        })
        ->addColumn('action', function ($record) {
            $btn = '';
            
            if(auth()->user()->hasRole(['admin'])){
	            $btn .= $this->makeButton([
                    'type' => 'url',
                    'class'   => 'teal icon',
                    'label'   => '<i class="eye text icon"></i>',
                    'tooltip' => 'Monitoring Jawaban',
                    'url'  => url($this->link.$record->id)
                ]);
	            // Delete
	            $btn .= $this->makeButton([
	                'type' => 'delete',
	                'id'   => $record->id
	            ]);
	        }else{
	        	$btn = '-';
	        }

            return $btn;
        })
        ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.survei.index', ['mockup' => false]);
    }

    public function create()
    {
        $this->setTitle("Buat Survei Baru");
        $jum = Survei::where('status', 1)->count();
        return $this->render('modules.master.survei.create', [
            'jum' => $jum
        ]);
    }

    public function show($id)
    {
       $record = Survei::find($id);
       $this->setTitle('Monitoring Survei');
       $this->setBreadcrumb(['Survei' => '#', 'Monitoring Survei' => '#']);
       return $this->render('modules.master.survei.monitoring',[
       		'record' => $record,
       		'grafik' => $this->grafik($record->id),
   		]);
    }

    public function store (SurveiRequest $request)
    {
    	DB::beginTransaction();
        try{

        	$data = new Survei;
            $data->fill($request->all());
            if($data->save()){
                if ($request->status == 1) {
                    Survei::changeStatus();
                }
            }

            foreach ($request->detail as $row) {
                $detail = new SurveiDetail;
                $detail->fill($row);
                $data->detail()->save($detail);
            }

            DB::commit();
            return response([
                'status' => true
            ]);

        }catch(\Exception $e){
        	return $e;
            DB::rollback();
            return response([
                'status' => true,
                'errors' => $e 
            ]);
        }
    }

    public function edit($id)
    {
        $record = Survei::find($id);
        $jum = Survei::where('status', 1)
        				->where('id', '!=' , $id)
        				->count();

        $this->setTitle("Ubah Survei");
        return $this->render('modules.master.survei.edit', [
            'record' => $record,
            'jum' => $jum
        ]);
    }

    public function update (SurveiRequest $request, $id)
    {
        DB::beginTransaction();
        try{
        	$data = Survei::find($id);
            $data->fill($request->all());
            if(is_null($request->status)){
                $data->status = 0;
            }else{
                $data->status = 1;
                Survei::changeStatus();
            }
            $data->save();

            $detail = SurveiDetail::where('survei_id', $id)
                               ->whereNotIn('id', $request->exists)
                               ->delete();

            foreach ($request->detail as $row) {
                if(array_key_exists('id', $row)){
                    $detail = SurveiDetail::find($row['id']);
                }else{
                    $detail = new SurveiDetail();
                }
                $detail->fill($row);                
                
                $data->detail()->save($detail);
            }

            DB::commit();
            return response([
                'status' => true
            ]);

        }catch(\Exception $e){
        	return $e;
            DB::rollback();
            return response([
                'status' => true,
                'errors' => $e 
            ]);
        }
    }

    public function grafik($id)
    {
    	$data = TransSurvei::getGrafik($id);
    	if($data){
	    	return [
	    		'data' => $data[0],
	        ];
    	}else{
    		return [
	    		'data' => $data,
	        ];
    	}
    }

    public function destroy($id)
    {
        $survei = Survei::find($id);
        if ($survei) {
            SurveiDetail::where('survei_id', $id)->delete();
            TransSurvei::where('survei_id', $id)->delete();
            $survei->delete();
        }

        return response([
            'status' => true,
        ]);
    }
}
