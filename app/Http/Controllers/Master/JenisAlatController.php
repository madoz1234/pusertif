<?php

namespace App\Http\Controllers\Master;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Master\JenisAlatRequest;

/* Models */
use App\Models\Model;
use App\Models\Authentication\Role;
use App\Models\Master\JenisAlat;

/* Libraries */
use DataTables;

class JenisAlatController extends Controller
{
    protected $link = 'master/jenis-alat/';
    protected $perms = 'master-jenis-alat';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Jenis Alat");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Master' => '#', 'Jenis Alat' => '#']);

        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama_jenis_alat',
                'name' => 'nama_jenis_alat',
                'label' => 'Jenis Alat',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'searchable' => false,
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '100px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = JenisAlat::select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama_jenis_alat', 'like', '%' . $nama . '%');
        }

        $records = $records->get();
        

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->addColumn('created_by', function ($record) {
            return $record->creatorName();
        })
        ->addColumn('action', function ($record) {
            $btn = '';
            
            if(auth()->user()->hasRole(['admin'])){
	            $btn .= $this->makeButton([
	                'type' => 'edit',
	                'id'   => $record->id
	            ]);
	            // Delete
	            $btn .= $this->makeButton([
	                'type' => 'delete',
	                'id'   => $record->id
	            ]);
	        }else{
	        	$btn = '-';
	        }
            return $btn;
        })
        ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.jenis-alat.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.master.jenis-alat.create');
    }

    public function store(JenisAlatRequest $request)
    {
        $record = new JenisAlat;
        $record->fill($request->all());
        $record->save();

        return response([
            'status' => true
        ]);
    }

    public function edit($id)
    {
        $record = JenisAlat::find($id);

        return $this->render('modules.master.jenis-alat.edit', [
            'record' => $record
        ]);
    }

    public function show($id)
    {
        // $record = Role::find($id);

        // return $this->render('modules.master.jenis-alat.detail', [
        //     'record' => $record
        // ]);
    }

    public function update(JenisAlatRequest $request, $id)
    {
        $pengujian = JenisAlat::find($id);
        $pengujian->fill($request->all());
        $pengujian->save();

        return response([
            'status' => true
        ]);
    }

    public function destroy($id)
    {
        $pengujian = JenisAlat::find($id);
        $pengujian->delete();

        return response([
            'status' => true,
        ]);
    }
}
