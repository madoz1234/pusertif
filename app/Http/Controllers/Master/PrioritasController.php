<?php

namespace App\Http\Controllers\Master;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Master\PrioritasRequest;

/* Models */
use App\Models\Model;
use App\Models\Authentication\Role;
use App\Models\Master\Prioritas;

/* Libraries */
use DataTables;

class PrioritasController extends Controller
{
    protected $link = 'master/prioritas/';
    protected $perms = 'master-prioritas';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Prioritas");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Master' => '#', 'Prioritas' => '#']);

        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'jenis_pelayanan',
                'name' => 'jenis_pelayanan',
                'label' => 'Jenis Pelayanan',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'waktu',
                'name' => 'waktu',
                'label' => 'Waktu',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'searchable' => false,
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '100px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Prioritas::select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('jenis_pelayanan', 'like', '%' . $nama . '%');
        }

        $records = $records->get();
        

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('jenis_pelayanan', function ($record) use ($request) {
            return $record->pelayanan->nama;
        })
        ->addColumn('waktu', function ($record) use ($request) {
            return $record->waktu.' Bulan';
        })
        ->addColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->addColumn('created_by', function ($record) {
            return $record->creatorName();
        })
       
        ->addColumn('action', function ($record) {
            $btn = '';
            
            if(auth()->user()->hasRole(['admin'])){
	            $btn .= $this->makeButton([
	                'type' => 'edit',
	                'id'   => $record->id
	            ]);
	            // Delete
	        }else{
	        	$btn = '-';
	        }
            // Delete
            // $btn .= $this->makeButton([
            //     'type' => 'delete',
            //     'id'   => $record->id
            // ]);

            return $btn;
        })
        ->rawColumns(['action','attachment','deskripsi'])
        ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.prioritas.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.master.prioritas.create');
    }

    public function store(PrioritasRequest $request)
    {
        $record = new Prioritas;
        $record->layanan_id = $request->jenis_pelayanan_id;
        $record->fill($request->all());
        $record->save();

        return response([
            'status' => true
        ]);
    }

    public function edit($id)
    {
        $record = Prioritas::find($id);

        return $this->render('modules.master.prioritas.edit', [
            'record' => $record
        ]);
    }

    public function show($id)
    {
        // $record = Role::find($id);

        // return $this->render('modules.master.prioritas.detail', [
        //     'record' => $record
        // ]);
    }

    public function update(PrioritasRequest $request, $id)
    {
        $prioritas = Prioritas::find($id);
        $prioritas->layanan_id = $request->jenis_pelayanan_id;
        $prioritas->fill($request->all());
        $prioritas->save();

        return response([
            'status' => true
        ]);
    }

    public function destroy($id)
    {
        $prioritas = Prioritas::find($id);
        $prioritas->delete();

        return response([
            'status' => true,
        ]);
    }
}
