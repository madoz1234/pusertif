<?php

namespace App\Http\Controllers\Master;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Master\SatuanRequest;

/* Models */
use App\Models\Model;
use App\Models\Authentication\Role;
use App\Models\Master\Satuan;

/* Libraries */
use DataTables;

class SatuanController extends Controller
{
    protected $link = 'master/satuan/';
    protected $perms = 'master-satuan';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Satuan");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Master' => '#', 'Satuan' => '#']);

        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'satuan',
                'name' => 'satuan',
                'label' => 'Nama Satuan',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'searchable' => false,
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '100px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Satuan::select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($satuan = $request->satuan) {
            $records->where('satuan', 'like', '%' . $satuan . '%');
        }

        $records = $records->get();
        

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->addColumn('created_by', function ($record) {
            return $record->creatorName();
        })
        ->addColumn('action', function ($record) {
            $btn = '';
            
            if(auth()->user()->hasRole(['admin'])){
	            $btn .= $this->makeButton([
	                'type' => 'edit',
	                'id'   => $record->id
	            ]);
	            // Delete
	            $btn .= $this->makeButton([
	                'type' => 'delete',
	                'id'   => $record->id
	            ]);
	        }else{
	        	$btn = '-';
	        }

            return $btn;
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.satuan.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.master.satuan.create');
    }

    public function store (SatuanRequest $request)
    {
        $record = new Satuan;
        $record->fill($request->all());
        $record->save();

        return response([
            'status' => true
        ]);
    }

    public function edit($id)
    {
        $record = Satuan::find($id);

        return $this->render('modules.master.satuan.edit', [
            'record' => $record
        ]);
    }

    public function show($id)
    {
        // $record = Role::find($id);

        // return $this->render('modules.master.satuan.detail', [
        //     'record' => $record
        // ]);
    }

    public function update (SatuanRequest $request, $id)
    {
        $satuan = Satuan::find($id);
        $satuan->fill($request->all());
        $satuan->save();

        return response([
            'status' => true
        ]);
    }

    public function destroy($id)
    {
        $satuan = Satuan::find($id);
        if($satuan->ppdetail->count() > 0){
        	return response([
		            'status' => true,
		    ], 500);
        }else{
        	if($satuan->ujiserahterimadetail->count() > 0){
        		return response([
		            'status' => true,
		        ], 500);
        	}else{
        		$satuan->delete();
        		return response([
		            'status' => true,
		        ], 200);
        	}
        }
    }
}
