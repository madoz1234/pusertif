<?php

namespace App\Http\Controllers\Master;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\Master\PerusahaanRequest;

/* Models */
use App\Models\Model;
use App\Models\Authentication\Role;
use App\Models\Master\Perusahaan;
use App\Models\Master\Pelanggan;

/* Libraries */
use DataTables;
use Session;
use Validator;

class PerusahaanController extends Controller
{
    protected $link = 'master/perusahaan/';
    protected $perms = 'master-perusahaan';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Perusahaan");
        $this->setPerms($this->perms);
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Master' => '#', 'Perusahaan' => '#']);

        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'kode',
                'name' => 'kode',
                'label' => 'Kode Perusahaan',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama Perusahaan',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
                'width' => '200px',
            ],
            [
                'data' => 'no_tlp',
                'name' => 'no_tlp',
                'label' => 'No. Telpon Perusahaan',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'alamat',
                'name' => 'alamat',
                'label' => 'Alamat Perusahaan',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
                'width' => '200px',
            ],
            [
                'data' => 'kategori',
                'name' => 'kategori',
                'label' => 'Kategori',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
                'width' => '80px',
            ],
            [
                'data' => 'email',
                'name' => 'email',
                'label' => 'Email',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
                'width' => '150px',
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
                'width' => '120px',
            ],
            [
            	'data' => 'created_by',
            	'name' => 'created_by',
            	'label' => 'Dibuat Oleh',
            	'searchable' => false,
            	'sortable' => true,
            	'className' => "center aligned",
            	'width' => '100px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '80px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Perusahaan::select('*');
        
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'like', '%' . $nama . '%');
        }

        $records = $records->get();
        

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('alamat', function ($record) use ($request) {
            return '<span class="ccount more" data-ccount="80">'.readMoreText(strip_tags($record->alamat),150).'</span>';
        })
        ->addColumn('status', function ($record) use ($request) {
        	$perusahaan ='-';
        	if($record->status == 1){
        		$perusahaan ='Aktif';
        	}else{
        		$perusahaan ='Tidak Aktif';
        	}
            return $perusahaan;
        })
        ->addColumn('kategori', function ($record) use ($request) {
        	$kategori ='-';
        	if($record->kategori == 0){
        		$kategori ='PLN';
        	}elseif($record->kategori == 1){
        		$kategori ='NON-PLN';
        	}elseif($record->kategori == 2){
        		$kategori ='A-PLN';
        	}else{
        		$kategori ='-';
        	}
            return $kategori;
        })
        ->addColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->addColumn('created_by', function ($record) {
            return $record->creatorName();
        })
        ->addColumn('action', function ($record) {
            $btn = '';
            
            if(auth()->user()->hasRole(['admin'])){
	            $btn .= $this->makeButton([
	                'type' => 'edit',
	                'id'   => $record->id
	            ]);
	            // Delete
	            $btn .= $this->makeButton([
	                'type' => 'delete',
	                'id'   => $record->id
	            ]);
	        }else{
	        	$btn = '-';
	        }

            return $btn;
        })
        ->rawColumns(['action','alamat'])
        ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.perusahaan.index', ['mockup' => false]);
    }

    public function create()
    {
    	$no_perusahaan 			= Perusahaan::generatePerusahaan();
        return $this->render('modules.master.perusahaan.create',[
            'no_perusahaan' => $no_perusahaan,
        ]);
    }

    public function store (PerusahaanRequest $request)
    {
        $p = new Perusahaan;

        if ($request->nama){
            $exist = Perusahaan::where('nama' ,$request->nama)
                                ->where('alamat',$request->alamat)
                                ->first();
            if (!$exist) {
                $p           = new Perusahaan;
                $p->kode     = $request->kode;
                $p->nama     = $request->nama;
                $p->no_tlp   = $request->no_tlp;
                $p->alamat   = $request->alamat;
                $p->status   = $request->status;
                $p->kategori = $request->kategori;
                $p->email    = $request->email;
                $p->save();
            }
        }

        return response([
            'status' => true
        ]);

    }

    public function edit($id)
    {
        $record = Perusahaan::find($id);

        return $this->render('modules.master.perusahaan.edit', [
            'record' => $record
        ]);
    }

    public function show($id)
    {
        // $record = Role::find($id);

        // return $this->render('modules.master.perusahaan.detail', [
        //     'record' => $record
        // ]);
    }

    public function update (PerusahaanRequest $request, $id)
    {
        $perusahaan = Perusahaan::find($id);
        $perusahaan->fill($request->all());
        $perusahaan->save();

        return response([
            'status' => true
        ]);
    }

    public function destroy($id)
    {
        $perusahaan = Perusahaan::find($id);
        if($perusahaan){
        	$cari = Pelanggan::where('perusahaan_id', $id)->get()->count();
        	if($cari > 0){
	        	return response([
                	'status' => true,
            	],500);
        	}else{
        		$perusahaan->delete();
	        	return response([
	            	'status' => true,
		        ],200);
        	}
        }else{
        	return response([
                'status' => true,
            ],500);
        }
    }
}
