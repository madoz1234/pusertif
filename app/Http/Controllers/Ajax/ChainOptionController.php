<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\JenisPengujian;
use App\Models\Master\Lingkup;
use App\Models\Master\Perusahaan;
use App\Models\Master\Pelanggan;
use App\Models\Master\Prioritas;
use App\Models\KajiUlang\KajiUlang;
use App\Models\KajiUlang\Detail;
use App\Models\Authentication\User;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\FrontEnd\PendaftaranPengujianDetail;
use App\Models\LaporanPengujian\LaporanPengujian;


use Datatables;

class ChainOptionController extends Controller
{
    
    function __construct()
    {
        
    }

    public function getLingkup(Request $request)
    {
        $layanan = $request->jenis_pelayanan_id;
        $lingkup_id = $request->lingkup_id;
        $selected = $request->selected;
        if($lingkup_id){
        	return Lingkup::options('nama', 'id', [
	            'selected' => ($lingkup_id ? $lingkup_id : null)
	        ]);
        }else{
	        return Lingkup::options('nama', 'id', [
	            'filters' => [
	                function ($q) use ($layanan) {
	                    $q->where('jenis_pelayanan_id', $layanan);
	                }
	            ],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }
    }

    public function getLingkupOrder(Request $request)
    {
        $layanan = $request->jenis_pelayanan_id;
        $lingkup_id = $request->lingkup_id;
        $selected = $request->selected;
        if($lingkup_id){
        	return Lingkup::options('nama', 'id', [
	            'selected' => ($lingkup_id ? $lingkup_id : null)
	        ]);
        }else{
	        return Lingkup::options('nama', 'id', [
	            'filters' => [
	                function ($q) use ($layanan) {
	                    $q->where('jenis_pelayanan_id', $layanan)
	                     ->where('nama', 'Uji Serah Terima');
	                }
	            ],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }
    }

    public function getPrioritas(Request $request)
    {
        $data = Prioritas::where('layanan_id', $request->jenis_pelayanan_id)->first();
        return $data->waktu;
    }

    public function getJenisPengujian(Request $request)
    {
        $lingkup = $request->lingkup_id;
        $selected = $request->selected;
        return JenisPengujian::options('nama', 'id', [
            'filters' => [
                function ($q) use ($lingkup) {
                    $q->where('lingkup_id', $lingkup);
                }
            ],
            'selected' => $selected
        ], '(Pilih Salah Satu)');
    }

    public function getJenisPengujianOrder(Request $request)
    {
        $lingkup = $request->lingkup_id;
        $selected = $request->selected;
        return JenisPengujian::options('nama', 'id', [
            'filters' => [
                function ($q) use ($lingkup) {
                    $q->where('lingkup_id', $lingkup)
                      ->where('nama', 'Uji Serah Terima');
                }
            ],
            'selected' => $selected
        ], '(Pilih Salah Satu)');
    }

    public function getPelanggan(Request $request)
    {
        $perusahaan = $request->perusahaan_id;
        $selected = $request->selected;
        return Pelanggan::options('nama_lengkap', 'user_id', [
            'filters' => [
                function ($q) use ($perusahaan) {
                    $q->where('perusahaan_id', $perusahaan)
                      ->where('status', 0);
                }
            ],
            'selected' => $selected
        ], '(Pilih Salah Satu)');
    }

    public function getPelangganSpm(Request $request)
    {
        $perusahaan = $request->perusahaan_id;
        $selected = $request->selected;
        return Pelanggan::options('nama_lengkap', 'user_id', [
            'filters' => [
                function ($q) use ($perusahaan) {
                    $q->where('perusahaan_id', $perusahaan)
                      ->where('status', 0);
                }
            ],
            'selected' => $selected
        ], '(Pilih Salah Satu)');
    }

    public function getDisposisi(Request $request)
    {
        $dispo_id = $request->dispo_id;
        $pengirim = $request->pengirim;
        $layanan = $request->layanan;
        $selected = $request->selected;
        if($request->dispo_id == 1){
        	return User::options('nama', 'id', [
	            'filters' => [
	                function ($q) use ($pengirim) {
	                    $q->aksesa($pengirim);
	                }
	            ],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }elseif($request->dispo_id == 2){
	        return User::options('nama', 'id', [
	            'filters' => [
	                function ($q) use ($layanan) {
	                    $q->akses($layanan);
	                }
	            ],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }else{

        }
    }

    public function getDisposisiMsb(Request $request)
    {
        $dispo_id = $request->dispo_id;
        $pengirim = $request->pengirim;
        $layanan = $request->layanan;
        $selected = $request->selected;
        if($request->dispo_id == 1){
        	return User::options('nama', 'id', [
	            'filters' => [
	                function ($q) use ($pengirim) {
	                    $q->aksesa($pengirim);
	                }
	            ],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }elseif($request->dispo_id == 2){
	        return User::options('nama', 'id', [
	            'filters' => [
	                function ($q) use ($layanan) {
	                    $q->dispoMsb($layanan);
	                }
	            ],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }else{

        }
    }

    public function getPerbaikanMsb(Request $request)
    {
        $dispo_id = $request->dispo_id;
        $pengirim = $request->pengirim;
        $layanan = $request->layanan;
        $selected = $request->selected;
        if($request->dispo_id == 1){
            if(auth()->user()->hasRole(['msb-kalibrasi', 'msb-siskit', 'msb-sistgi', 'msb-tegangan-rendah', 'msb-tegangan-tinggi'])){
                $pp = PendaftaranPengujian::find($request->pp_id);
                $arrayuser = [];
                if($pp){
                    if($pp->jenis==0){
                        $user = User::whereHas('roles', function ($query) {
                                    $query->where('name', 'yan-kalibrasi');
                                })->get();
                        $arrayuser = $user->pluck('id');
                    }else{
                        $user = User::whereHas('roles', function ($query) {
                                    $query->where('name', 'yan-uji');
                                })->get();
                        $arrayuser = $user->pluck('id');
                    }
                }

                return User::options('nama', 'id', [
                    'filters' => [
                        function ($q) use ($arrayuser) {
                            $q->whereIn('id',$arrayuser);
                        }
                    ],
                    'selected' => $selected
                ], '(Pilih Salah Satu)');
            }elseif(auth()->user()->hasRole(['asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-tinggi','asman-dal-tegangan-rendah', 'asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-tinggi','asman-lola-tegangan-rendah'])){
                return User::options('nama', 'id', [
                    'filters' => [
                        function ($q) use ($layanan) {
                            $q->akses($layanan);
                        }
                    ],
                    'selected' => $selected
                ], '(Pilih Salah Satu)');
            }

        }else{

        }
    }

    public function getVa(Request $request)
    {
        $data = PendaftaranPengujian::find($request->kaji_ulang);
        $cari_no = PendaftaranPengujian::where('no_surat', $data->no_surat)->get();
        $wbs = '';
        foreach ($cari_no as $key => $value) {
        	if($value->kaji_ulang){
        		if($value->kaji_ulang->va){
        			if($wbs){
	        			$wbs = $value->kaji_ulang->va->wbs_io;
        			}else{
        				$wbs .= $value->kaji_ulang->va->wbs_io;
        			}
        		}else{
        			$wbs .='';
        		}
        	}else{
				$wbs .= '';
        	}
        }
        return $wbs;
    }

    public function getcekVa(Request $request)
    {
        $data = PendaftaranPengujian::find($request->id[0]);
        if($data->kaji_ulang->surat->va){
        	return 1;
        }else{
        	return 0;
        }
    }

    public function getcekVa2(Request $request)
    {
        $data = PendaftaranPengujian::find($request->id);
        if($data->kaji_ulang->surat->va){
        	return 1;
        }else{
        	return 0;
        }
    }

    public function getDetailPP(Request $request)
    {	
    	$id = $request->id;
        $data = PendaftaranPengujianDetail::with('satuan')->find($id);
        return $data;
    }

    public function nextGetPengujian($id)
    {
        return JenisPengujian::where('jenis_pelayanan_id',$id)->get();
    }


    public function getJadwalKaji(Request $request){
        $layanan_id = null;
        if(auth()->user()->hasRole(['asman-dal-kalibrasi']) || auth()->user()->hasRole(['asman-lola-kalibrasi']) || auth()->user()->hasRole(['msb-kalibrasi'])){
            $layanan_id = [1];
        }
        if(auth()->user()->hasRole(['asman-dal-siskit']) || auth()->user()->hasRole(['asman-lola-siskit']) || auth()->user()->hasRole(['msb-siskit'])){
            $layanan_id = [2];
        }
        if(auth()->user()->hasRole(['asman-dal-sistgi']) || auth()->user()->hasRole(['asman-lola-sistgi']) || auth()->user()->hasRole(['msb-sistgi'])){
            $layanan_id = [3];
        }
        if(auth()->user()->hasRole(['asman-dal-tegangan-rendah']) || auth()->user()->hasRole(['asman-lola-tegangan-rendah']) || auth()->user()->hasRole(['msb-tegangan-rendah'])){
            $layanan_id = [4];
        }
        if(auth()->user()->hasRole(['asman-dal-tegangan-tinggi']) || auth()->user()->hasRole(['asman-lola-tegangan-tinggi']) || auth()->user()->hasRole(['msb-tegangan-tinggi'])){
            $layanan_id = [5];
        }

        $jenis=[];

        if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['yan-kalibrasi'])){
            if(auth()->user()->hasRole(['yan-kalibrasi'])){
                $layanan_id = [1];
            }else{
                $layanan_id = [2,3,4,5];
            }
        }else{
            $layanan_id = [1,2,3,4,5];
        }
        if($request->perusahaan_id){
        	$pt = Perusahaan::where('status', 1)->whereHas('pelanggan', function($u){
	        					$u->where('status', 0);
	        				})->where('id', $request->perusahaan_id)->first();
        	
        	$arraypt = [];
	        $arrayjadwal = [];
	        $pelanggan_id =[];
	        if($pt){
	        	foreach ($pt->pelanggan as $value) {
		        	$pelanggan_id[] = $value->user_id;
	        	}

	            $kaji = Detail::whereHas('kajiulang', function ($kajiulang) use ($pelanggan_id, $layanan_id) {
	                                $kajiulang->whereHas('surat', function ($surat) {
	                                    $surat->whereHas('va', function ($va) {
	                                        $va->whereHas('konfirmasi', function ($konfirmasi) {
	                                            $konfirmasi->whereNotNull('wbs_io');
	                                        });
	                                    });
	                                })->whereHas('pp', function($u) use ($pelanggan_id, $layanan_id){
	                                	$u->whereIn('user_id', $pelanggan_id)
	                                	  ->whereIn('layanan_id', $layanan_id);
	                                });
	                            })->get();
	           	$u=[];
	           	foreach ($kaji as $kiy => $vil) {
	           		$u[]=$vil->detail_pendaftaran->jenis_id;
	           	}
	           	$a=array_count_values($u);
	            foreach ($kaji as $key => $k) {
		            $arraypt[] = [
		                'id' => $k->detail_pendaftaran->jenis_id,
		                'building' => $pt->nama,
		                'title' => ($k->detail_pendaftaran->jenis->nama.' ('.$a[$k->detail_pendaftaran->jenis_id].')'),
		                'resourceId' => $k->detail_pendaftaran->jenis_id,
		            ];
	                $arrayjadwal[] = [
	                    'id' => $k->detail_pendaftaran->jenis_id,
	                    'title' => $k->detail_pendaftaran->pp->no_order,
	                    'start' => $k->tentative_start,
	                    'end' => $k->tentative_end,
	                    'resourceId' => $k->detail_pendaftaran->jenis_id,
	                ];
	            }
	        }
        }else{
	        $pt = Perusahaan::where('status', 1)->whereHas('pelanggan', function($u){
	        					$u->where('status', 0);
	        				})->get();
	        $arraypt = [];
	        $arrayjadwal = [];
	        $pelanggan_id =[];
	        foreach ($pt as $j) {
	        	foreach ($j->pelanggan as $value) {
		        	$pelanggan_id[] = $value->user_id;
	        	}

	            $kaji = Detail::whereHas('kajiulang', function ($kajiulang) use ($pelanggan_id, $layanan_id) {
	                                $kajiulang->whereHas('surat', function ($surat) {
	                                    $surat->whereHas('va', function ($va) {
	                                        $va->whereHas('konfirmasi', function ($konfirmasi) {
	                                            $konfirmasi->whereNotNull('wbs_io');
	                                        });
	                                    });
	                                })->whereHas('pp', function($u) use ($pelanggan_id, $layanan_id){
	                                	$u->whereIn('user_id', $pelanggan_id)
	                                	  ->whereIn('layanan_id', $layanan_id);
	                                });
	                            })->get();
	           	$i=0;
	           	$u=[];
	           	foreach ($kaji as $kiy => $vil) {
	           		$u[]=$vil->detail_pendaftaran->jenis_id;
	           	}
	           	$a=array_count_values($u);
	            foreach ($kaji as $key => $k) {
		            $arraypt[] = [
		                'id' => $k->detail_pendaftaran->jenis_id,
		                'building' => $j->nama,
		                'title' => ($k->detail_pendaftaran->jenis->nama.' ('.$a[$k->detail_pendaftaran->jenis_id].')'),
		                'resourceId' => $k->detail_pendaftaran->jenis_id,
		            ];
	                $arrayjadwal[] = [
	                    'id' => $k->detail_pendaftaran->jenis_id,
	                    'title' => $k->detail_pendaftaran->pp->no_order,
	                    'start' => $k->tentative_start,
	                    'end' => $k->tentative_end,
	                    'resourceId' => $k->detail_pendaftaran->jenis_id,
	                ];
	            }

	        }
        }
        return response([
            'jenis' => $arraypt,
            'jadwal' => $arrayjadwal
        ]);
    }


    public function getLayananUser(){
        $layanan_id = [1,2,3,4,5];
        if(auth()->user()->hasRole(['asman-dal-kalibrasi']) || auth()->user()->hasRole(['asman-lola-kalibrasi']) || auth()->user()->hasRole(['msb-kalibrasi']) || auth()->user()->hasRole(['adminlab-kalibrasi']) || auth()->user()->hasRole(['pelaksana-kalibrasi'])){
            $layanan_id = [1];
        }
        if(auth()->user()->hasRole(['asman-dal-siskit']) || auth()->user()->hasRole(['asman-lola-siskit']) || auth()->user()->hasRole(['msb-siskit']) || auth()->user()->hasRole(['adminlab-siskit']) || auth()->user()->hasRole(['pelaksana-siskit'])){
            $layanan_id = [2];
        }
        if(auth()->user()->hasRole(['asman-dal-sistgi']) || auth()->user()->hasRole(['asman-lola-sistgi']) || auth()->user()->hasRole(['msb-sistgi']) || auth()->user()->hasRole(['adminlab-sistgi']) || auth()->user()->hasRole(['pelaksana-sistgi'])){
            $layanan_id = [3];
        }
        if(auth()->user()->hasRole(['asman-dal-tegangan-rendah']) || auth()->user()->hasRole(['asman-lola-tegangan-rendah']) || auth()->user()->hasRole(['msb-tegangan-rendah']) || auth()->user()->hasRole(['adminlab-tegangan-rendah']) || auth()->user()->hasRole(['pelaksana-tegangan-rendah'])){
            $layanan_id = [4];
        }
        if(auth()->user()->hasRole(['asman-dal-tegangan-tinggi']) || auth()->user()->hasRole(['asman-lola-tegangan-tinggi']) || auth()->user()->hasRole(['msb-tegangan-tinggi']) || auth()->user()->hasRole(['adminlab-tegangan-tinggi']) || auth()->user()->hasRole(['pelaksana-tegangan-tinggi'])){
            $layanan_id = [5];
        }
        if(auth()->user()->hasRole(['yan-kalibrasi'])){
            $layanan_id = [1];
        }
        if(auth()->user()->hasRole(['yan-uji'])){
            $layanan_id = [2,3,4,5];
        }

        return $layanan_id;
    }

    public function getChartKaji(){
        $diskusi = 0;
        $progres = 0;
        $layanan_id = $this->getLayananUser();

        $user = auth()->user();
        if($user->hasRole(['msb-kalibrasi', 'msb-siskit', 'msb-sistgi', 'msb-tegangan-rendah', 'msb-tegangan-tinggi'])){
            $diskusi = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
                                                $q->where('penerima_id', $user->id)
                                                  ->where('jenis', 1);
                                            })
                                            ->where('status', 2)
                                            ->whereHas('kaji_ulang', function($u){
                                                $u->where('keputusan', 3);
                                            })
                                            ->count();

            $progres = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
                                                $q->where('penerima_id', $user->id)
                                                  ->where('jenis', 1);
                                            })
                                            ->whereIn('status', [2,12])
                                            ->doesntHave('kaji_ulang')
                                            ->count();
        }elseif($user->hasRole(['asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-tinggi','asman-dal-tegangan-rendah', 'asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-tinggi','asman-lola-tegangan-rendah'])){
            $diskusi = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
                                                $q->where('penerima_id', $user->id)
                                                  ->where('jenis', 2);
                                            })
                                            ->where('status', 2)
                                            ->whereHas('kaji_ulang', function($u){
                                                $u->where('keputusan', 3);
                                            })
                                            ->count();

            $progres = PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
                                                $q->where('penerima_id', $user->id)
                                                  ->where('jenis', 2);
                                            })
                                            ->where('status', 2)
                                            ->doesntHave('kaji_ulang')
                                            ->count();
        }else{
            $user = User::get();
            foreach ($user as $u) {
                if($u->hasRole(['msb-kalibrasi', 'msb-siskit', 'msb-sistgi', 'msb-tegangan-rendah', 'msb-tegangan-tinggi'])){
                    $diskusi += PendaftaranPengujian::whereHas('act_dispo', function($q) use ($u){
                                                $q->where('penerima_id', $u->id)
                                                  ->where('jenis', 1);
                                            })
                                            ->where('status', 2)
                                            ->whereHas('kaji_ulang', function($u){
                                                $u->where('keputusan', 3);
                                            })
                                            ->count();

                    $progres += PendaftaranPengujian::whereHas('act_dispo', function($q) use ($u){
                                                        $q->where('penerima_id', $u->id)
                                                          ->where('jenis', 1);
                                                    })
                                                    ->whereIn('status', [2,12])
                                                    ->doesntHave('kaji_ulang')
                                                    ->count();
                }
            }
        }

        $selesai = PendaftaranPengujian::whereIn('layanan_id',$layanan_id)
                                        ->whereHas('kaji_ulang', function($query){
                                            $query->where('keputusan','!=',3);
                                        })
                                        ->whereHas('act_dispo', function($query){
                                            $query->where('jenis',2);
                                        })
                                        ->count();
        return response([
            'selesai' => $selesai,
            'belum' => $diskusi+$progres
        ]);
    }


    public function getChartPengujian(){
        // $belum = 0;
        $layanan_id = $this->getLayananUser();

        $belum = PendaftaranPengujian::whereIn('layanan_id',$layanan_id)
                                        ->whereHas('kaji_ulang', function($query){
                                            $query->whereHas('surat', function($surat){
                                                $surat->whereHas('va', function($va){
                                                    $va->whereHas('konfirmasi', function($konfirmasi){
                                                        $konfirmasi->whereHas('penerimaan', function($penerimaan){
                                                            $penerimaan->whereHas('pengujian', function($pengujian){
                                                                            $pengujian->where('status_data_pengujian',0);
                                                                        })
                                                                        ->orWhere(function($condition){
                                                                            $condition->where('status_pengujian',0);
                                                                        });
                                                            // where('status_pengujian',0);
                                                        });
                                                    });
                                                });
                                            });
                                        })
                                        ->count();

        $selesai = PendaftaranPengujian::whereIn('layanan_id',$layanan_id)
                                        ->whereHas('kaji_ulang', function($query){
                                            $query->whereHas('surat', function($surat){
                                                $surat->whereHas('va', function($va){
                                                    $va->whereHas('konfirmasi', function($konfirmasi){
                                                        $konfirmasi->whereHas('penerimaan', function($penerimaan){
                                                            // $penerimaan->where('status_pengujian',1);
                                                            $penerimaan->whereHas('pengujian', function($pengujian){
                                                                $pengujian->where('status_data_pengujian',1);
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        })
                                        ->count();
        return response([
            'selesai' => $selesai,
            'belum' => $belum
        ]);
    }

    public function getChartLaporan(){
        // $belum = 0;
        $layanan_id = $this->getLayananUser();

        $belum = LaporanPengujian::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($layanan_id){
            $pp->whereIn('layanan_id',$layanan_id);
        })
                                        ->where('status',0)
                                        ->count();

        $selesai = LaporanPengujian::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($pp) use ($layanan_id){
            $pp->whereIn('layanan_id',$layanan_id);
        })
                                        ->where('status',1)
                                        ->count();
        return response([
            'selesai' => $selesai,
            'belum' => $belum
        ]);
    }

}