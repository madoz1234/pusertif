<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Translators;


use Datatables;
use Lang;
use Session;
use URL;
use Config;
use Auth;

class LanguageController extends Controller
{
  
    public function show(Request $request){
        // dd($request->all());
        Session::put('locale',$request->translator);
        Lang::setLocale(Session::get('locale')); 

        try {
            $data['translator'] = $request->translator;
            if(Auth::check()){
                $save = Translators::where('created_by',auth()->user()->id)->first();
            }else{
              $data['created_by'] = $request->ip;
              $save = Translators::where('created_by',999)->first();  
              
            }
            if(!isset($save)){
                $save = new Translators;
            }
            $save->fill($data);
            $save->save();
          
        }catch (\Exception $e) {
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'status' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true
        ]);
    }
}
