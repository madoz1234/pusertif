<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\FrontEnd\ProfileRequest;
use App\Models\Authentication\User;
use App\Models\Master\Pelanggan;
use App\Models\Master\Perusahaan;
use App\Models\Users;
use Carbon\Carbon;
use Datatables;
use Entrust;
use Hash;

class ProfileController extends Controller
{
    protected $link = '/frontend/profile/';
    // protected $perms = 'konfigurasi-profile';

    function __construct()
    {
        $this->setLink($this->link);
        // $this->setPerms($this->perms);
        $this->setTitle("Profile Pengguna");
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Profile Pengguna' => '#']);
    }

    public function index()
    {
        // $user = User::with('pelanggan')->find(auth()->user());
        $user = auth()->user();
        // dd($user);
        // $ongoing   = WorkingPermit::relatedTo($user)
        //                           ->onGoing()
        //                           ->get();
        // $done      = WorkingPermit::relatedTo($user)
        //                           ->done()
        //                           ->get();
        $this->setTitle(trans('translator.Profile Pengguna'));
        $this->setBreadcrumb([trans('translator.Profile Pengguna') => '#']);
        return $this->render('frontend.profile.index', [
            'user'      => $user, 
            'pelanggan' => $user->pelanggan()->first(), 
            // 'ongoing'   => $ongoing,
            // 'done'      => $done
        ]);
    }

    public function store(ProfileRequest $request)
    {   
        $user = auth()->user();
        $user->username = $request->user['username'];
        $user->nama = $request->pelanggan['nama_lengkap'];
        $user->email = $request->pelanggan['email'];
        $user->no_tlp = $request->pelanggan['no_hp'];
        if($file = $request->user['photo']){
            $photo = $file->store('uploads/user', 'public');
            $user->photo = $photo;
        }
        if($request->old_password && !Hash::check($request->old_password, $user->password)){
            return response([
                'message' => 'Password Lama tidak sesuai',
                'errors' => [
                    'old_password' => ['Password Lama tidak sesuai']
                ]
            ], 422);
        }elseif($request->password && $request->password == $request->confirm_password){
            $user->password = bcrypt($request->password);
        }
            
        // $user->password = bcrypt($request->password);
        // $user->last_activity = Carbon::now();
        $user->save();
        
        $pelanggan = Pelanggan::where('user_id',$user->id)->first();
        $pelanggan->nama_lengkap = $request->pelanggan['nama_lengkap'];
        $pelanggan->no_hp = $request->pelanggan['no_hp'];
        $pelanggan->email = $request->pelanggan['email'];
        $pelanggan->save();


        return response([
            'status' => true
        ]);
    }

    public function reads()
    {
        auth()->user()->unreadNotifications->markAsRead();

        return response([
            'status' => true
        ]);
    }
}
