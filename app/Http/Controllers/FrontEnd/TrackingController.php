<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FrontEnd\PendaftaranPengujian;
use DB;

class TrackingController extends Controller
{
    protected $link = 'tracking/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle(trans('translator.Tracking'));
        $this->setBreadcrumb([trans('translator.Tracking') => '#']);

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$this->setTitle(trans('translator.Tracking'));
        $this->setBreadcrumb([trans('translator.Tracking') => '#']);
        return $this->render('frontend.tracking.index');
    }

    public function search(Request $request)
    {
        $record = PendaftaranPengujian::when($no_order = $request->id, function($q) use ($no_order) {
                                             $q->where('no_order',$no_order);
                                        })
                                        ->when($no_surat = $request->surat, function($q) use ($no_surat) {
                                             $q->where('no_surat',$no_surat);
                                        })
                                        ->where('tipe_surat', 0)
                                        ->get();

        $data = [
            'records' => $record,
        ];

        return view('frontend.tracking.list', $data);
    }

}
