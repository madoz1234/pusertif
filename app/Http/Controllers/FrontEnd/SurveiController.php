<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
/* Models */
use App\Models\Model;
use App\Models\Master\Survei;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\FrontEnd\TransSurvei;

/* Libraries */
use DataTables;
use Carbon;
use Auth;
use DB;

class SurveiController extends Controller
{
    protected $link = 'frontend/survei/';

    function __construct()
    {
        if (Auth::check()){
            if(auth()->users()){
                $this->middleware('auth');
            }
        }
        $this->setLink($this->link);
        $this->setTitle("Survey");
        $this->setModalSize("small");
        $this->setBreadcrumb(['Survei' => '#']);

        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'no_order',
                'name' => 'no_order',
                'label' => 'No Pesanan',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'pemesan',
                'name' => 'pemesan',
                'label' => 'Peminta Jasa',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '120px',
            ],
            [
                'data' => 'layanan',
                'name' => 'layanan',
                'label' => 'Layanan',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '120px',
            ],
            [
                'data' => 'lingkup',
                'name' => 'lingkup',
                'label' => 'Lingkup',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '120px',
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '150px',
            ],
            // [
            //     'data' => 'rating',
            //     'name' => 'rating',
            //     'label' => 'Rating',
            //     'searchable' => false,
            //     'sortable' => true,
            //     'className' => "center aligned",
            //     'width' => '150px',
            // ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '100px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = PendaftaranPengujian::where('created_by', auth()->user()->id)
                                        ->where('status', 6)
                                        ->where('tipe_surat', 0)
                                        ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        if ($no_order = $request->no_order) {
            $records->where('no_order', $no_order);
        }


        return DataTables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('pemesan', function ($record) use ($request) {
            $pemesan = $record->user->pelanggans->nama_lengkap.'</br>'.$record->user->pelanggans->no_tlp;
            return $pemesan;
        })
        ->addColumn('layanan', function ($record) use ($request) {
            return $record->pelayanan->nama;
        })
        ->addColumn('lingkup', function ($record) use ($request) {
            return $record->lingkup->nama;
        })
        ->addColumn('status', function ($record) use ($request) {
            return '<a class="ui green tag label">'.trans('translator.Selesai').'</a>';
        })
        ->addColumn('rating', function ($record) use ($request) {
            // $average = 0;
            // if ($record->survei) {
            //     $data_count = $record->survei->survei->detail()->count();
            //     $data_sum = $record->survei->sum;
            //     $average = $data_sum / $data_count;
            // }
            // $average = (int)round($average);
            // return '<div class="ui star rating" data-rating="5" data-max-rating="5"></div>';
        })
        ->addColumn('action', function ($record) {
            $btn = '';
            if($record->survei){
            	 $btn = '-';
            }else{
	            $btn .= $this->makeButton([
	                'type' => 'url',
	                'class' => 'orange icon',
	                'label' => trans('translator.Ulasan'),
	                'url' => url($this->link.$record->id.'/edit')
	            ]);
            }

            return $btn;
        })
        ->rawColumns(['status','pemesan','rating','action'])
        ->make(true);
    }

    public function index()
    {
        $this->setTitle(trans('translator.Survei'));
        $this->setBreadcrumb([trans('translator.Survei') => '#']);
        return $this->render('frontend.survei.index', ['mockup' => false,]);
    }

    public function edit($id)
    {
        $record = Survei::with('detail')->where('status', 1)->first();

        $data = [
            'record' => $record,
            'pp_id' => $id
        ];

        $this->setTitle(trans('translator.Survei'));
        $this->setBreadcrumb([trans('translator.Survei') => '#']);
        return $this->render('frontend.survei.create',$data);
    }

    public function store (Request $request)
    {
    	$sum =0;
        DB::beginTransaction();
        try{
        	foreach ($request->detail as $key => $value) {
        		$sum += $value['jawaban'];
        	}
            $data = new TransSurvei;
            $data->pp_id 		= $request->pp_id;
            $data->survei_id  	= $request->survei_id;
            $data->sum 			= $sum;
            $data->save();

            DB::commit();
            return response([
                'status' => true
            ]);
        }catch(\Exception $e){
            return $e;
            DB::rollback();
            return response([
                'status' => true,
                'errors' => $e 
            ]);
        }
    }
}
