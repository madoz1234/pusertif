<?php

namespace App\Http\Controllers\FrontEnd;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\FrontEnd\AccountSettingRequest;
/* Models */
use App\Models\Authentication\Role;
use App\Models\Master\Pelanggan;
use App\Models\Authentication\User;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;

class AccountSettingController extends Controller
{
    protected $link = 'frontend/account-setting/';

    function __construct()
    {
        if (Auth::check()){
            if(auth()->users()){
                $this->middleware('auth');
            }
        }
        $this->setLink($this->link);
        $this->setTitle("Account Setting");
        $this->setModalSize("tiny");
        $this->setBreadcrumb(['Account Setting' => '#']);

// Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama_lengkap',
                'name' => 'nama_lengkap',
                'label' => 'Nama',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
                'width' => '200px',
            ],
            [
                'data' => 'no_hp',
                'name' => 'no_hp',
                'label' => 'No HP',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'email',
                'name' => 'email',
                'label' => 'Email',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
                'width' => '150px',
            ],
            [
                'data' => 'perusahaan',
                'name' => 'perusahaan',
                'label' => 'Perusahaan',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '200px',
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '120px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'searchable' => false,
                'className' => "center aligned",
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '100px',
            ]
        ]);
    }

    public function grid(Request $request)
    {	
    	$perusahaan = Pelanggan::where('user_id', auth()->user()->id)->first();
        $records = Pelanggan::where('perusahaan_id', ($perusahaan) ? $perusahaan->perusahaan_id : 0)->select('*');
        if ($nama = $request->nama) {
            $records->where('nama_lengkap','like', '%'.$nama.'%');
      	}
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        if ($no_order = $request->no_order) {
            $records->where('no_order', $no_order);
        }


        return DataTables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('perusahaan', function ($record) use ($request) {
            $perusahaan = $record->perusahaan->nama;
            return $perusahaan;
        })
        ->addColumn('status', function ($record) use ($request) {
        	if($record->status == 0){
	            return 'Aktif';
        	}else{
        		return 'Tidak Aktif';
        	}
        })
        ->editColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->addColumn('action', function ($record) {
            $btn = '';
            
            $btn .= $this->makeButton([
                'type' => 'edit',
                'id'   => $record->id
            ]);
            // Delete
            // $btn .= $this->makeButton([
            //     'type' => 'delete',
            //     'id'   => $record->id
            // ]);

            return $btn;
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public function index()
    {
    	$this->setTitle(trans('translator.Pengaturan Akun'));
    	$this->setBreadcrumb([trans('translator.Pengaturan Akun') => '#']);
        return $this->render('frontend.account-setting.index', ['mockup' => false]);
    }

    public function edit($id)
    {
        $record = Pelanggan::find($id);
        return $this->render('frontend.account-setting.edit',['record' => $record]);
    }

    public function update(AccountSettingRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $pelanggan = Pelanggan::find($id);
            $pelanggan->nama_lengkap = $request->nama;
            $pelanggan->email = $request->email;
            $pelanggan->no_hp = $request->no_hp;
            if(is_null($request->status)){
        		$pelanggan->status = 0;
        	}else{
        		$pelanggan->status = 1;
        	}
        	$pelanggan->save();
            if ($pelanggan) {
                $users = User::find($pelanggan->user_id);
                $users->username = $request->email;
                $users->email    = $request->email;
                $users->status   = ($pelanggan->status == 0) ? 1 : 0;
                $users->save();
            }

            DB::commit();
            return response([
                'status' => true
            ]);

        }catch(\Exception $e){
        	return $e;
            DB::rollback();
            return response([
                'status' => true,
                'errors' => $e 
            ]);
        }
    }

    // public function destroy($id)
    // {
    //     $record = Pelanggan::find($id);
    //     $record->delete();
    //     return response([
    //         'status' => true,
    //     ]);
    // }
}
