<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\FrontEnd\PendaftaranPengujianRequest;
use App\Http\Requests\FrontEnd\CekItemUjiRequest;

/* Models */
use App\Models\Authentication\Role;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\FrontEnd\PendaftaranPengujianDetail;
use App\Models\Master\JenisPelayanan;
use App\Models\Master\JenisPengujian;
use App\Models\Master\Lingkup;
use App\Models\Picture;
use App\Models\Files;


/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;

class DaftarOrderController extends Controller
{
    protected $link = 'frontend/daftar-order/';

    function __construct()
    {
        if (Auth::check()){
            if(auth()->users()){
                $this->middleware('auth');
            }
        }
        $this->setLink($this->link);
        $this->setTitle("Daftar Order");
        $this->setModalSize("small");
        $this->setBreadcrumb(['Daftar Order' => '#']);

        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'no_order',
                'name' => 'no_order',
                'label' => 'No Order',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'tgl_order',
                'name' => 'tgl_order',
                'label' => 'Tgl Order',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'no_surat',
                'name' => 'no_surat',
                'label' => 'No Surat Permintaan',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'pemesan',
                'name' => 'pemesan',
                'label' => 'Peminta Jasa',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '120px',
            ],
            [
                'data' => 'layanan',
                'name' => 'layanan',
                'label' => 'Layanan',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '120px',
            ],
            [
                'data' => 'lingkup',
                'name' => 'lingkup',
                'label' => 'Lingkup',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '120px',
            ],
            [
                'data' => 'total',
                'name' => 'total',
                'label' => 'Biaya',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '120px',
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '150px',
            ],
            [
                'data' => 'briefing_teknis',
                'name' => 'briefing_teknis',
                'label' => 'Briefing Teknis',
                'className' => "center aligned",
                'width' => '120px',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'searchable' => false,
                'className' => "center aligned",
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '100px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = PendaftaranPengujian::where('created_by', auth()->user()->id)->whereIn('status', [5,6,7])->where('tipe_surat', 0)->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('no_order','asc');
        }

        if ($no_order = $request->no_order) {
            $records->where('no_order', $no_order);
        }


        return DataTables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('pemesan', function ($record) use ($request) {
            $pemesan = $record->user->pelanggans->nama_lengkap.'</br>'.$record->user->pelanggans->no_tlp;
            return $pemesan;
        })
        ->addColumn('layanan', function ($record) use ($request) {
            return $record->pelayanan->nama;
        })
        ->addColumn('lingkup', function ($record) use ($request) {
            return $record->lingkup->nama;
        })
        ->addColumn('total', function ($record) use ($request) {
            return 0;
        })
        ->addColumn('status', function ($record) use ($request) {
            return status($record->status);
        })
        ->editColumn('briefing_teknis', function ($record) {
        	$btn = '-';
            if($record->lingkup->id == 10){
				 if(isset($record->kaji_ulang)){
					if($record->kaji_ulang->files->count()>0){
			            $btn = $this->makeButton([
		                	'type' => 'url',
		                	'tooltip' => 'Download',
		                	'class' => 'ui blue mini icon button',
		                	'label' => '<i class="download icon"></i>',
		                	'url' => url('download', $record->kaji_ulang->id).'/briefing-teknis'
		            	]);
		            }
		        }
            }else{
            	return '-';
            }
        	return $btn;
        })
        ->editColumn('created_at', function ($record) {
        	$btn = '';
            $btn .= $this->makeButton([
            	'type' => 'url',
            	'class' => 'ui blue mini icon button',
            	'tooltip' => trans('translator.View'),
            	'label' => '<i class="eye icon"></i>',
            	'url' => url($this->link.$record->id.'/detail')
            ]);

            $btn .= $this->makeButton([
            	'type' => 'url',
            	'class' => 'ui orange mini icon button',
            	'tooltip' => trans('translator.Cetak'),
            	'label' => '<i class="print icon"></i>',
            	// 'url' => url($this->link.$record->id.'/cetak')
            ]);

            return $btn;
        })
        ->addColumn('action', function ($record) {
            $btn = '';

            $btn .= $this->makeButton([
            	'type' => 'url',
            	'class' => 'ui blue mini icon button',
            	'tooltip' => trans('translator.Surat Penawaran'),
            	'label' => '<i class="list icon"></i>',
            	// 'url' => url($this->link.$record->id.'/edit')
            ]);

            $btn .= $this->makeButton([
            	'type' => 'url',
            	'class' => 'ui teal mini icon button',
            	'tooltip' => trans('translator.Bukti Pembayaran'),
            	'label' => '<i class="file alternate outline icon"></i>',
            	'url' => url($this->link.$record->id.'/download')
            ]);

            return $btn;
        })
        ->rawColumns(['created_at','pemesan','action'])
        ->make(true);
    }

    public function index()
    {
    	$this->setTitle(trans('translator.Riwayat Order'));
    	$this->setBreadcrumb([trans('translator.Riwayat Order') => '#']);
        return $this->render('frontend.list.index', ['mockup' => false]);
    }

    public function download($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

    public function detail($id)
    {
    	$this->setTitle(trans('translator.Riwayat Order'));
    	$this->setBreadcrumb([trans('translator.Riwayat Order') => '#']);
        $record = PendaftaranPengujian::find($id);
        return $this->render('frontend.list.detail',['record' => $record]);
    }

    public function edit($id)
    {
        $record = PendaftaranPengujian::find($id);

        return $this->render('frontend.list.detail',['record' => $record]);
    }
}
