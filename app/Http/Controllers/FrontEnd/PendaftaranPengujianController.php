<?php

namespace App\Http\Controllers\FrontEnd;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
use App\Http\Requests\FrontEnd\PendaftaranPengujianRequest;
use App\Http\Requests\FrontEnd\CekItemUjiRequest;

/* Models */
use App\Models\Authentication\Role;
use App\Models\Authentication\User;
use App\Models\FrontEnd\PendaftaranPengujian;
use App\Models\FrontEnd\PendaftaranPengujianDetail;
use App\Models\VirtualAccount\VirtualAccount;
use App\Models\Master\JenisPelayanan;
use App\Models\Master\JenisPengujian;
use App\Models\Master\Satuan;
use App\Models\KajiUlang\KajiUlang;
use App\Models\Master\Lingkup;
use App\Models\Picture;
use App\Models\Files;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Zipper;
use Excel;

class PendaftaranPengujianController extends Controller
{
    protected $link = 'frontend/pendaftaran-pengujian/';

    function __construct()
    {
        if (Auth::check()){
            if(auth()->users()){
                $this->middleware('auth');
            }
        }
        $this->setLink($this->link);
        $this->setTitle("Order Berjalan");
        $this->setModalSize("tiny");
        $this->setBreadcrumb(['Order Berjalan' => '#']);

// Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'no_order',
                'name' => 'no_order',
                'label' => 'No Order',
                'className' => "center aligned",
                'width' => '200px',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'tgl_order',
                'name' => 'tgl_order',
                'label' => 'Tgl Order',
                'className' => "center aligned",
                'width' => '120px',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'no_surat',
                'name' => 'no_surat',
                'label' => 'No Surat Permintaan',
                'className' => "center aligned",
                'width' => '120px',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'pemesan',
                'name' => 'pemesan',
                'label' => 'Peminta Jasa',
                'className' => "center aligned",
                'width' => '200px',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'layanan',
                'name' => 'layanan',
                'label' => 'Layanan',
                'className' => "center aligned",
                'width' => '200px',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'lingkup',
                'name' => 'lingkup',
                'label' => 'Lingkup',
                'className' => "center aligned",
                'width' => '200px',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'kelas',
                'name' => 'kelas',
                'label' => 'Kelas',
                'className' => "center aligned",
                'width' => '120px',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'total',
                'name' => 'total',
                'label' => 'Biaya',
                'className' => "center aligned",
                'width' => '120px',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'briefing_teknis',
                'name' => 'briefing_teknis',
                'label' => 'Download',
                'className' => "center aligned",
                'width' => '200px',
                'searchable' => false,
                'sortable' => false,
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '350px',
            ],
            [
                'data' => 'status_bayar',
                'name' => 'status_bayar',
                'label' => 'Status Bayar',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '300px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'searchable' => false,
                'className' => "center aligned",
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '100px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = PendaftaranPengujian::where('user_id', auth()->user()->id)->where('tipe_surat', 0)->where('tipe_surat', 0)->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at','desc');
        }

        if ($no_order = $request->no_order) {
            $records->where('no_order', $no_order);
        }


        return DataTables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('pemesan', function ($record) use ($request) {
            $pemesan = $record->user->pelanggans->perusahaan->nama.'</br>'.$record->user->pelanggans->nama_lengkap.'</br>'.$record->user->pelanggans->no_hp;
            	return $pemesan;
        })
        ->addColumn('layanan', function ($record) use ($request) {
            return $record->pelayanan->nama;
        })
        ->addColumn('tgl_order', function ($record) use ($request) {
            return DateToStringYear($record->tgl_order);
        })
        ->addColumn('lingkup', function ($record) use ($request) {
            return $record->lingkup->nama;
        })
        ->addColumn('total', function ($record) use ($request) {
            $biaya = 0;
            if($record->status==2){
                if($record->kaji_ulang){
                    if($record->kaji_ulang->surat){
                            $biaya = $record->kaji_ulang->surat->total;   
                    }
                }
            }
            return number_format($biaya,2,',','.');
        })
        ->addColumn('kelas', function ($record) use ($request) {
            $kelas = 'Reguler';
            if($record->kelas==2){
                $kelas = 'Prioritas';
            }

            return $kelas;
        })
        ->addColumn('status', function ($record) use ($request) {
        	if($record->status == 1){
        		return '<label class="ui grey tag label">'.status($record->status).'</label>';
        	}elseif($record->status == 2){
    			if($record->kaji_ulang){
    				if($record->kaji_ulang->surat){
    					if($record->kaji_ulang->surat->va){
    						if($record->kaji_ulang->surat->va->status == 0){
    							return '<label class="ui purple tag label">Menunggu Virtual Account</label>';
    						}elseif($record->kaji_ulang->surat->va->status == 1){
    							if($record->user->pelanggans->perusahaan->kategori !== 0){
                                    $bayar = 'Konfirmasi Pembayaran';
                                    if(!is_null($record->kaji_ulang->surat->va->bukti_url)){
                                        $bayar = 'Bukti Pembayaran Terupload';
                                    }
	        						return '<a href="'.url($this->link.$record->id.'/konfirmasi').'"><button class="ui red basic button"><i class="money bill alternate outline icon"></i>'.$bayar.'</button></a>';
    							}else{
    								return '<label class="ui olive tag label">Menunggu Konfirmasi Pembayaran</label>';
    							}
    						}elseif($record->kaji_ulang->surat->va->status == 2){
    							if($record->kaji_ulang->surat->va->bukti_url){
    								return '<label class="ui pink tag label">Virtual Account Kadaluarsa</label>';
    							}else{
	    							return '<label class="ui red tag label">Virtual Account Nonaktif</label>';
    							}
    						}else{
    							if($record->user->pelanggans->perusahaan->kategori !== 0){
    								if($record->kaji_ulang->surat->va->konfirmasi){
    									if($record->kaji_ulang->surat->va->konfirmasi->status == 1){
        									if($record->kaji_ulang->surat->va->konfirmasi->penerimaan){
		    									if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian){
		    										if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->status_data_pengujian == 0){
			    										if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->reschedule){
			    											if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->reschedule->status == 0){
			    												return '<label class="ui pink tag label">Reschedule</label>';
			    											}else{
			    												return '<label class="ui brown tag label">Pengujian</label>';
			    											}
			    										}else{
			    											return '<label class="ui brown tag label">Pengujian</label>';
			    										}
		    										}else{
		    											if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->laporan_pengujian){
		    												if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->laporan_pengujian->first()->status == 0){
			    												return '<label class="ui yellow tag label">Pengiriman Laporan</label>';
		    												}else{
		    													if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->closeorder){
		    														return '<label class="ui green tag label">Close Order Selesai</label>';	
		    													}else{
		    														return '<label class="ui violet tag label">Menunggu Close Order</label>';	
		    													}
		    												}
		    											}else{
			    											return '<label class="ui brown tag label">Pengujian</label>';
		    											}
		    										}
		    									}else{
			    									return '<label class="ui teal tag label">Menunggu Pengujian</label>';
		    									}
		    								}else{
			    								return '<label class="ui violet tag label">Penyerahan Barang Uji</label>';
		    								}
        								}elseif($record->kaji_ulang->surat->va->konfirmasi->status == 2){
        									return '<label class="ui teal tag label">Pengembalian Dana</label>';
        								}elseif($record->kaji_ulang->surat->va->konfirmasi->status == 3){
		    								if($record->kaji_ulang->surat->va->konfirmasi->penerimaan){
		    									if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian){
		    										if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->status_data_pengujian == 0){
			    										if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->reschedule){
			    											if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->reschedule->status == 0){
			    												return '<label class="ui pink tag label">Reschedule</label>';
			    											}else{
			    												return '<label class="ui brown tag label">Pengujian</label>';
			    											}
			    										}else{
			    											return '<label class="ui brown tag label">Pengujian</label>';
			    										}
		    										}else{
		    											if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->laporan_pengujian){
		    												if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->laporan_pengujian->first()->status == 0){
			    												return '<label class="ui yellow tag label">Pengiriman Laporan</label>';
		    												}else{
		    													if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->closeorder){
		    														return '<label class="ui green tag label">Close Order Selesai</label>';	
		    													}else{
		    														return '<label class="ui violet tag label">Menunggu Close Order</label>';	
		    													}
		    												}
		    											}else{
			    											return '<label class="ui brown tag label">Pengujian</label>';
		    											}
		    										}
		    									}else{
			    									return '<label class="ui teal tag label">Menunggu Pengujian</label>';
		    									}
		    								}else{
			    								return '<label class="ui violet tag label">Penyerahan Barang Uji</label>';
		    								}
        								}else{
        									return '<label class="ui orange tag label">Menunggu Konfirmasi Pembayaran</label>';
        								}
	    							}else{
		    							return '<label class="ui orange tag label">Menunggu Konfirmasi Pembayaran</label>';
	    							}
	    						}else{
	    							if($record->kaji_ulang->surat->va->konfirmasi){
	    								if($record->kaji_ulang->surat->va->konfirmasi->penerimaan){
	    									if($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian){
	    										return '<label class="ui brown tag label">Pengujian</label>';
	    									}else{
		    									return '<label class="ui teal tag label">Menunggu Pengujian</label>';
	    									}
	    								}else{
		    								return '<label class="ui violet tag label">Penyerahan Barang Uji</label>';
	    								}
	    							}else{
		    							return '<label class="ui green tag label">Pembayaran Berhasil</label>';
	    							}
	    						}
    						}
    					}else{
    						return '<label class="ui brown tag label">Menunggu Surat Penawaran Terkirim</label>';
    					}
    				}else{
    					if($record->kaji_ulang->keputusan == 3){
        					return '<label class="ui yellow tag label">Didiskusikan</label>';
    					}elseif($record->kaji_ulang->keputusan == 2){
    						if($record->kaji_ulang->bukti){
								return '<label class="ui red tag label">Ditolak</label>';
    						}else{
    							return '<label class="ui green tag label">Menunggu Surat Penawaran</label>';
    						}
    					}else{
    						return '<label class="ui green tag label">Menunggu Surat Penawaran</label>';
    					}
    				}
    			}else{
        			return '<label class="ui olive tag label">Menunggu Kaji Ulang</label>';
    			}
        	}elseif($record->status == 3){
        		return '-';
        	}elseif($record->status == 4){
        		return '<label class="ui blue tag label">'.status($record->status).'</label>';
        	}elseif($record->status == 5){
        		return '<label class="ui red tag label">Dibatalkan</label>';
        	}elseif($record->status == 6){
        		return '<label class="ui violet tag label">'.status($record->status).'</label>';
        	}elseif($record->status == 7){
        		return '<label class="ui green tag label">'.status($record->status).'</label>';
        	}elseif($record->status == 8){
        		return '<label class="ui orange tag label">'.status($record->status).'</label>';
        	}elseif($record->status == 9){
        		return '<label class="ui red tag label">'.status($record->status).'</label>';
        	}elseif($record->status == 10){
        		if($record->kaji_ulang->bukti){
        			return '<a href="'.url($this->link.$record->kaji_ulang->id.'/downloadPenolakan').'"><button class="ui red basic button" data-tooltip="Download Surat Penolakan" data-position="top center"><i class="download icon"></i>Ditolak</button></a>';
        		}else{
        			return '<label class="ui orange tag label">Menunggu Surat Penawaran</label>';
        		}
        		return '<label class="ui red tag label">'.status($record->status).'</label>';
        	}else{
        		return '-';
        	}
        })
		->editColumn('status_bayar', function ($record) {
			if($record->kaji_ulang){
				if($record->kaji_ulang->surat){
					if($record->kaji_ulang->surat->va){
						if($record->kaji_ulang->surat->va->konfirmasi){
							if($record->kaji_ulang->surat->va->konfirmasi->status == 1){
								if($record->kaji_ulang->surat->va->status == 1){
									return '<a href="'.url($this->link.$record->id.'/konfirmasi').'"><button class="ui red basic button"><i class="money bill alternate outline icon"></i>Upload Ulang Bukti Pembayaran</button></a>';
								}elseif($record->kaji_ulang->surat->va->konfirmasi->status == 2){
									return '<label class="ui orange tag label">Virtual Account Tidak Aktif</label>';	
								}else{
									return '-';
								}
							}else{
								return '-';
							}
						}else{
							return '-';	
						}
					}else{
						return '-';
					}
				}else{
					return '-';
				}
			}else{
				return '-';
			}
            return $record->created_at->diffForHumans();
        })
        ->editColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->editColumn('briefing_teknis', function ($record) {
        	$btn = '';
            if($record->pelayanan->id == 3){
				 if(isset($record->kaji_ulang)){
				 	if($record->kaji_ulang->files->where('type', 1)->count()>0){
				 		if($record->kaji_ulang->files->where('type', 2)->count()>0){
				 			$btn .= $this->makeButton([
				 				'type' => 'url',
				 				'tooltip' => 'Download Jadwal',
				 				'class' => 'ui blue mini icon button',
				 				'label' => '<i class="download icon"></i>',
				 				'url' => url('download-briefing', $record->kaji_ulang->id).'/briefing-teknis/1'
				 			]);

				 			$btn .= $this->makeButton([
				 				'type' => 'url',
				 				'tooltip' => 'Download Notulen',
				 				'class' => 'ui teal mini icon button',
				 				'label' => '<i class="download icon"></i>',
				 				'url' => url('download-briefing', $record->kaji_ulang->id).'/briefing-teknis/2'
				 			]);
				 		}else{
				 			$btn .= $this->makeButton([
				 				'type' => 'url',
				 				'tooltip' => 'Download Jadwal',
				 				'class' => 'ui blue mini icon button',
				 				'label' => '<i class="download icon"></i>',
				 				'url' => url('download-briefing', $record->kaji_ulang->id).'/briefing-teknis/1'
				 			]);
				 		}

                        if($record->kaji_ulang->surat){
                            if($record->kaji_ulang->surat->files->count() > 0){
                                $btn .= $this->makeButton([
                                    'type' => 'url',
                                    'tooltip' => 'Download Surat Penawaran',
                                    'class' => 'ui orange mini icon button',
                                    'label' => '<i class="download icon"></i>',
                                    'url' => url('download-briefing', $record->kaji_ulang->surat->id).'/surat-penawaran'
                                ]);
                            }
                        }
				 	}else{
				 		if($record->kaji_ulang->surat){
                            if($record->kaji_ulang->surat->files->count() > 0){
                                $btn .= $this->makeButton([
                                    'type' => 'url',
                                    'tooltip' => 'Download Surat Penawaran',
                                    'class' => 'ui orange mini icon button',
                                    'label' => '<i class="download icon"></i>',
                                    'url' => url('download-briefing', $record->kaji_ulang->surat->id).'/surat-penawaran'
                                ]);
                            }
                        }
				 	}
		        }
            }else{
            	if(isset($record->kaji_ulang)){
            		if($record->kaji_ulang->surat){
            			if($record->kaji_ulang->surat->files->count() > 0){
            				$btn .= $this->makeButton([
            					'type' => 'url',
            					'tooltip' => 'Download Surat Penawaran',
            					'class' => 'ui orange mini icon button',
            					'label' => '<i class="download icon"></i>',
            					'url' => url('download-briefing', $record->kaji_ulang->surat->id).'/surat-penawaran'
            				]);
            			}
            		}
            	}
            }
            if(isset($record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->laporan_pengujian)){
                $btn .= $this->makeButton([
                    'type' => 'url',
                    'tooltip' => 'Download Laporan Pengujian',
                    'class' => 'ui blue mini icon button',
                    'label' => '<i class="download icon"></i>',
                    'url' => url('download-laporan-pengujian', $record->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->id)
                ]);
            }
        	return $btn;
        })
        ->addColumn('action', function ($record) {
            $btn = '';

        	$btn = $this->makeButton([
            	'type' => 'url',
            	'class' => 'ui blue mini icon button',
            	'tooltip' => trans('translator.View'),
            	'label' => '<i class="eye icon"></i>',
            	'url' => url($this->link.$record->id.'/detil')
            ]);

            return $btn;
        })
        ->rawColumns(['status','pemesan','action','briefing_teknis','status_bayar'])
        ->make(true);
    }

    public function index()
    {
    	$this->setTitle(trans('translator.Order Berjalan'));
        $this->setBreadcrumb([trans('translator.Order Berjalan') => '#']);
        return $this->render('frontend.pendaftaran-pengujian.index', ['mockup' => false]);
    }

    public function create()
    {
    	$this->setTitle(trans('translator.Buat Order Baru'));
    	$this->setBreadcrumb([trans('translator.Order Berjalan') => '#', trans('translator.Buat') => '#']);
        return $this->render('frontend.pendaftaran-pengujian.create');
    }

    public function buat()
    {
        return $this->render('frontend.pendaftaran-pengujian.partials.buat');
    }

    public function upload($id)
    {
        return $this->render('frontend.pendaftaran-pengujian.partials.upload', ['lingkup_id' => $id]);
    }

    public function detil($id)
    {
    	$record = PendaftaranPengujian::find($id);
    	$kaji_ulang =$record->kaji_ulang;
    	if($record->kaji_ulang){
	    	$surat = $record->kaji_ulang->surat;
    	}else{
    		$surat = [];
    	}
    	$this->setTitle(trans('translator.Detil Order'));
        $this->setBreadcrumb([trans('translator.Detil Order') => '#']);
        $this->setSubtitle('No Order : '.$record->no_order);
        return $this->render('frontend.pendaftaran-pengujian.partials.detil',[
        	'record' => $record, 
			'kaji_ulang' => $kaji_ulang,
			'surat' => $surat,
        ]);
    }

    public function lihatLayanan()
    {
    	$record = JenisPelayanan::get();
        return $this->render('frontend.pendaftaran-pengujian.partials.pelayanan',['record' => $record]);
    }

    public function searchLayanan(Request $request)
    {
        $pelayanan = $request->pelayanan;
        $lingkup = $request->lingkup;
        $jenis_pengujian = $request->jenis_pengujian;

        // $record = JenisPelayanan::get();
        $record = JenisPelayanan::with('lingkup', 'pengujian');

        if ($pelayanan) {
            $record->where('id', $pelayanan);
        }

        if ($lingkup) {
             $record->whereHas('lingkup',function($q) use($lingkup){
                $q->where('id', $lingkup);
            });
        }

        if ($jenis_pengujian){
            $record->whereHas('lingkup',function($q) use($jenis_pengujian){
                $q->whereHas('pengujian',function($query) use($jenis_pengujian){
                    $query->where('nama', 'like', '%'.$jenis_pengujian.'%');
                });
            });
        }

        return view('frontend.pendaftaran-pengujian.partials.pelayanan-table', ['record' => $record->get()]);
    }

    public function ubah()
    {
        return $this->render('frontend.pendaftaran-pengujian.partials.ubah');
    }

    public function store(PendaftaranPengujianRequest $request)
    {
    	if($request->detail){
    	}else{
    		return response()->json([
                'status' => 'false',
                'message' => 'Data Item Uji tidak boleh kosong'
            ],402);
    	}

        $user = auth()->user()->id;
        $date = date('Y-m-d');
        $nomor = array();

        DB::beginTransaction();
        try {
        	$no = PendaftaranPengujian::generateNomor();
        	if($request->jenis_pelayanan_id == 3){
        		$a =0;
        		foreach ($request->detail as $key => $value) {
	        		$no_order = PendaftaranPengujian::generateNoOrder($user);
		            $pengujian = new PendaftaranPengujian;
		            $pengujian->no_order = PendaftaranPengujian::generateNoOrder($user);
		            $pengujian->nomor = $no;
		        	$pengujian->tgl_surat = $request->tgl_surat;
		        	$pengujian->no_surat = $request->no_surat;
		        	$pengujian->tipe = 1;
		            $pengujian->tgl_order = $date;
		            $pengujian->user_id = $user;
		            $pengujian->layanan_id = $request->jenis_pelayanan_id;
		            $pengujian->lingkup_id = $request->lingkup_id;
		            $pengujian->kelas = $request->kelas;
		            $pengujian->rencana = $request->rencana;
		            $pengujian->status = 1;
		            $pengujian->jenis = 1;
		            $pengujian->catatan = $request->catatan;

		            if($file = $request->surat){
		        		$path = $file->store('uploads/pendaftaran', 'public');
		        		$pengujian->filename = $file->getClientOriginalName();
		        		$pengujian->bukti = $path;
		            }
		            $pengujian->save();

		            $detail = new PendaftaranPengujianDetail;
		            $detail->pp_id = $pengujian->id;
		            $detail->jenis_id = $value['jenis_id'];
			        $detail->merk = $value['merk'];
			        $detail->tipe = $value['tipe'];
			        $detail->jumlah = $value['jumlah'];
			        $detail->satuan_id = $value['satuan_id'];
			        $detail->spesifikasi = $value['spesifikasi'];
		            $detail->save();

		            $pengujian->multipleFileUpload($request->lampiran);
		            if($a % 2 == 0){
			            $nomor[]= $no_order.'<br>';
		            }else{
			            $nomor[]= $no_order;
		            }

		            $a++;

                    if($pengujian->jenis==0){
                        $this->pushNotifKalibrasi($pengujian);
                    }else{
                        $this->pushNotifUji($pengujian);
                    }
        		}
        	}else{
		        $no_order = PendaftaranPengujian::generateNoOrder($user);
	            $pengujian = new PendaftaranPengujian;
	            $pengujian->no_order = PendaftaranPengujian::generateNoOrder($user);
	            $pengujian->nomor = $no;
	        	$pengujian->tgl_surat = $request->tgl_surat;
	        	$pengujian->no_surat = $request->no_surat;
	        	$pengujian->tipe = 1;
	            $pengujian->tgl_order = $date;
	            $pengujian->user_id = $user;
	            $pengujian->layanan_id = $request->jenis_pelayanan_id;
	            $pengujian->lingkup_id = $request->lingkup_id;
	            $pengujian->kelas = $request->kelas;
	            $pengujian->rencana = $request->rencana;
	            $pengujian->status = 1;
	            $pengujian->catatan = $request->catatan;

	            if($request->jenis_pelayanan_id == 1){
	            	$pengujian->jenis = 0;
	            }else{
	            	$pengujian->jenis = 1;
	            }

	            if($file = $request->surat){
	        		$path = $file->store('uploads/pendaftaran', 'public');
	        		$pengujian->filename = $file->getClientOriginalName();
	        		$pengujian->bukti = $path;
	            }
	            $pengujian->save();
	            $pengujian->saveDetail($request->detail);
	            $pengujian->multipleFileUpload($request->lampiran);

	            $nomor[] = $no_order;
                if($pengujian->jenis==0){
                    $this->pushNotifKalibrasi($pengujian);
                }else{
                    $this->pushNotifUji($pengujian);
                }
        	}
            DB::commit();

	    }catch (\Exception $e) {
	      DB::rollback();
	      return response([
	        'status' => 'error',
	        'message' => 'An error occurred!',
	        'error' => $e->getMessage(),
	      ], 500);
	    }

	    return response([
	      'status' => true,
          'message' => $nomor,
	    ]);
    }

    public function pushNotifKalibrasi($data){
        $user = User::whereHas('roles',function($query){
                    $query->where('name','yan-kalibrasi');
                })->get();
        foreach ($user as $row) {
            if(!is_null($row->device_id)){
                 $this->onesignal('Penerimaan Surat Masuk(Online) Baru dengan nomor order '.$data->no_order,$row->device_id,$data->toArray(),'penerimaan-surat','online');
            }
        }
    }

    public function pushNotifUji($data){
        $user = User::whereHas('roles',function($query){
                    $query->where('name','yan-uji');
                })->get();
        foreach ($user as $row) {
            if(!is_null($row->device_id)){
                 $this->onesignal('Penerimaan Surat Masuk(Online) Baru dengan nomor order '.$data->no_order,$row->device_id,$data->toArray(),'penerimaan-surat','online');
            }
        }
    }

    public function edit($id)
    {
    	$this->setTitle(trans('translator.Ubah Order Berjalan'));
    	$this->setBreadcrumb([trans('translator.Order Berjalan') => '#', trans('translator.Ubah') => '#']);
        $record = PendaftaranPengujian::find($id);
        return $this->render('frontend.pendaftaran-pengujian.edit',['record' => $record]);
    }

    public function konfirmasi($id)
    {
    	$this->setTitle(trans('translator.Konfirmasi Pembayaran'));
    	$this->setBreadcrumb([trans('translator.Konfirmasi Pembayaran') => '#', trans('translator.Konfirmasi Pembayaran') => '#']);
        $record = PendaftaranPengujian::find($id);
        return $this->render('frontend.pendaftaran-pengujian.partials.konfirmasi',[
        	'record' => $record,

        ]);
    }

    public function destroy($id)
    {
        $record = PendaftaranPengujian::find($id);
        unlink(public_path('storage/'.$record->bukti));
        $record->delete();
        return response([
            'status' => true,
        ]);
    }

    public function download($id)
    {
    	$daftar = PendaftaranPengujian::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

// BATAS BARU

    public function getDetail(Request $request){
        $recordJenisPengujian = JenisPelayanan::find($request->jenis_pelayanan);

        return $this->render('frontend.pendaftaran-pengujian.partials.detail',['recordJenisPengujian'=>$recordJenisPengujian]);
    }


    public function cekRequestItemUji(CekItemUjiRequest $request){
        return response([
            'status' => true,
        ]);
    }

    public function cekUpload(Request $request){
    	$cek_id = array();
    	$satuan_id = array();
    	$jenis_pengujian = JenisPengujian::where('lingkup_id', $request->lingkup_id)->get();
    	$satuan = Satuan::get();
    	foreach ($jenis_pengujian as $key => $val) {
    		$cek_id[] = $val->id;
    	}
    	foreach ($satuan as $key => $vl) {
    		$satuan_id[] = $vl->id;
    	}
    	if($request->hasFile('upload_data')){
    		$path = $request->file('upload_data')->getRealPath();
    		$data = Excel::load($path, function($reader) {})->get();
    		$insert = array();
    		$i = 1;
    		$y=0;
    		$z=0;
    		if(!empty($data) && $data->count()){
    			foreach ($data as $key => $value) {
    				if(!is_null($value->id_jenis_pengujian) && !is_null($value->id_satuan)){
    					if(in_array((int)$value->id_jenis_pengujian, $cek_id, TRUE)){
    						if(in_array((int)$value->id_satuan, $satuan_id, TRUE)){
    							$nama_jenis = JenisPengujian::find((int)$value->id_jenis_pengujian);
    							$nama_satuan = Satuan::find((int)$value->id_satuan);
    							if($i <= 350){
				    				$insert[] = [
				    					'id_jenis_pengujian' => (int)$value->id_jenis_pengujian,
				    					'nama_jenis' => $nama_jenis->nama,
				    					'nama_satuan' => $nama_satuan->satuan,
				    					'merk' => $value->merk,
				    					'tipe' => $value->tipe,
				    					'jumlah' => (int)$value->jumlah,
				    					'id_satuan' => (int)$value->id_satuan,
				    					'spesifikasi' => $value->spesifikasi,
				    				];
    							}
			    				$i++;
 							}else{
 								$y +=1;
 							}
 						}else{
 							$z +=1;
 						}
    				}
    			}

    			if($z > 0){
    				return response([
			            'status' => false,
			            'message' => 'Ada '.$z.' data Jenis Pengujian yang tidak sesuai',
			            'data' => $insert,
			        ]);
    			}elseif($y > 0){
    				return response([
			            'status' => false,
			            'message' => 'Ada '.$z.' data Satuan yang tidak sesuai',
			            'data' => $insert,
			        ]);
    			}else{
    				return response([
			            'status' => true,
			            'message' => '',
			            'data' => $insert,
			        ]);	
    			}
    		}else{
    			return response([
    				'status' => false,
    				'message' => 'Tidak Ada Data',
    				'data' => '',
    			]);
    		}
		}else{
			return response([
				'status' => false,
				'message' => 'Inputan tidak boleh kosong',
				'data' => '',
			]);
		}
    }

    public function lihat($data)
    {
    	$month = date('n');
    	$year = date("Y");
        return $this->render('frontend.pendaftaran-pengujian.partials.lihat-jadwal', [
        	'prioritas' => $data,
        	'tahun' => $year,
        	'bulan' => $month,
    	]);
    }

    public function exportTemplate(Request $request, $lingkup)
    {	
    	$jenis_pengujian = JenisPengujian::where('lingkup_id', $lingkup)->get();
    	$data_lingkup = Lingkup::find($lingkup);
    	$data_satuan = Satuan::get();
    	 $Export = Excel::create('List Item Uji - '.$data_lingkup->pelayanan->nama, function($excel) use($jenis_pengujian, $data_lingkup, $data_satuan) {
            $excel->setTitle('List Item Uji');
            $excel->setCreator('E-Uji dan E-Kal')->setCompany('E-Pusertif');
            $excel->setDescription('Export');
            $excel->sheet('List Item Uji ', function($sheet) use($jenis_pengujian, $data_lingkup, $data_satuan){
                $sheet->row(1, array(
	                'id_jenis_pengujian',
                    'merk',
                    'tipe',
                    'jumlah',
                    'id_satuan',
                    'spesifikasi',
                ));
                $sheet->setWidth(array('H'=>6));
                $sheet->SetCellValue("H1", "ID");
                $sheet->SetCellValue("I1", "Jenis Pengujian");
                $sheet->setBorder('H1:M1', 'thin');
                $sheet->mergeCells("I1:M1");
                $sheet->cells('H1:M1', function($cells){
                		$cells->setBackground('#999999');
                    	$cells->setFontColor('#FFFFFF');
                    	$cells->setAlignment('center');
                });

                $row = 2;
                $no =1;
                foreach ($jenis_pengujian as $key => $value) {
	                $from="I".$row;
					$to="M".$row;
	                $sheet->mergeCells("$from:$to");
	                $sheet->cells('H'.$row, function($cells){
                    	$cells->setAlignment('center');
                	});
	                $sheet->SetCellValue('H'.$row, $value->id);
	                $sheet->SetCellValue("I".$row, $value->nama);
	                $row++;
                }

                $awal="H".($row+2);
				$akhir="I".($row+2);
				$a="I".($row+2);
				$b="K".($row+2);
                $sheet->SetCellValue('H'.($row+2), "ID");
                $sheet->SetCellValue('I'.($row+2), "Satuan");
                $sheet->setBorder("$awal:$akhir", 'thin');
                $sheet->mergeCells("$a:$b");
                $sheet->cells("$awal", function($cells){
                		$cells->setBackground('#999999');
                    	$cells->setFontColor('#FFFFFF');
                    	$cells->setAlignment('center');
                });
                $sheet->cells("$akhir", function($cells){
                		$cells->setBackground('#999999');
                    	$cells->setFontColor('#FFFFFF');
                });

                $z = ($row+3);
                foreach ($data_satuan as $key => $val) {
	                $from="I".$z;
					$to="K".$z;
	                $sheet->mergeCells("$from:$to");
	                $sheet->cells('H'.$z, function($cells){
                    	$cells->setAlignment('center');
                	});
	                $sheet->SetCellValue('H'.$z, $val->id);
	                $sheet->SetCellValue("I".$z, $val->satuan);
	                $z++;
                }

                $sheet->setBorder('A1:F1', 'thin');
                $sheet->cells('A1:F1', function($cells){
                		$cells->setBackground('#999999');
                    	$cells->setFontColor('#FFFFFF');
                    	$cells->setAlignment('center');
                });
                $xx ="H".($z+2);
                $yy ="P".($z+2);
                $sheet->mergeCells("$xx:$yy");
                $sheet->SetCellValue($xx, 'Catatan : Mohon inputkan ID Pengujian dan ID Satuan Sesuai dengan List diatas.');
                $sheet->cells($xx, function($cells){
                    $cells->setFontSize(10);
                });
                $sheet->cells('A1:A300', function($cells){
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->setWidth(array('A'=>20,'B'=>40,'C'=>20, 'D'=>20, 'E'=>15, 'F'=>30));
            });
        });
        $Export ->download('xls');
    }

    public function saveKonfirmasi(Request $request, $id)
    {	
    	$pp = PendaftaranPengujian::find($id);
    	DB::beginTransaction();
    	try {
    		$va 				= VirtualAccount::find($pp->kaji_ulang->surat->va->id);
    		if($va->bukti_url){
	    		unlink(public_path('storage/'.$va->bukti_url));
    		}
    		$path 				= $request->bukti->store('konfirmasi', 'public');
    		$va->bukti_filename = $request->bukti->getClientOriginalName();
    		$va->bukti_url 		= $path;
    		$va->tgl_bayar 		= date("Y-m-d H:i:s");
	        $va->save();

	        DB::commit();

            $user = User::whereHas('roles',function($query){
                        $query->where('name','keuangan');
                    })->get();
            foreach ($user as $row) {
                if(!is_null($row->device_id)){
                     $this->onesignal('Konfirmasi Pembayaran Baru dengan nomor va '.$va->no_va,$row->device_id,$va->toArray(),'konfirmasi-pembayaran','belum-bayar');
                }
            }
        }catch (\Exception $e) {
	      DB::rollback();
	      return response([
	        'status' => 'error',
	        'message' => $e,
	        'error' => $e->getMessage(),
	      ], 500);
	    }
        return response([
            'status' => true
        ]);
    }

    public function downloadPenolakan($id)
    {
    	$daftar = KajiUlang::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return response()->view('errors.download');
    }

}
