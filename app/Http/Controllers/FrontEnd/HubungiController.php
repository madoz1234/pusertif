<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AduanRequest;
use App\Models\Aduan;
use App\Models\Files;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;

class HubungiController extends Controller
{
    protected $link = 'hubungi/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle(trans('translator.Hubungi Kami'));
        $this->setBreadcrumb([trans('translator.Hubungi Kami') => '#']);

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$this->setTitle(trans('translator.Hubungi Kami'));
        $this->setBreadcrumb([trans('translator.Hubungi Kami') => '#']);
        return $this->render('frontend.hubungi.index');
    }

    public function store(AduanRequest $request)
    {
    	DB::beginTransaction();
        try {
            $aduan = new Aduan;
            $aduan->pilihan = $request->pilihan;
            $aduan->nama = $request->nama;
            $aduan->email = $request->email;
            $aduan->no_hp = $request->no_hp;
            $aduan->pesan = $request->pesan;

            if($file = $request->lampiran){
        		$path = $file->store('uploads/aduan', 'public');
        		$aduan->filename = $file->getClientOriginalName();
        		$aduan->lampiran = $path;
            }
            $aduan->save();
            DB::commit();
	    }catch (\Exception $e) {
	      return response([
	        'status' => 'error',
	        'message' => 'An error occurred!',
	        'error' => $e->getMessage(),
	      ], 500);
	    }

	    return response([
	      'status' => true,
	    ]);
    }

}
