<?php

namespace App\Http\Controllers\Pengaduan;

/* Base App */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Validation */
// use App\Http\Requests\Konfigurasi\RolesRequest;
use App\Http\Requests\TanggapanRequest;

/* Models */
use App\Models\Authentication\Role;
use App\Models\Aduan;
use App\Models\Users;
use App\Models\Picture;
use App\Models\Files;

/* Libraries */
use DataTables;
use Carbon;
use Hash;
use Auth;
use DB;
use Storage;
use Mail;

class PengaduanController extends Controller
{
    protected $link = 'pengaduan/';
    protected $perms = 'pengaduan';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Pengaduan");
        $this->setPerms($this->perms);
        $this->setModalSize("tiny");
        $this->setBreadcrumb(['Pengaduan' => '#']);
// Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'perihal',
                'name' => 'perihal',
                'label' => 'Perihal',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
                'width' => '150px',
            ],
            [
                'data' => 'email',
                'name' => 'email',
                'label' => 'Email',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '150px',
            ],
            [
                'data' => 'no_hp',
                'name' => 'no_hp',
                'label' => 'No HP',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '100px',
            ],
            [
                'data' => 'lampiran',
                'name' => 'lampiran',
                'label' => 'Lampiran',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '80px',
            ],
            [
                'data' => 'pesan',
                'name' => 'pesan',
                'label' => 'Pesan',
                'searchable' => false,
                'sortable' => true,
                'width' => '250px',
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '150px',
            ],
            [
                'data' => 'oleh',
                'name' => 'oleh',
                'label' => 'Oleh',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '150px',
            ],
            [
                'data' => 'tgl',
                'name' => 'tgl',
                'label' => 'Tanggal',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '150px',
            ],
            [
                'data' => 'keterangan',
                'name' => 'keterangan',
                'label' => 'Keterangan',
                'searchable' => false,
                'sortable' => true,
                'width' => '250px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '50px',
            ]
        ]);
    }

    public function grid(Request $request)
    {	
    	$user = auth()->user();
    	$records =[];

        $records = Aduan::orderBy('created_at','asc')->select('*');
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }
        $records = $records->get();
        
		//Init Sort

        $link = $this->link;
        return DataTables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('perihal', function ($record) use ($request) {
        	$perihal = '-';
        	if($record->pilihan == 1){
        		$perihal = '<label class="ui orange tag label">Aduan</label>';
        	}else{
        		$perihal = '<label class="ui teal tag label">Saran</label>';
        	}
            return $perihal;
        })
        ->addColumn('lampiran', function ($record) {
            $download = '-';
            if($record->lampiran){
	            $download = $this->makeButton([
	                'type' => 'url',
	                'class' => 'ui blue mini icon button',
	                'label' => '<i class="download icon"></i>',
	                'tooltip' => 'Download',
	                'url' => url($this->link.'download/'.$record->id)
	            ]);
            }
            return $download;
        })
        ->addColumn('pesan', function ($record) use ($request) {
            return '<span class="ccount more" data-ccount="80">'.readMoreText(strip_tags($record->pesan),150).'</span>';
        })
        ->addColumn('status', function ($record) use ($request) {
        	$status = '-';
        	if($record->status == 0){
        		$status = '<label class="ui red tag label">Belum Ditanggapi</label>';
        	}elseif($record->status == 1){
        		$status = '<label class="ui green tag label">Ditanggapi</label>';
        	}else{
        		$status = '-';
        	}
            return $status;
        })
        ->addColumn('oleh', function ($record) use ($request) {
        	$oleh = '-';
            if($record->status == 1){
            	$oleh = $record->creatorName();
            }else{
            	 $oleh = '-';
            }
            return $oleh;
        })
        ->addColumn('tgl', function ($record) use ($request) {
            return DateToStringYear($record->tgl);
        })
        ->addColumn('keterangan', function ($record) use ($request) {
        	$keterangan = '-';
        	if($record->status == 0){
        		$keterangan = '-';
        	}elseif($record->status == 1){
        		$keterangan = '<span class="ccount more" data-ccount="80">'.readMoreText(strip_tags($record->keterangan),150).'</span>';
        	}else{
        		$keterangan = '-';
        	}
            return $keterangan;
        })
        ->addColumn('action', function ($record) use ($link){
                $btn = '-';
                if($record->status == 0){
                	$btn = $this->makeButton([
                		'type' => 'edit',
                		'label'   => '<i class="check icon"></i>',
                		'tooltip' => 'Tanggapi',
                		'data' => [
                			'id' => $record->id
                		],
                		'id'   => $record->id
                	]);
                }
                return $btn;
        })
        ->rawColumns(['perihal','lampiran','pesan','status','keterangan','action'])
        ->make(true);
    }

    public function index()
    {
        return $this->render('modules.pengaduan.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.pengaduan.create');
    }

    public function store(RolesRequest $request)
    {
        $record = new Role;
        $record->fill($request->all());
        $record->save();

        return response([
            'status' => true
        ]);
    }

    public function detil($id)
    {
        $record = PendaftaranPengujian::find($id);
        return $this->render('modules.pengaduan.detil',['record' => $record]);
    }

    public function detail()
    {
        return $this->render('modules.pengaduan.detil-ams');
    }

    public function edit($id)
    {
        $record = Aduan::find($id);
        return $this->render('modules.pengaduan.edit', [
            'record' => $record
        ]);
    }

    public function dispo()
    {
        return $this->render('modules.pengaduan.create-disposisi-ams');
    }

    public function update(TanggapanRequest $request, $id)
    {
    	DB::beginTransaction();
        try {
            $tanggapan = Aduan::find($id);
            $tanggapan->status = 1;
            $tanggapan->user_id = auth()->user()->id;
            $tanggapan->tgl = date("Y-m-d");
            $tanggapan->keterangan = $request->keterangan;
            if($file = $request->data){
        		$path = $file->store('uploads/aduan', 'public');
        		$tanggapan->tanggapan = $file->getClientOriginalName();
        		$tanggapan->path = $path;
	            $data=[
	                    'id'    		=> $id,
	                    'url'    		=> $this->link,
	                    'keterangan'    => $request->keterangan,
	                    'pesan'    		=> $tanggapan->pesan,
	                    'path'      	=> $path,
	                    'email'         => $tanggapan->email,
	                    'url'     		=> url('download-tanggapan', $id),
	                ];
	            sendEmailTanggapan($data);
            }else{
            	$data=[
	                    'id'    		=> $id,
	                    'url'    		=> $this->link,
	                    'pesan'    		=> $tanggapan->pesan,
	                    'keterangan'    => $request->keterangan,
	                    'email'         => $tanggapan->email,
	                ];
	            sendEmailTanggapan($data);
            }
            $tanggapan->save();

            DB::commit();
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

        return response([
          'status' => true
        ]);   
    }

    public function download($id)
    {
    	$pengaduan = Aduan::find($id);
    	if(file_exists(public_path('storage/'.$pengaduan->lampiran)))
    	{
    		return response()->download(public_path('storage/'.$pengaduan->lampiran), $pengaduan->filename);
    	}
    	return response()->view('errors.download');
    }

    // public function tanggapan($id)
    // {
    // 	$pengaduan = Aduan::find($id);
    // 	if(file_exists(public_path('storage/'.$pengaduan->path)))
    // 	{
    // 		return response()->download(public_path('storage/'.$pengaduan->path), $pengaduan->tanggapan);
    // 	}
    // 	return response()->view('errors.download');
    // }

    public function grant(Request $request, $id)
    {
        // $record = Role::find($id);
        // $record->perms()->sync($request->check);

        // return response([
        //     'status' => true
        // ]);
    }

    public function destroy($id)
    {
        // $record = Role::find($id);
        // $record->delete();

        // return response([
        //     'status' => true,
        // ]);
    }
}
