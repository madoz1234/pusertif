<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Master\Perusahaan;
use App\Models\Master\Pelanggan;
use App\Models\Authentication\User;
use App\Http\Requests\RegistrasiRequest;
use App\Models\Authentication\Role;
use App\Models\Translators;

use Carbon;
use Mail;
use DB;

class HomeController extends Controller
{	

	protected $link = '';
    // protected $perms  = '';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if (auth()->user()){
            $this->middleware('auth');
        }else{
        	$this->setLink($this->link);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	app('counter')->count();

        if (auth()->user()){
            return $this->render('frontend.user.index');
        }else{
            return $this->render('frontend.guest.index');
        }
    }

    public function tracking()
    {
        return $this->render('frontend.tracking.index');
    }

    public function perusahaan()
    {
    	$q = request()->get('q');
    	$kategori = request()->get('checked');
    	if($kategori == 99 || $kategori == null)
    	{
	        $perusahaan = Perusahaan::where('nama', 'like', '%'.$q.'%')
	        						->where('status', 1)
	        						->get();
    	}else{
			$perusahaan = Perusahaan::where('nama', 'like', '%'.$q.'%')
				        						->where('status', 1)
				        						->where('kategori', $kategori)
				        						->get();
    	}
        $data = $perusahaan->toArray();
        return [
    		'data' => $data,
        ];
    }

    public function aktivasi($id)
    {
    	$password = generateRandomString();
        DB::beginTransaction();

        try{
            $users = User::find($id);
            $users->password = bcrypt($password);
            $users->status = 1;
            $users->save();

            $data=[
            	'username'		=> $users->email,
            	'password'		=> $password,
            	'email'			=> $users->email,
            	'status'		=> 1,
            ];

            DB::commit();
            sendEmail($data);
            return redirect(url('/login'));

        }catch(\Exception $e){
        	return $e;
            DB::rollback();
            // return redirect(url('/login'));
        }
    }

    public function registrasi(RegistrasiRequest $request)
    {
    	$password = generateRandomString();
        $role = Role::where('name', 'user')->first();
        DB::beginTransaction();
        try{
            $users 				= new User;
            $users->username 	= $request->email;
            $users->email    	= $request->email;
            $users->last_login  = Carbon::now();

            if($request->perusahaan_id){
            	$perusahaan  = Perusahaan::find($request->perusahaan_id);
            	if($perusahaan->alamat == $request->alamat){
            		$users->password 		= bcrypt($password);
	            	$users->status 			= 0;
	            	// $users->status 			= 1;
	            	$users->nama 			= $request->nama_lengkap;
	                $id_perusahaan 			= $request->perusahaan_id;
	                $perusahaan->kategori 	= $request->tipe_customer;
	                $perusahaan->email 		= $request->email_perusahaan;
	                
	                $data=[
	                    'email'         	=> $request->email,
	                ];
            	}else{
            		$users->password 		= bcrypt($password);
	            	$users->status 			= 0;
	            	$users->nama 			= $request->nama_lengkap;
			    	$no_perusahaan 			= Perusahaan::generatePerusahaan();
	                $perusahaan             = new Perusahaan;
                    if ($request->nama_perusahaan) {
                        $exist = Perusahaan:: where('nama',$request->nama_perusahaan)
                                            ->where('alamat',$request->alamat)
                                            ->first();
                        // dd("test");
                        if (!$exist) {
        	                $perusahaan->nama       = $request->nama_perusahaan;
        	                $perusahaan->email 		= $request->email_perusahaan;
        	                $perusahaan->kode 		= $no_perusahaan;
        	                $perusahaan->alamat 	= $request->alamat;
        	                $perusahaan->no_tlp 	= $request->no_tlp;
        	                $perusahaan->kategori 	= $request->tipe_customer;
        	                $perusahaan->save();
                        }
                    }
	                $id_perusahaan 			= $perusahaan->id;
	                $data=[
	                    'email'         	=> $request->email,
	                ];
            	}
            }else{
            	$users->password 			= bcrypt($password);
            	$users->status 				= 0;
            	$users->nama 				= $request->nama_lengkap;
		    	$no_perusahaan 				= Perusahaan::generatePerusahaan();
                $perusahaan             	= new Perusahaan;
                if ($request->nama_perusahaan) {
                        $exist = Perusahaan:: where('nama',$request->nama_perusahaan)
                                            ->where('alamat',$request->alamat)
                                            ->first();
                        // dd("tos");
                        if (!$exist) {
                            $perusahaan->nama       = $request->nama_perusahaan;
                            $perusahaan->email      = $request->email_perusahaan;
                            $perusahaan->kode       = $no_perusahaan;
                            $perusahaan->alamat     = $request->alamat;
                            $perusahaan->no_tlp     = $request->no_tlp;
                            $perusahaan->kategori   = $request->tipe_customer;
                            $perusahaan->save();
                        }
                }
                $id_perusahaan 				= $perusahaan->id;
                $data=[
                	'email'         	=> $request->email,
                ];
            }
            $users->save();
            $users->roles()->sync($role->id);

            $record = new Pelanggan;
            $record->user_id            	= $users->id;
            $record->perusahaan_id      	= $id_perusahaan;
            $record->tipe_customer      	= $request->tipe_customer;
            $record->nama_lengkap       	= $request->nama_lengkap;
            $record->no_hp              	= $request->no_hp;
            $record->email              	= $request->email;
            $record->no_tlp             	= $request->no_tlp;
            $record->alamat             	= $request->alamat;
            $record->pesan             		= $request->pesan;
            $record->nip             		= $request->nip;
            $record->status             	= 1;
            $record->save();
            sendEmaila($data);

            DB::commit();
            return response([
                'status' => true
            ]);

        }catch(\Exception $e){
            DB::rollback();
            return response([
                'status' => false,
                'errors' => $e 
            ]);
        }
     return response([
                'status' => true
            ]);
    }
}
