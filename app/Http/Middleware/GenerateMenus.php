<?php

namespace App\Http\Middleware;
use Closure;
use Menu;
use Lang;
use Session;
use App\Models\Translators;
use Auth;

class GenerateMenus
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next 
    * @return mixed
    */
    public function handle($request, Closure $next)
    {
        $dataTrans = 'id';
        if(Auth::check())
        {
            if(auth()->user()->id == true){
                if(Translators::where('created_by',auth()->user()->id)->first()){
                    $dataTrans = Translators::where('created_by',auth()->user()->id)->first()->translator;
                }else {
                    Session::put('locale','en');
                    Lang::setLocale(Session::get('locale'));
                    $dataTrans = 'en';
                }
            }else {
                Session::put('locale','id');
                Lang::setLocale(Session::get('locale'));
                $dataTrans = 'id';
            }
        }else {
            $dataTranss = Translators::where('created_by',999)->first();
            if($dataTranss){
                $dataTrans = $dataTranss->translator;
            }else{
                $dataTrans = 'id';
            }
        }

        Lang::setLocale($dataTrans);

        Menu::make('mainMenu', function ($menu){
            $menu->add('Dashboard', 'backend/dashboard/')
	            ->data('icon', 'dashboard')
	            ->data('perms', 'dashboard')
	            ->active('home');

            $menu->add("Order", 'permintaan/spm/')
                 ->data('icon', 'inbox')
                 ->data('perms', 'permintaan-spm')
                 ->active('permintaan/spm/*');

            $menu->add("Penerimaan Surat", 'penerimaan/surat/')
                 ->data('icon', 'mail')
                 ->data('perms', 'penerimaan-surat')
                 ->active('penerimaan/surat/*');
            /* Proses Kaji Ulang */
            $menu->add('Kaji Ulang', 'kaji-ulang/proses-kaji-ulang/')
            	->data('icon', 'external alternate')
            	->data('perms', 'kaji-ulang')
            	->active('kaji-ulang/proses-kaji-ulang/*');


            /* Proses Surat Penawaran */
            $menu->add('Surat Penawaran', 'surat-penawaran/')
            	->data('icon', 'clone')
            	->data('perms', 'surat-penawaran')
            	->active('surat-penawaran/*');

            $menu->add('Virtual Account (VA)', 'virtual-account/')
            	->data('icon', 'credit card')
            	->data('perms', 'virtual-account')
            	->active('virtual-account/*');

            /*Nota Buku*/
            $menu->add('Nota Buku','nota-dinas/')
            	->data('icon','sticky note')
            	->data('perms','nota-dinas')
            	->active('nota-dinas/*');

            /* Proses Konfirmasi Pembayaran */
            $menu->add('Konfirmasi Pembayaran', 'konfirmasi/konfirmasi-pembayaran/')
            	->data('icon', 'check circle')
            	->data('perms', 'konfirmasi-pembayaran')
            	->active('konfirmasi/konfirmasi-pembayaran/*');

            /*Pengembalian Dana*/
            $menu->add('Pengembalian Dana','pengembalian-dana/')
            	->data('icon','undo')
            	->data('perms','pengembalian-dana')
            	->active('pengembalian-dana/*');

            /* Proses Realisasi Biaya */
            $menu->add('Realisasi Biaya', 'realisasi/realisasi-biaya/')
            	->data('icon', 'arrow circle up')
            	->data('perms', 'realisasi-biaya')
            	->active('realisasi/realisasi-biaya/*');
            /* Proses Reschedule */
            /* Proses Pengujian Serah Terima*/
            $menu->add('Order Uji Serah Terima', 'pengujian-serah-terima/')
            	->data('icon', 'tags')
            	->data('perms', 'pengujian-serah-terima')
            	->active('pengujian-serah-terima/*');

            /*Penerimaan Barang Uji*/
            $menu->add('Penerimaan Barang Uji','penerimaan-barang-uji/')
            	->data('icon','archive')
            	->data('perms','penerimaan-barang-uji')
            	->active('penerimaan-barang-uji/*');

            $menu->add('Pengujian')
                ->data('icon', 'tag')
                              // ->data('perms', 'pengujian')
                ->active('pengujian/*');
            $menu->pengujian->add('Kalibrasi', 'pengujian/layanan-kalibrasi/')
                ->data('icon', 'tag')
                ->data('perms', 'kalibrasi')
                ->active('pengujian/layanan-kalibrasi/*');

            $menu->pengujian->add('TR', 'pengujian/layanan-tr/')
                ->data('icon', '')
                ->data('perms', 'pengujian-tr')
                ->active('pengujian/layanan-tr/*');

            $menu->pengujian->add('TT', 'pengujian/layanan-tt/')
                ->data('icon', '')
                ->data('perms', 'pengujian-tt')
                ->active('pengujian/layanan-tt/*');

            $menu->pengujian->add('SISKIT', 'pengujian/layanan-siskit/')
                ->data('icon', '')
                ->data('perms', 'pengujian-siskit')
                ->active('pengujian/layanan-siskit/*');

            $menu->pengujian->add('SISTGI', 'pengujian/layanan-sistgi/')
                ->data('icon', '')
                ->data('perms', 'pengujian-sistgi')
                ->active('pengujian/layanan-sistgi/*');
            /*Penyerahan Barang Uji*/

            $menu->add('Reschedule', 'reschedules/')
                ->data('icon', 'calendar plus')
                ->data('perms', 'reschedule')
                ->active('reschedules/*');
                
            $menu->add('Pengiriman Laporan', 'pengiriman-laporan/')
                ->data('icon', 'send')
                ->data('perms', 'pengiriman-laporan')
                ->active('pengiriman-laporan/*');
            
            $menu->add('Pengembalian Barang Uji','pengembalian-barang-uji/')
                ->data('icon','briefcase')
                ->data('perms','pengembalian-barang-uji')
                ->active('pengembalian-barang-uji/*');

            /* Proses Laporan Pengujian */
            $menu->add('Laporan Pengujian', 'laporan-pengujian/')
	            ->data('perms', 'laporan-pengujian')
	            ->data('icon', 'folder open')
	            ->active('laporan-pengujian/*');
            
            /* Proses Close Order */
            $menu->add('Close Order', 'close-order/')
            	->data('icon', 'share square')->data('perms', 'close-order')
            	->active('close-order/*');
            /* Proses Posting Virtual Account */

            /* Proses Historikal data */
            $menu->add('Historikal Data','history-data/')
            	->data('icon', 'history')
            	->data('perms', 'historikal-data')
            	->active('history-data/*');
            /* Data Master */
            $menu->add('Data Master')
            	->data('icon', 'database')
            	->active('master/*');

            $menu->dataMaster->add('Pelayanan', 'master/jenis-layanan/')
            	->data('perms', 'master-jenis-pelayanan')
            	->active('master/jenis-layanan/*');

            $menu->dataMaster->add('Lingkup', 'master/lingkup/')
            	->data('perms', 'master-lingkup')
            	->active('master/lingkup/*');

            $menu->dataMaster->add('Jenis Pengujian', 'master/jenis-pengujian/')
            	->data('perms', 'master-jenis-pengujian')
            	->active('master/jenis-pengujian/*');
            $menu->dataMaster->add('Perusahaan', 'master/perusahaan/')
            	->data('perms', 'master-perusahaan')
            	->active('master/perusahaan/*');
            $menu->dataMaster->add('Contact', 'master/contact/')
            	->data('perms', 'master-contact')
            	->active('master/contact/*');
            $menu->dataMaster->add('Survei', 'master/survei/')
            	->data('perms', 'master-survei')
            	->active('master/survei/*');
            $menu->dataMaster->add('Satuan', 'master/satuan/')
            	->data('perms', 'master-satuan')
            	->active('master/satuan/*');
            $menu->dataMaster->add('Hari Kerja','master/hari-kerja/')
            	->data('perms','master-hari-kerja')
            	->active('master/hari-kerja/*');
            $menu->dataMaster->add('Prioritas','master/prioritas/')
            	->data('perms','master-prioritas')
            	->active('master/prioritas/*');

            /* Pengaduan */
            $menu->add('Pengaduan', 'pengaduan/')
            	->data('icon', 'phone')
            	->data('perms', 'pengaduan')
            	->active('pengaduan/*');

            /* Konfigurasi */
            $menu->add('Konfigurasi')
            	->data('icon', 'settings')
            	->active('konfigurasi/*');
            $menu->konfigurasi->add('Manajemen Pengguna', 'konfigurasi/users/')
            	->data('perms', 'konfigurasi-users')
            	->active('konfigurasi/users/*');
            $menu->konfigurasi->add('Hak Akses', 'konfigurasi/roles/')
            	->data('perms', 'konfigurasi-roles')
            	->active('konfigurasi/roles/*');

            /* Aktivasi User */
            $menu->add('Aktivasi Peminta Jasa ','aktivasiuser/aktivasi-user/')
            	->data('icon', 'unlock')
            	->data('perms', 'aktivasi-user')
            	->active('aktivasiuser/aktivasi-user/*');
        });

        Menu::make('topMenu', function ($menu) {
            $menu->add(trans('translator.Order Berjalan'), 'frontend/pendaftaran-pengujian/')
            	->data('icon', 'envelope close outline')
                         // ->data('perms', 'pendaftaran-pengujian')
            	->active('frontend/pendaftaran-pengujian/*');

            $menu->add(trans('translator.Riwayat Order'), 'frontend/daftar-order/')
            	->data('icon', 'envelope open')
                         // ->data('perms', 'daftar-order')
            	->active('frontend/daftar-order/*');

            $menu->add(trans('translator.Survei'), 'frontend/survei/')
            	->data('icon', 'comments outline')
                         // ->data('perms', 'daftar-order')
            	->active('frontend/survei/*');

            $menu->add(trans('translator.Pengaturan Akun'), 'frontend/account-setting/')
            	->data('icon', 'list')
                         // ->data('perms', 'account-setting')
            ->active('frontend/account-setting/*');
        });

        return $next($request);
    }
}
