<?php
ini_set('memory_limit', '1024M');
use Carbon\CarbonInterval;
if (!function_exists('DisplayStatusRole')){
  function DisplayStatusRole($string) {
    $return = '-';
    switch ($string)
    {
        case 'User Proyek': $return = 'Proyek';
        break;
        case 'User Head Office': $return = 'Head Office';
        break;
        case 'User Divisi': $return = 'Wilayah';
        break;
    }

    return $return;
  }
}



if (!function_exists('wrapText')){
  function wrapText($string) {
    return wordwrap($string, 50, '<br>\n');
  }
}



if (!function_exists('formatEnMonth')){
  function formatEnMonth($bulan) {
    $blnInt = 0;
    switch ($bulan)
    {
        case 'Januari': $blnInt = 'January';
        break;
        case 'Februari': $blnInt = 'February';
        break;
        case 'Maret': $blnInt = 'March';
        break;
        case 'April': $blnInt = 'April';
        break;
        case 'Mei': $blnInt = 'May';
        break;
        case 'Juni': $blnInt = 'June';
        break;
        case 'Juli': $blnInt = 'July';
        break;
        case 'Agustus': $blnInt = 'August';
        break;
        case 'September': $blnInt = 'September';
        break;
        case 'Oktober': $blnInt = 'October';
        break;
        case 'November': $blnInt = 'November';
        break;
        case 'Desember': $blnInt = 'December';
        break;
    }

    return $blnInt;
  }
}

if (!function_exists('formatNumMonth')){
  function formatNumMonth($bulan) {
    $blnInt = 0;
    switch ($bulan)
    {
        case 'Januari': $blnInt = 1;
        break;
        case 'Februari': $blnInt = 2;
        break;
        case 'Maret': $blnInt = 3;
        break;
        case 'April': $blnInt = 4;
        break;
        case 'Mei': $blnInt = 5;
        break;
        case 'Juni': $blnInt = 6;
        break;
        case 'Juli': $blnInt = 7;
        break;
        case 'Agustus': $blnInt = 8;
        break;
        case 'September': $blnInt = 9;
        break;
        case 'Oktober': $blnInt = 10;
        break;
        case 'November': $blnInt = 11;
        break;
        case 'Desember': $blnInt = 12;
        break;
    }

    return $blnInt;
  }
}

if (!function_exists('formatBarcode')){
  function formatBarcode($no_order, $jenis, $no_seri, $index) {
   		return $no_order."-".$jenis."-".$no_seri."-".$index;
  }
}

if (!function_exists('formatStringMonth')){
  function formatStringMonth($bulan) {
    $blnInt = 0;
    switch ($bulan)
    {
        case '01': $blnInt = 'Januari';
        break;
        case '02': $blnInt = 'Februari';
        break;
        case '03': $blnInt = 'Maret';
        break;
        case '04': $blnInt = 'April';
        break;
        case '05': $blnInt = 'Mei';
        break;
        case '06': $blnInt = 'Juni';
        break;
        case '07': $blnInt = 'Juli';
        break;
        case '08': $blnInt = 'Agustus';
        break;
        case '09': $blnInt = 'September';
        break;
        case '10': $blnInt = 'Oktober';
        break;
        case '11': $blnInt = 'November';
        break;
        case '12': $blnInt = 'Desember';
        break;
    }

    return $blnInt;
  }
}

if (!function_exists('DiffMnY')){
  function DiffMnY($bulan, $tahun) {
    $start = Carbon\Carbon::parse('first day of '.formatEnMonth($bulan).' '.$tahun);
    $end = Carbon\Carbon::parse('last day of '.formatEnMonth($bulan).' '.$tahun);

    return $start->diffInDays($end);
  }
}

if (!function_exists('DateToSql')) {
    function DateToSql($date) {
        if($date != NULL)
        {
            $pecah = explode(" ", $date);
            $tglStr = str_replace(",", "", $pecah[1]);
            if(strlen($tglStr) == 1)
            {
                $tglStr = "0".$tglStr;
            }
            $thnStr = $pecah[2];
            $blnStr = "";
            switch ($pecah[0])
            {
                case 'Januari': $blnStr = '01';
                break;
                case 'Februari': $blnStr = '02';
                break;
                case 'Maret': $blnStr = '03';
                break;
                case 'April': $blnStr = '04';
                break;
                case 'Mei': $blnStr = '05';
                break;
                case 'Juni': $blnStr = '06';
                break;
                case 'Juli': $blnStr = '07';
                break;
                case 'Agustus': $blnStr = '08';
                break;
                case 'September': $blnStr = '09';
                break;
                case 'Oktober': $blnStr = '10';
                break;
                case 'November': $blnStr = '11';
                break;
                case 'Desember': $blnStr = '12';
                break;
            }
            return $thnStr."-".$blnStr."-".$tglStr;
        }else{
            return NULL;
        }
    }
}

if (!function_exists('DayOf')) {
    function DayOf($date){
      switch ($date->format('l'))
      {
          case 'Sunday': return 'Minggu';
          break;
          case 'Monday': return 'Senin';
          break;
          case 'Tuesday': return 'Selasa';
          break;
          case 'Wednesday': return 'Rabu';
          break;
          case 'Thursday': return 'Kamis';
          break;
          case 'Friday': return 'Jumat';
          break;
          case 'Saturday': return 'Sabtu';
          break;
      }
    }
}

if (!function_exists('imageShow')) {
    function imageShow($file) {
      if($file->count() > 0)
      {
          $str = explode(".",$file->first()->url);

          if(strtolower($str[1]) == 'jpeg' || strtolower($str[1]) == 'png' || strtolower($str[1]) == 'jpg')
          {
              return url('storage/'.$file->first()->url);
          }

          return asset('img/archive.png');
      }
      return asset('img/no-images.png');
    }
}

if (!function_exists('singleImageShow')) {
    function singleImageShow($file) {
      $str = explode(".",$file->url);

      if(strtolower($str[1]) == 'jpeg' || strtolower($str[1]) == 'png' || strtolower($str[1]) == 'jpg')
      {
          return url('storage/'.$file->url);
      }

      return asset('img/archive.png');
    }
}

if (!function_exists('DateToString')) {
    function DateToString($date) {
        if(!$date)
        {
            return '-';
        }
        // $date = new DateTime($date);
        $date = \Carbon\Carbon::parse($date)->format('Y-m-d');
        // $tgl = $date->format('Y-m-d');
        $pecah = explode("-", $date);
        $thnStr = $pecah[0];
        $tglStr = $pecah[2].",";
        $blnStr = "";
        switch ($pecah[1])
        {
            case '01': $blnStr = 'Januari';
            break;
            case '02': $blnStr = 'Februari';
            break;
            case '03': $blnStr = 'Maret';
            break;
            case '04': $blnStr = 'April';
            break;
            case '05': $blnStr = 'Mei';
            break;
            case '06': $blnStr = 'Juni';
            break;
            case '07': $blnStr = 'Juli';
            break;
            case '08': $blnStr = 'Agustus';
            break;
            case '09': $blnStr = 'September';
            break;
            case '10': $blnStr = 'Oktober';
            break;
            case '11': $blnStr = 'November';
            break;
            case '12': $blnStr = 'Desember';
            break;
        }
        return $blnStr." ".$tglStr." ".$thnStr;
    }
}

if (!function_exists('BulanToString')) {
    function BulanToString($date) {
        if(!$date)
        {
            return '-';
        }
        $pecah = explode("-", $date);
        $thnStr = $pecah[0];
        $blnStr = "";
        switch ($pecah[1])
        {
            case '01': $blnStr = 'Januari';
            break;
            case '02': $blnStr = 'Februari';
            break;
            case '03': $blnStr = 'Maret';
            break;
            case '04': $blnStr = 'April';
            break;
            case '05': $blnStr = 'Mei';
            break;
            case '06': $blnStr = 'Juni';
            break;
            case '07': $blnStr = 'Juli';
            break;
            case '08': $blnStr = 'Agustus';
            break;
            case '09': $blnStr = 'September';
            break;
            case '10': $blnStr = 'Oktober';
            break;
            case '11': $blnStr = 'November';
            break;
            case '12': $blnStr = 'Desember';
            break;
        }
        return $blnStr." ".$thnStr;
    }
}

if (!function_exists('DateToStringWday')) {
    function DateToStringWday($date) {
        if(!$date)
        {
            return '-';
        }
        $tgl = $date->format('Y-m-d');
        $pecah = explode("-", $tgl);
        $thnStr = $pecah[0];
        $tglStr = $pecah[2]."";
        $blnStr = "";
        switch ($pecah[1])
        {
            case '01': $blnStr = 'Januari';
            break;
            case '02': $blnStr = 'Februari';
            break;
            case '03': $blnStr = 'Maret';
            break;
            case '04': $blnStr = 'April';
            break;
            case '05': $blnStr = 'Mei';
            break;
            case '06': $blnStr = 'Juni';
            break;
            case '07': $blnStr = 'Juli';
            break;
            case '08': $blnStr = 'Agustus';
            break;
            case '09': $blnStr = 'September';
            break;
            case '10': $blnStr = 'Oktober';
            break;
            case '11': $blnStr = 'November';
            break;
            case '12': $blnStr = 'Desember';
            break;
        }
        return DayOf($date)." ".$tglStr." ".$blnStr." ".$thnStr;
    }
}

if (!function_exists('statusReview')) {
    function statusReview($status, $url = null) {
        switch ($status)
        {
            case 0: return '<a class="ui tag label" href="'.$url.'">Belum Dibaca</a>';
            break;
            case 1: return '<a class="ui teal tag label" href="'.$url.'">Sudah Dibaca</a>';
            break;
        }
    }
}

if (!function_exists('statusLabel')) {
    function statusLabel($status, $url) {
        switch ($status)
        {
            case 0: return '<a href="'.url($url).'" class="ui orange ribbon label">Belum Dibaca</a>';
            break;
            case 1: return '<a href="'.url($url).'" class="ui teal ribbon label">Sudah Dibaca</a>';
            break;
        }
    }
}

if (!function_exists('statusTindakan')) {
    function statusTindakan($status) {
        switch ($status)
        {
            case 0: return '<a href="javascript:void(0)" class="ui orange tag label">Belum Selesai</a>';
            break;
            case 1: return '<a href="javascript:void(0)" class="ui teal tag label">Selesai</a>';
            break;
        }
    }
}

if (!function_exists('imageItem')) {
  function imageItem($picture) {
      if($picture)
      {
        return '<img src="'.url('storage/'.$picture).'" style="height:8rem">';
      }
      return '<img src="'.asset('img/no-images.png').'" style="height:8rem">';
  }
}

if (!function_exists('readMoreText')) {
    function readMoreText($value, $maxLength = 150)
    {
        $return = textarea($value);
        if (strlen($value) > $maxLength) {
            $return = substr(textarea($value), 0, $maxLength);
            $readmore = substr(textarea($value), $maxLength);

            $return .= '<a href="javascript: void(0)" class="read-more text-info" onclick="$(this).parent().find(\'.read-more-cage\').show(); $(this).hide()">&nbsp;&nbsp;Selengkapnya...</a>';

            $readless = '<a href="javascript: void(0)" class="read-less text-info" onclick="$(this).parent().parent().find(\'.read-more\').show(); $(this).parent().hide()">&nbsp;&nbsp;Kecilkan...</a>';

            $return = "<span>{$return}<span style='display: none' class='read-more-cage'>{$readmore} {$readless}</span></span>";
        }
        return $return;
    }
}

if (!function_exists('stringTakenAt')) {
    function stringTakenAt($taken, $waktu, $tanggal)
    {
        $pecah = explode(" ", $waktu);
        $p = explode(":", $pecah[0]);
        $hours = (int)$p[0];
        $minutes = (int)$p[1];
        if($pecah[1] == 'PM')
        {
            $hours = $hours + 12;
        }
        $ex = explode(" ", $tanggal);

        $fullDate = Carbon\Carbon::parse($ex[0]." ".$hours.":".$minutes.":00");
        $diff = $fullDate->diffInHours($taken);
        if($diff > 2)
        {
            return '<span style="font-size:10px;color:red;"><i>Data diambil diatas 2 jam pada : '.$taken->format('F d, Y (H:i:s)').'</i></span>';
        }
        return '<span style="font-size:10px;color:green;"><i>Data diambil dibawah 2 jam pada : '.$taken->format('F d, Y (H:i:s)').'</i></span>';
    }
}

if (!function_exists('textarea')) {
    function textarea($text)
    {
        $new = '';

        $new = str_replace("\n", "<br>", $text);

        return $new;
    }
}


// if(!function_exists('HTML2PDF')){
//     function HTML2PDF($content='', $data=[]){
//             $komponen['file_name'] = 'default';
//             $komponen['page'] = 'P'; //P Page atau L lanscape

//             $data = array_merge($komponen,$data);

//             ob_start();
//             ob_end_clean();
//             $content = $content;
//             $html2pdf = New Html2Pdf($data['page'], 'A4', 'fr', true, 'UTF-8', array(5, 5, 5, 5));
//             $html2pdf->pdf->SetDisplayMode('fullpage');
//             $html2pdf->writeHTML($content);
//             $html2pdf->output($data['file_name'].'.pdf');
//     }
// }

if (!function_exists('column_letter')) {
    function column_letter($c) {

        $c = intval($c);

        if ($c <= 0)
            return '';

        $letter = '';
        while($c != 0){
           $p = ($c - 1) % 26;
           $c = intval(($c - $p) / 26);
           $letter = chr(65 + $p) . $letter;
        }

        return $letter;
    }
}

if (!function_exists('numDay')) {
    function numDay($day) {
        if($day != NULL)
        {
            $blnStr = "";
            switch ($day)
            {
                case '0': $blnStr = 'Senin';
                break;
                case '1': $blnStr = 'Selasa';
                break;
                case '2': $blnStr = 'Rabu';
                break;
                case '3': $blnStr = 'Kamis';
                break;
                case '4': $blnStr = 'Jumat';
                break;
                case '5': $blnStr = 'Sabtu';
                break;
                case '6': $blnStr = 'Minggu';
                break;
            }
            return $blnStr;
        }else{
            return '';
        }
    }
}

if (!function_exists('ipAddress')) {
    function ipAddress(){
        switch(true){
      case (!empty($_SERVER['HTTP_X_REAL_IP'])) : return $_SERVER['HTTP_X_REAL_IP'];
      case (!empty($_SERVER['HTTP_CLIENT_IP'])) : return $_SERVER['HTTP_CLIENT_IP'];
      case (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) : return $_SERVER['HTTP_X_FORWARDED_FOR'];
      default : return $_SERVER['REMOTE_ADDR'];
    }
    }
}

if (!function_exists('slugify')) {
    function slugify($text){
          // replace non letter or digits by -
          $text = preg_replace('~[^\pL\d]+~u', '-', $text);

          // transliterate
          $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

          // remove unwanted characters
          $text = preg_replace('~[^-\w]+~', '', $text);

          // trim
          $text = trim($text, '-');

          // remove duplicate -
          $text = preg_replace('~-+~', '-', $text);

          // lowercase
          $text = strtolower($text);

          if (empty($text)) {
            return 'n-a';
          }

          return $text;
        }
}

if (!function_exists('tipeCostumers')) {
    function tipeCostumers($data)  
    {  
        if(!is_null($data))
        {
            switch ($data)
            {
                case 0: $costumer = 'PLN';
                break;
                case 1: $costumer = 'NON-PLN';
                break;
                case 2: $costumer = 'A-PLN';
                break;
            }
            return $costumer;
        }else{
            return '';
        }  
    }  
}

if (!function_exists('status')) {
    function status($data)  
    {  
        if(!is_null($data))
        {
            switch ($data)
            {
                case 0: $status = '-';
                break;
                case 1: $status = trans('translator.Penerimaan Surat');
                break;
                case 2: $status = trans('translator.Surat Penawaran Terkirim');
                break;
                case 3: $status = trans('translator.Konfirmasi Pembayaran');
                break;
                case 4: $status = trans('translator.Pelaksanaan Pengujian');
                break;
                case 5: $status = trans('translator.Penyelesaian Laporan');
                break;
                case 6: $status = trans('translator.Pengiriman Barang');
                break;
                case 7: $status = trans('translator.Selesai');
                break;
                case 8: $status = trans('translator.Dibatalkan');
                break;
                case 9: $status = trans('translator.Gagal Diproses');
                break;
                case 10: $status = trans('translator.Ditolak');
                break;
            }
            return $status;
        }else{
            return '-';
        }  
    }  
}

if (!function_exists('statusa')) {
    function statusa($data)  
    {  	
    	$status = '-';
        if(!is_null($data))
        {
            switch ($data)
            {
                case 0: $status = '-';
                break;
                case 1: $status = 'Penerimaan Surat';
                break;
                case 2: $status = 'Surat Penawaran Terkirim';
                break;
                case 3: $status = 'Konfirmasi Pembayaran';
                break;
                case 4: $status = 'Pelaksanaan Pengujian';
                break;
                case 5: $status = 'Penyelesaian Laporan';
                break;
                case 6: $status = 'Pengiriman Barang';
                break;
                case 7: $status = 'Selesai';
                break;
                case 8: $status = 'Dibatalkan';
                break;
                case 9: $status = 'Gagal Diproses';
                break;
                case 10: $status = 'Ditolak';
                break;
            }
            return $status;
        }else{
            return '-';
        }
    }  
}

if (!function_exists('DateToStringYear')) {
    function DateToStringYear($date) {
        if(!$date)
        {
            return '-';
        }
        $date = \Carbon\Carbon::parse($date)->format('Y-m-d');
        $pecah = explode("-", $date);
        $thnStr = $pecah[0];
        $tglStr = $pecah[2];
        $blnStr = "";
        switch ($pecah[1])
        {
            case '01': $blnStr = 'Januari';
            break;
            case '02': $blnStr = 'Februari';
            break;
            case '03': $blnStr = 'Maret';
            break;
            case '04': $blnStr = 'April';
            break;
            case '05': $blnStr = 'Mei';
            break;
            case '06': $blnStr = 'Juni';
            break;
            case '07': $blnStr = 'Juli';
            break;
            case '08': $blnStr = 'Agustus';
            break;
            case '09': $blnStr = 'September';
            break;
            case '10': $blnStr = 'Oktober';
            break;
            case '11': $blnStr = 'November';
            break;
            case '12': $blnStr = 'Desember';
            break;
        }
        return $tglStr." ".$blnStr." ".$thnStr;
    }
}

if(!function_exists('FormatNumber')){
    function FormatNumber($int){
        return number_format($int, 0, ',', '.');
    }
}

if (!function_exists('decimal_for_save')) {
    function decimal_for_save($text)
    {
        $text_edit = str_replace('.', '', $text);
        $newtext = str_replace(',', '.', $text_edit);

        return $newtext;
    }
}

if (!function_exists('timestamp')) {
    function timestamp($awal, $akhir, $tipe, $jenis)
    {
		$deadline = 1;

		if($jenis == 'yan-kalibrasi' || $jenis == 'yan-uji'){
			$deadline = 1;
		}elseif($jenis == 'msb-kalibrasi' || $jenis == 'msb-siskit' || $jenis == 'msb-sistgi' || $jenis == 'msb-tegangan-rendah' || $jenis == 'msb-tegangan-tinggi'){
			$deadline = 4;
		}elseif($jenis == 'asman-kalibrasi' || $jenis == 'asman-siskit' || $jenis == 'asman-sistgi' || $jenis == 'asman-tegangan-rendah' || $jenis == 'asman-tegangan-tinggi'){
			$deadline = 4;
		}

		$tgl1 	= Carbon::parse($awal->created_at);

		$tgl2 	= $akhir;
        $d  	= $tgl1->diffInDays($tgl2);
        $s  	= $tgl1->diffInSeconds($tgl2);

        $color = ($d > $deadline) ? "red" : "green";

		echo '<label class="ui tag" style="color: '.$color.';"><b> '.$d.' Hari '.gmdate('G', $s).' Jam '.intval(gmdate('i', $s)).' Menit '.intval(gmdate('s', $s)).' Detik</b></label>';
    }
}

if (!function_exists('timestampReturn')) {
    function timestampReturn($awal, $akhir, $tipe, $jenis)
    {
        $deadline = 1;

        if($jenis == 'yan-kalibrasi' || $jenis == 'yan-uji'){
            $deadline = 1;
        }elseif($jenis == 'msb-kalibrasi' || $jenis == 'msb-siskit' || $jenis == 'msb-sistgi' || $jenis == 'msb-tegangan-rendah' || $jenis == 'msb-tegangan-tinggi'){
            $deadline = 4;
        }elseif($jenis == 'asman-kalibrasi' || $jenis == 'asman-siskit' || $jenis == 'asman-sistgi' || $jenis == 'asman-tegangan-rendah' || $jenis == 'asman-tegangan-tinggi'){
            $deadline = 4;
        }
        $tgl1   = Carbon::parse($awal->created_at);
        $tgl2   = Carbon::parse($akhir);

        $diff = $tgl1->diffFiltered(CarbonInterval::second(), function(Carbon $date) {
		   return !$date->isWeekend();
		}, $tgl2, true);
        $d=intval(intval($diff) / (3600*24));
		$waktu = intval(intval($diff) / (3600*24)). " Hari "
				.intval(gmdate("H", $diff))." Jam "
				.intval(gmdate("i", $diff))." Menit "
				.intval(gmdate("s", $diff))." Detik";
        $color = ($d > $deadline) ? "red" : "green";

        return '<label class="ui tag" style="color: '.$color.';"><b> '.$waktu.'</b></label>, <label style="color:orange;">pada : '.$tgl2->format('d F Y  H:i:s').'</label>';
    }
}


if (!function_exists('timestampReturnHistory')) {
    function timestampReturnHistory($awal, $akhir, $tipe, $jenis, $nama)
    {
        $deadline = 1;

        if($jenis == 'yan-kalibrasi' || $jenis == 'yan-uji'){
            $deadline = 1;
        }elseif($jenis == 'msb-kalibrasi' || $jenis == 'msb-siskit' || $jenis == 'msb-sistgi' || $jenis == 'msb-tegangan-rendah' || $jenis == 'msb-tegangan-tinggi'){
            $deadline = 4;
        }elseif($jenis == 'asman-kalibrasi' || $jenis == 'asman-siskit' || $jenis == 'asman-sistgi' || $jenis == 'asman-tegangan-rendah' || $jenis == 'asman-tegangan-tinggi'){
            $deadline = 4;
        }

        $tgl1   = Carbon::parse($awal->created_at);

        $tgl2   = $akhir;
        $d      = $tgl1->diffInDays($tgl2);
        $s      = $tgl1->diffInSeconds($tgl2);

        $color = ($d > $deadline) ? "red" : "green";

        echo '<label class="ui tag" style="color: '.$color.';"><b> '.$d.' Hari '.gmdate('G', $s).' Jam '.intval(gmdate('i', $s)).' Menit '.intval(gmdate('s', $s)).' Detik</b></label></br> Oleh </br> '.$nama.'';
    }
}

if (!function_exists('timestampReturnHistoryV2')) {
    function timestampReturnHistoryV2($awal, $akhir, $tipe, $jenis, $nama)
    {
        $deadline = 1;

        if($jenis == 'yan-kalibrasi' || $jenis == 'yan-uji'){
            $deadline = 1;
        }elseif($jenis == 'msb-kalibrasi' || $jenis == 'msb-siskit' || $jenis == 'msb-sistgi' || $jenis == 'msb-tegangan-rendah' || $jenis == 'msb-tegangan-tinggi'){
            $deadline = 4;
        }elseif($jenis == 'asman-kalibrasi' || $jenis == 'asman-siskit' || $jenis == 'asman-sistgi' || $jenis == 'asman-tegangan-rendah' || $jenis == 'asman-tegangan-tinggi'){
            $deadline = 4;
        }

        $tgl1   = Carbon::parse($awal->created_at);

        $tgl2   = $akhir;
        $d      = $tgl1->diffInDays($tgl2);
        $s      = $tgl1->diffInSeconds($tgl2);

        $color = ($d > $deadline) ? "red" : "green";

        return '<label class="ui tag" style="color: '.$color.';"><b> '.$d.' Hari '.gmdate('G', $s).' Jam '.intval(gmdate('i', $s)).' Menit '.intval(gmdate('s', $s)).' Detik</b></label></br> Oleh </br> '.$nama.'';
    }
}

if (!function_exists('timestampReturnData')) {
    function timestampReturnData($awal, $akhir, $tipe, $jenis, $nama)
    {
        $deadline = 1;

        if($jenis == 'yan-kalibrasi' || $jenis == 'yan-uji'){
            $deadline = 1;
        }elseif($jenis == 'msb-kalibrasi' || $jenis == 'msb-siskit' || $jenis == 'msb-sistgi' || $jenis == 'msb-tegangan-rendah' || $jenis == 'msb-tegangan-tinggi'){
            $deadline = 4;
        }elseif($jenis == 'asman-kalibrasi' || $jenis == 'asman-siskit' || $jenis == 'asman-sistgi' || $jenis == 'asman-tegangan-rendah' || $jenis == 'asman-tegangan-tinggi'){
            $deadline = 4;
        }

        $tgl1   = Carbon::parse($awal);

        $tgl2   = $akhir;
        $d      = $tgl1->diffInDays($tgl2);
        $s      = $tgl1->diffInSeconds($tgl2);

        $color = ($d > $deadline) ? "red" : "green";

        echo '<label class="ui tag" style="color: '.$color.';"><b> '.$d.' Hari '.gmdate('G', $s).' Jam '.intval(gmdate('i', $s)).' Menit '.intval(gmdate('s', $s)).' Detik</b></label></br> Oleh </br> '.$nama.'';
    }
}

if (!function_exists('timestampReturnDataV2')) {
    function timestampReturnDataV2($awal, $akhir, $tipe, $jenis, $nama)
    {
        $deadline = 1;

        if($jenis == 'yan-kalibrasi' || $jenis == 'yan-uji'){
            $deadline = 1;
        }elseif($jenis == 'msb-kalibrasi' || $jenis == 'msb-siskit' || $jenis == 'msb-sistgi' || $jenis == 'msb-tegangan-rendah' || $jenis == 'msb-tegangan-tinggi'){
            $deadline = 4;
        }elseif($jenis == 'asman-kalibrasi' || $jenis == 'asman-siskit' || $jenis == 'asman-sistgi' || $jenis == 'asman-tegangan-rendah' || $jenis == 'asman-tegangan-tinggi'){
            $deadline = 4;
        }

        $tgl1   = Carbon::parse($awal);

        $tgl2   = $akhir;
        $d      = $tgl1->diffInDays($tgl2);
        $s      = $tgl1->diffInSeconds($tgl2);

        $color = ($d > $deadline) ? "red" : "green";

        return '<label class="ui tag" style="color: '.$color.';"><b> '.$d.' Hari '.gmdate('G', $s).' Jam '.intval(gmdate('i', $s)).' Menit '.intval(gmdate('s', $s)).' Detik</b></label></br> Oleh </br> '.$nama.'';
    }
}

if (!function_exists('timestampReturnData')) {
    function timestampReturnAsman($awal, $akhir, $tipe, $jenis, $nama)
    {
        $deadline = 1;

        if($jenis == 'yan-kalibrasi' || $jenis == 'yan-uji'){
            $deadline = 1;
        }elseif($jenis == 'msb-kalibrasi' || $jenis == 'msb-siskit' || $jenis == 'msb-sistgi' || $jenis == 'msb-tegangan-rendah' || $jenis == 'msb-tegangan-tinggi'){
            $deadline = 4;
        }elseif($jenis == 'asman-kalibrasi' || $jenis == 'asman-siskit' || $jenis == 'asman-sistgi' || $jenis == 'asman-tegangan-rendah' || $jenis == 'asman-tegangan-tinggi'){
            $deadline = 4;
        }

        $tgl1   = Carbon::parse($awal);

        $tgl2   = $akhir;
        $d      = $tgl1->diffInDays($tgl2);
        $s      = $tgl1->diffInSeconds($tgl2);

        $color = ($d > $deadline) ? "red" : "green";

        echo '<label class="ui tag" style="color: '.$color.';"><b> '.$d.' Hari '.gmdate('G', $s).' Jam '.intval(gmdate('i', $s)).' Menit '.intval(gmdate('s', $s)).' Detik</b></label></br> Oleh </br> '.$nama.'';
    }
}

if (!function_exists('timestampReturnData')) {
    function timestampReturnAsmanLaporan($awal, $akhir, $tipe, $jenis, $nama)
    {
        $deadline = 1;

        if($jenis == 'yan-kalibrasi' || $jenis == 'yan-uji'){
            $deadline = 1;
        }elseif($jenis == 'msb-kalibrasi' || $jenis == 'msb-siskit' || $jenis == 'msb-sistgi' || $jenis == 'msb-tegangan-rendah' || $jenis == 'msb-tegangan-tinggi'){
            $deadline = 4;
        }elseif($jenis == 'asman-kalibrasi' || $jenis == 'asman-siskit' || $jenis == 'asman-sistgi' || $jenis == 'asman-tegangan-rendah' || $jenis == 'asman-tegangan-tinggi'){
            $deadline = 4;
        }

        $tgl1   = Carbon::parse($awal);

        $tgl2   = $akhir;
        $d      = $tgl1->diffInDays($tgl2);
        $s      = $tgl1->diffInSeconds($tgl2);

        $color = ($d > $deadline) ? "red" : "green";

        echo '<label class="ui tag" style="color: '.$color.';"><b> '.$d.' Hari '.gmdate('G', $s).' Jam '.intval(gmdate('i', $s)).' Menit '.intval(gmdate('s', $s)).' Detik</b></label></br> Oleh </br> '.$nama.'';
    }
}


if (!function_exists('getHariKerja')) {
    function getHariKerja($menu, $layanan)
    {
        $hari = 4;

        $data = \App\Models\Master\HariKerja::where('menu',$menu)->where('jenis_pelayanan_id',$layanan)->first();
        if($data){
            if(!is_null($data->hk)){
                $hari = (int) $data->hk;
            }
        }

        return $hari;
    }
}

if (!function_exists('bubble')) {
    function bubble($menu)
    {
    	$user = auth()->user();
    	$online='';$ams='';$spm='';$jumlah='';$kaji='';$diskusi='';$surat='';$on_progress='';$va='';$konfirmasi='';$realisasi='';$angka='';$angka_kaji='';$angka_surat='';$angka_va='';$angka_konfirmasi='';$angka_realisasi='';$angka_nota='';$nota='';$pengembalian_dana='';$angka_dana='';

    	if($menu == 'penerimaanSurat'){
    		if($user->hasRole(['yan-kalibrasi','admin'])){
	    		$online 	= App\Models\FrontEnd\PendaftaranPengujian::whereIn('status', [1,11,12])->where('jenis', 0)->where('tipe', 1)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
	    		$spm 		= App\Models\FrontEnd\PendaftaranPengujian::whereIn('status', [1,11,12])->where('jenis', 0)->where('tipe', 3)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
	    		$ams        = App\Models\FrontEnd\PendaftaranPengujian::whereIn('status', [1,11,12])->where('tipe', 2)->where('status_ams', 1)->where('layanan_id', 1)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
	    		$jumlah 	= $online + $spm + $ams; 

				$records 	= App\Models\FrontEnd\PendaftaranPengujian::isDoneKal();
				$kirim 		= App\Models\FrontEnd\PendaftaranPengujian::isKirimKal();
				$histori 	= App\Models\FrontEnd\PendaftaranPengujian::isHistoriKal();

				$surat 		= count($records) + count($kirim);
	    	}elseif($user->hasRole(['yan-uji','admin'])){
	    		$online 	= App\Models\FrontEnd\PendaftaranPengujian::whereIn('status', [1,11,12])->where('jenis', 1)->where('tipe', 1)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
	    		$spm 		= App\Models\FrontEnd\PendaftaranPengujian::whereIn('status', [1,11,12])->where('jenis', 1)->where('tipe', 3)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
	    		$ams        = App\Models\FrontEnd\PendaftaranPengujian::whereIn('status', [1,11,12])->where('tipe', 2)->where('status_ams', 1)->whereIn('layanan_id', [2,3,4,5])->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
	    		$jumlah 	= $online + $spm + $ams;

	    		$records 	= App\Models\FrontEnd\PendaftaranPengujian::isDoneUji();
				$kirim 		= App\Models\FrontEnd\PendaftaranPengujian::isKirimUji();
				$histori 	= App\Models\FrontEnd\PendaftaranPengujian::isHistoriUji();

				$surat 		= count($records) + count($kirim);
	    	}elseif($user->hasRole(['admin'])){
                $ams        = App\Models\FrontEnd\PendaftaranPengujian::whereIn('status', [1,11,12])->where('tipe', 2)->where('status_ams', 0)->where('tipe_surat', 0)->orderBy('no_order','asc')->count();
	    		$jumlah 	= $ams; 
	    	}else{
	    		$jumlah 	='';
	    	}
	    	if($jumlah > 0){
		        echo "<span style='background-color:red;'class='ui circular label'>".$jumlah."</span>";
	    	}
    	}elseif($menu == 'kajiUlang'){
    		if($user->hasRole(['msb-kalibrasi', 'msb-siskit', 'msb-sistgi', 'msb-tegangan-rendah', 'msb-tegangan-tinggi','admin'])){
	    		$on_progress = App\Models\FrontEnd\PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	    											$q->where('penerima_id', $user->id)
	    											  ->where('jenis', 1);
	    										})
	    										->whereIn('status', [2,12])
	    										->doesntHave('kaji_ulang')
	    										->where('tipe_surat', 0)
	    										->orderBy('no_order','asc')->count();
	    		$diskusi = App\Models\FrontEnd\PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->where('penerima_id', $user->id)
	                            				  ->where('jenis', 1);
		                          				})
		        								->where('status', 2)
		        								->whereHas('kaji_ulang', function($u){
		        									$u->where('keputusan', 3);
		        								})
		        								->where('tipe_surat', 0)
		        								->orderBy('no_order','asc')->count();
		       	$kaji 		= $on_progress + $diskusi;
	    	}elseif($user->hasRole(['asman-dal-kalibrasi','asman-dal-siskit','asman-dal-sistgi','asman-dal-tegangan-tinggi','asman-dal-tegangan-rendah', 'asman-lola-kalibrasi','asman-lola-siskit','asman-lola-sistgi','asman-lola-tegangan-tinggi','asman-lola-tegangan-rendah','admin'])){
	    		$on_progress = App\Models\FrontEnd\PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	    											$q->where('penerima_id', $user->id)
	    											  ->where('jenis', 2);
	    										})
	    										->where('status', 2)
	    										->doesntHave('kaji_ulang')
	    										->where('tipe_surat', 0)
	    										->orderBy('no_order','asc')->count();
	    		$diskusi 	= App\Models\FrontEnd\PendaftaranPengujian::whereHas('act_dispo', function($q) use ($user){
	                            				$q->where('pengirim_id', $user->id)
	                            				  ->where('jenis', 2);
		                          				})
		        								->where('status', 2)
		        								->whereHas('kaji_ulang', function($u){
		        									$u->where('keputusan', 3);
		        								})
		        								->where('tipe_surat', 0)
		        								->orderBy('no_order','asc')->count();
		       	$kaji 		= $on_progress + $diskusi;
	    	}else{
	    		$kaji 		= '';
	    	}

	    	if($kaji > 0){
		        echo "<span style='background-color:red;'class='ui circular label'>".$kaji."</span>";
	    	}
    	}elseif($menu == 'suratPenawaran'){
    		if($user->hasRole(['yan-kalibrasi','admin'])){
				$records 	= App\Models\FrontEnd\PendaftaranPengujian::isDoneKal();
				$kirim 		= App\Models\FrontEnd\PendaftaranPengujian::isKirimKal();
				$tolak 		= App\Models\FrontEnd\PendaftaranPengujian::isTolakKal();
				$surat 		= count($records) + count($kirim) + count($tolak);
	    	}elseif($user->hasRole(['yan-uji','admin'])){
	    		$records 	= App\Models\FrontEnd\PendaftaranPengujian::isDoneUji();
				$kirim 		= App\Models\FrontEnd\PendaftaranPengujian::isKirimUji();
				$tolak 		= App\Models\FrontEnd\PendaftaranPengujian::isTolakUji();
				$surat 		= count($records) + count($kirim) + count($tolak);
	    	}else{
	    		$surat 	=0;
	    	}
	    	if($surat > 0){
		        echo "<span style='background-color:red;'class='ui circular label'>".$surat."</span>";
	    	}
    	}elseif($menu == 'virtualAccount(VA)'){
    		if($user->hasRole(['keuangan','admin'])){
		    	$satu 	= App\Models\VirtualAccount\VirtualAccount::where('status', 0)
	    							 ->whereIn('tipe_customer', [1,2])
	                                 ->whereNull('download_date')
	                                 ->where('tipe', 0)
	                                 ->get()->count();
		        $dua 	= App\Models\VirtualAccount\VirtualAccount::whereNotNull('download_by')
		        						  ->where('status', 0)
		        						  ->where('tipe_customer', '!=', 0)
		        						  ->where('tipe', 0)
		                              	  ->get()->count();
		        $tiga 	= App\Models\VirtualAccount\VirtualAccount::where('status', 1)
								         ->whereIn('tipe_customer', [1,2])
								         ->where('tipe', 0)
		                                 ->get()->count();
		        $empat 	= App\Models\VirtualAccount\VirtualAccount::whereIn('status', [2,3])
		        						 ->whereIn('tipe_customer', [1,2])
		        						 ->where('tipe', 0)
		                              	 ->get()->count();
		        $va 	= $satu + $dua + $tiga + $empat;
		    }else{
		    	$va='';
		    }

		    if($va > 0){
		        echo "<span style='background-color:red;'class='ui circular label'>".$va."</span>";
	    	}
    	}elseif($menu == 'notaBuku'){
    		if($user->hasRole(['keuangan','admin'])){
    		 	$nota = App\Models\VirtualAccount\VirtualAccount::whereHas('surat', function($s){
                                    $s->whereHas('kaji_ulang', function($k){
                                        $k->whereHas('pp', function($p){
                                            return $p->whereNotIn('status', [6,7]);
                                        });
                                    });
                                 })->where('tipe_customer', 0)->where('tipe', 0)->count();
		    }else{
		    	$nota='';
		    }

		    if($nota > 0){
		        echo "<span style='background-color:red;'class='ui circular label'>".$nota."</span>";
	    	}
    	}elseif($menu == 'konfirmasiPembayaran'){
    		if($user->hasRole(['keuangan', 'yan-uji', 'yan-kalibrasi','admin'])){
    		 	$first = App\Models\VirtualAccount\VirtualAccount::where('status', 1)
							        ->whereIn('tipe_customer', [0,1,2])
	                                ->doesnthave('konfirmasi')
	                                ->where('tipe', 0)
	                                ->get()->count();
		        $second = App\Models\VirtualAccount\VirtualAccount::where('status', 3)
		        						->whereIn('tipe_customer', [0,1,2])
		                                ->whereHas('konfirmasi', function($q){
		                                    return $q->where('status', 1);
		                                })
		                                ->where('tipe', 0)
		                                ->get()->count();
		        $third = App\Models\VirtualAccount\VirtualAccount::where('status', 3)
		        						->whereIn('tipe_customer', [0,1,2])
		                                ->whereHas('konfirmasi', function($q){
		                                    return $q->where('status', 2);
		                                })
		                                ->where('tipe', 0)
		                                ->get()->count();
		        $four = App\Models\VirtualAccount\VirtualAccount::where('status', 3)
		        						->whereIn('tipe_customer', [0,1,2])
		                                ->whereHas('konfirmasi', function($q){
		                                    return $q->where('status', 3);
		                                })
		                                ->where('tipe', 0)
		                                ->get()->count();
		        $five = App\Models\VirtualAccount\VirtualAccount::where('status', 3)
		        						->whereIn('tipe_customer', [0,1,2])
		        						->where('tipe', 0)
		                                ->get()->count();
                $six = App\Models\VirtualAccount\VirtualAccount::where('status', 3)
                                        ->whereIn('tipe_customer', [0,1,2])
                                        ->whereHas('surat', function($q){
                                        $q->whereHas('kaji_ulang',function($x){
                                            $x->whereHas('pp',function($y){
                                                return $y->where('status',6);
                                            });
                                        });
                                    })
                                   	->where('tipe', 0)
                                    ->get()->count();

		        $konfirmasi 	= $first + $second + $third + $four + $five;
		    }else{
		    	$konfirmasi='';
		    }

		    if($konfirmasi > 0){
		        echo "<span style='background-color:red;'class='ui circular label'>".$konfirmasi."</span>";
	    	}
    	}elseif($menu == 'pengembalianDana'){
    		if($user->hasRole(['keuangan','admin'])){
    		 	$satu = App\Models\VirtualAccount\VirtualAccount::where('status', 3)
											                                ->whereHas('konfirmasi',function($q) {
											                                    $q->whereHas('pengembalian',function($x) {
											                                            $x->whereNull('tgl_kembali');
											                                        })->where('status',2);
											                                })->where('tipe', 0)
											                                ->get()->count();
				$dua = App\Models\CloseOrder\CloseOrder::whereHas('penerimaan.pengujian.detailpengujian', function($u){
    																return $u->where('verifikasi', '!=', 1);
    						  								})
    						  								->doesntHave('pengembaliandana')
    						  								->get()->count();
    			$pengembalian_dana= $satu + $dua;
		    }else{
		    	$pengembalian_dana='';
		    }

		    if($pengembalian_dana > 0){
		        echo "<span style='background-color:red;'class='ui circular label'>".$pengembalian_dana."</span>";
	    	}
    	}elseif($menu == 'realisasiBiaya'){
    		if($user->hasRole(['keuangan','admin'])){
    		 	$one  = App\Models\VirtualAccount\KonfirmasiPembayaran::where('status', 3)
        						->whereHas('penerimaan', function($penerimaan){
                                    $penerimaan->doesnthave('closeorder');
                                })
                                ->orDoesntHave('penerimaan')
                                ->where('tipe', 0)
        						->get()->count();
		    }else{
		    	$one='';
		    }

		    if($one > 0){
		        echo "<span style='background-color:red;'class='ui circular label'>".$one."</span>";
	    	}
    	}elseif($menu == 'orderUjiSerahTerima'){
    		if($user->hasRole(['pelaksana-tegangan-rendah', 'yan-uji', 'msb-tegangan-rendah', 'asman-lola-tegangan-rendah', 'asman-dal-tegangan-rendah', 'adminlab-tegangan-rendah','admin'])){
    			$records = App\Models\OrderUji\UjiSerahTerima::where('layanan_id', 4)
	    														->whereHas('pp', function($u){
				    												$u->whereIn('status', [0,1,2,3,4,10]);
				    											})->get()->count();
	    	}elseif($user->hasRole(['pelaksana-tegangan-tinggi', 'yan-uji', 'msb-tegangan-tinggi', 'asman-lola-tegangan-tinggi', 'asman-dal-tegangan-tinggi', 'adminlab-tegangan-tinggi','admin'])){
		        $records = App\Models\OrderUji\UjiSerahTerima::where('layanan_id', 5)
		        								->whereHas('pp', function($u){
    												$u->whereIn('status', [0,1,2,3,4,10]);
    											})->get()->count();
	    	}else{
	    		$records = 0;
	    	}

		    if($records > 0){
		        echo "<span style='background-color:red;'class='ui circular label'>".$records."</span>";
	    	}
    	}elseif($menu == 'penerimaanBarangUji'){
    		if(auth()->user()->hasRole(['yan-uji']) || auth()->user()->hasRole(['admin']) || auth()->user()->hasRole(['adminlab-sistgi']) || auth()->user()->hasRole(['adminlab-tegangan-rendah']) || auth()->user()->hasRole(['adminlab-tegangan-tinggi'])){
	        	if(auth()->user()->hasRole(['adminlab-siskit','admin'])){
			        $records = App\Models\VirtualAccount\KonfirmasiPembayaran::where('status', '>', 0)
				        							 ->whereHas('va', function($z){
				        							 	$z->whereHas('surat', function($y){
				        							 		$y->whereHas('kaji_ulang', function($x){
				        							 			$x->whereHas('pp', function($m){
				        							 				$m->where('jenis', 1)
				        							 				  ->where('layanan_id', 2);
				        							 			});
				        							 		});
				        							 	});
				        							})
				        							->where('tipe', 0)
					        						->doesntHave('penerimaan')
			                                 		->get()->count();
			    }elseif(auth()->user()->hasRole(['adminlab-sistgi','admin'])){
			    	$records = App\Models\VirtualAccount\KonfirmasiPembayaran::where('status', '>', 0)
				        							 ->whereHas('va', function($z){
				        							 	$z->whereHas('surat', function($y){
				        							 		$y->whereHas('kaji_ulang', function($x){
				        							 			$x->whereHas('pp', function($m){
				        							 				$m->where('jenis', 1)
				        							 				  ->where('layanan_id', 3);
				        							 			});
				        							 		});
				        							 	});
				        							})
				        							->where('tipe', 0)
					        						->doesntHave('penerimaan')
			                                 		->get()->count();
			    }elseif(auth()->user()->hasRole(['adminlab-tegangan-rendah','admin'])){
			    	$records = App\Models\VirtualAccount\KonfirmasiPembayaran::where('status', '>', 0)
				        							 ->whereHas('va', function($z){
				        							 	$z->whereHas('surat', function($y){
				        							 		$y->whereHas('kaji_ulang', function($x){
				        							 			$x->whereHas('pp', function($m){
				        							 				$m->where('jenis', 1)
				        							 				  ->where('layanan_id', 4);
				        							 			});
				        							 		});
				        							 	});
				        							})
				        							->where('tipe', 0)
					        						->doesntHave('penerimaan')
			                                 		->get()->count();
			    }elseif(auth()->user()->hasRole(['adminlab-tegangan-tinggi','admin'])){
			    	$records = App\Models\VirtualAccount\KonfirmasiPembayaran::where('status', '>', 0)
				        							->whereHas('va', function($z){
				        							 	$z->whereHas('surat', function($y){
				        							 		$y->whereHas('kaji_ulang', function($x){
				        							 			$x->whereHas('pp', function($m){
				        							 				$m->where('jenis', 1)
				        							 				  ->where('layanan_id', 5);
				        							 			});
				        							 		});
				        							 	});
				        							})
				        						->where('tipe', 0)
					        						->doesntHave('penerimaan')
			                                 		->get()->count();
			    }else{
			    	$records = App\Models\VirtualAccount\KonfirmasiPembayaran::where('status', '>', 0)
				        							->whereHas('va', function($z){
				        							 	$z->whereHas('surat', function($y){
				        							 		$y->whereHas('kaji_ulang', function($x){
				        							 			$x->whereHas('pp', function($m){
				        							 				$m->where('jenis', 1);
				        							 			});
				        							 		});
				        							 	});
				        							})
				        						->where('tipe', 0)
					        						->doesntHave('penerimaan')
			                                 		->get()->count();
			    }
	    	}elseif(auth()->user()->hasRole(['yan-kalibrasi']) || auth()->user()->hasRole(['adminlab-kalibrasi']) || auth()->user()->hasRole(['admin'])){
	    		$records = App\Models\VirtualAccount\KonfirmasiPembayaran::where('status', '>', 0)
			        							->whereHas('va', function($z){
			        							 	$z->whereHas('surat', function($y){
			        							 		$y->whereHas('kaji_ulang', function($x){
			        							 			$x->whereHas('pp', function($m){
			        							 				$m->where('jenis', 0);
			        							 			});
			        							 		});
			        							 	});
			        							})
			        						->where('tipe', 0)
				        						->doesntHave('penerimaan')
		                                 		->get()->count();
	    	}else{
	    		$records ='';
	    	}

	    	if($records > 0){
		        echo "<span style='background-color:red;'class='ui circular label'>".$records."</span>";
	    	}
    	}elseif($menu == 'reschedule'){
    		if($user->hasRole(['asman-dal-kalibrasi','asman-lola-kalibrasi','admin'])){
	        		$satu = App\Models\Reschedule\Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
	        							$u->where('layanan_id', 1);
	        						})
	        						->where('status', 0)->get()->count();
	    	}elseif($user->hasRole(['asman-dal-siskit','asman-lola-siskit','admin'])){
		        	$satu = App\Models\Reschedule\Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
		        							$u->where('layanan_id', 2);
		        						})
		        						->where('status', 0)->get()->count();
	    	}elseif($user->hasRole(['asman-dal-siskit','asman-lola-siskit','admin'])){
		        	$satu = App\Models\Reschedule\Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
		        							$u->where('layanan_id', 3);
		        						})
		        						->where('status', 0)->get()->count();
	    	}elseif($user->hasRole(['asman-dal-tegangan-rendah','asman-lola-tegangan-rendah','admin'])){
		        	$satu = App\Models\Reschedule\Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
		        							$u->where('layanan_id', 4);
		        						})
		        						->where('status', 0)->get()->count();
	    	}elseif($user->hasRole(['asman-dal-tegangan-tinggi','asman-lola-tegangan-tinggi','admin'])){
		        	$satu = App\Models\Reschedule\Reschedule::whereHas('pengujian.penerimaan.konfirmasi.va.surat.kaji_ulang.pp', function($u){
		        							$u->where('layanan_id', 5);
		        						})
		        						->where('status', 0)->get()->count();
	    	}else{
	    		$satu = 0;
	    	}

		    if($satu > 0){
		        echo "<span style='background-color:red;'class='ui circular label'>".$satu."</span>";
	    	}
    	}elseif($menu == 'pengirimanLaporan'){
			if($user->hasRole(['yan-kalibrasi','admin'])){
				$records = App\Models\LaporanPengujian\LaporanPengujian::where('status', 0)
									->whereHas('pengujian.penerimaan', function($u){
											$u->byLayanan(1);
									})
									->get()->count();
			}elseif($user->hasRole(['yan-uji','admin'])){
				$records = App\Models\LaporanPengujian\LaporanPengujian::where('status', 0)
									->whereHas('pengujian.penerimaan', function($u){
											$u->byLayananV2([2,3,4,5]);
									})
									->get()->count();
			}else{
				$records ='';
			}

		    if($records > 0){
		        echo "<span style='background-color:red;'class='ui circular label'>".$records."</span>";
	    	}
    	}elseif($menu == 'closeOrder'){
    		if($user->hasRole(['msb-kalibrasi','admin'])){
            	$satu = App\Models\PenerimaanBarang\PenerimaanBarang::doesntHave('closeorder')->byLayanan(1)->get()->count();
	        }elseif($user->hasRole(['msb-siskit','admin'])){
	        	$satu = App\Models\PenerimaanBarang\PenerimaanBarang::doesntHave('closeorder')->byLayanan(2)->get()->count();
	        }elseif($user->hasRole(['msb-sistgi','admin'])){
	        	$satu = App\Models\PenerimaanBarang\PenerimaanBarang::doesntHave('closeorder')->byLayanan(3)->get()->count();
	        }elseif($user->hasRole(['msb-tegangan-rendah','admin'])){
	        	$satu = App\Models\PenerimaanBarang\PenerimaanBarang::doesntHave('closeorder')->byLayanan(4)->get()->count();
	        }elseif($user->hasRole(['msb-tegangan-tinggi','admin'])){
	        	$satu = App\Models\PenerimaanBarang\PenerimaanBarang::doesntHave('closeorder')->byLayanan(5)->get()->count();
	        }else{
	        	$satu = 0;
	        }

    		if($satu > 0){
		        echo "<span style='background-color:red;'class='ui circular label'>".$satu."</span>";
	    	}
    	}elseif($menu == 'pengaduan'){
    		$records =App\Models\Aduan::where('status', 0)->orderBy('created_at','asc')->get()->count();
    		
    		if($records > 0){
		        echo "<span style='background-color:red;'class='ui circular label'>".$records."</span>";
	    	}
    	}elseif($menu == 'aktivasiPemintaJasa'){
    		$active = App\Models\Master\Pelanggan::where('status', 1)->count();
	        if($active>0){
	            echo "<span style='background-color:red;'class='ui circular label'>".$active."</span>";
	        }
    	}elseif($menu == 'pengembalianBarangUji'){
    		if(auth()->user()->hasRole(['yan-kalibrasi','adminlab-kalibrasi','admin'])){
    			$satu = App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('pengujian', function($u){
	        								$u->where('status_data_pengujian', 1);
	        							})
    									->whereHas('detail_penerimaan_barang', function($u) {
						    			  $u->whereHas('detail_pengujian', function($z){
						    			  	  $z->whereHas('detailpelaksana', function($w){
						    			  	  	$w->where('status_barang', 1);
						    			  	  });
						    			  	})
						    			    ->where('flag', 0);
							    		})
    									->byLayanan(1)
	    								->where('status_pengembalian', 0)
	    								->get()->count();	
	    	}elseif(auth()->user()->hasRole(['adminlab-siskit','admin'])){
	    		$satu = App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('pengujian', function($u){
		        								$u->where('status_data_pengujian', 1);
		        							})
	    									->whereHas('detail_penerimaan_barang', function($u) {
							    			  $u->whereHas('detail_pengujian', function($z){
							    			  	  $z->whereHas('detailpelaksana', function($w){
							    			  	  	$w->where('status_barang', 1);
							    			  	  });
							    			  	})
							    			    ->where('flag', 0);
								    		})
	    									->byLayanan(2)
		    								->where('status_pengembalian', 0)
		    								->get()->count();
	    	}elseif(auth()->user()->hasRole(['adminlab-sistgi','admin'])){
	    		$satu = App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('pengujian', function($u){
		        								$u->where('status_data_pengujian', 1);
		        							})
	    									->whereHas('detail_penerimaan_barang', function($u) {
							    			  $u->whereHas('detail_pengujian', function($z){
							    			  	  $z->whereHas('detailpelaksana', function($w){
							    			  	  	$w->where('status_barang', 1);
							    			  	  });
							    			  	})
							    			    ->where('flag', 0);
								    		})
	    									->byLayanan(3)
		    								->where('status_pengembalian', 0)
		    								->get()->count();
	    	}elseif(auth()->user()->hasRole(['adminlab-tegangan-rendah','admin'])){
	    		$satu = App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('pengujian', function($u){
		        								$u->where('status_data_pengujian', 1);
		        							})
	    									->whereHas('detail_penerimaan_barang', function($u) {
							    			  $u->whereHas('detail_pengujian', function($z){
							    			  	  $z->whereHas('detailpelaksana', function($w){
							    			  	  	$w->where('status_barang', 1);
							    			  	  });
							    			  	})
							    			    ->where('flag', 0);
								    		})
	    									->byLayanan(4)
		    								->where('status_pengembalian', 0)
		    								->get()->count();
	    	}elseif(auth()->user()->hasRole(['adminlab-tegangan-tinggi','admin'])){
	    		$satu = App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('pengujian', function($u){
		        								$u->where('status_data_pengujian', 1);
		        							})
	    									->whereHas('detail_penerimaan_barang', function($u) {
							    			  $u->whereHas('detail_pengujian', function($z){
							    			  	  $z->whereHas('detailpelaksana', function($w){
							    			  	  	$w->where('status_barang', 1);
							    			  	  });
							    			  	})
							    			    ->where('flag', 0);
								    		})
	    									->byLayanan(5)
		    								->where('status_pengembalian', 0)
		    								->get()->count();
	    	}elseif(auth()->user()->hasRole(['yan-uji','admin'])){
	    		$satu = App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('pengujian', function($u){
		        								$u->where('status_data_pengujian', 1);
		        							})
	    									->whereHas('detail_penerimaan_barang', function($u) {
							    			  $u->whereHas('detail_pengujian', function($z){
							    			  	  $z->whereHas('detailpelaksana', function($w){
							    			  	  	$w->where('status_barang', 1);
							    			  	  });
							    			  	})
							    			    ->where('flag', 0);
								    		})
	    									->byLayananV2([2,3,4,5])
		    								->where('status_pengembalian', 0)
		    								->get()->count();
	    	}else{
	    		$satu = 0;
	    	}

	        if($satu>0){
	            echo "<span style='background-color:red;'class='ui circular label'>".$satu."</span>";
	        }
    	}elseif($menu == 'laporanPengujian'){
	    	if($user->hasRole(['srm-prosmkal','yan-kalibrasi', 'msb-kalibrasi', 'adminlab-kalibrasi', 'asman-dal-kalibrasi', 'asman-lola-kalibrasi', 'pelaksana-kalibrasi','admin'])){
		        $satu = App\Models\LaporanPengujian\LaporanPengujian::whereHas('pengujian', function($u){
		        								$u->where('tipe', 1);
		        							})->get()->count();
	    	}elseif($user->hasRole(['msb-siskit', 'adminlab-siskit', 'asman-dal-siskit', 'asman-lola-siskit', 'pelaksana-siskit','admin'])){
	    		$satu = App\Models\LaporanPengujian\LaporanPengujian::whereHas('pengujian', function($u){
		        								$u->where('tipe', 2);
		        							})->get()->count();
	    	}elseif($user->hasRole(['msb-sistgi', 'adminlab-sistgi', 'asman-dal-sistgi', 'asman-lola-sistgi', 'pelaksana-sistgi','admin'])){
	    		$satu = App\Models\LaporanPengujian\LaporanPengujian::whereHas('pengujian', function($u){
		        								$u->where('tipe', 3);
		        							})->get()->count();
	    	}elseif($user->hasRole(['msb-tegangan-rendah', 'adminlab-tegangan-rendah', 'asman-dal-tegangan-rendah', 'asman-lola-tegangan-rendah', 'pelaksana-tegangan-rendah','admin'])){
	    		$satu = App\Models\LaporanPengujian\LaporanPengujian::whereHas('pengujian', function($u){
		        								$u->where('tipe', 4);
		        							})->get()->count();
	    	}elseif($user->hasRole(['msb-tegangan-tinggi', 'adminlab-tegangan-tinggi', 'asman-dal-tegangan-tinggi', 'asman-lola-tegangan-tinggi', 'pelaksana-tegangan-tinggi','admin'])){
	    		$satu = App\Models\LaporanPengujian\LaporanPengujian::whereHas('pengujian', function($u){
		        								$u->where('tipe', 5);
		        							})->get()->count();
	    	}elseif($user->hasRole(['yan-uji', 'srm-uji','admin'])){
	    		$satu = App\Models\LaporanPengujian\LaporanPengujian::whereHas('pengujian', function($u){
		        								$u->whereIn('tipe', [2,3,4,5]);
		        							})->get()->count();
	    	}elseif($user->hasRole(['gm','admin'])){
	    		$satu = App\Models\LaporanPengujian\LaporanPengujian::get()->count();
	    	}else{
	    		$satu = App\Models\LaporanPengujian\LaporanPengujian::where('id', 0)->get()->count();;
	    	}

        	if($satu>0){
	        	echo "<span style='background-color:red;color: #ffffff;'class='ui circular label'>".$satu."</span>";
        	}
        }elseif($menu == 'historikalData'){
        	// if($user->hasRole(['yan-uji'])){
        	// 	$satu = App\Models\FrontEnd\PendaftaranPengujian::where('jenis',0)->get()->count();
        	// }elseif($user->hasRole(['yan-kalibrasi'])){
        	// 	$satu = App\Models\FrontEnd\PendaftaranPengujian::where('jenis',1)->get()->count();
        	// }else{
        	// 	$satu ='';
        	// }
        	// if($satu>0){
	        // 	echo "<span style='background-color:red;color: #ffffff;'class='ui circular label'>".$satu."</span>";
        	// }
        }else{

    	}
    }
}

if (!function_exists('bubblehead')) {
    function bubblehead($menu)
    {
    	$user = auth()->user();
        if($menu == 'pengujian'){
        	if($user->hasRole(['pelaksana-kalibrasi','admin'])){
	        	$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(1)
	        														->where('status', 1)
								                                    ->where('status_pengujian', 0)
								                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(1)
	            													->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}elseif($user->hasRole(['pelaksana-siskit','admin'])){
	        	$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(2)
	        														->where('status', 1)
								                                    ->where('status_pengujian', 0)
								                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(2)
	            													->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}elseif($user->hasRole(['pelaksana-sistgi','admin'])){
	        	$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(3)
	        														->where('status', 1)
								                                    ->where('status_pengujian', 0)
								                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(3)
	            													->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}elseif($user->hasRole(['pelaksana-tegangan-rendah','admin'])){
	        	$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(4)
	        														->where('status', 1)
								                                    ->where('status_pengujian', 0)
								                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(4)
	            													->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}elseif($user->hasRole(['pelaksana-tegangan-tinggi','admin'])){
	        	$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(5)
	        														->where('status', 1)
								                                    ->where('status_pengujian', 0)
								                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(5)
	            													->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}elseif($user->hasRole(['asman-dal-kalibrasi', 'asman-lola-kalibrasi','admin'])){
        		$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(1)
        															->where('status', 1)
								                                    ->where('status_pengujian', 0)
								                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(1)
	            													->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}elseif($user->hasRole(['asman-dal-siskit', 'asman-lola-siskit','admin'])){
        		$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(2)
        															->where('status', 1)
								                                    ->where('status_pengujian', 0)
								                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(2)
	            													->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}elseif($user->hasRole(['asman-dal-sistgi', 'asman-lola-sistgi','admin'])){
        		$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(3)
        															->where('status', 1)
								                                    ->where('status_pengujian', 0)
								                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(3)
	            													->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}elseif($user->hasRole(['asman-dal-tegangan-rendah', 'asman-lola-tegangan-rendah','admin'])){
        		$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(4)
        															->where('status', 1)
								                                    ->where('status_pengujian', 0)
								                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(4)
	            													->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}elseif($user->hasRole(['asman-dal-tegangan-tinggi', 'asman-lola-tegangan-tinggi','admin'])){
        		$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(5)
        															->where('status', 1)
								                                    ->where('status_pengujian', 0)
								                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(5)
	            													->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}elseif($user->hasRole(['yan-kalibrasi','msb-kalibrasi','admin'])){
        		$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(1)
        															->where('status', 1)
								                                    ->where('status_pengujian', 0)
								                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(1)
	            													->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}elseif($user->hasRole(['yan-uji','admin'])){
        		$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
																            $k->whereHas('va', function($v){
																                $v->whereHas('surat', function($s){
																                    $s->whereHas('kaji_ulang', function($ku){
																                        $ku->whereHas('pp', function($p){
																                            $p->whereIn('layanan_id', [2,3,4,5]);
																                        });
																                    });
																                });
																            })->whereIn('tipe', [0,1]);
														        		})
	        															->where('status', 1)
									                                    ->where('status_pengujian', 0)
									                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
															            $k->whereHas('va', function($v){
															                $v->whereHas('surat', function($s){
															                    $s->whereHas('kaji_ulang', function($ku){
															                        $ku->whereHas('pp', function($p){
															                            $p->whereIn('layanan_id', [2,3,4,5]);
															                        });
															                    });
															                });
															            })->whereIn('tipe', [0,1]);
														        	})
	            													->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}elseif($user->hasRole(['msb-siskit','admin'])){
        		$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
																            $k->whereHas('va', function($v){
																                $v->whereHas('surat', function($s){
																                    $s->whereHas('kaji_ulang', function($ku){
																                        $ku->whereHas('pp', function($p){
																                            $p->where('layanan_id', 2);
																                        });
																                    });
																                });
																            })->whereIn('tipe', [0,1]);
														        		})
	        															->where('status', 1)
									                                    ->where('status_pengujian', 0)
									                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
															            $k->whereHas('va', function($v){
															                $v->whereHas('surat', function($s){
															                    $s->whereHas('kaji_ulang', function($ku){
															                        $ku->whereHas('pp', function($p){
															                            $p->where('layanan_id', 2);
															                        });
															                    });
															                });
															            })->whereIn('tipe', [0,1]);
														        	})
	            													->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}elseif($user->hasRole(['msb-sistgi','admin'])){
        		$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
																            $k->whereHas('va', function($v){
																                $v->whereHas('surat', function($s){
																                    $s->whereHas('kaji_ulang', function($ku){
																                        $ku->whereHas('pp', function($p){
																                            $p->where('layanan_id', 3);
																                        });
																                    });
																                });
																            })->whereIn('tipe', [0,1]);
														        		})
	        															->where('status', 1)
									                                    ->where('status_pengujian', 0)
									                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
															            $k->whereHas('va', function($v){
															                $v->whereHas('surat', function($s){
															                    $s->whereHas('kaji_ulang', function($ku){
															                        $ku->whereHas('pp', function($p){
															                            $p->where('layanan_id', 3);
															                        });
															                    });
															                });
															            })->whereIn('tipe', [0,1]);
														        	})
	            													->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}elseif($user->hasRole(['msb-tegangan-rendah','admin'])){
        		$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
																            $k->whereHas('va', function($v){
																                $v->whereHas('surat', function($s){
																                    $s->whereHas('kaji_ulang', function($ku){
																                        $ku->whereHas('pp', function($p){
																                            $p->where('layanan_id', 4);
																                        });
																                    });
																                });
																            })->whereIn('tipe', [0,1]);
														        		})
	        															->where('status', 1)
									                                    ->where('status_pengujian', 0)
									                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
															            $k->whereHas('va', function($v){
															                $v->whereHas('surat', function($s){
															                    $s->whereHas('kaji_ulang', function($ku){
															                        $ku->whereHas('pp', function($p){
															                            $p->where('layanan_id', 4);
															                        });
															                    });
															                })->whereIn('tipe', [0,1]);
															            });
														        	})
	            													->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();	
	            $records = $satu + $dua;
        	}elseif($user->hasRole(['msb-tegangan-tinggi','admin'])){
        		$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
																            $k->whereHas('va', function($v){
																                $v->whereHas('surat', function($s){
																                    $s->whereHas('kaji_ulang', function($ku){
																                        $ku->whereHas('pp', function($p){
																                            $p->where('layanan_id', 5);
																                        });
																                    });
																                });
																            })->whereIn('tipe', [0,1]);
														        		})
	        															->where('status', 1)
									                                    ->where('status_pengujian', 0)
									                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::whereHas('konfirmasi', function($k){
															            $k->whereHas('va', function($v){
															                $v->whereHas('surat', function($s){
															                    $s->whereHas('kaji_ulang', function($ku){
															                        $ku->whereHas('pp', function($p){
															                            $p->where('layanan_id', 5);
															                        });
															                    });
															                });
															            })->whereIn('tipe', [0,1]);
														        	})
	            													->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}else{
        		$records='';
        	}
        	if($records>0){
	        	echo "<span style='background-color:red;color: #ffffff;'class='ui circular label'>".$records."</span>";
        	}
        }else{
        }
    }
}

if (!function_exists('bubblechild')) {
    function bubblechild($menu)
    {
    	$user = auth()->user();
        if($menu == 'kalibrasi'){
        	if($user->hasRole(['asman-dal-kalibrasi', 'yan-kalibrasi', 'msb-kalibrasi', 'pelaksana-kalibrasi', 'asman-lola-kalibrasi','admin'])){
	        	$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(1) // 1 = Jenis layanan Kalibrasi
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 0)
								                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(1) // 1 = Jenis layanan Kalibrasi
								                                    ->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}else{
        		$records='';
        	}
        	if($records>0){
	        	echo "<span style='background-color:red;'class='ui circular label'>".$records."</span>";
        	}
        }elseif($menu == 'tR'){
        	if($user->hasRole(['asman-dal-tegangan-rendah', 'yan-uji', 'msb-tegangan-rendah', 'pelaksana-tegangan-rendah', 'asman-lola-tegangan-rendah','admin'])){
	        	$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(4) // 1 = Jenis layanan Tegangan Rendah
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 0)
								                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(4) // 1 = Jenis layanan Tegangan Rendah
								                                    ->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}else{
        		$records='';
        	}
        	if($records>0){
	        	echo "<span style='background-color:red;'class='ui circular label'>".$records."</span>";
        	}
        }elseif($menu == 'tT'){
        	if($user->hasRole(['asman-dal-tegangan-tinggi', 'yan-uji', 'msb-tegangan-tinggi', 'pelaksana-tegangan-tinggi', 'asman-lola-tegangan-tinggi','admin'])){
	        	$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(5) // 1 = Jenis layanan Tegangan Tinggi
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 0)
								                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(5) // 1 = Jenis layanan Tegangan Tinggi
								                                    ->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}else{
        		$records='';
        	}
        	if($records>0){
	        	echo "<span style='background-color:red;'class='ui circular label'>".$records."</span>";
        	}
        }elseif($menu == 'sISKIT'){
        	if($user->hasRole(['asman-dal-siskit', 'yan-uji', 'msb-siskit', 'pelaksana-siskit', 'asman-lola-siskit','admin'])){
	        	$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(2) // 1 = Jenis layanan SISKIT
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 0)
								                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(2) // 1 = Jenis layanan SISKIT
								                                    ->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}else{
        		$records='';
        	}
        	if($records>0){
	        	echo "<span style='background-color:red;'class='ui circular label'>".$records."</span>";
        	}
        }elseif($menu == 'sISTGI'){
        	if($user->hasRole(['asman-dal-sistgi', 'yan-uji', 'msb-sistgi', 'pelaksana-sistgi', 'asman-lola-sistgi','admin'])){
	        	$satu =  App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(3) // 1 = Jenis layanan SISTGI
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 0)
								                                    ->get()->count();

	            $dua = App\Models\PenerimaanBarang\PenerimaanBarang::byLayanan(3) // 1 = Jenis layanan SISTGI
								                                    ->whereHas('pengujian', function($u){
								                                    	$u->where('status_data_pengujian', 0);
								                                    })
								                                    ->where('status', 1)
								                                    ->where('status_pengujian', 1)
								                                    ->get()->count();
	            $records = $satu + $dua;
        	}else{
        		$records='';
        	}
        	if($records>0){
	        	echo "<span style='background-color:red;'class='ui circular label'>".$records."</span>";
        	}
        }else{

        }
    }
}

if (!function_exists('riwayatAktivitas')) {
    function riwayatAktivitas($data)
    {
        $riwayat = '';
        if($data->act_dispo()->count()>0){
            $dispo = $data->act_dispo->sortByDesc('created_at');
            $prev = $data->logsyan()->first(); 
            foreach ($dispo as $key => $cek) {
                $Date1  = $cek->created_at;
                $prev   = $cek->prev();
                $text = '';
                switch ($cek->tipe) {
                    case 0:
                        $text = '<a> '.$cek->pengirim->nama .' </a> mengubah data Layanan, Lingkup dan Jenis Pengujian';
                        break;
                    case 1:
                        $text = '<a> '. $cek->pengirim->nama .' </a>  Mengalihkan surat <a> '. $data->no_order .'</a> kepada <a> '. $cek->penerima->nama .' </a> '.(($cek->keterangan) ? '<i>dengan catatan : '. $cek->keterangan .' </i>' : '');
                        break;
                    case 2:
                        $text = '<a> '. $cek->pengirim->nama .' </a>  Mendisposisikan surat <a> '. $data->no_order .' </a> kepada <a> '. $cek->penerima->nama .' </a> '. (($cek->keterangan) ? '<i>dengan catatan : '.  $cek->keterangan .'</i>' : '' ) .'
                                , dalam waktu &nbsp;&nbsp; '. timestampReturn($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name).' ';
                        break;
                    case 3:
                        $status_kaji = '';
                        $ket = '';
                        if($cek->status_kaji_ulang == 1){
                            $status_kaji = 'Diterima';
                            $ket = ', saat ini surat akan masuk ke tahap <a>Surat Penawaran</a>';
                        }elseif($cek->status_kaji_ulang == 2){
                            $status_kaji = 'Ditolak';
                            $ket = ', saat ini masuk ke tahap <a>Surat Penolakan</a> Oleh Yan.';
                        }elseif($cek->status_kaji_ulang == 3){
                            $status_kaji = 'Didiskusikan';
                            $ket = ', saat ini surat masih menunggu status selanjutnya.';
                        }
                        $text = '<a> '. $cek->pengirim->nama .' </a> menyelesaikan <a>Kaji Ulang</a> dengan status <a> '.$status_kaji.' </a>, dalam waktu &nbsp;&nbsp; '. timestampReturn($prev,$Date1,$cek->jenis,$cek->pengirim->roles->first()->name).' '.$ket;
                        break;
                    case 4:
                    	if($cek->status_surat_penawaran > 0){
	                        $text = '<a> '. $cek->pengirim->nama .' </a> merubah <a>Surat Penawaran,</a> No Order <a> '. $data->no_order .' </a> '.(($cek->status_surat_penawaran > 0) ? ' dengan <a> Adendum '. $cek->status_surat_penawaran.' </a>' : '').', saat ini surat menunggu untuk dikirim ke <a> Peminta Jasa</a>';
                    	}else{
                    		$text = '<a> '. $cek->pengirim->nama .' </a> menyelesaikan <a>Surat Penawaran,</a> No Order <a> '. $data->no_order .' </a> '.(($cek->status_surat_penawaran > 0) ? ' dengan <a> Adendum '. $cek->status_surat_penawaran.' </a>' : '').', saat ini surat menunggu untuk dikirim ke <a> Peminta Jasa</a>';
                    	}
                        break;
                    case 5:
                   		if($data->user->pelanggans->perusahaan->kategori == 0){
                   			$text = '<a> '. $cek->pengirim->nama .'</a> mengirim email ke <a> '.($data->user->pelanggans->perusahaan->nama) .' </a> dengan No Order <a> '. $data->no_order .' </a>, saat ini surat masuk ke tahap <a>Virtual Account</a>';
                    	}else{
	                        $text = '<a> '. $cek->pengirim->nama .' </a> menyelesaikan <a> Surat Penawaran,</a> No Order <a> '. $data->no_order .' </a> '.(($cek->status_surat_penawaran > 0) ? ' dengan <a> Adendum '. $cek->status_surat_penawaran .' </a>' : '').' dan mengirim email ke <a> '.($data->user->pelanggans->perusahaan->nama) .' </a>, saat ini surat masuk ke tahap <a>Virtual Account</a>';
                    	}
                        break;
                    case 6:
                    	if($cek->jenis == 5){
	                        $text = '<a> '. $cek->pengirim->nama .' </a>  Mendisposisikan surat <a> '. $data->no_order .' </a> kepada <a> '. $cek->penerima->nama .' </a> untuk dilakukan tahap <a>Verifikasi</a> ';
                    	}else{
                    		$text = '<a> '. $cek->pengirim->nama .' </a>  Mendisposisikan surat <a> '. $data->no_order .' </a> kepada <a> '. $cek->penerima->nama .' </a> untuk dilakukan tahap <a>Pengujian</a> ';
                    	}
                        break;
                    case 7:
                    	if($cek->jenis == 6){
                    		$text = $cek->keterangan;
                    	}else{
	                        $text = '<a> '. $cek->pengirim->nama .' </a> '.$cek->keterangan;
                    	}
                        break;
                    case 8:
                        $text = '<a> '. $cek->pengirim->nama .' </a> '.$cek->keterangan;
                        break;
                    case 9:
                        $text = '<a> '. $cek->pengirim->nama .' </a> '.$cek->keterangan;
                        break;
                }
                if($cek->tipe != 10 AND $cek->tipe != 11){
	                $riwayat .= '<div class="ui feed">
	                                <div class="event">
	                                    <div class="label">
	                                        <img src="'.url(asset('img/avatar04.png')).'">
	                                    </div>
	                                    <div class="content">
	                                        <div class="date">
	                                            '.$cek->created_at->diffForHumans().'
	                                        </div>
	                                        <div class="summary">
	                                        '.$text.'
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>';
                }
            }
        }

        $riwayat .= '<div class="ui feed">
                        <div class="event">
                            <div class="label">
                                <img src="'.url(asset('img/avatar04.png')).'">
                            </div>
                            <div class="content">
                                <div class="date">
                                    '.$data->created_at->diffForHumans().'
                                </div>
                                <div class="summary">
                                    <a>Penerimaan Surat</a>
                                </div>
                            </div>
                        </div>
                    </div>';

        echo $riwayat;
    }
}

if (!function_exists('riwayatPengujian')) {
    function riwayatPengujian($data)
    {
        $riwayat = '';
        if(isset($data->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian)){
        	$detail = $data->kaji_ulang->surat->va->konfirmasi->penerimaan->pengujian->detailpengujian->pluck('id');
        	if($detail){
	            $dispo = \App\Models\Act\ActPengujian::whereIn('parent_id', $detail)->orderBy('created_at','desc')->get();
        	}else{
        		$dispo = \App\Models\Act\ActPengujian::where('parent_id', $data->kaji_ulang->pp->id)->orderBy('created_at','desc')->get();
        	}
            foreach ($dispo as $key => $cek) {
                $text = '';
                $text = $cek->keterangan;
                $riwayat .= '<div class="ui feed">
                                <div class="event">
                                    <div class="label">
                                        <img src="'.url(asset('img/avatar04.png')).'">
                                    </div>
                                    <div class="content">
                                        <div class="date">
                                            '.$cek->created_at->diffForHumans().'
                                        </div>
                                        <div class="summary">
                                        '.$text.'
                                        </div>
                                    </div>
                                </div>
                            </div>';
            }
        }

        echo $riwayat;
    }
}