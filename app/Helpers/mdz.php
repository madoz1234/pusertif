<?php
if (!function_exists('generateRandomString')) {
    function generateRandomString($length = 10)
    {
    	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	$charactersLength = strlen($characters);
    	$randomString = '';
    	for ($i = 0; $i < $length; $i++) {
    		$randomString .= $characters[rand(0, $charactersLength - 1)];
    	}
    	return $randomString;
    }
}

if (!function_exists('sendEmail')) {
    function sendEmail($data){
       		$email = $data['email'];
            $test =[
                'subject' => 'Register',
                'content' => $data,
            ];
            $nama = config('mail.from.name');
            \Mail::send('email.register', $test, function ($message) use ($email, $nama){
	            $message->from($nama);
	            $message->to($email)->subject('Register Pusat Sertifikasi PT. PLN Persero');
            });
    }
}

if (!function_exists('sendEmaila')) {
    function sendEmaila($data){
       		$email = $data['email'];
            $test =[
                'subject' => 'Register',
                'content' => $data,
            ];
            $nama = config('mail.from.name');
            \Mail::send('email.registera', $test, function ($message) use ($email, $nama){
	            $message->from($nama);
	            $message->to($email)->subject('Register Pusat Sertifikasi PT. PLN Persero');
            });
    }
}

if (!function_exists('sendEmailBriefingJadwal')) {
    function sendEmailBriefingJadwal($data){
       		$email = $data['email'];
            $test =[
                'subject' => 'Jadwal Briefing Teknis',
                'content' => $data,
            ];
            $nama = config('mail.from.name');
            \Mail::send('email.briefing-jadwal', $test, function ($message) use ($email, $nama){
	            $message->from($nama);
	            $message->to($email)->subject('Jadwal Briefing Teknis Pusat Sertifikasi PT. PLN Persero');
            });
    }
}

if (!function_exists('sendEmailBriefingNotulen')) {
    function sendEmailBriefingNotulen($data){
       		$email = $data['email'];
            $test =[
                'subject' => 'Hasil Briefing Teknis',
                'content' => $data,
            ];
            $nama = config('mail.from.name');
            \Mail::send('email.briefing-notulen', $test, function ($message) use ($email, $nama){
	            $message->from($nama);
	            $message->to($email)->subject('Hasil Briefing Teknis Pusat Sertifikasi PT. PLN Persero');
            });
    }
}

if (!function_exists('sendEmailSuratPenawaran')) {
    function sendEmailSuratPenawaran($data){
       		$email = $data['email'];
            $test =[
                'subject' => 'Surat Penawaran',
                'content' => $data,
            ];
            $nama = config('mail.from.name');
            \Mail::send('email.surat-penawaran', $test, function ($message) use ($email, $nama){
	            $message->from($nama);
	            $message->to($email)->subject('Surat Penawaran Pusat Sertifikasi PT. PLN Persero');
            });
    }
}

if (!function_exists('sendEmailTanggapan')) {
    function sendEmailTanggapan($data){
       		$email = $data['email'];
            $test =[
                'subject' => 'Pengaduan Dan Saran',
                'content' => $data,
            ];
            $nama = config('mail.from.name');
            \Mail::send('email.pengaduan', $test, function ($message) use ($email, $nama){
	            $message->from($nama);
	            $message->to($email)->subject('Pengaduan Dan Saran Pusat Sertifikasi PT. PLN Persero');
            });
    }
}